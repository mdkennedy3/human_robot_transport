# Human Robot Transport #

This code is for research in robots assisting in the carrying of objects with human counter parts.


### Dependencies ###
* For Kinect: https://github.com/orbbec/ros_astra_camera.git  	https://github.com/orbbec/ros_astra_launch.git
  * Remember to create udev rules in astra_camera folder
* For arduino (force sensors from strain gauges): HX711 library found at: https://github.com/bogde/HX711.git  and setup arduino with http://wiki.ros.org/rosserial_arduino/Tutorials making sure sudo chmod 666 /dev/ttyACM0 (or whatever port its on) then to see arduino: rosrun rosserial_python serial_node.py /dev/ttyACM0 (or correct port) 
* For Laser: urg_node: http://wiki.ros.org/urg_node but use apt-get to install
  * for the laser add the udev rules: SUBSYSTEMS=="usb", KERNEL=="ttyACM[0-9]*", ACTION=="add", ATTRS{idVendor}=="15d1", ATTRS{idProduct}=="0000", MODE="666", SYMLINK+="sensors/hokuyo", GROUP="dialout"
for those later than hydro
  * For people tracking, use: https://github.com/LCAS/people_detection.git
  * for the leg-detector node, change the cfg/LegDetector.cfg Fixed Frame variable (usually odom)

* To use the usb, make sure user has group privileges: sudo usermod -a -G dialout monroe;  groups (tells you what you have)
* Side note: to make symbolic links, add a bin folder to home directory, contents will be added to $PATH, then inside bin directory, ln -s path_to_exe/exe exelink, then verify with ls -l path_to_exe/exe exelink

* For vicon: https://github.com/KumarRobotics/motion_capture_system.git  
  * roslaunch mocap_vicon vicon.launch  where server_address = 192.168.129.11 and connected to mrsl internal network
  * model_list: fetch_hat, human_human_load

### Directory ###

* path_planning
  * package se2path_finding, is a first attempt in navigating the holonomic robot in an obstacle filled environment by planning with ompl planner (leveraging pcl library), finding convex corridors, then solving with qp solver. Then implmenting controls to follow the desired trajectory

* intelligent\_coop\_carry
  * This package is for the [fetch mobile manipulator](http://docs.fetchrobotics.com), the fetch moves the arm to a compliant position ready to grasp an object for transport, then human intent quantities along with the system pose/twist are used with knowledge of surrounding obstacles to predict the highest expected desired pose (on a finite time horizon), and the instantaneous expected twist
* gp\_human\_pred
  * This package is for collecting data for two humans performing cooperative carrying, and determining if the proposed metrics are viable when huamns carry and what model is produced given the intent-quantities. 






