Maps are generated using: 
rosrun map_server map_saver -f test_save

save resultant pgm and yaml in this folder

then to load the map
     <arg name="map_file" default="$(find gp_human_pred)/maps/test_save.yaml" />

    <!-- serve up a map -->
    <node name="map_server" pkg="map_server" type="map_server" args="$(arg map_file)" />


