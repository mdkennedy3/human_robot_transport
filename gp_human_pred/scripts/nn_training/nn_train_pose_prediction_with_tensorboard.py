#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import argparse
import sys
#Import Tensor flow
import tensorflow as tf
import rospy
import pdb #for debugging: #pdb.set_trace() (n to next line, c to continue) :: https://docs.python.org/2/library/pdb.html
import numpy as np
import matplotlib.pyplot as plt
import os
import rospkg
import rosbag
from intelligent_coop_carry_msgs.msg import TrainingPoseOutput
import cv2
from cv_bridge import CvBridge, CvBridgeError
# import pickle
import cPickle as pickle
from multiprocessing import Pool
from contextlib import closing

class ImgStruct(object):
    """docstring for ImgStruct"""
    def __init__(self, height=107,width=107,channels=1):
        self.height = height #was 720x720
        self.width = width
        self.channels = channels


class NNPoseTraining(object):
    """docstring for NNPoseTraining"""
    def __init__(self, learning_rate = 0.001, file_writer_path="/tmp/nn_pose_prediction"):
        self.opt = tf.train.RMSPropOptimizer(learning_rate=learning_rate) #learning rate for gradient descent
        # self.session = None
        self.session = tf.Session()

        
        self.writer = tf.summary.FileWriter(file_writer_path+"/train/")
        self.writer_test = tf.summary.FileWriter(file_writer_path+"/test/")

        # self.merged_summary = tf.summary.merge_all()
        # self.writer.add_graph(self.session.graph) #now or later?

        rospack = rospkg.RosPack()
        self.bags_dir_path = rospack.get_path('gp_human_pred') + "/bagfiles/nn_training_data/"
        # self.bag_names = ["nn_trained_data_time_delay_0.500000_sec.bag"]
        self.bridge = CvBridge()
        self.bag_name = None
        self.training_data_msgs = []
        self.local_map_image_list = []
        self.hold_out_set_idx = None
        self.training_set_idx = None

    # def __del__(self):
    #     self.session.close() #destructor

    def init_layers(self,output_size=3, img_struct=ImgStruct()):
        #raw input placeholders:
        self.img_placeholder = tf.placeholder(tf.float64, (None,img_struct.height,img_struct.width,img_struct.channels), name="img_inpt") #expected I_{720x720x1}
        self.head_step_placeholder = tf.placeholder(tf.float64, (None,4), name="head_step_inpt") #head 2x1 over step 2x1
        self.twist_placeholder = tf.placeholder(tf.float64, (None,3), name="twist_inpt")
        self.wrench_placeholder = tf.placeholder(tf.float64, (None,3), name="wrench_inpt")
        #Additional NN Structure: 
        #CNN: https://www.tensorflow.org/tutorials/estimators/cnn 
        self.cnn_layer1  = self.conv_layer(input_var=self.img_placeholder, filters=32, kernel_size=[5,5], pool_size=[2,2], strides=2, name="conv_layer1")
        self.cnn_layer2 =  self.conv_layer(input_var=self.cnn_layer1, filters=1, kernel_size=[5,5], pool_size=[2,2], strides=2, name="conv_layer2")
        self.cnn_outlayer_init = tf.reshape(self.cnn_layer2,[-1,529]) #64 b/c 64 filters  (query with pbd tf.shape(layer))  #Make sure its not val x batch size
        self.cnn_outlayer = tf.layers.dense(self.cnn_outlayer_init, 100,activation=tf.nn.leaky_relu,name="cnn_outlayer") #*** Give this more depth?
        #Head/Step FC network
        self.head_step_FC_layer_setup_1 = self.fc_layer(input_var=self.head_step_placeholder, depth=20, name="head_step_fc_setup_1")
        self.head_step_FC_layer = self.fc_layer(input_var=self.head_step_FC_layer_setup_1, depth=20, name="head_step_fc")
        #make other layers larger for comparison (larger intutiion because its easier to search in larger dim space)
        self.vectors_inpt_to_expand_layers = tf.concat([self.head_step_FC_layer,self.twist_placeholder, self.wrench_placeholder],axis=1) #these are 8x1
        self.vect_expand_dense_layer1 = self.fc_layer(input_var=self.vectors_inpt_to_expand_layers, depth=100, name="vect_expand_dense_layer1")
        self.vect_expand_dense_layer2 = self.fc_layer(input_var=self.vect_expand_dense_layer1, depth=100, name="vect_expand_dense_layer2")
        #make a concatenation for final FC layers or recurrent NN layers
        self.inpt_to_final_layers = tf.concat([self.cnn_outlayer,self.vect_expand_dense_layer2],axis=1)
        #make Final FC layers
        self.final_fc_layer_1 = self.fc_layer(input_var=self.inpt_to_final_layers, depth=50, name="final_fc_l1")
        self.final_fc_layer_2 = self.fc_layer(input_var=self.final_fc_layer_1, depth=40, name="final_fc_l2")
        self.final_fc_layer_3 = self.fc_layer(input_var=self.final_fc_layer_2, depth=10, name="final_fc_l3")
        self.final_fc_layer_4 = self.fc_layer(input_var=self.final_fc_layer_3, depth=10, name="final_fc_l4")
        #now output to final desired size
        self.output = tf.layers.dense(self.final_fc_layer_4, output_size, activation=None, name="output")
        self.label = tf.placeholder(tf.float64, (None, output_size), name="label")
        # pdb.set_trace()


    def conv_layer(self, input_var=None, filters=1, kernel_size=[5,5], pool_size=[2,2], strides=2, name="conv"):
        # with tf.name_scope(name):
        act = tf.layers.conv2d(inputs=input_var,  filters=filters, kernel_size=kernel_size, padding="valid", activation=tf.nn.relu, name=name+"_conv")#size: H/2xW/2
        act = tf.layers.dropout(act,rate=0.1,training=True, name=name+"_dropout")
        pool_layer = tf.layers.max_pooling2d(inputs=act, pool_size=pool_size, strides=strides, name=name+"_pool") #size H/4 x W/4   
        tf.summary.histogram(name+"_activations",act, collections=['train',"train_hist"])
        return pool_layer

    def fc_layer(self, input_var=None, depth=3, name="fc"):
        # with tf.name_scope(name):
        act = tf.layers.dense(input_var,depth,activation=tf.nn.leaky_relu, name=name)
        act = tf.layers.dropout(act,rate=0.2,training=True,name=name+"_dropout")
        return act


    def define_error(self):
        #Define the Error
        # self.error_output = tf.losses.mean_squared_error(self.label, self.output)
        with tf.name_scope("accuracy"):
            pose_xy_gain = 100.0 #converts to cm error
            pose_th_gain = 30.0#80./np.pi

            e_th_out_vect_temp = tf.atan2(tf.sin(self.label[:,2])*tf.cos(self.output[:,2])-tf.cos(self.label[:,2])*tf.sin(self.output[:,2]), tf.cos(self.label[:,2])*tf.cos(self.output[:,2]) + tf.sin(self.label[:,2])*tf.sin(self.output[:,2]) )
            e_th_out_vect = pose_th_gain * e_th_out_vect_temp
            self.e_th_out = tf.reduce_mean(tf.square(e_th_out_vect),axis=0) 
            # e_th_hs_vect = pose_th_gain *  tf.atan2(tf.sin(self.label[:,2])*tf.cos(self.head_step_FC_layer[:,2])-tf.cos(self.label[:,2])*tf.sin(self.head_step_FC_layer[:,2]), tf.cos(self.label[:,2])*tf.cos(self.head_step_FC_layer[:,2]) + tf.sin(self.label[:,2])*tf.sin(self.head_step_FC_layer[:,2]) )
            # self.e_th_hs = tf.reduce_mean(tf.square(e_th_hs_vect),axis=0)

            xy_err_out_diff = pose_xy_gain*self.label[:,:2] - pose_xy_gain*self.output[:,:2]
            # self.xy_err_out = tf.reduce_mean(tf.reduce_sum(tf.square(pose_xy_gain*(self.label[:,:2] - self.output[:,:2])),axis=1) ,axis=0)
            self.xy_err_out = tf.reduce_mean(tf.reduce_sum(tf.square(xy_err_out_diff),axis=1) ,axis=0)

            # xy_err_hs_diff = pose_xy_gain*self.label[:,:2] - pose_xy_gain*self.head_step_FC_layer[:,:2]
            # self.xy_err_hs = tf.reduce_mean(tf.reduce_sum(tf.square(pose_xy_gain*(self.label[:,:2] - self.head_step_FC_layer[:,:2])),axis=1) ,axis=0)
            # self.xy_err_hs = tf.reduce_mean(tf.reduce_sum(tf.square(pose_xy_gain*(self.label[:,:2] - self.head_step_FC_layer[:,:2])),axis=1) ,axis=0)


            # self.error_total = self.e_th_out+self.e_th_hs+self.xy_err_out+self.xy_err_hs
            self.error_total = self.e_th_out + self.xy_err_out #+ self.xy_err_hs

            # tf.summary.scalar('xy_accuracy_out',xy_err_out)
            # tf.summary.scalar('th_accuracy_out',e_th_out)
            # tf.summary.image('input_img',self.img_placeholder,1)

        # Define the training op
        with tf.name_scope('train'):
            self.train_op = self.opt.minimize(self.error_total)

        # /tmp/nn_pose_prediction/test

    def setup_saver_and_visual(self):
        for var in tf.trainable_variables():
            tf.summary.histogram(var.name, var, collections=['train',"train_hist"])
        # Begin training  img is 107x107
        self.writer.add_graph(self.session.graph) #now or later?
        self.persist0 = train_acc_scaler = tf.summary.scalar('error_total',self.error_total,collections=['train','test'])
        
        self.persist1 = tf.summary.scalar('error_xy_sqrd_out',self.xy_err_out, collections=['train','test'])
        self.persist2 = tf.summary.scalar('error_th_sqrd_out',self.e_th_out, collections=['train','test'])
        # self.persist3 = tf.summary.scalar('error_xy_sqrd_hs',self.xy_err_hs, collections=['train','test'])
        # tf.summary.scalar('error_th_hs',self.e_th_hs)

        tf.summary.image('input_img',self.img_placeholder,1)
        # tf.summary.image('input_img',self.img_placeholder,1,collections=["train"])
        # self.merged_summary = tf.summary.merge_all(scope=['train'])
        self.merged_summary = tf.summary.merge_all('train')
        self.merged_summary_test = tf.summary.merge_all('test')
        # merged_summary = tf.summary.merge([train_acc_scaler])
        self.nn_saver = tf.train.Saver()


    def train_NN(self, training_iterations = 100000, sample_batch_size=32,model_name_path='test', user_spec_training_iteration=None):
        #start session:
        # Tensorflow session
        # self.session = tf.Session()
        # Run intializer for the variables


        self.session.run(tf.global_variables_initializer())

        for i in range(training_iterations):
            training_input_list, training_label_list = self.obtain_batch_training_data(batch_size=sample_batch_size)

            test_input_list, test_label_list = self.obtain_batch_training_data(batch_size=sample_batch_size, sample_training_set_bool=False)

            if i%5 == 0:
                s, error_out,_ = self.session.run([self.merged_summary,self.error_total, self.train_op], feed_dict={self.img_placeholder: training_input_list["map_img"],
                                                                                        self.head_step_placeholder: training_input_list["head_step"],
                                                                                        self.twist_placeholder: training_input_list["twist"],
                                                                                        self.wrench_placeholder: training_input_list["wrench"],
                                                                                        self.label: training_label_list["pose"]})
                
                # pdb.set_trace()
                self.writer.add_summary(s,i)
                s_test, error_out_test = self.session.run([self.merged_summary_test,self.error_total], feed_dict={self.img_placeholder: test_input_list["map_img"],
                                                                                        self.head_step_placeholder: test_input_list["head_step"],
                                                                                        self.twist_placeholder: test_input_list["twist"],
                                                                                        self.wrench_placeholder: test_input_list["wrench"],
                                                                                        self.label: test_label_list["pose"]})
                self.writer_test.add_summary(s_test,i)

            else:

                error_out, _ = self.session.run([self.error_total, self.train_op], feed_dict={self.img_placeholder: training_input_list["map_img"],
                                                                                            self.head_step_placeholder: training_input_list["head_step"],
                                                                                            self.twist_placeholder: training_input_list["twist"],
                                                                                            self.wrench_placeholder: training_input_list["wrench"],
                                                                                            self.label: training_label_list["pose"]})





            if i%100 == 0:
                print(error_out)
                print("percent done:",(i/training_iterations)*100.0)

        #Save the model
        model_path = self.bags_dir_path+"trained_models/"+model_name_path
        model_checkpnt_path = model_path+"/checkpoint/model.ckpt"

        self.make_path(model_path)
        self.make_path(model_checkpnt_path)

        # self.nn_saver = tf.train.Saver()
        str_path = self.nn_saver.save(self.session,model_checkpnt_path)
        print("Model saved in path: %s" % str_path)

        model_training_idx_path = model_path  + "/train_test_idx/model_trained_idx.p"
        prepath = model_path  + "/train_test_idx/"
        self.make_path(prepath)

        training_idxs = {"train_set":self.training_set_idx, "holdout_set":self.hold_out_set_idx}
        with open(model_training_idx_path, 'wb') as fp:
            pickle.dump(training_idxs,fp)

        #Now save the actual model
        self.save_model(model_path=model_path)

    def make_path(self,path=None):
        try:
            os.mkdir(path) #make the directory
        except:
            pass


    def save_model(self,model_path=""):
        export_dir = model_path + "/full_model"
        loop_bool = True
        counter = 1
        while loop_bool:
            try:
                tf.saved_model.simple_save(self.session,export_dir,inputs={"map_img": self.img_placeholder,
                                                                            "head_step": self.head_step_placeholder,
                                                                            "twist": self.twist_placeholder,
                                                                            "wrench": self.wrench_placeholder}, 
                                                                            outputs={"pose":self.output}) 
                loop_bool = False
            except:
                export_dir += "_temp"
            if counter > 3: 
                loop_bool = False
                print("unable to save file")


    def restore_nn_model(self, model_name_path=None, training_idx_name=None, save_tensorboard=False):
        model_path = self.bags_dir_path+"trained_models/"+model_name_path
        model_checkpnt_path = self.bags_dir_path+"trained_models/"+model_name_path+"/model.ckpt"
        self.nn_saver = tf.train.Saver()
        # with tf.Session() as self.session:
        self.nn_saver.restore(self.session, model_checkpnt_path)
        rospy.sleep(2.0)
        if save_tensorboard: self.writer.add_graph(self.session.graph) 

        try:
            model_training_idx_path = self.bags_dir_path+"trained_models/" + "model_trained_idx" + model_name_path + ".p"
            with open(model_training_idx_path, 'rb') as fp:
                training_idxs = pickle.load(fp)
                self.training_set_idx = training_idxs["train_set"]
                self.hold_out_set_idx = training_idxs["holdout_set"]
            
        except:
          print("could not find training labels idx pickle")

        self.save_model(model_path=model_path)


    def test_NN_prediction(self, num_training_pts=40):
        #this tests on the specified hold out set
        training_input_list, training_label_list = self.obtain_batch_training_data(batch_size=num_training_pts, sample_training_set_bool=False )
        self.output_pred = self.session.run(self.output, feed_dict={self.img_placeholder: training_input_list["map_img"],
                                                                                  self.head_step_placeholder: training_input_list["head_step"],
                                                                                  self.twist_placeholder: training_input_list["twist"],
                                                                                  self.wrench_placeholder: training_input_list["wrench"]})
        # pdb.set_trace()
        self.pose_difference(output=self.output_pred, label=training_label_list["pose"])

    def angle_diff(self,ang1=0.0,ang2=1.0):
        R1 = np.matrix([[np.cos(ang1),np.sin(ang1)],
                        [-np.sin(ang1),np.cos(ang1)]])

        R2 = np.matrix([[np.cos(ang2),np.sin(ang2)],
                        [-np.sin(ang2),np.cos(ang2)]])
        Rdiff = R1*R2.T
        ang_diff = np.arctan2(Rdiff[0,1],Rdiff[0,0])
        return ang_diff


    def pose_difference(self,output=None, label=None):
        #This gets the pose error between output and expected label for poses
        # pdb.set_trace()
        xy_pose_diff = np.abs(np.sum(output[:,:2] - label[:,:2],axis=1))
        ang_diff = [ np.abs(self.angle_diff(ang1=label[i,2], ang2=output[i,2])) for i in range(output.shape[0])]
        #plot the pose difference: 
        '''
        fig1 = plt.figure()
        ax1 = fig1.gca()
        fig2 = plt.figure()
        ax2 = fig2.gca()
        #pdb.set_trace() #to debug
        idx_range = range(len(ang_diff))
        ax1.scatter(idx_range,xy_pose_diff, label='xy_diff')
        ax2.scatter(idx_range,ang_diff, label='ang_diff')
        ax1.legend()
        ax2.legend()
        '''
        
        xy_mean = np.mean(np.abs(xy_pose_diff))
        xy_stdev = np.std(np.abs(xy_pose_diff))
        print("xy mean",xy_mean)
        print("xy stdev",xy_stdev)
        th_mean = np.mean(np.abs(ang_diff))
        th_stdev = np.std(np.abs(ang_diff))
        print("th mean",th_mean)
        print("th stdev",th_stdev)
        plt.show()

    def load_bag(self, bag_name=None):
        bag = rosbag.Bag(bag_name)
        for topic, msg, t in bag.read_messages(topics=['/nn_training_data']):
            self.training_data_msgs.append(msg)
        bag.close() 

    def find_bag_name(self, time_delay="0.5"):
        f = os.listdir(self.bags_dir_path)
        if type(time_delay) == float: time_delay = str(time_delay)
        self.bag_name = [n for n in f if time_delay in n]
        if len(self.bag_name)  == 0: 
            rospy.logwarn("Bag not found")
        elif len(self.bag_name) > 1:
            rospy.logwarn("Multiple bags found")
        else:
            self.bag_name = self.bag_name[0]

    def convert_imgs(self,img=None):
        #img is 107x107 need to go from rgb to grey
        if "rgb8" in img.encoding:
            try:
                cv_image = self.bridge.imgmsg_to_cv2(img, "rgb8") #http://docs.ros.org/melodic/api/cv_bridge/html/python/
            except CvBridgeError as e:
                print(e)
            img_gray = cv2.cvtColor(cv_image, cv2.COLOR_RGB2GRAY) #type cv::mat
        elif "mono8" in img.encoding:
            try:
                img_gray = self.bridge.imgmsg_to_cv2(img, "mono8") #http://docs.ros.org/melodic/api/cv_bridge/html/python/
            except CvBridgeError as e:
                print(e)
        #now make this a numpy array
        # cv2.imshow('test image', img_gray)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        img_gray_mat = np.asarray(img_gray)
        #expand on last dimension (channel)
        img_gray_mat = np.expand_dims(img_gray_mat,axis=3)
        return img_gray_mat

    def prepare_data_to_train(self,time_delay=None,hold_out_percentage=0.2):
        """
        All the data must be available 
        """
        #obtain bagname
        self.find_bag_name(time_delay=time_delay)
        #load bag if it is found
        if not self.bag_name:
            rospy.logwarn("nothing in bagname")
        else:
            bag_name = self.bags_dir_path + self.bag_name
            self.load_bag(bag_name=bag_name)
        #Now that data is loaded (in list), make a corresponding list of the local map images
        for idx in range(len(self.training_data_msgs)):
            Img = self.convert_imgs(img=self.training_data_msgs[idx].intent_quantities.local_gridmap_img)
            self.local_map_image_list.append(Img)
        '''
        Now setup the data for training: 
        1. Using indicies make sets: 
          a) Hold out set (never touched evaulation data (~20% of all data))  [randomly sampled]
          b) Training set: (batches are randomly sampled from this set) (remaining ~80% of data), [sub-batches are randomly sampled]
        '''
        N_data = len(self.training_data_msgs)
        total_data_idx = range(N_data)
        random_order_data_idx = np.random.choice(N_data,N_data,replace=False)
        self.hold_out_set_idx = random_order_data_idx[:int(N_data*hold_out_percentage)] #takes first percent of randomly ordered data as hold out set
        self.training_set_idx = random_order_data_idx[int(N_data*hold_out_percentage):]

    def obtain_batch_training_data(self, batch_size=10, sample_training_set_bool=True):
        """
        This script takes batch size and randomly samples 
        without replacement from the training set and returns 
        data correctly shaped in a dictionary for training algorithm
        """
        if sample_training_set_bool:
            N_training = len(self.training_set_idx)
            sample_batch_sub_idx = np.random.choice(N_training, batch_size,replace=False)
            sample_batch_idx = [self.training_set_idx[s] for s in sample_batch_sub_idx] #indicies obtained
        else:
            #sample from hold out set
            N_test = len(self.hold_out_set_idx)
            sample_batch_sub_idx = np.random.choice(N_test, batch_size,replace=False)
            sample_batch_idx = [self.hold_out_set_idx[s] for s in sample_batch_sub_idx] #indicies obtained

        #Now given batch, iterate through and construct training data
        training_img_input_list = []
        training_headstep_input_list = []
        training_twist_input_list = []
        training_wrench_input_list = []
        training_label_list = []
        for train_idx in sample_batch_idx:
            Img = self.local_map_image_list[train_idx]
            training_msg = self.training_data_msgs[train_idx]
            NN_input_dict, NN_label_dict = self.NN_data_dict(Img=Img,NN_input=training_msg,training_bool=True)
            #populate lists
            training_img_input_list.append(NN_input_dict["map_img"])
            training_headstep_input_list.append(NN_input_dict["head_step"])
            training_twist_input_list.append(NN_input_dict["twist"])
            training_wrench_input_list.append(NN_input_dict["wrench"])
            training_label_list.append(NN_label_dict["pose"])
        #now convert these to arrays and pass final dict
        training_img_input_list = np.array(training_img_input_list)
        training_headstep_input_list = np.array(training_headstep_input_list)
        training_twist_input_list = np.array(training_twist_input_list)
        training_wrench_input_list = np.array(training_wrench_input_list)
        training_label_list = np.array(training_label_list)
        #populate dict
        training_input_dict = {"map_img":training_img_input_list, "head_step":training_headstep_input_list, "twist":training_twist_input_list, "wrench":training_wrench_input_list}
        training_label_dict = {"pose":training_label_list}
        # pdb.set_trace()
        return training_input_dict,training_label_dict

    def expand_dims(self,cur_array=None):
        cur_array = np.expand_dims(cur_array,axis=1)
        return cur_array

    def quat_to_z_angle(self,q=None):
        return np.arctan2( 2*(q.w*q.z+q.x*q.y),(1-2*(q.y**2 + q.z**2)) )

    def NN_data_dict(self,Img=None,NN_input=None,training_bool=True):
        '''
        This function takes the current image and NN_output and constructs the input to the NN along with the label in the case training_bool is true
        '''
        # pdb.set_trace()
        head_step = np.array([NN_input.intent_quantities.local_head_vect.vector.x, 
                              NN_input.intent_quantities.local_head_vect.vector.y, 
                              NN_input.intent_quantities.local_step_vect.vector.x,
                              NN_input.intent_quantities.local_step_vect.vector.y])
        # head_step = self.expand_dims(head_step)
        twist = np.array([NN_input.intent_quantities.local_twist.twist.linear.x,
                          NN_input.intent_quantities.local_twist.twist.linear.y,
                          NN_input.intent_quantities.local_twist.twist.angular.z]) #DOUBLE CHECK THIS IS ABOUT Z-axis
        # twist = self.expand_dims(twist)
        wrench = np.array([NN_input.intent_quantities.local_wrench.wrench.force.x,
                           NN_input.intent_quantities.local_wrench.wrench.force.y,
                           NN_input.intent_quantities.local_wrench.wrench.torque.z])#DOUBLE CHECK THIS IS ABOUT Z-axis
        # wrench = self.expand_dims(wrench)
        NN_input_dict = {"map_img":Img, "head_step":head_step, "twist":twist, "wrench":wrench}
        if training_bool:
            future_pose = np.array([NN_input.future_pose_local_frame.pose.position.x,
                                  NN_input.future_pose_local_frame.pose.position.y,
                                  self.quat_to_z_angle(NN_input.future_pose_local_frame.pose.orientation)]) #radians
            # future_pose = self.expand_dims(future_pose)
            future_time = NN_input.time_delay
            NN_label_dict = {"pose":future_pose,"time_horizon":future_time}
        else:
            NN_label_dict = {"pose":None,"time_horizon":None}
        #val,_= funct() #to ignore second value
        return NN_input_dict, NN_label_dict




def run_model(args):
    (file_label,time_delays) = args
    # print("file_label",file_label)
    print("training for ",file_label)
    file_writer_path= "/tmp/nn_pose_prediction/time_horizon_no_hs_fb_with_dropout_fc0p2/"+file_label+"/"
    cls_obj = NNPoseTraining(learning_rate = 1e-3,file_writer_path=file_writer_path)

    #set time delay of bag file to train
    t_delay = rospy.get_param("training_time_delay",time_delays)
    #Initalize layers
    cls_obj.init_layers(output_size=3)
    #Define the error and optimizer functions
    cls_obj.define_error() #Defines error and optimizer
    #Prepare Training Data
    print("preparing data to train")
    cls_obj.prepare_data_to_train(time_delay=t_delay, hold_out_percentage=0.15)
    #'''
    #train the NN (start session and train)
    # nn_model_load = "training_1p5sec_1_9_19_t9_26_"
    nn_model_load = "training_"+file_label+"sec_1_13_19_no_hs_fb_with_dropout_fc0p2"
    cls_obj.setup_saver_and_visual()
    cls_obj.train_NN(training_iterations=int(1e5), sample_batch_size=20, model_name_path=nn_model_load) #100000//100  1e5 is not effective
    #to save after the fact
    # cls_obj.restore_nn_model(model_name_path=nn_model_load)

class run_multiple_modes(object):
    """docstring for run_multiple_modes"""

    def model_manager(self):


        train_dict = {"file_labels":["2p0","1p5","1p0","0p5"], "time_delays":[2.0,1.5,1.0,0.5]}
        for idx in range(4):        
            file_label = train_dict["file_labels"][idx]
            time_delays = train_dict["time_delays"][idx]
            with closing(Pool(1)) as p:
                p.map(run_model, [(file_label,time_delays)])
                p.terminate()




def main():
    rospy.init_node("nn_train_node")
    cls_obj = run_multiple_modes()
    cls_obj.model_manager()


if __name__ == '__main__':
  main()

#tensorboard --logdir /path/
