#!/usr/bin/env python

import numpy as np
import rospy, rosbag, rospkg
from intelligent_coop_carry_msgs.srv import * 
# from intelligent_coop_carry_msgs.msg import IntentQuantitiesLocal
from geometry_msgs.msg import PoseStamped
import pdb


class NN_prediction_eval(object):
  def __init__(self,time_horizon=0.5):
    rospack = rospkg.RosPack()
    self.bag_path = rospack.get_path("gp_human_pred") + "/bagfiles/nn_training_data/"
    self.time_horizon = time_horizon
    self.msg_list = []
    self.nn_service = rospy.ServiceProxy('nn_query_service',nn_intention_query)
    self.nn_errors = []

  def load_bag(self,bagname=''):
    full_bagname = self.bag_path + bagname
    bag = rosbag.Bag(full_bagname)
    for topic, msg, t in bag.read_messages(topics=['/nn_training_data']):
      self.msg_list.append(msg)
    bag.close()
    

  def find_summed_total_error(self):
    e_xy_nn = []
    e_th_nn = []
    e_xy_vel = []
    e_th_vel = []
    print "finding nn error"
    for msg in self.msg_list:
      self.time_horizon = msg.time_delay
      #get the expected pose from vel
      true_pose = msg.future_pose_local_frame
      #get the pose from NN & difference from true
      req = nn_intention_queryRequest()
      req.human_intent = msg.intent_quantities
      resp = self.nn_service(req)
      geom_diff_nn = self.geometry_difference(first_pose=resp.future_pose_local, second_pose=true_pose)
      e_xy_nn.append(geom_diff_nn["xy"])
      e_th_nn.append(geom_diff_nn["th"])
      #Get the pose from vel& difference from true
      vel_predicted_pose = self.vel_predicted_pose(human_intent=req.human_intent,time_horizon=self.time_horizon)
      #get vel diff
      geom_diff_vel = self.geometry_difference(first_pose=vel_predicted_pose, second_pose=true_pose)
      e_xy_vel.append(geom_diff_vel["xy"])
      e_th_vel.append(geom_diff_vel["th"])
    #Get averages and variances
    e_xy_nn_mean = np.mean(e_xy_nn)
    e_xy_nn_var = np.std(e_xy_nn)
    e_th_nn_mean = np.mean(e_th_nn)
    e_th_nn_var = np.std(e_th_nn)
    #now for vel
    e_xy_vel_mean = np.mean(e_xy_vel)
    e_xy_vel_var = np.std(e_xy_vel)
    e_th_vel_mean = np.mean(e_th_vel)
    e_th_vel_var = np.std(e_th_vel)
    # pdb.set_trace()
    print "\n\ntime horizon",self.time_horizon
    print "\ne_xy_nn_mean",e_xy_nn_mean
    print "\ne_xy_nn_var",e_xy_nn_var
    print "\ne_th_nn_mean",e_th_nn_mean
    print "\ne_th_nn_var",e_th_nn_var
    #Now for velocity
    print "\ne_xy_vel_mean",e_xy_vel_mean
    print "\ne_xy_vel_var",e_xy_vel_var
    print "\ne_th_vel_mean",e_th_vel_mean
    print "\ne_th_vel_var",e_th_vel_var

  def vel_predicted_pose(self,human_intent=None,time_horizon=None):
    #this is to predit the human intent with velocity: p(t+t_h) = p(t) + t_h* dp_dt(t)  Since this is in the body fixed frame, simply multiply the twist by t_h
    sys_twist = human_intent.local_twist
    # pdb.set_trace()
    future_est_pose= PoseStamped()
    #position:
    future_est_pose.pose.position.x = time_horizon*sys_twist.twist.linear.x
    future_est_pose.pose.position.y = time_horizon*sys_twist.twist.linear.y
    future_est_pose.pose.position.z = time_horizon*sys_twist.twist.linear.z
    #now twist:
    twist_vect = np.matrix([sys_twist.twist.angular.x,sys_twist.twist.angular.y,sys_twist.twist.angular.z])
    twist_rate = np.linalg.norm(twist_vect)
    twist_vect_normed = np.divide(twist_vect,twist_rate) 
    #integrate rotation about this same axis
    total_rotation = twist_rate*time_horizon #gets rotation about the axis
    #now turn this all into a quaternion
    future_est_pose.pose.orientation.x = twist_vect_normed.item(0)*np.sin(total_rotation/2.)
    future_est_pose.pose.orientation.y = twist_vect_normed.item(1)*np.sin(total_rotation/2.)
    future_est_pose.pose.orientation.z = twist_vect_normed.item(2)*np.sin(total_rotation/2.)
    future_est_pose.pose.orientation.w = np.cos(total_rotation/2.)
    #this is future pose in body fixed frame of current poses
    return future_est_pose





  def geometry_difference(self,first_pose=None,second_pose=None):
    #xy error
    # pdb.set_trace()
    dx = first_pose.pose.position.x - second_pose.pose.position.x
    dy = first_pose.pose.position.y - second_pose.pose.position.y
    e_xy = np.sqrt(dx**2 + dy**2)
    #angle error
    first_ang = self.quat_to_z_angle(first_pose.pose.orientation)
    second_ang = self.quat_to_z_angle(second_pose.pose.orientation)
    err_ang_rad = self.angle_diff(ang1=first_ang, ang2=second_ang) #rad
    err_ang_deg = (180./np.pi)*err_ang_rad
    error = {"xy":e_xy, "th":np.abs(err_ang_deg)}
    return error

  def angle_diff(self,ang1=0.0,ang2=1.0):
    R1 = np.matrix([[np.cos(ang1),np.sin(ang1)],
                    [-np.sin(ang1),np.cos(ang1)]])

    R2 = np.matrix([[np.cos(ang2),np.sin(ang2)],
                    [-np.sin(ang2),np.cos(ang2)]])
    Rdiff = R1*R2.T
    ang_diff = np.arctan2(Rdiff[0,1],Rdiff[0,0])
    return ang_diff

  def quat_to_z_angle(self,q=None):
    return np.arctan2( 2*(q.w*q.z+q.x*q.y),(1-2*(q.y**2 + q.z**2)) )




  def find_summed_total_vel_error(self):
    print "finding vel error"
    pass





def main():
  rospy.init_node("nn_eval_node")
  # bagname = "nn_trained_data_time_delay_0.500000_sec.bag" #1.0,1.5,2.0
  # bagname = "nn_trained_data_time_delay_1.000000_sec.bag" #1.0,1.5,2.0
  # bagname = "nn_trained_data_time_delay_1.500000_sec.bag" #1.0,1.5,2.0
  bagname = "nn_trained_data_time_delay_2.000000_sec.bag" #1.0,1.5,2.0
  time_horizon = 1.0
  rospy.wait_for_service('nn_query_service')
  cls_obj = NN_prediction_eval(time_horizon=time_horizon)
  #1. load desired rosbag
  cls_obj.load_bag(bagname=bagname)
  #2. for every message in bag, find the NN error for predicted pose and vel error for predicted pose
  cls_obj.find_summed_total_error()



if __name__ == '__main__':
  main()


'''
Data
###### 0.5 ##########
time horizon 0.5
e_xy_nn_mean 0.11220998550013296
e_xy_nn_var 0.07953320436361709

e_th_nn_mean 8.151296904815386
e_th_nn_var 7.576767874518492

e_xy_vel_mean 0.12873540104836975
e_xy_vel_var 0.09617454803656098

e_th_vel_mean 5.689499106230051
e_th_vel_var 5.332598250320865


###### 1.0 ##########

time horizon 1.0
e_xy_nn_mean 0.1657158387923552
e_xy_nn_var 0.12164643010277645

e_th_nn_mean 12.639244242657167
e_th_nn_var 11.70294140180956

e_xy_vel_mean 0.19498320157169813
e_xy_vel_var 0.16545702677713853

e_th_vel_mean 10.65173042413501
e_th_vel_var 10.293679259980586


###### 1.5 ##########
time horizon 1.5

e_xy_nn_mean 0.24954989272159708
e_xy_nn_var 0.17533891537944235

e_th_nn_mean 17.881050652016782
e_th_nn_var 17.472614089429552

e_xy_vel_mean 0.32430064952188936
e_xy_vel_var 0.27213370409500093

e_th_vel_mean 17.373540314706162
e_th_vel_var 17.470655828931136


###### 2.0 ##########
time horizon 2.0

e_xy_nn_mean 0.3554935922735669
e_xy_nn_var 0.23760593956325238

e_th_nn_mean 23.963289709915653
e_th_nn_var 22.436522298051226

e_xy_vel_mean 0.5032397817932196
e_xy_vel_var 0.4048383581106524

e_th_vel_mean 26.984625105569314
e_th_vel_var 27.094153319232987


'''