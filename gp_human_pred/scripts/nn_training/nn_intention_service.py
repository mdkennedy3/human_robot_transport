#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import argparse
import sys
import rospy
import numpy as np
import os
import rospkg
from intelligent_coop_carry_msgs.msg import IntentQuantitiesLocal
from gp_human_pred.nn_library_fncts import nn_library_functions
from intelligent_coop_carry_msgs.srv import * #nn_intention_query


class NNIntentionService(object):
  def __init__(self):
    self.nn_lib_funcs = nn_library_functions.NNLibraryFunctions()

  def load_nn(self,nn_model_name=None):
    self.nn_lib_funcs.load_nn_model(model_name_path=nn_model_name) 


  def query_net_service(self,req):
    se2_pose = self.nn_lib_funcs.NN_prediction(NN_input = req.human_intent)
    resp = nn_intention_queryResponse()
    resp.future_pose_local = se2_pose
    return resp
 

def main():
  rospy.init_node("nn_query_service")
  cls_obj = NNIntentionService()
  model_name =  rospy.get_param("~nn_model_name", "training_1p5sec_1_13_19_no_hs_fb_with_dropout_fc0p2")
  rospy.loginfo(model_name)
  cls_obj.load_nn(nn_model_name=model_name)
  print("running service")
  s = rospy.Service('nn_query_service',nn_intention_query, cls_obj.query_net_service)
  rospy.spin()

if __name__ == '__main__':
  main()


