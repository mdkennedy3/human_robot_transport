#!/usr/bin/env python
import rospy, numpy as np
from intelligent_coop_carry_msgs.msg import IntentQuantitiesLocal


class callbackclass(object):
  def __init__(self):
    self.init_time = None
  def cb(self,msg): 
    if not self.init_time:
      self.init_time = msg.system_pose.header.stamp.to_sec()
    print msg.system_pose.header.stamp.to_sec() - self.init_time

def main():
  rospy.init_node('test2')
  cc  = callbackclass()
  s = rospy.Subscriber("/local_intent_quanities",IntentQuantitiesLocal,cc.cb,queue_size=1)
  rospy.spin()


if __name__== '__main__':
  main()

