#!/usr/bin/env python2
import numpy as np, math
import rospy, time
import sys, getopt
import rospy
from moveit_msgs.msg import MoveItErrorCodes
from moveit_python import MoveGroupInterface, PlanningSceneInterface
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion

import actionlib
from control_msgs.msg import * #PointHeadAction


def main():
    rospy.init_node('go_to_start_node_head')

    client = actionlib.SimpleActionClient('head_controller/point_head', PointHeadAction)

    print "Waiting for server"
    client.wait_for_server()

    g = PointHeadGoal()
    g.target.header.frame_id = "torso_lift_link"
    g.target.point.x = 1.0
    g.target.point.y = 0.0
    g.target.point.z = 0.5

    print "sending goal"
    client.send_goal(g)

    client.wait_for_result(rospy.Duration.from_sec(3.0))
    
    rospy.spin()

    # PointHeadFeedback.msg
    # PointHeadGoal.msg
    # PointHeadResult.msg



if __name__ == '__main__':
    main()


