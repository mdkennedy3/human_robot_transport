#!/usr/bin/env python
import rospy
import numpy as np
# import rosbag
import rospkg
import tf
from tf import TransformerROS
import tf2_ros
import scipy
from scipy import linalg
from geometry_msgs.msg import TransformStamped, Transform
import tf2_msgs.msg
import yaml

class Map_Transform_Class(object):
  """docstring for Map_Transform_Class"""
  def __init__(self, map_name="gen_map"):
    self.tfBuffer = tf2_ros.Buffer()
    self.listener = tf2_ros.TransformListener
    self.tfros = TransformerROS()
    self.broadcast_static_transform = tf2_ros.static_transform_broadcaster.StaticTransformBroadcaster()



    self.rospack = rospkg.RosPack()
    self.tf_head_to_fetch_hat = TransformStamped() # <node pkg="tf" type="static_transform_publisher" name="fetch_hat_fetch_broadcaster" args="0.015 -0.007 0.06 0 0 0 1 head_pan_link fetch_hat 100" />
    self.tf_head_to_fetch_hat.transform.translation.x = -0.015
    self.tf_head_to_fetch_hat.transform.translation.y = 0.007
    self.tf_head_to_fetch_hat.transform.translation.z = -0.06
    self.tf_head_to_fetch_hat.transform.rotation.x = 0.0
    self.tf_head_to_fetch_hat.transform.rotation.y = 0.0
    self.tf_head_to_fetch_hat.transform.rotation.z = 0.0
    self.tf_head_to_fetch_hat.transform.rotation.w = 1.0
    #relavent frames:
    self.world_frame = "mocap"
    self.fetch_vicon_frame = "fetch_hat"
    self.fetch_head_frame = "head_pan_link"
    self.map_frame = "map"
    self.TF_map_to_world = None
    #map name for saving final transform
    self.map_name = map_name

    self.tf_rate = rospy.Rate(10) #hz

  def obtain_transform(self,target_frame='',source_frame=''):
    success_bool = True
    trans=None
    trans = self.tfBuffer.lookup_transform(target_frame, source_frame,rospy.Time())
    try:
      trans = self.tfBuffer.lookup_transform(target_frame, source_frame,rospy.Time())
    except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
      success_bool = False
      # self.tf_rate.sleep()
      if tf2_ros.LookupException:
        print "lookup exception"

    return trans,success_bool

  def tf_to_mat(self,tf_obj):
    translation = (tf_obj.transform.translation.x, tf_obj.transform.translation.y, tf_obj.transform.translation.z)
    rotation = (tf_obj.transform.rotation.x, tf_obj.transform.rotation.y, tf_obj.transform.rotation.z, tf_obj.transform.rotation.w)
    TF = np.matrix(self.tfros.fromTranslationRotation(translation,rotation))
    return TF


  def tf_callback(self):
    #this is called whenever new tf is available, find the latest transform
    #1. obtain transforms
    #1a. obtain the transform from map to head_pan
    tf_map_to_head, success_map_head = self.obtain_transform(target_frame=self.fetch_head_frame, source_frame=self.map_frame)
    #1b. obtain tf from fetch_hat to world 
    tf_fetch_hat_to_world, success_hat_world = self.obtain_transform(target_frame=self.world_frame, source_frame=self.fetch_vicon_frame)
    print "obtained map head", success_map_head
    print "obtained hat world", success_hat_world
    if success_map_head and success_hat_world:
      #2. Find the TF_{world <- map} = TF_{W<-hat} TF_{hat<-head} TF_{head<-map}
      TF_hat_to_world = self.tf_to_mat(tf_fetch_hat_to_world)
      TF_head_to_hat = self.tf_to_mat(self.tf_head_to_fetch_hat)
      TF_map_to_head = self.tf_to_mat(tf_map_to_head)
      self.TF_map_to_world = TF_hat_to_world*TF_head_to_hat*TF_map_to_head
      print "obtained tf: ", self.TF_map_to_world
    else:
      #either one of the neccessary transforms wasn't obtained, so skip this round
      pass

  def save_transform(self):
    pack_path = self.rospack.get_path('gp_human_pred')
    file_path = pack_path+'/maps/'+self.map_name+"_adjusted_tf.yaml"
    #Given the name of the current map, save with tf tag to be loaded
    if type(self.TF_map_to_world) != type(None):
      #TF tree exists, save the last
      with open(file_path,'w') as outfile:
        yaml.dump(self.TF_map_to_world, outfile,default_flow_style=False)


  def load_transform(self):
    pack_path = self.rospack.get_path('gp_human_pred')
    file_path = pack_path+'/maps/'+self.map_name+"_adjusted_tf.yaml"
    with open(file_path,'r') as stream: 
      self.TF_map_to_world = yaml.load(stream)


  def convert_TF_to_pos_quat(self,TF=None):
    #This function takes a TF function and returns the pose,quat
    #Quat first
    R = TF[:3,:3]
    skew_mat = scipy.linalg.logm(R)
    v_ang = np.matrix([skew_mat[2,1],skew_mat[0,2],skew_mat[0,1]]).T
    ang = np.linalg.norm(v_ang)
    if ang == 0:
      qx = 0.0
      qy = 0.0
      qz = 0.0
      qw = 1.0
    else:
      v = np.divide(v_ang,ang)
      qx = v.item(0)*np.sin(ang/2)
      qy = v.item(1)*np.sin(ang/2)
      qz = v.item(2)*np.sin(ang/2)
      qw = np.cos(ang/2)
    #Position
    d = TF[:3,3]
    trans_dict = Transform()
    trans_dict.translation.x = d.item(0)
    trans_dict.translation.y = d.item(1)
    trans_dict.translation.z = d.item(2)
    trans_dict.rotation.x = qx
    trans_dict.rotation.y = qy
    trans_dict.rotation.z = qz
    trans_dict.rotation.w = qw

    return trans_dict


  def gen_tf_by_hand(self):

    #set the map name:
    self.map_name = 'double_hall_diamond_island'

    #rosrun tf source target
    '''
    At time 1532114426.871
    - Translation: [2.955, 0.566, 1.043]
    - Rotation: in Quaternion [0.000, -0.001, 0.920, -0.393]
                in RPY (radian) [-0.001, -0.000, -2.334]
                in RPY (degree) [-0.069, -0.001, -133.737]
    '''
    #tf from map to head
    tf_map_to_head = TransformStamped()
    tf_map_to_head.transform.translation.x = 2.955
    tf_map_to_head.transform.translation.y = 0.566
    tf_map_to_head.transform.translation.z =  1.043
    tf_map_to_head.transform.rotation.x = 0.0
    tf_map_to_head.transform.rotation.y = -0.001
    tf_map_to_head.transform.rotation.z = 0.920
    tf_map_to_head.transform.rotation.w =  -0.393

    '''
    At time 1532114426.013
    - Translation: [-0.784, -0.966, -1.173]
    - Rotation: in Quaternion [-0.002, -0.009, 0.516, 0.856]
                in RPY (radian) [-0.012, -0.013, 1.085]
                in RPY (degree) [-0.710, -0.749, 62.174]
    '''

    #tf from fetch_hat to world
    tf_fetch_hat_to_world = TransformStamped()
    tf_fetch_hat_to_world.transform.translation.x = -0.784 
    tf_fetch_hat_to_world.transform.translation.y = -0.966 
    tf_fetch_hat_to_world.transform.translation.z = -1.173
    tf_fetch_hat_to_world.transform.rotation.x = -0.002
    tf_fetch_hat_to_world.transform.rotation.y =  -0.009
    tf_fetch_hat_to_world.transform.rotation.z = 0.516
    tf_fetch_hat_to_world.transform.rotation.w = 0.856

    TF_hat_to_world = self.tf_to_mat(tf_fetch_hat_to_world)
    TF_head_to_hat = self.tf_to_mat(self.tf_head_to_fetch_hat)
    TF_map_to_head = self.tf_to_mat(tf_map_to_head)
    TF_map_to_world_mat = TF_hat_to_world*np.linalg.inv(TF_head_to_hat)*TF_map_to_head

    # TF_map_to_world_mat = np.linalg.inv(TF_map_to_world_mat)
    #convert to better form 
    self.TF_map_to_world = self.convert_TF_to_pos_quat(TF_map_to_world_mat)



    #save the transform
    self.save_transform()


  def publish_static_transform(self, tf_obj):
    #given the file name, load the file and publish the static transform
    map_to_world_transform = TransformStamped()
    map_to_world_transform.header.stamp = rospy.Time.now()
    map_to_world_transform.header.frame_id = "mocap"
    map_to_world_transform.header.child_frame_id = "map"
    map_to_world_transform.transform.translation.x = tf_obj['position'][0]
    map_to_world_transform.transform.translation.y = tf_obj['position'][1]
    map_to_world_transform.transform.translation.z = tf_obj['position'][2]
    map_to_world_transform.transform.rotation.x = tf_obj['quaternion'][0]
    map_to_world_transform.transform.rotation.y = tf_obj['quaternion'][1]
    map_to_world_transform.transform.rotation.z = tf_obj['quaternion'][2]
    map_to_world_transform.transform.rotation.w = tf_obj['quaternion'][3]
    self.broadcast_static_transform.sendTransform(map_to_world_transform)


  def Static_transform_function(self):
    #load the data
    self.load_transform()
    #publish the data
    self.publish_static_transform(self.TF_map_to_world)


def main():

  rospy.init_node("map_mocap_tf_node")

  file_name = rospy.get_param("map_name", "gen_map")
  cls_obj = Map_Transform_Class(map_name = file_name)



  pub_static_mode = rospy.get_param("pub_static_mode","false")
  if pub_static_mode in ["true","True"]:
    #in this case pub the static transform
    cls_obj.Static_transform_function()
    print "pub static"
  else:
    #in this case determine the static transform
    cls_obj.gen_tf_by_hand()

    if False:
      #Script expects appropriate bagfile to play in background for fetch (to avoid dealing with tf time matching)
      # sub = rospy.Subscriber("/tf", tf2_msgs.msg.TFMessage, cls_obj.tf_callback)
      while not rospy.is_shutdown(): cls_obj.tf_callback()
      #setup the hook
      rospy.on_shutdown(cls_obj.save_transform)
  rospy.spin()




if __name__ == '__main__':
  main()