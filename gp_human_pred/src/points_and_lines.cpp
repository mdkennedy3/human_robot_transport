
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Point32.h>
#include <sensor_msgs/ChannelFloat32.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <Eigen/SVD>
#include "kmeans.cpp"
using namespace Eigen;
using namespace std;

int main( int argc, char** argv )
{
  ros::init(argc, argv, "points_and_lines");
  ros::NodeHandle n("~");
  ros::Publisher marker_pub = n.advertise<visualization_msgs::MarkerArray>("visualization_marker_array", 10, true);

  ros::Publisher sensor_pub = n.advertise<sensor_msgs::PointCloud>("sensor_marker", 10,true);

  ros::Publisher sensor_pub2 = n.advertise<sensor_msgs::PointCloud>("sensor_marker2", 10,true);

  ros::Publisher sensor_pub3 = n.advertise<sensor_msgs::PointCloud>("sensor_marker3", 10,true);

  ros::Publisher sensor_pub4 = n.advertise<sensor_msgs::PointCloud>("cluster2", 10, true);

  ros::Publisher sensor_pub5 = n.advertise<sensor_msgs::PointCloud>("cluster3", 10, true);

  ros::Publisher sensor_pub6 = n.advertise<sensor_msgs::PointCloud>("cluster4", 10, true);

  ros::Publisher sensor_pub7 = n.advertise<sensor_msgs::PointCloud>("cluster1", 10, true);

  ros::Publisher sensor_pub8 = n.advertise<sensor_msgs::PointCloud>("allClusts", 10, true);

  ros::Publisher training_pts_kmeans_labeled = n.advertise<sensor_msgs::PointCloud>("training_kmeans_labeled", 10, true);



  ros::Rate r(1);

    srand(time(NULL));
    double num_of_clusters;
    n.param<double>("number_of_clusters", num_of_clusters, 5);
    int K = num_of_clusters;// 5; //num of clusters
    int currClust = rand() % K; //cluster to focus on

    visualization_msgs::MarkerArray markerA;

    sensor_msgs::PointCloud pc;
    pc.header.frame_id = "/my_frame";
    pc.header.stamp = ros::Time::now();

    sensor_msgs::PointCloud pc2;
    pc2.header.frame_id = "/my_frame";
    pc2.header.stamp = ros::Time::now();

    //cluster 1
    sensor_msgs::PointCloud pc3;
    pc3.header.frame_id = "/my_frame";
    pc3.header.stamp = ros::Time::now();

    //2
    sensor_msgs::PointCloud pc4;
    pc4.header.frame_id = "/my_frame";
    pc4.header.stamp = ros::Time::now();

    //3
    sensor_msgs::PointCloud pc5;
    pc5.header.frame_id = "/my_frame";
    pc5.header.stamp = ros::Time::now();

    //4
    sensor_msgs::PointCloud pc6;
    pc6.header.frame_id = "/my_frame";
    pc6.header.stamp = ros::Time::now();

    //1
    sensor_msgs::PointCloud pc7;
    pc7.header.frame_id = "/my_frame";
    pc7.header.stamp = ros::Time::now();

    //All clusters
    sensor_msgs::PointCloud pc8;
    pc8.header.frame_id = "/my_frame";
    pc8.header.stamp = ros::Time::now();


    //These are for viewing which to which kmeans cluster each point was assigned.
    sensor_msgs::PointCloud pc_training_pts_kmeans_labeled;
    pc_training_pts_kmeans_labeled.header.frame_id = "/my_frame";
    pc_training_pts_kmeans_labeled.header.stamp = ros::Time::now();


    vector<Point> pointsV; //vector of training points

    int num = 100; // test data (num x num)
    int npoints = 20; // training data (npoints x npoints)
    int x_size = 10;
    int y_size = 10;

    //TEST
    for (uint32_t i = 0; i < num; ++i)
    {
      for (uint32_t j = 0; j < num; ++j)
      {
        float x = -10 + 20 * (num - i) / float(num);
        float y = -10 + 20 * (num - j) / float(num);
        float z = 0.4 * sin(x)+cos(y);

        geometry_msgs::Point32 pt1;
        pt1.x = x;
        pt1.y = y;
        pt1.z = z;
        pc.points.push_back(pt1);

        vector<double> vals1; //vector of x,y,z. Needed to make a point object
        vals1.push_back(x);
        vals1.push_back(y);
        vals1.push_back(z);
        Point poi1 = Point(0,vals1);
      }
    }

    //training vars
    
    //training points
    for (uint32_t i = 0; i < npoints; ++i)
    {
      for (uint32_t j = 0; j < npoints; ++j)
      {
        int x_r = rand() % 200;//5*(1 + i + j);//200;
        float x = -10 + (x_r / float(10));
        int y_r = rand() % 200;//5*(1 + i + j);//200;
        float y = -10 + (y_r / float(10));
        float z = 0.4 * sin(x)+cos(y);

        geometry_msgs::Point32 pt1;
        pt1.x = x;
        pt1.y = y;
        pt1.z = z;  
        pc3.points.push_back(pt1);

        vector<double> vals2; //vector of x,y,z. Needed to make a point object
        vals2.push_back(x);
        vals2.push_back(y);
        vals2.push_back(z); //We are comparing the 2D position of points
        Point poi2 = Point(0,vals2);
        pointsV.push_back(poi2);
      }
    }

    int total_points = npoints * npoints;
    int total_values = 3; //the 3 are x, y and z
    int max_iterations = 100; //Kmeans iterations

    KMeans kmeans(K, total_points, total_values, max_iterations);
    kmeans.run(pointsV);

    KMeans * kmeansPtr = &kmeans;
    int clustSizeTest = 0; //size of test point cluster that corresponds to current cluster
    int clustSizeTrain = kmeans.clusters[currClust].getTotalPoints(); //current cluster size, training points

    //All the covariance stuff for the current cluster

    vector< vector<Point> > testPts; //vector of TEST points
    vector <Point> row; //blank row to initialize vector
    for (int i = 0; i < K; i++) { //iterated through with each row representing a cluster
        testPts.push_back(row);
    }

    //clustSizeTest = testPts.at(currClust).size(); //Test points size

    MatrixXd mXTrain(2,clustSizeTrain);
    MatrixXd mYTrain(1,clustSizeTrain);

    //fills up X and Y vectors for training data
    for (uint32_t i = 0; i < clustSizeTrain; i++)
    {
        mXTrain(0,i) = kmeansPtr->clusters[currClust].getPoint(i).getValue(0);
        mXTrain(1,i) = kmeansPtr->clusters[currClust].getPoint(i).getValue(1);
        mYTrain(0,i) = kmeansPtr->clusters[currClust].getPoint(i).getValue(2);
    }

    float meanX[K]; //mean for X vector data, training
    float meanY[K]; //mean for Y vector data, training

    //Construct means by looping through each cluster 
    //Adds up all points and then divides them by the total number of points
    for (uint32_t i = 0; i < K; i++)
    {
        meanX[i] = 0;
        meanY[i] = 0;

        for (uint32_t j = 0; j < kmeansPtr->clusters[i].getTotalPoints(); j++)
        {
            meanX[i] += kmeansPtr->clusters[i].getPoint(j).getValue(0);
            if (j == kmeansPtr->clusters[i].getTotalPoints() - 1) {
                meanX[i] /= (kmeansPtr->clusters[i].getTotalPoints() * 1.0);
            }
            meanY[i] += kmeansPtr->clusters[i].getPoint(j).getValue(1);
            if (j == kmeansPtr->clusters[i].getTotalPoints() - 1) {
                meanY[i] /= (kmeansPtr->clusters[i].getTotalPoints() * 1.0);
            }
        }
        geometry_msgs::Point32 pt1;
        pt1.x = meanX[i];
        pt1.y = meanY[i];
        pt1.z = 10;
        pc7.points.push_back(pt1);
    }

    // Label the kmeans training points inside (pc_training_pts_kmeans_labeled)

    sensor_msgs::ChannelFloat32 pt_channel;
    pt_channel.name = "cluster_id";//std::to_string(i);
    for (uint32_t i = 0; i < K; i++)
    {
        for (uint32_t j = 0; j < kmeansPtr->clusters[i].getTotalPoints(); j++)
        {
            geometry_msgs::Point32 pt_curr;
            pt_curr.x = kmeansPtr->clusters[i].getPoint(j).getValue(0);
            pt_curr.y = kmeansPtr->clusters[i].getPoint(j).getValue(1);
            pt_curr.z = kmeansPtr->clusters[i].getPoint(j).getValue(2);
            pt_channel.values.push_back(i);
            pc_training_pts_kmeans_labeled.points.push_back(pt_curr);
            
        }
    }
    pc_training_pts_kmeans_labeled.channels.push_back(pt_channel);


    vector < vector <MatrixXd> > covList; //List of all covariances for in each cluster 
    vector <MatrixXd> blankVect;

    for (int i = 0; i < K; i++) {
        MatrixXd KernelI(2,1); //Kernel for X of current point in cluster
        MatrixXd KernelM(2,1); //Kernel for meanX
        MatrixXd covFunc(2,2); //Full Covariance Function at given point i
        covList.push_back(blankVect);
        for (int j = 0; j < kmeansPtr->clusters[i].getTotalPoints(); j++) {
            KernelM(0,0) = meanX[i];
            KernelM(1,0) = meanY[i];
            KernelI(0,0) = kmeansPtr->clusters[i].getPoint(j).getValue(0);
            KernelI(1,0) = kmeansPtr->clusters[i].getPoint(j).getValue(1);
            covFunc = (KernelI - KernelM)*(KernelI - KernelM).transpose();
            covList.at(i).push_back(covFunc);
        }
    }

    MatrixXd covClust(2,2); //variable used to construct the mean covariance for a cluster
    vector <MatrixXd> covClustM; //list of the mean covariance for each cluster

    //Add get the mean covariance for each cluster
    for (int i = 0; i < K; i++) {
        //initialize covClust to 0;
        covClust(0,0) = 0;
        covClust(0,1) = 0;
        covClust(1,0) = 0;
        covClust(1,1) = 0;
        for (int j = 0; j < covList.at(i).size(); j++) {
            covClust += covList.at(i).at(j);
        }
        covClust /= covList.at(i).size();
        covClustM.push_back(covClust);
    }

    //determines what cluster each test point falls under
    for (uint32_t i = 0; i < pc.points.size(); i++)
    {
        vector<double> vals1;
        vals1.push_back(pc.points[i].x);
        vals1.push_back(pc.points[i].y);
        vals1.push_back(pc.points[i].z);
        Point poi1 = Point(0,vals1);

        float GaussianRank = 0;
        float GaussianRankMax = 0;
        int clustChoice = 0;
        MatrixXd KernelI(2,1);
        MatrixXd KernelM(2,1);
        //loops through each cluster and tests the point against each cluster,
        //whichever gives thie highest rating (GaussianRank) is where the point falls
        for (int io = 0; io < K; io++) {
            MatrixXd covFunc(1,1);
            MatrixXd covM(kmeansPtr->clusters[io].getTotalPoints(),kmeansPtr->clusters[io].getTotalPoints());
            KernelM(0,0) = meanX[io];
            KernelM(1,0) = meanY[io];
            KernelI(0,0) = pc.points[i].x;
            KernelI(1,0) = pc.points[i].y;
            covM = covClustM.at(io);
            covFunc = (KernelI - KernelM).transpose()*(covM.inverse())*(KernelI - KernelM);
            GaussianRank = exp(-0.5*covFunc(0,0)) / sqrt((2*M_PI)*covM.determinant());
            if (GaussianRank > GaussianRankMax) {
                GaussianRankMax = GaussianRank;
                clustChoice = io;
            }
        }

        geometry_msgs::Point32 pt1;
        pt1.x = pc.points[i].x;
        pt1.y = pc.points[i].y;
        float K_float = K * 1.0;
        pt1.z = clustChoice / (K_float / 2.0) - 5;
        pc8.points.push_back(pt1);
        testPts.at(clustChoice).push_back(poi1);
    }

    clustSizeTest = testPts.at(currClust).size();
    MatrixXd mXTest(2,clustSizeTest);
    MatrixXd mYTest(1,clustSizeTest);

    for (uint32_t i = 0; i < clustSizeTest; i++)
    {
        mXTest(0,i) = testPts.at(currClust).at(i).getValue(0);
        mXTest(1,i) = testPts.at(currClust).at(i).getValue(1);
    }

    //depict the covariance in 3D for each cluster
    for (int ii = 0; ii < K; ii++) {
        visualization_msgs::Marker marker;
        marker.header.frame_id = "/my_frame";
        marker.header.stamp = ros::Time::now();
        marker.type = visualization_msgs::Marker::SPHERE;

        MatrixXd covUse(2,2); //Mean covariance of the specific cluster ii
        covUse = covClustM.at(ii);

        Eigen::JacobiSVD<Eigen::MatrixXd> covUse_svd(covUse, Eigen::ComputeThinU | Eigen::ComputeThinV);

        MatrixXd U_correct = covUse_svd.matrixU().determinant()*covUse_svd.matrixU() ;

        EigenSolver<MatrixXd> es(covUse, true);

        VectorXcd eigVals = es.eigenvalues();
        VectorXcd eigVect = es.eigenvectors().col(0);

        Vector3f cov1 = Vector3f(covUse(0,0), covUse(1,1), 4);

        Matrix3d rot;
        rot.block<2,2>(0,0) = U_correct;
        rot(2,2) = 1;

        double angle = atan2(covUse_svd.matrixU()(1,0),covUse_svd.matrixU()(0,0));
        double qw = cos(angle/2);
        double qz = sin(angle/2); 

        Quaterniond quat(rot);

        marker.id = ii;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = meanX[ii];
        marker.pose.position.y = meanY[ii];
        marker.pose.position.z = 4;
        marker.pose.orientation.x = 0;
        marker.pose.orientation.y = 0;
        marker.pose.orientation.z = qz;
        marker.pose.orientation.w = qw;

        marker.scale.x = 2*sqrt(covUse_svd.singularValues()(0));
        marker.scale.y = 2*sqrt(covUse_svd.singularValues()(1));
        marker.scale.z = 0.1;
        marker.color.a = 0.60;
        marker.color.r = 0.0;
        marker.color.g = 1.0;
        marker.color.b = 0.0;

        markerA.markers.push_back(marker);
    }



    sensor_msgs::ChannelFloat32 pt_test_channel;
    pt_test_channel.name = "GP_output";//std::to_string(i);


    geometry_msgs::Point32 ptx;

    MatrixXd Krs(clustSizeTrain,clustSizeTest); //Kernel of training points compared to test points
    MatrixXd Kss(clustSizeTest,clustSizeTest); //Kernel of test points compared to training points
    MatrixXd Krr(clustSizeTrain,clustSizeTrain); //Kernel of training points compared to training points

    MatrixXd mXTrainSingleI(2,1); //X training at a single point
    MatrixXd mXTestSingleI(2,1); //X test at a single point
    MatrixXd mXTrainSingleICopy(2,1); //X training at a single point
    MatrixXd mXTestSingleICopy(2,1); //X test at a single point
    MatrixXd mCov(1,1); //Covariance function

    //Constructing Variance between training and test points for current cluster done here

    //Training vs Test Kernel
    for (int i = 0; i < clustSizeTrain; i++) {
      for (int j = 0; j < clustSizeTest; j++) {
            mXTrainSingleI(0,0) = mXTrain(0,i);
            mXTrainSingleI(1,0) = mXTrain(1,i);
            mXTestSingleI(0,0) = mXTest(0,j);
            mXTestSingleI(1,0) = mXTest(1,j);
            mCov = (mXTrainSingleI - mXTestSingleI).transpose()*(mXTrainSingleI - mXTestSingleI);
            Krs(i,j) = exp(-0.5*(mCov(0,0)));
      } 
    }

    //Test vs Training Kernel
    for (int i = 0; i < clustSizeTest; i++) {
      for (int j = 0; j < clustSizeTest; j++) {
            mXTestSingleI(0,0) = mXTest(0,i);
            mXTestSingleI(1,0) = mXTest(1,i);
            mXTestSingleICopy(0,0) = mXTest(0,j);
            mXTestSingleICopy(1,0) = mXTest(1,j);
            mCov = (mXTestSingleI - mXTestSingleICopy).transpose()*(mXTestSingleI - mXTestSingleICopy);
            Kss(i,j) = exp(-0.5*(mCov(0,0)));
      } 
    }

    //Training vs Training Kernel
    for (int i = 0; i < clustSizeTrain; i++) {
      for (int j = 0; j < clustSizeTrain; j++) {
            mXTrainSingleI(0,0) = mXTrain(0,i);
            mXTrainSingleI(1,0) = mXTrain(1,i);
            mXTrainSingleICopy(0,0) = mXTrain(0,j);
            mXTrainSingleICopy(1,0) = mXTrain(1,j);
            mCov = (mXTrainSingleI - mXTrainSingleICopy).transpose()*(mXTrainSingleI - mXTrainSingleICopy);
            Krr(i,j) = exp(-0.5*(mCov(0,0)));
      } 
    }

    float noiseAmount = 0.1;
    MatrixXd mIdentityAndNoise(clustSizeTrain,clustSizeTrain);
    mIdentityAndNoise = mIdentityAndNoise.setIdentity()*(noiseAmount*noiseAmount);

    MatrixXd V(clustSizeTest,clustSizeTest); //Variance
    V = Kss - Krs.transpose()*(Krr + mIdentityAndNoise).inverse()*Krs;

    MatrixXd VDiag(1,clustSizeTest); //Diagonal matrix from V
    //Construct diagonal matrix from V
    for (int i = 0; i < clustSizeTest; i++)
    {
      VDiag(0,i) = V(i,i);
    }

    //test

    sensor_pub.publish(pc);

    //training
    sensor_pub3.publish(pc3);
    //cluster 1
    sensor_pub7.publish(pc7);
    //2
    sensor_pub4.publish(pc4);
    //3
    sensor_pub5.publish(pc5);
    //4
    sensor_pub6.publish(pc6);

    sensor_pub8.publish(pc8);

    //Training points kmeans labeled
    training_pts_kmeans_labeled.publish(pc_training_pts_kmeans_labeled);

    MatrixXd mYTestFormer(clustSizeTest, clustSizeTrain); //variable used to create mYTest
    mYTestFormer = Krs.transpose() * (Krr + mIdentityAndNoise).inverse() * mYTrain.transpose();

    mYTest = mYTestFormer.transpose(); //Predicted Y values for test points
    
    MatrixXd upper(1,clustSizeTest); //Upper Variance
    MatrixXd lower(1,clustSizeTest); //Lower Variance
    //populate upper and lower matrices
    for (int i = 0; i < clustSizeTest; i++)
    {
        upper(0,i) = mYTest(0,i) + VDiag(0,i);
        lower(0,i) = mYTest(0,i) - VDiag(0,i);
    }

    //send upper points to channel for display
    for (uint32_t i = 0; i < clustSizeTest; ++i)
    {
        geometry_msgs::Point32 pt1;
        pt1.x = mXTest(0,i);
        pt1.y = mXTest(1,i);
        pt1.z = upper(0,i);
        pc2.points.push_back(pt1);
        pt_test_channel.values.push_back(10.0);
    }

    //send predicted Y points to channel for display
    for (uint32_t i = 0; i < clustSizeTest; ++i)
    {
        geometry_msgs::Point32 pt1;
        pt1.x = mXTest(0,i);
        pt1.y = mXTest(1,i);
        pt1.z = mYTest(0,i);
        pc2.points.push_back(pt1);
        pt_test_channel.values.push_back(5.0);
    }

    //send lower points to channel for display
    for (uint32_t i = 0; i < clustSizeTest; ++i)
    {
        geometry_msgs::Point32 pt1;
        pt1.x = mXTest(0,i);
        pt1.y = mXTest(1,i);
        pt1.z = lower(0,i);
        pc2.points.push_back(pt1);
        pt_test_channel.values.push_back(1.0);
    }


    pc2.channels.push_back(pt_test_channel);


    sensor_pub2.publish(pc2);

    sensor_pub7.publish(pc7);

    marker_pub.publish(markerA);

    r.sleep();

    ros::spin();
}//main
