/*
Discription:
This script takes the intent quantities, transforms them to the system body-fixed frame and republishes this output allowing for cohesive expression of intent estimate and a controlled frequency. The outputs consist of: 
1. Wrench (felt by robot)
2. System odometry (pose/twist)
3. Planar Head orientation (to be scaled by twist magnitude)
4. Planer Step vector (to be scaled by twist magnitude)
5. Surrounding obstacles abstraction (output of local map in body-fixed frame and image representation (for NN))

Created by: Monroe Kennedy III
All rights reserved
*/
#include <gp_human_pred/basic_ros_include.h>
#include <gp_human_pred/gridmap_include.h>
#include <gp_human_pred/extract_intent_features_body_frame.h>


#include <intelligent_coop_carry_msgs/HumanTransport.h>
#include <intelligent_coop_carry_msgs/IntentQuantitiesLocal.h>


#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

class IntentGoverner
{
  public:
    IntentGoverner(ros::NodeHandle &nh);
    ~IntentGoverner();
    void HumanIntentCallback(const intelligent_coop_carry_msgs::HumanTransport& msg); 
    void SysOdomCallback(const nav_msgs::Odometry& msg);
    ros::Subscriber human_intent_raw_sub;
    ros::Subscriber sys_odom_sub;
    ros::Publisher local_intent_quantities_publisher;
    ros::Publisher local_map_publisher;
    //Debug checker
    ros::Publisher wrench_checker_publisher;
    ros::Publisher system_pose_checker_publisher;
    ros::Publisher step_checker_publisher;
    ros::Publisher head_checker_publisher;
    ros::Publisher head_step_pose_vect_publisher;
    void VisualizeSystemPoseHeadStepVectors(const geometry_msgs::TransformStamped&  system_pose,const geometry_msgs::Vector3Stamped& local_step_vect,const geometry_msgs::Vector3Stamped local_head_vect,const geometry_msgs::TwistStamped local_twist);
    void VisualizationVectorMarkerGenerator(visualization_msgs::Marker& marker,const std_msgs::Header& header, int& marker_id,const geometry_msgs::Vector3& vector_base,const geometry_msgs::Vector3& vector,const std::string& marker_text, visualization_msgs::Marker& marker_text_marker, float red=1, float blue=0, float green=0);

  private:
    humanintentcls::TransformIntentQuantities trans_intent_quant_obj_;
    ros::NodeHandle pnh_;
    nav_msgs::Odometry sys_odom_;
    bool sys_odom_populated_;  
    tf2_ros::Buffer tfBuffer;
    std::unique_ptr<tf2_ros::TransformListener> tfListener;
    tf2_ros::TransformBroadcaster tfbroadcast;
    std::string  system_frame_name_;
    std::string  world_frame_name_;
    float local_obstacle_radius_;
    int pose_header_seq_;
    bool debug_msgs_bool_;
    double dt_, start_time_;
    double bag_dt_, bag_prev_time_;
};

IntentGoverner::IntentGoverner(ros::NodeHandle &nh) : pnh_(nh)
{
  //Setup subscribers
  human_intent_raw_sub = pnh_.subscribe("/human_transport_combined",1,&IntentGoverner::HumanIntentCallback, this);
  sys_odom_sub = pnh_.subscribe("/vicon/human_human_load/odom",1,&IntentGoverner::SysOdomCallback, this);
  local_intent_quantities_publisher = pnh_.advertise<intelligent_coop_carry_msgs::IntentQuantitiesLocal>("local_intent_quantities",1); //publish local intent quantities
  local_map_publisher = pnh_.advertise<sensor_msgs::Image>("local_map_pub",1);
  sys_odom_populated_ = false;
  tfListener.reset(new tf2_ros::TransformListener(tfBuffer));
  pnh_.param<std::string>("world_frame_name", world_frame_name_, "mocap");
  pnh_.param<std::string>("system_frame_name", system_frame_name_, "carried_load");
  pnh_.param<bool>("debug_msgs", debug_msgs_bool_,true);
  local_obstacle_radius_ = 4.0; //meters (radius around system to consider obstacles)
  pose_header_seq_ = 0;
  //Debug publishers
  wrench_checker_publisher = pnh_.advertise<geometry_msgs::WrenchStamped>("local_wrench_checker",1);
  system_pose_checker_publisher = pnh_.advertise<geometry_msgs::TransformStamped>("local_sys_pose_checker",1);
  step_checker_publisher = pnh_.advertise<geometry_msgs::Vector3Stamped>("local_step_checker",1);
  head_checker_publisher = pnh_.advertise<geometry_msgs::Vector3Stamped>("local_head_checker",1);
  head_step_pose_vect_publisher = pnh_.advertise<visualization_msgs::MarkerArray>("Vector_pub_checker",1);
  //check time
  dt_ = 0.0;
  start_time_ = ros::Time::now().toSec();
  bag_prev_time_ = ros::Time::now().toSec();
}

IntentGoverner::~IntentGoverner()
{
}

void IntentGoverner::HumanIntentCallback(const intelligent_coop_carry_msgs::HumanTransport& msg)
{
  //When this callback returns, utilize all values interior and also call local map evaluator, then transform the quanities and publish for use
  //0. generate converted types
  geometry_msgs::WrenchStamped local_wrench;
  // nav_msgs::Odometry local_odom;
  geometry_msgs::TwistStamped local_odom_twist;
  //Initial vectors in world frame must first be obtained
  geometry_msgs::Vector3Stamped step_vect_world_frame;
  geometry_msgs::Vector3Stamped head_vect_world_frame;
  //When returned, the following will have been scaled by the twist vector after being normalized
  geometry_msgs::Vector3Stamped local_step_vect;
  geometry_msgs::Vector3Stamped local_head_vect;
  //For map
  grid_map::GridMap local_gridmap_worldframe({"layer"});
  grid_map::GridMap robo_frame_local_map({"layer"});
  grid_map_msgs::GridMap robo_frame_local_map_msg;
  sensor_msgs::Image local_map_img_robo_frame;
  //For system pose
  geometry_msgs::TransformStamped  system_pose; //pose of system in world frame
  bool transform_bool = trans_intent_quant_obj_.ObtainTransform(world_frame_name_, system_frame_name_, system_pose);
  system_pose.header.seq = ++pose_header_seq_;
  //1. query system odom and system transform
  if(sys_odom_populated_ == false || transform_bool == false)
  {
    ROS_WARN("Unable to obtain transform from system to world");
    return; //sys odom hasn't been filled or transform not available
  }
  //2. Convert human step to vector then 
  bool prev_step_pop = trans_intent_quant_obj_.UpdateStepVector(msg.step_position, step_vect_world_frame);
  if(prev_step_pop==false)
  {
    return; //two steps have not yet been observed, return
    ROS_WARN("Two steps have not been observed in intent_regulator");
  }
  trans_intent_quant_obj_.ConvertHeadPoseToVector(msg.head_pose, head_vect_world_frame);
  //3. Obtain Local map
  bool map_obtained = trans_intent_quant_obj_.LocalMapGenerator(sys_odom_, local_gridmap_worldframe, local_obstacle_radius_);
  if (map_obtained == false) 
  {
    return; //no map, exit
  }
  //4. Convert all intent quantities to body fixed frame
  trans_intent_quant_obj_.ObtainLocalStepVect(step_vect_world_frame, local_step_vect, system_pose);
  trans_intent_quant_obj_.ObtainLocalHeadVect(head_vect_world_frame, local_head_vect, system_pose);
  trans_intent_quant_obj_.ObtainLocalWrenchVect(msg.follower_wrench, local_wrench, system_pose);
  trans_intent_quant_obj_.ObtainLocalTwist(sys_odom_, local_odom_twist, system_pose);
  trans_intent_quant_obj_.ObtainLocalMap(local_gridmap_worldframe, robo_frame_local_map, local_map_img_robo_frame, system_pose);
  grid_map::GridMapRosConverter::toMessage(robo_frame_local_map, robo_frame_local_map_msg); //grid_map::GridMapRosConverter::fromOccupancyGrid(*msg, "layer", map);
  //5. Scale the head/step vectors by the twist
  trans_intent_quant_obj_.PlanarVectorScaledByTwist(local_step_vect, local_odom_twist);
  trans_intent_quant_obj_.PlanarVectorScaledByTwist(local_head_vect, local_odom_twist);
  //6. construct all quantities into a message (with pose in world frame) and publish
  intelligent_coop_carry_msgs::IntentQuantitiesLocal local_intent_quanities_msg;
  local_intent_quanities_msg.header = msg.header;
  local_intent_quanities_msg.header.frame_id = system_frame_name_;
  local_intent_quanities_msg.system_pose = system_pose;
  local_intent_quanities_msg.local_step_vect = local_step_vect;
  local_intent_quanities_msg.local_head_vect = local_head_vect;
  local_intent_quanities_msg.local_wrench = local_wrench;
  local_intent_quanities_msg.local_twist = local_odom_twist;
  local_intent_quanities_msg.local_gridmap = robo_frame_local_map_msg;
  local_intent_quanities_msg.local_gridmap_img = local_map_img_robo_frame;
  //7. Publish
  local_intent_quantities_publisher.publish(local_intent_quanities_msg);
  local_map_publisher.publish(local_map_img_robo_frame);
  if(debug_msgs_bool_)
  {
    wrench_checker_publisher.publish(local_wrench);
    system_pose_checker_publisher.publish(system_pose);
    step_checker_publisher.publish(local_step_vect);
    head_checker_publisher.publish(local_head_vect);
    VisualizeSystemPoseHeadStepVectors(system_pose,local_step_vect,local_head_vect,local_odom_twist);
  }
  //reset bool
  if(sys_odom_populated_)
  {
    sys_odom_populated_ = false; //reset
  }

  //Check time passage
  /*
  double curr_time = ros::Time::now().toSec();
  dt_ = curr_time - start_time_;
  start_time_ = curr_time;

  bag_dt_ = msg.header.stamp.toSec() - bag_prev_time_;
  bag_prev_time_ = msg.header.stamp.toSec();

  std::cout << "bag dt: " << bag_dt_ << "\nsim time: " << dt_ << std::endl;
  */
}

void IntentGoverner::SysOdomCallback(const nav_msgs::Odometry& msg)
{
  sys_odom_ = msg; //populate this to be access in humanintent callback
  sys_odom_populated_ = true;
}


void IntentGoverner::VisualizeSystemPoseHeadStepVectors(const geometry_msgs::TransformStamped&  system_pose,const geometry_msgs::Vector3Stamped& local_step_vect,const geometry_msgs::Vector3Stamped local_head_vect, const geometry_msgs::TwistStamped local_twist)
{
  //Generate markers and array
  visualization_msgs::Marker sys_pose_marker;
  visualization_msgs::Marker sys_pose_marker_text;
  int sys_pose_marker_id = 0;
  visualization_msgs::Marker head_marker;
  visualization_msgs::Marker head_marker_text;
  int head_marker_id = 1;
  visualization_msgs::Marker step_marker;
  visualization_msgs::Marker step_marker_text;
  int step_marker_id = 2;
  visualization_msgs::Marker twist_marker;
  visualization_msgs::Marker twist_marker_text;
  int twist_marker_id = 3;
  visualization_msgs::MarkerArray marker_array;
  std::vector<visualization_msgs::Marker> marker_list;
  //system pose
  Eigen::Affine3d system_pose_eig = tf2::transformToEigen(system_pose);
  Eigen::Vector3d x_axis(1,0,0);
  Eigen::Vector3d x_axis_sys = system_pose_eig.rotation()*x_axis;
  geometry_msgs::Vector3 sys_pose_vect_base;
  sys_pose_vect_base.x = system_pose.transform.translation.x;
  sys_pose_vect_base.y = system_pose.transform.translation.y;
  sys_pose_vect_base.z = system_pose.transform.translation.z;
  geometry_msgs::Vector3 sys_pose_vect;
  sys_pose_vect.x = x_axis_sys[0];
  sys_pose_vect.y = x_axis_sys[1];
  sys_pose_vect.z = x_axis_sys[2];
  float red = 1.0;  float blue = 0.0;  float green = 0.0; 
  std::string marker_text_pose = "pose";
  VisualizationVectorMarkerGenerator(sys_pose_marker, system_pose.header,sys_pose_marker_id,sys_pose_vect_base,sys_pose_vect,marker_text_pose,sys_pose_marker_text,red,blue,green);
  //Step 
  geometry_msgs::Vector3 local_frame_zero;
  float red_s = 0.0;  float blue_s = 1.0;  float green_s = 0.0; 
  std::string marker_text_step = "step";
  VisualizationVectorMarkerGenerator(step_marker, local_step_vect.header,head_marker_id,local_frame_zero,local_step_vect.vector,marker_text_step,head_marker_text,red_s,blue_s,green_s);
  //Head
  float red_h = 0.0; float blue_h = 0.0; float green_h = 1.0; 
  std::string marker_text_head = "head";
  VisualizationVectorMarkerGenerator(head_marker, local_head_vect.header,step_marker_id,local_frame_zero,local_head_vect.vector,marker_text_head,step_marker_text,red_h,blue_h,green_h);
  //Twist (xy)
  float red_t = 0.0; float blue_t = 1.0; float green_t = 1.0; 
  std::string marker_text_twist = "twist";
  geometry_msgs::Vector3 twist_vect;
  twist_vect.x = local_twist.twist.linear.x;
  twist_vect.y = local_twist.twist.linear.y;
  twist_vect.z = local_twist.twist.linear.z;
  VisualizationVectorMarkerGenerator(twist_marker, local_twist.header,twist_marker_id,local_frame_zero,twist_vect,marker_text_twist,twist_marker_text,red_t,blue_t,green_t);
  //publish
  marker_list.push_back(sys_pose_marker);
  marker_list.push_back(sys_pose_marker_text);
  marker_list.push_back(head_marker);
  marker_list.push_back(head_marker_text);
  marker_list.push_back(step_marker);
  marker_list.push_back(step_marker_text);
  marker_list.push_back(twist_marker);
  marker_list.push_back(twist_marker_text);
  marker_array.markers = marker_list;
  head_step_pose_vect_publisher.publish(marker_array);
}

void IntentGoverner::VisualizationVectorMarkerGenerator(visualization_msgs::Marker& marker,const std_msgs::Header& header, int& marker_id,const geometry_msgs::Vector3& vector_base,const geometry_msgs::Vector3& vector,const std::string& marker_text, visualization_msgs::Marker& marker_text_marker, float red, float blue, float green)
{
  marker.header = header;
  // marker.ns = "my_namespace";
  marker.id = marker_id;
  marker.type = visualization_msgs::Marker::ARROW;
  marker.action = visualization_msgs::Marker::ADD;
  geometry_msgs::Point base_point;
  base_point.x = vector_base.x;
  base_point.y = vector_base.y;
  base_point.z = vector_base.z;
  geometry_msgs::Point tip_point;
  tip_point.x = vector_base.x + vector.x;
  tip_point.y = vector_base.y + vector.y;
  tip_point.z = vector_base.z + vector.z;
  marker.points.push_back(base_point);
  marker.points.push_back(tip_point);
  marker.scale.x = 0.03;
  marker.scale.y = 0.05;
  // marker.scale.z = 0.1;
  marker.color.a = 1.0; // Don't forget to set the alpha!
  marker.color.r = red;
  marker.color.g = green;
  marker.color.b = blue;
  //now for marker text:
  marker_text_marker.header = header;//same position
  marker_text_marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  marker_text_marker.id = (int) marker_id + 10;
  marker_text_marker.pose.position.x = tip_point.x;
  marker_text_marker.pose.position.y = tip_point.y;
  marker_text_marker.pose.position.z = tip_point.z;
  marker_text_marker.color.a = 1.0;
  marker_text_marker.color.r = red;
  marker_text_marker.color.g = green;
  marker_text_marker.color.b = blue;
  marker_text_marker.scale.z = 0.1;
  marker_text_marker.text = marker_text;
}

int main(int argc, char **argv)
{
  ros::init(argc,argv, "intent_regulator");
  ros::NodeHandle nh("~");
  IntentGoverner intent_gov_obj(nh);
  ros::spin();
  return 0;
}

