#include <ros.h>
#include <gp_human_pred/WrenchArduino.h>

#include "HX711.h"

HX711 Bottom_Right;
HX711 Top_Right;
HX711 Bottom_Left;
HX711 Top_Left;

//For Ros:
ros::NodeHandle nh;
gp_human_pred::WrenchArduino wrench_msg;
ros::Publisher wrench_pub("arduino_wrench", &wrench_msg);

void setup() {
  Serial.begin(38400);
  Serial.println("HX711 Demo");

  // Initialize Pins
  Bottom_Right.begin(A1, A0);
  Top_Right.begin(A3, A2);
  Bottom_Left.begin(A5, A4);
  Top_Left.begin(A7, A6);

  // Tare System
  Serial.println("Get ready to zero system in ...");
  delay(1500);
  Serial.println("3...");
  delay(1500);
  Serial.println("2...");
  delay(1500);
  Serial.println("1...");
  delay(1500);
  Serial.println("Zeroing System");
  
  Bottom_Right.set_scale(410.f);      // 410 // this value is obtained by calibrating the scale with known weights; see the README for details
  Bottom_Right.tare();				        // reset the scale to 0

  Top_Right.set_scale(410.f);         // 410 // this value is obtained by calibrating the scale with known weights; see the README for details
  Top_Right.tare();                   // reset the scale to 0

  Bottom_Left.set_scale(410.f);       // 410 // this value is obtained by calibrating the scale with known weights; see the README for details
  Bottom_Left.tare();                 // reset the scale to 0

  Top_Left.set_scale(410.f);          // 410 // this value is obtained by calibrating the scale with known weights; see the README for details
  Top_Left.tare();                    // reset the scale to 0

  Serial.println("System Zeroed");
  Serial.println("Start Readings: (in grams)");

  //For ROS:
  nh.initNode();
  nh.advertise(wrench_pub);
}

// Define variables
float XR;
float YR;
float XL;
float YL;
float frequency = 2;

float threshold_bottom = 3;
float threshold_top = 3;

void loop() {
    
// X axis / Right Sensor Calculations
  XR = Bottom_Right.get_units(frequency);     // Get Raw Input Average for X axis / Right Sensor

  if (abs(XR) > threshold_bottom)
  {
    if (XR > 0)
    {
      XR = 0.9945*XR - 0.3861; // X axis, Right Sensor Positive Condition
    }
    else
    {
      XR = 1.0002*XR + 2.739; // X axis, Right Sensor Negative Condition
    }
  }
  else
   {
    XR = XR; 
   }

// Y axis / Right Sensor Calculations
  YR = Top_Right.get_units(frequency);        // Get Raw Input Average for Y axis / Right Sensor

  if (abs(YR) > threshold_top)
  {
    if (YR > 0)
      {
      YR = 0.9262*YR + 3.0511; // Y axis, Right Sensor Positive Condition
      }
    else
      {
      YR = 0.931*YR - 0.8427; // Y axis, Right Sensor Negative Condition
      }
  }
  else
    {
    YR = YR;
    }  

  YR = -YR;

// X axis / Left Sensor Calculations
  XL = Bottom_Left.get_units(frequency);      // Get Raw Input Average for X axis / Left Sensor
  
  if (abs(XL) > threshold_bottom)
  {
    if (XL > 0)
    {
      XL = 0.9945*XL - 0.3861; // X axis, Left Sensor Positive Condition
    }
    else
    {
      XL = 1.0002*XL + 2.739; // X axis, Left Sensor Negative Condition
    }
  }
  else
   {
    XL = XL; 
   }

   XL = -XL;

// Y axis / Left Sensor Calculations
  YL = Top_Left.get_units(frequency);         // Get Raw Input Average for Y axis / Right Sensor

  if (abs(YL) > threshold_top)
  {
    if (YL > 0)
      {
      YL = 0.9262*YL + 3.0511; // Y axis, Left Sensor Positive Condition
      }
    else
      {
      YL = 0.931*YL - 0.8427; // Y axis, Left Sensor Negative Condition
      }
  }
  else
    {
    YL = YL;
    }  

// Print Results of all Sensors
//  Serial.print("X axis, Right Sensor Reading:\t");
//  Serial.print(XR, 1);
//  Serial.print("\t| Y axis, Right Sensor Reading:\t");
//  Serial.println(YR, 1);
//
//  Serial.print("X axis, Left Sensor Reading:\t");
//  Serial.print(XL, 1);
//  Serial.print("\t| Y axis, Left Sensor Reading:\t");
//  Serial.println(YL, 1);
//  Serial.println();

  //For ROS: 
  wrench_msg.header.stamp = nh.now();
  wrench_msg.header.frame_id = "carried_load";
  wrench_msg.Fr.header.stamp = nh.now();
  wrench_msg.Fr.header.frame_id = "right_force_sensor";
  wrench_msg.Fl.header.stamp = nh.now();
  wrench_msg.Fl.header.frame_id = "left_force_sensor";
  
  wrench_msg.Fr.wrench.force.x = 9.81*XR/1000.0; //Convert back to kg for Newtons
  wrench_msg.Fr.wrench.force.y = 9.81*YR/1000.0;
  wrench_msg.Fl.wrench.force.x = 9.81*XL/1000.0;
  wrench_msg.Fl.wrench.force.y = 9.81*YL/1000.0;
  wrench_pub.publish(&wrench_msg);
  nh.spinOnce();

}
