/*
Copyright (c) <2018>, <Monroe Kennedy III>
* All rights reserved.
*/
#include <ros/ros.h>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <Eigen/SVD>
#include <time.h>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <map> //dictionary equivalent
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/transform_broadcaster.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
//Bring in sensor messages
#include <std_msgs/Header.h>
#include <gp_human_pred/WrenchArduino.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/WrenchStamped.h>
#include <gp_human_pred/HumanTransport.h>


class TFTestCLass
{
  public:
    TFTestCLass(); //constructor
    //List public class functions/variables

    void TFCallback(const gp_human_pred::HumanTransport::ConstPtr& msg); //inside each callback, a variable is populated that is constantly updated with latest version of msg

  private:
    //List private variables/fncts
    ros::NodeHandle nh;
    //Subscribe to topics
    ros::Subscriber arduino_wrench_sub_;
    //Tf in case needed
    tf2_ros::Buffer tfBuffer;
    std::unique_ptr<tf2_ros::TransformListener> tfListener;
    tf2_ros::TransformBroadcaster tfbroadcast;
}; //Remember to put semicolon after class declaration.


TFTestCLass::TFTestCLass()
{
  nh = ros::NodeHandle("~"); //This argument makes all topics internal to this node namespace
  //Estabilish the transformer
  tfListener.reset(new tf2_ros::TransformListener(tfBuffer));

  arduino_wrench_sub_ = nh.subscribe("/human_transport_combined",1,&TFTestCLass::TFCallback,this);
}

void TFTestCLass::TFCallback(const gp_human_pred::HumanTransport::ConstPtr& msg){
  //Collect msg and populate variable
  geometry_msgs::TransformStamped tf_stamped_RH_to_EE;
  geometry_msgs::TransformStamped tf_stamped_laser_to_mocap;
  bool tf_success = true; //To determine if tf's were obtained to continue with calculations
  try{
    tf_stamped_laser_to_mocap = tfBuffer.lookupTransform("mocap", "laser_front", ros::Time(0)); 

  }
  catch (tf2::TransformException &ex) {
    ROS_WARN("%s",ex.what());
    ROS_WARN("Transform failed");
    ros::Duration(1.0).sleep();
    tf_success = false;
  }
  if(tf_success) //Only proceed if tf's were obtained to perform calculations
  {

    std::cout << "tf a success: " << tf_stamped_laser_to_mocap << std::endl;

  }
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "test_tf");
  ROS_INFO("test tf node running");
  //intatiate class object
  // nh = ros::NodeHandle("~")
  TFTestCLass sensor_repub_obj;
  ros::spin();//Call the class, service callback is in the constructor, and looped by the ros spin
  return 0;
}
