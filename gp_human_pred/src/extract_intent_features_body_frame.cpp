#include<gp_human_pred/extract_intent_features_body_frame.h>

namespace humanintentcls
{

  TransformIntentQuantities::TransformIntentQuantities()
  {
  //Constructor
    tfListener.reset(new tf2_ros::TransformListener(tfBuffer));
    prev_step_position_filled_ = false; //set initially to false, then after first measurement this is updated, if sufficient time passes without update this is set back to false to avoid large jumps
  }

  TransformIntentQuantities::~TransformIntentQuantities()
  {
  //Destructor
  }


  void TransformIntentQuantities::ObtainLocalMap(const grid_map::GridMap &orig_local_map, grid_map::GridMap &robo_frame_local_map, sensor_msgs::Image &out_img, const geometry_msgs::TransformStamped& robo_world_tf)
  {
    /*
    1. Get TF from gridmap fram to robot frame, 
       take all the points in radius circle of 
       robot and rotate them
    2. Populate new robo frame local map, and 
       convert to image to send back
    */
    //1. Set position of local frame to be 0,0
    grid_map::Position origin_local(0,0);
    // Eigen::Array2d local_map_length = orig_local_map.getLength(); 
    // local_map_length(0) = local_map_length(0)*1.414;
    // local_map_length(1) = local_map_length(1)*1.414;
    robo_frame_local_map.setGeometry(orig_local_map.getLength(), 1.5*orig_local_map.getResolution(), origin_local); //1.5res is to avoid holes when rotating map (for square grid, furthest distance is sqrt(2)*res)
    // robo_frame_local_map.setGeometry(local_map_length, 1.5*orig_local_map.getResolution(), origin_local); //1.5res is to avoid holes when rotating map (for square grid, furthest distance is sqrt(2)*res)
    robo_frame_local_map.setFrameId(robo_world_tf.child_frame_id); //system local frame
    //2. Get the rotation between world to local frame, to be used to rotate points
    Eigen::Affine3d sys_trans_eig = tf2::transformToEigen(robo_world_tf);
    Eigen::Affine3d sys_trans_eig_z_aligned;
    ExtractZframeRotation(sys_trans_eig, sys_trans_eig_z_aligned);
    //3. Iterate through points, rotating them accordingly and populate new local gridmap
    std::vector<grid_map::Position> new_positions;
    std::vector<float> value_at_new_positions;
    grid_map::Position orig_local_map_center = orig_local_map.getPosition();
    grid_map::Length orig_map_length = orig_local_map.getLength();
    float map_radius = orig_map_length[0]/2.0; //radius of the map :: can also use a private variable

    // for (grid_map::GridMapIterator iterator(orig_local_map); !iterator.isPastEnd(); ++iterator) {
    for (grid_map::SpiralIterator iterator(orig_local_map, orig_local_map_center, map_radius); !iterator.isPastEnd(); ++iterator) {
      float curr_point_value = orig_local_map.at("layer",*iterator);
      value_at_new_positions.push_back(curr_point_value);
      grid_map::Position curr_position;
      orig_local_map.getPosition(*iterator,curr_position); //accessed curr_position[0],[1] (2D)
      Eigen::Vector3d curr_pos_3d(curr_position[0] - orig_local_map_center[0],curr_position[1] - orig_local_map_center[1],0);
      Eigen::Vector3d rotated_curr_pos_3d = sys_trans_eig_z_aligned.inverse().rotation()*curr_pos_3d;
      grid_map::Position rotated_curr_pos(rotated_curr_pos_3d[0],rotated_curr_pos_3d[1]);
      new_positions.push_back(rotated_curr_pos);
    }
    //populating new gridmap in local frame at specified local-frame positions    
    for (int iter=0; iter < new_positions.size(); ++iter)
    {
      robo_frame_local_map.atPosition("layer",new_positions[iter]) = value_at_new_positions[iter];
    }
    //4. Convert new map in local frame to image to export
    const float minValue = -1.0;
    const float maxValue = 1.0;
    //std::string encoding = "rgb8";
    std::string encoding = "mono8";
    bool conversion_success = grid_map::GridMapRosConverter::toImage(robo_frame_local_map, "layer", encoding, minValue, maxValue, out_img);
    if (conversion_success == false)
    {
      ROS_WARN("conversion of gridmap to image failed");
    }
  }

  void TransformIntentQuantities::ExtractZframeRotation(const Eigen::Affine3d& tf_inpt, Eigen::Affine3d& tf_outpt)
  {
    //This function assumes that the input frame z axis must be rotated to the world frame, and the resultant matrix is desired rotation (only about world z-axis)
    Eigen::Vector3d world_z_axis(0,0,1);
    Eigen::Vector3d sys_frame_z_axis = tf_inpt.rotation()*world_z_axis;
    //Angle
    double dot_prod = sys_frame_z_axis.dot(world_z_axis);
    double ang = 0.0;
    if(dot_prod >= 1.0)
    {
      ang = 0.0;
      // ROS_WARN("handling 1.0 ang case");
    }else if(dot_prod <= -1.0)
    {
      ang = - PI;
    }else
    {
      double ang = std::acos(dot_prod);
    }
    
    //Axis
    Eigen::Vector3d z_sys_crs_w = sys_frame_z_axis.cross(world_z_axis); //key is here: rotates sys frame to world frame (this is the tf)
    double cross_prod_norm = z_sys_crs_w.norm();
    Eigen::Vector3d rot_vect;
    if (cross_prod_norm < 0.0001)
    {
      //Vectors are aligned
      rot_vect = world_z_axis;
    }
    else{
      rot_vect = z_sys_crs_w.normalized();
    }
    tf_outpt = Eigen::AngleAxisd(ang,rot_vect)*tf_inpt; //careful here, not sure what action this has on the translation component
  }

  bool TransformIntentQuantities::ObtainTransform(const std::string& target_frame, const std::string& source_frame, geometry_msgs::TransformStamped& transform)
  {
    bool resp = true;
    try{
      transform = tfBuffer.lookupTransform(target_frame, source_frame, ros::Time(0));
    }
    catch (tf2::TransformException &ex) {
      ROS_WARN("%s",ex.what());
      ROS_WARN("Transform from %s to %s not possible",source_frame.c_str(),target_frame.c_str());
      ros::Duration(1.0).sleep();
      resp = false;
    }
    return resp;
  }

  void TransformIntentQuantities::OdomToTransform (const nav_msgs::Odometry& odom, geometry_msgs::TransformStamped& transform)
  {
    //takes pose in odom and makes it a transformstamped 
    transform.header = odom.header;
    transform.transform.translation.x = odom.pose.pose.position.x;
    transform.transform.translation.y = odom.pose.pose.position.y;
    transform.transform.translation.z = odom.pose.pose.position.z;
    transform.transform.rotation.x = odom.pose.pose.orientation.x;
    transform.transform.rotation.y = odom.pose.pose.orientation.y;
    transform.transform.rotation.z = odom.pose.pose.orientation.z;
    transform.transform.rotation.w = odom.pose.pose.orientation.w;
  }

  bool TransformIntentQuantities::TransformSysToMapFrame(geometry_msgs::TransformStamped& sys_transform, const std::string map_frame)
  {
    geometry_msgs::TransformStamped transform;
    bool tf_success = ObtainTransform(map_frame, sys_transform.header.frame_id, transform);
    if (tf_success == false)
    {
      return tf_success;
    }
    //Had T_{sys to world}, found T_{world to map} (which may be identity), used these to find T_{sys to map}
    Eigen::Affine3d sys_trans_eig = tf2::transformToEigen(sys_transform);
    Eigen::Affine3d trans_to_map_eig = tf2::transformToEigen(transform);
    Eigen::Affine3d sys_trans_upd  = trans_to_map_eig * sys_trans_eig;
    sys_transform = tf2::eigenToTransform(sys_trans_upd);
    return tf_success;
  }

  bool TransformIntentQuantities::LocalMapGenerator(const nav_msgs::Odometry& sys_odom, grid_map::GridMap& local_gridmap_worldframe, const float local_obstacle_radius)
  {
    //This function obtains the local map centered at the current system pose
    bool map_gen_bool = true;
    //Read in map
    boost::shared_ptr<const nav_msgs::OccupancyGrid_<std::allocator<void> > > req_map_ptr; //boost used because waitformessage requires it
    req_map_ptr = ros::topic::waitForMessage<nav_msgs::OccupancyGrid>("/map", ros::Duration(1));
    if(req_map_ptr == NULL){
      ROS_ERROR("Robot map not published on /map");
      return false; //map not seen/published
    }
    nav_msgs::OccupancyGrid req_map = *req_map_ptr;
    //convert map to gridmap object
    grid_map::GridMap complete_map({"layer"});
    grid_map::GridMap local_map({"layer"});
    grid_map::GridMapRosConverter::fromOccupancyGrid(req_map, "layer", complete_map);
    local_map.setFrameId(complete_map.getFrameId()); //consistent frame id as complete map
    //Ensure that odom and map are in the same frame (if not transform odom)
    geometry_msgs::TransformStamped sys_transform;
    OdomToTransform(sys_odom,sys_transform);
    map_gen_bool = TransformSysToMapFrame(sys_transform, req_map.header.frame_id); //ensure odom is in right frame
    grid_map::Position center(sys_transform.transform.translation.x, sys_transform.transform.translation.y);
    //Using the system odometry to obtain center, use a circular iterator to extract the local map centered at odom with radius 'local_obstacle_radius' and populate local_map
    // local_map.setPosition(center);
    grid_map::Length submaplength(2*local_obstacle_radius,2*local_obstacle_radius);//containing square
    local_map.setGeometry(submaplength, complete_map.getResolution(),center);
    // local_map.setPosition(center);
    std::vector<float> map_values;
    std::vector<grid_map::Position> map_positions;
    //obtain the local map from global map
    for (grid_map::SpiralIterator iterator(complete_map, center, local_obstacle_radius); !iterator.isPastEnd(); ++iterator){
      float curr_point_value = complete_map.at("layer",*iterator); //Store the value at the current point (-1 is unobserved, 0 is free, 1 is occupied)
      map_values.push_back(curr_point_value);
      grid_map::Position curr_position;
      complete_map.getPosition(*iterator,curr_position);
      map_positions.push_back(curr_position);
    }
    //Now populate local map object
    for (int iter=0; iter < map_positions.size(); ++iter)
    {
      local_map.atPosition("layer",map_positions[iter]) = map_values[iter];
    }
    local_gridmap_worldframe = local_map;
    return map_gen_bool;
  }

  void TransformIntentQuantities::RotateVectorGivenTransform(const geometry_msgs::Vector3& inpt_vect, geometry_msgs::Vector3& outpt_vect, const geometry_msgs::TransformStamped& transform)
  {
    //This rotates the vector from world frame to local frame, given the transform is local frame to world frame
    Eigen::Affine3d trans_eig = tf2::transformToEigen(transform);
    Eigen::Vector3d inpt_vect_eig(inpt_vect.x,inpt_vect.y,inpt_vect.z);
    Eigen::Vector3d outpt_vect_eig;
    outpt_vect_eig=trans_eig.inverse().rotation()*inpt_vect_eig;
    outpt_vect.x = outpt_vect_eig[0];
    outpt_vect.y = outpt_vect_eig[1];
    outpt_vect.z = outpt_vect_eig[2];
  }

  void TransformIntentQuantities::ObtainLocalStepVect(const geometry_msgs::Vector3Stamped& inpt_vect, geometry_msgs::Vector3Stamped& outpt_vect, const geometry_msgs::TransformStamped& robo_world_tf)
  {
    //Transform step vector
    outpt_vect.header = inpt_vect.header;
    outpt_vect.header.frame_id = robo_world_tf.child_frame_id; //label as system local frame
    //Rotate vector
    RotateVectorGivenTransform(inpt_vect.vector,outpt_vect.vector,robo_world_tf);
  }

  void TransformIntentQuantities::ObtainLocalHeadVect(const geometry_msgs::Vector3Stamped& inpt_pose, geometry_msgs::Vector3Stamped& outpt_vect, const geometry_msgs::TransformStamped& robo_world_tf)
  {
    //Transform head vector
    outpt_vect.header = inpt_pose.header;
    outpt_vect.header.frame_id = robo_world_tf.child_frame_id; //label as system local frame
    //Rotate vector
    RotateVectorGivenTransform(inpt_pose.vector,outpt_vect.vector,robo_world_tf); 
  }

  void TransformIntentQuantities::ObtainLocalWrenchVect(const geometry_msgs::WrenchStamped& inpt_wrench, geometry_msgs::WrenchStamped& outpt_wrench, const geometry_msgs::TransformStamped& robo_world_tf)
  {
    //transform wrench to local frame
    Eigen::Affine3d transform_eig = tf2::transformToEigen(robo_world_tf);
    //Make eigen force/torque
    Eigen::Vector3d force_vect(inpt_wrench.wrench.force.x,inpt_wrench.wrench.force.y,inpt_wrench.wrench.force.z);
    Eigen::Vector3d torque_vect(inpt_wrench.wrench.torque.x,inpt_wrench.wrench.torque.y,inpt_wrench.wrench.torque.z);
    //First ensure wrench is in world frame
    geometry_msgs::TransformStamped transform_wrench_to_world;
    ObtainTransform(robo_world_tf.header.frame_id, inpt_wrench.header.frame_id, transform_wrench_to_world);
    Eigen::Affine3d transform_wrench_to_world_eig = tf2::transformToEigen(transform_wrench_to_world);
    //rotate into system frame force/torque
    Eigen::Vector3d force_vect_local =  transform_eig.inverse().rotation()*transform_wrench_to_world_eig.rotation()*force_vect; // f_local = TF_{world to local} TF_{wrench to world} * f_wrench
    Eigen::Vector3d torque_vect_local =  transform_eig.inverse().rotation()*transform_wrench_to_world_eig.rotation()*torque_vect; 
    //Populate new wrench object
    outpt_wrench.header = inpt_wrench.header;
    outpt_wrench.header.frame_id = robo_world_tf.child_frame_id;
    outpt_wrench.wrench.force.x = force_vect_local[0];
    outpt_wrench.wrench.force.y = force_vect_local[1];
    outpt_wrench.wrench.force.z = force_vect_local[2];
    outpt_wrench.wrench.torque.x = torque_vect_local[0];
    outpt_wrench.wrench.torque.y = torque_vect_local[1];
    outpt_wrench.wrench.torque.z = torque_vect_local[2];
  }

  void TransformIntentQuantities::ObtainLocalTwist(const nav_msgs::Odometry& inpt_odom, geometry_msgs::TwistStamped& odom_twist, const geometry_msgs::TransformStamped& robo_world_tf)
  {
    //Transform odom msg
    //TF from odom frame to system base frame,
    geometry_msgs::TransformStamped transform_odom_to_world;
    ObtainTransform(robo_world_tf.header.frame_id, inpt_odom.child_frame_id, transform_odom_to_world);  //The twist is in the child_frame_id (the pose is in the header.frame_id) :: http://wiki.ros.org/navigation/Tutorials/RobotSetup/Odom
    Eigen::Affine3d transform_odom_to_world_eig = tf2::transformToEigen(transform_odom_to_world);
    Eigen::Affine3d transform_eig = tf2::transformToEigen(robo_world_tf);//sys frame to world frame
    //Rotate linear and angular vectors
    Eigen::Vector3d twist_linear_odom(inpt_odom.twist.twist.linear.x,inpt_odom.twist.twist.linear.y,inpt_odom.twist.twist.linear.z);
    Eigen::Vector3d twist_angular_odom(inpt_odom.twist.twist.angular.x,inpt_odom.twist.twist.angular.y,inpt_odom.twist.twist.angular.z);
    Eigen::Vector3d twist_linear_sys = transform_eig.inverse().rotation()*transform_odom_to_world_eig.rotation()*twist_linear_odom;
    Eigen::Vector3d twist_angular_sys = transform_eig.inverse().rotation()*transform_odom_to_world_eig.rotation()*twist_angular_odom;
    //Populate new twist
    odom_twist.header = inpt_odom.header;
    odom_twist.header.frame_id = robo_world_tf.child_frame_id;
    odom_twist.twist.linear.x = twist_linear_sys[0];
    odom_twist.twist.linear.y = twist_linear_sys[1];
    odom_twist.twist.linear.z = twist_linear_sys[2];
    odom_twist.twist.angular.x = twist_angular_sys[0];
    odom_twist.twist.angular.y = twist_angular_sys[1];
    odom_twist.twist.angular.z = twist_angular_sys[2];
  }
  
  bool TransformIntentQuantities::UpdateStepVector(const people_msgs::PositionMeasurement& curr_step_position, geometry_msgs::Vector3Stamped& step_vector, float max_upd_time)
  {
    /* This function takes steps and calculates vector if consecutive steps are given */
    if (prev_step_position_filled_ == false)
    {
      //indicate if more than two steps have been observed
      prev_step_position_filled_ = true;
      prev_step_position_ = curr_step_position; //Update prev step position
      return false; //first pass
    }
    //It is assumed here that prev_step_position is populated, now get the vector
    /* a) Checks
        i) ensure they are in the same frame  
        ii) (they should also have the same object-id, if not then set prev position filled to false and store this in prev step position)
        iii) ensure the update time is less than max_upd_time or reset
    */
    float time_diff = curr_step_position.header.stamp.toSec() - prev_step_position_.header.stamp.toSec();
    if((curr_step_position.header.frame_id.compare(prev_step_position_.header.frame_id)!=0 )
      || (curr_step_position.object_id.compare(prev_step_position_.object_id)!=0)
      || (time_diff > max_upd_time))
    {
      //  i) frame strings are different
      // ii) object id's are different
      //iii) longer than alloted time since last message
      prev_step_position_filled_ = false;
      return false;
    }
    //b) take the difference in positions
    step_vector.header = curr_step_position.header;
    step_vector.vector.x = curr_step_position.pos.x - prev_step_position_.pos.x;
    step_vector.vector.y = curr_step_position.pos.y - prev_step_position_.pos.y;
    step_vector.vector.z = curr_step_position.pos.z - prev_step_position_.pos.z;
    //Update prev step position
    prev_step_position_ = curr_step_position;
    return prev_step_position_filled_;
  }

  void TransformIntentQuantities::PlanarVectorScaledByTwist(geometry_msgs::Vector3Stamped& vect_to_scale, const geometry_msgs::TwistStamped& sys_odom_twist)
  {
    //This is for head/step vectors, takes in the vector, normalizes if neccessary, scales by twist and outputs resultant (all frames system body-fixed frame)
    //This function also handles the halfspace of the vector with the twist in addition to normalizing and scaling
    Eigen::Vector3d vect_to_scale_eig(vect_to_scale.vector.x,vect_to_scale.vector.y,0.0); //only interested in planer vector
    Eigen::Vector3d sys_twist(sys_odom_twist.twist.linear.x,sys_odom_twist.twist.linear.y,0.0); //only interested in planer twist
    //Half space check
    double vect_dot_prod = vect_to_scale_eig.normalized().dot(sys_twist.normalized());
    if (vect_dot_prod >= 0)
    {
      //Now scale and populate
      Eigen::Vector3d vect_scaled = vect_to_scale_eig.normalized()*sys_twist.norm();
      vect_to_scale.vector.x = vect_scaled[0];
      vect_to_scale.vector.y = vect_scaled[1];
      vect_to_scale.vector.z = vect_scaled[2];
    }else{
      //Now scale and populate
      Eigen::Vector3d vect_scaled = vect_to_scale_eig.normalized()*sys_twist.norm();
      vect_to_scale.vector.x = 0.0;
      vect_to_scale.vector.y = 0.0;
      vect_to_scale.vector.z = 0.0;
    }
  }

  void TransformIntentQuantities::ConvertHeadPoseToVector(const geometry_msgs::PoseStamped& head_pose_inpt, geometry_msgs::Vector3Stamped& head_vect_outpt)
  {
    //turn posestamped into vector3stamped
    head_vect_outpt.header = head_pose_inpt.header;
    //only x,y pose in world frame is required
    Eigen::Vector3d head_x_local(1,0,0);
    //Convert pose to transform
    geometry_msgs::TransformStamped head_pose_transform;
    std::string head_frame = "head_origin";
    PoseToTransformStamped(head_pose_inpt, head_pose_transform, head_frame);
    //Need to get head pose to pass as child frame id
    Eigen::Affine3d head_transform = tf2::transformToEigen(head_pose_transform);
    Eigen::Affine3d head_transform_z_planer;
    ExtractZframeRotation(head_transform, head_transform_z_planer); //make head planar (x points out, y in plane, and z axis up)
    Eigen::Vector3d head_x_world = head_transform_z_planer.rotation()*head_x_local;
    head_vect_outpt.vector.x = head_x_world[0];
    head_vect_outpt.vector.y = head_x_world[1];
    head_vect_outpt.vector.z = head_x_world[2];
  }


  void TransformIntentQuantities::PoseToTransformStamped(const geometry_msgs::PoseStamped& inpt_pose, geometry_msgs::TransformStamped& outpt_transform, const std::string& child_frame_id)
  {
    outpt_transform.header = inpt_pose.header;
    outpt_transform.child_frame_id = child_frame_id; //dafault value populates if not specified
    outpt_transform.transform.translation.x = inpt_pose.pose.position.x;
    outpt_transform.transform.translation.y = inpt_pose.pose.position.y;
    outpt_transform.transform.translation.z = inpt_pose.pose.position.z;
    outpt_transform.transform.rotation.x = inpt_pose.pose.orientation.x;
    outpt_transform.transform.rotation.y = inpt_pose.pose.orientation.y;
    outpt_transform.transform.rotation.z = inpt_pose.pose.orientation.z;
    outpt_transform.transform.rotation.w = inpt_pose.pose.orientation.w;
  }


}

