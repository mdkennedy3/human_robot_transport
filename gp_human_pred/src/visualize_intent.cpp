#include<gp_human_pred/visualize_intent.h>

VisualizeHumanIntentClass::VisualizeHumanIntentClass()
{}

VisualizeHumanIntentClass::~VisualizeHumanIntentClass()
{}


void VisualizeHumanIntentClass::VisualizeSystemPoseHeadStepVectors(const geometry_msgs::TransformStamped&  system_pose,const geometry_msgs::Vector3Stamped& local_step_vect,const geometry_msgs::Vector3Stamped local_head_vect, const geometry_msgs::TwistStamped local_twist, ros::Publisher& head_step_pose_vect_publisher)
{
  //Generate markers and array
  visualization_msgs::Marker sys_pose_marker;
  visualization_msgs::Marker sys_pose_marker_text;
  int sys_pose_marker_id = 0;
  visualization_msgs::Marker head_marker;
  visualization_msgs::Marker head_marker_text;
  int head_marker_id = 1;
  visualization_msgs::Marker step_marker;
  visualization_msgs::Marker step_marker_text;
  int step_marker_id = 2;
  visualization_msgs::Marker twist_marker;
  visualization_msgs::Marker twist_marker_text;
  int twist_marker_id = 3;
  visualization_msgs::MarkerArray marker_array;
  std::vector<visualization_msgs::Marker> marker_list;
  //system pose
  Eigen::Affine3d system_pose_eig = tf2::transformToEigen(system_pose);
  Eigen::Vector3d x_axis(1,0,0);
  Eigen::Vector3d x_axis_sys = system_pose_eig.rotation()*x_axis;
  geometry_msgs::Vector3 sys_pose_vect_base;
  sys_pose_vect_base.x = system_pose.transform.translation.x;
  sys_pose_vect_base.y = system_pose.transform.translation.y;
  sys_pose_vect_base.z = system_pose.transform.translation.z;
  geometry_msgs::Vector3 sys_pose_vect;
  sys_pose_vect.x = x_axis_sys[0];
  sys_pose_vect.y = x_axis_sys[1];
  sys_pose_vect.z = x_axis_sys[2];
  float red = 1.0;  float blue = 0.0;  float green = 0.0; 
  std::string marker_text_pose = "pose";
  VisualizationVectorMarkerGenerator(sys_pose_marker, system_pose.header,sys_pose_marker_id,sys_pose_vect_base,sys_pose_vect,marker_text_pose,sys_pose_marker_text,red,blue,green);
  //Step 
  geometry_msgs::Vector3 local_frame_zero;
  float red_s = 0.0;  float blue_s = 1.0;  float green_s = 0.0; 
  std::string marker_text_step = "step";
  VisualizationVectorMarkerGenerator(step_marker, local_step_vect.header,head_marker_id,local_frame_zero,local_step_vect.vector,marker_text_step,head_marker_text,red_s,blue_s,green_s);
  //Head
  float red_h = 0.0; float blue_h = 0.0; float green_h = 1.0; 
  std::string marker_text_head = "head";
  VisualizationVectorMarkerGenerator(head_marker, local_head_vect.header,step_marker_id,local_frame_zero,local_head_vect.vector,marker_text_head,step_marker_text,red_h,blue_h,green_h);
  //Twist (xy)
  float red_t = 0.0; float blue_t = 1.0; float green_t = 1.0; 
  std::string marker_text_twist = "twist";
  geometry_msgs::Vector3 twist_vect;
  twist_vect.x = local_twist.twist.linear.x;
  twist_vect.y = local_twist.twist.linear.y;
  twist_vect.z = local_twist.twist.linear.z;
  VisualizationVectorMarkerGenerator(twist_marker, local_twist.header,twist_marker_id,local_frame_zero,twist_vect,marker_text_twist,twist_marker_text,red_t,blue_t,green_t);
  //publish
  marker_list.push_back(sys_pose_marker);
  marker_list.push_back(sys_pose_marker_text);
  marker_list.push_back(head_marker);
  marker_list.push_back(head_marker_text);
  marker_list.push_back(step_marker);
  marker_list.push_back(step_marker_text);
  marker_list.push_back(twist_marker);
  marker_list.push_back(twist_marker_text);
  marker_array.markers = marker_list;
  head_step_pose_vect_publisher.publish(marker_array);
}

void VisualizeHumanIntentClass::VisualizationVectorMarkerGenerator(visualization_msgs::Marker& marker,const std_msgs::Header& header, int& marker_id,const geometry_msgs::Vector3& vector_base,const geometry_msgs::Vector3& vector,const std::string& marker_text, visualization_msgs::Marker& marker_text_marker, float red, float blue, float green)
{
  marker.header = header;
  // marker.ns = "my_namespace";
  marker.id = marker_id;
  marker.type = visualization_msgs::Marker::ARROW;
  marker.action = visualization_msgs::Marker::ADD;
  geometry_msgs::Point base_point;
  base_point.x = vector_base.x;
  base_point.y = vector_base.y;
  base_point.z = vector_base.z;
  geometry_msgs::Point tip_point;
  tip_point.x = vector_base.x + vector.x;
  tip_point.y = vector_base.y + vector.y;
  tip_point.z = vector_base.z + vector.z;
  marker.points.push_back(base_point);
  marker.points.push_back(tip_point);
  marker.scale.x = 0.03;
  marker.scale.y = 0.05;
  // marker.scale.z = 0.1;
  marker.color.a = 1.0; // Don't forget to set the alpha!
  marker.color.r = red;
  marker.color.g = green;
  marker.color.b = blue;
  //now for marker text:
  marker_text_marker.header = header;//same position
  marker_text_marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  marker_text_marker.id = (int) marker_id + 10;
  marker_text_marker.pose.position.x = tip_point.x;
  marker_text_marker.pose.position.y = tip_point.y;
  marker_text_marker.pose.position.z = tip_point.z;
  marker_text_marker.color.a = 1.0;
  marker_text_marker.color.r = red;
  marker_text_marker.color.g = green;
  marker_text_marker.color.b = blue;
  marker_text_marker.scale.z = 0.1;
  marker_text_marker.text = marker_text;
}







void VisualizeHumanIntentClass::Publish_Viable_Person_Position(const people_msgs::PositionMeasurement& viable_personi, ros::Publisher& viable_person_step_visual, people_msgs::PositionMeasurement& viable_person)
{
  //Publish the viable person position (convert from person msg to marker type)
  visualization_msgs::Marker marker;
  marker.header = viable_person.header;
  marker.id = std::atoi(viable_person.object_id.c_str()) ;// std::stoi(viable_person.object_id);
  marker.type=2;
  marker.pose.position.x = viable_person.pos.x;
  marker.pose.position.y = viable_person.pos.y;
  marker.pose.position.z = viable_person.pos.z;
  marker.scale.x = 0.20;
  marker.scale.y = 0.20;
  marker.scale.z = 0.20;
  marker.color.a = 1.0; //marker transparency
  marker.color.r = 0.0;
  marker.color.g = 0.0;
  marker.color.b = 1.0;
  marker.lifetime = ros::Duration(5);
  //Now publish the marker
  viable_person_step_visual.publish(marker);
}


