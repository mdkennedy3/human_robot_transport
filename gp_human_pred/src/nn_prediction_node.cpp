/*
Discription:
This script listens for the topic for intent quantities in body fixed frame, then uses tensorflow service 
to query pre-trained nets to estimate the expected pose on the given time horizon

Created by: Monroe Kennedy III
All rights reserved
*/
#include <gp_human_pred/basic_ros_include.h>
#include <gp_human_pred/extract_intent_features_body_frame.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <intelligent_coop_carry_msgs/IntentQuantitiesLocal.h>
#include <intelligent_coop_carry_msgs/nn_intention_query.h>

class NNPredictionNodeClass
{
  public:
    NNPredictionNodeClass(ros::NodeHandle &nh);
    ~NNPredictionNodeClass();
    void QueryNNService(const intelligent_coop_carry_msgs::IntentQuantitiesLocal &msg);
    void PubPoseOdom(const geometry_msgs::PoseStamped& future_pose);
    ros::ServiceClient nn_query_sc;
    ros::Subscriber human_intent_local_sub;
    ros::Publisher pred_pose_pub;
    ros::Publisher pred_pose_odom_pub;
  private:
    ros::NodeHandle pnh_;
};

NNPredictionNodeClass::NNPredictionNodeClass(ros::NodeHandle &nh) : pnh_(nh)
{
  //constructor
   human_intent_local_sub = pnh_.subscribe("local_intent_quantities",1,&NNPredictionNodeClass::QueryNNService, this);
  
   nn_query_sc = pnh_.serviceClient<intelligent_coop_carry_msgs::nn_intention_query>("nn_query_service");

   pred_pose_pub = pnh_.advertise<geometry_msgs::PoseStamped>("nn_predicted_pose",1);
   pred_pose_odom_pub = pnh_.advertise<nav_msgs::Odometry>("nn_predicted_pose_odom",1);
   
}

NNPredictionNodeClass::~NNPredictionNodeClass()
{}

void NNPredictionNodeClass::QueryNNService(const intelligent_coop_carry_msgs::IntentQuantitiesLocal &msg)
{
  //This function calls the service and receives the predicted pose in body fixed frame, then publishes this (visual and msg)
  //call service
  intelligent_coop_carry_msgs::nn_intention_query req;
  req.request.human_intent = msg;
  if (nn_query_sc.call(req))
    {
      ROS_INFO("\nnn_query_sc callback successful\n");
      geometry_msgs::PoseStamped resp = req.response.future_pose_local;
      PubPoseOdom(resp);
      pred_pose_pub.publish(resp);
    }
  else
    {
      ROS_WARN("nn_query_sc callback NOT successful");
    }

}

void NNPredictionNodeClass::PubPoseOdom(const geometry_msgs::PoseStamped& future_pose)
{
  nav_msgs::Odometry pose_odom;
  pose_odom.header = future_pose.header;
  pose_odom.child_frame_id = "future_pose";
  pose_odom.pose.pose = future_pose.pose;
  pred_pose_odom_pub.publish(pose_odom);
}


int main(int argc, char **argv)
{
  ros::init(argc,argv, "nn_prediction_node");
  ros::NodeHandle nh("~");
  ros::service::waitForService("nn_query_service");
  NNPredictionNodeClass nn_pred_node(nh);
  ros::spin();
  return 0;
}
