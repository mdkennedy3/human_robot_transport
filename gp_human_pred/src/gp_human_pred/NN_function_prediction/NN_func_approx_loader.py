#Toy example by Steven Chen & Monroe Kennedy
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import argparse
import ast
import functools
import sys
#Import Tensor flow
import tensorflow as tf
import pdb #for debugging: #pdb.set_trace() (n to next line)

import numpy as np

import matplotlib.pyplot as plt

class NNUser(object):

  def load_model(self,path=None):
    # Load graph from directory used by tf.saved_model.simple_save.
    self.session = tf.Session()
    meta_graph_def = tf.saved_model.loader.load(self.session,
                                                [tf.saved_model.tag_constants.SERVING],
                                                path)

    # Collect input and output tensors from signature.
    signature_key = tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY
    sig = meta_graph_def.signature_def[signature_key]
    self.inputs = dict()
    self.outputs = dict()
    for key in sig.inputs:
      self.inputs[key] = self.session.graph.get_tensor_by_name(sig.inputs[key].name)
    for key in sig.outputs:
      self.outputs[key] = self.session.graph.get_tensor_by_name(sig.outputs[key].name)




  def test_NN_prediction(self, xmin=0, xmax=2*np.pi, steps=100):
    self.x_test = np.expand_dims(np.linspace(xmin, xmax,steps), axis=1)
    self.y_truth = self.output_function(self.x_test)
    self.y_pred = self.session.run(fetches = self.outputs["y"], feed_dict={self.inputs["x"]:self.x_test})

  def output_function(self,x):
    #choose the function you wish to approximate y = f(x)
    # y = 4*np.sin(x) + 3*np.cos(x)
    a,b,c,d,e = .1,.1,10,1,1 
    #y = a*x**5 + b*x**4 + c*x**3 + d*x**2 + e*x**1 
    #y = np.exp(np.cos(x)) + 4*np.sin(x)
    #y = np.exp(x) 
    #y = np.cosh(x)
    y = np.sin(3*x)
    return y


  def plot_1d_output(self):
    fig = plt.figure()
    ax = fig.gca()
    #pdb.set_trace() #to debug
    ax.scatter(self.x_test[:,0],self.y_truth[:,0], label='train')
    ax.scatter(self.x_test[:,0],self.y_pred[:,0], label='test')
    ax.legend()
    plt.show()


def main():
  #Define the optimizer
  cls_obj = NNUser()
  #load the model
  model_save_path = "/tmp/nn_saver_test"
  export_dir = model_save_path + "/full_model"
  cls_obj.load_model(path=export_dir)
  #Test the prediction
  cls_obj.test_NN_prediction()
  #Print the estimated function
  cls_obj.plot_1d_output()

if __name__ == '__main__':
  main()