#Toy example by Steven Chen & Monroe Kennedy
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import argparse
import ast
import functools
import sys
#Import Tensor flow
import tensorflow as tf
import pdb #for debugging: #pdb.set_trace() (n to next line)

import numpy as np

import matplotlib.pyplot as plt

class NN_training_examples(object):
  """docstring for NN_training_examples"""
  def __init__(self, learning_rate = 0.001):
    self.opt = tf.train.RMSPropOptimizer(learning_rate=learning_rate) #learning rate for gradient descent

  def init_layers(self, input_size=1, output_size=1):
    # Define the tensorflow computation graph
    self.inpt = tf.placeholder(tf.float64, (None, input_size), name="input")  #None as first argument allows for variable input size
    self.layer1a = tf.layers.dense(self.inpt, 5, activation=None, name="layer1a")
    self.layer1a = tf.sin(self.layer1a) #explicitly define the activation function
    self.layer1b = tf.layers.dense(self.inpt, 5, activation=None, name="layer1b")
    self.layer1b = tf.nn.relu(self.layer1b)
    self.layer1 = tf.concat([self.layer1a, self.layer1b], axis=1) # stack the layers vertically 
    #for CNN: layer1 = tf.reshape(layer1, [-1,H*W*C]) (for height, width, channel) then concat
    #second layer:
    self.layer2a = tf.layers.dense(self.layer1, 5, activation=None, name="layer2a")
    self.layer2a = tf.cos(self.layer2a)
    self.layer2b = tf.layers.dense(self.layer1, 5, activation=None, name="layer2b")
    self.layer2b = tf.nn.relu(self.layer2b)
    self.layer2 = tf.concat([self.layer2a, self.layer2b], axis=1)
    #output and label layer
    self.output = tf.layers.dense(self.layer2, output_size, activation=None, name="output")
    self.label = tf.placeholder(tf.float64, (None, output_size), name="label")

  def simple_init_layers(self,input_size=1, output_size=1):
    #This is a simple 2 layer NN
    # Define the tensorflow computation graph
    self.inpt = tf.placeholder(tf.float64, (None, 1), name="input") #None as first argument allows for variable input size
    self.layer1 = tf.layers.dense(self.inpt, 5, activation=None, name="layer1") #activation None allows for explicit definition
    layer1 = tf.nn.relu(self.layer1)
    # self.layer1 = tf.sin(self.layer1)
    self.layer2 = tf.layers.dense(self.layer1, 5, activation=None, name="layer2")
    layer2 = tf.nn.relu(self.layer2)
    # self.layer2 = tf.cos(self.layer2)
    self.output = tf.layers.dense(layer1, 1, activation=None, name="output")


  def define_error(self):
    self.error = tf.losses.mean_squared_error(self.label, self.output)
    # Define the training op
    self.train_op = self.opt.minimize(self.error)

  def train_NN(self, training_iterations = 100000, sample_batch_size=32):
    #start session:
    # Tensorflow session
    self.session = tf.Session()
    self.nn_saver = tf.train.Saver()
    # Run intializer for the variables
    self.session.run(tf.global_variables_initializer())
    # Begin training
    # x_train = [] #For statistics analysis
    for i in range(training_iterations):
      x,y = self.generate_example(sample_batch_size)
      # x_train.append(x)
      x = np.expand_dims(x, axis=1)
      y = np.expand_dims(y, axis=1)
      error_out, _ = self.session.run([self.error, self.train_op], feed_dict={self.inpt: x, self.label:y})
      if i%100 == 0:
        print(error_out)

    model_save_path = "/tmp/nn_saver_test"
    str_path = self.nn_saver.save(self.session,model_save_path)
    export_dir = model_save_path + "/full_model"
    loop_bool = True
    counter = 1
    while loop_bool:
      try:
        tf.saved_model.simple_save(self.session,export_dir,inputs={"x":self.inpt}, outputs={"y":self.output})
        loop_bool = False
      except:
        export_dir += "_temp"
        if counter > 3: 
          loop_bool = False
          print("unable to save file")



    print("nn_saver model saved in path: %s" % str_path)
    print("Model saved in path: %s" % str_path)



  def test_NN_prediction(self, xmin=0, xmax=2*np.pi, steps=100):
    self.x_test = np.expand_dims(np.linspace(xmin, xmax,steps), axis=1)
    self.y_truth = self.output_function(self.x_test)
    self.y_pred = self.session.run(self.output, feed_dict={self.inpt:self.x_test})


  def plot_1d_output(self):
    fig = plt.figure()
    ax = fig.gca()
    #pdb.set_trace() #to debug
    ax.scatter(self.x_test[:,0],self.y_truth[:,0], label='train')
    ax.scatter(self.x_test[:,0],self.y_pred[:,0], label='test')
    ax.legend()
    plt.show()


  def output_function(self,x):
    #choose the function you wish to approximate y = f(x)
    # y = 4*np.sin(x) + 3*np.cos(x)
    a,b,c,d,e = .1,.1,10,1,1 
    #y = a*x**5 + b*x**4 + c*x**3 + d*x**2 + e*x**1 
    #y = np.exp(np.cos(x)) + 4*np.sin(x)
    #y = np.exp(x) 
    #y = np.cosh(x)
    y = np.sin(3*x)
    return y

  def generate_example(self,batch_size):
    x = 2*np.pi*np.random.rand(batch_size)
    # y = 4*np.sin(x) + 3*np.cos(x) 
    y = self.output_function(x)
    return x,y


def main():
  #Define the optimizer
  cls_obj = NN_training_examples(learning_rate = 0.001)
  #Initalize layers
  cls_obj.init_layers()
  #Define the error and optimizer functions
  cls_obj.define_error() #Defines error and optimizer
  #train the NN (start session and train)
  cls_obj.train_NN(training_iterations=int(1e5), sample_batch_size=32)
  #Test the prediction
  cls_obj.test_NN_prediction()
  #Print the estimated function
  cls_obj.plot_1d_output()


if __name__ == '__main__':
  main()






