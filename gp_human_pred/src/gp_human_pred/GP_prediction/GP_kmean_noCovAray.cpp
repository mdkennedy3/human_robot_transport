#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Point32.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <Eigen/Dense>
#include "kmeans.cpp"
using namespace Eigen;
using namespace std;

int main( int argc, char** argv )
{
  ros::init(argc, argv, "points_and_lines");
  ros::NodeHandle n;
  ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 10);

  ros::Publisher sensor_pub = n.advertise<sensor_msgs::PointCloud>("sensor_marker", 10);

  ros::Publisher sensor_pub2 = n.advertise<sensor_msgs::PointCloud>("sensor_marker2", 10);

  ros::Publisher sensor_pub3 = n.advertise<sensor_msgs::PointCloud>("sensor_marker3", 10);

  ros::Publisher sensor_pub4 = n.advertise<sensor_msgs::PointCloud>("cluster2", 10);

  ros::Publisher sensor_pub5 = n.advertise<sensor_msgs::PointCloud>("cluster3", 10);

  ros::Publisher sensor_pub6 = n.advertise<sensor_msgs::PointCloud>("cluster4", 10);

  ros::Publisher sensor_pub7 = n.advertise<sensor_msgs::PointCloud>("cluster1", 10);

  ros::Publisher sensor_pub8 = n.advertise<sensor_msgs::PointCloud>("allClusts", 10);


  ros::Rate r(30);

    srand(time(NULL));

    int K = 30; // num clusters
    int currClust = rand() % K; // cluster you want to focus on var

    sensor_msgs::PointCloud pc;
    pc.header.frame_id = "/my_frame";
    pc.header.stamp = ros::Time::now();

    sensor_msgs::PointCloud pc2;
    pc2.header.frame_id = "/my_frame";
    pc2.header.stamp = ros::Time::now();

    //cluster 1
    sensor_msgs::PointCloud pc3;
    pc3.header.frame_id = "/my_frame";
    pc3.header.stamp = ros::Time::now();

    //2
    sensor_msgs::PointCloud pc4;
    pc4.header.frame_id = "/my_frame";
    pc4.header.stamp = ros::Time::now();

    //3
    sensor_msgs::PointCloud pc5;
    pc5.header.frame_id = "/my_frame";
    pc5.header.stamp = ros::Time::now();

    //4
    sensor_msgs::PointCloud pc6;
    pc6.header.frame_id = "/my_frame";
    pc6.header.stamp = ros::Time::now();

    //1
    sensor_msgs::PointCloud pc7;
    pc7.header.frame_id = "/my_frame";
    pc7.header.stamp = ros::Time::now();

    //All clusters
    sensor_msgs::PointCloud pc8;
    pc8.header.frame_id = "/my_frame";
    pc8.header.stamp = ros::Time::now();



    vector<Point> pointsV;

    int num = 100; // num for test data (num x num)
    int npoints = 20; //num for training data (npoints x npoints)
    int x_size = 10;
    int y_size = 10;

    int falseInd = 0; //ignore?

    //TEST DATA CREATION
    for (uint32_t i = 0; i < num; ++i)
    {
      for (uint32_t j = 0; j < num; ++j)
      {
        float x = -10 + 20 * (num - i) / float(num);
        float y = -10 + 20 * (num - j) / float(num);
        float z = 0.4 * sin(x)+cos(y);

        geometry_msgs::Point32 pt1;
        pt1.x = x;
        pt1.y = y;
        pt1.z = z;
        pc.points.push_back(pt1);

        vector<double> vals1;
        vals1.push_back(x);
        vals1.push_back(y);
        vals1.push_back(z);
        Point poi1 = Point(0,vals1);
      }
    }

    //training vars
    int tpCount[K]; //count of training points in a cluster
    vector< vector<Point> > tpPts;
    vector <Point> row;
    for (int i = 0; i < K; i++) { 
        tpPts.push_back(row);
    }

    int nextInd = 0;
    
    //training points creation
    for (uint32_t i = 0; i < npoints; ++i)
    {
      for (uint32_t j = 0; j < npoints; ++j)
      {
        int xop = rand() % 200;
        float x = -10 + (xop / float(10));
        int yop = rand() % 200;
        float y = -10 + (yop / float(10));
        float z = 0.4 * sin(x)+cos(y);

        geometry_msgs::Point32 pt1;
        pt1.x = x;
        pt1.y = y;
        pt1.z = z;
        pc3.points.push_back(pt1);

        vector<double> vals2;
        vals2.push_back(x);
        vals2.push_back(y);
        vals2.push_back(z);
        Point poi2 = Point(0,vals2);
        pointsV.push_back(poi2);

        pt1.z += 5;
      }
    }

    int total_points = npoints * npoints;
    int total_values = 3;
    int max_iterations = 10;

    KMeans kmeans(K, total_points, total_values, max_iterations);
    kmeans.run(pointsV);

    KMeans * kmeansX = &kmeans;
    int tpClustSize = 0;
    int clustSize = kmeans.clusters[currClust].getTotalPoints();

    int clustSizeT = clustSize; //IGNORE

    MatrixXd mTXpm(2,clustSize);
    MatrixXd mTYpm(1,clustSize);

    for (uint32_t i = 0; i < clustSize; i++)
    {
        mTXpm(0,i) = kmeansX->clusters[currClust].getPoint(i).getValue(0);
        mTXpm(1,i) = kmeansX->clusters[currClust].getPoint(i).getValue(1);
        mTYpm(0,i) = kmeansX->clusters[currClust].getPoint(i).getValue(2);
    }

    tpClustSize = tpPts.at(currClust).size();
    int tpClustSizeT = tpClustSize;
    MatrixXd mTXq(2,tpClustSize);
    MatrixXd mTYq(1,tpClustSize);


    float meanX[K];
    float meanY[K];

    for (uint32_t i = 0; i < K; i++)
    {
        meanX[i] = 0;
        meanY[i] = 0;

        for (uint32_t j = 0; j < kmeansX->clusters[i].getTotalPoints(); j++)
        {
            meanX[i] += kmeansX->clusters[i].getPoint(j).getValue(0);
            if (j == kmeansX->clusters[i].getTotalPoints() - 1) {
                meanX[i] /= (kmeansX->clusters[i].getTotalPoints() * 1.0);
            }
            meanY[i] += kmeansX->clusters[i].getPoint(j).getValue(1);
            if (j == kmeansX->clusters[i].getTotalPoints() - 1) {
                meanY[i] /= (kmeansX->clusters[i].getTotalPoints() * 1.0);
            }
        }
    }


    //Covariance PART
    vector <MatrixXd> covMas;

    for (int i = 0; i < K; i++) {
        MatrixXd GXKi(2,1);
        MatrixXd GXKm(2,1);
        MatrixXd GXK(1,1);
        MatrixXd covForm(kmeansX->clusters[i].getTotalPoints(),kmeansX->clusters[i].getTotalPoints());
        for (int j = 0; j < kmeansX->clusters[i].getTotalPoints(); j++) {
                GXKm(0,0) = meanX[i];
                GXKm(1,0) = meanY[i];
                GXKi(0,0) = kmeansX->clusters[i].getPoint(j).getValue(0);
                GXKi(1,0) = kmeansX->clusters[i].getPoint(j).getValue(1);
                GXK = (GXKi - GXKm)*(GXKi - GXKm).transpose();
                covMas.push_back(GXK);
        }
    }

    MatrixXd covTrue(2,2);
    covTrue(0,0) = 0;
    covTrue(0,1) = 0;
    covTrue(1,0) = 0;
    covTrue(1,1) = 0;
    for (int i = 0; i < covMas.size(); i++) {
        covTrue += covMas.at(i);
    }
    covTrue /= covMas.size();

    for (uint32_t i = 0; i < pc.points.size(); i++)
    {
            vector<double> vals1;
            vals1.push_back(pc.points[i].x);
            vals1.push_back(pc.points[i].y);
            vals1.push_back(pc.points[i].z);
            Point poi1 = Point(0,vals1);

            float SiHold = 0;
            float Si = 0;
            int clustCh = 0;
            MatrixXd GXKi(2,1);
            MatrixXd GXKm(2,1);
            MatrixXd GXTbT(2,2);
            MatrixXd GXObO(1,1);
            MatrixXd GXObOT(1,1);
            GXTbT(0,0) = rand() % 5;
            GXTbT(1,0) = rand() % 5;
            GXTbT(0,1) = rand() % 5;
            GXTbT(1,1) = rand() % 5;
            for (int io = 0; io < K; io++) {
                MatrixXd GXK(1,1);
                MatrixXd covForm(kmeansX->clusters[io].getTotalPoints(),kmeansX->clusters[io].getTotalPoints());
                        GXKm(0,0) = meanX[io];
                        GXKm(1,0) = meanY[io];
                        GXKi(0,0) = pc.points[i].x;
                        GXKi(1,0) = pc.points[i].y;
                        covForm = covMas.at(io);
                        GXObO(0,0) = 1;
                        GXK = (GXKi - GXKm).transpose()*(covTrue.inverse())*(GXKi - GXKm);
                SiHold = exp(-0.5*GXK(0,0)) / sqrt((2*M_PI)*covTrue.determinant());
                if (SiHold > Si) {
                    Si = SiHold;
                    clustCh = io;
                }
            }


            int cT = clustCh;
            geometry_msgs::Point32 pt1;
            pt1.x = pc.points[i].x;
            pt1.y = pc.points[i].y;
            float Krep = K * 1.0;
            pt1.z = cT / (Krep / 2.0) - 5;
            pc8.points.push_back(pt1);
    }

  float f = 0.0;
  while (ros::ok())
  {
    geometry_msgs::Point32 ptx;

    MatrixXd Krs(tpClustSize,clustSize);
    MatrixXd Kss(clustSize,clustSize);
    MatrixXd Krr(tpClustSize,tpClustSize);

    MatrixXd EXKq(2,1);
    MatrixXd EXKpm(2,1);
    MatrixXd EXKq2(2,1);
    MatrixXd EXKpm2(2,1);
    MatrixXd EXK(1,1);

    for (int i = 0; i < tpClustSize; i++) {
      for (int j = 0; j < clustSize; j++) {
            EXKq(0,0) = mTXq(0,i);
            EXKq(1,0) = mTXq(1,i);
            EXKpm(0,0) = mTXpm(0,j);
            EXKpm(1,0) = mTXpm(1,j);
            EXK = (EXKq - EXKpm).transpose()*(EXKq - EXKpm);
            Krs(i,j) = exp(-0.5*(EXK(0,0)));
      } 
    }

    for (int i = 0; i < clustSize; i++) {
      for (int j = 0; j < clustSize; j++) {
            EXKpm(0,0) = mTXpm(0,i);
            EXKpm(1,0) = mTXpm(1,i);
            EXKpm2(0,0) = mTXpm(0,j);
            EXKpm2(1,0) = mTXpm(1,j);
            EXK = (EXKpm - EXKpm2).transpose()*(EXKpm - EXKpm2);
            Kss(i,j) = exp(-0.5*(EXK(0,0)));
      } 
    }

    for (int i = 0; i < tpClustSize; i++) {
      for (int j = 0; j < tpClustSize; j++) {
            EXKq(0,0) = mTXq(0,i);
            EXKq(1,0) = mTXq(1,i);
            EXKq2(0,0) = mTXq(0,j);
            EXKq2(1,0) = mTXq(1,j);
            EXK = (EXKq - EXKq2).transpose()*(EXKq - EXKq2);
            Krr(i,j) = exp(-0.5*(EXK(0,0)));
      } 
    }

    MatrixXd mId(tpClustSize,tpClustSize);
    MatrixXd mId2(tpClustSize,tpClustSize);
    MatrixXd invTm(tpClustSize,tpClustSize);
    MatrixXd invTmT(tpClustSize,tpClustSize);
    mId = mId.setIdentity();
    mId2 = (0.1*0.1)*mId;
    invTm = Krr + mId2;
    invTmT = invTm.inverse();

    MatrixXd V(clustSize,clustSize);
    V = Kss - Krs.transpose()*(Krr + mId2).inverse()*Krs;

    MatrixXd Vtt(1,clustSize);
    for (int i = 0; i < clustSize; i++)
    {
      Vtt(0,i) = V(i,i);
    }

    //test

    sensor_pub.publish(pc);

    //training
    sensor_pub3.publish(pc3);
    //cluster 1
    sensor_pub7.publish(pc7);
    //2
    sensor_pub4.publish(pc4);
    //3
    sensor_pub5.publish(pc5);
    //4
    sensor_pub6.publish(pc6);
    
    sensor_pub8.publish(pc8);
    
    MatrixXd upper(clustSize,clustSize);
    MatrixXd lower(clustSize,clustSize);
    for (int i = 0; i < clustSize; i++)
    {
        upper(0,i) = mTYpm(0,i) + Vtt(0,i);
        lower(0,i) = mTYpm(0,i) - Vtt(0,i);
    }

    for (uint32_t i = 0; i < clustSize; ++i)
    {
        geometry_msgs::Point32 pt1;
        pt1.x = mTXpm(0,i);
        pt1.y = mTXpm(1,i);
        pt1.z = upper(0,i);
        pc2.points.push_back(pt1);
    }
    //covariance for current cluster
    sensor_pub2.publish(pc2);

    for (uint32_t i = 0; i < clustSize; ++i)
    {
        geometry_msgs::Point32 pt1;
        pt1.x = mTXpm(0,i);
        pt1.y = mTXpm(1,i);
        pt1.z = lower(0,i);
        pc2.points.push_back(pt1);
    }
    //covariance for current cluster
    sensor_pub2.publish(pc2);

    r.sleep();

    f += 0.04;
  }
}