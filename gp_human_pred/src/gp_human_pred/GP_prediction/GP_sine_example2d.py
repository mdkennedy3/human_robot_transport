#!/usr/bin/env python
import numpy as np
import math
import matplotlib.pyplot as plt #for plotting just like in matlab

class GPExample(object):
  """docstring for GPExample
    This is a simple script that demos the abilities of the GP on a simple 2D example of a sine curve  
  """
  def __init__(self, a=1,b=1,c=0,d=0, num_training_pts=20, training_data_span=[0,10]):
    self.x_train = np.linspace(training_data_span[0], training_data_span[1], num_training_pts)
    self.y_train = a*np.sin(b*self.x_train + c ) + d
    #turn these into matricies
    self.x_train = np.matrix(self.x_train).T  #these were lists, defaults to row vector, make coloumn
    self.y_train = np.matrix(self.y_train).T

    self.K_train = None  #covariance matrix for training data
    self.alpha_vect = None  #decision vector soley based on training data
    self.sigma = 1e-1  #this is to help when covariance is very small with inversion

  def kernel_function(self,xp=0,xq=0):
    #defines Gaussian kernel function: k(xp,xq) = exp(-(0.5)(xp-xq).T(xp-xq))
    #first check that size and dim are the same else print error and return 0
    kern = 0 #default kernel value
    if type(xp) != type(xq):
      #this is the case when you want to get a kernel vector or non-square type, but make them both the same type (e.g matrix)
      print "types are wrong"
    if type(xp) == np.matrixlib.defmatrix.matrix and type(xq) == np.matrixlib.defmatrix.matrix:
      if np.min(xp.shape) == 1 and np.min(xq.shape) == 1:
        #make sure they have the same orientation
        if xp.shape[0] == 1: xp = xp.T
        if xq.shape[0] == 1: xq = xq.T
        #calculate the kernel
        kern = np.zeros([np.max(xp.shape),np.max(xq.shape)])  #non-square kernel
        for idx in range(np.max(xp.shape)):
          for jdx in range(np.max(xq.shape)):
            kern[idx,jdx] = np.exp(-0.5*(xp.item(idx)-xq.item(jdx))**2)
      else:
        print "vectors (nx1) were not passed"
        #need a vector
    else:
      #this assumes xp, xq are both scalars
      kern = np.exp(-0.5*(xp-xq)**2)
    #Return the kernel
    kern = np.matrix(kern)
    return kern

  def obtain_K_train(self):
    #obtain the training covariance
    self.K_train = self.kernel_function(self.x_train, self.x_train) #this uses the kernel to establish positional relationship between the training data points
    #Check properties
    print "K", self.K_train
    print "xt", self.x_train
    #is it positive semi-definite?
    print "\n K positive semi def in det(K) > 0: ", np.linalg.det(self.K_train)
    #is it symmetric as the covariance must be?
    KtmK = self.K_train - self.K_train.T
    print "\n K is symmetric of K - K^T is a matrix of zeros, max min elements of K-K^T:", np.max(KtmK), np.min(KtmK)

  def obtain_alpha(self):
    #This is the decision vector: alpha = (K+s*I)^{-1} y_train
    K = self.K_train
    self.alpha_vect = np.linalg.inv(K + self.sigma**2*np.eye(np.max(K.shape)))*self.y_train
    print "alpha vect", self.alpha_vect

  def generate_test_points(self,num_test_pts=20,test_data_span=[0,9.5]):
    #generate test points, this can be reduced to a single test point if desired
    self.x_test = np.matrix(np.linspace(test_data_span[0], test_data_span[1], num_test_pts)).T

  def evaluate_test_points(self):
    #Evaluate the test points to find y_test, Ky_test (mean and variance with each test point)
    #1. find the kernel vector with the training data
    K_s = self.kernel_function(xp=self.x_train, xq=self.x_test)
    K_ss = self.kernel_function(xp=self.x_test, xq=self.x_test)
    #Check their dimensions
    print "\nK_s shape:", K_s.shape, "\nK_ss shape:", K_ss.shape
    print "\nKs, ", K_s, "\n K_ss", K_ss, ", K_ss det: ", np.linalg.det(K_ss)
    #2. Find f = Ks^T*(K+s*I)^{-1} y
    self.y_test = K_s.T * self.alpha_vect
    print "y_test", self.y_test
    #3. Find V[f] (covariance): Kss - Ks^T (K + s*I)^{-1} Ks
    K = self.K_train
    print "\nKss shape", K_ss.shape, " Ks shape", K_s.shape, "\ndet Kss", np.linalg.det(K_ss)
    self.cov_test = K_ss - K_s.T*np.linalg.inv(K + self.sigma*np.eye(np.max(K.shape)))*K_s
    #This too should have expected covariance properties
    cov_sym = self.cov_test - self.cov_test.T
    print "cov_test det:", np.linalg.det(self.cov_test), " V^T-V max/min values:", np.max(cov_sym), np.min(cov_sym)

  def display_GP_output(self):
    fig = plt.figure()
    ax = fig.gca()
    #Plot the training data
    train_data = ax.plot(self.x_train.T.tolist()[0], self.y_train.T.tolist()[0],label="training data")
    train_data_scatter = ax.scatter(self.x_train.T.tolist()[0], self.y_train.T.tolist()[0],label="training data scatter")
    #Plot the mean test data
    test_data_mean = ax.scatter(self.x_test.T.tolist()[0],self.y_test.T.tolist()[0],label="test data mean")
    #Plot the associated covariance with the test data, realize that the associated covariance with each point is the diagonal elements of V[f]
    Vtt = [self.cov_test[idx,idx] for idx in range(self.cov_test.shape[0])]
    x = self.x_test.T.tolist()[0]
    y = self.y_test.T.tolist()[0]
    upper = [y[idx]+Vtt[idx] for idx in range(len(y))]
    lower = [y[idx]-Vtt[idx] for idx in range(len(y))]
    # print "vtt", Vtt
    test_data_mean_cov = ax.fill_between(x,lower,upper, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
    ax.legend()
    plt.show()

def main():
  #Define some params
  #y = A*sin(Bx+C) + D
  a = 1; b = 1; c = 0; d = 0;
  num_training_pts = 30
  data_span = [0,10]  #x in [xs,xf]
  #generate class object
  cls_obj = GPExample(a=a,b=b,c=c,d=d, num_training_pts=num_training_pts, training_data_span=data_span)
  #Use training data to make covariance matrix
  cls_obj.obtain_K_train()
  #Then find the decision vector alpha
  cls_obj.obtain_alpha()
  #Generate the testpoints (feel free to update the default params)
  cls_obj.generate_test_points(num_test_pts=60)
  #Evaluate the test points (their mean and covariance w.r.t. the training data)
  cls_obj.evaluate_test_points()
  #Print the output
  cls_obj.display_GP_output()

if __name__ == '__main__':
  main()