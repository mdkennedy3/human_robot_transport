//Currently Uncommented 
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Point32.h>

#include <cmath>
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;

int main( int argc, char** argv )
{
  ros::init(argc, argv, "points_and_lines");
  ros::NodeHandle n;
  ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 10);

  ros::Publisher sensor_pub = n.advertise<sensor_msgs::PointCloud>("sensor_marker", 10);

  ros::Publisher sensor_pub2 = n.advertise<sensor_msgs::PointCloud>("sensor_marker2", 10);

  ros::Publisher sensor_pub3 = n.advertise<sensor_msgs::PointCloud>("sensor_marker3", 10);


  ros::Rate r(30);

  float f = 0.0;
  while (ros::ok())
  {


    sensor_msgs::PointCloud pc;
    pc.header.frame_id = "/my_frame";
    pc.header.stamp = ros::Time::now();


    sensor_msgs::PointCloud pc2;
    pc2.header.frame_id = "/my_frame";
    pc2.header.stamp = ros::Time::now();

    sensor_msgs::PointCloud pc3;
    pc3.header.frame_id = "/my_frame";
    pc3.header.stamp = ros::Time::now();


    int num = 100;
    int npoints = 10;
    int x_size = 10;
    int y_size = 10;

    float Xpm[num];
    float Ypm[num][num];
    float Zpm[num];
    float TXpm[2][num];
    float TYpm[num];
    MatrixXd mXpm(1,num);
    MatrixXd mYpm(1,num);
    MatrixXd mZpm(1,num);
    MatrixXd mTXpm(2,num);
    MatrixXd mTYpm(1,num);


    //PT 1
    // Create the vertices for the points and lines
    for (uint32_t i = 0; i < num; ++i)
    {
      //float y = 1 * sin(f + i / 100.0f * 2 * M_PI);
      //float z = 1 * cos(f + i / 100.0f * 2 * M_PI);
      for (uint32_t j = 0; j < num; ++j)
      {
        float x = -10 + 20 * (num - i) / float(num);
        float y = -10 + 20 * (num - j) / float(num);
        float z = 0.4 * sin(x)+cos(y);

        Xpm[i] = x;
        Ypm[i][j] = z;
        Zpm[i] = y;
        TXpm[0][i] = x;
        TXpm[1][i] = y;
        TYpm[i] = z;
        mXpm(0,i) = x;
        mYpm(0,i) = z;
        mZpm(0,i) = y;
        mTXpm(0,i) = x;
        mTXpm(1,i) = y;
        mTYpm(0,i) = z;

        geometry_msgs::Point32 pt1;
        pt1.x = x;
        pt1.y = y;
        pt1.z = z;
        pc.points.push_back(pt1);
      }
    }

    sensor_pub.publish(pc);

    float Xq[npoints];
    float TXq[2][npoints];
    float Yq[npoints];
    float TYq[npoints];
    float YqT[npoints][1];
    MatrixXd mXq(1,npoints);
    MatrixXd mYq(1,npoints);
    MatrixXd mTXq(2,npoints);
    MatrixXd mTYq(1,npoints);

    
    // Create the vertices for the points and lines
    for (uint32_t i = 0; i < npoints; ++i)
    {
      //float y = 1 * sin(f + i / 100.0f * 2 * M_PI);
      //float z = 1 * cos(f + i / 100.0f * 2 * M_PI);
      for (uint32_t j = 0; j < npoints; ++j)
      {
        float x = -10 + 20 * (npoints - i) / float(npoints);
        float y = -10 + 20 * (npoints - j) / float(npoints);
        float z = 0.4 * sin(x)+cos(y);

        Xq[i] = x;
        Yq[i] = z;
        Zpm[i] = y;
        TXq[0][i] = x;
        TXq[1][i] = y;
        TYq[i] = z;
        mXq(0,i) = x;
        mYq(0,i) = z;
        mZpm(0,i) = y;
        mTXq(0,i) = x;
        mTXq(1,i) = y;
        mTYq(0,i) = z;

        geometry_msgs::Point32 pt1;
        pt1.x = x;
        pt1.y = y;
        pt1.z = z;
        pc3.points.push_back(pt1);
      }
    }

    sensor_pub3.publish(pc3);

    MatrixXd Krs(npoints,num);
    MatrixXd Kss(num,num);
    MatrixXd Krr(npoints,npoints);

    MatrixXd EXKq(2,1);
    MatrixXd EXKpm(2,1);
    MatrixXd EXKq2(2,1);
    MatrixXd EXKpm2(2,1);
    MatrixXd EXK(1,1);

    /*Krs = ((mTXq - mTXpm).transpose()*(mTXq - mTXpm));
    Kss = ((mTXpm - mTXpm).transpose()*(mTXpm - mTXpm));
    Krr = ((mTXq - mTXq).transpose()*(mTXq - mTXq));*/

    for (int i = 0; i < npoints; i++) {
      for (int j = 0; j < num; j++) {
            EXKq(0,0) = mTXq(0,i);
            EXKq(1,0) = mTXq(1,i);
            EXKpm(0,0) = mTXpm(0,j);
            EXKpm(1,0) = mTXpm(1,j);
            EXK = (EXKq - EXKpm).transpose()*(EXKq - EXKpm);
            Krs(i,j) = exp(-0.5*(EXK(0,0)));
      } 
    }

    for (int i = 0; i < num; i++) {
      for (int j = 0; j < num; j++) {
            EXKpm(0,0) = mTXpm(0,i);
            EXKpm(1,0) = mTXpm(1,i);
            EXKpm2(0,0) = mTXpm(0,j);
            EXKpm2(1,0) = mTXpm(1,j);
            EXK = (EXKpm - EXKpm2).transpose()*(EXKpm - EXKpm2);
            Kss(i,j) = exp(-0.5*(EXK(0,0)));
      } 
    }

    for (int i = 0; i < npoints; i++) {
      for (int j = 0; j < npoints; j++) {
            EXKq(0,0) = mTXq(0,i);
            EXKq(1,0) = mTXq(1,i);
            EXKq2(0,0) = mTXq(0,j);
            EXKq2(1,0) = mTXq(1,j);
            EXK = (EXKq - EXKq2).transpose()*(EXKq - EXKq2);
            Krr(i,j) = exp(-0.5*(EXK(0,0)));
      } 
    }

    MatrixXd mId(npoints,npoints);
    MatrixXd mId2(npoints,npoints);
    MatrixXd invTm(npoints,npoints);
    MatrixXd invTmT(npoints,npoints);
    mId = mId.setIdentity();
    mId2 = (0.1*0.1)*mId;
    invTm = Krr + mId2;
    invTmT = invTm.inverse();

    MatrixXd V(num,num);
    V = Kss - Krs.transpose()*(Krr + mId2).inverse()*Krs;

    MatrixXd Vtt(1,num);
    for (int i = 0; i < num; i++)
    {
      Vtt(0,i) = V(i,i);
    }

    MatrixXd upper(num,num);
    MatrixXd lower(num,num);
    for (int i = 0; i < num; i++)
    {
      for (int j = 0; j < num; j++)
      {
        upper(i,j) = Ypm[i][j] + Vtt(0,i);
        lower(i,j) = Ypm[i][j] - Vtt(0,i);
      }
    }

    for (uint32_t i = 0; i < num; ++i)
    {
      //float y = 1 * sin(f + i / 100.0f * 2 * M_PI);
      //float z = 1 * cos(f + i / 100.0f * 2 * M_PI);
      for (uint32_t j = 0; j < num; ++j)
      {
        float x = -10 + 20 * (num - i) / float(num);
        float y = -10 + 20 * (num - j) / float(num);
        float z = 0.4 * sin(x)+cos(y);

        geometry_msgs::Point32 pt1;
        pt1.x = x;
        pt1.y = y;
        pt1.z = upper(i,j);
        pc2.points.push_back(pt1);
      }
    }

    sensor_pub2.publish(pc2);

    for (uint32_t i = 0; i < num; ++i)
    {
      //float y = 1 * sin(f + i / 100.0f * 2 * M_PI);
      //float z = 1 * cos(f + i / 100.0f * 2 * M_PI);
      for (uint32_t j = 0; j < num; ++j)
      {
        float x = -10 + 20 * (num - i) / float(num);
        float y = -10 + 20 * (num - j) / float(num);
        float z = 0.4 * sin(x)+cos(y);

        geometry_msgs::Point32 pt1;
        pt1.x = x;
        pt1.y = y;
        pt1.z = lower(i,j);
        pc2.points.push_back(pt1);
      }
    }

    sensor_pub2.publish(pc2);

    r.sleep();

    f += 0.04;
  }
}
