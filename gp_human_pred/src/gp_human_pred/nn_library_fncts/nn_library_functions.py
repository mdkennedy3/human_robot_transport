#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import argparse
import sys
#Import Tensor flow
import tensorflow as tf
import rospy
import pdb #for debugging: #pdb.set_trace() (n to next line, c to continue) :: https://docs.python.org/2/library/pdb.html
import numpy as np
import os
import rospkg
import rosbag
from intelligent_coop_carry_msgs.msg import IntentQuantitiesLocal
from geometry_msgs.msg import PoseStamped
import cv2
from cv_bridge import CvBridge, CvBridgeError


class NNLibraryFunctions(object):
  """preps bagfile data for NN"""
  def __init__(self):
    self.session = tf.Session()
    rospack = rospkg.RosPack()
    self.bridge = CvBridge()
    self.bags_dir_path = rospack.get_path('gp_human_pred') + "/bagfiles/nn_training_data/"

  def convert_imgs(self,img=None):
    #img is 107x107 need to go from rgb to grey
    if "rgb8" in img.encoding:
      try:
        cv_image = self.bridge.imgmsg_to_cv2(img, "rgb8") #http://docs.ros.org/melodic/api/cv_bridge/html/python/
      except CvBridgeError as e:
        print(e)
      img_gray = cv2.cvtColor(cv_image, cv2.COLOR_RGB2GRAY) #type cv::mat
    elif "mono8" in img.encoding:
      try:
        img_gray = self.bridge.imgmsg_to_cv2(img, "mono8") #http://docs.ros.org/melodic/api/cv_bridge/html/python/
      except CvBridgeError as e:
        print(e)
    #now make this a numpy array
    img_gray_mat = np.asarray(img_gray)
    #expand on last dimension (channel)
    img_gray_mat = np.expand_dims(img_gray_mat,axis=3)
    return img_gray_mat

  def NN_data_dict(self, NN_input=None):
    #make dict for NN
    Img = self.convert_imgs(img=NN_input.local_gridmap_img)
    head_step = np.array([NN_input.local_head_vect.vector.x, 
                          NN_input.local_head_vect.vector.y, 
                          NN_input.local_step_vect.vector.x,
                          NN_input.local_step_vect.vector.y])
    #Next two quantites makes explicit assumption that body fixed frame is pointing 'upward', sys frame z-axis must align with world z-axis
    twist = np.array([NN_input.local_twist.twist.linear.x,
                      NN_input.local_twist.twist.linear.y,
                      NN_input.local_twist.twist.angular.z]) 
    wrench = np.array([NN_input.local_wrench.wrench.force.x,
                       NN_input.local_wrench.wrench.force.y,
                       NN_input.local_wrench.wrench.torque.z])
    NN_input_dict = {"map_img":[Img], "head_step":[head_step], "twist":[twist], "wrench":[wrench]}
    return NN_input_dict

  def load_nn_model(self, model_name_path=None):
    #generate model path
    model_path = self.bags_dir_path+"trained_models/"+model_name_path+"/full_model"
    #construct graph model from file
    meta_graph_def = tf.saved_model.loader.load(self.session,
                                            [tf.saved_model.tag_constants.SERVING],
                                            model_path)
    # Collect input and output tensors from signature.
    signature_key = tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY
    sig = meta_graph_def.signature_def[signature_key]
    self.inputs = dict()
    self.outputs = dict()
    for key in sig.inputs:
      self.inputs[key] = self.session.graph.get_tensor_by_name(sig.inputs[key].name)
    for key in sig.outputs:
      self.outputs[key] = self.session.graph.get_tensor_by_name(sig.outputs[key].name)

  def NN_prediction(self, NN_input = None):
    data = self.NN_data_dict(NN_input=NN_input)
    output_pred = self.session.run(fetches = self.outputs["pose"], feed_dict={self.inputs["map_img"]:data["map_img"],
                                                                              self.inputs["head_step"]:data["head_step"],
                                                                              self.inputs["twist"]:data["twist"],
                                                                              self.inputs["wrench"]: data["wrench"]})
    #output is a 1x3 array: (x,y,th_z) of new pose in current pose frame
    pred_pose = PoseStamped()
    pred_pose.header = NN_input.header #same header (all is local frame)
    # pred_pose.child_frame_id = "predicted_pose"
    pred_pose.pose.position.x = output_pred[0][0] 
    pred_pose.pose.position.y = output_pred[0][1]
    pred_pose.pose.position.z = 0.0 #same height as base frame (system frame) 
    pred_pose.pose.orientation.x = 0.0
    pred_pose.pose.orientation.y = 0.0
    pred_pose.pose.orientation.z = np.sin(output_pred[0][2]/2.)
    pred_pose.pose.orientation.w = np.cos(output_pred[0][2]/2.)
    return pred_pose


'''
Prep: 
cls_obj = NNLibraryFunctions()
cls_obj.load_nn_model(model_name_path="training_1p5sec_1_13_19_no_hs_fb_with_dropout_fc0p2") 

Usage:
pred_pose = self.NN_prediction(NN_input=input_msg) #input: intelligent_coop_carry_msgs/IntentQuantitiesLocal Output: geometry_msgs/TransformStamped (pred at future time but stamped current time)
'''