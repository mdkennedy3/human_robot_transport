/*
Copyright (c) <2018>, <Monroe Kennedy III>
* All rights reserved.
*/
#include <ros/ros.h>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <Eigen/SVD>
#include <time.h>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <map> //dictionary equivalent
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/transform_broadcaster.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
//Bring in sensor messages
#include <std_msgs/Header.h>
#include <gp_human_pred/WrenchArduino.h>
#include <geometry_msgs/PoseStamped.h>
#include <people_msgs/PositionMeasurementArray.h>
#include <people_msgs/PositionMeasurement.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/WrenchStamped.h>
//Msg to pub together
#include <gp_human_pred/HumanTransport.h>
//Msg for wrench
#include <intelligent_coop_carry_msgs/DynamicalTerms.h>
// //For visualizations
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

class SensorRepub
{
  public:
    SensorRepub(); //constructor
    //List public class functions/variables
    void Repub_Human_Transport_topics(); // This function publishes the combined message
    void WrenchCallback(const intelligent_coop_carry_msgs::DynamicalTerms::ConstPtr& msg); //inside each callback, a variable is populated that is constantly updated with latest version of msg
    void LaserScanCallback(const sensor_msgs::LaserScan::ConstPtr& msg); //::ConstPtr ensures that msg cannot be modified (does not require boost with scope operator)
    void HeadPoseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
    void FootPoseCallback(const people_msgs::PositionMeasurementArray::ConstPtr& msg);
    void Publish_Viable_Person_Position(const people_msgs::PositionMeasurement& viable_person);
  private:
    //List private variables/fncts
    ros::NodeHandle nh;
    //Subscribe to topics
    ros::Subscriber wrench_sub_;
    ros::Subscriber laser_sub_;
    ros::Subscriber head_pose_sub_;
    ros::Subscriber foot_pose_sub_;
    //Publisher
    ros::Publisher human_transport_pub_;
    ros::Publisher RH_applied_wrench_;
    ros::Publisher LH_applied_wrench_;
    ros::Publisher Net_applied_wrench_;
    ros::Publisher viable_person_step_visual_;
    //Variables
    intelligent_coop_carry_msgs::DynamicalTerms wrench_msg_output_;
    sensor_msgs::LaserScan scan_msg_output_;
    geometry_msgs::PoseStamped head_pose_msg_output_;
    people_msgs::PositionMeasurement foot_pose_msg_output_;
    bool foot_pose_populated_ever_;
    geometry_msgs::WrenchStamped follower_wrench_calculated_output_;
    double person_laser_dx_max_;
    double person_laser_dy_max_;
    //Tf in case needed
    tf2_ros::Buffer tfBuffer;
    std::unique_ptr<tf2_ros::TransformListener> tfListener;
    tf2_ros::TransformBroadcaster tfbroadcast;
}; //Remember to put semicolon after class declaration.


SensorRepub::SensorRepub()
{
  nh = ros::NodeHandle("~"); //This argument makes all topics internal to this node namespace
  //Estabilish the transformer
  tfListener.reset(new tf2_ros::TransformListener(tfBuffer));
  //Make subscribers and retrieve all values from callbacks, put them all in the message then publish them
  wrench_sub_ = nh.subscribe("/dynamical_terms",1,&SensorRepub::WrenchCallback,this);
  laser_sub_ = nh.subscribe("/base_scan",1,&SensorRepub::LaserScanCallback,this);
  head_pose_sub_ = nh.subscribe("/head_pose",1,&SensorRepub::HeadPoseCallback,this);
  foot_pose_sub_ = nh.subscribe("/leg_tracker_measurements",1,&SensorRepub::FootPoseCallback,this);
  //Make publisher
  human_transport_pub_ = nh.advertise<gp_human_pred::HumanTransport>("/human_transport_combined",1);
  RH_applied_wrench_ = nh.advertise<geometry_msgs::WrenchStamped>("/RH_applied_wrench",1);
  LH_applied_wrench_ = nh.advertise<geometry_msgs::WrenchStamped>("/LH_applied_wrench",1);
  Net_applied_wrench_ = nh.advertise<geometry_msgs::WrenchStamped>("/Net_applied_wrench",1);
  viable_person_step_visual_ = nh.advertise<visualization_msgs::Marker>("/viable_person_vis",1);
  //Establish this bool to use when filtering ppl, to determine if the name should be checked
  foot_pose_populated_ever_ = false;
  //Make the max distance position can be from laser to be considered valid
  person_laser_dx_max_ = 0.7; //Meters ([0,dx])
  person_laser_dy_max_ = 0.5; //Meters (either side [-dy,dy])
}

void SensorRepub::WrenchCallback(const intelligent_coop_carry_msgs::DynamicalTerms::ConstPtr& msg){
  //Collect msg and populate variable
  wrench_msg_output_ = *msg; // uses pointers msg->element: de-refrences a pointer to select a field, data contains everything, this makes a copy;
  //Make the forces vector objects for convenience
  geometry_msgs::WrenchStamped FR_wrench_stamped = wrench_msg_output_.Fr;
  geometry_msgs::WrenchStamped Fl_wrench_stamped = wrench_msg_output_.Fl;
  Eigen::Vector3d RH_force;
  RH_force(0) = wrench_msg_output_.Fr.wrench.force.x;
  RH_force(1) = wrench_msg_output_.Fr.wrench.force.y;
  RH_force(2) = wrench_msg_output_.Fr.wrench.force.z;
  Eigen::Vector3d LH_force;
  LH_force(0) = wrench_msg_output_.Fl.wrench.force.x;
  LH_force(1) = wrench_msg_output_.Fl.wrench.force.y;
  LH_force(2) = wrench_msg_output_.Fl.wrench.force.z;
  //Get the frame id's (good practice, names known to be "right_force_sensor", "left_force_sensor")
  std::string rh_frame_id = wrench_msg_output_.Fr.header.frame_id;
  std::string lh_frame_id = wrench_msg_output_.Fl.header.frame_id;
  //1. Get transform between End-effector frame and right/left force sensors  (R(A->EE),d(EE, E->A))
  geometry_msgs::TransformStamped tf_stamped_RH_to_EE;
  geometry_msgs::TransformStamped tf_stamped_LH_to_EE;
  bool tf_success = true; //To determine if tf's were obtained to continue with calculations
  try{
    tf_stamped_RH_to_EE = tfBuffer.lookupTransform("virtual_end_effector", rh_frame_id, ros::Time(0)); 
    tf_stamped_LH_to_EE = tfBuffer.lookupTransform("virtual_end_effector", lh_frame_id, ros::Time(0)); 
  }
  catch (tf2::TransformException &ex) {
    ROS_WARN("%s",ex.what());
    ROS_WARN("EE-Wrench cannot be calculated for this instance");
    ros::Duration(1.0).sleep();
    tf_success = false;
  }
  if(tf_success) //Only proceed if tf's were obtained to perform calculations
  {
    //2. Rotate forces from frame A (right/left) to frame E
      //2a. obtain eigen transform matricies
      Eigen::Affine3d TF_RH_to_EE = tf2::transformToEigen(tf_stamped_RH_to_EE);
      Eigen::Affine3d TF_LH_to_EE = tf2::transformToEigen(tf_stamped_LH_to_EE);
      //2b. rotate forces
      Eigen::Vector3d RH_force_EEframe = TF_RH_to_EE.rotation()*RH_force;
      Eigen::Vector3d LH_force_EEframe = TF_LH_to_EE.rotation()*LH_force;
      //2c. obtain tranformation vectors
      Eigen::Vector3d RH_pos_vect_EEframe = TF_RH_to_EE.translation();
      Eigen::Vector3d LH_pos_vect_EEframe = TF_LH_to_EE.translation();
    //3. Calculate torques and sum forces and populate wrench/stamped object
      //3a. calculate torques (since pos-vects r are from frame E->A in E frame and F's are in E frame, then torque in E frame is t = r x F )
      Eigen::Vector3d RH_torque_EEframe = RH_pos_vect_EEframe.cross(RH_force_EEframe);
      Eigen::Vector3d LH_torque_EEframe = LH_pos_vect_EEframe.cross(LH_force_EEframe);
      //3b. sum forces and torques
      follower_wrench_calculated_output_.header = wrench_msg_output_.header;
      follower_wrench_calculated_output_.header.frame_id = "virtual_end_effector";
      follower_wrench_calculated_output_.wrench.force.x = RH_force_EEframe(0) + LH_force_EEframe(0);
      follower_wrench_calculated_output_.wrench.force.y = RH_force_EEframe(1) + LH_force_EEframe(1);
      follower_wrench_calculated_output_.wrench.force.z = RH_force_EEframe(2) + LH_force_EEframe(2);
      follower_wrench_calculated_output_.wrench.torque.x = RH_torque_EEframe(0) + LH_torque_EEframe(0);
      follower_wrench_calculated_output_.wrench.torque.y = RH_torque_EEframe(1) + LH_torque_EEframe(1);
      follower_wrench_calculated_output_.wrench.torque.z = RH_torque_EEframe(2) + LH_torque_EEframe(2);
    //Visualize the applied wrenches and net wrench
      RH_applied_wrench_.publish(FR_wrench_stamped);
      LH_applied_wrench_.publish(Fl_wrench_stamped);
      Net_applied_wrench_.publish(follower_wrench_calculated_output_);
  }
}

void SensorRepub::LaserScanCallback(const sensor_msgs::LaserScan::ConstPtr& msg){
  //Collect msg and populate variable
  scan_msg_output_ = *msg; // * de-references the pointer msg
  //Call the publish node to populate the new message from this subscriber
  Repub_Human_Transport_topics(); //collects all variables through class
}

void SensorRepub::HeadPoseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg){
  //Collect msg and populate variable


  //The message just has this as the x-axis in the head-origin frame, instead populate this in the world frame (mocap), but allow it to be triggered here
  geometry_msgs::TransformStamped head_origin_tf;
  bool tf_success = true;
  std::string world_frame_str = "mocap";
  try{
    head_origin_tf = tfBuffer.lookupTransform(world_frame_str, "head_origin", ros::Time(0)); 
  }
  catch (tf2::TransformException &ex) {
    ROS_WARN("%s",ex.what());
    ROS_WARN("head origin transform wasnt obtained");
    ros::Duration(1.0).sleep();
    tf_success = false;
  }
  if(tf_success) //Only proceed if tf's were obtained to perform calculations
  {
    //obtain the msg output (header file primary)
    head_pose_msg_output_ = *msg;
    //Make the frame correct
    head_pose_msg_output_.header.frame_id = world_frame_str;
    //Populate with correct transform
    head_pose_msg_output_.pose.position.x = head_origin_tf.transform.translation.x;
    head_pose_msg_output_.pose.position.y = head_origin_tf.transform.translation.y;
    head_pose_msg_output_.pose.position.z = head_origin_tf.transform.translation.z;
    head_pose_msg_output_.pose.orientation = head_origin_tf.transform.rotation;
  }//send head origin if tf call successful


}

void SensorRepub::FootPoseCallback(const people_msgs::PositionMeasurementArray::ConstPtr& msg){
  /*
  Collect msg, filter to feet positions in laser-scan frame that are directly 
  in front (present variables for x,y), and report that position (and name) 
  If there are more than one individual in the space, then if one of them has 
  same name as previous value, then store the one with the same 'person' name
  */
  people_msgs::PositionMeasurementArray pos_meas_array = *msg;
  //1. Check that the list of people is not empty
  bool viable_person_found = false;
  if(pos_meas_array.people.size()>0)
  {
    //2. Loop through the msg array of people, storing those that meet criteria, (if none meet criteria, then simply do not update the pose variable)
    people_msgs::PositionMeasurementArray pos_meas_array_filtered;
    people_msgs::PositionMeasurement viable_person; //The most viable person candidate
    for(int i=0; i<pos_meas_array.people.size(); i++){
      people_msgs::PositionMeasurement curr_person = pos_meas_array.people[i]; //Current person
      //Check if in laser frame a) x is in [0,dx] and b) y in [-dy,dy]
      if((curr_person.pos.x>0.0) && (curr_person.pos.x < person_laser_dx_max_) && (curr_person.pos.y > -person_laser_dy_max_) && (curr_person.pos.y < person_laser_dy_max_) ){
        pos_meas_array_filtered.people.push_back(curr_person); //If it satisfies the filtering condition, then add it to the filtered array
      }
      //If this set is non-empty then proceed
      if(pos_meas_array_filtered.people.size()>0)
      {
        viable_person_found = true;
        viable_person = pos_meas_array_filtered.people[0]; //default to first in list, but update if same name is found
        if(foot_pose_populated_ever_){
          std::string prev_name = foot_pose_msg_output_.name; //previous name
          //was previously populated, now check to see if there is a name that was viable (this is not guaranteed, just highly probable that same name will persist)
          for (int jdx=0; jdx<pos_meas_array_filtered.people.size(); jdx++){
            std::string test_name = pos_meas_array_filtered.people[jdx].name;
            if (prev_name.compare(test_name) == 0){
              //These are the same string
              viable_person = pos_meas_array_filtered.people[jdx];
              break;//exit because same string was found
            }
          }
        }else{
          foot_pose_populated_ever_ = true; //there was something in the list, so make this true for next time
        }
      }else{
        foot_pose_populated_ever_ = false; //Reset this, as all viable candidates were lost
      }
    }
    /*
    3. (if this isn't first trial) then compare names of valid members with previous names, 
    if there is a match , then retain that as the person, if there is no match then take 
    one with smallest y value (as person should be centered on y axis)
    */
    if(viable_person_found){
      foot_pose_msg_output_ = viable_person;
      Publish_Viable_Person_Position(viable_person); //visualize the viable person pose
    }    
  }
}

void SensorRepub::Publish_Viable_Person_Position(const people_msgs::PositionMeasurement& viable_person)
{
  //Publish the viable person position (convert from person msg to marker type)
  visualization_msgs::Marker marker;
  marker.header = viable_person.header;
  marker.id = std::atoi(viable_person.object_id.c_str()) ;// std::stoi(viable_person.object_id);
  marker.type=2;
  marker.pose.position.x = viable_person.pos.x;
  marker.pose.position.y = viable_person.pos.y;
  marker.pose.position.z = viable_person.pos.z;
  marker.scale.x = 0.20;
  marker.scale.y = 0.20;
  marker.scale.z = 0.20;
  marker.color.a = 1.0; //marker transparency
  marker.color.r = 0.0;
  marker.color.g = 0.0;
  marker.color.b = 1.0;
  marker.lifetime = ros::Duration(5);
  //Now publish the marker
  viable_person_step_visual_.publish(marker);
}

void SensorRepub::Repub_Human_Transport_topics()
{
  //Make variable 
  gp_human_pred::HumanTransport human_transport_combined_msg;
  human_transport_combined_msg.header = scan_msg_output_.header; //As this is the generating message the headers are the same
  //Populate Variable
  human_transport_combined_msg.applied_wrench = wrench_msg_output_;
  human_transport_combined_msg.follower_wrench = follower_wrench_calculated_output_;
  human_transport_combined_msg.head_pose = head_pose_msg_output_;
  human_transport_combined_msg.step_position = foot_pose_msg_output_;
  human_transport_combined_msg.laserscan = scan_msg_output_;
  //Publish variable
  human_transport_pub_.publish(human_transport_combined_msg);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "repub_human_intent");
  ROS_INFO("repub human intent node running");
  //intatiate class object
  SensorRepub sensor_repub_obj;
  ros::spin();//Call the class, service callback is in the constructor, and looped by the ros spin
  return 0;
}
