//Written by Monroe Kennedy III 12/2018
#include <gp_human_pred/basic_ros_include.h>
#include <gp_human_pred/extract_intent_features_body_frame.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <intelligent_coop_carry_msgs/IntentQuantitiesLocal.h>
#include <intelligent_coop_carry_msgs/TrainingPoseOutput.h>

class ConstructTrainingData
{
  public:
    ConstructTrainingData(ros::NodeHandle &nh);
    ~ConstructTrainingData();
    void ReadBag(const std::string& bag_name,std::vector<intelligent_coop_carry_msgs::IntentQuantitiesLocal>& intent_quantities_list);
    void GenerateBagFileNameList();
    void LoadBagFiles();
    void ConstructTrainingDataOutput(std::vector<intelligent_coop_carry_msgs::IntentQuantitiesLocal>& intent_quantities_list);
    void SaveTrainingDataToBag();
    void TransformStampedToPoseStamped(const geometry_msgs::TransformStamped& sys_trans, geometry_msgs::PoseStamped& sys_pose);
    void FuturePoseInCurrentBodyFrame(const geometry_msgs::TransformStamped& sys_trans_current,const geometry_msgs::TransformStamped& sys_trans_future , geometry_msgs::PoseStamped& sys_pose_future);
  
  private:
    ros::NodeHandle pnh_;
    humanintentcls::TransformIntentQuantities trans_intent_quant_obj_;
    std::vector<std::string> bag_file_name_list_;
    std::vector<intelligent_coop_carry_msgs::TrainingPoseOutput> training_pose_output_;
    std::string intent_topic_name_;
    std::string local_bag_file_dir_;
    std::string nn_training_data_output_file_dir_;
    float time_delay_;
};

ConstructTrainingData::ConstructTrainingData(ros::NodeHandle &nh) : pnh_(nh)
{
  GenerateBagFileNameList();
  intent_topic_name_ = "/local_intent_quanities";
  local_bag_file_dir_ = "/bagfiles/body_frame_output/";
  nn_training_data_output_file_dir_ = "/bagfiles/nn_training_data/";
  pnh_.param<float>("training_time_delay", time_delay_,1.0); //lookahead time
}

ConstructTrainingData::~ConstructTrainingData(){}

void ConstructTrainingData::GenerateBagFileNameList()
{
  bag_file_name_list_.push_back("dashed_lines_pre_train_data_trial_001");
  bag_file_name_list_.push_back("dead_ends_pre_train_data_trial_001");
  bag_file_name_list_.push_back("four_rings_pre_train_data_trial_001");
  bag_file_name_list_.push_back("hall_and_diamond_pre_train_data_trial_001");
  bag_file_name_list_.push_back("hall_and_diamond_pre_train_data_trial_002");
  bag_file_name_list_.push_back("scattered_obstacles_pre_train_data_trial_001");  
  bag_file_name_list_.push_back("two_halls_pre_train_data_trial_001"); 
}

void ConstructTrainingData::LoadBagFiles()
{
  for (int i = 0; i < bag_file_name_list_.size(); ++i)
  {
    std::vector<intelligent_coop_carry_msgs::IntentQuantitiesLocal> intent_quantities_list;//intent quantities list for this bag
    ReadBag(bag_file_name_list_[i],intent_quantities_list);
    ConstructTrainingDataOutput(intent_quantities_list);
  }
}

void ConstructTrainingData::ReadBag(const std::string& bag_name, std::vector<intelligent_coop_carry_msgs::IntentQuantitiesLocal>& intent_quantities_list)
{
  //read in the bagname specified
  std::cout << "reading bag file: " << bag_name << std::endl;
  std::string bags_path = ros::package::getPath("gp_human_pred") + local_bag_file_dir_;
  std::string curr_bag = bag_name + ".bag";
  rosbag::Bag bag;
  bag.open(bags_path+curr_bag);
  //Make topics
  std::vector<std::string> topics;
  topics.push_back(std::string(intent_topic_name_));
  int num_msgs = 0;
  //extract msgs
  for (rosbag::MessageInstance const m: rosbag::View(bag, rosbag::TopicQuery(topics)))
  {
    intelligent_coop_carry_msgs::IntentQuantitiesLocalConstPtr msg_ptr;
    msg_ptr = m.instantiate<intelligent_coop_carry_msgs::IntentQuantitiesLocal>();
    if (msg_ptr != NULL)
    {
      intent_quantities_list.push_back(*msg_ptr); //load contents
      ++num_msgs;
    }
  }
  bag.close();
  std::cout << "\nnumber of messages: " << num_msgs << std::endl;
}

void ConstructTrainingData::ConstructTrainingDataOutput(std::vector<intelligent_coop_carry_msgs::IntentQuantitiesLocal>& intent_quantities_list)
{
  //Iterate through the current bag data for the given time delay, adding each output to the 
  for (int idx = 0; idx < intent_quantities_list.size(); ++idx)
  {
    double current_time = intent_quantities_list[idx].system_pose.header.stamp.toSec();
    for (int jdx = idx; jdx < intent_quantities_list.size(); ++jdx)
    {
      /* find the time that is time delay ahead */
      double future_data_time = intent_quantities_list[jdx].system_pose.header.stamp.toSec();
      double dt = future_data_time - current_time;
      bool condition = dt >= time_delay_;
      if(dt >= time_delay_)
      {
        //populate training msg
        intelligent_coop_carry_msgs::TrainingPoseOutput training_pose_output_obj_;
        training_pose_output_obj_.header = intent_quantities_list[idx].header;
        training_pose_output_obj_.intent_quantities = intent_quantities_list[idx];
        training_pose_output_obj_.time_delay = time_delay_;
        TransformStampedToPoseStamped(intent_quantities_list[jdx].system_pose,training_pose_output_obj_.future_pose_world_frame);
        //Now get the future pose in the current body fixed frame (get this pose and return it)
        FuturePoseInCurrentBodyFrame(intent_quantities_list[idx].system_pose,intent_quantities_list[jdx].system_pose, training_pose_output_obj_.future_pose_local_frame);
        //Add this to the list
        training_pose_output_.push_back(training_pose_output_obj_);
        break;
      }
    }
  }
  std::cout << "current size of training data: " << training_pose_output_.size() << std::endl;
}

void ConstructTrainingData::FuturePoseInCurrentBodyFrame(const geometry_msgs::TransformStamped& sys_trans_current,const geometry_msgs::TransformStamped& sys_trans_future , geometry_msgs::PoseStamped& sys_pose_future)
{
  /* Implementation: 
    1. Get the transform from future frame to current frame (assuming both start as body frame to world frame)
    2. Express this as pose message with body frame as header frame id
  */
  Eigen::Affine3d sys_tf_current_eig = tf2::transformToEigen(sys_trans_current);
  Eigen::Affine3d sys_tf_future_eig = tf2::transformToEigen(sys_trans_future);
  Eigen::Affine3d sys_tf_future_to_current_eig = sys_tf_current_eig.inverse()*sys_tf_future_eig;
  geometry_msgs::TransformStamped sys_tf_future_to_current = tf2::eigenToTransform(sys_tf_future_to_current_eig);
  TransformStampedToPoseStamped(sys_tf_future_to_current, sys_pose_future);
  sys_pose_future.header = sys_trans_future.header;
  sys_pose_future.header.frame_id = sys_trans_future.child_frame_id;
}

void ConstructTrainingData::TransformStampedToPoseStamped(const geometry_msgs::TransformStamped& sys_trans, geometry_msgs::PoseStamped& sys_pose)
{
  sys_pose.header = sys_trans.header;
  sys_pose.pose.position.x = sys_trans.transform.translation.x;
  sys_pose.pose.position.y = sys_trans.transform.translation.y;
  sys_pose.pose.position.z = sys_trans.transform.translation.z;
  sys_pose.pose.orientation.x = sys_trans.transform.rotation.x;
  sys_pose.pose.orientation.y = sys_trans.transform.rotation.y;
  sys_pose.pose.orientation.z = sys_trans.transform.rotation.z;
  sys_pose.pose.orientation.w = sys_trans.transform.rotation.w;
}

void ConstructTrainingData::SaveTrainingDataToBag()
{
  //save nn training data
  std::string bags_path = ros::package::getPath("gp_human_pred") + nn_training_data_output_file_dir_;
  std::string bagname = bags_path + "nn_trained_data_time_delay_" + std::to_string(time_delay_) + "_sec.bag";
  rosbag::Bag newbag;
  newbag.open(bagname,rosbag::bagmode::Write);
  
  std::cout << "\nTotal size of training data: " << training_pose_output_.size() << std::endl;
  
  if(training_pose_output_.size() > 0)
  {
    for (int idx = 0; idx < training_pose_output_.size(); ++idx)
    {
      newbag.write("/nn_training_data",ros::Time::now(), training_pose_output_[idx]);
    }
  }else
  {
    ROS_WARN("No data was written to output bag, size of training output list was zero");
  }
  newbag.close();
  std::cout << "Finished!" << std::endl;
}


int main(int argc, char **argv)
{
  ros::init(argc,argv,"construct_training_data_cpp");
  ros::NodeHandle nh("~");
  ConstructTrainingData ctd_obj(nh);
  ctd_obj.LoadBagFiles(); //load bagfiles
  ctd_obj.SaveTrainingDataToBag();
  return 0;
}


