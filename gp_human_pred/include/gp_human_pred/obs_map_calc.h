#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <Eigen/SVD>
#include <eigen_conversions/eigen_msg.h>
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>
#include <gp_human_pred/OccupancyLocalFrame.h>



class ObstacleMapCalc
{
  public:
    ObstacleMapCalc(const double& rad); //radius of interest in the map
    Eigen::ArrayXXf relavent_angles;
    std::vector<gp_human_pred::OccupancyLocalFrame> MapVector(const grid_map::GridMap& local_map, const Eigen::Affine3d& pose);  // map_obj_vector = f(map,pose_frame_world)    
  
    
   
  
};

ObstacleMapCalc::ObstacleMapCalc(const double& rad){

  double radius = rad;

  relavent_angles = Eigen::ArrayXXf::Zero(35, 4);
  relavent_angles.col(0) = Eigen::ArrayXf::LinSpaced(35, 0, 350); //angle degrees
  relavent_angles.col(1) = M_PI / 180 * relavent_angles.col(0); //angle radians
  relavent_angles.col(2) = radius*relavent_angles.col(1).sin(); //yposition
  relavent_angles.col(3) = radius*relavent_angles.col(1).cos(); //xposition

}



std::vector<gp_human_pred::OccupancyLocalFrame> ObstacleMapCalc::MapVector(const grid_map::GridMap& local_map, const Eigen::Affine3d& pose){


std::vector<gp_human_pred::OccupancyLocalFrame> obj;
return obj;

}


