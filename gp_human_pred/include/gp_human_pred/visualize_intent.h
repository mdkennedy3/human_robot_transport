#pragma once
#include <gp_human_pred/basic_ros_include.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <people_msgs/PositionMeasurementArray.h>
#include <people_msgs/PositionMeasurement.h>

class VisualizeHumanIntentClass
{
  public:
    VisualizeHumanIntentClass();
    ~VisualizeHumanIntentClass();
    void VisualizeSystemPoseHeadStepVectors(const geometry_msgs::TransformStamped&  system_pose,const geometry_msgs::Vector3Stamped& local_step_vect,const geometry_msgs::Vector3Stamped local_head_vect,const geometry_msgs::TwistStamped local_twist, ros::Publisher& head_step_pose_vect_publisher);
    void VisualizationVectorMarkerGenerator(visualization_msgs::Marker& marker,const std_msgs::Header& header, int& marker_id,const geometry_msgs::Vector3& vector_base,const geometry_msgs::Vector3& vector,const std::string& marker_text, visualization_msgs::Marker& marker_text_marker, float red=1, float blue=0, float green=0);
    void Publish_Viable_Person_Position(const people_msgs::PositionMeasurement& viable_personi, ros::Publisher& viable_person_step_visual, people_msgs::PositionMeasurement& viable_person);
};

