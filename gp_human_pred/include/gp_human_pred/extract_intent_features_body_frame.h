#pragma once

#include <gp_human_pred/basic_ros_include.h>
#include <gp_human_pred/gridmap_include.h>
#include <gp_human_pred/obs_map_calc.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

//geometry_msgs/PoseStamped
//#include <people_msgs/PositionMeasurementArray.h>
#include <people_msgs/PositionMeasurement.h>
#include <nav_msgs/Odometry.h>


//geometry_msgs/Vector3Stamped  //need to convert step_position to stamped vector
/*
Class Description
This class allows for intent quantities to be transformed into body fixed frame to be used for later operations
*/
namespace humanintentcls
{
  class TransformIntentQuantities
  {
  public:
    TransformIntentQuantities(); //constructor
    ~TransformIntentQuantities(); //destructor
    void ObtainLocalMap(const grid_map::GridMap &orig_local_map, grid_map::GridMap &robo_frame_local_map, sensor_msgs::Image &out_img, const geometry_msgs::TransformStamped& robo_world_tf); //transforms local map and returns image
    void ObtainLocalStepVect(const geometry_msgs::Vector3Stamped& inpt_vect, geometry_msgs::Vector3Stamped& outpt_vect, const geometry_msgs::TransformStamped& robo_world_tf); //transform step vector to body fixed frame
    void ObtainLocalHeadVect(const geometry_msgs::Vector3Stamped& inpt_pose, geometry_msgs::Vector3Stamped& outpt_vect, const geometry_msgs::TransformStamped& robo_world_tf); //transform head pose to body-fixed frame
    void ObtainLocalWrenchVect(const geometry_msgs::WrenchStamped& inpt_wrench, geometry_msgs::WrenchStamped& outpt_wrench, const geometry_msgs::TransformStamped& robo_world_tf); //transform wrench
    void ObtainLocalTwist(const nav_msgs::Odometry& inpt_odom, geometry_msgs::TwistStamped& odom_twist, const geometry_msgs::TransformStamped& robo_world_tf); //This has both pose and twist

    void ConvertHeadPoseToVector(const geometry_msgs::PoseStamped& head_pose_inpt, geometry_msgs::Vector3Stamped& head_vect_outpt);

    void PlanarVectorScaledByTwist(geometry_msgs::Vector3Stamped& vect_to_scale, const geometry_msgs::TwistStamped& sys_odom_twist); //This is for head/step vectors, takes in the vector, normalizes if neccessary, scales by twist and outputs resultant (all frames system body-fixed frame) 

    bool UpdateStepVector(const people_msgs::PositionMeasurement& curr_step_position, geometry_msgs::Vector3Stamped& step_vector, float max_upd_time = 3.0); //This function takes steps and calculates vector if consecutive steps are given	
    bool LocalMapGenerator(const nav_msgs::Odometry& sys_odom, grid_map::GridMap& local_gridmap_worldframe, const float local_obstacle_radius);
    void OdomToTransform (const nav_msgs::Odometry& odom, geometry_msgs::TransformStamped& transform);
    bool ObtainTransform(const std::string& target_frame, const std::string& source_frame, geometry_msgs::TransformStamped& transform);
    bool TransformSysToMapFrame(geometry_msgs::TransformStamped& sys_transform, const std::string map_frame);

  private:
    people_msgs::PositionMeasurement prev_step_position_; 
    bool prev_step_position_filled_; //set initially to false, then after first measurement this is updated, if sufficient time passes without update this is set back to false to avoid large jumps
    tf2_ros::Buffer tfBuffer;
    std::unique_ptr<tf2_ros::TransformListener> tfListener;    

    void ExtractZframeRotation(const Eigen::Affine3d& tf_inpt, Eigen::Affine3d& tf_outpt);
    void RotateVectorGivenTransform(const geometry_msgs::Vector3& inpt_vect, geometry_msgs::Vector3& outpt_vect, const geometry_msgs::TransformStamped& transform);
    void PoseToTransformStamped(const geometry_msgs::PoseStamped& inpt_pose, geometry_msgs::TransformStamped& outpt_transform, const std::string& child_frame_id="default");

  };
}
