№Н
–2І2
:
Add
x"T
y"T
z"T"
Ttype:
2	
W
AddN
inputs"T*N
sum"T"
Nint(0"!
Ttype:
2	АР
Ј
ApplyRMSProp
var"TА

ms"TА
mom"TА
lr"T
rho"T
momentum"T
epsilon"T	
grad"T
out"TА" 
Ttype:
2	"
use_lockingbool( 
x
Assign
ref"TА

value"T

output_ref"TА"	
Ttype"
validate_shapebool("
use_lockingbool(Ш
4
Atan2
y"T
x"T
z"T"
Ttype:
2
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
~
BiasAddGrad
out_backprop"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
R
BroadcastGradientArgs
s0"T
s1"T
r0"T
r1"T"
Ttype0:
2	
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
I
ConcatOffset

concat_dim
shape*N
offset*N"
Nint(0
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
м
Conv2D

input"T
filter"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

Т
Conv2DBackpropFilter

input"T
filter_sizes
out_backprop"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

С
Conv2DBackpropInput
input_sizes
filter"T
out_backprop"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

,
Cos
x"T
y"T"
Ttype:

2
S
DynamicStitch
indices*N
data"T*N
merged"T"
Nint(0"	
Ttype
^
Fill
dims"
index_type

value"T
output"T"	
Ttype"

index_typetype0:
2	
,
Floor
x"T
y"T"
Ttype:
2
?
FloorDiv
x"T
y"T
z"T"
Ttype:
2	
9
FloorMod
x"T
y"T
z"T"
Ttype:

2	
B
GreaterEqual
x"T
y"T
z
"
Ttype:
2	
V
HistogramSummary
tag
values"T
summary"
Ttype0:
2	
.
Identity

input"T
output"T"	
Ttype
О
ImageSummary
tag
tensor"T
summary"

max_imagesint(0"
Ttype0:
2"'
	bad_colortensorB:€  €
p
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:
	2
‘
MaxPool

input"T
output"T"
Ttype0:
2	"
ksize	list(int)(0"
strides	list(int)(0""
paddingstring:
SAMEVALID":
data_formatstringNHWC:
NHWCNCHWNCHW_VECT_C
о
MaxPoolGrad

orig_input"T
orig_output"T	
grad"T
output"T"
ksize	list(int)(0"
strides	list(int)(0""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW"
Ttype0:
2	
;
Maximum
x"T
y"T
z"T"
Ttype:

2	Р
Н
Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
8
MergeSummary
inputs*N
summary"
Nint(0
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(И
=
Mul
x"T
y"T
z"T"
Ttype:
2	Р
.
Neg
x"T
y"T"
Ttype:

2	

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
Н
Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
~
RandomUniform

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	И
a
Range
start"Tidx
limit"Tidx
delta"Tidx
output"Tidx"
Tidxtype0:	
2	
>
RealDiv
x"T
y"T
z"T"
Ttype:
2	
E
Relu
features"T
activations"T"
Ttype:
2	
V
ReluGrad
	gradients"T
features"T
	backprops"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0И
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0И
P
ScalarSummary
tags
values"T
summary"
Ttype:
2	
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
e
ShapeN
input"T*N
output"out_type*N"
Nint(0"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
,
Sin
x"T
y"T"
Ttype:

2
a
Slice

input"T
begin"Index
size"Index
output"T"	
Ttype"
Indextype:
2	
1
Square
x"T
y"T"
Ttype:

2	
ц
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
З
StridedSliceGrad
shape"Index
begin"Index
end"Index
strides"Index
dy"T
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
:
Sub
x"T
y"T
z"T"
Ttype:
2	
М
Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
c
Tile

input"T
	multiples"
Tmultiples
output"T"	
Ttype"

Tmultiplestype0:
2	
s

VariableV2
ref"dtypeА"
shapeshape"
dtypetype"
	containerstring "
shared_namestring И"serve*1.12.02v1.12.0-0-ga6d8ffae09Иџ
{
img_inptPlaceholder*$
shape:€€€€€€€€€kk*
dtype0*/
_output_shapes
:€€€€€€€€€kk
q
head_step_inptPlaceholder*
dtype0*'
_output_shapes
:€€€€€€€€€*
shape:€€€€€€€€€
m

twist_inptPlaceholder*
dtype0*'
_output_shapes
:€€€€€€€€€*
shape:€€€€€€€€€
n
wrench_inptPlaceholder*
dtype0*'
_output_shapes
:€€€€€€€€€*
shape:€€€€€€€€€
љ
8conv_layer1_conv/kernel/Initializer/random_uniform/shapeConst*
dtype0*
_output_shapes
:*%
valueB"             **
_class 
loc:@conv_layer1_conv/kernel
Ђ
6conv_layer1_conv/kernel/Initializer/random_uniform/minConst*
valueB 2@D±…н‘µњ**
_class 
loc:@conv_layer1_conv/kernel*
dtype0*
_output_shapes
: 
Ђ
6conv_layer1_conv/kernel/Initializer/random_uniform/maxConst*
valueB 2@D±…н‘µ?**
_class 
loc:@conv_layer1_conv/kernel*
dtype0*
_output_shapes
: 
О
@conv_layer1_conv/kernel/Initializer/random_uniform/RandomUniformRandomUniform8conv_layer1_conv/kernel/Initializer/random_uniform/shape*
T0**
_class 
loc:@conv_layer1_conv/kernel*
seed2 *
dtype0*&
_output_shapes
: *

seed 
ъ
6conv_layer1_conv/kernel/Initializer/random_uniform/subSub6conv_layer1_conv/kernel/Initializer/random_uniform/max6conv_layer1_conv/kernel/Initializer/random_uniform/min*
T0**
_class 
loc:@conv_layer1_conv/kernel*
_output_shapes
: 
Ф
6conv_layer1_conv/kernel/Initializer/random_uniform/mulMul@conv_layer1_conv/kernel/Initializer/random_uniform/RandomUniform6conv_layer1_conv/kernel/Initializer/random_uniform/sub*
T0**
_class 
loc:@conv_layer1_conv/kernel*&
_output_shapes
: 
Ж
2conv_layer1_conv/kernel/Initializer/random_uniformAdd6conv_layer1_conv/kernel/Initializer/random_uniform/mul6conv_layer1_conv/kernel/Initializer/random_uniform/min*
T0**
_class 
loc:@conv_layer1_conv/kernel*&
_output_shapes
: 
«
conv_layer1_conv/kernel
VariableV2*
shape: *
dtype0*&
_output_shapes
: *
shared_name **
_class 
loc:@conv_layer1_conv/kernel*
	container 
ы
conv_layer1_conv/kernel/AssignAssignconv_layer1_conv/kernel2conv_layer1_conv/kernel/Initializer/random_uniform*
use_locking(*
T0**
_class 
loc:@conv_layer1_conv/kernel*
validate_shape(*&
_output_shapes
: 
Ю
conv_layer1_conv/kernel/readIdentityconv_layer1_conv/kernel*
T0**
_class 
loc:@conv_layer1_conv/kernel*&
_output_shapes
: 
Ґ
'conv_layer1_conv/bias/Initializer/zerosConst*
valueB 2        *(
_class
loc:@conv_layer1_conv/bias*
dtype0*
_output_shapes
: 
Ђ
conv_layer1_conv/bias
VariableV2*
shared_name *(
_class
loc:@conv_layer1_conv/bias*
	container *
shape: *
dtype0*
_output_shapes
: 
ё
conv_layer1_conv/bias/AssignAssignconv_layer1_conv/bias'conv_layer1_conv/bias/Initializer/zeros*
T0*(
_class
loc:@conv_layer1_conv/bias*
validate_shape(*
_output_shapes
: *
use_locking(
М
conv_layer1_conv/bias/readIdentityconv_layer1_conv/bias*
T0*(
_class
loc:@conv_layer1_conv/bias*
_output_shapes
: 
o
conv_layer1_conv/dilation_rateConst*
valueB"      *
dtype0*
_output_shapes
:
т
conv_layer1_conv/Conv2DConv2Dimg_inptconv_layer1_conv/kernel/read*/
_output_shapes
:€€€€€€€€€gg *
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingVALID
©
conv_layer1_conv/BiasAddBiasAddconv_layer1_conv/Conv2Dconv_layer1_conv/bias/read*
data_formatNHWC*/
_output_shapes
:€€€€€€€€€gg *
T0
q
conv_layer1_conv/ReluReluconv_layer1_conv/BiasAdd*/
_output_shapes
:€€€€€€€€€gg *
T0
n
%conv_layer1_dropout/dropout/keep_probConst*
dtype0*
_output_shapes
: *
valueB 2Ќћћћћћм?
v
!conv_layer1_dropout/dropout/ShapeShapeconv_layer1_conv/Relu*
T0*
out_type0*
_output_shapes
:
w
.conv_layer1_dropout/dropout/random_uniform/minConst*
valueB 2        *
dtype0*
_output_shapes
: 
w
.conv_layer1_dropout/dropout/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB 2      р?
ћ
8conv_layer1_dropout/dropout/random_uniform/RandomUniformRandomUniform!conv_layer1_dropout/dropout/Shape*

seed *
T0*
dtype0*
seed2 */
_output_shapes
:€€€€€€€€€gg 
ґ
.conv_layer1_dropout/dropout/random_uniform/subSub.conv_layer1_dropout/dropout/random_uniform/max.conv_layer1_dropout/dropout/random_uniform/min*
_output_shapes
: *
T0
ў
.conv_layer1_dropout/dropout/random_uniform/mulMul8conv_layer1_dropout/dropout/random_uniform/RandomUniform.conv_layer1_dropout/dropout/random_uniform/sub*
T0*/
_output_shapes
:€€€€€€€€€gg 
Ћ
*conv_layer1_dropout/dropout/random_uniformAdd.conv_layer1_dropout/dropout/random_uniform/mul.conv_layer1_dropout/dropout/random_uniform/min*
T0*/
_output_shapes
:€€€€€€€€€gg 
≥
conv_layer1_dropout/dropout/addAdd%conv_layer1_dropout/dropout/keep_prob*conv_layer1_dropout/dropout/random_uniform*/
_output_shapes
:€€€€€€€€€gg *
T0
Е
!conv_layer1_dropout/dropout/FloorFloorconv_layer1_dropout/dropout/add*
T0*/
_output_shapes
:€€€€€€€€€gg 
Ґ
conv_layer1_dropout/dropout/divRealDivconv_layer1_conv/Relu%conv_layer1_dropout/dropout/keep_prob*
T0*/
_output_shapes
:€€€€€€€€€gg 
§
conv_layer1_dropout/dropout/mulMulconv_layer1_dropout/dropout/div!conv_layer1_dropout/dropout/Floor*
T0*/
_output_shapes
:€€€€€€€€€gg 
—
conv_layer1_pool/MaxPoolMaxPoolconv_layer1_dropout/dropout/mul*
T0*
data_formatNHWC*
strides
*
ksize
*
paddingVALID*/
_output_shapes
:€€€€€€€€€33 
s
conv_layer1_activations/tagConst*
dtype0*
_output_shapes
: *(
valueB Bconv_layer1_activations
К
conv_layer1_activationsHistogramSummaryconv_layer1_activations/tagconv_layer1_dropout/dropout/mul*
T0*
_output_shapes
: 
љ
8conv_layer2_conv/kernel/Initializer/random_uniform/shapeConst*%
valueB"             **
_class 
loc:@conv_layer2_conv/kernel*
dtype0*
_output_shapes
:
Ђ
6conv_layer2_conv/kernel/Initializer/random_uniform/minConst*
valueB 2@D±…н‘µњ**
_class 
loc:@conv_layer2_conv/kernel*
dtype0*
_output_shapes
: 
Ђ
6conv_layer2_conv/kernel/Initializer/random_uniform/maxConst*
valueB 2@D±…н‘µ?**
_class 
loc:@conv_layer2_conv/kernel*
dtype0*
_output_shapes
: 
О
@conv_layer2_conv/kernel/Initializer/random_uniform/RandomUniformRandomUniform8conv_layer2_conv/kernel/Initializer/random_uniform/shape*
dtype0*&
_output_shapes
: *

seed *
T0**
_class 
loc:@conv_layer2_conv/kernel*
seed2 
ъ
6conv_layer2_conv/kernel/Initializer/random_uniform/subSub6conv_layer2_conv/kernel/Initializer/random_uniform/max6conv_layer2_conv/kernel/Initializer/random_uniform/min*
T0**
_class 
loc:@conv_layer2_conv/kernel*
_output_shapes
: 
Ф
6conv_layer2_conv/kernel/Initializer/random_uniform/mulMul@conv_layer2_conv/kernel/Initializer/random_uniform/RandomUniform6conv_layer2_conv/kernel/Initializer/random_uniform/sub*&
_output_shapes
: *
T0**
_class 
loc:@conv_layer2_conv/kernel
Ж
2conv_layer2_conv/kernel/Initializer/random_uniformAdd6conv_layer2_conv/kernel/Initializer/random_uniform/mul6conv_layer2_conv/kernel/Initializer/random_uniform/min*
T0**
_class 
loc:@conv_layer2_conv/kernel*&
_output_shapes
: 
«
conv_layer2_conv/kernel
VariableV2*
dtype0*&
_output_shapes
: *
shared_name **
_class 
loc:@conv_layer2_conv/kernel*
	container *
shape: 
ы
conv_layer2_conv/kernel/AssignAssignconv_layer2_conv/kernel2conv_layer2_conv/kernel/Initializer/random_uniform*
use_locking(*
T0**
_class 
loc:@conv_layer2_conv/kernel*
validate_shape(*&
_output_shapes
: 
Ю
conv_layer2_conv/kernel/readIdentityconv_layer2_conv/kernel*
T0**
_class 
loc:@conv_layer2_conv/kernel*&
_output_shapes
: 
Ґ
'conv_layer2_conv/bias/Initializer/zerosConst*
valueB2        *(
_class
loc:@conv_layer2_conv/bias*
dtype0*
_output_shapes
:
Ђ
conv_layer2_conv/bias
VariableV2*
shape:*
dtype0*
_output_shapes
:*
shared_name *(
_class
loc:@conv_layer2_conv/bias*
	container 
ё
conv_layer2_conv/bias/AssignAssignconv_layer2_conv/bias'conv_layer2_conv/bias/Initializer/zeros*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:*
use_locking(
М
conv_layer2_conv/bias/readIdentityconv_layer2_conv/bias*
T0*(
_class
loc:@conv_layer2_conv/bias*
_output_shapes
:
o
conv_layer2_conv/dilation_rateConst*
valueB"      *
dtype0*
_output_shapes
:
В
conv_layer2_conv/Conv2DConv2Dconv_layer1_pool/MaxPoolconv_layer2_conv/kernel/read*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingVALID*/
_output_shapes
:€€€€€€€€€//
©
conv_layer2_conv/BiasAddBiasAddconv_layer2_conv/Conv2Dconv_layer2_conv/bias/read*
T0*
data_formatNHWC*/
_output_shapes
:€€€€€€€€€//
q
conv_layer2_conv/ReluReluconv_layer2_conv/BiasAdd*
T0*/
_output_shapes
:€€€€€€€€€//
n
%conv_layer2_dropout/dropout/keep_probConst*
valueB 2Ќћћћћћм?*
dtype0*
_output_shapes
: 
v
!conv_layer2_dropout/dropout/ShapeShapeconv_layer2_conv/Relu*
_output_shapes
:*
T0*
out_type0
w
.conv_layer2_dropout/dropout/random_uniform/minConst*
valueB 2        *
dtype0*
_output_shapes
: 
w
.conv_layer2_dropout/dropout/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB 2      р?
ћ
8conv_layer2_dropout/dropout/random_uniform/RandomUniformRandomUniform!conv_layer2_dropout/dropout/Shape*
dtype0*
seed2 */
_output_shapes
:€€€€€€€€€//*

seed *
T0
ґ
.conv_layer2_dropout/dropout/random_uniform/subSub.conv_layer2_dropout/dropout/random_uniform/max.conv_layer2_dropout/dropout/random_uniform/min*
T0*
_output_shapes
: 
ў
.conv_layer2_dropout/dropout/random_uniform/mulMul8conv_layer2_dropout/dropout/random_uniform/RandomUniform.conv_layer2_dropout/dropout/random_uniform/sub*
T0*/
_output_shapes
:€€€€€€€€€//
Ћ
*conv_layer2_dropout/dropout/random_uniformAdd.conv_layer2_dropout/dropout/random_uniform/mul.conv_layer2_dropout/dropout/random_uniform/min*
T0*/
_output_shapes
:€€€€€€€€€//
≥
conv_layer2_dropout/dropout/addAdd%conv_layer2_dropout/dropout/keep_prob*conv_layer2_dropout/dropout/random_uniform*
T0*/
_output_shapes
:€€€€€€€€€//
Е
!conv_layer2_dropout/dropout/FloorFloorconv_layer2_dropout/dropout/add*
T0*/
_output_shapes
:€€€€€€€€€//
Ґ
conv_layer2_dropout/dropout/divRealDivconv_layer2_conv/Relu%conv_layer2_dropout/dropout/keep_prob*/
_output_shapes
:€€€€€€€€€//*
T0
§
conv_layer2_dropout/dropout/mulMulconv_layer2_dropout/dropout/div!conv_layer2_dropout/dropout/Floor*
T0*/
_output_shapes
:€€€€€€€€€//
—
conv_layer2_pool/MaxPoolMaxPoolconv_layer2_dropout/dropout/mul*/
_output_shapes
:€€€€€€€€€*
T0*
strides
*
data_formatNHWC*
ksize
*
paddingVALID
s
conv_layer2_activations/tagConst*
dtype0*
_output_shapes
: *(
valueB Bconv_layer2_activations
К
conv_layer2_activationsHistogramSummaryconv_layer2_activations/tagconv_layer2_dropout/dropout/mul*
T0*
_output_shapes
: 
^
Reshape/shapeConst*
valueB"€€€€  *
dtype0*
_output_shapes
:
|
ReshapeReshapeconv_layer2_pool/MaxPoolReshape/shape*
T0*
Tshape0*(
_output_shapes
:€€€€€€€€€С
≠
4cnn_outlayer/kernel/Initializer/random_uniform/shapeConst*
valueB"  d   *&
_class
loc:@cnn_outlayer/kernel*
dtype0*
_output_shapes
:
£
2cnn_outlayer/kernel/Initializer/random_uniform/minConst*
valueB 2~B¬Эљ єњ*&
_class
loc:@cnn_outlayer/kernel*
dtype0*
_output_shapes
: 
£
2cnn_outlayer/kernel/Initializer/random_uniform/maxConst*
valueB 2~B¬Эљ є?*&
_class
loc:@cnn_outlayer/kernel*
dtype0*
_output_shapes
: 
ы
<cnn_outlayer/kernel/Initializer/random_uniform/RandomUniformRandomUniform4cnn_outlayer/kernel/Initializer/random_uniform/shape*

seed *
T0*&
_class
loc:@cnn_outlayer/kernel*
seed2 *
dtype0*
_output_shapes
:	Сd
к
2cnn_outlayer/kernel/Initializer/random_uniform/subSub2cnn_outlayer/kernel/Initializer/random_uniform/max2cnn_outlayer/kernel/Initializer/random_uniform/min*
T0*&
_class
loc:@cnn_outlayer/kernel*
_output_shapes
: 
э
2cnn_outlayer/kernel/Initializer/random_uniform/mulMul<cnn_outlayer/kernel/Initializer/random_uniform/RandomUniform2cnn_outlayer/kernel/Initializer/random_uniform/sub*
T0*&
_class
loc:@cnn_outlayer/kernel*
_output_shapes
:	Сd
п
.cnn_outlayer/kernel/Initializer/random_uniformAdd2cnn_outlayer/kernel/Initializer/random_uniform/mul2cnn_outlayer/kernel/Initializer/random_uniform/min*
T0*&
_class
loc:@cnn_outlayer/kernel*
_output_shapes
:	Сd
±
cnn_outlayer/kernel
VariableV2*
	container *
shape:	Сd*
dtype0*
_output_shapes
:	Сd*
shared_name *&
_class
loc:@cnn_outlayer/kernel
д
cnn_outlayer/kernel/AssignAssigncnn_outlayer/kernel.cnn_outlayer/kernel/Initializer/random_uniform*
validate_shape(*
_output_shapes
:	Сd*
use_locking(*
T0*&
_class
loc:@cnn_outlayer/kernel
Л
cnn_outlayer/kernel/readIdentitycnn_outlayer/kernel*
T0*&
_class
loc:@cnn_outlayer/kernel*
_output_shapes
:	Сd
Ъ
#cnn_outlayer/bias/Initializer/zerosConst*
valueBd2        *$
_class
loc:@cnn_outlayer/bias*
dtype0*
_output_shapes
:d
£
cnn_outlayer/bias
VariableV2*$
_class
loc:@cnn_outlayer/bias*
	container *
shape:d*
dtype0*
_output_shapes
:d*
shared_name 
ќ
cnn_outlayer/bias/AssignAssigncnn_outlayer/bias#cnn_outlayer/bias/Initializer/zeros*
use_locking(*
T0*$
_class
loc:@cnn_outlayer/bias*
validate_shape(*
_output_shapes
:d
А
cnn_outlayer/bias/readIdentitycnn_outlayer/bias*
_output_shapes
:d*
T0*$
_class
loc:@cnn_outlayer/bias
Ш
cnn_outlayer/MatMulMatMulReshapecnn_outlayer/kernel/read*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€d*
transpose_b( 
Х
cnn_outlayer/BiasAddBiasAddcnn_outlayer/MatMulcnn_outlayer/bias/read*
T0*
data_formatNHWC*'
_output_shapes
:€€€€€€€€€d
e
cnn_outlayer/LeakyRelu/alphaConst*
valueB 2ЪЩЩЩЩЩ…?*
dtype0*
_output_shapes
: 
З
cnn_outlayer/LeakyRelu/mulMulcnn_outlayer/LeakyRelu/alphacnn_outlayer/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€d
Е
cnn_outlayer/LeakyReluMaximumcnn_outlayer/LeakyRelu/mulcnn_outlayer/BiasAdd*'
_output_shapes
:€€€€€€€€€d*
T0
љ
<head_step_fc_setup_1/kernel/Initializer/random_uniform/shapeConst*
valueB"      *.
_class$
" loc:@head_step_fc_setup_1/kernel*
dtype0*
_output_shapes
:
≥
:head_step_fc_setup_1/kernel/Initializer/random_uniform/minConst*
valueB 2      ањ*.
_class$
" loc:@head_step_fc_setup_1/kernel*
dtype0*
_output_shapes
: 
≥
:head_step_fc_setup_1/kernel/Initializer/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB 2      а?*.
_class$
" loc:@head_step_fc_setup_1/kernel
Т
Dhead_step_fc_setup_1/kernel/Initializer/random_uniform/RandomUniformRandomUniform<head_step_fc_setup_1/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes

:*

seed *
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
seed2 
К
:head_step_fc_setup_1/kernel/Initializer/random_uniform/subSub:head_step_fc_setup_1/kernel/Initializer/random_uniform/max:head_step_fc_setup_1/kernel/Initializer/random_uniform/min*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
_output_shapes
: 
Ь
:head_step_fc_setup_1/kernel/Initializer/random_uniform/mulMulDhead_step_fc_setup_1/kernel/Initializer/random_uniform/RandomUniform:head_step_fc_setup_1/kernel/Initializer/random_uniform/sub*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
_output_shapes

:
О
6head_step_fc_setup_1/kernel/Initializer/random_uniformAdd:head_step_fc_setup_1/kernel/Initializer/random_uniform/mul:head_step_fc_setup_1/kernel/Initializer/random_uniform/min*
_output_shapes

:*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel
њ
head_step_fc_setup_1/kernel
VariableV2*
dtype0*
_output_shapes

:*
shared_name *.
_class$
" loc:@head_step_fc_setup_1/kernel*
	container *
shape
:
Г
"head_step_fc_setup_1/kernel/AssignAssignhead_step_fc_setup_1/kernel6head_step_fc_setup_1/kernel/Initializer/random_uniform*
validate_shape(*
_output_shapes

:*
use_locking(*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel
Ґ
 head_step_fc_setup_1/kernel/readIdentityhead_step_fc_setup_1/kernel*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
_output_shapes

:
™
+head_step_fc_setup_1/bias/Initializer/zerosConst*
valueB2        *,
_class"
 loc:@head_step_fc_setup_1/bias*
dtype0*
_output_shapes
:
≥
head_step_fc_setup_1/bias
VariableV2*
shared_name *,
_class"
 loc:@head_step_fc_setup_1/bias*
	container *
shape:*
dtype0*
_output_shapes
:
о
 head_step_fc_setup_1/bias/AssignAssignhead_step_fc_setup_1/bias+head_step_fc_setup_1/bias/Initializer/zeros*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias
Ш
head_step_fc_setup_1/bias/readIdentityhead_step_fc_setup_1/bias*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
_output_shapes
:
ѓ
head_step_fc_setup_1/MatMulMatMulhead_step_inpt head_step_fc_setup_1/kernel/read*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€*
transpose_b( 
≠
head_step_fc_setup_1/BiasAddBiasAddhead_step_fc_setup_1/MatMulhead_step_fc_setup_1/bias/read*
T0*
data_formatNHWC*'
_output_shapes
:€€€€€€€€€
m
$head_step_fc_setup_1/LeakyRelu/alphaConst*
valueB 2ЪЩЩЩЩЩ…?*
dtype0*
_output_shapes
: 
Я
"head_step_fc_setup_1/LeakyRelu/mulMul$head_step_fc_setup_1/LeakyRelu/alphahead_step_fc_setup_1/BiasAdd*'
_output_shapes
:€€€€€€€€€*
T0
Э
head_step_fc_setup_1/LeakyReluMaximum"head_step_fc_setup_1/LeakyRelu/mulhead_step_fc_setup_1/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€
w
.head_step_fc_setup_1_dropout/dropout/keep_probConst*
valueB 2ffffffж?*
dtype0*
_output_shapes
: 
И
*head_step_fc_setup_1_dropout/dropout/ShapeShapehead_step_fc_setup_1/LeakyRelu*
T0*
out_type0*
_output_shapes
:
А
7head_step_fc_setup_1_dropout/dropout/random_uniform/minConst*
valueB 2        *
dtype0*
_output_shapes
: 
А
7head_step_fc_setup_1_dropout/dropout/random_uniform/maxConst*
valueB 2      р?*
dtype0*
_output_shapes
: 
÷
Ahead_step_fc_setup_1_dropout/dropout/random_uniform/RandomUniformRandomUniform*head_step_fc_setup_1_dropout/dropout/Shape*

seed *
T0*
dtype0*
seed2 *'
_output_shapes
:€€€€€€€€€
—
7head_step_fc_setup_1_dropout/dropout/random_uniform/subSub7head_step_fc_setup_1_dropout/dropout/random_uniform/max7head_step_fc_setup_1_dropout/dropout/random_uniform/min*
T0*
_output_shapes
: 
м
7head_step_fc_setup_1_dropout/dropout/random_uniform/mulMulAhead_step_fc_setup_1_dropout/dropout/random_uniform/RandomUniform7head_step_fc_setup_1_dropout/dropout/random_uniform/sub*
T0*'
_output_shapes
:€€€€€€€€€
ё
3head_step_fc_setup_1_dropout/dropout/random_uniformAdd7head_step_fc_setup_1_dropout/dropout/random_uniform/mul7head_step_fc_setup_1_dropout/dropout/random_uniform/min*
T0*'
_output_shapes
:€€€€€€€€€
∆
(head_step_fc_setup_1_dropout/dropout/addAdd.head_step_fc_setup_1_dropout/dropout/keep_prob3head_step_fc_setup_1_dropout/dropout/random_uniform*
T0*'
_output_shapes
:€€€€€€€€€
П
*head_step_fc_setup_1_dropout/dropout/FloorFloor(head_step_fc_setup_1_dropout/dropout/add*
T0*'
_output_shapes
:€€€€€€€€€
µ
(head_step_fc_setup_1_dropout/dropout/divRealDivhead_step_fc_setup_1/LeakyRelu.head_step_fc_setup_1_dropout/dropout/keep_prob*'
_output_shapes
:€€€€€€€€€*
T0
Ј
(head_step_fc_setup_1_dropout/dropout/mulMul(head_step_fc_setup_1_dropout/dropout/div*head_step_fc_setup_1_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€
≠
4head_step_fc/kernel/Initializer/random_uniform/shapeConst*
valueB"      *&
_class
loc:@head_step_fc/kernel*
dtype0*
_output_shapes
:
£
2head_step_fc/kernel/Initializer/random_uniform/minConst*
valueB 2Hr?ф~…Ўњ*&
_class
loc:@head_step_fc/kernel*
dtype0*
_output_shapes
: 
£
2head_step_fc/kernel/Initializer/random_uniform/maxConst*
valueB 2Hr?ф~…Ў?*&
_class
loc:@head_step_fc/kernel*
dtype0*
_output_shapes
: 
ъ
<head_step_fc/kernel/Initializer/random_uniform/RandomUniformRandomUniform4head_step_fc/kernel/Initializer/random_uniform/shape*
seed2 *
dtype0*
_output_shapes

:*

seed *
T0*&
_class
loc:@head_step_fc/kernel
к
2head_step_fc/kernel/Initializer/random_uniform/subSub2head_step_fc/kernel/Initializer/random_uniform/max2head_step_fc/kernel/Initializer/random_uniform/min*
T0*&
_class
loc:@head_step_fc/kernel*
_output_shapes
: 
ь
2head_step_fc/kernel/Initializer/random_uniform/mulMul<head_step_fc/kernel/Initializer/random_uniform/RandomUniform2head_step_fc/kernel/Initializer/random_uniform/sub*
_output_shapes

:*
T0*&
_class
loc:@head_step_fc/kernel
о
.head_step_fc/kernel/Initializer/random_uniformAdd2head_step_fc/kernel/Initializer/random_uniform/mul2head_step_fc/kernel/Initializer/random_uniform/min*
T0*&
_class
loc:@head_step_fc/kernel*
_output_shapes

:
ѓ
head_step_fc/kernel
VariableV2*
dtype0*
_output_shapes

:*
shared_name *&
_class
loc:@head_step_fc/kernel*
	container *
shape
:
г
head_step_fc/kernel/AssignAssignhead_step_fc/kernel.head_step_fc/kernel/Initializer/random_uniform*
validate_shape(*
_output_shapes

:*
use_locking(*
T0*&
_class
loc:@head_step_fc/kernel
К
head_step_fc/kernel/readIdentityhead_step_fc/kernel*
T0*&
_class
loc:@head_step_fc/kernel*
_output_shapes

:
Ъ
#head_step_fc/bias/Initializer/zerosConst*
valueB2        *$
_class
loc:@head_step_fc/bias*
dtype0*
_output_shapes
:
£
head_step_fc/bias
VariableV2*
	container *
shape:*
dtype0*
_output_shapes
:*
shared_name *$
_class
loc:@head_step_fc/bias
ќ
head_step_fc/bias/AssignAssignhead_step_fc/bias#head_step_fc/bias/Initializer/zeros*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*$
_class
loc:@head_step_fc/bias
А
head_step_fc/bias/readIdentityhead_step_fc/bias*
_output_shapes
:*
T0*$
_class
loc:@head_step_fc/bias
є
head_step_fc/MatMulMatMul(head_step_fc_setup_1_dropout/dropout/mulhead_step_fc/kernel/read*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€*
transpose_b( 
Х
head_step_fc/BiasAddBiasAddhead_step_fc/MatMulhead_step_fc/bias/read*
data_formatNHWC*'
_output_shapes
:€€€€€€€€€*
T0
e
head_step_fc/LeakyRelu/alphaConst*
valueB 2ЪЩЩЩЩЩ…?*
dtype0*
_output_shapes
: 
З
head_step_fc/LeakyRelu/mulMulhead_step_fc/LeakyRelu/alphahead_step_fc/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€
Е
head_step_fc/LeakyReluMaximumhead_step_fc/LeakyRelu/mulhead_step_fc/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€
o
&head_step_fc_dropout/dropout/keep_probConst*
valueB 2ffffffж?*
dtype0*
_output_shapes
: 
x
"head_step_fc_dropout/dropout/ShapeShapehead_step_fc/LeakyRelu*
_output_shapes
:*
T0*
out_type0
x
/head_step_fc_dropout/dropout/random_uniform/minConst*
valueB 2        *
dtype0*
_output_shapes
: 
x
/head_step_fc_dropout/dropout/random_uniform/maxConst*
valueB 2      р?*
dtype0*
_output_shapes
: 
∆
9head_step_fc_dropout/dropout/random_uniform/RandomUniformRandomUniform"head_step_fc_dropout/dropout/Shape*

seed *
T0*
dtype0*
seed2 *'
_output_shapes
:€€€€€€€€€
є
/head_step_fc_dropout/dropout/random_uniform/subSub/head_step_fc_dropout/dropout/random_uniform/max/head_step_fc_dropout/dropout/random_uniform/min*
_output_shapes
: *
T0
‘
/head_step_fc_dropout/dropout/random_uniform/mulMul9head_step_fc_dropout/dropout/random_uniform/RandomUniform/head_step_fc_dropout/dropout/random_uniform/sub*
T0*'
_output_shapes
:€€€€€€€€€
∆
+head_step_fc_dropout/dropout/random_uniformAdd/head_step_fc_dropout/dropout/random_uniform/mul/head_step_fc_dropout/dropout/random_uniform/min*
T0*'
_output_shapes
:€€€€€€€€€
Ѓ
 head_step_fc_dropout/dropout/addAdd&head_step_fc_dropout/dropout/keep_prob+head_step_fc_dropout/dropout/random_uniform*
T0*'
_output_shapes
:€€€€€€€€€

"head_step_fc_dropout/dropout/FloorFloor head_step_fc_dropout/dropout/add*
T0*'
_output_shapes
:€€€€€€€€€
Э
 head_step_fc_dropout/dropout/divRealDivhead_step_fc/LeakyRelu&head_step_fc_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€
Я
 head_step_fc_dropout/dropout/mulMul head_step_fc_dropout/dropout/div"head_step_fc_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€
M
concat/axisConst*
value	B :*
dtype0*
_output_shapes
: 
°
concatConcatV2 head_step_fc_dropout/dropout/mul
twist_inptwrench_inptconcat/axis*
T0*
N*'
_output_shapes
:€€€€€€€€€*

Tidx0
≈
@vect_expand_dense_layer1/kernel/Initializer/random_uniform/shapeConst*
valueB"   d   *2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
dtype0*
_output_shapes
:
ї
>vect_expand_dense_layer1/kernel/Initializer/random_uniform/minConst*
dtype0*
_output_shapes
: *
valueB 28ЬыVРоЋњ*2
_class(
&$loc:@vect_expand_dense_layer1/kernel
ї
>vect_expand_dense_layer1/kernel/Initializer/random_uniform/maxConst*
valueB 28ЬыVРоЋ?*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
dtype0*
_output_shapes
: 
Ю
Hvect_expand_dense_layer1/kernel/Initializer/random_uniform/RandomUniformRandomUniform@vect_expand_dense_layer1/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes

:d*

seed *
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
seed2 
Ъ
>vect_expand_dense_layer1/kernel/Initializer/random_uniform/subSub>vect_expand_dense_layer1/kernel/Initializer/random_uniform/max>vect_expand_dense_layer1/kernel/Initializer/random_uniform/min*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
_output_shapes
: 
ђ
>vect_expand_dense_layer1/kernel/Initializer/random_uniform/mulMulHvect_expand_dense_layer1/kernel/Initializer/random_uniform/RandomUniform>vect_expand_dense_layer1/kernel/Initializer/random_uniform/sub*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
_output_shapes

:d
Ю
:vect_expand_dense_layer1/kernel/Initializer/random_uniformAdd>vect_expand_dense_layer1/kernel/Initializer/random_uniform/mul>vect_expand_dense_layer1/kernel/Initializer/random_uniform/min*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
_output_shapes

:d
«
vect_expand_dense_layer1/kernel
VariableV2*
	container *
shape
:d*
dtype0*
_output_shapes

:d*
shared_name *2
_class(
&$loc:@vect_expand_dense_layer1/kernel
У
&vect_expand_dense_layer1/kernel/AssignAssignvect_expand_dense_layer1/kernel:vect_expand_dense_layer1/kernel/Initializer/random_uniform*
validate_shape(*
_output_shapes

:d*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel
Ѓ
$vect_expand_dense_layer1/kernel/readIdentityvect_expand_dense_layer1/kernel*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
_output_shapes

:d
≤
/vect_expand_dense_layer1/bias/Initializer/zerosConst*
valueBd2        *0
_class&
$"loc:@vect_expand_dense_layer1/bias*
dtype0*
_output_shapes
:d
ї
vect_expand_dense_layer1/bias
VariableV2*
shape:d*
dtype0*
_output_shapes
:d*
shared_name *0
_class&
$"loc:@vect_expand_dense_layer1/bias*
	container 
ю
$vect_expand_dense_layer1/bias/AssignAssignvect_expand_dense_layer1/bias/vect_expand_dense_layer1/bias/Initializer/zeros*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
validate_shape(*
_output_shapes
:d
§
"vect_expand_dense_layer1/bias/readIdentityvect_expand_dense_layer1/bias*
_output_shapes
:d*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias
ѓ
vect_expand_dense_layer1/MatMulMatMulconcat$vect_expand_dense_layer1/kernel/read*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€d*
transpose_b( 
є
 vect_expand_dense_layer1/BiasAddBiasAddvect_expand_dense_layer1/MatMul"vect_expand_dense_layer1/bias/read*
T0*
data_formatNHWC*'
_output_shapes
:€€€€€€€€€d
q
(vect_expand_dense_layer1/LeakyRelu/alphaConst*
valueB 2ЪЩЩЩЩЩ…?*
dtype0*
_output_shapes
: 
Ђ
&vect_expand_dense_layer1/LeakyRelu/mulMul(vect_expand_dense_layer1/LeakyRelu/alpha vect_expand_dense_layer1/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€d
©
"vect_expand_dense_layer1/LeakyReluMaximum&vect_expand_dense_layer1/LeakyRelu/mul vect_expand_dense_layer1/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€d
{
2vect_expand_dense_layer1_dropout/dropout/keep_probConst*
dtype0*
_output_shapes
: *
valueB 2ffffffж?
Р
.vect_expand_dense_layer1_dropout/dropout/ShapeShape"vect_expand_dense_layer1/LeakyRelu*
T0*
out_type0*
_output_shapes
:
Д
;vect_expand_dense_layer1_dropout/dropout/random_uniform/minConst*
dtype0*
_output_shapes
: *
valueB 2        
Д
;vect_expand_dense_layer1_dropout/dropout/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB 2      р?
ё
Evect_expand_dense_layer1_dropout/dropout/random_uniform/RandomUniformRandomUniform.vect_expand_dense_layer1_dropout/dropout/Shape*

seed *
T0*
dtype0*
seed2 *'
_output_shapes
:€€€€€€€€€d
Ё
;vect_expand_dense_layer1_dropout/dropout/random_uniform/subSub;vect_expand_dense_layer1_dropout/dropout/random_uniform/max;vect_expand_dense_layer1_dropout/dropout/random_uniform/min*
T0*
_output_shapes
: 
ш
;vect_expand_dense_layer1_dropout/dropout/random_uniform/mulMulEvect_expand_dense_layer1_dropout/dropout/random_uniform/RandomUniform;vect_expand_dense_layer1_dropout/dropout/random_uniform/sub*'
_output_shapes
:€€€€€€€€€d*
T0
к
7vect_expand_dense_layer1_dropout/dropout/random_uniformAdd;vect_expand_dense_layer1_dropout/dropout/random_uniform/mul;vect_expand_dense_layer1_dropout/dropout/random_uniform/min*
T0*'
_output_shapes
:€€€€€€€€€d
“
,vect_expand_dense_layer1_dropout/dropout/addAdd2vect_expand_dense_layer1_dropout/dropout/keep_prob7vect_expand_dense_layer1_dropout/dropout/random_uniform*
T0*'
_output_shapes
:€€€€€€€€€d
Ч
.vect_expand_dense_layer1_dropout/dropout/FloorFloor,vect_expand_dense_layer1_dropout/dropout/add*'
_output_shapes
:€€€€€€€€€d*
T0
Ѕ
,vect_expand_dense_layer1_dropout/dropout/divRealDiv"vect_expand_dense_layer1/LeakyRelu2vect_expand_dense_layer1_dropout/dropout/keep_prob*'
_output_shapes
:€€€€€€€€€d*
T0
√
,vect_expand_dense_layer1_dropout/dropout/mulMul,vect_expand_dense_layer1_dropout/dropout/div.vect_expand_dense_layer1_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€d
≈
@vect_expand_dense_layer2/kernel/Initializer/random_uniform/shapeConst*
valueB"d   d   *2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
dtype0*
_output_shapes
:
ї
>vect_expand_dense_layer2/kernel/Initializer/random_uniform/minConst*
dtype0*
_output_shapes
: *
valueB 2"
≠ЖХ+∆њ*2
_class(
&$loc:@vect_expand_dense_layer2/kernel
ї
>vect_expand_dense_layer2/kernel/Initializer/random_uniform/maxConst*
valueB 2"
≠ЖХ+∆?*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
dtype0*
_output_shapes
: 
Ю
Hvect_expand_dense_layer2/kernel/Initializer/random_uniform/RandomUniformRandomUniform@vect_expand_dense_layer2/kernel/Initializer/random_uniform/shape*

seed *
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
seed2 *
dtype0*
_output_shapes

:dd
Ъ
>vect_expand_dense_layer2/kernel/Initializer/random_uniform/subSub>vect_expand_dense_layer2/kernel/Initializer/random_uniform/max>vect_expand_dense_layer2/kernel/Initializer/random_uniform/min*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
_output_shapes
: 
ђ
>vect_expand_dense_layer2/kernel/Initializer/random_uniform/mulMulHvect_expand_dense_layer2/kernel/Initializer/random_uniform/RandomUniform>vect_expand_dense_layer2/kernel/Initializer/random_uniform/sub*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
_output_shapes

:dd
Ю
:vect_expand_dense_layer2/kernel/Initializer/random_uniformAdd>vect_expand_dense_layer2/kernel/Initializer/random_uniform/mul>vect_expand_dense_layer2/kernel/Initializer/random_uniform/min*
_output_shapes

:dd*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel
«
vect_expand_dense_layer2/kernel
VariableV2*
	container *
shape
:dd*
dtype0*
_output_shapes

:dd*
shared_name *2
_class(
&$loc:@vect_expand_dense_layer2/kernel
У
&vect_expand_dense_layer2/kernel/AssignAssignvect_expand_dense_layer2/kernel:vect_expand_dense_layer2/kernel/Initializer/random_uniform*
validate_shape(*
_output_shapes

:dd*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel
Ѓ
$vect_expand_dense_layer2/kernel/readIdentityvect_expand_dense_layer2/kernel*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
_output_shapes

:dd
≤
/vect_expand_dense_layer2/bias/Initializer/zerosConst*
dtype0*
_output_shapes
:d*
valueBd2        *0
_class&
$"loc:@vect_expand_dense_layer2/bias
ї
vect_expand_dense_layer2/bias
VariableV2*
shared_name *0
_class&
$"loc:@vect_expand_dense_layer2/bias*
	container *
shape:d*
dtype0*
_output_shapes
:d
ю
$vect_expand_dense_layer2/bias/AssignAssignvect_expand_dense_layer2/bias/vect_expand_dense_layer2/bias/Initializer/zeros*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
validate_shape(*
_output_shapes
:d
§
"vect_expand_dense_layer2/bias/readIdentityvect_expand_dense_layer2/bias*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
_output_shapes
:d
’
vect_expand_dense_layer2/MatMulMatMul,vect_expand_dense_layer1_dropout/dropout/mul$vect_expand_dense_layer2/kernel/read*
transpose_a( *'
_output_shapes
:€€€€€€€€€d*
transpose_b( *
T0
є
 vect_expand_dense_layer2/BiasAddBiasAddvect_expand_dense_layer2/MatMul"vect_expand_dense_layer2/bias/read*
T0*
data_formatNHWC*'
_output_shapes
:€€€€€€€€€d
q
(vect_expand_dense_layer2/LeakyRelu/alphaConst*
valueB 2ЪЩЩЩЩЩ…?*
dtype0*
_output_shapes
: 
Ђ
&vect_expand_dense_layer2/LeakyRelu/mulMul(vect_expand_dense_layer2/LeakyRelu/alpha vect_expand_dense_layer2/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€d
©
"vect_expand_dense_layer2/LeakyReluMaximum&vect_expand_dense_layer2/LeakyRelu/mul vect_expand_dense_layer2/BiasAdd*'
_output_shapes
:€€€€€€€€€d*
T0
{
2vect_expand_dense_layer2_dropout/dropout/keep_probConst*
valueB 2ffffffж?*
dtype0*
_output_shapes
: 
Р
.vect_expand_dense_layer2_dropout/dropout/ShapeShape"vect_expand_dense_layer2/LeakyRelu*
T0*
out_type0*
_output_shapes
:
Д
;vect_expand_dense_layer2_dropout/dropout/random_uniform/minConst*
valueB 2        *
dtype0*
_output_shapes
: 
Д
;vect_expand_dense_layer2_dropout/dropout/random_uniform/maxConst*
valueB 2      р?*
dtype0*
_output_shapes
: 
ё
Evect_expand_dense_layer2_dropout/dropout/random_uniform/RandomUniformRandomUniform.vect_expand_dense_layer2_dropout/dropout/Shape*
T0*
dtype0*
seed2 *'
_output_shapes
:€€€€€€€€€d*

seed 
Ё
;vect_expand_dense_layer2_dropout/dropout/random_uniform/subSub;vect_expand_dense_layer2_dropout/dropout/random_uniform/max;vect_expand_dense_layer2_dropout/dropout/random_uniform/min*
_output_shapes
: *
T0
ш
;vect_expand_dense_layer2_dropout/dropout/random_uniform/mulMulEvect_expand_dense_layer2_dropout/dropout/random_uniform/RandomUniform;vect_expand_dense_layer2_dropout/dropout/random_uniform/sub*'
_output_shapes
:€€€€€€€€€d*
T0
к
7vect_expand_dense_layer2_dropout/dropout/random_uniformAdd;vect_expand_dense_layer2_dropout/dropout/random_uniform/mul;vect_expand_dense_layer2_dropout/dropout/random_uniform/min*
T0*'
_output_shapes
:€€€€€€€€€d
“
,vect_expand_dense_layer2_dropout/dropout/addAdd2vect_expand_dense_layer2_dropout/dropout/keep_prob7vect_expand_dense_layer2_dropout/dropout/random_uniform*
T0*'
_output_shapes
:€€€€€€€€€d
Ч
.vect_expand_dense_layer2_dropout/dropout/FloorFloor,vect_expand_dense_layer2_dropout/dropout/add*'
_output_shapes
:€€€€€€€€€d*
T0
Ѕ
,vect_expand_dense_layer2_dropout/dropout/divRealDiv"vect_expand_dense_layer2/LeakyRelu2vect_expand_dense_layer2_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€d
√
,vect_expand_dense_layer2_dropout/dropout/mulMul,vect_expand_dense_layer2_dropout/dropout/div.vect_expand_dense_layer2_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€d
O
concat_1/axisConst*
value	B :*
dtype0*
_output_shapes
: 
±
concat_1ConcatV2cnn_outlayer/LeakyRelu,vect_expand_dense_layer2_dropout/dropout/mulconcat_1/axis*
N*(
_output_shapes
:€€€€€€€€€»*

Tidx0*
T0
Ђ
3final_fc_l1/kernel/Initializer/random_uniform/shapeConst*
valueB"»   2   *%
_class
loc:@final_fc_l1/kernel*
dtype0*
_output_shapes
:
°
1final_fc_l1/kernel/Initializer/random_uniform/minConst*
valueB 2”Ѕ2Рe‘√њ*%
_class
loc:@final_fc_l1/kernel*
dtype0*
_output_shapes
: 
°
1final_fc_l1/kernel/Initializer/random_uniform/maxConst*
valueB 2”Ѕ2Рe‘√?*%
_class
loc:@final_fc_l1/kernel*
dtype0*
_output_shapes
: 
ш
;final_fc_l1/kernel/Initializer/random_uniform/RandomUniformRandomUniform3final_fc_l1/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes
:	»2*

seed *
T0*%
_class
loc:@final_fc_l1/kernel*
seed2 
ж
1final_fc_l1/kernel/Initializer/random_uniform/subSub1final_fc_l1/kernel/Initializer/random_uniform/max1final_fc_l1/kernel/Initializer/random_uniform/min*
T0*%
_class
loc:@final_fc_l1/kernel*
_output_shapes
: 
щ
1final_fc_l1/kernel/Initializer/random_uniform/mulMul;final_fc_l1/kernel/Initializer/random_uniform/RandomUniform1final_fc_l1/kernel/Initializer/random_uniform/sub*
T0*%
_class
loc:@final_fc_l1/kernel*
_output_shapes
:	»2
л
-final_fc_l1/kernel/Initializer/random_uniformAdd1final_fc_l1/kernel/Initializer/random_uniform/mul1final_fc_l1/kernel/Initializer/random_uniform/min*
T0*%
_class
loc:@final_fc_l1/kernel*
_output_shapes
:	»2
ѓ
final_fc_l1/kernel
VariableV2*
	container *
shape:	»2*
dtype0*
_output_shapes
:	»2*
shared_name *%
_class
loc:@final_fc_l1/kernel
а
final_fc_l1/kernel/AssignAssignfinal_fc_l1/kernel-final_fc_l1/kernel/Initializer/random_uniform*
use_locking(*
T0*%
_class
loc:@final_fc_l1/kernel*
validate_shape(*
_output_shapes
:	»2
И
final_fc_l1/kernel/readIdentityfinal_fc_l1/kernel*
T0*%
_class
loc:@final_fc_l1/kernel*
_output_shapes
:	»2
Ш
"final_fc_l1/bias/Initializer/zerosConst*
valueB22        *#
_class
loc:@final_fc_l1/bias*
dtype0*
_output_shapes
:2
°
final_fc_l1/bias
VariableV2*
dtype0*
_output_shapes
:2*
shared_name *#
_class
loc:@final_fc_l1/bias*
	container *
shape:2
 
final_fc_l1/bias/AssignAssignfinal_fc_l1/bias"final_fc_l1/bias/Initializer/zeros*
use_locking(*
T0*#
_class
loc:@final_fc_l1/bias*
validate_shape(*
_output_shapes
:2
}
final_fc_l1/bias/readIdentityfinal_fc_l1/bias*
T0*#
_class
loc:@final_fc_l1/bias*
_output_shapes
:2
Ч
final_fc_l1/MatMulMatMulconcat_1final_fc_l1/kernel/read*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€2*
transpose_b( 
Т
final_fc_l1/BiasAddBiasAddfinal_fc_l1/MatMulfinal_fc_l1/bias/read*
data_formatNHWC*'
_output_shapes
:€€€€€€€€€2*
T0
d
final_fc_l1/LeakyRelu/alphaConst*
valueB 2ЪЩЩЩЩЩ…?*
dtype0*
_output_shapes
: 
Д
final_fc_l1/LeakyRelu/mulMulfinal_fc_l1/LeakyRelu/alphafinal_fc_l1/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€2
В
final_fc_l1/LeakyReluMaximumfinal_fc_l1/LeakyRelu/mulfinal_fc_l1/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€2
n
%final_fc_l1_dropout/dropout/keep_probConst*
dtype0*
_output_shapes
: *
valueB 2ffffffж?
v
!final_fc_l1_dropout/dropout/ShapeShapefinal_fc_l1/LeakyRelu*
_output_shapes
:*
T0*
out_type0
w
.final_fc_l1_dropout/dropout/random_uniform/minConst*
valueB 2        *
dtype0*
_output_shapes
: 
w
.final_fc_l1_dropout/dropout/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB 2      р?
ƒ
8final_fc_l1_dropout/dropout/random_uniform/RandomUniformRandomUniform!final_fc_l1_dropout/dropout/Shape*
dtype0*
seed2 *'
_output_shapes
:€€€€€€€€€2*

seed *
T0
ґ
.final_fc_l1_dropout/dropout/random_uniform/subSub.final_fc_l1_dropout/dropout/random_uniform/max.final_fc_l1_dropout/dropout/random_uniform/min*
_output_shapes
: *
T0
—
.final_fc_l1_dropout/dropout/random_uniform/mulMul8final_fc_l1_dropout/dropout/random_uniform/RandomUniform.final_fc_l1_dropout/dropout/random_uniform/sub*
T0*'
_output_shapes
:€€€€€€€€€2
√
*final_fc_l1_dropout/dropout/random_uniformAdd.final_fc_l1_dropout/dropout/random_uniform/mul.final_fc_l1_dropout/dropout/random_uniform/min*
T0*'
_output_shapes
:€€€€€€€€€2
Ђ
final_fc_l1_dropout/dropout/addAdd%final_fc_l1_dropout/dropout/keep_prob*final_fc_l1_dropout/dropout/random_uniform*
T0*'
_output_shapes
:€€€€€€€€€2
}
!final_fc_l1_dropout/dropout/FloorFloorfinal_fc_l1_dropout/dropout/add*
T0*'
_output_shapes
:€€€€€€€€€2
Ъ
final_fc_l1_dropout/dropout/divRealDivfinal_fc_l1/LeakyRelu%final_fc_l1_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€2
Ь
final_fc_l1_dropout/dropout/mulMulfinal_fc_l1_dropout/dropout/div!final_fc_l1_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€2
Ђ
3final_fc_l2/kernel/Initializer/random_uniform/shapeConst*
dtype0*
_output_shapes
:*
valueB"2   (   *%
_class
loc:@final_fc_l2/kernel
°
1final_fc_l2/kernel/Initializer/random_uniform/minConst*
valueB 2Џц‘ҐTЖ–њ*%
_class
loc:@final_fc_l2/kernel*
dtype0*
_output_shapes
: 
°
1final_fc_l2/kernel/Initializer/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB 2Џц‘ҐTЖ–?*%
_class
loc:@final_fc_l2/kernel
ч
;final_fc_l2/kernel/Initializer/random_uniform/RandomUniformRandomUniform3final_fc_l2/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes

:2(*

seed *
T0*%
_class
loc:@final_fc_l2/kernel*
seed2 
ж
1final_fc_l2/kernel/Initializer/random_uniform/subSub1final_fc_l2/kernel/Initializer/random_uniform/max1final_fc_l2/kernel/Initializer/random_uniform/min*
T0*%
_class
loc:@final_fc_l2/kernel*
_output_shapes
: 
ш
1final_fc_l2/kernel/Initializer/random_uniform/mulMul;final_fc_l2/kernel/Initializer/random_uniform/RandomUniform1final_fc_l2/kernel/Initializer/random_uniform/sub*
T0*%
_class
loc:@final_fc_l2/kernel*
_output_shapes

:2(
к
-final_fc_l2/kernel/Initializer/random_uniformAdd1final_fc_l2/kernel/Initializer/random_uniform/mul1final_fc_l2/kernel/Initializer/random_uniform/min*
T0*%
_class
loc:@final_fc_l2/kernel*
_output_shapes

:2(
≠
final_fc_l2/kernel
VariableV2*
	container *
shape
:2(*
dtype0*
_output_shapes

:2(*
shared_name *%
_class
loc:@final_fc_l2/kernel
я
final_fc_l2/kernel/AssignAssignfinal_fc_l2/kernel-final_fc_l2/kernel/Initializer/random_uniform*
use_locking(*
T0*%
_class
loc:@final_fc_l2/kernel*
validate_shape(*
_output_shapes

:2(
З
final_fc_l2/kernel/readIdentityfinal_fc_l2/kernel*
T0*%
_class
loc:@final_fc_l2/kernel*
_output_shapes

:2(
Ш
"final_fc_l2/bias/Initializer/zerosConst*
dtype0*
_output_shapes
:(*
valueB(2        *#
_class
loc:@final_fc_l2/bias
°
final_fc_l2/bias
VariableV2*
	container *
shape:(*
dtype0*
_output_shapes
:(*
shared_name *#
_class
loc:@final_fc_l2/bias
 
final_fc_l2/bias/AssignAssignfinal_fc_l2/bias"final_fc_l2/bias/Initializer/zeros*
use_locking(*
T0*#
_class
loc:@final_fc_l2/bias*
validate_shape(*
_output_shapes
:(
}
final_fc_l2/bias/readIdentityfinal_fc_l2/bias*
T0*#
_class
loc:@final_fc_l2/bias*
_output_shapes
:(
Ѓ
final_fc_l2/MatMulMatMulfinal_fc_l1_dropout/dropout/mulfinal_fc_l2/kernel/read*
transpose_a( *'
_output_shapes
:€€€€€€€€€(*
transpose_b( *
T0
Т
final_fc_l2/BiasAddBiasAddfinal_fc_l2/MatMulfinal_fc_l2/bias/read*
T0*
data_formatNHWC*'
_output_shapes
:€€€€€€€€€(
d
final_fc_l2/LeakyRelu/alphaConst*
valueB 2ЪЩЩЩЩЩ…?*
dtype0*
_output_shapes
: 
Д
final_fc_l2/LeakyRelu/mulMulfinal_fc_l2/LeakyRelu/alphafinal_fc_l2/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€(
В
final_fc_l2/LeakyReluMaximumfinal_fc_l2/LeakyRelu/mulfinal_fc_l2/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€(
n
%final_fc_l2_dropout/dropout/keep_probConst*
valueB 2ffffffж?*
dtype0*
_output_shapes
: 
v
!final_fc_l2_dropout/dropout/ShapeShapefinal_fc_l2/LeakyRelu*
T0*
out_type0*
_output_shapes
:
w
.final_fc_l2_dropout/dropout/random_uniform/minConst*
valueB 2        *
dtype0*
_output_shapes
: 
w
.final_fc_l2_dropout/dropout/random_uniform/maxConst*
valueB 2      р?*
dtype0*
_output_shapes
: 
ƒ
8final_fc_l2_dropout/dropout/random_uniform/RandomUniformRandomUniform!final_fc_l2_dropout/dropout/Shape*
T0*
dtype0*
seed2 *'
_output_shapes
:€€€€€€€€€(*

seed 
ґ
.final_fc_l2_dropout/dropout/random_uniform/subSub.final_fc_l2_dropout/dropout/random_uniform/max.final_fc_l2_dropout/dropout/random_uniform/min*
_output_shapes
: *
T0
—
.final_fc_l2_dropout/dropout/random_uniform/mulMul8final_fc_l2_dropout/dropout/random_uniform/RandomUniform.final_fc_l2_dropout/dropout/random_uniform/sub*'
_output_shapes
:€€€€€€€€€(*
T0
√
*final_fc_l2_dropout/dropout/random_uniformAdd.final_fc_l2_dropout/dropout/random_uniform/mul.final_fc_l2_dropout/dropout/random_uniform/min*
T0*'
_output_shapes
:€€€€€€€€€(
Ђ
final_fc_l2_dropout/dropout/addAdd%final_fc_l2_dropout/dropout/keep_prob*final_fc_l2_dropout/dropout/random_uniform*'
_output_shapes
:€€€€€€€€€(*
T0
}
!final_fc_l2_dropout/dropout/FloorFloorfinal_fc_l2_dropout/dropout/add*
T0*'
_output_shapes
:€€€€€€€€€(
Ъ
final_fc_l2_dropout/dropout/divRealDivfinal_fc_l2/LeakyRelu%final_fc_l2_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€(
Ь
final_fc_l2_dropout/dropout/mulMulfinal_fc_l2_dropout/dropout/div!final_fc_l2_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€(
Ђ
3final_fc_l3/kernel/Initializer/random_uniform/shapeConst*
valueB"(   
   *%
_class
loc:@final_fc_l3/kernel*
dtype0*
_output_shapes
:
°
1final_fc_l3/kernel/Initializer/random_uniform/minConst*
valueB 2"
≠ЖХ+÷њ*%
_class
loc:@final_fc_l3/kernel*
dtype0*
_output_shapes
: 
°
1final_fc_l3/kernel/Initializer/random_uniform/maxConst*
valueB 2"
≠ЖХ+÷?*%
_class
loc:@final_fc_l3/kernel*
dtype0*
_output_shapes
: 
ч
;final_fc_l3/kernel/Initializer/random_uniform/RandomUniformRandomUniform3final_fc_l3/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes

:(
*

seed *
T0*%
_class
loc:@final_fc_l3/kernel*
seed2 
ж
1final_fc_l3/kernel/Initializer/random_uniform/subSub1final_fc_l3/kernel/Initializer/random_uniform/max1final_fc_l3/kernel/Initializer/random_uniform/min*
T0*%
_class
loc:@final_fc_l3/kernel*
_output_shapes
: 
ш
1final_fc_l3/kernel/Initializer/random_uniform/mulMul;final_fc_l3/kernel/Initializer/random_uniform/RandomUniform1final_fc_l3/kernel/Initializer/random_uniform/sub*
_output_shapes

:(
*
T0*%
_class
loc:@final_fc_l3/kernel
к
-final_fc_l3/kernel/Initializer/random_uniformAdd1final_fc_l3/kernel/Initializer/random_uniform/mul1final_fc_l3/kernel/Initializer/random_uniform/min*
T0*%
_class
loc:@final_fc_l3/kernel*
_output_shapes

:(

≠
final_fc_l3/kernel
VariableV2*
shape
:(
*
dtype0*
_output_shapes

:(
*
shared_name *%
_class
loc:@final_fc_l3/kernel*
	container 
я
final_fc_l3/kernel/AssignAssignfinal_fc_l3/kernel-final_fc_l3/kernel/Initializer/random_uniform*
use_locking(*
T0*%
_class
loc:@final_fc_l3/kernel*
validate_shape(*
_output_shapes

:(

З
final_fc_l3/kernel/readIdentityfinal_fc_l3/kernel*
_output_shapes

:(
*
T0*%
_class
loc:@final_fc_l3/kernel
Ш
"final_fc_l3/bias/Initializer/zerosConst*
valueB
2        *#
_class
loc:@final_fc_l3/bias*
dtype0*
_output_shapes
:

°
final_fc_l3/bias
VariableV2*
shared_name *#
_class
loc:@final_fc_l3/bias*
	container *
shape:
*
dtype0*
_output_shapes
:

 
final_fc_l3/bias/AssignAssignfinal_fc_l3/bias"final_fc_l3/bias/Initializer/zeros*
T0*#
_class
loc:@final_fc_l3/bias*
validate_shape(*
_output_shapes
:
*
use_locking(
}
final_fc_l3/bias/readIdentityfinal_fc_l3/bias*
_output_shapes
:
*
T0*#
_class
loc:@final_fc_l3/bias
Ѓ
final_fc_l3/MatMulMatMulfinal_fc_l2_dropout/dropout/mulfinal_fc_l3/kernel/read*
transpose_a( *'
_output_shapes
:€€€€€€€€€
*
transpose_b( *
T0
Т
final_fc_l3/BiasAddBiasAddfinal_fc_l3/MatMulfinal_fc_l3/bias/read*
T0*
data_formatNHWC*'
_output_shapes
:€€€€€€€€€

d
final_fc_l3/LeakyRelu/alphaConst*
valueB 2ЪЩЩЩЩЩ…?*
dtype0*
_output_shapes
: 
Д
final_fc_l3/LeakyRelu/mulMulfinal_fc_l3/LeakyRelu/alphafinal_fc_l3/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€

В
final_fc_l3/LeakyReluMaximumfinal_fc_l3/LeakyRelu/mulfinal_fc_l3/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€

n
%final_fc_l3_dropout/dropout/keep_probConst*
dtype0*
_output_shapes
: *
valueB 2ffffffж?
v
!final_fc_l3_dropout/dropout/ShapeShapefinal_fc_l3/LeakyRelu*
T0*
out_type0*
_output_shapes
:
w
.final_fc_l3_dropout/dropout/random_uniform/minConst*
dtype0*
_output_shapes
: *
valueB 2        
w
.final_fc_l3_dropout/dropout/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB 2      р?
ƒ
8final_fc_l3_dropout/dropout/random_uniform/RandomUniformRandomUniform!final_fc_l3_dropout/dropout/Shape*
dtype0*
seed2 *'
_output_shapes
:€€€€€€€€€
*

seed *
T0
ґ
.final_fc_l3_dropout/dropout/random_uniform/subSub.final_fc_l3_dropout/dropout/random_uniform/max.final_fc_l3_dropout/dropout/random_uniform/min*
T0*
_output_shapes
: 
—
.final_fc_l3_dropout/dropout/random_uniform/mulMul8final_fc_l3_dropout/dropout/random_uniform/RandomUniform.final_fc_l3_dropout/dropout/random_uniform/sub*
T0*'
_output_shapes
:€€€€€€€€€

√
*final_fc_l3_dropout/dropout/random_uniformAdd.final_fc_l3_dropout/dropout/random_uniform/mul.final_fc_l3_dropout/dropout/random_uniform/min*'
_output_shapes
:€€€€€€€€€
*
T0
Ђ
final_fc_l3_dropout/dropout/addAdd%final_fc_l3_dropout/dropout/keep_prob*final_fc_l3_dropout/dropout/random_uniform*
T0*'
_output_shapes
:€€€€€€€€€

}
!final_fc_l3_dropout/dropout/FloorFloorfinal_fc_l3_dropout/dropout/add*
T0*'
_output_shapes
:€€€€€€€€€

Ъ
final_fc_l3_dropout/dropout/divRealDivfinal_fc_l3/LeakyRelu%final_fc_l3_dropout/dropout/keep_prob*'
_output_shapes
:€€€€€€€€€
*
T0
Ь
final_fc_l3_dropout/dropout/mulMulfinal_fc_l3_dropout/dropout/div!final_fc_l3_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€

Ђ
3final_fc_l4/kernel/Initializer/random_uniform/shapeConst*
valueB"
   
   *%
_class
loc:@final_fc_l4/kernel*
dtype0*
_output_shapes
:
°
1final_fc_l4/kernel/Initializer/random_uniform/minConst*
valueB 2sДшtсЖбњ*%
_class
loc:@final_fc_l4/kernel*
dtype0*
_output_shapes
: 
°
1final_fc_l4/kernel/Initializer/random_uniform/maxConst*
valueB 2sДшtсЖб?*%
_class
loc:@final_fc_l4/kernel*
dtype0*
_output_shapes
: 
ч
;final_fc_l4/kernel/Initializer/random_uniform/RandomUniformRandomUniform3final_fc_l4/kernel/Initializer/random_uniform/shape*
seed2 *
dtype0*
_output_shapes

:

*

seed *
T0*%
_class
loc:@final_fc_l4/kernel
ж
1final_fc_l4/kernel/Initializer/random_uniform/subSub1final_fc_l4/kernel/Initializer/random_uniform/max1final_fc_l4/kernel/Initializer/random_uniform/min*
T0*%
_class
loc:@final_fc_l4/kernel*
_output_shapes
: 
ш
1final_fc_l4/kernel/Initializer/random_uniform/mulMul;final_fc_l4/kernel/Initializer/random_uniform/RandomUniform1final_fc_l4/kernel/Initializer/random_uniform/sub*
_output_shapes

:

*
T0*%
_class
loc:@final_fc_l4/kernel
к
-final_fc_l4/kernel/Initializer/random_uniformAdd1final_fc_l4/kernel/Initializer/random_uniform/mul1final_fc_l4/kernel/Initializer/random_uniform/min*
T0*%
_class
loc:@final_fc_l4/kernel*
_output_shapes

:


≠
final_fc_l4/kernel
VariableV2*
shared_name *%
_class
loc:@final_fc_l4/kernel*
	container *
shape
:

*
dtype0*
_output_shapes

:


я
final_fc_l4/kernel/AssignAssignfinal_fc_l4/kernel-final_fc_l4/kernel/Initializer/random_uniform*
validate_shape(*
_output_shapes

:

*
use_locking(*
T0*%
_class
loc:@final_fc_l4/kernel
З
final_fc_l4/kernel/readIdentityfinal_fc_l4/kernel*
T0*%
_class
loc:@final_fc_l4/kernel*
_output_shapes

:


Ш
"final_fc_l4/bias/Initializer/zerosConst*
valueB
2        *#
_class
loc:@final_fc_l4/bias*
dtype0*
_output_shapes
:

°
final_fc_l4/bias
VariableV2*#
_class
loc:@final_fc_l4/bias*
	container *
shape:
*
dtype0*
_output_shapes
:
*
shared_name 
 
final_fc_l4/bias/AssignAssignfinal_fc_l4/bias"final_fc_l4/bias/Initializer/zeros*
use_locking(*
T0*#
_class
loc:@final_fc_l4/bias*
validate_shape(*
_output_shapes
:

}
final_fc_l4/bias/readIdentityfinal_fc_l4/bias*
T0*#
_class
loc:@final_fc_l4/bias*
_output_shapes
:

Ѓ
final_fc_l4/MatMulMatMulfinal_fc_l3_dropout/dropout/mulfinal_fc_l4/kernel/read*
transpose_b( *
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€

Т
final_fc_l4/BiasAddBiasAddfinal_fc_l4/MatMulfinal_fc_l4/bias/read*
data_formatNHWC*'
_output_shapes
:€€€€€€€€€
*
T0
d
final_fc_l4/LeakyRelu/alphaConst*
valueB 2ЪЩЩЩЩЩ…?*
dtype0*
_output_shapes
: 
Д
final_fc_l4/LeakyRelu/mulMulfinal_fc_l4/LeakyRelu/alphafinal_fc_l4/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€

В
final_fc_l4/LeakyReluMaximumfinal_fc_l4/LeakyRelu/mulfinal_fc_l4/BiasAdd*'
_output_shapes
:€€€€€€€€€
*
T0
n
%final_fc_l4_dropout/dropout/keep_probConst*
valueB 2ffffffж?*
dtype0*
_output_shapes
: 
v
!final_fc_l4_dropout/dropout/ShapeShapefinal_fc_l4/LeakyRelu*
T0*
out_type0*
_output_shapes
:
w
.final_fc_l4_dropout/dropout/random_uniform/minConst*
valueB 2        *
dtype0*
_output_shapes
: 
w
.final_fc_l4_dropout/dropout/random_uniform/maxConst*
valueB 2      р?*
dtype0*
_output_shapes
: 
ƒ
8final_fc_l4_dropout/dropout/random_uniform/RandomUniformRandomUniform!final_fc_l4_dropout/dropout/Shape*
dtype0*
seed2 *'
_output_shapes
:€€€€€€€€€
*

seed *
T0
ґ
.final_fc_l4_dropout/dropout/random_uniform/subSub.final_fc_l4_dropout/dropout/random_uniform/max.final_fc_l4_dropout/dropout/random_uniform/min*
T0*
_output_shapes
: 
—
.final_fc_l4_dropout/dropout/random_uniform/mulMul8final_fc_l4_dropout/dropout/random_uniform/RandomUniform.final_fc_l4_dropout/dropout/random_uniform/sub*
T0*'
_output_shapes
:€€€€€€€€€

√
*final_fc_l4_dropout/dropout/random_uniformAdd.final_fc_l4_dropout/dropout/random_uniform/mul.final_fc_l4_dropout/dropout/random_uniform/min*
T0*'
_output_shapes
:€€€€€€€€€

Ђ
final_fc_l4_dropout/dropout/addAdd%final_fc_l4_dropout/dropout/keep_prob*final_fc_l4_dropout/dropout/random_uniform*
T0*'
_output_shapes
:€€€€€€€€€

}
!final_fc_l4_dropout/dropout/FloorFloorfinal_fc_l4_dropout/dropout/add*
T0*'
_output_shapes
:€€€€€€€€€

Ъ
final_fc_l4_dropout/dropout/divRealDivfinal_fc_l4/LeakyRelu%final_fc_l4_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€

Ь
final_fc_l4_dropout/dropout/mulMulfinal_fc_l4_dropout/dropout/div!final_fc_l4_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€

°
.output/kernel/Initializer/random_uniform/shapeConst*
valueB"
      * 
_class
loc:@output/kernel*
dtype0*
_output_shapes
:
Ч
,output/kernel/Initializer/random_uniform/minConst*
valueB 2Он_:^љењ* 
_class
loc:@output/kernel*
dtype0*
_output_shapes
: 
Ч
,output/kernel/Initializer/random_uniform/maxConst*
valueB 2Он_:^ље?* 
_class
loc:@output/kernel*
dtype0*
_output_shapes
: 
и
6output/kernel/Initializer/random_uniform/RandomUniformRandomUniform.output/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes

:
*

seed *
T0* 
_class
loc:@output/kernel*
seed2 
“
,output/kernel/Initializer/random_uniform/subSub,output/kernel/Initializer/random_uniform/max,output/kernel/Initializer/random_uniform/min*
T0* 
_class
loc:@output/kernel*
_output_shapes
: 
д
,output/kernel/Initializer/random_uniform/mulMul6output/kernel/Initializer/random_uniform/RandomUniform,output/kernel/Initializer/random_uniform/sub*
T0* 
_class
loc:@output/kernel*
_output_shapes

:

÷
(output/kernel/Initializer/random_uniformAdd,output/kernel/Initializer/random_uniform/mul,output/kernel/Initializer/random_uniform/min*
T0* 
_class
loc:@output/kernel*
_output_shapes

:

£
output/kernel
VariableV2*
	container *
shape
:
*
dtype0*
_output_shapes

:
*
shared_name * 
_class
loc:@output/kernel
Ћ
output/kernel/AssignAssignoutput/kernel(output/kernel/Initializer/random_uniform*
use_locking(*
T0* 
_class
loc:@output/kernel*
validate_shape(*
_output_shapes

:

x
output/kernel/readIdentityoutput/kernel*
_output_shapes

:
*
T0* 
_class
loc:@output/kernel
О
output/bias/Initializer/zerosConst*
valueB2        *
_class
loc:@output/bias*
dtype0*
_output_shapes
:
Ч
output/bias
VariableV2*
shape:*
dtype0*
_output_shapes
:*
shared_name *
_class
loc:@output/bias*
	container 
ґ
output/bias/AssignAssignoutput/biasoutput/bias/Initializer/zeros*
use_locking(*
T0*
_class
loc:@output/bias*
validate_shape(*
_output_shapes
:
n
output/bias/readIdentityoutput/bias*
_output_shapes
:*
T0*
_class
loc:@output/bias
§
output/MatMulMatMulfinal_fc_l4_dropout/dropout/muloutput/kernel/read*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€*
transpose_b( 
Г
output/BiasAddBiasAddoutput/MatMuloutput/bias/read*
T0*
data_formatNHWC*'
_output_shapes
:€€€€€€€€€
h
labelPlaceholder*
dtype0*'
_output_shapes
:€€€€€€€€€*
shape:€€€€€€€€€
m
accuracy/strided_slice/stackConst*
valueB"       *
dtype0*
_output_shapes
:
o
accuracy/strided_slice/stack_1Const*
dtype0*
_output_shapes
:*
valueB"       
o
accuracy/strided_slice/stack_2Const*
valueB"      *
dtype0*
_output_shapes
:
™
accuracy/strided_sliceStridedSlicelabelaccuracy/strided_slice/stackaccuracy/strided_slice/stack_1accuracy/strided_slice/stack_2*
shrink_axis_mask*
ellipsis_mask *

begin_mask*
new_axis_mask *
end_mask*#
_output_shapes
:€€€€€€€€€*
T0*
Index0
Y
accuracy/SinSinaccuracy/strided_slice*
T0*#
_output_shapes
:€€€€€€€€€
o
accuracy/strided_slice_1/stackConst*
valueB"       *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_1/stack_1Const*
valueB"       *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_1/stack_2Const*
valueB"      *
dtype0*
_output_shapes
:
ї
accuracy/strided_slice_1StridedSliceoutput/BiasAddaccuracy/strided_slice_1/stack accuracy/strided_slice_1/stack_1 accuracy/strided_slice_1/stack_2*
shrink_axis_mask*
ellipsis_mask *

begin_mask*
new_axis_mask *
end_mask*#
_output_shapes
:€€€€€€€€€*
T0*
Index0
[
accuracy/CosCosaccuracy/strided_slice_1*#
_output_shapes
:€€€€€€€€€*
T0
]
accuracy/mulMulaccuracy/Sinaccuracy/Cos*
T0*#
_output_shapes
:€€€€€€€€€
o
accuracy/strided_slice_2/stackConst*
valueB"       *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_2/stack_1Const*
dtype0*
_output_shapes
:*
valueB"       
q
 accuracy/strided_slice_2/stack_2Const*
valueB"      *
dtype0*
_output_shapes
:
≤
accuracy/strided_slice_2StridedSlicelabelaccuracy/strided_slice_2/stack accuracy/strided_slice_2/stack_1 accuracy/strided_slice_2/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*#
_output_shapes
:€€€€€€€€€
]
accuracy/Cos_1Cosaccuracy/strided_slice_2*
T0*#
_output_shapes
:€€€€€€€€€
o
accuracy/strided_slice_3/stackConst*
valueB"       *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_3/stack_1Const*
valueB"       *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_3/stack_2Const*
valueB"      *
dtype0*
_output_shapes
:
ї
accuracy/strided_slice_3StridedSliceoutput/BiasAddaccuracy/strided_slice_3/stack accuracy/strided_slice_3/stack_1 accuracy/strided_slice_3/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*#
_output_shapes
:€€€€€€€€€
]
accuracy/Sin_1Sinaccuracy/strided_slice_3*#
_output_shapes
:€€€€€€€€€*
T0
c
accuracy/mul_1Mulaccuracy/Cos_1accuracy/Sin_1*
T0*#
_output_shapes
:€€€€€€€€€
_
accuracy/subSubaccuracy/mulaccuracy/mul_1*
T0*#
_output_shapes
:€€€€€€€€€
o
accuracy/strided_slice_4/stackConst*
valueB"       *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_4/stack_1Const*
valueB"       *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_4/stack_2Const*
valueB"      *
dtype0*
_output_shapes
:
≤
accuracy/strided_slice_4StridedSlicelabelaccuracy/strided_slice_4/stack accuracy/strided_slice_4/stack_1 accuracy/strided_slice_4/stack_2*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*#
_output_shapes
:€€€€€€€€€*
T0*
Index0*
shrink_axis_mask
]
accuracy/Cos_2Cosaccuracy/strided_slice_4*
T0*#
_output_shapes
:€€€€€€€€€
o
accuracy/strided_slice_5/stackConst*
dtype0*
_output_shapes
:*
valueB"       
q
 accuracy/strided_slice_5/stack_1Const*
dtype0*
_output_shapes
:*
valueB"       
q
 accuracy/strided_slice_5/stack_2Const*
valueB"      *
dtype0*
_output_shapes
:
ї
accuracy/strided_slice_5StridedSliceoutput/BiasAddaccuracy/strided_slice_5/stack accuracy/strided_slice_5/stack_1 accuracy/strided_slice_5/stack_2*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*#
_output_shapes
:€€€€€€€€€*
Index0*
T0*
shrink_axis_mask
]
accuracy/Cos_3Cosaccuracy/strided_slice_5*
T0*#
_output_shapes
:€€€€€€€€€
c
accuracy/mul_2Mulaccuracy/Cos_2accuracy/Cos_3*#
_output_shapes
:€€€€€€€€€*
T0
o
accuracy/strided_slice_6/stackConst*
valueB"       *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_6/stack_1Const*
valueB"       *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_6/stack_2Const*
valueB"      *
dtype0*
_output_shapes
:
≤
accuracy/strided_slice_6StridedSlicelabelaccuracy/strided_slice_6/stack accuracy/strided_slice_6/stack_1 accuracy/strided_slice_6/stack_2*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*#
_output_shapes
:€€€€€€€€€*
Index0*
T0
]
accuracy/Sin_2Sinaccuracy/strided_slice_6*
T0*#
_output_shapes
:€€€€€€€€€
o
accuracy/strided_slice_7/stackConst*
valueB"       *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_7/stack_1Const*
valueB"       *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_7/stack_2Const*
valueB"      *
dtype0*
_output_shapes
:
ї
accuracy/strided_slice_7StridedSliceoutput/BiasAddaccuracy/strided_slice_7/stack accuracy/strided_slice_7/stack_1 accuracy/strided_slice_7/stack_2*
shrink_axis_mask*
ellipsis_mask *

begin_mask*
new_axis_mask *
end_mask*#
_output_shapes
:€€€€€€€€€*
Index0*
T0
]
accuracy/Sin_3Sinaccuracy/strided_slice_7*
T0*#
_output_shapes
:€€€€€€€€€
c
accuracy/mul_3Mulaccuracy/Sin_2accuracy/Sin_3*
T0*#
_output_shapes
:€€€€€€€€€
a
accuracy/addAddaccuracy/mul_2accuracy/mul_3*#
_output_shapes
:€€€€€€€€€*
T0
a
accuracy/Atan2Atan2accuracy/subaccuracy/add*
T0*#
_output_shapes
:€€€€€€€€€
Y
accuracy/mul_4/xConst*
valueB 2      >@*
dtype0*
_output_shapes
: 
e
accuracy/mul_4Mulaccuracy/mul_4/xaccuracy/Atan2*#
_output_shapes
:€€€€€€€€€*
T0
W
accuracy/SquareSquareaccuracy/mul_4*
T0*#
_output_shapes
:€€€€€€€€€
a
accuracy/Mean/reduction_indicesConst*
value	B : *
dtype0*
_output_shapes
: 
Е
accuracy/MeanMeanaccuracy/Squareaccuracy/Mean/reduction_indices*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
o
accuracy/strided_slice_8/stackConst*
valueB"        *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_8/stack_1Const*
dtype0*
_output_shapes
:*
valueB"       
q
 accuracy/strided_slice_8/stack_2Const*
valueB"      *
dtype0*
_output_shapes
:
ґ
accuracy/strided_slice_8StridedSlicelabelaccuracy/strided_slice_8/stack accuracy/strided_slice_8/stack_1 accuracy/strided_slice_8/stack_2*
end_mask*'
_output_shapes
:€€€€€€€€€*
T0*
Index0*
shrink_axis_mask *
ellipsis_mask *

begin_mask*
new_axis_mask 
Y
accuracy/mul_5/xConst*
valueB 2      Y@*
dtype0*
_output_shapes
: 
s
accuracy/mul_5Mulaccuracy/mul_5/xaccuracy/strided_slice_8*
T0*'
_output_shapes
:€€€€€€€€€
o
accuracy/strided_slice_9/stackConst*
valueB"        *
dtype0*
_output_shapes
:
q
 accuracy/strided_slice_9/stack_1Const*
dtype0*
_output_shapes
:*
valueB"       
q
 accuracy/strided_slice_9/stack_2Const*
valueB"      *
dtype0*
_output_shapes
:
њ
accuracy/strided_slice_9StridedSliceoutput/BiasAddaccuracy/strided_slice_9/stack accuracy/strided_slice_9/stack_1 accuracy/strided_slice_9/stack_2*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*'
_output_shapes
:€€€€€€€€€*
T0*
Index0*
shrink_axis_mask 
Y
accuracy/mul_6/xConst*
valueB 2      Y@*
dtype0*
_output_shapes
: 
s
accuracy/mul_6Mulaccuracy/mul_6/xaccuracy/strided_slice_9*
T0*'
_output_shapes
:€€€€€€€€€
g
accuracy/sub_1Subaccuracy/mul_5accuracy/mul_6*
T0*'
_output_shapes
:€€€€€€€€€
]
accuracy/Square_1Squareaccuracy/sub_1*
T0*'
_output_shapes
:€€€€€€€€€
`
accuracy/Sum/reduction_indicesConst*
dtype0*
_output_shapes
: *
value	B :
С
accuracy/SumSumaccuracy/Square_1accuracy/Sum/reduction_indices*
T0*#
_output_shapes
:€€€€€€€€€*

Tidx0*
	keep_dims( 
c
!accuracy/Mean_1/reduction_indicesConst*
value	B : *
dtype0*
_output_shapes
: 
Ж
accuracy/Mean_1Meanaccuracy/Sum!accuracy/Mean_1/reduction_indices*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
V
accuracy/add_1Addaccuracy/Meanaccuracy/Mean_1*
T0*
_output_shapes
: 
X
train/gradients/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
b
train/gradients/grad_ys_0Const*
valueB 2      р?*
dtype0*
_output_shapes
: 
Б
train/gradients/FillFilltrain/gradients/Shapetrain/gradients/grad_ys_0*
T0*

index_type0*
_output_shapes
: 
S
4train/gradients/accuracy/add_1_grad/tuple/group_depsNoOp^train/gradients/Fill
я
<train/gradients/accuracy/add_1_grad/tuple/control_dependencyIdentitytrain/gradients/Fill5^train/gradients/accuracy/add_1_grad/tuple/group_deps*
T0*'
_class
loc:@train/gradients/Fill*
_output_shapes
: 
б
>train/gradients/accuracy/add_1_grad/tuple/control_dependency_1Identitytrain/gradients/Fill5^train/gradients/accuracy/add_1_grad/tuple/group_deps*
_output_shapes
: *
T0*'
_class
loc:@train/gradients/Fill
w
(train/gradients/accuracy/Mean_grad/ShapeShapeaccuracy/Square*
T0*
out_type0*
_output_shapes
:
¶
'train/gradients/accuracy/Mean_grad/SizeConst*
dtype0*
_output_shapes
: *
value	B :*;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape
’
&train/gradients/accuracy/Mean_grad/addAddaccuracy/Mean/reduction_indices'train/gradients/accuracy/Mean_grad/Size*
T0*;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
_output_shapes
: 
б
&train/gradients/accuracy/Mean_grad/modFloorMod&train/gradients/accuracy/Mean_grad/add'train/gradients/accuracy/Mean_grad/Size*
T0*;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
_output_shapes
: 
™
*train/gradients/accuracy/Mean_grad/Shape_1Const*
valueB *;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
dtype0*
_output_shapes
: 
≠
.train/gradients/accuracy/Mean_grad/range/startConst*
value	B : *;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
dtype0*
_output_shapes
: 
≠
.train/gradients/accuracy/Mean_grad/range/deltaConst*
value	B :*;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
dtype0*
_output_shapes
: 
Я
(train/gradients/accuracy/Mean_grad/rangeRange.train/gradients/accuracy/Mean_grad/range/start'train/gradients/accuracy/Mean_grad/Size.train/gradients/accuracy/Mean_grad/range/delta*

Tidx0*;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
_output_shapes
:
ђ
-train/gradients/accuracy/Mean_grad/Fill/valueConst*
value	B :*;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
dtype0*
_output_shapes
: 
ъ
'train/gradients/accuracy/Mean_grad/FillFill*train/gradients/accuracy/Mean_grad/Shape_1-train/gradients/accuracy/Mean_grad/Fill/value*
T0*

index_type0*;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
_output_shapes
: 
—
0train/gradients/accuracy/Mean_grad/DynamicStitchDynamicStitch(train/gradients/accuracy/Mean_grad/range&train/gradients/accuracy/Mean_grad/mod(train/gradients/accuracy/Mean_grad/Shape'train/gradients/accuracy/Mean_grad/Fill*
T0*;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
N*
_output_shapes
:
Ђ
,train/gradients/accuracy/Mean_grad/Maximum/yConst*
value	B :*;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
dtype0*
_output_shapes
: 
ч
*train/gradients/accuracy/Mean_grad/MaximumMaximum0train/gradients/accuracy/Mean_grad/DynamicStitch,train/gradients/accuracy/Mean_grad/Maximum/y*
T0*;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
_output_shapes
:
п
+train/gradients/accuracy/Mean_grad/floordivFloorDiv(train/gradients/accuracy/Mean_grad/Shape*train/gradients/accuracy/Mean_grad/Maximum*
T0*;
_class1
/-loc:@train/gradients/accuracy/Mean_grad/Shape*
_output_shapes
:
Ў
*train/gradients/accuracy/Mean_grad/ReshapeReshape<train/gradients/accuracy/add_1_grad/tuple/control_dependency0train/gradients/accuracy/Mean_grad/DynamicStitch*
T0*
Tshape0*
_output_shapes
:
»
'train/gradients/accuracy/Mean_grad/TileTile*train/gradients/accuracy/Mean_grad/Reshape+train/gradients/accuracy/Mean_grad/floordiv*

Tmultiples0*
T0*#
_output_shapes
:€€€€€€€€€
y
*train/gradients/accuracy/Mean_grad/Shape_2Shapeaccuracy/Square*
T0*
out_type0*
_output_shapes
:
m
*train/gradients/accuracy/Mean_grad/Shape_3Const*
valueB *
dtype0*
_output_shapes
: 
r
(train/gradients/accuracy/Mean_grad/ConstConst*
dtype0*
_output_shapes
:*
valueB: 
√
'train/gradients/accuracy/Mean_grad/ProdProd*train/gradients/accuracy/Mean_grad/Shape_2(train/gradients/accuracy/Mean_grad/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
t
*train/gradients/accuracy/Mean_grad/Const_1Const*
valueB: *
dtype0*
_output_shapes
:
«
)train/gradients/accuracy/Mean_grad/Prod_1Prod*train/gradients/accuracy/Mean_grad/Shape_3*train/gradients/accuracy/Mean_grad/Const_1*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
p
.train/gradients/accuracy/Mean_grad/Maximum_1/yConst*
value	B :*
dtype0*
_output_shapes
: 
≥
,train/gradients/accuracy/Mean_grad/Maximum_1Maximum)train/gradients/accuracy/Mean_grad/Prod_1.train/gradients/accuracy/Mean_grad/Maximum_1/y*
_output_shapes
: *
T0
±
-train/gradients/accuracy/Mean_grad/floordiv_1FloorDiv'train/gradients/accuracy/Mean_grad/Prod,train/gradients/accuracy/Mean_grad/Maximum_1*
T0*
_output_shapes
: 
Ю
'train/gradients/accuracy/Mean_grad/CastCast-train/gradients/accuracy/Mean_grad/floordiv_1*

SrcT0*
Truncate( *

DstT0*
_output_shapes
: 
µ
*train/gradients/accuracy/Mean_grad/truedivRealDiv'train/gradients/accuracy/Mean_grad/Tile'train/gradients/accuracy/Mean_grad/Cast*#
_output_shapes
:€€€€€€€€€*
T0
v
*train/gradients/accuracy/Mean_1_grad/ShapeShapeaccuracy/Sum*
_output_shapes
:*
T0*
out_type0
™
)train/gradients/accuracy/Mean_1_grad/SizeConst*
value	B :*=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape*
dtype0*
_output_shapes
: 
Ё
(train/gradients/accuracy/Mean_1_grad/addAdd!accuracy/Mean_1/reduction_indices)train/gradients/accuracy/Mean_1_grad/Size*
T0*=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape*
_output_shapes
: 
й
(train/gradients/accuracy/Mean_1_grad/modFloorMod(train/gradients/accuracy/Mean_1_grad/add)train/gradients/accuracy/Mean_1_grad/Size*
T0*=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape*
_output_shapes
: 
Ѓ
,train/gradients/accuracy/Mean_1_grad/Shape_1Const*
valueB *=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape*
dtype0*
_output_shapes
: 
±
0train/gradients/accuracy/Mean_1_grad/range/startConst*
dtype0*
_output_shapes
: *
value	B : *=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape
±
0train/gradients/accuracy/Mean_1_grad/range/deltaConst*
value	B :*=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape*
dtype0*
_output_shapes
: 
©
*train/gradients/accuracy/Mean_1_grad/rangeRange0train/gradients/accuracy/Mean_1_grad/range/start)train/gradients/accuracy/Mean_1_grad/Size0train/gradients/accuracy/Mean_1_grad/range/delta*=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape*
_output_shapes
:*

Tidx0
∞
/train/gradients/accuracy/Mean_1_grad/Fill/valueConst*
value	B :*=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape*
dtype0*
_output_shapes
: 
В
)train/gradients/accuracy/Mean_1_grad/FillFill,train/gradients/accuracy/Mean_1_grad/Shape_1/train/gradients/accuracy/Mean_1_grad/Fill/value*
T0*

index_type0*=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape*
_output_shapes
: 
Ё
2train/gradients/accuracy/Mean_1_grad/DynamicStitchDynamicStitch*train/gradients/accuracy/Mean_1_grad/range(train/gradients/accuracy/Mean_1_grad/mod*train/gradients/accuracy/Mean_1_grad/Shape)train/gradients/accuracy/Mean_1_grad/Fill*
N*
_output_shapes
:*
T0*=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape
ѓ
.train/gradients/accuracy/Mean_1_grad/Maximum/yConst*
value	B :*=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape*
dtype0*
_output_shapes
: 
€
,train/gradients/accuracy/Mean_1_grad/MaximumMaximum2train/gradients/accuracy/Mean_1_grad/DynamicStitch.train/gradients/accuracy/Mean_1_grad/Maximum/y*
_output_shapes
:*
T0*=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape
ч
-train/gradients/accuracy/Mean_1_grad/floordivFloorDiv*train/gradients/accuracy/Mean_1_grad/Shape,train/gradients/accuracy/Mean_1_grad/Maximum*
T0*=
_class3
1/loc:@train/gradients/accuracy/Mean_1_grad/Shape*
_output_shapes
:
ё
,train/gradients/accuracy/Mean_1_grad/ReshapeReshape>train/gradients/accuracy/add_1_grad/tuple/control_dependency_12train/gradients/accuracy/Mean_1_grad/DynamicStitch*
T0*
Tshape0*
_output_shapes
:
ќ
)train/gradients/accuracy/Mean_1_grad/TileTile,train/gradients/accuracy/Mean_1_grad/Reshape-train/gradients/accuracy/Mean_1_grad/floordiv*

Tmultiples0*
T0*#
_output_shapes
:€€€€€€€€€
x
,train/gradients/accuracy/Mean_1_grad/Shape_2Shapeaccuracy/Sum*
_output_shapes
:*
T0*
out_type0
o
,train/gradients/accuracy/Mean_1_grad/Shape_3Const*
valueB *
dtype0*
_output_shapes
: 
t
*train/gradients/accuracy/Mean_1_grad/ConstConst*
valueB: *
dtype0*
_output_shapes
:
…
)train/gradients/accuracy/Mean_1_grad/ProdProd,train/gradients/accuracy/Mean_1_grad/Shape_2*train/gradients/accuracy/Mean_1_grad/Const*

Tidx0*
	keep_dims( *
T0*
_output_shapes
: 
v
,train/gradients/accuracy/Mean_1_grad/Const_1Const*
valueB: *
dtype0*
_output_shapes
:
Ќ
+train/gradients/accuracy/Mean_1_grad/Prod_1Prod,train/gradients/accuracy/Mean_1_grad/Shape_3,train/gradients/accuracy/Mean_1_grad/Const_1*
_output_shapes
: *

Tidx0*
	keep_dims( *
T0
r
0train/gradients/accuracy/Mean_1_grad/Maximum_1/yConst*
value	B :*
dtype0*
_output_shapes
: 
є
.train/gradients/accuracy/Mean_1_grad/Maximum_1Maximum+train/gradients/accuracy/Mean_1_grad/Prod_10train/gradients/accuracy/Mean_1_grad/Maximum_1/y*
T0*
_output_shapes
: 
Ј
/train/gradients/accuracy/Mean_1_grad/floordiv_1FloorDiv)train/gradients/accuracy/Mean_1_grad/Prod.train/gradients/accuracy/Mean_1_grad/Maximum_1*
T0*
_output_shapes
: 
Ґ
)train/gradients/accuracy/Mean_1_grad/CastCast/train/gradients/accuracy/Mean_1_grad/floordiv_1*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0
ї
,train/gradients/accuracy/Mean_1_grad/truedivRealDiv)train/gradients/accuracy/Mean_1_grad/Tile)train/gradients/accuracy/Mean_1_grad/Cast*
T0*#
_output_shapes
:€€€€€€€€€
†
*train/gradients/accuracy/Square_grad/ConstConst+^train/gradients/accuracy/Mean_grad/truediv*
valueB 2       @*
dtype0*
_output_shapes
: 
Щ
(train/gradients/accuracy/Square_grad/MulMulaccuracy/mul_4*train/gradients/accuracy/Square_grad/Const*
T0*#
_output_shapes
:€€€€€€€€€
µ
*train/gradients/accuracy/Square_grad/Mul_1Mul*train/gradients/accuracy/Mean_grad/truediv(train/gradients/accuracy/Square_grad/Mul*
T0*#
_output_shapes
:€€€€€€€€€
x
'train/gradients/accuracy/Sum_grad/ShapeShapeaccuracy/Square_1*
T0*
out_type0*
_output_shapes
:
§
&train/gradients/accuracy/Sum_grad/SizeConst*
value	B :*:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape*
dtype0*
_output_shapes
: 
—
%train/gradients/accuracy/Sum_grad/addAddaccuracy/Sum/reduction_indices&train/gradients/accuracy/Sum_grad/Size*
T0*:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape*
_output_shapes
: 
Ё
%train/gradients/accuracy/Sum_grad/modFloorMod%train/gradients/accuracy/Sum_grad/add&train/gradients/accuracy/Sum_grad/Size*
T0*:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape*
_output_shapes
: 
®
)train/gradients/accuracy/Sum_grad/Shape_1Const*
valueB *:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape*
dtype0*
_output_shapes
: 
Ђ
-train/gradients/accuracy/Sum_grad/range/startConst*
value	B : *:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape*
dtype0*
_output_shapes
: 
Ђ
-train/gradients/accuracy/Sum_grad/range/deltaConst*
dtype0*
_output_shapes
: *
value	B :*:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape
Ъ
'train/gradients/accuracy/Sum_grad/rangeRange-train/gradients/accuracy/Sum_grad/range/start&train/gradients/accuracy/Sum_grad/Size-train/gradients/accuracy/Sum_grad/range/delta*:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape*
_output_shapes
:*

Tidx0
™
,train/gradients/accuracy/Sum_grad/Fill/valueConst*
value	B :*:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape*
dtype0*
_output_shapes
: 
ц
&train/gradients/accuracy/Sum_grad/FillFill)train/gradients/accuracy/Sum_grad/Shape_1,train/gradients/accuracy/Sum_grad/Fill/value*
T0*

index_type0*:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape*
_output_shapes
: 
Ћ
/train/gradients/accuracy/Sum_grad/DynamicStitchDynamicStitch'train/gradients/accuracy/Sum_grad/range%train/gradients/accuracy/Sum_grad/mod'train/gradients/accuracy/Sum_grad/Shape&train/gradients/accuracy/Sum_grad/Fill*
T0*:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape*
N*
_output_shapes
:
©
+train/gradients/accuracy/Sum_grad/Maximum/yConst*
value	B :*:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape*
dtype0*
_output_shapes
: 
у
)train/gradients/accuracy/Sum_grad/MaximumMaximum/train/gradients/accuracy/Sum_grad/DynamicStitch+train/gradients/accuracy/Sum_grad/Maximum/y*
T0*:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape*
_output_shapes
:
л
*train/gradients/accuracy/Sum_grad/floordivFloorDiv'train/gradients/accuracy/Sum_grad/Shape)train/gradients/accuracy/Sum_grad/Maximum*
_output_shapes
:*
T0*:
_class0
.,loc:@train/gradients/accuracy/Sum_grad/Shape
№
)train/gradients/accuracy/Sum_grad/ReshapeReshape,train/gradients/accuracy/Mean_1_grad/truediv/train/gradients/accuracy/Sum_grad/DynamicStitch*
T0*
Tshape0*0
_output_shapes
:€€€€€€€€€€€€€€€€€€
…
&train/gradients/accuracy/Sum_grad/TileTile)train/gradients/accuracy/Sum_grad/Reshape*train/gradients/accuracy/Sum_grad/floordiv*
T0*'
_output_shapes
:€€€€€€€€€*

Tmultiples0
l
)train/gradients/accuracy/mul_4_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
y
+train/gradients/accuracy/mul_4_grad/Shape_1Shapeaccuracy/Atan2*
T0*
out_type0*
_output_shapes
:
з
9train/gradients/accuracy/mul_4_grad/BroadcastGradientArgsBroadcastGradientArgs)train/gradients/accuracy/mul_4_grad/Shape+train/gradients/accuracy/mul_4_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
Ш
'train/gradients/accuracy/mul_4_grad/MulMul*train/gradients/accuracy/Square_grad/Mul_1accuracy/Atan2*#
_output_shapes
:€€€€€€€€€*
T0
“
'train/gradients/accuracy/mul_4_grad/SumSum'train/gradients/accuracy/mul_4_grad/Mul9train/gradients/accuracy/mul_4_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
є
+train/gradients/accuracy/mul_4_grad/ReshapeReshape'train/gradients/accuracy/mul_4_grad/Sum)train/gradients/accuracy/mul_4_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
Ь
)train/gradients/accuracy/mul_4_grad/Mul_1Mulaccuracy/mul_4/x*train/gradients/accuracy/Square_grad/Mul_1*
T0*#
_output_shapes
:€€€€€€€€€
Ў
)train/gradients/accuracy/mul_4_grad/Sum_1Sum)train/gradients/accuracy/mul_4_grad/Mul_1;train/gradients/accuracy/mul_4_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ћ
-train/gradients/accuracy/mul_4_grad/Reshape_1Reshape)train/gradients/accuracy/mul_4_grad/Sum_1+train/gradients/accuracy/mul_4_grad/Shape_1*
T0*
Tshape0*#
_output_shapes
:€€€€€€€€€
Ъ
4train/gradients/accuracy/mul_4_grad/tuple/group_depsNoOp,^train/gradients/accuracy/mul_4_grad/Reshape.^train/gradients/accuracy/mul_4_grad/Reshape_1
Н
<train/gradients/accuracy/mul_4_grad/tuple/control_dependencyIdentity+train/gradients/accuracy/mul_4_grad/Reshape5^train/gradients/accuracy/mul_4_grad/tuple/group_deps*
T0*>
_class4
20loc:@train/gradients/accuracy/mul_4_grad/Reshape*
_output_shapes
: 
†
>train/gradients/accuracy/mul_4_grad/tuple/control_dependency_1Identity-train/gradients/accuracy/mul_4_grad/Reshape_15^train/gradients/accuracy/mul_4_grad/tuple/group_deps*
T0*@
_class6
42loc:@train/gradients/accuracy/mul_4_grad/Reshape_1*#
_output_shapes
:€€€€€€€€€
Ю
,train/gradients/accuracy/Square_1_grad/ConstConst'^train/gradients/accuracy/Sum_grad/Tile*
valueB 2       @*
dtype0*
_output_shapes
: 
°
*train/gradients/accuracy/Square_1_grad/MulMulaccuracy/sub_1,train/gradients/accuracy/Square_1_grad/Const*'
_output_shapes
:€€€€€€€€€*
T0
є
,train/gradients/accuracy/Square_1_grad/Mul_1Mul&train/gradients/accuracy/Sum_grad/Tile*train/gradients/accuracy/Square_1_grad/Mul*
T0*'
_output_shapes
:€€€€€€€€€
±
*train/gradients/accuracy/Atan2_grad/SquareSquareaccuracy/add?^train/gradients/accuracy/mul_4_grad/tuple/control_dependency_1*
T0*#
_output_shapes
:€€€€€€€€€
≥
,train/gradients/accuracy/Atan2_grad/Square_1Squareaccuracy/sub?^train/gradients/accuracy/mul_4_grad/tuple/control_dependency_1*
T0*#
_output_shapes
:€€€€€€€€€
ґ
'train/gradients/accuracy/Atan2_grad/addAdd*train/gradients/accuracy/Atan2_grad/Square,train/gradients/accuracy/Atan2_grad/Square_1*
T0*#
_output_shapes
:€€€€€€€€€
Ќ
+train/gradients/accuracy/Atan2_grad/truedivRealDiv>train/gradients/accuracy/mul_4_grad/tuple/control_dependency_1'train/gradients/accuracy/Atan2_grad/add*
T0*#
_output_shapes
:€€€€€€€€€
Ч
'train/gradients/accuracy/Atan2_grad/mulMulaccuracy/add+train/gradients/accuracy/Atan2_grad/truediv*
T0*#
_output_shapes
:€€€€€€€€€
Ђ
'train/gradients/accuracy/Atan2_grad/NegNegaccuracy/sub?^train/gradients/accuracy/mul_4_grad/tuple/control_dependency_1*
T0*#
_output_shapes
:€€€€€€€€€
і
)train/gradients/accuracy/Atan2_grad/mul_1Mul'train/gradients/accuracy/Atan2_grad/Neg+train/gradients/accuracy/Atan2_grad/truediv*
T0*#
_output_shapes
:€€€€€€€€€
Т
4train/gradients/accuracy/Atan2_grad/tuple/group_depsNoOp(^train/gradients/accuracy/Atan2_grad/mul*^train/gradients/accuracy/Atan2_grad/mul_1
Т
<train/gradients/accuracy/Atan2_grad/tuple/control_dependencyIdentity'train/gradients/accuracy/Atan2_grad/mul5^train/gradients/accuracy/Atan2_grad/tuple/group_deps*
T0*:
_class0
.,loc:@train/gradients/accuracy/Atan2_grad/mul*#
_output_shapes
:€€€€€€€€€
Ш
>train/gradients/accuracy/Atan2_grad/tuple/control_dependency_1Identity)train/gradients/accuracy/Atan2_grad/mul_15^train/gradients/accuracy/Atan2_grad/tuple/group_deps*
T0*<
_class2
0.loc:@train/gradients/accuracy/Atan2_grad/mul_1*#
_output_shapes
:€€€€€€€€€
w
)train/gradients/accuracy/sub_1_grad/ShapeShapeaccuracy/mul_5*
T0*
out_type0*
_output_shapes
:
y
+train/gradients/accuracy/sub_1_grad/Shape_1Shapeaccuracy/mul_6*
T0*
out_type0*
_output_shapes
:
з
9train/gradients/accuracy/sub_1_grad/BroadcastGradientArgsBroadcastGradientArgs)train/gradients/accuracy/sub_1_grad/Shape+train/gradients/accuracy/sub_1_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
„
'train/gradients/accuracy/sub_1_grad/SumSum,train/gradients/accuracy/Square_1_grad/Mul_19train/gradients/accuracy/sub_1_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
 
+train/gradients/accuracy/sub_1_grad/ReshapeReshape'train/gradients/accuracy/sub_1_grad/Sum)train/gradients/accuracy/sub_1_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
џ
)train/gradients/accuracy/sub_1_grad/Sum_1Sum,train/gradients/accuracy/Square_1_grad/Mul_1;train/gradients/accuracy/sub_1_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
|
'train/gradients/accuracy/sub_1_grad/NegNeg)train/gradients/accuracy/sub_1_grad/Sum_1*
T0*
_output_shapes
:
ќ
-train/gradients/accuracy/sub_1_grad/Reshape_1Reshape'train/gradients/accuracy/sub_1_grad/Neg+train/gradients/accuracy/sub_1_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
Ъ
4train/gradients/accuracy/sub_1_grad/tuple/group_depsNoOp,^train/gradients/accuracy/sub_1_grad/Reshape.^train/gradients/accuracy/sub_1_grad/Reshape_1
Ю
<train/gradients/accuracy/sub_1_grad/tuple/control_dependencyIdentity+train/gradients/accuracy/sub_1_grad/Reshape5^train/gradients/accuracy/sub_1_grad/tuple/group_deps*
T0*>
_class4
20loc:@train/gradients/accuracy/sub_1_grad/Reshape*'
_output_shapes
:€€€€€€€€€
§
>train/gradients/accuracy/sub_1_grad/tuple/control_dependency_1Identity-train/gradients/accuracy/sub_1_grad/Reshape_15^train/gradients/accuracy/sub_1_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€*
T0*@
_class6
42loc:@train/gradients/accuracy/sub_1_grad/Reshape_1
s
'train/gradients/accuracy/sub_grad/ShapeShapeaccuracy/mul*
T0*
out_type0*
_output_shapes
:
w
)train/gradients/accuracy/sub_grad/Shape_1Shapeaccuracy/mul_1*
T0*
out_type0*
_output_shapes
:
б
7train/gradients/accuracy/sub_grad/BroadcastGradientArgsBroadcastGradientArgs'train/gradients/accuracy/sub_grad/Shape)train/gradients/accuracy/sub_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
г
%train/gradients/accuracy/sub_grad/SumSum<train/gradients/accuracy/Atan2_grad/tuple/control_dependency7train/gradients/accuracy/sub_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ј
)train/gradients/accuracy/sub_grad/ReshapeReshape%train/gradients/accuracy/sub_grad/Sum'train/gradients/accuracy/sub_grad/Shape*
T0*
Tshape0*#
_output_shapes
:€€€€€€€€€
з
'train/gradients/accuracy/sub_grad/Sum_1Sum<train/gradients/accuracy/Atan2_grad/tuple/control_dependency9train/gradients/accuracy/sub_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
x
%train/gradients/accuracy/sub_grad/NegNeg'train/gradients/accuracy/sub_grad/Sum_1*
T0*
_output_shapes
:
ƒ
+train/gradients/accuracy/sub_grad/Reshape_1Reshape%train/gradients/accuracy/sub_grad/Neg)train/gradients/accuracy/sub_grad/Shape_1*
T0*
Tshape0*#
_output_shapes
:€€€€€€€€€
Ф
2train/gradients/accuracy/sub_grad/tuple/group_depsNoOp*^train/gradients/accuracy/sub_grad/Reshape,^train/gradients/accuracy/sub_grad/Reshape_1
Т
:train/gradients/accuracy/sub_grad/tuple/control_dependencyIdentity)train/gradients/accuracy/sub_grad/Reshape3^train/gradients/accuracy/sub_grad/tuple/group_deps*#
_output_shapes
:€€€€€€€€€*
T0*<
_class2
0.loc:@train/gradients/accuracy/sub_grad/Reshape
Ш
<train/gradients/accuracy/sub_grad/tuple/control_dependency_1Identity+train/gradients/accuracy/sub_grad/Reshape_13^train/gradients/accuracy/sub_grad/tuple/group_deps*#
_output_shapes
:€€€€€€€€€*
T0*>
_class4
20loc:@train/gradients/accuracy/sub_grad/Reshape_1
u
'train/gradients/accuracy/add_grad/ShapeShapeaccuracy/mul_2*
T0*
out_type0*
_output_shapes
:
w
)train/gradients/accuracy/add_grad/Shape_1Shapeaccuracy/mul_3*
T0*
out_type0*
_output_shapes
:
б
7train/gradients/accuracy/add_grad/BroadcastGradientArgsBroadcastGradientArgs'train/gradients/accuracy/add_grad/Shape)train/gradients/accuracy/add_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
е
%train/gradients/accuracy/add_grad/SumSum>train/gradients/accuracy/Atan2_grad/tuple/control_dependency_17train/gradients/accuracy/add_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
ј
)train/gradients/accuracy/add_grad/ReshapeReshape%train/gradients/accuracy/add_grad/Sum'train/gradients/accuracy/add_grad/Shape*
T0*
Tshape0*#
_output_shapes
:€€€€€€€€€
й
'train/gradients/accuracy/add_grad/Sum_1Sum>train/gradients/accuracy/Atan2_grad/tuple/control_dependency_19train/gradients/accuracy/add_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
∆
+train/gradients/accuracy/add_grad/Reshape_1Reshape'train/gradients/accuracy/add_grad/Sum_1)train/gradients/accuracy/add_grad/Shape_1*
T0*
Tshape0*#
_output_shapes
:€€€€€€€€€
Ф
2train/gradients/accuracy/add_grad/tuple/group_depsNoOp*^train/gradients/accuracy/add_grad/Reshape,^train/gradients/accuracy/add_grad/Reshape_1
Т
:train/gradients/accuracy/add_grad/tuple/control_dependencyIdentity)train/gradients/accuracy/add_grad/Reshape3^train/gradients/accuracy/add_grad/tuple/group_deps*#
_output_shapes
:€€€€€€€€€*
T0*<
_class2
0.loc:@train/gradients/accuracy/add_grad/Reshape
Ш
<train/gradients/accuracy/add_grad/tuple/control_dependency_1Identity+train/gradients/accuracy/add_grad/Reshape_13^train/gradients/accuracy/add_grad/tuple/group_deps*
T0*>
_class4
20loc:@train/gradients/accuracy/add_grad/Reshape_1*#
_output_shapes
:€€€€€€€€€
l
)train/gradients/accuracy/mul_6_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
Г
+train/gradients/accuracy/mul_6_grad/Shape_1Shapeaccuracy/strided_slice_9*
T0*
out_type0*
_output_shapes
:
з
9train/gradients/accuracy/mul_6_grad/BroadcastGradientArgsBroadcastGradientArgs)train/gradients/accuracy/mul_6_grad/Shape+train/gradients/accuracy/mul_6_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
Ї
'train/gradients/accuracy/mul_6_grad/MulMul>train/gradients/accuracy/sub_1_grad/tuple/control_dependency_1accuracy/strided_slice_9*
T0*'
_output_shapes
:€€€€€€€€€
“
'train/gradients/accuracy/mul_6_grad/SumSum'train/gradients/accuracy/mul_6_grad/Mul9train/gradients/accuracy/mul_6_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
є
+train/gradients/accuracy/mul_6_grad/ReshapeReshape'train/gradients/accuracy/mul_6_grad/Sum)train/gradients/accuracy/mul_6_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
і
)train/gradients/accuracy/mul_6_grad/Mul_1Mulaccuracy/mul_6/x>train/gradients/accuracy/sub_1_grad/tuple/control_dependency_1*
T0*'
_output_shapes
:€€€€€€€€€
Ў
)train/gradients/accuracy/mul_6_grad/Sum_1Sum)train/gradients/accuracy/mul_6_grad/Mul_1;train/gradients/accuracy/mul_6_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
–
-train/gradients/accuracy/mul_6_grad/Reshape_1Reshape)train/gradients/accuracy/mul_6_grad/Sum_1+train/gradients/accuracy/mul_6_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
Ъ
4train/gradients/accuracy/mul_6_grad/tuple/group_depsNoOp,^train/gradients/accuracy/mul_6_grad/Reshape.^train/gradients/accuracy/mul_6_grad/Reshape_1
Н
<train/gradients/accuracy/mul_6_grad/tuple/control_dependencyIdentity+train/gradients/accuracy/mul_6_grad/Reshape5^train/gradients/accuracy/mul_6_grad/tuple/group_deps*
T0*>
_class4
20loc:@train/gradients/accuracy/mul_6_grad/Reshape*
_output_shapes
: 
§
>train/gradients/accuracy/mul_6_grad/tuple/control_dependency_1Identity-train/gradients/accuracy/mul_6_grad/Reshape_15^train/gradients/accuracy/mul_6_grad/tuple/group_deps*
T0*@
_class6
42loc:@train/gradients/accuracy/mul_6_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€
s
'train/gradients/accuracy/mul_grad/ShapeShapeaccuracy/Sin*
T0*
out_type0*
_output_shapes
:
u
)train/gradients/accuracy/mul_grad/Shape_1Shapeaccuracy/Cos*
T0*
out_type0*
_output_shapes
:
б
7train/gradients/accuracy/mul_grad/BroadcastGradientArgsBroadcastGradientArgs'train/gradients/accuracy/mul_grad/Shape)train/gradients/accuracy/mul_grad/Shape_1*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€*
T0
§
%train/gradients/accuracy/mul_grad/MulMul:train/gradients/accuracy/sub_grad/tuple/control_dependencyaccuracy/Cos*
T0*#
_output_shapes
:€€€€€€€€€
ћ
%train/gradients/accuracy/mul_grad/SumSum%train/gradients/accuracy/mul_grad/Mul7train/gradients/accuracy/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ј
)train/gradients/accuracy/mul_grad/ReshapeReshape%train/gradients/accuracy/mul_grad/Sum'train/gradients/accuracy/mul_grad/Shape*
T0*
Tshape0*#
_output_shapes
:€€€€€€€€€
¶
'train/gradients/accuracy/mul_grad/Mul_1Mulaccuracy/Sin:train/gradients/accuracy/sub_grad/tuple/control_dependency*
T0*#
_output_shapes
:€€€€€€€€€
“
'train/gradients/accuracy/mul_grad/Sum_1Sum'train/gradients/accuracy/mul_grad/Mul_19train/gradients/accuracy/mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
∆
+train/gradients/accuracy/mul_grad/Reshape_1Reshape'train/gradients/accuracy/mul_grad/Sum_1)train/gradients/accuracy/mul_grad/Shape_1*#
_output_shapes
:€€€€€€€€€*
T0*
Tshape0
Ф
2train/gradients/accuracy/mul_grad/tuple/group_depsNoOp*^train/gradients/accuracy/mul_grad/Reshape,^train/gradients/accuracy/mul_grad/Reshape_1
Т
:train/gradients/accuracy/mul_grad/tuple/control_dependencyIdentity)train/gradients/accuracy/mul_grad/Reshape3^train/gradients/accuracy/mul_grad/tuple/group_deps*#
_output_shapes
:€€€€€€€€€*
T0*<
_class2
0.loc:@train/gradients/accuracy/mul_grad/Reshape
Ш
<train/gradients/accuracy/mul_grad/tuple/control_dependency_1Identity+train/gradients/accuracy/mul_grad/Reshape_13^train/gradients/accuracy/mul_grad/tuple/group_deps*
T0*>
_class4
20loc:@train/gradients/accuracy/mul_grad/Reshape_1*#
_output_shapes
:€€€€€€€€€
w
)train/gradients/accuracy/mul_1_grad/ShapeShapeaccuracy/Cos_1*
_output_shapes
:*
T0*
out_type0
y
+train/gradients/accuracy/mul_1_grad/Shape_1Shapeaccuracy/Sin_1*
T0*
out_type0*
_output_shapes
:
з
9train/gradients/accuracy/mul_1_grad/BroadcastGradientArgsBroadcastGradientArgs)train/gradients/accuracy/mul_1_grad/Shape+train/gradients/accuracy/mul_1_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
™
'train/gradients/accuracy/mul_1_grad/MulMul<train/gradients/accuracy/sub_grad/tuple/control_dependency_1accuracy/Sin_1*
T0*#
_output_shapes
:€€€€€€€€€
“
'train/gradients/accuracy/mul_1_grad/SumSum'train/gradients/accuracy/mul_1_grad/Mul9train/gradients/accuracy/mul_1_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
∆
+train/gradients/accuracy/mul_1_grad/ReshapeReshape'train/gradients/accuracy/mul_1_grad/Sum)train/gradients/accuracy/mul_1_grad/Shape*
T0*
Tshape0*#
_output_shapes
:€€€€€€€€€
ђ
)train/gradients/accuracy/mul_1_grad/Mul_1Mulaccuracy/Cos_1<train/gradients/accuracy/sub_grad/tuple/control_dependency_1*#
_output_shapes
:€€€€€€€€€*
T0
Ў
)train/gradients/accuracy/mul_1_grad/Sum_1Sum)train/gradients/accuracy/mul_1_grad/Mul_1;train/gradients/accuracy/mul_1_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ћ
-train/gradients/accuracy/mul_1_grad/Reshape_1Reshape)train/gradients/accuracy/mul_1_grad/Sum_1+train/gradients/accuracy/mul_1_grad/Shape_1*
T0*
Tshape0*#
_output_shapes
:€€€€€€€€€
Ъ
4train/gradients/accuracy/mul_1_grad/tuple/group_depsNoOp,^train/gradients/accuracy/mul_1_grad/Reshape.^train/gradients/accuracy/mul_1_grad/Reshape_1
Ъ
<train/gradients/accuracy/mul_1_grad/tuple/control_dependencyIdentity+train/gradients/accuracy/mul_1_grad/Reshape5^train/gradients/accuracy/mul_1_grad/tuple/group_deps*
T0*>
_class4
20loc:@train/gradients/accuracy/mul_1_grad/Reshape*#
_output_shapes
:€€€€€€€€€
†
>train/gradients/accuracy/mul_1_grad/tuple/control_dependency_1Identity-train/gradients/accuracy/mul_1_grad/Reshape_15^train/gradients/accuracy/mul_1_grad/tuple/group_deps*
T0*@
_class6
42loc:@train/gradients/accuracy/mul_1_grad/Reshape_1*#
_output_shapes
:€€€€€€€€€
w
)train/gradients/accuracy/mul_2_grad/ShapeShapeaccuracy/Cos_2*
T0*
out_type0*
_output_shapes
:
y
+train/gradients/accuracy/mul_2_grad/Shape_1Shapeaccuracy/Cos_3*
T0*
out_type0*
_output_shapes
:
з
9train/gradients/accuracy/mul_2_grad/BroadcastGradientArgsBroadcastGradientArgs)train/gradients/accuracy/mul_2_grad/Shape+train/gradients/accuracy/mul_2_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
®
'train/gradients/accuracy/mul_2_grad/MulMul:train/gradients/accuracy/add_grad/tuple/control_dependencyaccuracy/Cos_3*#
_output_shapes
:€€€€€€€€€*
T0
“
'train/gradients/accuracy/mul_2_grad/SumSum'train/gradients/accuracy/mul_2_grad/Mul9train/gradients/accuracy/mul_2_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
∆
+train/gradients/accuracy/mul_2_grad/ReshapeReshape'train/gradients/accuracy/mul_2_grad/Sum)train/gradients/accuracy/mul_2_grad/Shape*
T0*
Tshape0*#
_output_shapes
:€€€€€€€€€
™
)train/gradients/accuracy/mul_2_grad/Mul_1Mulaccuracy/Cos_2:train/gradients/accuracy/add_grad/tuple/control_dependency*
T0*#
_output_shapes
:€€€€€€€€€
Ў
)train/gradients/accuracy/mul_2_grad/Sum_1Sum)train/gradients/accuracy/mul_2_grad/Mul_1;train/gradients/accuracy/mul_2_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ћ
-train/gradients/accuracy/mul_2_grad/Reshape_1Reshape)train/gradients/accuracy/mul_2_grad/Sum_1+train/gradients/accuracy/mul_2_grad/Shape_1*#
_output_shapes
:€€€€€€€€€*
T0*
Tshape0
Ъ
4train/gradients/accuracy/mul_2_grad/tuple/group_depsNoOp,^train/gradients/accuracy/mul_2_grad/Reshape.^train/gradients/accuracy/mul_2_grad/Reshape_1
Ъ
<train/gradients/accuracy/mul_2_grad/tuple/control_dependencyIdentity+train/gradients/accuracy/mul_2_grad/Reshape5^train/gradients/accuracy/mul_2_grad/tuple/group_deps*#
_output_shapes
:€€€€€€€€€*
T0*>
_class4
20loc:@train/gradients/accuracy/mul_2_grad/Reshape
†
>train/gradients/accuracy/mul_2_grad/tuple/control_dependency_1Identity-train/gradients/accuracy/mul_2_grad/Reshape_15^train/gradients/accuracy/mul_2_grad/tuple/group_deps*
T0*@
_class6
42loc:@train/gradients/accuracy/mul_2_grad/Reshape_1*#
_output_shapes
:€€€€€€€€€
w
)train/gradients/accuracy/mul_3_grad/ShapeShapeaccuracy/Sin_2*
_output_shapes
:*
T0*
out_type0
y
+train/gradients/accuracy/mul_3_grad/Shape_1Shapeaccuracy/Sin_3*
_output_shapes
:*
T0*
out_type0
з
9train/gradients/accuracy/mul_3_grad/BroadcastGradientArgsBroadcastGradientArgs)train/gradients/accuracy/mul_3_grad/Shape+train/gradients/accuracy/mul_3_grad/Shape_1*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€*
T0
™
'train/gradients/accuracy/mul_3_grad/MulMul<train/gradients/accuracy/add_grad/tuple/control_dependency_1accuracy/Sin_3*
T0*#
_output_shapes
:€€€€€€€€€
“
'train/gradients/accuracy/mul_3_grad/SumSum'train/gradients/accuracy/mul_3_grad/Mul9train/gradients/accuracy/mul_3_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
∆
+train/gradients/accuracy/mul_3_grad/ReshapeReshape'train/gradients/accuracy/mul_3_grad/Sum)train/gradients/accuracy/mul_3_grad/Shape*
T0*
Tshape0*#
_output_shapes
:€€€€€€€€€
ђ
)train/gradients/accuracy/mul_3_grad/Mul_1Mulaccuracy/Sin_2<train/gradients/accuracy/add_grad/tuple/control_dependency_1*#
_output_shapes
:€€€€€€€€€*
T0
Ў
)train/gradients/accuracy/mul_3_grad/Sum_1Sum)train/gradients/accuracy/mul_3_grad/Mul_1;train/gradients/accuracy/mul_3_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
ћ
-train/gradients/accuracy/mul_3_grad/Reshape_1Reshape)train/gradients/accuracy/mul_3_grad/Sum_1+train/gradients/accuracy/mul_3_grad/Shape_1*
T0*
Tshape0*#
_output_shapes
:€€€€€€€€€
Ъ
4train/gradients/accuracy/mul_3_grad/tuple/group_depsNoOp,^train/gradients/accuracy/mul_3_grad/Reshape.^train/gradients/accuracy/mul_3_grad/Reshape_1
Ъ
<train/gradients/accuracy/mul_3_grad/tuple/control_dependencyIdentity+train/gradients/accuracy/mul_3_grad/Reshape5^train/gradients/accuracy/mul_3_grad/tuple/group_deps*#
_output_shapes
:€€€€€€€€€*
T0*>
_class4
20loc:@train/gradients/accuracy/mul_3_grad/Reshape
†
>train/gradients/accuracy/mul_3_grad/tuple/control_dependency_1Identity-train/gradients/accuracy/mul_3_grad/Reshape_15^train/gradients/accuracy/mul_3_grad/tuple/group_deps*
T0*@
_class6
42loc:@train/gradients/accuracy/mul_3_grad/Reshape_1*#
_output_shapes
:€€€€€€€€€
Б
3train/gradients/accuracy/strided_slice_9_grad/ShapeShapeoutput/BiasAdd*
T0*
out_type0*
_output_shapes
:
ќ
>train/gradients/accuracy/strided_slice_9_grad/StridedSliceGradStridedSliceGrad3train/gradients/accuracy/strided_slice_9_grad/Shapeaccuracy/strided_slice_9/stack accuracy/strided_slice_9/stack_1 accuracy/strided_slice_9/stack_2>train/gradients/accuracy/mul_6_grad/tuple/control_dependency_1*
T0*
Index0*
shrink_axis_mask *
ellipsis_mask *

begin_mask*
new_axis_mask *
end_mask*'
_output_shapes
:€€€€€€€€€
Ш
%train/gradients/accuracy/Cos_grad/NegNeg<train/gradients/accuracy/mul_grad/tuple/control_dependency_1*
T0*#
_output_shapes
:€€€€€€€€€
≥
%train/gradients/accuracy/Cos_grad/SinSinaccuracy/strided_slice_1=^train/gradients/accuracy/mul_grad/tuple/control_dependency_1*
T0*#
_output_shapes
:€€€€€€€€€
®
%train/gradients/accuracy/Cos_grad/mulMul%train/gradients/accuracy/Cos_grad/Neg%train/gradients/accuracy/Cos_grad/Sin*
T0*#
_output_shapes
:€€€€€€€€€
Ј
'train/gradients/accuracy/Sin_1_grad/CosCosaccuracy/strided_slice_3?^train/gradients/accuracy/mul_1_grad/tuple/control_dependency_1*#
_output_shapes
:€€€€€€€€€*
T0
≈
'train/gradients/accuracy/Sin_1_grad/mulMul>train/gradients/accuracy/mul_1_grad/tuple/control_dependency_1'train/gradients/accuracy/Sin_1_grad/Cos*
T0*#
_output_shapes
:€€€€€€€€€
Ь
'train/gradients/accuracy/Cos_3_grad/NegNeg>train/gradients/accuracy/mul_2_grad/tuple/control_dependency_1*
T0*#
_output_shapes
:€€€€€€€€€
Ј
'train/gradients/accuracy/Cos_3_grad/SinSinaccuracy/strided_slice_5?^train/gradients/accuracy/mul_2_grad/tuple/control_dependency_1*
T0*#
_output_shapes
:€€€€€€€€€
Ѓ
'train/gradients/accuracy/Cos_3_grad/mulMul'train/gradients/accuracy/Cos_3_grad/Neg'train/gradients/accuracy/Cos_3_grad/Sin*
T0*#
_output_shapes
:€€€€€€€€€
Ј
'train/gradients/accuracy/Sin_3_grad/CosCosaccuracy/strided_slice_7?^train/gradients/accuracy/mul_3_grad/tuple/control_dependency_1*
T0*#
_output_shapes
:€€€€€€€€€
≈
'train/gradients/accuracy/Sin_3_grad/mulMul>train/gradients/accuracy/mul_3_grad/tuple/control_dependency_1'train/gradients/accuracy/Sin_3_grad/Cos*#
_output_shapes
:€€€€€€€€€*
T0
Б
3train/gradients/accuracy/strided_slice_1_grad/ShapeShapeoutput/BiasAdd*
T0*
out_type0*
_output_shapes
:
µ
>train/gradients/accuracy/strided_slice_1_grad/StridedSliceGradStridedSliceGrad3train/gradients/accuracy/strided_slice_1_grad/Shapeaccuracy/strided_slice_1/stack accuracy/strided_slice_1/stack_1 accuracy/strided_slice_1/stack_2%train/gradients/accuracy/Cos_grad/mul*
Index0*
T0*
shrink_axis_mask*
ellipsis_mask *

begin_mask*
new_axis_mask *
end_mask*'
_output_shapes
:€€€€€€€€€
Б
3train/gradients/accuracy/strided_slice_3_grad/ShapeShapeoutput/BiasAdd*
T0*
out_type0*
_output_shapes
:
Ј
>train/gradients/accuracy/strided_slice_3_grad/StridedSliceGradStridedSliceGrad3train/gradients/accuracy/strided_slice_3_grad/Shapeaccuracy/strided_slice_3/stack accuracy/strided_slice_3/stack_1 accuracy/strided_slice_3/stack_2'train/gradients/accuracy/Sin_1_grad/mul*
Index0*
T0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*'
_output_shapes
:€€€€€€€€€
Б
3train/gradients/accuracy/strided_slice_5_grad/ShapeShapeoutput/BiasAdd*
T0*
out_type0*
_output_shapes
:
Ј
>train/gradients/accuracy/strided_slice_5_grad/StridedSliceGradStridedSliceGrad3train/gradients/accuracy/strided_slice_5_grad/Shapeaccuracy/strided_slice_5/stack accuracy/strided_slice_5/stack_1 accuracy/strided_slice_5/stack_2'train/gradients/accuracy/Cos_3_grad/mul*
end_mask*'
_output_shapes
:€€€€€€€€€*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask 
Б
3train/gradients/accuracy/strided_slice_7_grad/ShapeShapeoutput/BiasAdd*
T0*
out_type0*
_output_shapes
:
Ј
>train/gradients/accuracy/strided_slice_7_grad/StridedSliceGradStridedSliceGrad3train/gradients/accuracy/strided_slice_7_grad/Shapeaccuracy/strided_slice_7/stack accuracy/strided_slice_7/stack_1 accuracy/strided_slice_7/stack_2'train/gradients/accuracy/Sin_3_grad/mul*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask*'
_output_shapes
:€€€€€€€€€
к
train/gradients/AddNAddN>train/gradients/accuracy/strided_slice_9_grad/StridedSliceGrad>train/gradients/accuracy/strided_slice_1_grad/StridedSliceGrad>train/gradients/accuracy/strided_slice_3_grad/StridedSliceGrad>train/gradients/accuracy/strided_slice_5_grad/StridedSliceGrad>train/gradients/accuracy/strided_slice_7_grad/StridedSliceGrad*
T0*Q
_classG
ECloc:@train/gradients/accuracy/strided_slice_9_grad/StridedSliceGrad*
N*'
_output_shapes
:€€€€€€€€€
Р
/train/gradients/output/BiasAdd_grad/BiasAddGradBiasAddGradtrain/gradients/AddN*
T0*
data_formatNHWC*
_output_shapes
:
Е
4train/gradients/output/BiasAdd_grad/tuple/group_depsNoOp^train/gradients/AddN0^train/gradients/output/BiasAdd_grad/BiasAddGrad
Ъ
<train/gradients/output/BiasAdd_grad/tuple/control_dependencyIdentitytrain/gradients/AddN5^train/gradients/output/BiasAdd_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@train/gradients/accuracy/strided_slice_9_grad/StridedSliceGrad*'
_output_shapes
:€€€€€€€€€
Ы
>train/gradients/output/BiasAdd_grad/tuple/control_dependency_1Identity/train/gradients/output/BiasAdd_grad/BiasAddGrad5^train/gradients/output/BiasAdd_grad/tuple/group_deps*
_output_shapes
:*
T0*B
_class8
64loc:@train/gradients/output/BiasAdd_grad/BiasAddGrad
Ё
)train/gradients/output/MatMul_grad/MatMulMatMul<train/gradients/output/BiasAdd_grad/tuple/control_dependencyoutput/kernel/read*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€
*
transpose_b(
г
+train/gradients/output/MatMul_grad/MatMul_1MatMulfinal_fc_l4_dropout/dropout/mul<train/gradients/output/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
_output_shapes

:
*
transpose_b( 
Х
3train/gradients/output/MatMul_grad/tuple/group_depsNoOp*^train/gradients/output/MatMul_grad/MatMul,^train/gradients/output/MatMul_grad/MatMul_1
Ш
;train/gradients/output/MatMul_grad/tuple/control_dependencyIdentity)train/gradients/output/MatMul_grad/MatMul4^train/gradients/output/MatMul_grad/tuple/group_deps*
T0*<
_class2
0.loc:@train/gradients/output/MatMul_grad/MatMul*'
_output_shapes
:€€€€€€€€€

Х
=train/gradients/output/MatMul_grad/tuple/control_dependency_1Identity+train/gradients/output/MatMul_grad/MatMul_14^train/gradients/output/MatMul_grad/tuple/group_deps*
_output_shapes

:
*
T0*>
_class4
20loc:@train/gradients/output/MatMul_grad/MatMul_1
Щ
:train/gradients/final_fc_l4_dropout/dropout/mul_grad/ShapeShapefinal_fc_l4_dropout/dropout/div*
T0*
out_type0*
_output_shapes
:
Э
<train/gradients/final_fc_l4_dropout/dropout/mul_grad/Shape_1Shape!final_fc_l4_dropout/dropout/Floor*
T0*
out_type0*
_output_shapes
:
Ъ
Jtrain/gradients/final_fc_l4_dropout/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/final_fc_l4_dropout/dropout/mul_grad/Shape<train/gradients/final_fc_l4_dropout/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
—
8train/gradients/final_fc_l4_dropout/dropout/mul_grad/MulMul;train/gradients/output/MatMul_grad/tuple/control_dependency!final_fc_l4_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€

Е
8train/gradients/final_fc_l4_dropout/dropout/mul_grad/SumSum8train/gradients/final_fc_l4_dropout/dropout/mul_grad/MulJtrain/gradients/final_fc_l4_dropout/dropout/mul_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
э
<train/gradients/final_fc_l4_dropout/dropout/mul_grad/ReshapeReshape8train/gradients/final_fc_l4_dropout/dropout/mul_grad/Sum:train/gradients/final_fc_l4_dropout/dropout/mul_grad/Shape*'
_output_shapes
:€€€€€€€€€
*
T0*
Tshape0
—
:train/gradients/final_fc_l4_dropout/dropout/mul_grad/Mul_1Mulfinal_fc_l4_dropout/dropout/div;train/gradients/output/MatMul_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€

Л
:train/gradients/final_fc_l4_dropout/dropout/mul_grad/Sum_1Sum:train/gradients/final_fc_l4_dropout/dropout/mul_grad/Mul_1Ltrain/gradients/final_fc_l4_dropout/dropout/mul_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
Г
>train/gradients/final_fc_l4_dropout/dropout/mul_grad/Reshape_1Reshape:train/gradients/final_fc_l4_dropout/dropout/mul_grad/Sum_1<train/gradients/final_fc_l4_dropout/dropout/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€

Ќ
Etrain/gradients/final_fc_l4_dropout/dropout/mul_grad/tuple/group_depsNoOp=^train/gradients/final_fc_l4_dropout/dropout/mul_grad/Reshape?^train/gradients/final_fc_l4_dropout/dropout/mul_grad/Reshape_1
в
Mtrain/gradients/final_fc_l4_dropout/dropout/mul_grad/tuple/control_dependencyIdentity<train/gradients/final_fc_l4_dropout/dropout/mul_grad/ReshapeF^train/gradients/final_fc_l4_dropout/dropout/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€
*
T0*O
_classE
CAloc:@train/gradients/final_fc_l4_dropout/dropout/mul_grad/Reshape
и
Otrain/gradients/final_fc_l4_dropout/dropout/mul_grad/tuple/control_dependency_1Identity>train/gradients/final_fc_l4_dropout/dropout/mul_grad/Reshape_1F^train/gradients/final_fc_l4_dropout/dropout/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€
*
T0*Q
_classG
ECloc:@train/gradients/final_fc_l4_dropout/dropout/mul_grad/Reshape_1
П
:train/gradients/final_fc_l4_dropout/dropout/div_grad/ShapeShapefinal_fc_l4/LeakyRelu*
T0*
out_type0*
_output_shapes
:

<train/gradients/final_fc_l4_dropout/dropout/div_grad/Shape_1Const*
dtype0*
_output_shapes
: *
valueB 
Ъ
Jtrain/gradients/final_fc_l4_dropout/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/final_fc_l4_dropout/dropout/div_grad/Shape<train/gradients/final_fc_l4_dropout/dropout/div_grad/Shape_1*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€*
T0
п
<train/gradients/final_fc_l4_dropout/dropout/div_grad/RealDivRealDivMtrain/gradients/final_fc_l4_dropout/dropout/mul_grad/tuple/control_dependency%final_fc_l4_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€

Й
8train/gradients/final_fc_l4_dropout/dropout/div_grad/SumSum<train/gradients/final_fc_l4_dropout/dropout/div_grad/RealDivJtrain/gradients/final_fc_l4_dropout/dropout/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
э
<train/gradients/final_fc_l4_dropout/dropout/div_grad/ReshapeReshape8train/gradients/final_fc_l4_dropout/dropout/div_grad/Sum:train/gradients/final_fc_l4_dropout/dropout/div_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€

И
8train/gradients/final_fc_l4_dropout/dropout/div_grad/NegNegfinal_fc_l4/LeakyRelu*
T0*'
_output_shapes
:€€€€€€€€€

№
>train/gradients/final_fc_l4_dropout/dropout/div_grad/RealDiv_1RealDiv8train/gradients/final_fc_l4_dropout/dropout/div_grad/Neg%final_fc_l4_dropout/dropout/keep_prob*'
_output_shapes
:€€€€€€€€€
*
T0
в
>train/gradients/final_fc_l4_dropout/dropout/div_grad/RealDiv_2RealDiv>train/gradients/final_fc_l4_dropout/dropout/div_grad/RealDiv_1%final_fc_l4_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€

А
8train/gradients/final_fc_l4_dropout/dropout/div_grad/mulMulMtrain/gradients/final_fc_l4_dropout/dropout/mul_grad/tuple/control_dependency>train/gradients/final_fc_l4_dropout/dropout/div_grad/RealDiv_2*'
_output_shapes
:€€€€€€€€€
*
T0
Й
:train/gradients/final_fc_l4_dropout/dropout/div_grad/Sum_1Sum8train/gradients/final_fc_l4_dropout/dropout/div_grad/mulLtrain/gradients/final_fc_l4_dropout/dropout/div_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
т
>train/gradients/final_fc_l4_dropout/dropout/div_grad/Reshape_1Reshape:train/gradients/final_fc_l4_dropout/dropout/div_grad/Sum_1<train/gradients/final_fc_l4_dropout/dropout/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 
Ќ
Etrain/gradients/final_fc_l4_dropout/dropout/div_grad/tuple/group_depsNoOp=^train/gradients/final_fc_l4_dropout/dropout/div_grad/Reshape?^train/gradients/final_fc_l4_dropout/dropout/div_grad/Reshape_1
в
Mtrain/gradients/final_fc_l4_dropout/dropout/div_grad/tuple/control_dependencyIdentity<train/gradients/final_fc_l4_dropout/dropout/div_grad/ReshapeF^train/gradients/final_fc_l4_dropout/dropout/div_grad/tuple/group_deps*
T0*O
_classE
CAloc:@train/gradients/final_fc_l4_dropout/dropout/div_grad/Reshape*'
_output_shapes
:€€€€€€€€€

„
Otrain/gradients/final_fc_l4_dropout/dropout/div_grad/tuple/control_dependency_1Identity>train/gradients/final_fc_l4_dropout/dropout/div_grad/Reshape_1F^train/gradients/final_fc_l4_dropout/dropout/div_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@train/gradients/final_fc_l4_dropout/dropout/div_grad/Reshape_1*
_output_shapes
: 
Й
0train/gradients/final_fc_l4/LeakyRelu_grad/ShapeShapefinal_fc_l4/LeakyRelu/mul*
T0*
out_type0*
_output_shapes
:
Е
2train/gradients/final_fc_l4/LeakyRelu_grad/Shape_1Shapefinal_fc_l4/BiasAdd*
T0*
out_type0*
_output_shapes
:
њ
2train/gradients/final_fc_l4/LeakyRelu_grad/Shape_2ShapeMtrain/gradients/final_fc_l4_dropout/dropout/div_grad/tuple/control_dependency*
_output_shapes
:*
T0*
out_type0

6train/gradients/final_fc_l4/LeakyRelu_grad/zeros/ConstConst*
valueB 2        *
dtype0*
_output_shapes
: 
и
0train/gradients/final_fc_l4/LeakyRelu_grad/zerosFill2train/gradients/final_fc_l4/LeakyRelu_grad/Shape_26train/gradients/final_fc_l4/LeakyRelu_grad/zeros/Const*
T0*

index_type0*'
_output_shapes
:€€€€€€€€€

©
7train/gradients/final_fc_l4/LeakyRelu_grad/GreaterEqualGreaterEqualfinal_fc_l4/LeakyRelu/mulfinal_fc_l4/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€

ь
@train/gradients/final_fc_l4/LeakyRelu_grad/BroadcastGradientArgsBroadcastGradientArgs0train/gradients/final_fc_l4/LeakyRelu_grad/Shape2train/gradients/final_fc_l4/LeakyRelu_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
І
1train/gradients/final_fc_l4/LeakyRelu_grad/SelectSelect7train/gradients/final_fc_l4/LeakyRelu_grad/GreaterEqualMtrain/gradients/final_fc_l4_dropout/dropout/div_grad/tuple/control_dependency0train/gradients/final_fc_l4/LeakyRelu_grad/zeros*'
_output_shapes
:€€€€€€€€€
*
T0
©
3train/gradients/final_fc_l4/LeakyRelu_grad/Select_1Select7train/gradients/final_fc_l4/LeakyRelu_grad/GreaterEqual0train/gradients/final_fc_l4/LeakyRelu_grad/zerosMtrain/gradients/final_fc_l4_dropout/dropout/div_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€

к
.train/gradients/final_fc_l4/LeakyRelu_grad/SumSum1train/gradients/final_fc_l4/LeakyRelu_grad/Select@train/gradients/final_fc_l4/LeakyRelu_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
я
2train/gradients/final_fc_l4/LeakyRelu_grad/ReshapeReshape.train/gradients/final_fc_l4/LeakyRelu_grad/Sum0train/gradients/final_fc_l4/LeakyRelu_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€

р
0train/gradients/final_fc_l4/LeakyRelu_grad/Sum_1Sum3train/gradients/final_fc_l4/LeakyRelu_grad/Select_1Btrain/gradients/final_fc_l4/LeakyRelu_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
е
4train/gradients/final_fc_l4/LeakyRelu_grad/Reshape_1Reshape0train/gradients/final_fc_l4/LeakyRelu_grad/Sum_12train/gradients/final_fc_l4/LeakyRelu_grad/Shape_1*'
_output_shapes
:€€€€€€€€€
*
T0*
Tshape0
ѓ
;train/gradients/final_fc_l4/LeakyRelu_grad/tuple/group_depsNoOp3^train/gradients/final_fc_l4/LeakyRelu_grad/Reshape5^train/gradients/final_fc_l4/LeakyRelu_grad/Reshape_1
Ї
Ctrain/gradients/final_fc_l4/LeakyRelu_grad/tuple/control_dependencyIdentity2train/gradients/final_fc_l4/LeakyRelu_grad/Reshape<^train/gradients/final_fc_l4/LeakyRelu_grad/tuple/group_deps*
T0*E
_class;
97loc:@train/gradients/final_fc_l4/LeakyRelu_grad/Reshape*'
_output_shapes
:€€€€€€€€€

ј
Etrain/gradients/final_fc_l4/LeakyRelu_grad/tuple/control_dependency_1Identity4train/gradients/final_fc_l4/LeakyRelu_grad/Reshape_1<^train/gradients/final_fc_l4/LeakyRelu_grad/tuple/group_deps*
T0*G
_class=
;9loc:@train/gradients/final_fc_l4/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€

w
4train/gradients/final_fc_l4/LeakyRelu/mul_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
Й
6train/gradients/final_fc_l4/LeakyRelu/mul_grad/Shape_1Shapefinal_fc_l4/BiasAdd*
T0*
out_type0*
_output_shapes
:
И
Dtrain/gradients/final_fc_l4/LeakyRelu/mul_grad/BroadcastGradientArgsBroadcastGradientArgs4train/gradients/final_fc_l4/LeakyRelu/mul_grad/Shape6train/gradients/final_fc_l4/LeakyRelu/mul_grad/Shape_1*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€*
T0
≈
2train/gradients/final_fc_l4/LeakyRelu/mul_grad/MulMulCtrain/gradients/final_fc_l4/LeakyRelu_grad/tuple/control_dependencyfinal_fc_l4/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€

у
2train/gradients/final_fc_l4/LeakyRelu/mul_grad/SumSum2train/gradients/final_fc_l4/LeakyRelu/mul_grad/MulDtrain/gradients/final_fc_l4/LeakyRelu/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Џ
6train/gradients/final_fc_l4/LeakyRelu/mul_grad/ReshapeReshape2train/gradients/final_fc_l4/LeakyRelu/mul_grad/Sum4train/gradients/final_fc_l4/LeakyRelu/mul_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
ѕ
4train/gradients/final_fc_l4/LeakyRelu/mul_grad/Mul_1Mulfinal_fc_l4/LeakyRelu/alphaCtrain/gradients/final_fc_l4/LeakyRelu_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€

щ
4train/gradients/final_fc_l4/LeakyRelu/mul_grad/Sum_1Sum4train/gradients/final_fc_l4/LeakyRelu/mul_grad/Mul_1Ftrain/gradients/final_fc_l4/LeakyRelu/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
с
8train/gradients/final_fc_l4/LeakyRelu/mul_grad/Reshape_1Reshape4train/gradients/final_fc_l4/LeakyRelu/mul_grad/Sum_16train/gradients/final_fc_l4/LeakyRelu/mul_grad/Shape_1*'
_output_shapes
:€€€€€€€€€
*
T0*
Tshape0
ї
?train/gradients/final_fc_l4/LeakyRelu/mul_grad/tuple/group_depsNoOp7^train/gradients/final_fc_l4/LeakyRelu/mul_grad/Reshape9^train/gradients/final_fc_l4/LeakyRelu/mul_grad/Reshape_1
є
Gtrain/gradients/final_fc_l4/LeakyRelu/mul_grad/tuple/control_dependencyIdentity6train/gradients/final_fc_l4/LeakyRelu/mul_grad/Reshape@^train/gradients/final_fc_l4/LeakyRelu/mul_grad/tuple/group_deps*
T0*I
_class?
=;loc:@train/gradients/final_fc_l4/LeakyRelu/mul_grad/Reshape*
_output_shapes
: 
–
Itrain/gradients/final_fc_l4/LeakyRelu/mul_grad/tuple/control_dependency_1Identity8train/gradients/final_fc_l4/LeakyRelu/mul_grad/Reshape_1@^train/gradients/final_fc_l4/LeakyRelu/mul_grad/tuple/group_deps*
T0*K
_classA
?=loc:@train/gradients/final_fc_l4/LeakyRelu/mul_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€

і
train/gradients/AddN_1AddNEtrain/gradients/final_fc_l4/LeakyRelu_grad/tuple/control_dependency_1Itrain/gradients/final_fc_l4/LeakyRelu/mul_grad/tuple/control_dependency_1*
T0*G
_class=
;9loc:@train/gradients/final_fc_l4/LeakyRelu_grad/Reshape_1*
N*'
_output_shapes
:€€€€€€€€€

Ч
4train/gradients/final_fc_l4/BiasAdd_grad/BiasAddGradBiasAddGradtrain/gradients/AddN_1*
T0*
data_formatNHWC*
_output_shapes
:

С
9train/gradients/final_fc_l4/BiasAdd_grad/tuple/group_depsNoOp^train/gradients/AddN_15^train/gradients/final_fc_l4/BiasAdd_grad/BiasAddGrad
Ь
Atrain/gradients/final_fc_l4/BiasAdd_grad/tuple/control_dependencyIdentitytrain/gradients/AddN_1:^train/gradients/final_fc_l4/BiasAdd_grad/tuple/group_deps*
T0*G
_class=
;9loc:@train/gradients/final_fc_l4/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€

ѓ
Ctrain/gradients/final_fc_l4/BiasAdd_grad/tuple/control_dependency_1Identity4train/gradients/final_fc_l4/BiasAdd_grad/BiasAddGrad:^train/gradients/final_fc_l4/BiasAdd_grad/tuple/group_deps*
T0*G
_class=
;9loc:@train/gradients/final_fc_l4/BiasAdd_grad/BiasAddGrad*
_output_shapes
:

м
.train/gradients/final_fc_l4/MatMul_grad/MatMulMatMulAtrain/gradients/final_fc_l4/BiasAdd_grad/tuple/control_dependencyfinal_fc_l4/kernel/read*
transpose_b(*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€

н
0train/gradients/final_fc_l4/MatMul_grad/MatMul_1MatMulfinal_fc_l3_dropout/dropout/mulAtrain/gradients/final_fc_l4/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(*
_output_shapes

:


§
8train/gradients/final_fc_l4/MatMul_grad/tuple/group_depsNoOp/^train/gradients/final_fc_l4/MatMul_grad/MatMul1^train/gradients/final_fc_l4/MatMul_grad/MatMul_1
ђ
@train/gradients/final_fc_l4/MatMul_grad/tuple/control_dependencyIdentity.train/gradients/final_fc_l4/MatMul_grad/MatMul9^train/gradients/final_fc_l4/MatMul_grad/tuple/group_deps*
T0*A
_class7
53loc:@train/gradients/final_fc_l4/MatMul_grad/MatMul*'
_output_shapes
:€€€€€€€€€

©
Btrain/gradients/final_fc_l4/MatMul_grad/tuple/control_dependency_1Identity0train/gradients/final_fc_l4/MatMul_grad/MatMul_19^train/gradients/final_fc_l4/MatMul_grad/tuple/group_deps*
_output_shapes

:

*
T0*C
_class9
75loc:@train/gradients/final_fc_l4/MatMul_grad/MatMul_1
Щ
:train/gradients/final_fc_l3_dropout/dropout/mul_grad/ShapeShapefinal_fc_l3_dropout/dropout/div*
T0*
out_type0*
_output_shapes
:
Э
<train/gradients/final_fc_l3_dropout/dropout/mul_grad/Shape_1Shape!final_fc_l3_dropout/dropout/Floor*
_output_shapes
:*
T0*
out_type0
Ъ
Jtrain/gradients/final_fc_l3_dropout/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/final_fc_l3_dropout/dropout/mul_grad/Shape<train/gradients/final_fc_l3_dropout/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
÷
8train/gradients/final_fc_l3_dropout/dropout/mul_grad/MulMul@train/gradients/final_fc_l4/MatMul_grad/tuple/control_dependency!final_fc_l3_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€

Е
8train/gradients/final_fc_l3_dropout/dropout/mul_grad/SumSum8train/gradients/final_fc_l3_dropout/dropout/mul_grad/MulJtrain/gradients/final_fc_l3_dropout/dropout/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
э
<train/gradients/final_fc_l3_dropout/dropout/mul_grad/ReshapeReshape8train/gradients/final_fc_l3_dropout/dropout/mul_grad/Sum:train/gradients/final_fc_l3_dropout/dropout/mul_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€

÷
:train/gradients/final_fc_l3_dropout/dropout/mul_grad/Mul_1Mulfinal_fc_l3_dropout/dropout/div@train/gradients/final_fc_l4/MatMul_grad/tuple/control_dependency*'
_output_shapes
:€€€€€€€€€
*
T0
Л
:train/gradients/final_fc_l3_dropout/dropout/mul_grad/Sum_1Sum:train/gradients/final_fc_l3_dropout/dropout/mul_grad/Mul_1Ltrain/gradients/final_fc_l3_dropout/dropout/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Г
>train/gradients/final_fc_l3_dropout/dropout/mul_grad/Reshape_1Reshape:train/gradients/final_fc_l3_dropout/dropout/mul_grad/Sum_1<train/gradients/final_fc_l3_dropout/dropout/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€

Ќ
Etrain/gradients/final_fc_l3_dropout/dropout/mul_grad/tuple/group_depsNoOp=^train/gradients/final_fc_l3_dropout/dropout/mul_grad/Reshape?^train/gradients/final_fc_l3_dropout/dropout/mul_grad/Reshape_1
в
Mtrain/gradients/final_fc_l3_dropout/dropout/mul_grad/tuple/control_dependencyIdentity<train/gradients/final_fc_l3_dropout/dropout/mul_grad/ReshapeF^train/gradients/final_fc_l3_dropout/dropout/mul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@train/gradients/final_fc_l3_dropout/dropout/mul_grad/Reshape*'
_output_shapes
:€€€€€€€€€

и
Otrain/gradients/final_fc_l3_dropout/dropout/mul_grad/tuple/control_dependency_1Identity>train/gradients/final_fc_l3_dropout/dropout/mul_grad/Reshape_1F^train/gradients/final_fc_l3_dropout/dropout/mul_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@train/gradients/final_fc_l3_dropout/dropout/mul_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€

П
:train/gradients/final_fc_l3_dropout/dropout/div_grad/ShapeShapefinal_fc_l3/LeakyRelu*
T0*
out_type0*
_output_shapes
:

<train/gradients/final_fc_l3_dropout/dropout/div_grad/Shape_1Const*
dtype0*
_output_shapes
: *
valueB 
Ъ
Jtrain/gradients/final_fc_l3_dropout/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/final_fc_l3_dropout/dropout/div_grad/Shape<train/gradients/final_fc_l3_dropout/dropout/div_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
п
<train/gradients/final_fc_l3_dropout/dropout/div_grad/RealDivRealDivMtrain/gradients/final_fc_l3_dropout/dropout/mul_grad/tuple/control_dependency%final_fc_l3_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€

Й
8train/gradients/final_fc_l3_dropout/dropout/div_grad/SumSum<train/gradients/final_fc_l3_dropout/dropout/div_grad/RealDivJtrain/gradients/final_fc_l3_dropout/dropout/div_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
э
<train/gradients/final_fc_l3_dropout/dropout/div_grad/ReshapeReshape8train/gradients/final_fc_l3_dropout/dropout/div_grad/Sum:train/gradients/final_fc_l3_dropout/dropout/div_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€

И
8train/gradients/final_fc_l3_dropout/dropout/div_grad/NegNegfinal_fc_l3/LeakyRelu*'
_output_shapes
:€€€€€€€€€
*
T0
№
>train/gradients/final_fc_l3_dropout/dropout/div_grad/RealDiv_1RealDiv8train/gradients/final_fc_l3_dropout/dropout/div_grad/Neg%final_fc_l3_dropout/dropout/keep_prob*'
_output_shapes
:€€€€€€€€€
*
T0
в
>train/gradients/final_fc_l3_dropout/dropout/div_grad/RealDiv_2RealDiv>train/gradients/final_fc_l3_dropout/dropout/div_grad/RealDiv_1%final_fc_l3_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€

А
8train/gradients/final_fc_l3_dropout/dropout/div_grad/mulMulMtrain/gradients/final_fc_l3_dropout/dropout/mul_grad/tuple/control_dependency>train/gradients/final_fc_l3_dropout/dropout/div_grad/RealDiv_2*
T0*'
_output_shapes
:€€€€€€€€€

Й
:train/gradients/final_fc_l3_dropout/dropout/div_grad/Sum_1Sum8train/gradients/final_fc_l3_dropout/dropout/div_grad/mulLtrain/gradients/final_fc_l3_dropout/dropout/div_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
т
>train/gradients/final_fc_l3_dropout/dropout/div_grad/Reshape_1Reshape:train/gradients/final_fc_l3_dropout/dropout/div_grad/Sum_1<train/gradients/final_fc_l3_dropout/dropout/div_grad/Shape_1*
_output_shapes
: *
T0*
Tshape0
Ќ
Etrain/gradients/final_fc_l3_dropout/dropout/div_grad/tuple/group_depsNoOp=^train/gradients/final_fc_l3_dropout/dropout/div_grad/Reshape?^train/gradients/final_fc_l3_dropout/dropout/div_grad/Reshape_1
в
Mtrain/gradients/final_fc_l3_dropout/dropout/div_grad/tuple/control_dependencyIdentity<train/gradients/final_fc_l3_dropout/dropout/div_grad/ReshapeF^train/gradients/final_fc_l3_dropout/dropout/div_grad/tuple/group_deps*
T0*O
_classE
CAloc:@train/gradients/final_fc_l3_dropout/dropout/div_grad/Reshape*'
_output_shapes
:€€€€€€€€€

„
Otrain/gradients/final_fc_l3_dropout/dropout/div_grad/tuple/control_dependency_1Identity>train/gradients/final_fc_l3_dropout/dropout/div_grad/Reshape_1F^train/gradients/final_fc_l3_dropout/dropout/div_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@train/gradients/final_fc_l3_dropout/dropout/div_grad/Reshape_1*
_output_shapes
: 
Й
0train/gradients/final_fc_l3/LeakyRelu_grad/ShapeShapefinal_fc_l3/LeakyRelu/mul*
T0*
out_type0*
_output_shapes
:
Е
2train/gradients/final_fc_l3/LeakyRelu_grad/Shape_1Shapefinal_fc_l3/BiasAdd*
T0*
out_type0*
_output_shapes
:
њ
2train/gradients/final_fc_l3/LeakyRelu_grad/Shape_2ShapeMtrain/gradients/final_fc_l3_dropout/dropout/div_grad/tuple/control_dependency*
T0*
out_type0*
_output_shapes
:

6train/gradients/final_fc_l3/LeakyRelu_grad/zeros/ConstConst*
valueB 2        *
dtype0*
_output_shapes
: 
и
0train/gradients/final_fc_l3/LeakyRelu_grad/zerosFill2train/gradients/final_fc_l3/LeakyRelu_grad/Shape_26train/gradients/final_fc_l3/LeakyRelu_grad/zeros/Const*
T0*

index_type0*'
_output_shapes
:€€€€€€€€€

©
7train/gradients/final_fc_l3/LeakyRelu_grad/GreaterEqualGreaterEqualfinal_fc_l3/LeakyRelu/mulfinal_fc_l3/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€

ь
@train/gradients/final_fc_l3/LeakyRelu_grad/BroadcastGradientArgsBroadcastGradientArgs0train/gradients/final_fc_l3/LeakyRelu_grad/Shape2train/gradients/final_fc_l3/LeakyRelu_grad/Shape_1*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€*
T0
І
1train/gradients/final_fc_l3/LeakyRelu_grad/SelectSelect7train/gradients/final_fc_l3/LeakyRelu_grad/GreaterEqualMtrain/gradients/final_fc_l3_dropout/dropout/div_grad/tuple/control_dependency0train/gradients/final_fc_l3/LeakyRelu_grad/zeros*
T0*'
_output_shapes
:€€€€€€€€€

©
3train/gradients/final_fc_l3/LeakyRelu_grad/Select_1Select7train/gradients/final_fc_l3/LeakyRelu_grad/GreaterEqual0train/gradients/final_fc_l3/LeakyRelu_grad/zerosMtrain/gradients/final_fc_l3_dropout/dropout/div_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€

к
.train/gradients/final_fc_l3/LeakyRelu_grad/SumSum1train/gradients/final_fc_l3/LeakyRelu_grad/Select@train/gradients/final_fc_l3/LeakyRelu_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
я
2train/gradients/final_fc_l3/LeakyRelu_grad/ReshapeReshape.train/gradients/final_fc_l3/LeakyRelu_grad/Sum0train/gradients/final_fc_l3/LeakyRelu_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€

р
0train/gradients/final_fc_l3/LeakyRelu_grad/Sum_1Sum3train/gradients/final_fc_l3/LeakyRelu_grad/Select_1Btrain/gradients/final_fc_l3/LeakyRelu_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
е
4train/gradients/final_fc_l3/LeakyRelu_grad/Reshape_1Reshape0train/gradients/final_fc_l3/LeakyRelu_grad/Sum_12train/gradients/final_fc_l3/LeakyRelu_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€

ѓ
;train/gradients/final_fc_l3/LeakyRelu_grad/tuple/group_depsNoOp3^train/gradients/final_fc_l3/LeakyRelu_grad/Reshape5^train/gradients/final_fc_l3/LeakyRelu_grad/Reshape_1
Ї
Ctrain/gradients/final_fc_l3/LeakyRelu_grad/tuple/control_dependencyIdentity2train/gradients/final_fc_l3/LeakyRelu_grad/Reshape<^train/gradients/final_fc_l3/LeakyRelu_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€
*
T0*E
_class;
97loc:@train/gradients/final_fc_l3/LeakyRelu_grad/Reshape
ј
Etrain/gradients/final_fc_l3/LeakyRelu_grad/tuple/control_dependency_1Identity4train/gradients/final_fc_l3/LeakyRelu_grad/Reshape_1<^train/gradients/final_fc_l3/LeakyRelu_grad/tuple/group_deps*
T0*G
_class=
;9loc:@train/gradients/final_fc_l3/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€

w
4train/gradients/final_fc_l3/LeakyRelu/mul_grad/ShapeConst*
dtype0*
_output_shapes
: *
valueB 
Й
6train/gradients/final_fc_l3/LeakyRelu/mul_grad/Shape_1Shapefinal_fc_l3/BiasAdd*
T0*
out_type0*
_output_shapes
:
И
Dtrain/gradients/final_fc_l3/LeakyRelu/mul_grad/BroadcastGradientArgsBroadcastGradientArgs4train/gradients/final_fc_l3/LeakyRelu/mul_grad/Shape6train/gradients/final_fc_l3/LeakyRelu/mul_grad/Shape_1*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€*
T0
≈
2train/gradients/final_fc_l3/LeakyRelu/mul_grad/MulMulCtrain/gradients/final_fc_l3/LeakyRelu_grad/tuple/control_dependencyfinal_fc_l3/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€

у
2train/gradients/final_fc_l3/LeakyRelu/mul_grad/SumSum2train/gradients/final_fc_l3/LeakyRelu/mul_grad/MulDtrain/gradients/final_fc_l3/LeakyRelu/mul_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
Џ
6train/gradients/final_fc_l3/LeakyRelu/mul_grad/ReshapeReshape2train/gradients/final_fc_l3/LeakyRelu/mul_grad/Sum4train/gradients/final_fc_l3/LeakyRelu/mul_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
ѕ
4train/gradients/final_fc_l3/LeakyRelu/mul_grad/Mul_1Mulfinal_fc_l3/LeakyRelu/alphaCtrain/gradients/final_fc_l3/LeakyRelu_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€

щ
4train/gradients/final_fc_l3/LeakyRelu/mul_grad/Sum_1Sum4train/gradients/final_fc_l3/LeakyRelu/mul_grad/Mul_1Ftrain/gradients/final_fc_l3/LeakyRelu/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
с
8train/gradients/final_fc_l3/LeakyRelu/mul_grad/Reshape_1Reshape4train/gradients/final_fc_l3/LeakyRelu/mul_grad/Sum_16train/gradients/final_fc_l3/LeakyRelu/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€

ї
?train/gradients/final_fc_l3/LeakyRelu/mul_grad/tuple/group_depsNoOp7^train/gradients/final_fc_l3/LeakyRelu/mul_grad/Reshape9^train/gradients/final_fc_l3/LeakyRelu/mul_grad/Reshape_1
є
Gtrain/gradients/final_fc_l3/LeakyRelu/mul_grad/tuple/control_dependencyIdentity6train/gradients/final_fc_l3/LeakyRelu/mul_grad/Reshape@^train/gradients/final_fc_l3/LeakyRelu/mul_grad/tuple/group_deps*
T0*I
_class?
=;loc:@train/gradients/final_fc_l3/LeakyRelu/mul_grad/Reshape*
_output_shapes
: 
–
Itrain/gradients/final_fc_l3/LeakyRelu/mul_grad/tuple/control_dependency_1Identity8train/gradients/final_fc_l3/LeakyRelu/mul_grad/Reshape_1@^train/gradients/final_fc_l3/LeakyRelu/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€
*
T0*K
_classA
?=loc:@train/gradients/final_fc_l3/LeakyRelu/mul_grad/Reshape_1
і
train/gradients/AddN_2AddNEtrain/gradients/final_fc_l3/LeakyRelu_grad/tuple/control_dependency_1Itrain/gradients/final_fc_l3/LeakyRelu/mul_grad/tuple/control_dependency_1*
T0*G
_class=
;9loc:@train/gradients/final_fc_l3/LeakyRelu_grad/Reshape_1*
N*'
_output_shapes
:€€€€€€€€€

Ч
4train/gradients/final_fc_l3/BiasAdd_grad/BiasAddGradBiasAddGradtrain/gradients/AddN_2*
T0*
data_formatNHWC*
_output_shapes
:

С
9train/gradients/final_fc_l3/BiasAdd_grad/tuple/group_depsNoOp^train/gradients/AddN_25^train/gradients/final_fc_l3/BiasAdd_grad/BiasAddGrad
Ь
Atrain/gradients/final_fc_l3/BiasAdd_grad/tuple/control_dependencyIdentitytrain/gradients/AddN_2:^train/gradients/final_fc_l3/BiasAdd_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€
*
T0*G
_class=
;9loc:@train/gradients/final_fc_l3/LeakyRelu_grad/Reshape_1
ѓ
Ctrain/gradients/final_fc_l3/BiasAdd_grad/tuple/control_dependency_1Identity4train/gradients/final_fc_l3/BiasAdd_grad/BiasAddGrad:^train/gradients/final_fc_l3/BiasAdd_grad/tuple/group_deps*
T0*G
_class=
;9loc:@train/gradients/final_fc_l3/BiasAdd_grad/BiasAddGrad*
_output_shapes
:

м
.train/gradients/final_fc_l3/MatMul_grad/MatMulMatMulAtrain/gradients/final_fc_l3/BiasAdd_grad/tuple/control_dependencyfinal_fc_l3/kernel/read*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€(*
transpose_b(
н
0train/gradients/final_fc_l3/MatMul_grad/MatMul_1MatMulfinal_fc_l2_dropout/dropout/mulAtrain/gradients/final_fc_l3/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(*
_output_shapes

:(

§
8train/gradients/final_fc_l3/MatMul_grad/tuple/group_depsNoOp/^train/gradients/final_fc_l3/MatMul_grad/MatMul1^train/gradients/final_fc_l3/MatMul_grad/MatMul_1
ђ
@train/gradients/final_fc_l3/MatMul_grad/tuple/control_dependencyIdentity.train/gradients/final_fc_l3/MatMul_grad/MatMul9^train/gradients/final_fc_l3/MatMul_grad/tuple/group_deps*
T0*A
_class7
53loc:@train/gradients/final_fc_l3/MatMul_grad/MatMul*'
_output_shapes
:€€€€€€€€€(
©
Btrain/gradients/final_fc_l3/MatMul_grad/tuple/control_dependency_1Identity0train/gradients/final_fc_l3/MatMul_grad/MatMul_19^train/gradients/final_fc_l3/MatMul_grad/tuple/group_deps*
T0*C
_class9
75loc:@train/gradients/final_fc_l3/MatMul_grad/MatMul_1*
_output_shapes

:(

Щ
:train/gradients/final_fc_l2_dropout/dropout/mul_grad/ShapeShapefinal_fc_l2_dropout/dropout/div*
T0*
out_type0*
_output_shapes
:
Э
<train/gradients/final_fc_l2_dropout/dropout/mul_grad/Shape_1Shape!final_fc_l2_dropout/dropout/Floor*
T0*
out_type0*
_output_shapes
:
Ъ
Jtrain/gradients/final_fc_l2_dropout/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/final_fc_l2_dropout/dropout/mul_grad/Shape<train/gradients/final_fc_l2_dropout/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
÷
8train/gradients/final_fc_l2_dropout/dropout/mul_grad/MulMul@train/gradients/final_fc_l3/MatMul_grad/tuple/control_dependency!final_fc_l2_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€(
Е
8train/gradients/final_fc_l2_dropout/dropout/mul_grad/SumSum8train/gradients/final_fc_l2_dropout/dropout/mul_grad/MulJtrain/gradients/final_fc_l2_dropout/dropout/mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
э
<train/gradients/final_fc_l2_dropout/dropout/mul_grad/ReshapeReshape8train/gradients/final_fc_l2_dropout/dropout/mul_grad/Sum:train/gradients/final_fc_l2_dropout/dropout/mul_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€(
÷
:train/gradients/final_fc_l2_dropout/dropout/mul_grad/Mul_1Mulfinal_fc_l2_dropout/dropout/div@train/gradients/final_fc_l3/MatMul_grad/tuple/control_dependency*'
_output_shapes
:€€€€€€€€€(*
T0
Л
:train/gradients/final_fc_l2_dropout/dropout/mul_grad/Sum_1Sum:train/gradients/final_fc_l2_dropout/dropout/mul_grad/Mul_1Ltrain/gradients/final_fc_l2_dropout/dropout/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Г
>train/gradients/final_fc_l2_dropout/dropout/mul_grad/Reshape_1Reshape:train/gradients/final_fc_l2_dropout/dropout/mul_grad/Sum_1<train/gradients/final_fc_l2_dropout/dropout/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€(
Ќ
Etrain/gradients/final_fc_l2_dropout/dropout/mul_grad/tuple/group_depsNoOp=^train/gradients/final_fc_l2_dropout/dropout/mul_grad/Reshape?^train/gradients/final_fc_l2_dropout/dropout/mul_grad/Reshape_1
в
Mtrain/gradients/final_fc_l2_dropout/dropout/mul_grad/tuple/control_dependencyIdentity<train/gradients/final_fc_l2_dropout/dropout/mul_grad/ReshapeF^train/gradients/final_fc_l2_dropout/dropout/mul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@train/gradients/final_fc_l2_dropout/dropout/mul_grad/Reshape*'
_output_shapes
:€€€€€€€€€(
и
Otrain/gradients/final_fc_l2_dropout/dropout/mul_grad/tuple/control_dependency_1Identity>train/gradients/final_fc_l2_dropout/dropout/mul_grad/Reshape_1F^train/gradients/final_fc_l2_dropout/dropout/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€(*
T0*Q
_classG
ECloc:@train/gradients/final_fc_l2_dropout/dropout/mul_grad/Reshape_1
П
:train/gradients/final_fc_l2_dropout/dropout/div_grad/ShapeShapefinal_fc_l2/LeakyRelu*
T0*
out_type0*
_output_shapes
:

<train/gradients/final_fc_l2_dropout/dropout/div_grad/Shape_1Const*
dtype0*
_output_shapes
: *
valueB 
Ъ
Jtrain/gradients/final_fc_l2_dropout/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/final_fc_l2_dropout/dropout/div_grad/Shape<train/gradients/final_fc_l2_dropout/dropout/div_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
п
<train/gradients/final_fc_l2_dropout/dropout/div_grad/RealDivRealDivMtrain/gradients/final_fc_l2_dropout/dropout/mul_grad/tuple/control_dependency%final_fc_l2_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€(
Й
8train/gradients/final_fc_l2_dropout/dropout/div_grad/SumSum<train/gradients/final_fc_l2_dropout/dropout/div_grad/RealDivJtrain/gradients/final_fc_l2_dropout/dropout/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
э
<train/gradients/final_fc_l2_dropout/dropout/div_grad/ReshapeReshape8train/gradients/final_fc_l2_dropout/dropout/div_grad/Sum:train/gradients/final_fc_l2_dropout/dropout/div_grad/Shape*'
_output_shapes
:€€€€€€€€€(*
T0*
Tshape0
И
8train/gradients/final_fc_l2_dropout/dropout/div_grad/NegNegfinal_fc_l2/LeakyRelu*'
_output_shapes
:€€€€€€€€€(*
T0
№
>train/gradients/final_fc_l2_dropout/dropout/div_grad/RealDiv_1RealDiv8train/gradients/final_fc_l2_dropout/dropout/div_grad/Neg%final_fc_l2_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€(
в
>train/gradients/final_fc_l2_dropout/dropout/div_grad/RealDiv_2RealDiv>train/gradients/final_fc_l2_dropout/dropout/div_grad/RealDiv_1%final_fc_l2_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€(
А
8train/gradients/final_fc_l2_dropout/dropout/div_grad/mulMulMtrain/gradients/final_fc_l2_dropout/dropout/mul_grad/tuple/control_dependency>train/gradients/final_fc_l2_dropout/dropout/div_grad/RealDiv_2*
T0*'
_output_shapes
:€€€€€€€€€(
Й
:train/gradients/final_fc_l2_dropout/dropout/div_grad/Sum_1Sum8train/gradients/final_fc_l2_dropout/dropout/div_grad/mulLtrain/gradients/final_fc_l2_dropout/dropout/div_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
т
>train/gradients/final_fc_l2_dropout/dropout/div_grad/Reshape_1Reshape:train/gradients/final_fc_l2_dropout/dropout/div_grad/Sum_1<train/gradients/final_fc_l2_dropout/dropout/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 
Ќ
Etrain/gradients/final_fc_l2_dropout/dropout/div_grad/tuple/group_depsNoOp=^train/gradients/final_fc_l2_dropout/dropout/div_grad/Reshape?^train/gradients/final_fc_l2_dropout/dropout/div_grad/Reshape_1
в
Mtrain/gradients/final_fc_l2_dropout/dropout/div_grad/tuple/control_dependencyIdentity<train/gradients/final_fc_l2_dropout/dropout/div_grad/ReshapeF^train/gradients/final_fc_l2_dropout/dropout/div_grad/tuple/group_deps*
T0*O
_classE
CAloc:@train/gradients/final_fc_l2_dropout/dropout/div_grad/Reshape*'
_output_shapes
:€€€€€€€€€(
„
Otrain/gradients/final_fc_l2_dropout/dropout/div_grad/tuple/control_dependency_1Identity>train/gradients/final_fc_l2_dropout/dropout/div_grad/Reshape_1F^train/gradients/final_fc_l2_dropout/dropout/div_grad/tuple/group_deps*
_output_shapes
: *
T0*Q
_classG
ECloc:@train/gradients/final_fc_l2_dropout/dropout/div_grad/Reshape_1
Й
0train/gradients/final_fc_l2/LeakyRelu_grad/ShapeShapefinal_fc_l2/LeakyRelu/mul*
T0*
out_type0*
_output_shapes
:
Е
2train/gradients/final_fc_l2/LeakyRelu_grad/Shape_1Shapefinal_fc_l2/BiasAdd*
T0*
out_type0*
_output_shapes
:
њ
2train/gradients/final_fc_l2/LeakyRelu_grad/Shape_2ShapeMtrain/gradients/final_fc_l2_dropout/dropout/div_grad/tuple/control_dependency*
T0*
out_type0*
_output_shapes
:

6train/gradients/final_fc_l2/LeakyRelu_grad/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB 2        
и
0train/gradients/final_fc_l2/LeakyRelu_grad/zerosFill2train/gradients/final_fc_l2/LeakyRelu_grad/Shape_26train/gradients/final_fc_l2/LeakyRelu_grad/zeros/Const*'
_output_shapes
:€€€€€€€€€(*
T0*

index_type0
©
7train/gradients/final_fc_l2/LeakyRelu_grad/GreaterEqualGreaterEqualfinal_fc_l2/LeakyRelu/mulfinal_fc_l2/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€(
ь
@train/gradients/final_fc_l2/LeakyRelu_grad/BroadcastGradientArgsBroadcastGradientArgs0train/gradients/final_fc_l2/LeakyRelu_grad/Shape2train/gradients/final_fc_l2/LeakyRelu_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
І
1train/gradients/final_fc_l2/LeakyRelu_grad/SelectSelect7train/gradients/final_fc_l2/LeakyRelu_grad/GreaterEqualMtrain/gradients/final_fc_l2_dropout/dropout/div_grad/tuple/control_dependency0train/gradients/final_fc_l2/LeakyRelu_grad/zeros*'
_output_shapes
:€€€€€€€€€(*
T0
©
3train/gradients/final_fc_l2/LeakyRelu_grad/Select_1Select7train/gradients/final_fc_l2/LeakyRelu_grad/GreaterEqual0train/gradients/final_fc_l2/LeakyRelu_grad/zerosMtrain/gradients/final_fc_l2_dropout/dropout/div_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€(
к
.train/gradients/final_fc_l2/LeakyRelu_grad/SumSum1train/gradients/final_fc_l2/LeakyRelu_grad/Select@train/gradients/final_fc_l2/LeakyRelu_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
я
2train/gradients/final_fc_l2/LeakyRelu_grad/ReshapeReshape.train/gradients/final_fc_l2/LeakyRelu_grad/Sum0train/gradients/final_fc_l2/LeakyRelu_grad/Shape*'
_output_shapes
:€€€€€€€€€(*
T0*
Tshape0
р
0train/gradients/final_fc_l2/LeakyRelu_grad/Sum_1Sum3train/gradients/final_fc_l2/LeakyRelu_grad/Select_1Btrain/gradients/final_fc_l2/LeakyRelu_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
е
4train/gradients/final_fc_l2/LeakyRelu_grad/Reshape_1Reshape0train/gradients/final_fc_l2/LeakyRelu_grad/Sum_12train/gradients/final_fc_l2/LeakyRelu_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€(
ѓ
;train/gradients/final_fc_l2/LeakyRelu_grad/tuple/group_depsNoOp3^train/gradients/final_fc_l2/LeakyRelu_grad/Reshape5^train/gradients/final_fc_l2/LeakyRelu_grad/Reshape_1
Ї
Ctrain/gradients/final_fc_l2/LeakyRelu_grad/tuple/control_dependencyIdentity2train/gradients/final_fc_l2/LeakyRelu_grad/Reshape<^train/gradients/final_fc_l2/LeakyRelu_grad/tuple/group_deps*
T0*E
_class;
97loc:@train/gradients/final_fc_l2/LeakyRelu_grad/Reshape*'
_output_shapes
:€€€€€€€€€(
ј
Etrain/gradients/final_fc_l2/LeakyRelu_grad/tuple/control_dependency_1Identity4train/gradients/final_fc_l2/LeakyRelu_grad/Reshape_1<^train/gradients/final_fc_l2/LeakyRelu_grad/tuple/group_deps*
T0*G
_class=
;9loc:@train/gradients/final_fc_l2/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€(
w
4train/gradients/final_fc_l2/LeakyRelu/mul_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
Й
6train/gradients/final_fc_l2/LeakyRelu/mul_grad/Shape_1Shapefinal_fc_l2/BiasAdd*
T0*
out_type0*
_output_shapes
:
И
Dtrain/gradients/final_fc_l2/LeakyRelu/mul_grad/BroadcastGradientArgsBroadcastGradientArgs4train/gradients/final_fc_l2/LeakyRelu/mul_grad/Shape6train/gradients/final_fc_l2/LeakyRelu/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
≈
2train/gradients/final_fc_l2/LeakyRelu/mul_grad/MulMulCtrain/gradients/final_fc_l2/LeakyRelu_grad/tuple/control_dependencyfinal_fc_l2/BiasAdd*'
_output_shapes
:€€€€€€€€€(*
T0
у
2train/gradients/final_fc_l2/LeakyRelu/mul_grad/SumSum2train/gradients/final_fc_l2/LeakyRelu/mul_grad/MulDtrain/gradients/final_fc_l2/LeakyRelu/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Џ
6train/gradients/final_fc_l2/LeakyRelu/mul_grad/ReshapeReshape2train/gradients/final_fc_l2/LeakyRelu/mul_grad/Sum4train/gradients/final_fc_l2/LeakyRelu/mul_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
ѕ
4train/gradients/final_fc_l2/LeakyRelu/mul_grad/Mul_1Mulfinal_fc_l2/LeakyRelu/alphaCtrain/gradients/final_fc_l2/LeakyRelu_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€(
щ
4train/gradients/final_fc_l2/LeakyRelu/mul_grad/Sum_1Sum4train/gradients/final_fc_l2/LeakyRelu/mul_grad/Mul_1Ftrain/gradients/final_fc_l2/LeakyRelu/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
с
8train/gradients/final_fc_l2/LeakyRelu/mul_grad/Reshape_1Reshape4train/gradients/final_fc_l2/LeakyRelu/mul_grad/Sum_16train/gradients/final_fc_l2/LeakyRelu/mul_grad/Shape_1*'
_output_shapes
:€€€€€€€€€(*
T0*
Tshape0
ї
?train/gradients/final_fc_l2/LeakyRelu/mul_grad/tuple/group_depsNoOp7^train/gradients/final_fc_l2/LeakyRelu/mul_grad/Reshape9^train/gradients/final_fc_l2/LeakyRelu/mul_grad/Reshape_1
є
Gtrain/gradients/final_fc_l2/LeakyRelu/mul_grad/tuple/control_dependencyIdentity6train/gradients/final_fc_l2/LeakyRelu/mul_grad/Reshape@^train/gradients/final_fc_l2/LeakyRelu/mul_grad/tuple/group_deps*
T0*I
_class?
=;loc:@train/gradients/final_fc_l2/LeakyRelu/mul_grad/Reshape*
_output_shapes
: 
–
Itrain/gradients/final_fc_l2/LeakyRelu/mul_grad/tuple/control_dependency_1Identity8train/gradients/final_fc_l2/LeakyRelu/mul_grad/Reshape_1@^train/gradients/final_fc_l2/LeakyRelu/mul_grad/tuple/group_deps*
T0*K
_classA
?=loc:@train/gradients/final_fc_l2/LeakyRelu/mul_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€(
і
train/gradients/AddN_3AddNEtrain/gradients/final_fc_l2/LeakyRelu_grad/tuple/control_dependency_1Itrain/gradients/final_fc_l2/LeakyRelu/mul_grad/tuple/control_dependency_1*
T0*G
_class=
;9loc:@train/gradients/final_fc_l2/LeakyRelu_grad/Reshape_1*
N*'
_output_shapes
:€€€€€€€€€(
Ч
4train/gradients/final_fc_l2/BiasAdd_grad/BiasAddGradBiasAddGradtrain/gradients/AddN_3*
T0*
data_formatNHWC*
_output_shapes
:(
С
9train/gradients/final_fc_l2/BiasAdd_grad/tuple/group_depsNoOp^train/gradients/AddN_35^train/gradients/final_fc_l2/BiasAdd_grad/BiasAddGrad
Ь
Atrain/gradients/final_fc_l2/BiasAdd_grad/tuple/control_dependencyIdentitytrain/gradients/AddN_3:^train/gradients/final_fc_l2/BiasAdd_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€(*
T0*G
_class=
;9loc:@train/gradients/final_fc_l2/LeakyRelu_grad/Reshape_1
ѓ
Ctrain/gradients/final_fc_l2/BiasAdd_grad/tuple/control_dependency_1Identity4train/gradients/final_fc_l2/BiasAdd_grad/BiasAddGrad:^train/gradients/final_fc_l2/BiasAdd_grad/tuple/group_deps*
T0*G
_class=
;9loc:@train/gradients/final_fc_l2/BiasAdd_grad/BiasAddGrad*
_output_shapes
:(
м
.train/gradients/final_fc_l2/MatMul_grad/MatMulMatMulAtrain/gradients/final_fc_l2/BiasAdd_grad/tuple/control_dependencyfinal_fc_l2/kernel/read*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€2*
transpose_b(
н
0train/gradients/final_fc_l2/MatMul_grad/MatMul_1MatMulfinal_fc_l1_dropout/dropout/mulAtrain/gradients/final_fc_l2/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
_output_shapes

:2(*
transpose_b( 
§
8train/gradients/final_fc_l2/MatMul_grad/tuple/group_depsNoOp/^train/gradients/final_fc_l2/MatMul_grad/MatMul1^train/gradients/final_fc_l2/MatMul_grad/MatMul_1
ђ
@train/gradients/final_fc_l2/MatMul_grad/tuple/control_dependencyIdentity.train/gradients/final_fc_l2/MatMul_grad/MatMul9^train/gradients/final_fc_l2/MatMul_grad/tuple/group_deps*
T0*A
_class7
53loc:@train/gradients/final_fc_l2/MatMul_grad/MatMul*'
_output_shapes
:€€€€€€€€€2
©
Btrain/gradients/final_fc_l2/MatMul_grad/tuple/control_dependency_1Identity0train/gradients/final_fc_l2/MatMul_grad/MatMul_19^train/gradients/final_fc_l2/MatMul_grad/tuple/group_deps*
T0*C
_class9
75loc:@train/gradients/final_fc_l2/MatMul_grad/MatMul_1*
_output_shapes

:2(
Щ
:train/gradients/final_fc_l1_dropout/dropout/mul_grad/ShapeShapefinal_fc_l1_dropout/dropout/div*
T0*
out_type0*
_output_shapes
:
Э
<train/gradients/final_fc_l1_dropout/dropout/mul_grad/Shape_1Shape!final_fc_l1_dropout/dropout/Floor*
T0*
out_type0*
_output_shapes
:
Ъ
Jtrain/gradients/final_fc_l1_dropout/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/final_fc_l1_dropout/dropout/mul_grad/Shape<train/gradients/final_fc_l1_dropout/dropout/mul_grad/Shape_1*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€*
T0
÷
8train/gradients/final_fc_l1_dropout/dropout/mul_grad/MulMul@train/gradients/final_fc_l2/MatMul_grad/tuple/control_dependency!final_fc_l1_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€2
Е
8train/gradients/final_fc_l1_dropout/dropout/mul_grad/SumSum8train/gradients/final_fc_l1_dropout/dropout/mul_grad/MulJtrain/gradients/final_fc_l1_dropout/dropout/mul_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
э
<train/gradients/final_fc_l1_dropout/dropout/mul_grad/ReshapeReshape8train/gradients/final_fc_l1_dropout/dropout/mul_grad/Sum:train/gradients/final_fc_l1_dropout/dropout/mul_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€2
÷
:train/gradients/final_fc_l1_dropout/dropout/mul_grad/Mul_1Mulfinal_fc_l1_dropout/dropout/div@train/gradients/final_fc_l2/MatMul_grad/tuple/control_dependency*'
_output_shapes
:€€€€€€€€€2*
T0
Л
:train/gradients/final_fc_l1_dropout/dropout/mul_grad/Sum_1Sum:train/gradients/final_fc_l1_dropout/dropout/mul_grad/Mul_1Ltrain/gradients/final_fc_l1_dropout/dropout/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Г
>train/gradients/final_fc_l1_dropout/dropout/mul_grad/Reshape_1Reshape:train/gradients/final_fc_l1_dropout/dropout/mul_grad/Sum_1<train/gradients/final_fc_l1_dropout/dropout/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€2
Ќ
Etrain/gradients/final_fc_l1_dropout/dropout/mul_grad/tuple/group_depsNoOp=^train/gradients/final_fc_l1_dropout/dropout/mul_grad/Reshape?^train/gradients/final_fc_l1_dropout/dropout/mul_grad/Reshape_1
в
Mtrain/gradients/final_fc_l1_dropout/dropout/mul_grad/tuple/control_dependencyIdentity<train/gradients/final_fc_l1_dropout/dropout/mul_grad/ReshapeF^train/gradients/final_fc_l1_dropout/dropout/mul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@train/gradients/final_fc_l1_dropout/dropout/mul_grad/Reshape*'
_output_shapes
:€€€€€€€€€2
и
Otrain/gradients/final_fc_l1_dropout/dropout/mul_grad/tuple/control_dependency_1Identity>train/gradients/final_fc_l1_dropout/dropout/mul_grad/Reshape_1F^train/gradients/final_fc_l1_dropout/dropout/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€2*
T0*Q
_classG
ECloc:@train/gradients/final_fc_l1_dropout/dropout/mul_grad/Reshape_1
П
:train/gradients/final_fc_l1_dropout/dropout/div_grad/ShapeShapefinal_fc_l1/LeakyRelu*
_output_shapes
:*
T0*
out_type0

<train/gradients/final_fc_l1_dropout/dropout/div_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
Ъ
Jtrain/gradients/final_fc_l1_dropout/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/final_fc_l1_dropout/dropout/div_grad/Shape<train/gradients/final_fc_l1_dropout/dropout/div_grad/Shape_1*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€*
T0
п
<train/gradients/final_fc_l1_dropout/dropout/div_grad/RealDivRealDivMtrain/gradients/final_fc_l1_dropout/dropout/mul_grad/tuple/control_dependency%final_fc_l1_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€2
Й
8train/gradients/final_fc_l1_dropout/dropout/div_grad/SumSum<train/gradients/final_fc_l1_dropout/dropout/div_grad/RealDivJtrain/gradients/final_fc_l1_dropout/dropout/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
э
<train/gradients/final_fc_l1_dropout/dropout/div_grad/ReshapeReshape8train/gradients/final_fc_l1_dropout/dropout/div_grad/Sum:train/gradients/final_fc_l1_dropout/dropout/div_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€2
И
8train/gradients/final_fc_l1_dropout/dropout/div_grad/NegNegfinal_fc_l1/LeakyRelu*'
_output_shapes
:€€€€€€€€€2*
T0
№
>train/gradients/final_fc_l1_dropout/dropout/div_grad/RealDiv_1RealDiv8train/gradients/final_fc_l1_dropout/dropout/div_grad/Neg%final_fc_l1_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€2
в
>train/gradients/final_fc_l1_dropout/dropout/div_grad/RealDiv_2RealDiv>train/gradients/final_fc_l1_dropout/dropout/div_grad/RealDiv_1%final_fc_l1_dropout/dropout/keep_prob*'
_output_shapes
:€€€€€€€€€2*
T0
А
8train/gradients/final_fc_l1_dropout/dropout/div_grad/mulMulMtrain/gradients/final_fc_l1_dropout/dropout/mul_grad/tuple/control_dependency>train/gradients/final_fc_l1_dropout/dropout/div_grad/RealDiv_2*
T0*'
_output_shapes
:€€€€€€€€€2
Й
:train/gradients/final_fc_l1_dropout/dropout/div_grad/Sum_1Sum8train/gradients/final_fc_l1_dropout/dropout/div_grad/mulLtrain/gradients/final_fc_l1_dropout/dropout/div_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
т
>train/gradients/final_fc_l1_dropout/dropout/div_grad/Reshape_1Reshape:train/gradients/final_fc_l1_dropout/dropout/div_grad/Sum_1<train/gradients/final_fc_l1_dropout/dropout/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 
Ќ
Etrain/gradients/final_fc_l1_dropout/dropout/div_grad/tuple/group_depsNoOp=^train/gradients/final_fc_l1_dropout/dropout/div_grad/Reshape?^train/gradients/final_fc_l1_dropout/dropout/div_grad/Reshape_1
в
Mtrain/gradients/final_fc_l1_dropout/dropout/div_grad/tuple/control_dependencyIdentity<train/gradients/final_fc_l1_dropout/dropout/div_grad/ReshapeF^train/gradients/final_fc_l1_dropout/dropout/div_grad/tuple/group_deps*
T0*O
_classE
CAloc:@train/gradients/final_fc_l1_dropout/dropout/div_grad/Reshape*'
_output_shapes
:€€€€€€€€€2
„
Otrain/gradients/final_fc_l1_dropout/dropout/div_grad/tuple/control_dependency_1Identity>train/gradients/final_fc_l1_dropout/dropout/div_grad/Reshape_1F^train/gradients/final_fc_l1_dropout/dropout/div_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@train/gradients/final_fc_l1_dropout/dropout/div_grad/Reshape_1*
_output_shapes
: 
Й
0train/gradients/final_fc_l1/LeakyRelu_grad/ShapeShapefinal_fc_l1/LeakyRelu/mul*
T0*
out_type0*
_output_shapes
:
Е
2train/gradients/final_fc_l1/LeakyRelu_grad/Shape_1Shapefinal_fc_l1/BiasAdd*
_output_shapes
:*
T0*
out_type0
њ
2train/gradients/final_fc_l1/LeakyRelu_grad/Shape_2ShapeMtrain/gradients/final_fc_l1_dropout/dropout/div_grad/tuple/control_dependency*
T0*
out_type0*
_output_shapes
:

6train/gradients/final_fc_l1/LeakyRelu_grad/zeros/ConstConst*
valueB 2        *
dtype0*
_output_shapes
: 
и
0train/gradients/final_fc_l1/LeakyRelu_grad/zerosFill2train/gradients/final_fc_l1/LeakyRelu_grad/Shape_26train/gradients/final_fc_l1/LeakyRelu_grad/zeros/Const*
T0*

index_type0*'
_output_shapes
:€€€€€€€€€2
©
7train/gradients/final_fc_l1/LeakyRelu_grad/GreaterEqualGreaterEqualfinal_fc_l1/LeakyRelu/mulfinal_fc_l1/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€2
ь
@train/gradients/final_fc_l1/LeakyRelu_grad/BroadcastGradientArgsBroadcastGradientArgs0train/gradients/final_fc_l1/LeakyRelu_grad/Shape2train/gradients/final_fc_l1/LeakyRelu_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
І
1train/gradients/final_fc_l1/LeakyRelu_grad/SelectSelect7train/gradients/final_fc_l1/LeakyRelu_grad/GreaterEqualMtrain/gradients/final_fc_l1_dropout/dropout/div_grad/tuple/control_dependency0train/gradients/final_fc_l1/LeakyRelu_grad/zeros*
T0*'
_output_shapes
:€€€€€€€€€2
©
3train/gradients/final_fc_l1/LeakyRelu_grad/Select_1Select7train/gradients/final_fc_l1/LeakyRelu_grad/GreaterEqual0train/gradients/final_fc_l1/LeakyRelu_grad/zerosMtrain/gradients/final_fc_l1_dropout/dropout/div_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€2
к
.train/gradients/final_fc_l1/LeakyRelu_grad/SumSum1train/gradients/final_fc_l1/LeakyRelu_grad/Select@train/gradients/final_fc_l1/LeakyRelu_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
я
2train/gradients/final_fc_l1/LeakyRelu_grad/ReshapeReshape.train/gradients/final_fc_l1/LeakyRelu_grad/Sum0train/gradients/final_fc_l1/LeakyRelu_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€2
р
0train/gradients/final_fc_l1/LeakyRelu_grad/Sum_1Sum3train/gradients/final_fc_l1/LeakyRelu_grad/Select_1Btrain/gradients/final_fc_l1/LeakyRelu_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
е
4train/gradients/final_fc_l1/LeakyRelu_grad/Reshape_1Reshape0train/gradients/final_fc_l1/LeakyRelu_grad/Sum_12train/gradients/final_fc_l1/LeakyRelu_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€2
ѓ
;train/gradients/final_fc_l1/LeakyRelu_grad/tuple/group_depsNoOp3^train/gradients/final_fc_l1/LeakyRelu_grad/Reshape5^train/gradients/final_fc_l1/LeakyRelu_grad/Reshape_1
Ї
Ctrain/gradients/final_fc_l1/LeakyRelu_grad/tuple/control_dependencyIdentity2train/gradients/final_fc_l1/LeakyRelu_grad/Reshape<^train/gradients/final_fc_l1/LeakyRelu_grad/tuple/group_deps*
T0*E
_class;
97loc:@train/gradients/final_fc_l1/LeakyRelu_grad/Reshape*'
_output_shapes
:€€€€€€€€€2
ј
Etrain/gradients/final_fc_l1/LeakyRelu_grad/tuple/control_dependency_1Identity4train/gradients/final_fc_l1/LeakyRelu_grad/Reshape_1<^train/gradients/final_fc_l1/LeakyRelu_grad/tuple/group_deps*
T0*G
_class=
;9loc:@train/gradients/final_fc_l1/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€2
w
4train/gradients/final_fc_l1/LeakyRelu/mul_grad/ShapeConst*
dtype0*
_output_shapes
: *
valueB 
Й
6train/gradients/final_fc_l1/LeakyRelu/mul_grad/Shape_1Shapefinal_fc_l1/BiasAdd*
T0*
out_type0*
_output_shapes
:
И
Dtrain/gradients/final_fc_l1/LeakyRelu/mul_grad/BroadcastGradientArgsBroadcastGradientArgs4train/gradients/final_fc_l1/LeakyRelu/mul_grad/Shape6train/gradients/final_fc_l1/LeakyRelu/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
≈
2train/gradients/final_fc_l1/LeakyRelu/mul_grad/MulMulCtrain/gradients/final_fc_l1/LeakyRelu_grad/tuple/control_dependencyfinal_fc_l1/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€2
у
2train/gradients/final_fc_l1/LeakyRelu/mul_grad/SumSum2train/gradients/final_fc_l1/LeakyRelu/mul_grad/MulDtrain/gradients/final_fc_l1/LeakyRelu/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Џ
6train/gradients/final_fc_l1/LeakyRelu/mul_grad/ReshapeReshape2train/gradients/final_fc_l1/LeakyRelu/mul_grad/Sum4train/gradients/final_fc_l1/LeakyRelu/mul_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
ѕ
4train/gradients/final_fc_l1/LeakyRelu/mul_grad/Mul_1Mulfinal_fc_l1/LeakyRelu/alphaCtrain/gradients/final_fc_l1/LeakyRelu_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€2
щ
4train/gradients/final_fc_l1/LeakyRelu/mul_grad/Sum_1Sum4train/gradients/final_fc_l1/LeakyRelu/mul_grad/Mul_1Ftrain/gradients/final_fc_l1/LeakyRelu/mul_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
с
8train/gradients/final_fc_l1/LeakyRelu/mul_grad/Reshape_1Reshape4train/gradients/final_fc_l1/LeakyRelu/mul_grad/Sum_16train/gradients/final_fc_l1/LeakyRelu/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€2
ї
?train/gradients/final_fc_l1/LeakyRelu/mul_grad/tuple/group_depsNoOp7^train/gradients/final_fc_l1/LeakyRelu/mul_grad/Reshape9^train/gradients/final_fc_l1/LeakyRelu/mul_grad/Reshape_1
є
Gtrain/gradients/final_fc_l1/LeakyRelu/mul_grad/tuple/control_dependencyIdentity6train/gradients/final_fc_l1/LeakyRelu/mul_grad/Reshape@^train/gradients/final_fc_l1/LeakyRelu/mul_grad/tuple/group_deps*
T0*I
_class?
=;loc:@train/gradients/final_fc_l1/LeakyRelu/mul_grad/Reshape*
_output_shapes
: 
–
Itrain/gradients/final_fc_l1/LeakyRelu/mul_grad/tuple/control_dependency_1Identity8train/gradients/final_fc_l1/LeakyRelu/mul_grad/Reshape_1@^train/gradients/final_fc_l1/LeakyRelu/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€2*
T0*K
_classA
?=loc:@train/gradients/final_fc_l1/LeakyRelu/mul_grad/Reshape_1
і
train/gradients/AddN_4AddNEtrain/gradients/final_fc_l1/LeakyRelu_grad/tuple/control_dependency_1Itrain/gradients/final_fc_l1/LeakyRelu/mul_grad/tuple/control_dependency_1*
T0*G
_class=
;9loc:@train/gradients/final_fc_l1/LeakyRelu_grad/Reshape_1*
N*'
_output_shapes
:€€€€€€€€€2
Ч
4train/gradients/final_fc_l1/BiasAdd_grad/BiasAddGradBiasAddGradtrain/gradients/AddN_4*
data_formatNHWC*
_output_shapes
:2*
T0
С
9train/gradients/final_fc_l1/BiasAdd_grad/tuple/group_depsNoOp^train/gradients/AddN_45^train/gradients/final_fc_l1/BiasAdd_grad/BiasAddGrad
Ь
Atrain/gradients/final_fc_l1/BiasAdd_grad/tuple/control_dependencyIdentitytrain/gradients/AddN_4:^train/gradients/final_fc_l1/BiasAdd_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€2*
T0*G
_class=
;9loc:@train/gradients/final_fc_l1/LeakyRelu_grad/Reshape_1
ѓ
Ctrain/gradients/final_fc_l1/BiasAdd_grad/tuple/control_dependency_1Identity4train/gradients/final_fc_l1/BiasAdd_grad/BiasAddGrad:^train/gradients/final_fc_l1/BiasAdd_grad/tuple/group_deps*
_output_shapes
:2*
T0*G
_class=
;9loc:@train/gradients/final_fc_l1/BiasAdd_grad/BiasAddGrad
н
.train/gradients/final_fc_l1/MatMul_grad/MatMulMatMulAtrain/gradients/final_fc_l1/BiasAdd_grad/tuple/control_dependencyfinal_fc_l1/kernel/read*
T0*
transpose_a( *(
_output_shapes
:€€€€€€€€€»*
transpose_b(
„
0train/gradients/final_fc_l1/MatMul_grad/MatMul_1MatMulconcat_1Atrain/gradients/final_fc_l1/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
_output_shapes
:	»2*
transpose_b( 
§
8train/gradients/final_fc_l1/MatMul_grad/tuple/group_depsNoOp/^train/gradients/final_fc_l1/MatMul_grad/MatMul1^train/gradients/final_fc_l1/MatMul_grad/MatMul_1
≠
@train/gradients/final_fc_l1/MatMul_grad/tuple/control_dependencyIdentity.train/gradients/final_fc_l1/MatMul_grad/MatMul9^train/gradients/final_fc_l1/MatMul_grad/tuple/group_deps*
T0*A
_class7
53loc:@train/gradients/final_fc_l1/MatMul_grad/MatMul*(
_output_shapes
:€€€€€€€€€»
™
Btrain/gradients/final_fc_l1/MatMul_grad/tuple/control_dependency_1Identity0train/gradients/final_fc_l1/MatMul_grad/MatMul_19^train/gradients/final_fc_l1/MatMul_grad/tuple/group_deps*
T0*C
_class9
75loc:@train/gradients/final_fc_l1/MatMul_grad/MatMul_1*
_output_shapes
:	»2
d
"train/gradients/concat_1_grad/RankConst*
dtype0*
_output_shapes
: *
value	B :
Б
!train/gradients/concat_1_grad/modFloorModconcat_1/axis"train/gradients/concat_1_grad/Rank*
T0*
_output_shapes
: 
y
#train/gradients/concat_1_grad/ShapeShapecnn_outlayer/LeakyRelu*
T0*
out_type0*
_output_shapes
:
Є
$train/gradients/concat_1_grad/ShapeNShapeNcnn_outlayer/LeakyRelu,vect_expand_dense_layer2_dropout/dropout/mul*
T0*
out_type0*
N* 
_output_shapes
::
÷
*train/gradients/concat_1_grad/ConcatOffsetConcatOffset!train/gradients/concat_1_grad/mod$train/gradients/concat_1_grad/ShapeN&train/gradients/concat_1_grad/ShapeN:1*
N* 
_output_shapes
::
€
#train/gradients/concat_1_grad/SliceSlice@train/gradients/final_fc_l1/MatMul_grad/tuple/control_dependency*train/gradients/concat_1_grad/ConcatOffset$train/gradients/concat_1_grad/ShapeN*
T0*
Index0*'
_output_shapes
:€€€€€€€€€d
Е
%train/gradients/concat_1_grad/Slice_1Slice@train/gradients/final_fc_l1/MatMul_grad/tuple/control_dependency,train/gradients/concat_1_grad/ConcatOffset:1&train/gradients/concat_1_grad/ShapeN:1*
T0*
Index0*'
_output_shapes
:€€€€€€€€€d
Д
.train/gradients/concat_1_grad/tuple/group_depsNoOp$^train/gradients/concat_1_grad/Slice&^train/gradients/concat_1_grad/Slice_1
В
6train/gradients/concat_1_grad/tuple/control_dependencyIdentity#train/gradients/concat_1_grad/Slice/^train/gradients/concat_1_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€d*
T0*6
_class,
*(loc:@train/gradients/concat_1_grad/Slice
И
8train/gradients/concat_1_grad/tuple/control_dependency_1Identity%train/gradients/concat_1_grad/Slice_1/^train/gradients/concat_1_grad/tuple/group_deps*
T0*8
_class.
,*loc:@train/gradients/concat_1_grad/Slice_1*'
_output_shapes
:€€€€€€€€€d
Л
1train/gradients/cnn_outlayer/LeakyRelu_grad/ShapeShapecnn_outlayer/LeakyRelu/mul*
T0*
out_type0*
_output_shapes
:
З
3train/gradients/cnn_outlayer/LeakyRelu_grad/Shape_1Shapecnn_outlayer/BiasAdd*
T0*
out_type0*
_output_shapes
:
©
3train/gradients/cnn_outlayer/LeakyRelu_grad/Shape_2Shape6train/gradients/concat_1_grad/tuple/control_dependency*
T0*
out_type0*
_output_shapes
:
А
7train/gradients/cnn_outlayer/LeakyRelu_grad/zeros/ConstConst*
valueB 2        *
dtype0*
_output_shapes
: 
л
1train/gradients/cnn_outlayer/LeakyRelu_grad/zerosFill3train/gradients/cnn_outlayer/LeakyRelu_grad/Shape_27train/gradients/cnn_outlayer/LeakyRelu_grad/zeros/Const*
T0*

index_type0*'
_output_shapes
:€€€€€€€€€d
ђ
8train/gradients/cnn_outlayer/LeakyRelu_grad/GreaterEqualGreaterEqualcnn_outlayer/LeakyRelu/mulcnn_outlayer/BiasAdd*'
_output_shapes
:€€€€€€€€€d*
T0
€
Atrain/gradients/cnn_outlayer/LeakyRelu_grad/BroadcastGradientArgsBroadcastGradientArgs1train/gradients/cnn_outlayer/LeakyRelu_grad/Shape3train/gradients/cnn_outlayer/LeakyRelu_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
У
2train/gradients/cnn_outlayer/LeakyRelu_grad/SelectSelect8train/gradients/cnn_outlayer/LeakyRelu_grad/GreaterEqual6train/gradients/concat_1_grad/tuple/control_dependency1train/gradients/cnn_outlayer/LeakyRelu_grad/zeros*'
_output_shapes
:€€€€€€€€€d*
T0
Х
4train/gradients/cnn_outlayer/LeakyRelu_grad/Select_1Select8train/gradients/cnn_outlayer/LeakyRelu_grad/GreaterEqual1train/gradients/cnn_outlayer/LeakyRelu_grad/zeros6train/gradients/concat_1_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€d
н
/train/gradients/cnn_outlayer/LeakyRelu_grad/SumSum2train/gradients/cnn_outlayer/LeakyRelu_grad/SelectAtrain/gradients/cnn_outlayer/LeakyRelu_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
в
3train/gradients/cnn_outlayer/LeakyRelu_grad/ReshapeReshape/train/gradients/cnn_outlayer/LeakyRelu_grad/Sum1train/gradients/cnn_outlayer/LeakyRelu_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
у
1train/gradients/cnn_outlayer/LeakyRelu_grad/Sum_1Sum4train/gradients/cnn_outlayer/LeakyRelu_grad/Select_1Ctrain/gradients/cnn_outlayer/LeakyRelu_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
и
5train/gradients/cnn_outlayer/LeakyRelu_grad/Reshape_1Reshape1train/gradients/cnn_outlayer/LeakyRelu_grad/Sum_13train/gradients/cnn_outlayer/LeakyRelu_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
≤
<train/gradients/cnn_outlayer/LeakyRelu_grad/tuple/group_depsNoOp4^train/gradients/cnn_outlayer/LeakyRelu_grad/Reshape6^train/gradients/cnn_outlayer/LeakyRelu_grad/Reshape_1
Њ
Dtrain/gradients/cnn_outlayer/LeakyRelu_grad/tuple/control_dependencyIdentity3train/gradients/cnn_outlayer/LeakyRelu_grad/Reshape=^train/gradients/cnn_outlayer/LeakyRelu_grad/tuple/group_deps*
T0*F
_class<
:8loc:@train/gradients/cnn_outlayer/LeakyRelu_grad/Reshape*'
_output_shapes
:€€€€€€€€€d
ƒ
Ftrain/gradients/cnn_outlayer/LeakyRelu_grad/tuple/control_dependency_1Identity5train/gradients/cnn_outlayer/LeakyRelu_grad/Reshape_1=^train/gradients/cnn_outlayer/LeakyRelu_grad/tuple/group_deps*
T0*H
_class>
<:loc:@train/gradients/cnn_outlayer/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€d
≥
Gtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/ShapeShape,vect_expand_dense_layer2_dropout/dropout/div*
T0*
out_type0*
_output_shapes
:
Ј
Itrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Shape_1Shape.vect_expand_dense_layer2_dropout/dropout/Floor*
T0*
out_type0*
_output_shapes
:
Ѕ
Wtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgsGtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/ShapeItrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
и
Etrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/MulMul8train/gradients/concat_1_grad/tuple/control_dependency_1.vect_expand_dense_layer2_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€d
ђ
Etrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/SumSumEtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/MulWtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
§
Itrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/ReshapeReshapeEtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/SumGtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
и
Gtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Mul_1Mul,vect_expand_dense_layer2_dropout/dropout/div8train/gradients/concat_1_grad/tuple/control_dependency_1*'
_output_shapes
:€€€€€€€€€d*
T0
≤
Gtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Sum_1SumGtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Mul_1Ytrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
™
Ktrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Reshape_1ReshapeGtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Sum_1Itrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
ф
Rtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/tuple/group_depsNoOpJ^train/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/ReshapeL^train/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Reshape_1
Ц
Ztrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/tuple/control_dependencyIdentityItrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/ReshapeS^train/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€d*
T0*\
_classR
PNloc:@train/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Reshape
Ь
\train/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/tuple/control_dependency_1IdentityKtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Reshape_1S^train/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€d*
T0*^
_classT
RPloc:@train/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/Reshape_1
x
5train/gradients/cnn_outlayer/LeakyRelu/mul_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
Л
7train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Shape_1Shapecnn_outlayer/BiasAdd*
T0*
out_type0*
_output_shapes
:
Л
Etrain/gradients/cnn_outlayer/LeakyRelu/mul_grad/BroadcastGradientArgsBroadcastGradientArgs5train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Shape7train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
»
3train/gradients/cnn_outlayer/LeakyRelu/mul_grad/MulMulDtrain/gradients/cnn_outlayer/LeakyRelu_grad/tuple/control_dependencycnn_outlayer/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€d
ц
3train/gradients/cnn_outlayer/LeakyRelu/mul_grad/SumSum3train/gradients/cnn_outlayer/LeakyRelu/mul_grad/MulEtrain/gradients/cnn_outlayer/LeakyRelu/mul_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
Ё
7train/gradients/cnn_outlayer/LeakyRelu/mul_grad/ReshapeReshape3train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Sum5train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
“
5train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Mul_1Mulcnn_outlayer/LeakyRelu/alphaDtrain/gradients/cnn_outlayer/LeakyRelu_grad/tuple/control_dependency*'
_output_shapes
:€€€€€€€€€d*
T0
ь
5train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Sum_1Sum5train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Mul_1Gtrain/gradients/cnn_outlayer/LeakyRelu/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ф
9train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Reshape_1Reshape5train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Sum_17train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
Њ
@train/gradients/cnn_outlayer/LeakyRelu/mul_grad/tuple/group_depsNoOp8^train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Reshape:^train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Reshape_1
љ
Htrain/gradients/cnn_outlayer/LeakyRelu/mul_grad/tuple/control_dependencyIdentity7train/gradients/cnn_outlayer/LeakyRelu/mul_grad/ReshapeA^train/gradients/cnn_outlayer/LeakyRelu/mul_grad/tuple/group_deps*
T0*J
_class@
><loc:@train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Reshape*
_output_shapes
: 
‘
Jtrain/gradients/cnn_outlayer/LeakyRelu/mul_grad/tuple/control_dependency_1Identity9train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Reshape_1A^train/gradients/cnn_outlayer/LeakyRelu/mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@train/gradients/cnn_outlayer/LeakyRelu/mul_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€d
©
Gtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/ShapeShape"vect_expand_dense_layer2/LeakyRelu*
T0*
out_type0*
_output_shapes
:
М
Itrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
Ѕ
Wtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgsGtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/ShapeItrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
Ц
Itrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/RealDivRealDivZtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/tuple/control_dependency2vect_expand_dense_layer2_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€d
∞
Etrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/SumSumItrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/RealDivWtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
§
Itrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/ReshapeReshapeEtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/SumGtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
Ґ
Etrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/NegNeg"vect_expand_dense_layer2/LeakyRelu*'
_output_shapes
:€€€€€€€€€d*
T0
Г
Ktrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/RealDiv_1RealDivEtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Neg2vect_expand_dense_layer2_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€d
Й
Ktrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/RealDiv_2RealDivKtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/RealDiv_12vect_expand_dense_layer2_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€d
І
Etrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/mulMulZtrain/gradients/vect_expand_dense_layer2_dropout/dropout/mul_grad/tuple/control_dependencyKtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/RealDiv_2*'
_output_shapes
:€€€€€€€€€d*
T0
∞
Gtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Sum_1SumEtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/mulYtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Щ
Ktrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Reshape_1ReshapeGtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Sum_1Itrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Shape_1*
_output_shapes
: *
T0*
Tshape0
ф
Rtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/tuple/group_depsNoOpJ^train/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/ReshapeL^train/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Reshape_1
Ц
Ztrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/tuple/control_dependencyIdentityItrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/ReshapeS^train/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/tuple/group_deps*
T0*\
_classR
PNloc:@train/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Reshape*'
_output_shapes
:€€€€€€€€€d
Л
\train/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/tuple/control_dependency_1IdentityKtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Reshape_1S^train/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/tuple/group_deps*
T0*^
_classT
RPloc:@train/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/Reshape_1*
_output_shapes
: 
Ј
train/gradients/AddN_5AddNFtrain/gradients/cnn_outlayer/LeakyRelu_grad/tuple/control_dependency_1Jtrain/gradients/cnn_outlayer/LeakyRelu/mul_grad/tuple/control_dependency_1*
T0*H
_class>
<:loc:@train/gradients/cnn_outlayer/LeakyRelu_grad/Reshape_1*
N*'
_output_shapes
:€€€€€€€€€d
Ш
5train/gradients/cnn_outlayer/BiasAdd_grad/BiasAddGradBiasAddGradtrain/gradients/AddN_5*
T0*
data_formatNHWC*
_output_shapes
:d
У
:train/gradients/cnn_outlayer/BiasAdd_grad/tuple/group_depsNoOp^train/gradients/AddN_56^train/gradients/cnn_outlayer/BiasAdd_grad/BiasAddGrad
Я
Btrain/gradients/cnn_outlayer/BiasAdd_grad/tuple/control_dependencyIdentitytrain/gradients/AddN_5;^train/gradients/cnn_outlayer/BiasAdd_grad/tuple/group_deps*
T0*H
_class>
<:loc:@train/gradients/cnn_outlayer/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€d
≥
Dtrain/gradients/cnn_outlayer/BiasAdd_grad/tuple/control_dependency_1Identity5train/gradients/cnn_outlayer/BiasAdd_grad/BiasAddGrad;^train/gradients/cnn_outlayer/BiasAdd_grad/tuple/group_deps*
T0*H
_class>
<:loc:@train/gradients/cnn_outlayer/BiasAdd_grad/BiasAddGrad*
_output_shapes
:d
£
=train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/ShapeShape&vect_expand_dense_layer2/LeakyRelu/mul*
T0*
out_type0*
_output_shapes
:
Я
?train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Shape_1Shape vect_expand_dense_layer2/BiasAdd*
T0*
out_type0*
_output_shapes
:
ў
?train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Shape_2ShapeZtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/tuple/control_dependency*
T0*
out_type0*
_output_shapes
:
М
Ctrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/zeros/ConstConst*
valueB 2        *
dtype0*
_output_shapes
: 
П
=train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/zerosFill?train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Shape_2Ctrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/zeros/Const*'
_output_shapes
:€€€€€€€€€d*
T0*

index_type0
–
Dtrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/GreaterEqualGreaterEqual&vect_expand_dense_layer2/LeakyRelu/mul vect_expand_dense_layer2/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€d
£
Mtrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/BroadcastGradientArgsBroadcastGradientArgs=train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Shape?train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
џ
>train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/SelectSelectDtrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/GreaterEqualZtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/tuple/control_dependency=train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/zeros*
T0*'
_output_shapes
:€€€€€€€€€d
Ё
@train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Select_1SelectDtrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/GreaterEqual=train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/zerosZtrain/gradients/vect_expand_dense_layer2_dropout/dropout/div_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€d
С
;train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/SumSum>train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/SelectMtrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ж
?train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/ReshapeReshape;train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Sum=train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
Ч
=train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Sum_1Sum@train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Select_1Otrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
М
Atrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Reshape_1Reshape=train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Sum_1?train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Shape_1*'
_output_shapes
:€€€€€€€€€d*
T0*
Tshape0
÷
Htrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/tuple/group_depsNoOp@^train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/ReshapeB^train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Reshape_1
о
Ptrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/tuple/control_dependencyIdentity?train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/ReshapeI^train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/tuple/group_deps*
T0*R
_classH
FDloc:@train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Reshape*'
_output_shapes
:€€€€€€€€€d
ф
Rtrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/tuple/control_dependency_1IdentityAtrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Reshape_1I^train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€d
р
/train/gradients/cnn_outlayer/MatMul_grad/MatMulMatMulBtrain/gradients/cnn_outlayer/BiasAdd_grad/tuple/control_dependencycnn_outlayer/kernel/read*
T0*
transpose_a( *(
_output_shapes
:€€€€€€€€€С*
transpose_b(
Ў
1train/gradients/cnn_outlayer/MatMul_grad/MatMul_1MatMulReshapeBtrain/gradients/cnn_outlayer/BiasAdd_grad/tuple/control_dependency*
transpose_a(*
_output_shapes
:	Сd*
transpose_b( *
T0
І
9train/gradients/cnn_outlayer/MatMul_grad/tuple/group_depsNoOp0^train/gradients/cnn_outlayer/MatMul_grad/MatMul2^train/gradients/cnn_outlayer/MatMul_grad/MatMul_1
±
Atrain/gradients/cnn_outlayer/MatMul_grad/tuple/control_dependencyIdentity/train/gradients/cnn_outlayer/MatMul_grad/MatMul:^train/gradients/cnn_outlayer/MatMul_grad/tuple/group_deps*
T0*B
_class8
64loc:@train/gradients/cnn_outlayer/MatMul_grad/MatMul*(
_output_shapes
:€€€€€€€€€С
Ѓ
Ctrain/gradients/cnn_outlayer/MatMul_grad/tuple/control_dependency_1Identity1train/gradients/cnn_outlayer/MatMul_grad/MatMul_1:^train/gradients/cnn_outlayer/MatMul_grad/tuple/group_deps*
T0*D
_class:
86loc:@train/gradients/cnn_outlayer/MatMul_grad/MatMul_1*
_output_shapes
:	Сd
Д
Atrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
£
Ctrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Shape_1Shape vect_expand_dense_layer2/BiasAdd*
T0*
out_type0*
_output_shapes
:
ѓ
Qtrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/BroadcastGradientArgsBroadcastGradientArgsAtrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/ShapeCtrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
м
?train/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/MulMulPtrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/tuple/control_dependency vect_expand_dense_layer2/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€d
Ъ
?train/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/SumSum?train/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/MulQtrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Б
Ctrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/ReshapeReshape?train/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/SumAtrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
ц
Atrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Mul_1Mul(vect_expand_dense_layer2/LeakyRelu/alphaPtrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€d
†
Atrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Sum_1SumAtrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Mul_1Strain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ш
Etrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Reshape_1ReshapeAtrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Sum_1Ctrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
в
Ltrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/tuple/group_depsNoOpD^train/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/ReshapeF^train/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Reshape_1
н
Ttrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/tuple/control_dependencyIdentityCtrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/ReshapeM^train/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/tuple/group_deps*
T0*V
_classL
JHloc:@train/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Reshape*
_output_shapes
: 
Д
Vtrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/tuple/control_dependency_1IdentityEtrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Reshape_1M^train/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@train/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€d
z
"train/gradients/Reshape_grad/ShapeShapeconv_layer2_pool/MaxPool*
T0*
out_type0*
_output_shapes
:
ё
$train/gradients/Reshape_grad/ReshapeReshapeAtrain/gradients/cnn_outlayer/MatMul_grad/tuple/control_dependency"train/gradients/Reshape_grad/Shape*
T0*
Tshape0*/
_output_shapes
:€€€€€€€€€
џ
train/gradients/AddN_6AddNRtrain/gradients/vect_expand_dense_layer2/LeakyRelu_grad/tuple/control_dependency_1Vtrain/gradients/vect_expand_dense_layer2/LeakyRelu/mul_grad/tuple/control_dependency_1*
T0*T
_classJ
HFloc:@train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Reshape_1*
N*'
_output_shapes
:€€€€€€€€€d
§
Atrain/gradients/vect_expand_dense_layer2/BiasAdd_grad/BiasAddGradBiasAddGradtrain/gradients/AddN_6*
data_formatNHWC*
_output_shapes
:d*
T0
Ђ
Ftrain/gradients/vect_expand_dense_layer2/BiasAdd_grad/tuple/group_depsNoOp^train/gradients/AddN_6B^train/gradients/vect_expand_dense_layer2/BiasAdd_grad/BiasAddGrad
√
Ntrain/gradients/vect_expand_dense_layer2/BiasAdd_grad/tuple/control_dependencyIdentitytrain/gradients/AddN_6G^train/gradients/vect_expand_dense_layer2/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@train/gradients/vect_expand_dense_layer2/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€d
г
Ptrain/gradients/vect_expand_dense_layer2/BiasAdd_grad/tuple/control_dependency_1IdentityAtrain/gradients/vect_expand_dense_layer2/BiasAdd_grad/BiasAddGradG^train/gradients/vect_expand_dense_layer2/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@train/gradients/vect_expand_dense_layer2/BiasAdd_grad/BiasAddGrad*
_output_shapes
:d
ґ
9train/gradients/conv_layer2_pool/MaxPool_grad/MaxPoolGradMaxPoolGradconv_layer2_dropout/dropout/mulconv_layer2_pool/MaxPool$train/gradients/Reshape_grad/Reshape*
T0*
data_formatNHWC*
strides
*
ksize
*
paddingVALID*/
_output_shapes
:€€€€€€€€€//
У
;train/gradients/vect_expand_dense_layer2/MatMul_grad/MatMulMatMulNtrain/gradients/vect_expand_dense_layer2/BiasAdd_grad/tuple/control_dependency$vect_expand_dense_layer2/kernel/read*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€d*
transpose_b(
Ф
=train/gradients/vect_expand_dense_layer2/MatMul_grad/MatMul_1MatMul,vect_expand_dense_layer1_dropout/dropout/mulNtrain/gradients/vect_expand_dense_layer2/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
_output_shapes

:dd*
transpose_b( 
Ћ
Etrain/gradients/vect_expand_dense_layer2/MatMul_grad/tuple/group_depsNoOp<^train/gradients/vect_expand_dense_layer2/MatMul_grad/MatMul>^train/gradients/vect_expand_dense_layer2/MatMul_grad/MatMul_1
а
Mtrain/gradients/vect_expand_dense_layer2/MatMul_grad/tuple/control_dependencyIdentity;train/gradients/vect_expand_dense_layer2/MatMul_grad/MatMulF^train/gradients/vect_expand_dense_layer2/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@train/gradients/vect_expand_dense_layer2/MatMul_grad/MatMul*'
_output_shapes
:€€€€€€€€€d
Ё
Otrain/gradients/vect_expand_dense_layer2/MatMul_grad/tuple/control_dependency_1Identity=train/gradients/vect_expand_dense_layer2/MatMul_grad/MatMul_1F^train/gradients/vect_expand_dense_layer2/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@train/gradients/vect_expand_dense_layer2/MatMul_grad/MatMul_1*
_output_shapes

:dd
Щ
:train/gradients/conv_layer2_dropout/dropout/mul_grad/ShapeShapeconv_layer2_dropout/dropout/div*
_output_shapes
:*
T0*
out_type0
Э
<train/gradients/conv_layer2_dropout/dropout/mul_grad/Shape_1Shape!conv_layer2_dropout/dropout/Floor*
T0*
out_type0*
_output_shapes
:
Ъ
Jtrain/gradients/conv_layer2_dropout/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/conv_layer2_dropout/dropout/mul_grad/Shape<train/gradients/conv_layer2_dropout/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
„
8train/gradients/conv_layer2_dropout/dropout/mul_grad/MulMul9train/gradients/conv_layer2_pool/MaxPool_grad/MaxPoolGrad!conv_layer2_dropout/dropout/Floor*/
_output_shapes
:€€€€€€€€€//*
T0
Е
8train/gradients/conv_layer2_dropout/dropout/mul_grad/SumSum8train/gradients/conv_layer2_dropout/dropout/mul_grad/MulJtrain/gradients/conv_layer2_dropout/dropout/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Е
<train/gradients/conv_layer2_dropout/dropout/mul_grad/ReshapeReshape8train/gradients/conv_layer2_dropout/dropout/mul_grad/Sum:train/gradients/conv_layer2_dropout/dropout/mul_grad/Shape*
T0*
Tshape0*/
_output_shapes
:€€€€€€€€€//
„
:train/gradients/conv_layer2_dropout/dropout/mul_grad/Mul_1Mulconv_layer2_dropout/dropout/div9train/gradients/conv_layer2_pool/MaxPool_grad/MaxPoolGrad*
T0*/
_output_shapes
:€€€€€€€€€//
Л
:train/gradients/conv_layer2_dropout/dropout/mul_grad/Sum_1Sum:train/gradients/conv_layer2_dropout/dropout/mul_grad/Mul_1Ltrain/gradients/conv_layer2_dropout/dropout/mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
Л
>train/gradients/conv_layer2_dropout/dropout/mul_grad/Reshape_1Reshape:train/gradients/conv_layer2_dropout/dropout/mul_grad/Sum_1<train/gradients/conv_layer2_dropout/dropout/mul_grad/Shape_1*/
_output_shapes
:€€€€€€€€€//*
T0*
Tshape0
Ќ
Etrain/gradients/conv_layer2_dropout/dropout/mul_grad/tuple/group_depsNoOp=^train/gradients/conv_layer2_dropout/dropout/mul_grad/Reshape?^train/gradients/conv_layer2_dropout/dropout/mul_grad/Reshape_1
к
Mtrain/gradients/conv_layer2_dropout/dropout/mul_grad/tuple/control_dependencyIdentity<train/gradients/conv_layer2_dropout/dropout/mul_grad/ReshapeF^train/gradients/conv_layer2_dropout/dropout/mul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@train/gradients/conv_layer2_dropout/dropout/mul_grad/Reshape*/
_output_shapes
:€€€€€€€€€//
р
Otrain/gradients/conv_layer2_dropout/dropout/mul_grad/tuple/control_dependency_1Identity>train/gradients/conv_layer2_dropout/dropout/mul_grad/Reshape_1F^train/gradients/conv_layer2_dropout/dropout/mul_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@train/gradients/conv_layer2_dropout/dropout/mul_grad/Reshape_1*/
_output_shapes
:€€€€€€€€€//
≥
Gtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/ShapeShape,vect_expand_dense_layer1_dropout/dropout/div*
T0*
out_type0*
_output_shapes
:
Ј
Itrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Shape_1Shape.vect_expand_dense_layer1_dropout/dropout/Floor*
T0*
out_type0*
_output_shapes
:
Ѕ
Wtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgsGtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/ShapeItrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
э
Etrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/MulMulMtrain/gradients/vect_expand_dense_layer2/MatMul_grad/tuple/control_dependency.vect_expand_dense_layer1_dropout/dropout/Floor*'
_output_shapes
:€€€€€€€€€d*
T0
ђ
Etrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/SumSumEtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/MulWtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
§
Itrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/ReshapeReshapeEtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/SumGtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Shape*'
_output_shapes
:€€€€€€€€€d*
T0*
Tshape0
э
Gtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Mul_1Mul,vect_expand_dense_layer1_dropout/dropout/divMtrain/gradients/vect_expand_dense_layer2/MatMul_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€d
≤
Gtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Sum_1SumGtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Mul_1Ytrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
™
Ktrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Reshape_1ReshapeGtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Sum_1Itrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
ф
Rtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/tuple/group_depsNoOpJ^train/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/ReshapeL^train/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Reshape_1
Ц
Ztrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/tuple/control_dependencyIdentityItrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/ReshapeS^train/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€d*
T0*\
_classR
PNloc:@train/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Reshape
Ь
\train/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/tuple/control_dependency_1IdentityKtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Reshape_1S^train/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€d*
T0*^
_classT
RPloc:@train/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/Reshape_1
П
:train/gradients/conv_layer2_dropout/dropout/div_grad/ShapeShapeconv_layer2_conv/Relu*
T0*
out_type0*
_output_shapes
:

<train/gradients/conv_layer2_dropout/dropout/div_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
Ъ
Jtrain/gradients/conv_layer2_dropout/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/conv_layer2_dropout/dropout/div_grad/Shape<train/gradients/conv_layer2_dropout/dropout/div_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
ч
<train/gradients/conv_layer2_dropout/dropout/div_grad/RealDivRealDivMtrain/gradients/conv_layer2_dropout/dropout/mul_grad/tuple/control_dependency%conv_layer2_dropout/dropout/keep_prob*/
_output_shapes
:€€€€€€€€€//*
T0
Й
8train/gradients/conv_layer2_dropout/dropout/div_grad/SumSum<train/gradients/conv_layer2_dropout/dropout/div_grad/RealDivJtrain/gradients/conv_layer2_dropout/dropout/div_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
Е
<train/gradients/conv_layer2_dropout/dropout/div_grad/ReshapeReshape8train/gradients/conv_layer2_dropout/dropout/div_grad/Sum:train/gradients/conv_layer2_dropout/dropout/div_grad/Shape*
T0*
Tshape0*/
_output_shapes
:€€€€€€€€€//
Р
8train/gradients/conv_layer2_dropout/dropout/div_grad/NegNegconv_layer2_conv/Relu*
T0*/
_output_shapes
:€€€€€€€€€//
д
>train/gradients/conv_layer2_dropout/dropout/div_grad/RealDiv_1RealDiv8train/gradients/conv_layer2_dropout/dropout/div_grad/Neg%conv_layer2_dropout/dropout/keep_prob*/
_output_shapes
:€€€€€€€€€//*
T0
к
>train/gradients/conv_layer2_dropout/dropout/div_grad/RealDiv_2RealDiv>train/gradients/conv_layer2_dropout/dropout/div_grad/RealDiv_1%conv_layer2_dropout/dropout/keep_prob*
T0*/
_output_shapes
:€€€€€€€€€//
И
8train/gradients/conv_layer2_dropout/dropout/div_grad/mulMulMtrain/gradients/conv_layer2_dropout/dropout/mul_grad/tuple/control_dependency>train/gradients/conv_layer2_dropout/dropout/div_grad/RealDiv_2*
T0*/
_output_shapes
:€€€€€€€€€//
Й
:train/gradients/conv_layer2_dropout/dropout/div_grad/Sum_1Sum8train/gradients/conv_layer2_dropout/dropout/div_grad/mulLtrain/gradients/conv_layer2_dropout/dropout/div_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
т
>train/gradients/conv_layer2_dropout/dropout/div_grad/Reshape_1Reshape:train/gradients/conv_layer2_dropout/dropout/div_grad/Sum_1<train/gradients/conv_layer2_dropout/dropout/div_grad/Shape_1*
_output_shapes
: *
T0*
Tshape0
Ќ
Etrain/gradients/conv_layer2_dropout/dropout/div_grad/tuple/group_depsNoOp=^train/gradients/conv_layer2_dropout/dropout/div_grad/Reshape?^train/gradients/conv_layer2_dropout/dropout/div_grad/Reshape_1
к
Mtrain/gradients/conv_layer2_dropout/dropout/div_grad/tuple/control_dependencyIdentity<train/gradients/conv_layer2_dropout/dropout/div_grad/ReshapeF^train/gradients/conv_layer2_dropout/dropout/div_grad/tuple/group_deps*
T0*O
_classE
CAloc:@train/gradients/conv_layer2_dropout/dropout/div_grad/Reshape*/
_output_shapes
:€€€€€€€€€//
„
Otrain/gradients/conv_layer2_dropout/dropout/div_grad/tuple/control_dependency_1Identity>train/gradients/conv_layer2_dropout/dropout/div_grad/Reshape_1F^train/gradients/conv_layer2_dropout/dropout/div_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@train/gradients/conv_layer2_dropout/dropout/div_grad/Reshape_1*
_output_shapes
: 
©
Gtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/ShapeShape"vect_expand_dense_layer1/LeakyRelu*
T0*
out_type0*
_output_shapes
:
М
Itrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
Ѕ
Wtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgsGtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/ShapeItrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
Ц
Itrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/RealDivRealDivZtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/tuple/control_dependency2vect_expand_dense_layer1_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€d
∞
Etrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/SumSumItrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/RealDivWtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
§
Itrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/ReshapeReshapeEtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/SumGtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
Ґ
Etrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/NegNeg"vect_expand_dense_layer1/LeakyRelu*
T0*'
_output_shapes
:€€€€€€€€€d
Г
Ktrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/RealDiv_1RealDivEtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Neg2vect_expand_dense_layer1_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€d
Й
Ktrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/RealDiv_2RealDivKtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/RealDiv_12vect_expand_dense_layer1_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€d
І
Etrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/mulMulZtrain/gradients/vect_expand_dense_layer1_dropout/dropout/mul_grad/tuple/control_dependencyKtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/RealDiv_2*
T0*'
_output_shapes
:€€€€€€€€€d
∞
Gtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Sum_1SumEtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/mulYtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
Щ
Ktrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Reshape_1ReshapeGtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Sum_1Itrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 
ф
Rtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/tuple/group_depsNoOpJ^train/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/ReshapeL^train/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Reshape_1
Ц
Ztrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/tuple/control_dependencyIdentityItrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/ReshapeS^train/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/tuple/group_deps*
T0*\
_classR
PNloc:@train/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Reshape*'
_output_shapes
:€€€€€€€€€d
Л
\train/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/tuple/control_dependency_1IdentityKtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Reshape_1S^train/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/tuple/group_deps*
T0*^
_classT
RPloc:@train/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/Reshape_1*
_output_shapes
: 
я
3train/gradients/conv_layer2_conv/Relu_grad/ReluGradReluGradMtrain/gradients/conv_layer2_dropout/dropout/div_grad/tuple/control_dependencyconv_layer2_conv/Relu*
T0*/
_output_shapes
:€€€€€€€€€//
£
=train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/ShapeShape&vect_expand_dense_layer1/LeakyRelu/mul*
T0*
out_type0*
_output_shapes
:
Я
?train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Shape_1Shape vect_expand_dense_layer1/BiasAdd*
_output_shapes
:*
T0*
out_type0
ў
?train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Shape_2ShapeZtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/tuple/control_dependency*
T0*
out_type0*
_output_shapes
:
М
Ctrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/zeros/ConstConst*
valueB 2        *
dtype0*
_output_shapes
: 
П
=train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/zerosFill?train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Shape_2Ctrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/zeros/Const*
T0*

index_type0*'
_output_shapes
:€€€€€€€€€d
–
Dtrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/GreaterEqualGreaterEqual&vect_expand_dense_layer1/LeakyRelu/mul vect_expand_dense_layer1/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€d
£
Mtrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/BroadcastGradientArgsBroadcastGradientArgs=train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Shape?train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
џ
>train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/SelectSelectDtrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/GreaterEqualZtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/tuple/control_dependency=train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/zeros*
T0*'
_output_shapes
:€€€€€€€€€d
Ё
@train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Select_1SelectDtrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/GreaterEqual=train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/zerosZtrain/gradients/vect_expand_dense_layer1_dropout/dropout/div_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€d
С
;train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/SumSum>train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/SelectMtrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ж
?train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/ReshapeReshape;train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Sum=train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
Ч
=train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Sum_1Sum@train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Select_1Otrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
М
Atrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Reshape_1Reshape=train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Sum_1?train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
÷
Htrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/tuple/group_depsNoOp@^train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/ReshapeB^train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Reshape_1
о
Ptrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/tuple/control_dependencyIdentity?train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/ReshapeI^train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/tuple/group_deps*
T0*R
_classH
FDloc:@train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Reshape*'
_output_shapes
:€€€€€€€€€d
ф
Rtrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/tuple/control_dependency_1IdentityAtrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Reshape_1I^train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€d
є
9train/gradients/conv_layer2_conv/BiasAdd_grad/BiasAddGradBiasAddGrad3train/gradients/conv_layer2_conv/Relu_grad/ReluGrad*
data_formatNHWC*
_output_shapes
:*
T0
Є
>train/gradients/conv_layer2_conv/BiasAdd_grad/tuple/group_depsNoOp:^train/gradients/conv_layer2_conv/BiasAdd_grad/BiasAddGrad4^train/gradients/conv_layer2_conv/Relu_grad/ReluGrad
 
Ftrain/gradients/conv_layer2_conv/BiasAdd_grad/tuple/control_dependencyIdentity3train/gradients/conv_layer2_conv/Relu_grad/ReluGrad?^train/gradients/conv_layer2_conv/BiasAdd_grad/tuple/group_deps*
T0*F
_class<
:8loc:@train/gradients/conv_layer2_conv/Relu_grad/ReluGrad*/
_output_shapes
:€€€€€€€€€//
√
Htrain/gradients/conv_layer2_conv/BiasAdd_grad/tuple/control_dependency_1Identity9train/gradients/conv_layer2_conv/BiasAdd_grad/BiasAddGrad?^train/gradients/conv_layer2_conv/BiasAdd_grad/tuple/group_deps*
T0*L
_classB
@>loc:@train/gradients/conv_layer2_conv/BiasAdd_grad/BiasAddGrad*
_output_shapes
:
Д
Atrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
£
Ctrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Shape_1Shape vect_expand_dense_layer1/BiasAdd*
T0*
out_type0*
_output_shapes
:
ѓ
Qtrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/BroadcastGradientArgsBroadcastGradientArgsAtrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/ShapeCtrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
м
?train/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/MulMulPtrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/tuple/control_dependency vect_expand_dense_layer1/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€d
Ъ
?train/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/SumSum?train/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/MulQtrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
Б
Ctrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/ReshapeReshape?train/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/SumAtrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Shape*
_output_shapes
: *
T0*
Tshape0
ц
Atrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Mul_1Mul(vect_expand_dense_layer1/LeakyRelu/alphaPtrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€d
†
Atrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Sum_1SumAtrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Mul_1Strain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ш
Etrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Reshape_1ReshapeAtrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Sum_1Ctrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€d
в
Ltrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/tuple/group_depsNoOpD^train/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/ReshapeF^train/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Reshape_1
н
Ttrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/tuple/control_dependencyIdentityCtrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/ReshapeM^train/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/tuple/group_deps*
_output_shapes
: *
T0*V
_classL
JHloc:@train/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Reshape
Д
Vtrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/tuple/control_dependency_1IdentityEtrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Reshape_1M^train/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@train/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€d
є
3train/gradients/conv_layer2_conv/Conv2D_grad/ShapeNShapeNconv_layer1_pool/MaxPoolconv_layer2_conv/kernel/read*
T0*
out_type0*
N* 
_output_shapes
::
Ы
@train/gradients/conv_layer2_conv/Conv2D_grad/Conv2DBackpropInputConv2DBackpropInput3train/gradients/conv_layer2_conv/Conv2D_grad/ShapeNconv_layer2_conv/kernel/readFtrain/gradients/conv_layer2_conv/BiasAdd_grad/tuple/control_dependency*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingVALID*/
_output_shapes
:€€€€€€€€€33 *
	dilations

Т
Atrain/gradients/conv_layer2_conv/Conv2D_grad/Conv2DBackpropFilterConv2DBackpropFilterconv_layer1_pool/MaxPool5train/gradients/conv_layer2_conv/Conv2D_grad/ShapeN:1Ftrain/gradients/conv_layer2_conv/BiasAdd_grad/tuple/control_dependency*&
_output_shapes
: *
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingVALID
ћ
=train/gradients/conv_layer2_conv/Conv2D_grad/tuple/group_depsNoOpB^train/gradients/conv_layer2_conv/Conv2D_grad/Conv2DBackpropFilterA^train/gradients/conv_layer2_conv/Conv2D_grad/Conv2DBackpropInput
в
Etrain/gradients/conv_layer2_conv/Conv2D_grad/tuple/control_dependencyIdentity@train/gradients/conv_layer2_conv/Conv2D_grad/Conv2DBackpropInput>^train/gradients/conv_layer2_conv/Conv2D_grad/tuple/group_deps*/
_output_shapes
:€€€€€€€€€33 *
T0*S
_classI
GEloc:@train/gradients/conv_layer2_conv/Conv2D_grad/Conv2DBackpropInput
Ё
Gtrain/gradients/conv_layer2_conv/Conv2D_grad/tuple/control_dependency_1IdentityAtrain/gradients/conv_layer2_conv/Conv2D_grad/Conv2DBackpropFilter>^train/gradients/conv_layer2_conv/Conv2D_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@train/gradients/conv_layer2_conv/Conv2D_grad/Conv2DBackpropFilter*&
_output_shapes
: 
џ
train/gradients/AddN_7AddNRtrain/gradients/vect_expand_dense_layer1/LeakyRelu_grad/tuple/control_dependency_1Vtrain/gradients/vect_expand_dense_layer1/LeakyRelu/mul_grad/tuple/control_dependency_1*
T0*T
_classJ
HFloc:@train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Reshape_1*
N*'
_output_shapes
:€€€€€€€€€d
§
Atrain/gradients/vect_expand_dense_layer1/BiasAdd_grad/BiasAddGradBiasAddGradtrain/gradients/AddN_7*
T0*
data_formatNHWC*
_output_shapes
:d
Ђ
Ftrain/gradients/vect_expand_dense_layer1/BiasAdd_grad/tuple/group_depsNoOp^train/gradients/AddN_7B^train/gradients/vect_expand_dense_layer1/BiasAdd_grad/BiasAddGrad
√
Ntrain/gradients/vect_expand_dense_layer1/BiasAdd_grad/tuple/control_dependencyIdentitytrain/gradients/AddN_7G^train/gradients/vect_expand_dense_layer1/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@train/gradients/vect_expand_dense_layer1/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€d
г
Ptrain/gradients/vect_expand_dense_layer1/BiasAdd_grad/tuple/control_dependency_1IdentityAtrain/gradients/vect_expand_dense_layer1/BiasAdd_grad/BiasAddGradG^train/gradients/vect_expand_dense_layer1/BiasAdd_grad/tuple/group_deps*
_output_shapes
:d*
T0*T
_classJ
HFloc:@train/gradients/vect_expand_dense_layer1/BiasAdd_grad/BiasAddGrad
„
9train/gradients/conv_layer1_pool/MaxPool_grad/MaxPoolGradMaxPoolGradconv_layer1_dropout/dropout/mulconv_layer1_pool/MaxPoolEtrain/gradients/conv_layer2_conv/Conv2D_grad/tuple/control_dependency*
ksize
*
paddingVALID*/
_output_shapes
:€€€€€€€€€gg *
T0*
data_formatNHWC*
strides

У
;train/gradients/vect_expand_dense_layer1/MatMul_grad/MatMulMatMulNtrain/gradients/vect_expand_dense_layer1/BiasAdd_grad/tuple/control_dependency$vect_expand_dense_layer1/kernel/read*
transpose_a( *'
_output_shapes
:€€€€€€€€€*
transpose_b(*
T0
о
=train/gradients/vect_expand_dense_layer1/MatMul_grad/MatMul_1MatMulconcatNtrain/gradients/vect_expand_dense_layer1/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(*
_output_shapes

:d
Ћ
Etrain/gradients/vect_expand_dense_layer1/MatMul_grad/tuple/group_depsNoOp<^train/gradients/vect_expand_dense_layer1/MatMul_grad/MatMul>^train/gradients/vect_expand_dense_layer1/MatMul_grad/MatMul_1
а
Mtrain/gradients/vect_expand_dense_layer1/MatMul_grad/tuple/control_dependencyIdentity;train/gradients/vect_expand_dense_layer1/MatMul_grad/MatMulF^train/gradients/vect_expand_dense_layer1/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@train/gradients/vect_expand_dense_layer1/MatMul_grad/MatMul*'
_output_shapes
:€€€€€€€€€
Ё
Otrain/gradients/vect_expand_dense_layer1/MatMul_grad/tuple/control_dependency_1Identity=train/gradients/vect_expand_dense_layer1/MatMul_grad/MatMul_1F^train/gradients/vect_expand_dense_layer1/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@train/gradients/vect_expand_dense_layer1/MatMul_grad/MatMul_1*
_output_shapes

:d
Щ
:train/gradients/conv_layer1_dropout/dropout/mul_grad/ShapeShapeconv_layer1_dropout/dropout/div*
_output_shapes
:*
T0*
out_type0
Э
<train/gradients/conv_layer1_dropout/dropout/mul_grad/Shape_1Shape!conv_layer1_dropout/dropout/Floor*
T0*
out_type0*
_output_shapes
:
Ъ
Jtrain/gradients/conv_layer1_dropout/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/conv_layer1_dropout/dropout/mul_grad/Shape<train/gradients/conv_layer1_dropout/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
„
8train/gradients/conv_layer1_dropout/dropout/mul_grad/MulMul9train/gradients/conv_layer1_pool/MaxPool_grad/MaxPoolGrad!conv_layer1_dropout/dropout/Floor*
T0*/
_output_shapes
:€€€€€€€€€gg 
Е
8train/gradients/conv_layer1_dropout/dropout/mul_grad/SumSum8train/gradients/conv_layer1_dropout/dropout/mul_grad/MulJtrain/gradients/conv_layer1_dropout/dropout/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Е
<train/gradients/conv_layer1_dropout/dropout/mul_grad/ReshapeReshape8train/gradients/conv_layer1_dropout/dropout/mul_grad/Sum:train/gradients/conv_layer1_dropout/dropout/mul_grad/Shape*/
_output_shapes
:€€€€€€€€€gg *
T0*
Tshape0
„
:train/gradients/conv_layer1_dropout/dropout/mul_grad/Mul_1Mulconv_layer1_dropout/dropout/div9train/gradients/conv_layer1_pool/MaxPool_grad/MaxPoolGrad*
T0*/
_output_shapes
:€€€€€€€€€gg 
Л
:train/gradients/conv_layer1_dropout/dropout/mul_grad/Sum_1Sum:train/gradients/conv_layer1_dropout/dropout/mul_grad/Mul_1Ltrain/gradients/conv_layer1_dropout/dropout/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Л
>train/gradients/conv_layer1_dropout/dropout/mul_grad/Reshape_1Reshape:train/gradients/conv_layer1_dropout/dropout/mul_grad/Sum_1<train/gradients/conv_layer1_dropout/dropout/mul_grad/Shape_1*/
_output_shapes
:€€€€€€€€€gg *
T0*
Tshape0
Ќ
Etrain/gradients/conv_layer1_dropout/dropout/mul_grad/tuple/group_depsNoOp=^train/gradients/conv_layer1_dropout/dropout/mul_grad/Reshape?^train/gradients/conv_layer1_dropout/dropout/mul_grad/Reshape_1
к
Mtrain/gradients/conv_layer1_dropout/dropout/mul_grad/tuple/control_dependencyIdentity<train/gradients/conv_layer1_dropout/dropout/mul_grad/ReshapeF^train/gradients/conv_layer1_dropout/dropout/mul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@train/gradients/conv_layer1_dropout/dropout/mul_grad/Reshape*/
_output_shapes
:€€€€€€€€€gg 
р
Otrain/gradients/conv_layer1_dropout/dropout/mul_grad/tuple/control_dependency_1Identity>train/gradients/conv_layer1_dropout/dropout/mul_grad/Reshape_1F^train/gradients/conv_layer1_dropout/dropout/mul_grad/tuple/group_deps*/
_output_shapes
:€€€€€€€€€gg *
T0*Q
_classG
ECloc:@train/gradients/conv_layer1_dropout/dropout/mul_grad/Reshape_1
b
 train/gradients/concat_grad/RankConst*
value	B :*
dtype0*
_output_shapes
: 
{
train/gradients/concat_grad/modFloorModconcat/axis train/gradients/concat_grad/Rank*
T0*
_output_shapes
: 
Б
!train/gradients/concat_grad/ShapeShape head_step_fc_dropout/dropout/mul*
T0*
out_type0*
_output_shapes
:
±
"train/gradients/concat_grad/ShapeNShapeN head_step_fc_dropout/dropout/mul
twist_inptwrench_inpt*
T0*
out_type0*
N*&
_output_shapes
:::
ъ
(train/gradients/concat_grad/ConcatOffsetConcatOffsettrain/gradients/concat_grad/mod"train/gradients/concat_grad/ShapeN$train/gradients/concat_grad/ShapeN:1$train/gradients/concat_grad/ShapeN:2*
N*&
_output_shapes
:::
Ж
!train/gradients/concat_grad/SliceSliceMtrain/gradients/vect_expand_dense_layer1/MatMul_grad/tuple/control_dependency(train/gradients/concat_grad/ConcatOffset"train/gradients/concat_grad/ShapeN*'
_output_shapes
:€€€€€€€€€*
T0*
Index0
М
#train/gradients/concat_grad/Slice_1SliceMtrain/gradients/vect_expand_dense_layer1/MatMul_grad/tuple/control_dependency*train/gradients/concat_grad/ConcatOffset:1$train/gradients/concat_grad/ShapeN:1*
T0*
Index0*'
_output_shapes
:€€€€€€€€€
М
#train/gradients/concat_grad/Slice_2SliceMtrain/gradients/vect_expand_dense_layer1/MatMul_grad/tuple/control_dependency*train/gradients/concat_grad/ConcatOffset:2$train/gradients/concat_grad/ShapeN:2*
T0*
Index0*'
_output_shapes
:€€€€€€€€€
§
,train/gradients/concat_grad/tuple/group_depsNoOp"^train/gradients/concat_grad/Slice$^train/gradients/concat_grad/Slice_1$^train/gradients/concat_grad/Slice_2
ъ
4train/gradients/concat_grad/tuple/control_dependencyIdentity!train/gradients/concat_grad/Slice-^train/gradients/concat_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€*
T0*4
_class*
(&loc:@train/gradients/concat_grad/Slice
А
6train/gradients/concat_grad/tuple/control_dependency_1Identity#train/gradients/concat_grad/Slice_1-^train/gradients/concat_grad/tuple/group_deps*
T0*6
_class,
*(loc:@train/gradients/concat_grad/Slice_1*'
_output_shapes
:€€€€€€€€€
А
6train/gradients/concat_grad/tuple/control_dependency_2Identity#train/gradients/concat_grad/Slice_2-^train/gradients/concat_grad/tuple/group_deps*
T0*6
_class,
*(loc:@train/gradients/concat_grad/Slice_2*'
_output_shapes
:€€€€€€€€€
П
:train/gradients/conv_layer1_dropout/dropout/div_grad/ShapeShapeconv_layer1_conv/Relu*
T0*
out_type0*
_output_shapes
:

<train/gradients/conv_layer1_dropout/dropout/div_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
Ъ
Jtrain/gradients/conv_layer1_dropout/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs:train/gradients/conv_layer1_dropout/dropout/div_grad/Shape<train/gradients/conv_layer1_dropout/dropout/div_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
ч
<train/gradients/conv_layer1_dropout/dropout/div_grad/RealDivRealDivMtrain/gradients/conv_layer1_dropout/dropout/mul_grad/tuple/control_dependency%conv_layer1_dropout/dropout/keep_prob*
T0*/
_output_shapes
:€€€€€€€€€gg 
Й
8train/gradients/conv_layer1_dropout/dropout/div_grad/SumSum<train/gradients/conv_layer1_dropout/dropout/div_grad/RealDivJtrain/gradients/conv_layer1_dropout/dropout/div_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
Е
<train/gradients/conv_layer1_dropout/dropout/div_grad/ReshapeReshape8train/gradients/conv_layer1_dropout/dropout/div_grad/Sum:train/gradients/conv_layer1_dropout/dropout/div_grad/Shape*
T0*
Tshape0*/
_output_shapes
:€€€€€€€€€gg 
Р
8train/gradients/conv_layer1_dropout/dropout/div_grad/NegNegconv_layer1_conv/Relu*
T0*/
_output_shapes
:€€€€€€€€€gg 
д
>train/gradients/conv_layer1_dropout/dropout/div_grad/RealDiv_1RealDiv8train/gradients/conv_layer1_dropout/dropout/div_grad/Neg%conv_layer1_dropout/dropout/keep_prob*
T0*/
_output_shapes
:€€€€€€€€€gg 
к
>train/gradients/conv_layer1_dropout/dropout/div_grad/RealDiv_2RealDiv>train/gradients/conv_layer1_dropout/dropout/div_grad/RealDiv_1%conv_layer1_dropout/dropout/keep_prob*
T0*/
_output_shapes
:€€€€€€€€€gg 
И
8train/gradients/conv_layer1_dropout/dropout/div_grad/mulMulMtrain/gradients/conv_layer1_dropout/dropout/mul_grad/tuple/control_dependency>train/gradients/conv_layer1_dropout/dropout/div_grad/RealDiv_2*
T0*/
_output_shapes
:€€€€€€€€€gg 
Й
:train/gradients/conv_layer1_dropout/dropout/div_grad/Sum_1Sum8train/gradients/conv_layer1_dropout/dropout/div_grad/mulLtrain/gradients/conv_layer1_dropout/dropout/div_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
т
>train/gradients/conv_layer1_dropout/dropout/div_grad/Reshape_1Reshape:train/gradients/conv_layer1_dropout/dropout/div_grad/Sum_1<train/gradients/conv_layer1_dropout/dropout/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 
Ќ
Etrain/gradients/conv_layer1_dropout/dropout/div_grad/tuple/group_depsNoOp=^train/gradients/conv_layer1_dropout/dropout/div_grad/Reshape?^train/gradients/conv_layer1_dropout/dropout/div_grad/Reshape_1
к
Mtrain/gradients/conv_layer1_dropout/dropout/div_grad/tuple/control_dependencyIdentity<train/gradients/conv_layer1_dropout/dropout/div_grad/ReshapeF^train/gradients/conv_layer1_dropout/dropout/div_grad/tuple/group_deps*
T0*O
_classE
CAloc:@train/gradients/conv_layer1_dropout/dropout/div_grad/Reshape*/
_output_shapes
:€€€€€€€€€gg 
„
Otrain/gradients/conv_layer1_dropout/dropout/div_grad/tuple/control_dependency_1Identity>train/gradients/conv_layer1_dropout/dropout/div_grad/Reshape_1F^train/gradients/conv_layer1_dropout/dropout/div_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@train/gradients/conv_layer1_dropout/dropout/div_grad/Reshape_1*
_output_shapes
: 
Ы
;train/gradients/head_step_fc_dropout/dropout/mul_grad/ShapeShape head_step_fc_dropout/dropout/div*
T0*
out_type0*
_output_shapes
:
Я
=train/gradients/head_step_fc_dropout/dropout/mul_grad/Shape_1Shape"head_step_fc_dropout/dropout/Floor*
T0*
out_type0*
_output_shapes
:
Э
Ktrain/gradients/head_step_fc_dropout/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs;train/gradients/head_step_fc_dropout/dropout/mul_grad/Shape=train/gradients/head_step_fc_dropout/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
ћ
9train/gradients/head_step_fc_dropout/dropout/mul_grad/MulMul4train/gradients/concat_grad/tuple/control_dependency"head_step_fc_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€
И
9train/gradients/head_step_fc_dropout/dropout/mul_grad/SumSum9train/gradients/head_step_fc_dropout/dropout/mul_grad/MulKtrain/gradients/head_step_fc_dropout/dropout/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
А
=train/gradients/head_step_fc_dropout/dropout/mul_grad/ReshapeReshape9train/gradients/head_step_fc_dropout/dropout/mul_grad/Sum;train/gradients/head_step_fc_dropout/dropout/mul_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
ћ
;train/gradients/head_step_fc_dropout/dropout/mul_grad/Mul_1Mul head_step_fc_dropout/dropout/div4train/gradients/concat_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€
О
;train/gradients/head_step_fc_dropout/dropout/mul_grad/Sum_1Sum;train/gradients/head_step_fc_dropout/dropout/mul_grad/Mul_1Mtrain/gradients/head_step_fc_dropout/dropout/mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
Ж
?train/gradients/head_step_fc_dropout/dropout/mul_grad/Reshape_1Reshape;train/gradients/head_step_fc_dropout/dropout/mul_grad/Sum_1=train/gradients/head_step_fc_dropout/dropout/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
–
Ftrain/gradients/head_step_fc_dropout/dropout/mul_grad/tuple/group_depsNoOp>^train/gradients/head_step_fc_dropout/dropout/mul_grad/Reshape@^train/gradients/head_step_fc_dropout/dropout/mul_grad/Reshape_1
ж
Ntrain/gradients/head_step_fc_dropout/dropout/mul_grad/tuple/control_dependencyIdentity=train/gradients/head_step_fc_dropout/dropout/mul_grad/ReshapeG^train/gradients/head_step_fc_dropout/dropout/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€*
T0*P
_classF
DBloc:@train/gradients/head_step_fc_dropout/dropout/mul_grad/Reshape
м
Ptrain/gradients/head_step_fc_dropout/dropout/mul_grad/tuple/control_dependency_1Identity?train/gradients/head_step_fc_dropout/dropout/mul_grad/Reshape_1G^train/gradients/head_step_fc_dropout/dropout/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€*
T0*R
_classH
FDloc:@train/gradients/head_step_fc_dropout/dropout/mul_grad/Reshape_1
я
3train/gradients/conv_layer1_conv/Relu_grad/ReluGradReluGradMtrain/gradients/conv_layer1_dropout/dropout/div_grad/tuple/control_dependencyconv_layer1_conv/Relu*/
_output_shapes
:€€€€€€€€€gg *
T0
С
;train/gradients/head_step_fc_dropout/dropout/div_grad/ShapeShapehead_step_fc/LeakyRelu*
T0*
out_type0*
_output_shapes
:
А
=train/gradients/head_step_fc_dropout/dropout/div_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
Э
Ktrain/gradients/head_step_fc_dropout/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs;train/gradients/head_step_fc_dropout/dropout/div_grad/Shape=train/gradients/head_step_fc_dropout/dropout/div_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
т
=train/gradients/head_step_fc_dropout/dropout/div_grad/RealDivRealDivNtrain/gradients/head_step_fc_dropout/dropout/mul_grad/tuple/control_dependency&head_step_fc_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€
М
9train/gradients/head_step_fc_dropout/dropout/div_grad/SumSum=train/gradients/head_step_fc_dropout/dropout/div_grad/RealDivKtrain/gradients/head_step_fc_dropout/dropout/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
А
=train/gradients/head_step_fc_dropout/dropout/div_grad/ReshapeReshape9train/gradients/head_step_fc_dropout/dropout/div_grad/Sum;train/gradients/head_step_fc_dropout/dropout/div_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
К
9train/gradients/head_step_fc_dropout/dropout/div_grad/NegNeghead_step_fc/LeakyRelu*'
_output_shapes
:€€€€€€€€€*
T0
я
?train/gradients/head_step_fc_dropout/dropout/div_grad/RealDiv_1RealDiv9train/gradients/head_step_fc_dropout/dropout/div_grad/Neg&head_step_fc_dropout/dropout/keep_prob*'
_output_shapes
:€€€€€€€€€*
T0
е
?train/gradients/head_step_fc_dropout/dropout/div_grad/RealDiv_2RealDiv?train/gradients/head_step_fc_dropout/dropout/div_grad/RealDiv_1&head_step_fc_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€
Г
9train/gradients/head_step_fc_dropout/dropout/div_grad/mulMulNtrain/gradients/head_step_fc_dropout/dropout/mul_grad/tuple/control_dependency?train/gradients/head_step_fc_dropout/dropout/div_grad/RealDiv_2*
T0*'
_output_shapes
:€€€€€€€€€
М
;train/gradients/head_step_fc_dropout/dropout/div_grad/Sum_1Sum9train/gradients/head_step_fc_dropout/dropout/div_grad/mulMtrain/gradients/head_step_fc_dropout/dropout/div_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
х
?train/gradients/head_step_fc_dropout/dropout/div_grad/Reshape_1Reshape;train/gradients/head_step_fc_dropout/dropout/div_grad/Sum_1=train/gradients/head_step_fc_dropout/dropout/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 
–
Ftrain/gradients/head_step_fc_dropout/dropout/div_grad/tuple/group_depsNoOp>^train/gradients/head_step_fc_dropout/dropout/div_grad/Reshape@^train/gradients/head_step_fc_dropout/dropout/div_grad/Reshape_1
ж
Ntrain/gradients/head_step_fc_dropout/dropout/div_grad/tuple/control_dependencyIdentity=train/gradients/head_step_fc_dropout/dropout/div_grad/ReshapeG^train/gradients/head_step_fc_dropout/dropout/div_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€*
T0*P
_classF
DBloc:@train/gradients/head_step_fc_dropout/dropout/div_grad/Reshape
џ
Ptrain/gradients/head_step_fc_dropout/dropout/div_grad/tuple/control_dependency_1Identity?train/gradients/head_step_fc_dropout/dropout/div_grad/Reshape_1G^train/gradients/head_step_fc_dropout/dropout/div_grad/tuple/group_deps*
T0*R
_classH
FDloc:@train/gradients/head_step_fc_dropout/dropout/div_grad/Reshape_1*
_output_shapes
: 
є
9train/gradients/conv_layer1_conv/BiasAdd_grad/BiasAddGradBiasAddGrad3train/gradients/conv_layer1_conv/Relu_grad/ReluGrad*
T0*
data_formatNHWC*
_output_shapes
: 
Є
>train/gradients/conv_layer1_conv/BiasAdd_grad/tuple/group_depsNoOp:^train/gradients/conv_layer1_conv/BiasAdd_grad/BiasAddGrad4^train/gradients/conv_layer1_conv/Relu_grad/ReluGrad
 
Ftrain/gradients/conv_layer1_conv/BiasAdd_grad/tuple/control_dependencyIdentity3train/gradients/conv_layer1_conv/Relu_grad/ReluGrad?^train/gradients/conv_layer1_conv/BiasAdd_grad/tuple/group_deps*/
_output_shapes
:€€€€€€€€€gg *
T0*F
_class<
:8loc:@train/gradients/conv_layer1_conv/Relu_grad/ReluGrad
√
Htrain/gradients/conv_layer1_conv/BiasAdd_grad/tuple/control_dependency_1Identity9train/gradients/conv_layer1_conv/BiasAdd_grad/BiasAddGrad?^train/gradients/conv_layer1_conv/BiasAdd_grad/tuple/group_deps*
_output_shapes
: *
T0*L
_classB
@>loc:@train/gradients/conv_layer1_conv/BiasAdd_grad/BiasAddGrad
Л
1train/gradients/head_step_fc/LeakyRelu_grad/ShapeShapehead_step_fc/LeakyRelu/mul*
T0*
out_type0*
_output_shapes
:
З
3train/gradients/head_step_fc/LeakyRelu_grad/Shape_1Shapehead_step_fc/BiasAdd*
_output_shapes
:*
T0*
out_type0
Ѕ
3train/gradients/head_step_fc/LeakyRelu_grad/Shape_2ShapeNtrain/gradients/head_step_fc_dropout/dropout/div_grad/tuple/control_dependency*
T0*
out_type0*
_output_shapes
:
А
7train/gradients/head_step_fc/LeakyRelu_grad/zeros/ConstConst*
valueB 2        *
dtype0*
_output_shapes
: 
л
1train/gradients/head_step_fc/LeakyRelu_grad/zerosFill3train/gradients/head_step_fc/LeakyRelu_grad/Shape_27train/gradients/head_step_fc/LeakyRelu_grad/zeros/Const*'
_output_shapes
:€€€€€€€€€*
T0*

index_type0
ђ
8train/gradients/head_step_fc/LeakyRelu_grad/GreaterEqualGreaterEqualhead_step_fc/LeakyRelu/mulhead_step_fc/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€
€
Atrain/gradients/head_step_fc/LeakyRelu_grad/BroadcastGradientArgsBroadcastGradientArgs1train/gradients/head_step_fc/LeakyRelu_grad/Shape3train/gradients/head_step_fc/LeakyRelu_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
Ђ
2train/gradients/head_step_fc/LeakyRelu_grad/SelectSelect8train/gradients/head_step_fc/LeakyRelu_grad/GreaterEqualNtrain/gradients/head_step_fc_dropout/dropout/div_grad/tuple/control_dependency1train/gradients/head_step_fc/LeakyRelu_grad/zeros*
T0*'
_output_shapes
:€€€€€€€€€
≠
4train/gradients/head_step_fc/LeakyRelu_grad/Select_1Select8train/gradients/head_step_fc/LeakyRelu_grad/GreaterEqual1train/gradients/head_step_fc/LeakyRelu_grad/zerosNtrain/gradients/head_step_fc_dropout/dropout/div_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€
н
/train/gradients/head_step_fc/LeakyRelu_grad/SumSum2train/gradients/head_step_fc/LeakyRelu_grad/SelectAtrain/gradients/head_step_fc/LeakyRelu_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
в
3train/gradients/head_step_fc/LeakyRelu_grad/ReshapeReshape/train/gradients/head_step_fc/LeakyRelu_grad/Sum1train/gradients/head_step_fc/LeakyRelu_grad/Shape*'
_output_shapes
:€€€€€€€€€*
T0*
Tshape0
у
1train/gradients/head_step_fc/LeakyRelu_grad/Sum_1Sum4train/gradients/head_step_fc/LeakyRelu_grad/Select_1Ctrain/gradients/head_step_fc/LeakyRelu_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
и
5train/gradients/head_step_fc/LeakyRelu_grad/Reshape_1Reshape1train/gradients/head_step_fc/LeakyRelu_grad/Sum_13train/gradients/head_step_fc/LeakyRelu_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
≤
<train/gradients/head_step_fc/LeakyRelu_grad/tuple/group_depsNoOp4^train/gradients/head_step_fc/LeakyRelu_grad/Reshape6^train/gradients/head_step_fc/LeakyRelu_grad/Reshape_1
Њ
Dtrain/gradients/head_step_fc/LeakyRelu_grad/tuple/control_dependencyIdentity3train/gradients/head_step_fc/LeakyRelu_grad/Reshape=^train/gradients/head_step_fc/LeakyRelu_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€*
T0*F
_class<
:8loc:@train/gradients/head_step_fc/LeakyRelu_grad/Reshape
ƒ
Ftrain/gradients/head_step_fc/LeakyRelu_grad/tuple/control_dependency_1Identity5train/gradients/head_step_fc/LeakyRelu_grad/Reshape_1=^train/gradients/head_step_fc/LeakyRelu_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€*
T0*H
_class>
<:loc:@train/gradients/head_step_fc/LeakyRelu_grad/Reshape_1
©
3train/gradients/conv_layer1_conv/Conv2D_grad/ShapeNShapeNimg_inptconv_layer1_conv/kernel/read*
T0*
out_type0*
N* 
_output_shapes
::
Ы
@train/gradients/conv_layer1_conv/Conv2D_grad/Conv2DBackpropInputConv2DBackpropInput3train/gradients/conv_layer1_conv/Conv2D_grad/ShapeNconv_layer1_conv/kernel/readFtrain/gradients/conv_layer1_conv/BiasAdd_grad/tuple/control_dependency*/
_output_shapes
:€€€€€€€€€kk*
	dilations
*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingVALID
В
Atrain/gradients/conv_layer1_conv/Conv2D_grad/Conv2DBackpropFilterConv2DBackpropFilterimg_inpt5train/gradients/conv_layer1_conv/Conv2D_grad/ShapeN:1Ftrain/gradients/conv_layer1_conv/BiasAdd_grad/tuple/control_dependency*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingVALID*&
_output_shapes
: 
ћ
=train/gradients/conv_layer1_conv/Conv2D_grad/tuple/group_depsNoOpB^train/gradients/conv_layer1_conv/Conv2D_grad/Conv2DBackpropFilterA^train/gradients/conv_layer1_conv/Conv2D_grad/Conv2DBackpropInput
в
Etrain/gradients/conv_layer1_conv/Conv2D_grad/tuple/control_dependencyIdentity@train/gradients/conv_layer1_conv/Conv2D_grad/Conv2DBackpropInput>^train/gradients/conv_layer1_conv/Conv2D_grad/tuple/group_deps*
T0*S
_classI
GEloc:@train/gradients/conv_layer1_conv/Conv2D_grad/Conv2DBackpropInput*/
_output_shapes
:€€€€€€€€€kk
Ё
Gtrain/gradients/conv_layer1_conv/Conv2D_grad/tuple/control_dependency_1IdentityAtrain/gradients/conv_layer1_conv/Conv2D_grad/Conv2DBackpropFilter>^train/gradients/conv_layer1_conv/Conv2D_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@train/gradients/conv_layer1_conv/Conv2D_grad/Conv2DBackpropFilter*&
_output_shapes
: 
x
5train/gradients/head_step_fc/LeakyRelu/mul_grad/ShapeConst*
dtype0*
_output_shapes
: *
valueB 
Л
7train/gradients/head_step_fc/LeakyRelu/mul_grad/Shape_1Shapehead_step_fc/BiasAdd*
_output_shapes
:*
T0*
out_type0
Л
Etrain/gradients/head_step_fc/LeakyRelu/mul_grad/BroadcastGradientArgsBroadcastGradientArgs5train/gradients/head_step_fc/LeakyRelu/mul_grad/Shape7train/gradients/head_step_fc/LeakyRelu/mul_grad/Shape_1*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€*
T0
»
3train/gradients/head_step_fc/LeakyRelu/mul_grad/MulMulDtrain/gradients/head_step_fc/LeakyRelu_grad/tuple/control_dependencyhead_step_fc/BiasAdd*'
_output_shapes
:€€€€€€€€€*
T0
ц
3train/gradients/head_step_fc/LeakyRelu/mul_grad/SumSum3train/gradients/head_step_fc/LeakyRelu/mul_grad/MulEtrain/gradients/head_step_fc/LeakyRelu/mul_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
Ё
7train/gradients/head_step_fc/LeakyRelu/mul_grad/ReshapeReshape3train/gradients/head_step_fc/LeakyRelu/mul_grad/Sum5train/gradients/head_step_fc/LeakyRelu/mul_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
“
5train/gradients/head_step_fc/LeakyRelu/mul_grad/Mul_1Mulhead_step_fc/LeakyRelu/alphaDtrain/gradients/head_step_fc/LeakyRelu_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€
ь
5train/gradients/head_step_fc/LeakyRelu/mul_grad/Sum_1Sum5train/gradients/head_step_fc/LeakyRelu/mul_grad/Mul_1Gtrain/gradients/head_step_fc/LeakyRelu/mul_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
ф
9train/gradients/head_step_fc/LeakyRelu/mul_grad/Reshape_1Reshape5train/gradients/head_step_fc/LeakyRelu/mul_grad/Sum_17train/gradients/head_step_fc/LeakyRelu/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
Њ
@train/gradients/head_step_fc/LeakyRelu/mul_grad/tuple/group_depsNoOp8^train/gradients/head_step_fc/LeakyRelu/mul_grad/Reshape:^train/gradients/head_step_fc/LeakyRelu/mul_grad/Reshape_1
љ
Htrain/gradients/head_step_fc/LeakyRelu/mul_grad/tuple/control_dependencyIdentity7train/gradients/head_step_fc/LeakyRelu/mul_grad/ReshapeA^train/gradients/head_step_fc/LeakyRelu/mul_grad/tuple/group_deps*
T0*J
_class@
><loc:@train/gradients/head_step_fc/LeakyRelu/mul_grad/Reshape*
_output_shapes
: 
‘
Jtrain/gradients/head_step_fc/LeakyRelu/mul_grad/tuple/control_dependency_1Identity9train/gradients/head_step_fc/LeakyRelu/mul_grad/Reshape_1A^train/gradients/head_step_fc/LeakyRelu/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€*
T0*L
_classB
@>loc:@train/gradients/head_step_fc/LeakyRelu/mul_grad/Reshape_1
Ј
train/gradients/AddN_8AddNFtrain/gradients/head_step_fc/LeakyRelu_grad/tuple/control_dependency_1Jtrain/gradients/head_step_fc/LeakyRelu/mul_grad/tuple/control_dependency_1*
T0*H
_class>
<:loc:@train/gradients/head_step_fc/LeakyRelu_grad/Reshape_1*
N*'
_output_shapes
:€€€€€€€€€
Ш
5train/gradients/head_step_fc/BiasAdd_grad/BiasAddGradBiasAddGradtrain/gradients/AddN_8*
T0*
data_formatNHWC*
_output_shapes
:
У
:train/gradients/head_step_fc/BiasAdd_grad/tuple/group_depsNoOp^train/gradients/AddN_86^train/gradients/head_step_fc/BiasAdd_grad/BiasAddGrad
Я
Btrain/gradients/head_step_fc/BiasAdd_grad/tuple/control_dependencyIdentitytrain/gradients/AddN_8;^train/gradients/head_step_fc/BiasAdd_grad/tuple/group_deps*
T0*H
_class>
<:loc:@train/gradients/head_step_fc/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€
≥
Dtrain/gradients/head_step_fc/BiasAdd_grad/tuple/control_dependency_1Identity5train/gradients/head_step_fc/BiasAdd_grad/BiasAddGrad;^train/gradients/head_step_fc/BiasAdd_grad/tuple/group_deps*
T0*H
_class>
<:loc:@train/gradients/head_step_fc/BiasAdd_grad/BiasAddGrad*
_output_shapes
:
п
/train/gradients/head_step_fc/MatMul_grad/MatMulMatMulBtrain/gradients/head_step_fc/BiasAdd_grad/tuple/control_dependencyhead_step_fc/kernel/read*
transpose_a( *'
_output_shapes
:€€€€€€€€€*
transpose_b(*
T0
ш
1train/gradients/head_step_fc/MatMul_grad/MatMul_1MatMul(head_step_fc_setup_1_dropout/dropout/mulBtrain/gradients/head_step_fc/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
_output_shapes

:*
transpose_b( 
І
9train/gradients/head_step_fc/MatMul_grad/tuple/group_depsNoOp0^train/gradients/head_step_fc/MatMul_grad/MatMul2^train/gradients/head_step_fc/MatMul_grad/MatMul_1
∞
Atrain/gradients/head_step_fc/MatMul_grad/tuple/control_dependencyIdentity/train/gradients/head_step_fc/MatMul_grad/MatMul:^train/gradients/head_step_fc/MatMul_grad/tuple/group_deps*
T0*B
_class8
64loc:@train/gradients/head_step_fc/MatMul_grad/MatMul*'
_output_shapes
:€€€€€€€€€
≠
Ctrain/gradients/head_step_fc/MatMul_grad/tuple/control_dependency_1Identity1train/gradients/head_step_fc/MatMul_grad/MatMul_1:^train/gradients/head_step_fc/MatMul_grad/tuple/group_deps*
_output_shapes

:*
T0*D
_class:
86loc:@train/gradients/head_step_fc/MatMul_grad/MatMul_1
Ђ
Ctrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/ShapeShape(head_step_fc_setup_1_dropout/dropout/div*
T0*
out_type0*
_output_shapes
:
ѓ
Etrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Shape_1Shape*head_step_fc_setup_1_dropout/dropout/Floor*
T0*
out_type0*
_output_shapes
:
µ
Strain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgsCtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/ShapeEtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
й
Atrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/MulMulAtrain/gradients/head_step_fc/MatMul_grad/tuple/control_dependency*head_step_fc_setup_1_dropout/dropout/Floor*
T0*'
_output_shapes
:€€€€€€€€€
†
Atrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/SumSumAtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/MulStrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
Ш
Etrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/ReshapeReshapeAtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/SumCtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
й
Ctrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Mul_1Mul(head_step_fc_setup_1_dropout/dropout/divAtrain/gradients/head_step_fc/MatMul_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€
¶
Ctrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Sum_1SumCtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Mul_1Utrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ю
Gtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Reshape_1ReshapeCtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Sum_1Etrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
и
Ntrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/tuple/group_depsNoOpF^train/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/ReshapeH^train/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Reshape_1
Ж
Vtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/tuple/control_dependencyIdentityEtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/ReshapeO^train/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@train/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Reshape*'
_output_shapes
:€€€€€€€€€
М
Xtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/tuple/control_dependency_1IdentityGtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Reshape_1O^train/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€*
T0*Z
_classP
NLloc:@train/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/Reshape_1
°
Ctrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/ShapeShapehead_step_fc_setup_1/LeakyRelu*
_output_shapes
:*
T0*
out_type0
И
Etrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
µ
Strain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgsCtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/ShapeEtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
К
Etrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/RealDivRealDivVtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/tuple/control_dependency.head_step_fc_setup_1_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€
§
Atrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/SumSumEtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/RealDivStrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ш
Etrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/ReshapeReshapeAtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/SumCtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Shape*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
Ъ
Atrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/NegNeghead_step_fc_setup_1/LeakyRelu*
T0*'
_output_shapes
:€€€€€€€€€
ч
Gtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/RealDiv_1RealDivAtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Neg.head_step_fc_setup_1_dropout/dropout/keep_prob*
T0*'
_output_shapes
:€€€€€€€€€
э
Gtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/RealDiv_2RealDivGtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/RealDiv_1.head_step_fc_setup_1_dropout/dropout/keep_prob*'
_output_shapes
:€€€€€€€€€*
T0
Ы
Atrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/mulMulVtrain/gradients/head_step_fc_setup_1_dropout/dropout/mul_grad/tuple/control_dependencyGtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/RealDiv_2*'
_output_shapes
:€€€€€€€€€*
T0
§
Ctrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Sum_1SumAtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/mulUtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
Н
Gtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Reshape_1ReshapeCtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Sum_1Etrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 
и
Ntrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/tuple/group_depsNoOpF^train/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/ReshapeH^train/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Reshape_1
Ж
Vtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/tuple/control_dependencyIdentityEtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/ReshapeO^train/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/tuple/group_deps*
T0*X
_classN
LJloc:@train/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Reshape*'
_output_shapes
:€€€€€€€€€
ы
Xtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/tuple/control_dependency_1IdentityGtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Reshape_1O^train/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@train/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/Reshape_1*
_output_shapes
: 
Ы
9train/gradients/head_step_fc_setup_1/LeakyRelu_grad/ShapeShape"head_step_fc_setup_1/LeakyRelu/mul*
_output_shapes
:*
T0*
out_type0
Ч
;train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Shape_1Shapehead_step_fc_setup_1/BiasAdd*
T0*
out_type0*
_output_shapes
:
—
;train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Shape_2ShapeVtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/tuple/control_dependency*
_output_shapes
:*
T0*
out_type0
И
?train/gradients/head_step_fc_setup_1/LeakyRelu_grad/zeros/ConstConst*
valueB 2        *
dtype0*
_output_shapes
: 
Г
9train/gradients/head_step_fc_setup_1/LeakyRelu_grad/zerosFill;train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Shape_2?train/gradients/head_step_fc_setup_1/LeakyRelu_grad/zeros/Const*'
_output_shapes
:€€€€€€€€€*
T0*

index_type0
ƒ
@train/gradients/head_step_fc_setup_1/LeakyRelu_grad/GreaterEqualGreaterEqual"head_step_fc_setup_1/LeakyRelu/mulhead_step_fc_setup_1/BiasAdd*
T0*'
_output_shapes
:€€€€€€€€€
Ч
Itrain/gradients/head_step_fc_setup_1/LeakyRelu_grad/BroadcastGradientArgsBroadcastGradientArgs9train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Shape;train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
Ћ
:train/gradients/head_step_fc_setup_1/LeakyRelu_grad/SelectSelect@train/gradients/head_step_fc_setup_1/LeakyRelu_grad/GreaterEqualVtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/tuple/control_dependency9train/gradients/head_step_fc_setup_1/LeakyRelu_grad/zeros*
T0*'
_output_shapes
:€€€€€€€€€
Ќ
<train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Select_1Select@train/gradients/head_step_fc_setup_1/LeakyRelu_grad/GreaterEqual9train/gradients/head_step_fc_setup_1/LeakyRelu_grad/zerosVtrain/gradients/head_step_fc_setup_1_dropout/dropout/div_grad/tuple/control_dependency*'
_output_shapes
:€€€€€€€€€*
T0
Е
7train/gradients/head_step_fc_setup_1/LeakyRelu_grad/SumSum:train/gradients/head_step_fc_setup_1/LeakyRelu_grad/SelectItrain/gradients/head_step_fc_setup_1/LeakyRelu_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ъ
;train/gradients/head_step_fc_setup_1/LeakyRelu_grad/ReshapeReshape7train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Sum9train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Shape*'
_output_shapes
:€€€€€€€€€*
T0*
Tshape0
Л
9train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Sum_1Sum<train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Select_1Ktrain/gradients/head_step_fc_setup_1/LeakyRelu_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
А
=train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Reshape_1Reshape9train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Sum_1;train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Shape_1*'
_output_shapes
:€€€€€€€€€*
T0*
Tshape0
 
Dtrain/gradients/head_step_fc_setup_1/LeakyRelu_grad/tuple/group_depsNoOp<^train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Reshape>^train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Reshape_1
ё
Ltrain/gradients/head_step_fc_setup_1/LeakyRelu_grad/tuple/control_dependencyIdentity;train/gradients/head_step_fc_setup_1/LeakyRelu_grad/ReshapeE^train/gradients/head_step_fc_setup_1/LeakyRelu_grad/tuple/group_deps*
T0*N
_classD
B@loc:@train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Reshape*'
_output_shapes
:€€€€€€€€€
д
Ntrain/gradients/head_step_fc_setup_1/LeakyRelu_grad/tuple/control_dependency_1Identity=train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Reshape_1E^train/gradients/head_step_fc_setup_1/LeakyRelu_grad/tuple/group_deps*'
_output_shapes
:€€€€€€€€€*
T0*P
_classF
DBloc:@train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Reshape_1
А
=train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
Ы
?train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Shape_1Shapehead_step_fc_setup_1/BiasAdd*
_output_shapes
:*
T0*
out_type0
£
Mtrain/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/BroadcastGradientArgsBroadcastGradientArgs=train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Shape?train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Shape_1*
T0*2
_output_shapes 
:€€€€€€€€€:€€€€€€€€€
а
;train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/MulMulLtrain/gradients/head_step_fc_setup_1/LeakyRelu_grad/tuple/control_dependencyhead_step_fc_setup_1/BiasAdd*'
_output_shapes
:€€€€€€€€€*
T0
О
;train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/SumSum;train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/MulMtrain/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
х
?train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/ReshapeReshape;train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Sum=train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
к
=train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Mul_1Mul$head_step_fc_setup_1/LeakyRelu/alphaLtrain/gradients/head_step_fc_setup_1/LeakyRelu_grad/tuple/control_dependency*
T0*'
_output_shapes
:€€€€€€€€€
Ф
=train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Sum_1Sum=train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Mul_1Otrain/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
М
Atrain/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Reshape_1Reshape=train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Sum_1?train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Shape_1*
T0*
Tshape0*'
_output_shapes
:€€€€€€€€€
÷
Htrain/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/tuple/group_depsNoOp@^train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/ReshapeB^train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Reshape_1
Ё
Ptrain/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/tuple/control_dependencyIdentity?train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/ReshapeI^train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/tuple/group_deps*
T0*R
_classH
FDloc:@train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Reshape*
_output_shapes
: 
ф
Rtrain/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/tuple/control_dependency_1IdentityAtrain/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Reshape_1I^train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@train/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€
ѕ
train/gradients/AddN_9AddNNtrain/gradients/head_step_fc_setup_1/LeakyRelu_grad/tuple/control_dependency_1Rtrain/gradients/head_step_fc_setup_1/LeakyRelu/mul_grad/tuple/control_dependency_1*
T0*P
_classF
DBloc:@train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Reshape_1*
N*'
_output_shapes
:€€€€€€€€€
†
=train/gradients/head_step_fc_setup_1/BiasAdd_grad/BiasAddGradBiasAddGradtrain/gradients/AddN_9*
T0*
data_formatNHWC*
_output_shapes
:
£
Btrain/gradients/head_step_fc_setup_1/BiasAdd_grad/tuple/group_depsNoOp^train/gradients/AddN_9>^train/gradients/head_step_fc_setup_1/BiasAdd_grad/BiasAddGrad
Ј
Jtrain/gradients/head_step_fc_setup_1/BiasAdd_grad/tuple/control_dependencyIdentitytrain/gradients/AddN_9C^train/gradients/head_step_fc_setup_1/BiasAdd_grad/tuple/group_deps*
T0*P
_classF
DBloc:@train/gradients/head_step_fc_setup_1/LeakyRelu_grad/Reshape_1*'
_output_shapes
:€€€€€€€€€
”
Ltrain/gradients/head_step_fc_setup_1/BiasAdd_grad/tuple/control_dependency_1Identity=train/gradients/head_step_fc_setup_1/BiasAdd_grad/BiasAddGradC^train/gradients/head_step_fc_setup_1/BiasAdd_grad/tuple/group_deps*
T0*P
_classF
DBloc:@train/gradients/head_step_fc_setup_1/BiasAdd_grad/BiasAddGrad*
_output_shapes
:
З
7train/gradients/head_step_fc_setup_1/MatMul_grad/MatMulMatMulJtrain/gradients/head_step_fc_setup_1/BiasAdd_grad/tuple/control_dependency head_step_fc_setup_1/kernel/read*
T0*
transpose_a( *'
_output_shapes
:€€€€€€€€€*
transpose_b(
о
9train/gradients/head_step_fc_setup_1/MatMul_grad/MatMul_1MatMulhead_step_inptJtrain/gradients/head_step_fc_setup_1/BiasAdd_grad/tuple/control_dependency*
T0*
transpose_a(*
_output_shapes

:*
transpose_b( 
њ
Atrain/gradients/head_step_fc_setup_1/MatMul_grad/tuple/group_depsNoOp8^train/gradients/head_step_fc_setup_1/MatMul_grad/MatMul:^train/gradients/head_step_fc_setup_1/MatMul_grad/MatMul_1
–
Itrain/gradients/head_step_fc_setup_1/MatMul_grad/tuple/control_dependencyIdentity7train/gradients/head_step_fc_setup_1/MatMul_grad/MatMulB^train/gradients/head_step_fc_setup_1/MatMul_grad/tuple/group_deps*
T0*J
_class@
><loc:@train/gradients/head_step_fc_setup_1/MatMul_grad/MatMul*'
_output_shapes
:€€€€€€€€€
Ќ
Ktrain/gradients/head_step_fc_setup_1/MatMul_grad/tuple/control_dependency_1Identity9train/gradients/head_step_fc_setup_1/MatMul_grad/MatMul_1B^train/gradients/head_step_fc_setup_1/MatMul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@train/gradients/head_step_fc_setup_1/MatMul_grad/MatMul_1*
_output_shapes

:
≈
0conv_layer1_conv/kernel/RMSProp/Initializer/onesConst*)
value B 2      р?**
_class 
loc:@conv_layer1_conv/kernel*
dtype0*&
_output_shapes
: 
ѕ
conv_layer1_conv/kernel/RMSProp
VariableV2*
shape: *
dtype0*&
_output_shapes
: *
shared_name **
_class 
loc:@conv_layer1_conv/kernel*
	container 
Й
&conv_layer1_conv/kernel/RMSProp/AssignAssignconv_layer1_conv/kernel/RMSProp0conv_layer1_conv/kernel/RMSProp/Initializer/ones*
validate_shape(*&
_output_shapes
: *
use_locking(*
T0**
_class 
loc:@conv_layer1_conv/kernel
Ѓ
$conv_layer1_conv/kernel/RMSProp/readIdentityconv_layer1_conv/kernel/RMSProp*
T0**
_class 
loc:@conv_layer1_conv/kernel*&
_output_shapes
: 
»
3conv_layer1_conv/kernel/RMSProp_1/Initializer/zerosConst*)
value B 2        **
_class 
loc:@conv_layer1_conv/kernel*
dtype0*&
_output_shapes
: 
—
!conv_layer1_conv/kernel/RMSProp_1
VariableV2*
shape: *
dtype0*&
_output_shapes
: *
shared_name **
_class 
loc:@conv_layer1_conv/kernel*
	container 
Р
(conv_layer1_conv/kernel/RMSProp_1/AssignAssign!conv_layer1_conv/kernel/RMSProp_13conv_layer1_conv/kernel/RMSProp_1/Initializer/zeros*
validate_shape(*&
_output_shapes
: *
use_locking(*
T0**
_class 
loc:@conv_layer1_conv/kernel
≤
&conv_layer1_conv/kernel/RMSProp_1/readIdentity!conv_layer1_conv/kernel/RMSProp_1*
T0**
_class 
loc:@conv_layer1_conv/kernel*&
_output_shapes
: 
©
.conv_layer1_conv/bias/RMSProp/Initializer/onesConst*
valueB 2      р?*(
_class
loc:@conv_layer1_conv/bias*
dtype0*
_output_shapes
: 
≥
conv_layer1_conv/bias/RMSProp
VariableV2*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_name *(
_class
loc:@conv_layer1_conv/bias
х
$conv_layer1_conv/bias/RMSProp/AssignAssignconv_layer1_conv/bias/RMSProp.conv_layer1_conv/bias/RMSProp/Initializer/ones*
use_locking(*
T0*(
_class
loc:@conv_layer1_conv/bias*
validate_shape(*
_output_shapes
: 
Ь
"conv_layer1_conv/bias/RMSProp/readIdentityconv_layer1_conv/bias/RMSProp*
T0*(
_class
loc:@conv_layer1_conv/bias*
_output_shapes
: 
ђ
1conv_layer1_conv/bias/RMSProp_1/Initializer/zerosConst*
dtype0*
_output_shapes
: *
valueB 2        *(
_class
loc:@conv_layer1_conv/bias
µ
conv_layer1_conv/bias/RMSProp_1
VariableV2*
shared_name *(
_class
loc:@conv_layer1_conv/bias*
	container *
shape: *
dtype0*
_output_shapes
: 
ь
&conv_layer1_conv/bias/RMSProp_1/AssignAssignconv_layer1_conv/bias/RMSProp_11conv_layer1_conv/bias/RMSProp_1/Initializer/zeros*
T0*(
_class
loc:@conv_layer1_conv/bias*
validate_shape(*
_output_shapes
: *
use_locking(
†
$conv_layer1_conv/bias/RMSProp_1/readIdentityconv_layer1_conv/bias/RMSProp_1*
T0*(
_class
loc:@conv_layer1_conv/bias*
_output_shapes
: 
≈
0conv_layer2_conv/kernel/RMSProp/Initializer/onesConst*)
value B 2      р?**
_class 
loc:@conv_layer2_conv/kernel*
dtype0*&
_output_shapes
: 
ѕ
conv_layer2_conv/kernel/RMSProp
VariableV2*
dtype0*&
_output_shapes
: *
shared_name **
_class 
loc:@conv_layer2_conv/kernel*
	container *
shape: 
Й
&conv_layer2_conv/kernel/RMSProp/AssignAssignconv_layer2_conv/kernel/RMSProp0conv_layer2_conv/kernel/RMSProp/Initializer/ones*
T0**
_class 
loc:@conv_layer2_conv/kernel*
validate_shape(*&
_output_shapes
: *
use_locking(
Ѓ
$conv_layer2_conv/kernel/RMSProp/readIdentityconv_layer2_conv/kernel/RMSProp*
T0**
_class 
loc:@conv_layer2_conv/kernel*&
_output_shapes
: 
»
3conv_layer2_conv/kernel/RMSProp_1/Initializer/zerosConst*)
value B 2        **
_class 
loc:@conv_layer2_conv/kernel*
dtype0*&
_output_shapes
: 
—
!conv_layer2_conv/kernel/RMSProp_1
VariableV2*
shape: *
dtype0*&
_output_shapes
: *
shared_name **
_class 
loc:@conv_layer2_conv/kernel*
	container 
Р
(conv_layer2_conv/kernel/RMSProp_1/AssignAssign!conv_layer2_conv/kernel/RMSProp_13conv_layer2_conv/kernel/RMSProp_1/Initializer/zeros*
T0**
_class 
loc:@conv_layer2_conv/kernel*
validate_shape(*&
_output_shapes
: *
use_locking(
≤
&conv_layer2_conv/kernel/RMSProp_1/readIdentity!conv_layer2_conv/kernel/RMSProp_1*&
_output_shapes
: *
T0**
_class 
loc:@conv_layer2_conv/kernel
©
.conv_layer2_conv/bias/RMSProp/Initializer/onesConst*
valueB2      р?*(
_class
loc:@conv_layer2_conv/bias*
dtype0*
_output_shapes
:
≥
conv_layer2_conv/bias/RMSProp
VariableV2*
shared_name *(
_class
loc:@conv_layer2_conv/bias*
	container *
shape:*
dtype0*
_output_shapes
:
х
$conv_layer2_conv/bias/RMSProp/AssignAssignconv_layer2_conv/bias/RMSProp.conv_layer2_conv/bias/RMSProp/Initializer/ones*
use_locking(*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:
Ь
"conv_layer2_conv/bias/RMSProp/readIdentityconv_layer2_conv/bias/RMSProp*
_output_shapes
:*
T0*(
_class
loc:@conv_layer2_conv/bias
ђ
1conv_layer2_conv/bias/RMSProp_1/Initializer/zerosConst*
valueB2        *(
_class
loc:@conv_layer2_conv/bias*
dtype0*
_output_shapes
:
µ
conv_layer2_conv/bias/RMSProp_1
VariableV2*
dtype0*
_output_shapes
:*
shared_name *(
_class
loc:@conv_layer2_conv/bias*
	container *
shape:
ь
&conv_layer2_conv/bias/RMSProp_1/AssignAssignconv_layer2_conv/bias/RMSProp_11conv_layer2_conv/bias/RMSProp_1/Initializer/zeros*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:*
use_locking(
†
$conv_layer2_conv/bias/RMSProp_1/readIdentityconv_layer2_conv/bias/RMSProp_1*
T0*(
_class
loc:@conv_layer2_conv/bias*
_output_shapes
:
µ
<cnn_outlayer/kernel/RMSProp/Initializer/ones/shape_as_tensorConst*
valueB"  d   *&
_class
loc:@cnn_outlayer/kernel*
dtype0*
_output_shapes
:
£
2cnn_outlayer/kernel/RMSProp/Initializer/ones/ConstConst*
valueB 2      р?*&
_class
loc:@cnn_outlayer/kernel*
dtype0*
_output_shapes
: 
К
,cnn_outlayer/kernel/RMSProp/Initializer/onesFill<cnn_outlayer/kernel/RMSProp/Initializer/ones/shape_as_tensor2cnn_outlayer/kernel/RMSProp/Initializer/ones/Const*
_output_shapes
:	Сd*
T0*

index_type0*&
_class
loc:@cnn_outlayer/kernel
є
cnn_outlayer/kernel/RMSProp
VariableV2*&
_class
loc:@cnn_outlayer/kernel*
	container *
shape:	Сd*
dtype0*
_output_shapes
:	Сd*
shared_name 
т
"cnn_outlayer/kernel/RMSProp/AssignAssigncnn_outlayer/kernel/RMSProp,cnn_outlayer/kernel/RMSProp/Initializer/ones*
use_locking(*
T0*&
_class
loc:@cnn_outlayer/kernel*
validate_shape(*
_output_shapes
:	Сd
Ы
 cnn_outlayer/kernel/RMSProp/readIdentitycnn_outlayer/kernel/RMSProp*
T0*&
_class
loc:@cnn_outlayer/kernel*
_output_shapes
:	Сd
Є
?cnn_outlayer/kernel/RMSProp_1/Initializer/zeros/shape_as_tensorConst*
valueB"  d   *&
_class
loc:@cnn_outlayer/kernel*
dtype0*
_output_shapes
:
¶
5cnn_outlayer/kernel/RMSProp_1/Initializer/zeros/ConstConst*
valueB 2        *&
_class
loc:@cnn_outlayer/kernel*
dtype0*
_output_shapes
: 
У
/cnn_outlayer/kernel/RMSProp_1/Initializer/zerosFill?cnn_outlayer/kernel/RMSProp_1/Initializer/zeros/shape_as_tensor5cnn_outlayer/kernel/RMSProp_1/Initializer/zeros/Const*
T0*

index_type0*&
_class
loc:@cnn_outlayer/kernel*
_output_shapes
:	Сd
ї
cnn_outlayer/kernel/RMSProp_1
VariableV2*
	container *
shape:	Сd*
dtype0*
_output_shapes
:	Сd*
shared_name *&
_class
loc:@cnn_outlayer/kernel
щ
$cnn_outlayer/kernel/RMSProp_1/AssignAssigncnn_outlayer/kernel/RMSProp_1/cnn_outlayer/kernel/RMSProp_1/Initializer/zeros*
T0*&
_class
loc:@cnn_outlayer/kernel*
validate_shape(*
_output_shapes
:	Сd*
use_locking(
Я
"cnn_outlayer/kernel/RMSProp_1/readIdentitycnn_outlayer/kernel/RMSProp_1*
_output_shapes
:	Сd*
T0*&
_class
loc:@cnn_outlayer/kernel
°
*cnn_outlayer/bias/RMSProp/Initializer/onesConst*
valueBd2      р?*$
_class
loc:@cnn_outlayer/bias*
dtype0*
_output_shapes
:d
Ђ
cnn_outlayer/bias/RMSProp
VariableV2*
shared_name *$
_class
loc:@cnn_outlayer/bias*
	container *
shape:d*
dtype0*
_output_shapes
:d
е
 cnn_outlayer/bias/RMSProp/AssignAssigncnn_outlayer/bias/RMSProp*cnn_outlayer/bias/RMSProp/Initializer/ones*
use_locking(*
T0*$
_class
loc:@cnn_outlayer/bias*
validate_shape(*
_output_shapes
:d
Р
cnn_outlayer/bias/RMSProp/readIdentitycnn_outlayer/bias/RMSProp*
T0*$
_class
loc:@cnn_outlayer/bias*
_output_shapes
:d
§
-cnn_outlayer/bias/RMSProp_1/Initializer/zerosConst*
dtype0*
_output_shapes
:d*
valueBd2        *$
_class
loc:@cnn_outlayer/bias
≠
cnn_outlayer/bias/RMSProp_1
VariableV2*
dtype0*
_output_shapes
:d*
shared_name *$
_class
loc:@cnn_outlayer/bias*
	container *
shape:d
м
"cnn_outlayer/bias/RMSProp_1/AssignAssigncnn_outlayer/bias/RMSProp_1-cnn_outlayer/bias/RMSProp_1/Initializer/zeros*
use_locking(*
T0*$
_class
loc:@cnn_outlayer/bias*
validate_shape(*
_output_shapes
:d
Ф
 cnn_outlayer/bias/RMSProp_1/readIdentitycnn_outlayer/bias/RMSProp_1*
_output_shapes
:d*
T0*$
_class
loc:@cnn_outlayer/bias
љ
4head_step_fc_setup_1/kernel/RMSProp/Initializer/onesConst*!
valueB2      р?*.
_class$
" loc:@head_step_fc_setup_1/kernel*
dtype0*
_output_shapes

:
«
#head_step_fc_setup_1/kernel/RMSProp
VariableV2*
shared_name *.
_class$
" loc:@head_step_fc_setup_1/kernel*
	container *
shape
:*
dtype0*
_output_shapes

:
С
*head_step_fc_setup_1/kernel/RMSProp/AssignAssign#head_step_fc_setup_1/kernel/RMSProp4head_step_fc_setup_1/kernel/RMSProp/Initializer/ones*
validate_shape(*
_output_shapes

:*
use_locking(*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel
≤
(head_step_fc_setup_1/kernel/RMSProp/readIdentity#head_step_fc_setup_1/kernel/RMSProp*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
_output_shapes

:
ј
7head_step_fc_setup_1/kernel/RMSProp_1/Initializer/zerosConst*!
valueB2        *.
_class$
" loc:@head_step_fc_setup_1/kernel*
dtype0*
_output_shapes

:
…
%head_step_fc_setup_1/kernel/RMSProp_1
VariableV2*
dtype0*
_output_shapes

:*
shared_name *.
_class$
" loc:@head_step_fc_setup_1/kernel*
	container *
shape
:
Ш
,head_step_fc_setup_1/kernel/RMSProp_1/AssignAssign%head_step_fc_setup_1/kernel/RMSProp_17head_step_fc_setup_1/kernel/RMSProp_1/Initializer/zeros*
use_locking(*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
validate_shape(*
_output_shapes

:
ґ
*head_step_fc_setup_1/kernel/RMSProp_1/readIdentity%head_step_fc_setup_1/kernel/RMSProp_1*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
_output_shapes

:
±
2head_step_fc_setup_1/bias/RMSProp/Initializer/onesConst*
valueB2      р?*,
_class"
 loc:@head_step_fc_setup_1/bias*
dtype0*
_output_shapes
:
ї
!head_step_fc_setup_1/bias/RMSProp
VariableV2*
shared_name *,
_class"
 loc:@head_step_fc_setup_1/bias*
	container *
shape:*
dtype0*
_output_shapes
:
Е
(head_step_fc_setup_1/bias/RMSProp/AssignAssign!head_step_fc_setup_1/bias/RMSProp2head_step_fc_setup_1/bias/RMSProp/Initializer/ones*
use_locking(*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
validate_shape(*
_output_shapes
:
®
&head_step_fc_setup_1/bias/RMSProp/readIdentity!head_step_fc_setup_1/bias/RMSProp*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
_output_shapes
:
і
5head_step_fc_setup_1/bias/RMSProp_1/Initializer/zerosConst*
dtype0*
_output_shapes
:*
valueB2        *,
_class"
 loc:@head_step_fc_setup_1/bias
љ
#head_step_fc_setup_1/bias/RMSProp_1
VariableV2*
	container *
shape:*
dtype0*
_output_shapes
:*
shared_name *,
_class"
 loc:@head_step_fc_setup_1/bias
М
*head_step_fc_setup_1/bias/RMSProp_1/AssignAssign#head_step_fc_setup_1/bias/RMSProp_15head_step_fc_setup_1/bias/RMSProp_1/Initializer/zeros*
use_locking(*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
validate_shape(*
_output_shapes
:
ђ
(head_step_fc_setup_1/bias/RMSProp_1/readIdentity#head_step_fc_setup_1/bias/RMSProp_1*
_output_shapes
:*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias
≠
,head_step_fc/kernel/RMSProp/Initializer/onesConst*
dtype0*
_output_shapes

:*!
valueB2      р?*&
_class
loc:@head_step_fc/kernel
Ј
head_step_fc/kernel/RMSProp
VariableV2*
dtype0*
_output_shapes

:*
shared_name *&
_class
loc:@head_step_fc/kernel*
	container *
shape
:
с
"head_step_fc/kernel/RMSProp/AssignAssignhead_step_fc/kernel/RMSProp,head_step_fc/kernel/RMSProp/Initializer/ones*
T0*&
_class
loc:@head_step_fc/kernel*
validate_shape(*
_output_shapes

:*
use_locking(
Ъ
 head_step_fc/kernel/RMSProp/readIdentityhead_step_fc/kernel/RMSProp*
_output_shapes

:*
T0*&
_class
loc:@head_step_fc/kernel
∞
/head_step_fc/kernel/RMSProp_1/Initializer/zerosConst*
dtype0*
_output_shapes

:*!
valueB2        *&
_class
loc:@head_step_fc/kernel
є
head_step_fc/kernel/RMSProp_1
VariableV2*&
_class
loc:@head_step_fc/kernel*
	container *
shape
:*
dtype0*
_output_shapes

:*
shared_name 
ш
$head_step_fc/kernel/RMSProp_1/AssignAssignhead_step_fc/kernel/RMSProp_1/head_step_fc/kernel/RMSProp_1/Initializer/zeros*
T0*&
_class
loc:@head_step_fc/kernel*
validate_shape(*
_output_shapes

:*
use_locking(
Ю
"head_step_fc/kernel/RMSProp_1/readIdentityhead_step_fc/kernel/RMSProp_1*
_output_shapes

:*
T0*&
_class
loc:@head_step_fc/kernel
°
*head_step_fc/bias/RMSProp/Initializer/onesConst*
dtype0*
_output_shapes
:*
valueB2      р?*$
_class
loc:@head_step_fc/bias
Ђ
head_step_fc/bias/RMSProp
VariableV2*$
_class
loc:@head_step_fc/bias*
	container *
shape:*
dtype0*
_output_shapes
:*
shared_name 
е
 head_step_fc/bias/RMSProp/AssignAssignhead_step_fc/bias/RMSProp*head_step_fc/bias/RMSProp/Initializer/ones*
use_locking(*
T0*$
_class
loc:@head_step_fc/bias*
validate_shape(*
_output_shapes
:
Р
head_step_fc/bias/RMSProp/readIdentityhead_step_fc/bias/RMSProp*
T0*$
_class
loc:@head_step_fc/bias*
_output_shapes
:
§
-head_step_fc/bias/RMSProp_1/Initializer/zerosConst*
valueB2        *$
_class
loc:@head_step_fc/bias*
dtype0*
_output_shapes
:
≠
head_step_fc/bias/RMSProp_1
VariableV2*$
_class
loc:@head_step_fc/bias*
	container *
shape:*
dtype0*
_output_shapes
:*
shared_name 
м
"head_step_fc/bias/RMSProp_1/AssignAssignhead_step_fc/bias/RMSProp_1-head_step_fc/bias/RMSProp_1/Initializer/zeros*
use_locking(*
T0*$
_class
loc:@head_step_fc/bias*
validate_shape(*
_output_shapes
:
Ф
 head_step_fc/bias/RMSProp_1/readIdentityhead_step_fc/bias/RMSProp_1*
T0*$
_class
loc:@head_step_fc/bias*
_output_shapes
:
Ќ
Hvect_expand_dense_layer1/kernel/RMSProp/Initializer/ones/shape_as_tensorConst*
valueB"   d   *2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
dtype0*
_output_shapes
:
ї
>vect_expand_dense_layer1/kernel/RMSProp/Initializer/ones/ConstConst*
valueB 2      р?*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
dtype0*
_output_shapes
: 
є
8vect_expand_dense_layer1/kernel/RMSProp/Initializer/onesFillHvect_expand_dense_layer1/kernel/RMSProp/Initializer/ones/shape_as_tensor>vect_expand_dense_layer1/kernel/RMSProp/Initializer/ones/Const*
T0*

index_type0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
_output_shapes

:d
ѕ
'vect_expand_dense_layer1/kernel/RMSProp
VariableV2*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
	container *
shape
:d*
dtype0*
_output_shapes

:d*
shared_name 
°
.vect_expand_dense_layer1/kernel/RMSProp/AssignAssign'vect_expand_dense_layer1/kernel/RMSProp8vect_expand_dense_layer1/kernel/RMSProp/Initializer/ones*
validate_shape(*
_output_shapes

:d*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel
Њ
,vect_expand_dense_layer1/kernel/RMSProp/readIdentity'vect_expand_dense_layer1/kernel/RMSProp*
_output_shapes

:d*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel
–
Kvect_expand_dense_layer1/kernel/RMSProp_1/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB"   d   *2
_class(
&$loc:@vect_expand_dense_layer1/kernel
Њ
Avect_expand_dense_layer1/kernel/RMSProp_1/Initializer/zeros/ConstConst*
valueB 2        *2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
dtype0*
_output_shapes
: 
¬
;vect_expand_dense_layer1/kernel/RMSProp_1/Initializer/zerosFillKvect_expand_dense_layer1/kernel/RMSProp_1/Initializer/zeros/shape_as_tensorAvect_expand_dense_layer1/kernel/RMSProp_1/Initializer/zeros/Const*
T0*

index_type0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
_output_shapes

:d
—
)vect_expand_dense_layer1/kernel/RMSProp_1
VariableV2*
dtype0*
_output_shapes

:d*
shared_name *2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
	container *
shape
:d
®
0vect_expand_dense_layer1/kernel/RMSProp_1/AssignAssign)vect_expand_dense_layer1/kernel/RMSProp_1;vect_expand_dense_layer1/kernel/RMSProp_1/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
validate_shape(*
_output_shapes

:d
¬
.vect_expand_dense_layer1/kernel/RMSProp_1/readIdentity)vect_expand_dense_layer1/kernel/RMSProp_1*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
_output_shapes

:d
є
6vect_expand_dense_layer1/bias/RMSProp/Initializer/onesConst*
valueBd2      р?*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
dtype0*
_output_shapes
:d
√
%vect_expand_dense_layer1/bias/RMSProp
VariableV2*
dtype0*
_output_shapes
:d*
shared_name *0
_class&
$"loc:@vect_expand_dense_layer1/bias*
	container *
shape:d
Х
,vect_expand_dense_layer1/bias/RMSProp/AssignAssign%vect_expand_dense_layer1/bias/RMSProp6vect_expand_dense_layer1/bias/RMSProp/Initializer/ones*
validate_shape(*
_output_shapes
:d*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias
і
*vect_expand_dense_layer1/bias/RMSProp/readIdentity%vect_expand_dense_layer1/bias/RMSProp*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
_output_shapes
:d
Љ
9vect_expand_dense_layer1/bias/RMSProp_1/Initializer/zerosConst*
valueBd2        *0
_class&
$"loc:@vect_expand_dense_layer1/bias*
dtype0*
_output_shapes
:d
≈
'vect_expand_dense_layer1/bias/RMSProp_1
VariableV2*
dtype0*
_output_shapes
:d*
shared_name *0
_class&
$"loc:@vect_expand_dense_layer1/bias*
	container *
shape:d
Ь
.vect_expand_dense_layer1/bias/RMSProp_1/AssignAssign'vect_expand_dense_layer1/bias/RMSProp_19vect_expand_dense_layer1/bias/RMSProp_1/Initializer/zeros*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
validate_shape(*
_output_shapes
:d*
use_locking(
Є
,vect_expand_dense_layer1/bias/RMSProp_1/readIdentity'vect_expand_dense_layer1/bias/RMSProp_1*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
_output_shapes
:d
Ќ
Hvect_expand_dense_layer2/kernel/RMSProp/Initializer/ones/shape_as_tensorConst*
valueB"d   d   *2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
dtype0*
_output_shapes
:
ї
>vect_expand_dense_layer2/kernel/RMSProp/Initializer/ones/ConstConst*
valueB 2      р?*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
dtype0*
_output_shapes
: 
є
8vect_expand_dense_layer2/kernel/RMSProp/Initializer/onesFillHvect_expand_dense_layer2/kernel/RMSProp/Initializer/ones/shape_as_tensor>vect_expand_dense_layer2/kernel/RMSProp/Initializer/ones/Const*
T0*

index_type0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
_output_shapes

:dd
ѕ
'vect_expand_dense_layer2/kernel/RMSProp
VariableV2*
dtype0*
_output_shapes

:dd*
shared_name *2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
	container *
shape
:dd
°
.vect_expand_dense_layer2/kernel/RMSProp/AssignAssign'vect_expand_dense_layer2/kernel/RMSProp8vect_expand_dense_layer2/kernel/RMSProp/Initializer/ones*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
validate_shape(*
_output_shapes

:dd*
use_locking(
Њ
,vect_expand_dense_layer2/kernel/RMSProp/readIdentity'vect_expand_dense_layer2/kernel/RMSProp*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
_output_shapes

:dd
–
Kvect_expand_dense_layer2/kernel/RMSProp_1/Initializer/zeros/shape_as_tensorConst*
valueB"d   d   *2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
dtype0*
_output_shapes
:
Њ
Avect_expand_dense_layer2/kernel/RMSProp_1/Initializer/zeros/ConstConst*
valueB 2        *2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
dtype0*
_output_shapes
: 
¬
;vect_expand_dense_layer2/kernel/RMSProp_1/Initializer/zerosFillKvect_expand_dense_layer2/kernel/RMSProp_1/Initializer/zeros/shape_as_tensorAvect_expand_dense_layer2/kernel/RMSProp_1/Initializer/zeros/Const*
_output_shapes

:dd*
T0*

index_type0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel
—
)vect_expand_dense_layer2/kernel/RMSProp_1
VariableV2*
shared_name *2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
	container *
shape
:dd*
dtype0*
_output_shapes

:dd
®
0vect_expand_dense_layer2/kernel/RMSProp_1/AssignAssign)vect_expand_dense_layer2/kernel/RMSProp_1;vect_expand_dense_layer2/kernel/RMSProp_1/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
validate_shape(*
_output_shapes

:dd
¬
.vect_expand_dense_layer2/kernel/RMSProp_1/readIdentity)vect_expand_dense_layer2/kernel/RMSProp_1*
_output_shapes

:dd*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel
є
6vect_expand_dense_layer2/bias/RMSProp/Initializer/onesConst*
valueBd2      р?*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
dtype0*
_output_shapes
:d
√
%vect_expand_dense_layer2/bias/RMSProp
VariableV2*
shared_name *0
_class&
$"loc:@vect_expand_dense_layer2/bias*
	container *
shape:d*
dtype0*
_output_shapes
:d
Х
,vect_expand_dense_layer2/bias/RMSProp/AssignAssign%vect_expand_dense_layer2/bias/RMSProp6vect_expand_dense_layer2/bias/RMSProp/Initializer/ones*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
validate_shape(*
_output_shapes
:d*
use_locking(
і
*vect_expand_dense_layer2/bias/RMSProp/readIdentity%vect_expand_dense_layer2/bias/RMSProp*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
_output_shapes
:d
Љ
9vect_expand_dense_layer2/bias/RMSProp_1/Initializer/zerosConst*
valueBd2        *0
_class&
$"loc:@vect_expand_dense_layer2/bias*
dtype0*
_output_shapes
:d
≈
'vect_expand_dense_layer2/bias/RMSProp_1
VariableV2*
shape:d*
dtype0*
_output_shapes
:d*
shared_name *0
_class&
$"loc:@vect_expand_dense_layer2/bias*
	container 
Ь
.vect_expand_dense_layer2/bias/RMSProp_1/AssignAssign'vect_expand_dense_layer2/bias/RMSProp_19vect_expand_dense_layer2/bias/RMSProp_1/Initializer/zeros*
validate_shape(*
_output_shapes
:d*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias
Є
,vect_expand_dense_layer2/bias/RMSProp_1/readIdentity'vect_expand_dense_layer2/bias/RMSProp_1*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
_output_shapes
:d
≥
;final_fc_l1/kernel/RMSProp/Initializer/ones/shape_as_tensorConst*
valueB"»   2   *%
_class
loc:@final_fc_l1/kernel*
dtype0*
_output_shapes
:
°
1final_fc_l1/kernel/RMSProp/Initializer/ones/ConstConst*
valueB 2      р?*%
_class
loc:@final_fc_l1/kernel*
dtype0*
_output_shapes
: 
Ж
+final_fc_l1/kernel/RMSProp/Initializer/onesFill;final_fc_l1/kernel/RMSProp/Initializer/ones/shape_as_tensor1final_fc_l1/kernel/RMSProp/Initializer/ones/Const*
T0*

index_type0*%
_class
loc:@final_fc_l1/kernel*
_output_shapes
:	»2
Ј
final_fc_l1/kernel/RMSProp
VariableV2*
dtype0*
_output_shapes
:	»2*
shared_name *%
_class
loc:@final_fc_l1/kernel*
	container *
shape:	»2
о
!final_fc_l1/kernel/RMSProp/AssignAssignfinal_fc_l1/kernel/RMSProp+final_fc_l1/kernel/RMSProp/Initializer/ones*
use_locking(*
T0*%
_class
loc:@final_fc_l1/kernel*
validate_shape(*
_output_shapes
:	»2
Ш
final_fc_l1/kernel/RMSProp/readIdentityfinal_fc_l1/kernel/RMSProp*
_output_shapes
:	»2*
T0*%
_class
loc:@final_fc_l1/kernel
ґ
>final_fc_l1/kernel/RMSProp_1/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB"»   2   *%
_class
loc:@final_fc_l1/kernel
§
4final_fc_l1/kernel/RMSProp_1/Initializer/zeros/ConstConst*
valueB 2        *%
_class
loc:@final_fc_l1/kernel*
dtype0*
_output_shapes
: 
П
.final_fc_l1/kernel/RMSProp_1/Initializer/zerosFill>final_fc_l1/kernel/RMSProp_1/Initializer/zeros/shape_as_tensor4final_fc_l1/kernel/RMSProp_1/Initializer/zeros/Const*
T0*

index_type0*%
_class
loc:@final_fc_l1/kernel*
_output_shapes
:	»2
є
final_fc_l1/kernel/RMSProp_1
VariableV2*
dtype0*
_output_shapes
:	»2*
shared_name *%
_class
loc:@final_fc_l1/kernel*
	container *
shape:	»2
х
#final_fc_l1/kernel/RMSProp_1/AssignAssignfinal_fc_l1/kernel/RMSProp_1.final_fc_l1/kernel/RMSProp_1/Initializer/zeros*
T0*%
_class
loc:@final_fc_l1/kernel*
validate_shape(*
_output_shapes
:	»2*
use_locking(
Ь
!final_fc_l1/kernel/RMSProp_1/readIdentityfinal_fc_l1/kernel/RMSProp_1*
T0*%
_class
loc:@final_fc_l1/kernel*
_output_shapes
:	»2
Я
)final_fc_l1/bias/RMSProp/Initializer/onesConst*
valueB22      р?*#
_class
loc:@final_fc_l1/bias*
dtype0*
_output_shapes
:2
©
final_fc_l1/bias/RMSProp
VariableV2*
dtype0*
_output_shapes
:2*
shared_name *#
_class
loc:@final_fc_l1/bias*
	container *
shape:2
б
final_fc_l1/bias/RMSProp/AssignAssignfinal_fc_l1/bias/RMSProp)final_fc_l1/bias/RMSProp/Initializer/ones*
T0*#
_class
loc:@final_fc_l1/bias*
validate_shape(*
_output_shapes
:2*
use_locking(
Н
final_fc_l1/bias/RMSProp/readIdentityfinal_fc_l1/bias/RMSProp*
T0*#
_class
loc:@final_fc_l1/bias*
_output_shapes
:2
Ґ
,final_fc_l1/bias/RMSProp_1/Initializer/zerosConst*
dtype0*
_output_shapes
:2*
valueB22        *#
_class
loc:@final_fc_l1/bias
Ђ
final_fc_l1/bias/RMSProp_1
VariableV2*
shape:2*
dtype0*
_output_shapes
:2*
shared_name *#
_class
loc:@final_fc_l1/bias*
	container 
и
!final_fc_l1/bias/RMSProp_1/AssignAssignfinal_fc_l1/bias/RMSProp_1,final_fc_l1/bias/RMSProp_1/Initializer/zeros*
use_locking(*
T0*#
_class
loc:@final_fc_l1/bias*
validate_shape(*
_output_shapes
:2
С
final_fc_l1/bias/RMSProp_1/readIdentityfinal_fc_l1/bias/RMSProp_1*
T0*#
_class
loc:@final_fc_l1/bias*
_output_shapes
:2
≥
;final_fc_l2/kernel/RMSProp/Initializer/ones/shape_as_tensorConst*
valueB"2   (   *%
_class
loc:@final_fc_l2/kernel*
dtype0*
_output_shapes
:
°
1final_fc_l2/kernel/RMSProp/Initializer/ones/ConstConst*
valueB 2      р?*%
_class
loc:@final_fc_l2/kernel*
dtype0*
_output_shapes
: 
Е
+final_fc_l2/kernel/RMSProp/Initializer/onesFill;final_fc_l2/kernel/RMSProp/Initializer/ones/shape_as_tensor1final_fc_l2/kernel/RMSProp/Initializer/ones/Const*
T0*

index_type0*%
_class
loc:@final_fc_l2/kernel*
_output_shapes

:2(
µ
final_fc_l2/kernel/RMSProp
VariableV2*
dtype0*
_output_shapes

:2(*
shared_name *%
_class
loc:@final_fc_l2/kernel*
	container *
shape
:2(
н
!final_fc_l2/kernel/RMSProp/AssignAssignfinal_fc_l2/kernel/RMSProp+final_fc_l2/kernel/RMSProp/Initializer/ones*
T0*%
_class
loc:@final_fc_l2/kernel*
validate_shape(*
_output_shapes

:2(*
use_locking(
Ч
final_fc_l2/kernel/RMSProp/readIdentityfinal_fc_l2/kernel/RMSProp*
T0*%
_class
loc:@final_fc_l2/kernel*
_output_shapes

:2(
ґ
>final_fc_l2/kernel/RMSProp_1/Initializer/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*
valueB"2   (   *%
_class
loc:@final_fc_l2/kernel
§
4final_fc_l2/kernel/RMSProp_1/Initializer/zeros/ConstConst*
valueB 2        *%
_class
loc:@final_fc_l2/kernel*
dtype0*
_output_shapes
: 
О
.final_fc_l2/kernel/RMSProp_1/Initializer/zerosFill>final_fc_l2/kernel/RMSProp_1/Initializer/zeros/shape_as_tensor4final_fc_l2/kernel/RMSProp_1/Initializer/zeros/Const*
T0*

index_type0*%
_class
loc:@final_fc_l2/kernel*
_output_shapes

:2(
Ј
final_fc_l2/kernel/RMSProp_1
VariableV2*
dtype0*
_output_shapes

:2(*
shared_name *%
_class
loc:@final_fc_l2/kernel*
	container *
shape
:2(
ф
#final_fc_l2/kernel/RMSProp_1/AssignAssignfinal_fc_l2/kernel/RMSProp_1.final_fc_l2/kernel/RMSProp_1/Initializer/zeros*
use_locking(*
T0*%
_class
loc:@final_fc_l2/kernel*
validate_shape(*
_output_shapes

:2(
Ы
!final_fc_l2/kernel/RMSProp_1/readIdentityfinal_fc_l2/kernel/RMSProp_1*
T0*%
_class
loc:@final_fc_l2/kernel*
_output_shapes

:2(
Я
)final_fc_l2/bias/RMSProp/Initializer/onesConst*
valueB(2      р?*#
_class
loc:@final_fc_l2/bias*
dtype0*
_output_shapes
:(
©
final_fc_l2/bias/RMSProp
VariableV2*
dtype0*
_output_shapes
:(*
shared_name *#
_class
loc:@final_fc_l2/bias*
	container *
shape:(
б
final_fc_l2/bias/RMSProp/AssignAssignfinal_fc_l2/bias/RMSProp)final_fc_l2/bias/RMSProp/Initializer/ones*
use_locking(*
T0*#
_class
loc:@final_fc_l2/bias*
validate_shape(*
_output_shapes
:(
Н
final_fc_l2/bias/RMSProp/readIdentityfinal_fc_l2/bias/RMSProp*
T0*#
_class
loc:@final_fc_l2/bias*
_output_shapes
:(
Ґ
,final_fc_l2/bias/RMSProp_1/Initializer/zerosConst*
valueB(2        *#
_class
loc:@final_fc_l2/bias*
dtype0*
_output_shapes
:(
Ђ
final_fc_l2/bias/RMSProp_1
VariableV2*
shape:(*
dtype0*
_output_shapes
:(*
shared_name *#
_class
loc:@final_fc_l2/bias*
	container 
и
!final_fc_l2/bias/RMSProp_1/AssignAssignfinal_fc_l2/bias/RMSProp_1,final_fc_l2/bias/RMSProp_1/Initializer/zeros*
T0*#
_class
loc:@final_fc_l2/bias*
validate_shape(*
_output_shapes
:(*
use_locking(
С
final_fc_l2/bias/RMSProp_1/readIdentityfinal_fc_l2/bias/RMSProp_1*
_output_shapes
:(*
T0*#
_class
loc:@final_fc_l2/bias
Ђ
+final_fc_l3/kernel/RMSProp/Initializer/onesConst*!
valueB(
2      р?*%
_class
loc:@final_fc_l3/kernel*
dtype0*
_output_shapes

:(

µ
final_fc_l3/kernel/RMSProp
VariableV2*
	container *
shape
:(
*
dtype0*
_output_shapes

:(
*
shared_name *%
_class
loc:@final_fc_l3/kernel
н
!final_fc_l3/kernel/RMSProp/AssignAssignfinal_fc_l3/kernel/RMSProp+final_fc_l3/kernel/RMSProp/Initializer/ones*
T0*%
_class
loc:@final_fc_l3/kernel*
validate_shape(*
_output_shapes

:(
*
use_locking(
Ч
final_fc_l3/kernel/RMSProp/readIdentityfinal_fc_l3/kernel/RMSProp*
T0*%
_class
loc:@final_fc_l3/kernel*
_output_shapes

:(

Ѓ
.final_fc_l3/kernel/RMSProp_1/Initializer/zerosConst*!
valueB(
2        *%
_class
loc:@final_fc_l3/kernel*
dtype0*
_output_shapes

:(

Ј
final_fc_l3/kernel/RMSProp_1
VariableV2*
dtype0*
_output_shapes

:(
*
shared_name *%
_class
loc:@final_fc_l3/kernel*
	container *
shape
:(

ф
#final_fc_l3/kernel/RMSProp_1/AssignAssignfinal_fc_l3/kernel/RMSProp_1.final_fc_l3/kernel/RMSProp_1/Initializer/zeros*
validate_shape(*
_output_shapes

:(
*
use_locking(*
T0*%
_class
loc:@final_fc_l3/kernel
Ы
!final_fc_l3/kernel/RMSProp_1/readIdentityfinal_fc_l3/kernel/RMSProp_1*
T0*%
_class
loc:@final_fc_l3/kernel*
_output_shapes

:(

Я
)final_fc_l3/bias/RMSProp/Initializer/onesConst*
valueB
2      р?*#
_class
loc:@final_fc_l3/bias*
dtype0*
_output_shapes
:

©
final_fc_l3/bias/RMSProp
VariableV2*
shared_name *#
_class
loc:@final_fc_l3/bias*
	container *
shape:
*
dtype0*
_output_shapes
:

б
final_fc_l3/bias/RMSProp/AssignAssignfinal_fc_l3/bias/RMSProp)final_fc_l3/bias/RMSProp/Initializer/ones*
use_locking(*
T0*#
_class
loc:@final_fc_l3/bias*
validate_shape(*
_output_shapes
:

Н
final_fc_l3/bias/RMSProp/readIdentityfinal_fc_l3/bias/RMSProp*
T0*#
_class
loc:@final_fc_l3/bias*
_output_shapes
:

Ґ
,final_fc_l3/bias/RMSProp_1/Initializer/zerosConst*
valueB
2        *#
_class
loc:@final_fc_l3/bias*
dtype0*
_output_shapes
:

Ђ
final_fc_l3/bias/RMSProp_1
VariableV2*
shared_name *#
_class
loc:@final_fc_l3/bias*
	container *
shape:
*
dtype0*
_output_shapes
:

и
!final_fc_l3/bias/RMSProp_1/AssignAssignfinal_fc_l3/bias/RMSProp_1,final_fc_l3/bias/RMSProp_1/Initializer/zeros*
use_locking(*
T0*#
_class
loc:@final_fc_l3/bias*
validate_shape(*
_output_shapes
:

С
final_fc_l3/bias/RMSProp_1/readIdentityfinal_fc_l3/bias/RMSProp_1*
T0*#
_class
loc:@final_fc_l3/bias*
_output_shapes
:

Ђ
+final_fc_l4/kernel/RMSProp/Initializer/onesConst*
dtype0*
_output_shapes

:

*!
valueB

2      р?*%
_class
loc:@final_fc_l4/kernel
µ
final_fc_l4/kernel/RMSProp
VariableV2*
dtype0*
_output_shapes

:

*
shared_name *%
_class
loc:@final_fc_l4/kernel*
	container *
shape
:


н
!final_fc_l4/kernel/RMSProp/AssignAssignfinal_fc_l4/kernel/RMSProp+final_fc_l4/kernel/RMSProp/Initializer/ones*
validate_shape(*
_output_shapes

:

*
use_locking(*
T0*%
_class
loc:@final_fc_l4/kernel
Ч
final_fc_l4/kernel/RMSProp/readIdentityfinal_fc_l4/kernel/RMSProp*
T0*%
_class
loc:@final_fc_l4/kernel*
_output_shapes

:


Ѓ
.final_fc_l4/kernel/RMSProp_1/Initializer/zerosConst*!
valueB

2        *%
_class
loc:@final_fc_l4/kernel*
dtype0*
_output_shapes

:


Ј
final_fc_l4/kernel/RMSProp_1
VariableV2*
dtype0*
_output_shapes

:

*
shared_name *%
_class
loc:@final_fc_l4/kernel*
	container *
shape
:


ф
#final_fc_l4/kernel/RMSProp_1/AssignAssignfinal_fc_l4/kernel/RMSProp_1.final_fc_l4/kernel/RMSProp_1/Initializer/zeros*
T0*%
_class
loc:@final_fc_l4/kernel*
validate_shape(*
_output_shapes

:

*
use_locking(
Ы
!final_fc_l4/kernel/RMSProp_1/readIdentityfinal_fc_l4/kernel/RMSProp_1*
T0*%
_class
loc:@final_fc_l4/kernel*
_output_shapes

:


Я
)final_fc_l4/bias/RMSProp/Initializer/onesConst*
dtype0*
_output_shapes
:
*
valueB
2      р?*#
_class
loc:@final_fc_l4/bias
©
final_fc_l4/bias/RMSProp
VariableV2*
shared_name *#
_class
loc:@final_fc_l4/bias*
	container *
shape:
*
dtype0*
_output_shapes
:

б
final_fc_l4/bias/RMSProp/AssignAssignfinal_fc_l4/bias/RMSProp)final_fc_l4/bias/RMSProp/Initializer/ones*
use_locking(*
T0*#
_class
loc:@final_fc_l4/bias*
validate_shape(*
_output_shapes
:

Н
final_fc_l4/bias/RMSProp/readIdentityfinal_fc_l4/bias/RMSProp*
T0*#
_class
loc:@final_fc_l4/bias*
_output_shapes
:

Ґ
,final_fc_l4/bias/RMSProp_1/Initializer/zerosConst*
valueB
2        *#
_class
loc:@final_fc_l4/bias*
dtype0*
_output_shapes
:

Ђ
final_fc_l4/bias/RMSProp_1
VariableV2*
shared_name *#
_class
loc:@final_fc_l4/bias*
	container *
shape:
*
dtype0*
_output_shapes
:

и
!final_fc_l4/bias/RMSProp_1/AssignAssignfinal_fc_l4/bias/RMSProp_1,final_fc_l4/bias/RMSProp_1/Initializer/zeros*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*#
_class
loc:@final_fc_l4/bias
С
final_fc_l4/bias/RMSProp_1/readIdentityfinal_fc_l4/bias/RMSProp_1*
T0*#
_class
loc:@final_fc_l4/bias*
_output_shapes
:

°
&output/kernel/RMSProp/Initializer/onesConst*!
valueB
2      р?* 
_class
loc:@output/kernel*
dtype0*
_output_shapes

:

Ђ
output/kernel/RMSProp
VariableV2*
dtype0*
_output_shapes

:
*
shared_name * 
_class
loc:@output/kernel*
	container *
shape
:

ў
output/kernel/RMSProp/AssignAssignoutput/kernel/RMSProp&output/kernel/RMSProp/Initializer/ones*
T0* 
_class
loc:@output/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(
И
output/kernel/RMSProp/readIdentityoutput/kernel/RMSProp*
T0* 
_class
loc:@output/kernel*
_output_shapes

:

§
)output/kernel/RMSProp_1/Initializer/zerosConst*!
valueB
2        * 
_class
loc:@output/kernel*
dtype0*
_output_shapes

:

≠
output/kernel/RMSProp_1
VariableV2*
dtype0*
_output_shapes

:
*
shared_name * 
_class
loc:@output/kernel*
	container *
shape
:

а
output/kernel/RMSProp_1/AssignAssignoutput/kernel/RMSProp_1)output/kernel/RMSProp_1/Initializer/zeros*
validate_shape(*
_output_shapes

:
*
use_locking(*
T0* 
_class
loc:@output/kernel
М
output/kernel/RMSProp_1/readIdentityoutput/kernel/RMSProp_1*
T0* 
_class
loc:@output/kernel*
_output_shapes

:

Х
$output/bias/RMSProp/Initializer/onesConst*
dtype0*
_output_shapes
:*
valueB2      р?*
_class
loc:@output/bias
Я
output/bias/RMSProp
VariableV2*
_class
loc:@output/bias*
	container *
shape:*
dtype0*
_output_shapes
:*
shared_name 
Ќ
output/bias/RMSProp/AssignAssignoutput/bias/RMSProp$output/bias/RMSProp/Initializer/ones*
use_locking(*
T0*
_class
loc:@output/bias*
validate_shape(*
_output_shapes
:
~
output/bias/RMSProp/readIdentityoutput/bias/RMSProp*
T0*
_class
loc:@output/bias*
_output_shapes
:
Ш
'output/bias/RMSProp_1/Initializer/zerosConst*
valueB2        *
_class
loc:@output/bias*
dtype0*
_output_shapes
:
°
output/bias/RMSProp_1
VariableV2*
shape:*
dtype0*
_output_shapes
:*
shared_name *
_class
loc:@output/bias*
	container 
‘
output/bias/RMSProp_1/AssignAssignoutput/bias/RMSProp_1'output/bias/RMSProp_1/Initializer/zeros*
use_locking(*
T0*
_class
loc:@output/bias*
validate_shape(*
_output_shapes
:
В
output/bias/RMSProp_1/readIdentityoutput/bias/RMSProp_1*
T0*
_class
loc:@output/bias*
_output_shapes
:
`
train/RMSProp/learning_rateConst*
valueB
 *oГ:*
dtype0*
_output_shapes
: 
X
train/RMSProp/decayConst*
valueB
 *fff?*
dtype0*
_output_shapes
: 
[
train/RMSProp/momentumConst*
valueB
 *    *
dtype0*
_output_shapes
: 
Z
train/RMSProp/epsilonConst*
valueB
 *€жџ.*
dtype0*
_output_shapes
: 
¬
1train/RMSProp/update_conv_layer1_conv/kernel/CastCasttrain/RMSProp/learning_rate*

SrcT0**
_class 
loc:@conv_layer1_conv/kernel*
Truncate( *

DstT0*
_output_shapes
: 
Љ
3train/RMSProp/update_conv_layer1_conv/kernel/Cast_1Casttrain/RMSProp/decay*

SrcT0**
_class 
loc:@conv_layer1_conv/kernel*
Truncate( *

DstT0*
_output_shapes
: 
њ
3train/RMSProp/update_conv_layer1_conv/kernel/Cast_2Casttrain/RMSProp/momentum*

SrcT0**
_class 
loc:@conv_layer1_conv/kernel*
Truncate( *

DstT0*
_output_shapes
: 
Њ
3train/RMSProp/update_conv_layer1_conv/kernel/Cast_3Casttrain/RMSProp/epsilon*

SrcT0**
_class 
loc:@conv_layer1_conv/kernel*
Truncate( *

DstT0*
_output_shapes
: 
±
9train/RMSProp/update_conv_layer1_conv/kernel/ApplyRMSPropApplyRMSPropconv_layer1_conv/kernelconv_layer1_conv/kernel/RMSProp!conv_layer1_conv/kernel/RMSProp_11train/RMSProp/update_conv_layer1_conv/kernel/Cast3train/RMSProp/update_conv_layer1_conv/kernel/Cast_13train/RMSProp/update_conv_layer1_conv/kernel/Cast_23train/RMSProp/update_conv_layer1_conv/kernel/Cast_3Gtrain/gradients/conv_layer1_conv/Conv2D_grad/tuple/control_dependency_1*&
_output_shapes
: *
use_locking( *
T0**
_class 
loc:@conv_layer1_conv/kernel
Њ
/train/RMSProp/update_conv_layer1_conv/bias/CastCasttrain/RMSProp/learning_rate*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*(
_class
loc:@conv_layer1_conv/bias
Є
1train/RMSProp/update_conv_layer1_conv/bias/Cast_1Casttrain/RMSProp/decay*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*(
_class
loc:@conv_layer1_conv/bias
ї
1train/RMSProp/update_conv_layer1_conv/bias/Cast_2Casttrain/RMSProp/momentum*

SrcT0*(
_class
loc:@conv_layer1_conv/bias*
Truncate( *

DstT0*
_output_shapes
: 
Ї
1train/RMSProp/update_conv_layer1_conv/bias/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*(
_class
loc:@conv_layer1_conv/bias*
Truncate( *

DstT0*
_output_shapes
: 
Ф
7train/RMSProp/update_conv_layer1_conv/bias/ApplyRMSPropApplyRMSPropconv_layer1_conv/biasconv_layer1_conv/bias/RMSPropconv_layer1_conv/bias/RMSProp_1/train/RMSProp/update_conv_layer1_conv/bias/Cast1train/RMSProp/update_conv_layer1_conv/bias/Cast_11train/RMSProp/update_conv_layer1_conv/bias/Cast_21train/RMSProp/update_conv_layer1_conv/bias/Cast_3Htrain/gradients/conv_layer1_conv/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*(
_class
loc:@conv_layer1_conv/bias*
_output_shapes
: 
¬
1train/RMSProp/update_conv_layer2_conv/kernel/CastCasttrain/RMSProp/learning_rate*

SrcT0**
_class 
loc:@conv_layer2_conv/kernel*
Truncate( *

DstT0*
_output_shapes
: 
Љ
3train/RMSProp/update_conv_layer2_conv/kernel/Cast_1Casttrain/RMSProp/decay*

SrcT0**
_class 
loc:@conv_layer2_conv/kernel*
Truncate( *

DstT0*
_output_shapes
: 
њ
3train/RMSProp/update_conv_layer2_conv/kernel/Cast_2Casttrain/RMSProp/momentum*

SrcT0**
_class 
loc:@conv_layer2_conv/kernel*
Truncate( *

DstT0*
_output_shapes
: 
Њ
3train/RMSProp/update_conv_layer2_conv/kernel/Cast_3Casttrain/RMSProp/epsilon*

SrcT0**
_class 
loc:@conv_layer2_conv/kernel*
Truncate( *

DstT0*
_output_shapes
: 
±
9train/RMSProp/update_conv_layer2_conv/kernel/ApplyRMSPropApplyRMSPropconv_layer2_conv/kernelconv_layer2_conv/kernel/RMSProp!conv_layer2_conv/kernel/RMSProp_11train/RMSProp/update_conv_layer2_conv/kernel/Cast3train/RMSProp/update_conv_layer2_conv/kernel/Cast_13train/RMSProp/update_conv_layer2_conv/kernel/Cast_23train/RMSProp/update_conv_layer2_conv/kernel/Cast_3Gtrain/gradients/conv_layer2_conv/Conv2D_grad/tuple/control_dependency_1*
T0**
_class 
loc:@conv_layer2_conv/kernel*&
_output_shapes
: *
use_locking( 
Њ
/train/RMSProp/update_conv_layer2_conv/bias/CastCasttrain/RMSProp/learning_rate*

SrcT0*(
_class
loc:@conv_layer2_conv/bias*
Truncate( *

DstT0*
_output_shapes
: 
Є
1train/RMSProp/update_conv_layer2_conv/bias/Cast_1Casttrain/RMSProp/decay*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*(
_class
loc:@conv_layer2_conv/bias
ї
1train/RMSProp/update_conv_layer2_conv/bias/Cast_2Casttrain/RMSProp/momentum*

SrcT0*(
_class
loc:@conv_layer2_conv/bias*
Truncate( *

DstT0*
_output_shapes
: 
Ї
1train/RMSProp/update_conv_layer2_conv/bias/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*(
_class
loc:@conv_layer2_conv/bias*
Truncate( *

DstT0*
_output_shapes
: 
Ф
7train/RMSProp/update_conv_layer2_conv/bias/ApplyRMSPropApplyRMSPropconv_layer2_conv/biasconv_layer2_conv/bias/RMSPropconv_layer2_conv/bias/RMSProp_1/train/RMSProp/update_conv_layer2_conv/bias/Cast1train/RMSProp/update_conv_layer2_conv/bias/Cast_11train/RMSProp/update_conv_layer2_conv/bias/Cast_21train/RMSProp/update_conv_layer2_conv/bias/Cast_3Htrain/gradients/conv_layer2_conv/BiasAdd_grad/tuple/control_dependency_1*
_output_shapes
:*
use_locking( *
T0*(
_class
loc:@conv_layer2_conv/bias
Ї
-train/RMSProp/update_cnn_outlayer/kernel/CastCasttrain/RMSProp/learning_rate*

SrcT0*&
_class
loc:@cnn_outlayer/kernel*
Truncate( *

DstT0*
_output_shapes
: 
і
/train/RMSProp/update_cnn_outlayer/kernel/Cast_1Casttrain/RMSProp/decay*

SrcT0*&
_class
loc:@cnn_outlayer/kernel*
Truncate( *

DstT0*
_output_shapes
: 
Ј
/train/RMSProp/update_cnn_outlayer/kernel/Cast_2Casttrain/RMSProp/momentum*

SrcT0*&
_class
loc:@cnn_outlayer/kernel*
Truncate( *

DstT0*
_output_shapes
: 
ґ
/train/RMSProp/update_cnn_outlayer/kernel/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*&
_class
loc:@cnn_outlayer/kernel*
Truncate( *

DstT0*
_output_shapes
: 
В
5train/RMSProp/update_cnn_outlayer/kernel/ApplyRMSPropApplyRMSPropcnn_outlayer/kernelcnn_outlayer/kernel/RMSPropcnn_outlayer/kernel/RMSProp_1-train/RMSProp/update_cnn_outlayer/kernel/Cast/train/RMSProp/update_cnn_outlayer/kernel/Cast_1/train/RMSProp/update_cnn_outlayer/kernel/Cast_2/train/RMSProp/update_cnn_outlayer/kernel/Cast_3Ctrain/gradients/cnn_outlayer/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*&
_class
loc:@cnn_outlayer/kernel*
_output_shapes
:	Сd
ґ
+train/RMSProp/update_cnn_outlayer/bias/CastCasttrain/RMSProp/learning_rate*

SrcT0*$
_class
loc:@cnn_outlayer/bias*
Truncate( *

DstT0*
_output_shapes
: 
∞
-train/RMSProp/update_cnn_outlayer/bias/Cast_1Casttrain/RMSProp/decay*

SrcT0*$
_class
loc:@cnn_outlayer/bias*
Truncate( *

DstT0*
_output_shapes
: 
≥
-train/RMSProp/update_cnn_outlayer/bias/Cast_2Casttrain/RMSProp/momentum*

SrcT0*$
_class
loc:@cnn_outlayer/bias*
Truncate( *

DstT0*
_output_shapes
: 
≤
-train/RMSProp/update_cnn_outlayer/bias/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*$
_class
loc:@cnn_outlayer/bias*
Truncate( *

DstT0*
_output_shapes
: 
м
3train/RMSProp/update_cnn_outlayer/bias/ApplyRMSPropApplyRMSPropcnn_outlayer/biascnn_outlayer/bias/RMSPropcnn_outlayer/bias/RMSProp_1+train/RMSProp/update_cnn_outlayer/bias/Cast-train/RMSProp/update_cnn_outlayer/bias/Cast_1-train/RMSProp/update_cnn_outlayer/bias/Cast_2-train/RMSProp/update_cnn_outlayer/bias/Cast_3Dtrain/gradients/cnn_outlayer/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*$
_class
loc:@cnn_outlayer/bias*
_output_shapes
:d
 
5train/RMSProp/update_head_step_fc_setup_1/kernel/CastCasttrain/RMSProp/learning_rate*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*.
_class$
" loc:@head_step_fc_setup_1/kernel
ƒ
7train/RMSProp/update_head_step_fc_setup_1/kernel/Cast_1Casttrain/RMSProp/decay*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*.
_class$
" loc:@head_step_fc_setup_1/kernel
«
7train/RMSProp/update_head_step_fc_setup_1/kernel/Cast_2Casttrain/RMSProp/momentum*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*.
_class$
" loc:@head_step_fc_setup_1/kernel
∆
7train/RMSProp/update_head_step_fc_setup_1/kernel/Cast_3Casttrain/RMSProp/epsilon*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*.
_class$
" loc:@head_step_fc_setup_1/kernel
—
=train/RMSProp/update_head_step_fc_setup_1/kernel/ApplyRMSPropApplyRMSProphead_step_fc_setup_1/kernel#head_step_fc_setup_1/kernel/RMSProp%head_step_fc_setup_1/kernel/RMSProp_15train/RMSProp/update_head_step_fc_setup_1/kernel/Cast7train/RMSProp/update_head_step_fc_setup_1/kernel/Cast_17train/RMSProp/update_head_step_fc_setup_1/kernel/Cast_27train/RMSProp/update_head_step_fc_setup_1/kernel/Cast_3Ktrain/gradients/head_step_fc_setup_1/MatMul_grad/tuple/control_dependency_1*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
_output_shapes

:*
use_locking( 
∆
3train/RMSProp/update_head_step_fc_setup_1/bias/CastCasttrain/RMSProp/learning_rate*

SrcT0*,
_class"
 loc:@head_step_fc_setup_1/bias*
Truncate( *

DstT0*
_output_shapes
: 
ј
5train/RMSProp/update_head_step_fc_setup_1/bias/Cast_1Casttrain/RMSProp/decay*

SrcT0*,
_class"
 loc:@head_step_fc_setup_1/bias*
Truncate( *

DstT0*
_output_shapes
: 
√
5train/RMSProp/update_head_step_fc_setup_1/bias/Cast_2Casttrain/RMSProp/momentum*

SrcT0*,
_class"
 loc:@head_step_fc_setup_1/bias*
Truncate( *

DstT0*
_output_shapes
: 
¬
5train/RMSProp/update_head_step_fc_setup_1/bias/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*,
_class"
 loc:@head_step_fc_setup_1/bias*
Truncate( *

DstT0*
_output_shapes
: 
Љ
;train/RMSProp/update_head_step_fc_setup_1/bias/ApplyRMSPropApplyRMSProphead_step_fc_setup_1/bias!head_step_fc_setup_1/bias/RMSProp#head_step_fc_setup_1/bias/RMSProp_13train/RMSProp/update_head_step_fc_setup_1/bias/Cast5train/RMSProp/update_head_step_fc_setup_1/bias/Cast_15train/RMSProp/update_head_step_fc_setup_1/bias/Cast_25train/RMSProp/update_head_step_fc_setup_1/bias/Cast_3Ltrain/gradients/head_step_fc_setup_1/BiasAdd_grad/tuple/control_dependency_1*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
_output_shapes
:*
use_locking( 
Ї
-train/RMSProp/update_head_step_fc/kernel/CastCasttrain/RMSProp/learning_rate*

SrcT0*&
_class
loc:@head_step_fc/kernel*
Truncate( *

DstT0*
_output_shapes
: 
і
/train/RMSProp/update_head_step_fc/kernel/Cast_1Casttrain/RMSProp/decay*

SrcT0*&
_class
loc:@head_step_fc/kernel*
Truncate( *

DstT0*
_output_shapes
: 
Ј
/train/RMSProp/update_head_step_fc/kernel/Cast_2Casttrain/RMSProp/momentum*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*&
_class
loc:@head_step_fc/kernel
ґ
/train/RMSProp/update_head_step_fc/kernel/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*&
_class
loc:@head_step_fc/kernel*
Truncate( *

DstT0*
_output_shapes
: 
Б
5train/RMSProp/update_head_step_fc/kernel/ApplyRMSPropApplyRMSProphead_step_fc/kernelhead_step_fc/kernel/RMSProphead_step_fc/kernel/RMSProp_1-train/RMSProp/update_head_step_fc/kernel/Cast/train/RMSProp/update_head_step_fc/kernel/Cast_1/train/RMSProp/update_head_step_fc/kernel/Cast_2/train/RMSProp/update_head_step_fc/kernel/Cast_3Ctrain/gradients/head_step_fc/MatMul_grad/tuple/control_dependency_1*
T0*&
_class
loc:@head_step_fc/kernel*
_output_shapes

:*
use_locking( 
ґ
+train/RMSProp/update_head_step_fc/bias/CastCasttrain/RMSProp/learning_rate*

SrcT0*$
_class
loc:@head_step_fc/bias*
Truncate( *

DstT0*
_output_shapes
: 
∞
-train/RMSProp/update_head_step_fc/bias/Cast_1Casttrain/RMSProp/decay*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*$
_class
loc:@head_step_fc/bias
≥
-train/RMSProp/update_head_step_fc/bias/Cast_2Casttrain/RMSProp/momentum*

SrcT0*$
_class
loc:@head_step_fc/bias*
Truncate( *

DstT0*
_output_shapes
: 
≤
-train/RMSProp/update_head_step_fc/bias/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*$
_class
loc:@head_step_fc/bias*
Truncate( *

DstT0*
_output_shapes
: 
м
3train/RMSProp/update_head_step_fc/bias/ApplyRMSPropApplyRMSProphead_step_fc/biashead_step_fc/bias/RMSProphead_step_fc/bias/RMSProp_1+train/RMSProp/update_head_step_fc/bias/Cast-train/RMSProp/update_head_step_fc/bias/Cast_1-train/RMSProp/update_head_step_fc/bias/Cast_2-train/RMSProp/update_head_step_fc/bias/Cast_3Dtrain/gradients/head_step_fc/BiasAdd_grad/tuple/control_dependency_1*
_output_shapes
:*
use_locking( *
T0*$
_class
loc:@head_step_fc/bias
“
9train/RMSProp/update_vect_expand_dense_layer1/kernel/CastCasttrain/RMSProp/learning_rate*

SrcT0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
Truncate( *

DstT0*
_output_shapes
: 
ћ
;train/RMSProp/update_vect_expand_dense_layer1/kernel/Cast_1Casttrain/RMSProp/decay*

SrcT0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
Truncate( *

DstT0*
_output_shapes
: 
ѕ
;train/RMSProp/update_vect_expand_dense_layer1/kernel/Cast_2Casttrain/RMSProp/momentum*

SrcT0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
Truncate( *

DstT0*
_output_shapes
: 
ќ
;train/RMSProp/update_vect_expand_dense_layer1/kernel/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
Truncate( *

DstT0*
_output_shapes
: 
щ
Atrain/RMSProp/update_vect_expand_dense_layer1/kernel/ApplyRMSPropApplyRMSPropvect_expand_dense_layer1/kernel'vect_expand_dense_layer1/kernel/RMSProp)vect_expand_dense_layer1/kernel/RMSProp_19train/RMSProp/update_vect_expand_dense_layer1/kernel/Cast;train/RMSProp/update_vect_expand_dense_layer1/kernel/Cast_1;train/RMSProp/update_vect_expand_dense_layer1/kernel/Cast_2;train/RMSProp/update_vect_expand_dense_layer1/kernel/Cast_3Otrain/gradients/vect_expand_dense_layer1/MatMul_grad/tuple/control_dependency_1*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
_output_shapes

:d*
use_locking( 
ќ
7train/RMSProp/update_vect_expand_dense_layer1/bias/CastCasttrain/RMSProp/learning_rate*

SrcT0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
Truncate( *

DstT0*
_output_shapes
: 
»
9train/RMSProp/update_vect_expand_dense_layer1/bias/Cast_1Casttrain/RMSProp/decay*

SrcT0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
Truncate( *

DstT0*
_output_shapes
: 
Ћ
9train/RMSProp/update_vect_expand_dense_layer1/bias/Cast_2Casttrain/RMSProp/momentum*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*0
_class&
$"loc:@vect_expand_dense_layer1/bias
 
9train/RMSProp/update_vect_expand_dense_layer1/bias/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
Truncate( *

DstT0*
_output_shapes
: 
д
?train/RMSProp/update_vect_expand_dense_layer1/bias/ApplyRMSPropApplyRMSPropvect_expand_dense_layer1/bias%vect_expand_dense_layer1/bias/RMSProp'vect_expand_dense_layer1/bias/RMSProp_17train/RMSProp/update_vect_expand_dense_layer1/bias/Cast9train/RMSProp/update_vect_expand_dense_layer1/bias/Cast_19train/RMSProp/update_vect_expand_dense_layer1/bias/Cast_29train/RMSProp/update_vect_expand_dense_layer1/bias/Cast_3Ptrain/gradients/vect_expand_dense_layer1/BiasAdd_grad/tuple/control_dependency_1*
_output_shapes
:d*
use_locking( *
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias
“
9train/RMSProp/update_vect_expand_dense_layer2/kernel/CastCasttrain/RMSProp/learning_rate*

SrcT0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
Truncate( *

DstT0*
_output_shapes
: 
ћ
;train/RMSProp/update_vect_expand_dense_layer2/kernel/Cast_1Casttrain/RMSProp/decay*

SrcT0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
Truncate( *

DstT0*
_output_shapes
: 
ѕ
;train/RMSProp/update_vect_expand_dense_layer2/kernel/Cast_2Casttrain/RMSProp/momentum*

SrcT0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
Truncate( *

DstT0*
_output_shapes
: 
ќ
;train/RMSProp/update_vect_expand_dense_layer2/kernel/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
Truncate( *

DstT0*
_output_shapes
: 
щ
Atrain/RMSProp/update_vect_expand_dense_layer2/kernel/ApplyRMSPropApplyRMSPropvect_expand_dense_layer2/kernel'vect_expand_dense_layer2/kernel/RMSProp)vect_expand_dense_layer2/kernel/RMSProp_19train/RMSProp/update_vect_expand_dense_layer2/kernel/Cast;train/RMSProp/update_vect_expand_dense_layer2/kernel/Cast_1;train/RMSProp/update_vect_expand_dense_layer2/kernel/Cast_2;train/RMSProp/update_vect_expand_dense_layer2/kernel/Cast_3Otrain/gradients/vect_expand_dense_layer2/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
_output_shapes

:dd
ќ
7train/RMSProp/update_vect_expand_dense_layer2/bias/CastCasttrain/RMSProp/learning_rate*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*0
_class&
$"loc:@vect_expand_dense_layer2/bias
»
9train/RMSProp/update_vect_expand_dense_layer2/bias/Cast_1Casttrain/RMSProp/decay*

SrcT0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
Truncate( *

DstT0*
_output_shapes
: 
Ћ
9train/RMSProp/update_vect_expand_dense_layer2/bias/Cast_2Casttrain/RMSProp/momentum*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*0
_class&
$"loc:@vect_expand_dense_layer2/bias
 
9train/RMSProp/update_vect_expand_dense_layer2/bias/Cast_3Casttrain/RMSProp/epsilon*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*0
_class&
$"loc:@vect_expand_dense_layer2/bias
д
?train/RMSProp/update_vect_expand_dense_layer2/bias/ApplyRMSPropApplyRMSPropvect_expand_dense_layer2/bias%vect_expand_dense_layer2/bias/RMSProp'vect_expand_dense_layer2/bias/RMSProp_17train/RMSProp/update_vect_expand_dense_layer2/bias/Cast9train/RMSProp/update_vect_expand_dense_layer2/bias/Cast_19train/RMSProp/update_vect_expand_dense_layer2/bias/Cast_29train/RMSProp/update_vect_expand_dense_layer2/bias/Cast_3Ptrain/gradients/vect_expand_dense_layer2/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
_output_shapes
:d
Є
,train/RMSProp/update_final_fc_l1/kernel/CastCasttrain/RMSProp/learning_rate*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*%
_class
loc:@final_fc_l1/kernel
≤
.train/RMSProp/update_final_fc_l1/kernel/Cast_1Casttrain/RMSProp/decay*

SrcT0*%
_class
loc:@final_fc_l1/kernel*
Truncate( *

DstT0*
_output_shapes
: 
µ
.train/RMSProp/update_final_fc_l1/kernel/Cast_2Casttrain/RMSProp/momentum*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*%
_class
loc:@final_fc_l1/kernel
і
.train/RMSProp/update_final_fc_l1/kernel/Cast_3Casttrain/RMSProp/epsilon*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*%
_class
loc:@final_fc_l1/kernel
ш
4train/RMSProp/update_final_fc_l1/kernel/ApplyRMSPropApplyRMSPropfinal_fc_l1/kernelfinal_fc_l1/kernel/RMSPropfinal_fc_l1/kernel/RMSProp_1,train/RMSProp/update_final_fc_l1/kernel/Cast.train/RMSProp/update_final_fc_l1/kernel/Cast_1.train/RMSProp/update_final_fc_l1/kernel/Cast_2.train/RMSProp/update_final_fc_l1/kernel/Cast_3Btrain/gradients/final_fc_l1/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*%
_class
loc:@final_fc_l1/kernel*
_output_shapes
:	»2
і
*train/RMSProp/update_final_fc_l1/bias/CastCasttrain/RMSProp/learning_rate*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*#
_class
loc:@final_fc_l1/bias
Ѓ
,train/RMSProp/update_final_fc_l1/bias/Cast_1Casttrain/RMSProp/decay*

SrcT0*#
_class
loc:@final_fc_l1/bias*
Truncate( *

DstT0*
_output_shapes
: 
±
,train/RMSProp/update_final_fc_l1/bias/Cast_2Casttrain/RMSProp/momentum*

SrcT0*#
_class
loc:@final_fc_l1/bias*
Truncate( *

DstT0*
_output_shapes
: 
∞
,train/RMSProp/update_final_fc_l1/bias/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*#
_class
loc:@final_fc_l1/bias*
Truncate( *

DstT0*
_output_shapes
: 
в
2train/RMSProp/update_final_fc_l1/bias/ApplyRMSPropApplyRMSPropfinal_fc_l1/biasfinal_fc_l1/bias/RMSPropfinal_fc_l1/bias/RMSProp_1*train/RMSProp/update_final_fc_l1/bias/Cast,train/RMSProp/update_final_fc_l1/bias/Cast_1,train/RMSProp/update_final_fc_l1/bias/Cast_2,train/RMSProp/update_final_fc_l1/bias/Cast_3Ctrain/gradients/final_fc_l1/BiasAdd_grad/tuple/control_dependency_1*
T0*#
_class
loc:@final_fc_l1/bias*
_output_shapes
:2*
use_locking( 
Є
,train/RMSProp/update_final_fc_l2/kernel/CastCasttrain/RMSProp/learning_rate*

SrcT0*%
_class
loc:@final_fc_l2/kernel*
Truncate( *

DstT0*
_output_shapes
: 
≤
.train/RMSProp/update_final_fc_l2/kernel/Cast_1Casttrain/RMSProp/decay*

SrcT0*%
_class
loc:@final_fc_l2/kernel*
Truncate( *

DstT0*
_output_shapes
: 
µ
.train/RMSProp/update_final_fc_l2/kernel/Cast_2Casttrain/RMSProp/momentum*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*%
_class
loc:@final_fc_l2/kernel
і
.train/RMSProp/update_final_fc_l2/kernel/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*%
_class
loc:@final_fc_l2/kernel*
Truncate( *

DstT0*
_output_shapes
: 
ч
4train/RMSProp/update_final_fc_l2/kernel/ApplyRMSPropApplyRMSPropfinal_fc_l2/kernelfinal_fc_l2/kernel/RMSPropfinal_fc_l2/kernel/RMSProp_1,train/RMSProp/update_final_fc_l2/kernel/Cast.train/RMSProp/update_final_fc_l2/kernel/Cast_1.train/RMSProp/update_final_fc_l2/kernel/Cast_2.train/RMSProp/update_final_fc_l2/kernel/Cast_3Btrain/gradients/final_fc_l2/MatMul_grad/tuple/control_dependency_1*
_output_shapes

:2(*
use_locking( *
T0*%
_class
loc:@final_fc_l2/kernel
і
*train/RMSProp/update_final_fc_l2/bias/CastCasttrain/RMSProp/learning_rate*

SrcT0*#
_class
loc:@final_fc_l2/bias*
Truncate( *

DstT0*
_output_shapes
: 
Ѓ
,train/RMSProp/update_final_fc_l2/bias/Cast_1Casttrain/RMSProp/decay*

SrcT0*#
_class
loc:@final_fc_l2/bias*
Truncate( *

DstT0*
_output_shapes
: 
±
,train/RMSProp/update_final_fc_l2/bias/Cast_2Casttrain/RMSProp/momentum*

SrcT0*#
_class
loc:@final_fc_l2/bias*
Truncate( *

DstT0*
_output_shapes
: 
∞
,train/RMSProp/update_final_fc_l2/bias/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*#
_class
loc:@final_fc_l2/bias*
Truncate( *

DstT0*
_output_shapes
: 
в
2train/RMSProp/update_final_fc_l2/bias/ApplyRMSPropApplyRMSPropfinal_fc_l2/biasfinal_fc_l2/bias/RMSPropfinal_fc_l2/bias/RMSProp_1*train/RMSProp/update_final_fc_l2/bias/Cast,train/RMSProp/update_final_fc_l2/bias/Cast_1,train/RMSProp/update_final_fc_l2/bias/Cast_2,train/RMSProp/update_final_fc_l2/bias/Cast_3Ctrain/gradients/final_fc_l2/BiasAdd_grad/tuple/control_dependency_1*
_output_shapes
:(*
use_locking( *
T0*#
_class
loc:@final_fc_l2/bias
Є
,train/RMSProp/update_final_fc_l3/kernel/CastCasttrain/RMSProp/learning_rate*

SrcT0*%
_class
loc:@final_fc_l3/kernel*
Truncate( *

DstT0*
_output_shapes
: 
≤
.train/RMSProp/update_final_fc_l3/kernel/Cast_1Casttrain/RMSProp/decay*

SrcT0*%
_class
loc:@final_fc_l3/kernel*
Truncate( *

DstT0*
_output_shapes
: 
µ
.train/RMSProp/update_final_fc_l3/kernel/Cast_2Casttrain/RMSProp/momentum*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*%
_class
loc:@final_fc_l3/kernel
і
.train/RMSProp/update_final_fc_l3/kernel/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*%
_class
loc:@final_fc_l3/kernel*
Truncate( *

DstT0*
_output_shapes
: 
ч
4train/RMSProp/update_final_fc_l3/kernel/ApplyRMSPropApplyRMSPropfinal_fc_l3/kernelfinal_fc_l3/kernel/RMSPropfinal_fc_l3/kernel/RMSProp_1,train/RMSProp/update_final_fc_l3/kernel/Cast.train/RMSProp/update_final_fc_l3/kernel/Cast_1.train/RMSProp/update_final_fc_l3/kernel/Cast_2.train/RMSProp/update_final_fc_l3/kernel/Cast_3Btrain/gradients/final_fc_l3/MatMul_grad/tuple/control_dependency_1*
T0*%
_class
loc:@final_fc_l3/kernel*
_output_shapes

:(
*
use_locking( 
і
*train/RMSProp/update_final_fc_l3/bias/CastCasttrain/RMSProp/learning_rate*

SrcT0*#
_class
loc:@final_fc_l3/bias*
Truncate( *

DstT0*
_output_shapes
: 
Ѓ
,train/RMSProp/update_final_fc_l3/bias/Cast_1Casttrain/RMSProp/decay*

SrcT0*#
_class
loc:@final_fc_l3/bias*
Truncate( *

DstT0*
_output_shapes
: 
±
,train/RMSProp/update_final_fc_l3/bias/Cast_2Casttrain/RMSProp/momentum*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*#
_class
loc:@final_fc_l3/bias
∞
,train/RMSProp/update_final_fc_l3/bias/Cast_3Casttrain/RMSProp/epsilon*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*#
_class
loc:@final_fc_l3/bias
в
2train/RMSProp/update_final_fc_l3/bias/ApplyRMSPropApplyRMSPropfinal_fc_l3/biasfinal_fc_l3/bias/RMSPropfinal_fc_l3/bias/RMSProp_1*train/RMSProp/update_final_fc_l3/bias/Cast,train/RMSProp/update_final_fc_l3/bias/Cast_1,train/RMSProp/update_final_fc_l3/bias/Cast_2,train/RMSProp/update_final_fc_l3/bias/Cast_3Ctrain/gradients/final_fc_l3/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*#
_class
loc:@final_fc_l3/bias*
_output_shapes
:

Є
,train/RMSProp/update_final_fc_l4/kernel/CastCasttrain/RMSProp/learning_rate*

SrcT0*%
_class
loc:@final_fc_l4/kernel*
Truncate( *

DstT0*
_output_shapes
: 
≤
.train/RMSProp/update_final_fc_l4/kernel/Cast_1Casttrain/RMSProp/decay*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*%
_class
loc:@final_fc_l4/kernel
µ
.train/RMSProp/update_final_fc_l4/kernel/Cast_2Casttrain/RMSProp/momentum*

SrcT0*%
_class
loc:@final_fc_l4/kernel*
Truncate( *

DstT0*
_output_shapes
: 
і
.train/RMSProp/update_final_fc_l4/kernel/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*%
_class
loc:@final_fc_l4/kernel*
Truncate( *

DstT0*
_output_shapes
: 
ч
4train/RMSProp/update_final_fc_l4/kernel/ApplyRMSPropApplyRMSPropfinal_fc_l4/kernelfinal_fc_l4/kernel/RMSPropfinal_fc_l4/kernel/RMSProp_1,train/RMSProp/update_final_fc_l4/kernel/Cast.train/RMSProp/update_final_fc_l4/kernel/Cast_1.train/RMSProp/update_final_fc_l4/kernel/Cast_2.train/RMSProp/update_final_fc_l4/kernel/Cast_3Btrain/gradients/final_fc_l4/MatMul_grad/tuple/control_dependency_1*
_output_shapes

:

*
use_locking( *
T0*%
_class
loc:@final_fc_l4/kernel
і
*train/RMSProp/update_final_fc_l4/bias/CastCasttrain/RMSProp/learning_rate*

SrcT0*#
_class
loc:@final_fc_l4/bias*
Truncate( *

DstT0*
_output_shapes
: 
Ѓ
,train/RMSProp/update_final_fc_l4/bias/Cast_1Casttrain/RMSProp/decay*

SrcT0*#
_class
loc:@final_fc_l4/bias*
Truncate( *

DstT0*
_output_shapes
: 
±
,train/RMSProp/update_final_fc_l4/bias/Cast_2Casttrain/RMSProp/momentum*

SrcT0*#
_class
loc:@final_fc_l4/bias*
Truncate( *

DstT0*
_output_shapes
: 
∞
,train/RMSProp/update_final_fc_l4/bias/Cast_3Casttrain/RMSProp/epsilon*

SrcT0*#
_class
loc:@final_fc_l4/bias*
Truncate( *

DstT0*
_output_shapes
: 
в
2train/RMSProp/update_final_fc_l4/bias/ApplyRMSPropApplyRMSPropfinal_fc_l4/biasfinal_fc_l4/bias/RMSPropfinal_fc_l4/bias/RMSProp_1*train/RMSProp/update_final_fc_l4/bias/Cast,train/RMSProp/update_final_fc_l4/bias/Cast_1,train/RMSProp/update_final_fc_l4/bias/Cast_2,train/RMSProp/update_final_fc_l4/bias/Cast_3Ctrain/gradients/final_fc_l4/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*#
_class
loc:@final_fc_l4/bias*
_output_shapes
:

Ѓ
'train/RMSProp/update_output/kernel/CastCasttrain/RMSProp/learning_rate*

SrcT0* 
_class
loc:@output/kernel*
Truncate( *

DstT0*
_output_shapes
: 
®
)train/RMSProp/update_output/kernel/Cast_1Casttrain/RMSProp/decay*

SrcT0* 
_class
loc:@output/kernel*
Truncate( *

DstT0*
_output_shapes
: 
Ђ
)train/RMSProp/update_output/kernel/Cast_2Casttrain/RMSProp/momentum*

SrcT0* 
_class
loc:@output/kernel*
Truncate( *

DstT0*
_output_shapes
: 
™
)train/RMSProp/update_output/kernel/Cast_3Casttrain/RMSProp/epsilon*

SrcT0* 
_class
loc:@output/kernel*
Truncate( *

DstT0*
_output_shapes
: 
≈
/train/RMSProp/update_output/kernel/ApplyRMSPropApplyRMSPropoutput/kerneloutput/kernel/RMSPropoutput/kernel/RMSProp_1'train/RMSProp/update_output/kernel/Cast)train/RMSProp/update_output/kernel/Cast_1)train/RMSProp/update_output/kernel/Cast_2)train/RMSProp/update_output/kernel/Cast_3=train/gradients/output/MatMul_grad/tuple/control_dependency_1*
_output_shapes

:
*
use_locking( *
T0* 
_class
loc:@output/kernel
™
%train/RMSProp/update_output/bias/CastCasttrain/RMSProp/learning_rate*

SrcT0*
_class
loc:@output/bias*
Truncate( *

DstT0*
_output_shapes
: 
§
'train/RMSProp/update_output/bias/Cast_1Casttrain/RMSProp/decay*

SrcT0*
_class
loc:@output/bias*
Truncate( *

DstT0*
_output_shapes
: 
І
'train/RMSProp/update_output/bias/Cast_2Casttrain/RMSProp/momentum*

SrcT0*
_class
loc:@output/bias*
Truncate( *

DstT0*
_output_shapes
: 
¶
'train/RMSProp/update_output/bias/Cast_3Casttrain/RMSProp/epsilon*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0*
_class
loc:@output/bias
∞
-train/RMSProp/update_output/bias/ApplyRMSPropApplyRMSPropoutput/biasoutput/bias/RMSPropoutput/bias/RMSProp_1%train/RMSProp/update_output/bias/Cast'train/RMSProp/update_output/bias/Cast_1'train/RMSProp/update_output/bias/Cast_2'train/RMSProp/update_output/bias/Cast_3>train/gradients/output/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*
_class
loc:@output/bias*
_output_shapes
:
щ

train/RMSPropNoOp4^train/RMSProp/update_cnn_outlayer/bias/ApplyRMSProp6^train/RMSProp/update_cnn_outlayer/kernel/ApplyRMSProp8^train/RMSProp/update_conv_layer1_conv/bias/ApplyRMSProp:^train/RMSProp/update_conv_layer1_conv/kernel/ApplyRMSProp8^train/RMSProp/update_conv_layer2_conv/bias/ApplyRMSProp:^train/RMSProp/update_conv_layer2_conv/kernel/ApplyRMSProp3^train/RMSProp/update_final_fc_l1/bias/ApplyRMSProp5^train/RMSProp/update_final_fc_l1/kernel/ApplyRMSProp3^train/RMSProp/update_final_fc_l2/bias/ApplyRMSProp5^train/RMSProp/update_final_fc_l2/kernel/ApplyRMSProp3^train/RMSProp/update_final_fc_l3/bias/ApplyRMSProp5^train/RMSProp/update_final_fc_l3/kernel/ApplyRMSProp3^train/RMSProp/update_final_fc_l4/bias/ApplyRMSProp5^train/RMSProp/update_final_fc_l4/kernel/ApplyRMSProp4^train/RMSProp/update_head_step_fc/bias/ApplyRMSProp6^train/RMSProp/update_head_step_fc/kernel/ApplyRMSProp<^train/RMSProp/update_head_step_fc_setup_1/bias/ApplyRMSProp>^train/RMSProp/update_head_step_fc_setup_1/kernel/ApplyRMSProp.^train/RMSProp/update_output/bias/ApplyRMSProp0^train/RMSProp/update_output/kernel/ApplyRMSProp@^train/RMSProp/update_vect_expand_dense_layer1/bias/ApplyRMSPropB^train/RMSProp/update_vect_expand_dense_layer1/kernel/ApplyRMSProp@^train/RMSProp/update_vect_expand_dense_layer2/bias/ApplyRMSPropB^train/RMSProp/update_vect_expand_dense_layer2/kernel/ApplyRMSProp
w
conv_layer1_conv/kernel_0/tagConst**
value!B Bconv_layer1_conv/kernel_0*
dtype0*
_output_shapes
: 
Л
conv_layer1_conv/kernel_0HistogramSummaryconv_layer1_conv/kernel_0/tagconv_layer1_conv/kernel/read*
_output_shapes
: *
T0
s
conv_layer1_conv/bias_0/tagConst*(
valueB Bconv_layer1_conv/bias_0*
dtype0*
_output_shapes
: 
Е
conv_layer1_conv/bias_0HistogramSummaryconv_layer1_conv/bias_0/tagconv_layer1_conv/bias/read*
_output_shapes
: *
T0
w
conv_layer2_conv/kernel_0/tagConst**
value!B Bconv_layer2_conv/kernel_0*
dtype0*
_output_shapes
: 
Л
conv_layer2_conv/kernel_0HistogramSummaryconv_layer2_conv/kernel_0/tagconv_layer2_conv/kernel/read*
T0*
_output_shapes
: 
s
conv_layer2_conv/bias_0/tagConst*(
valueB Bconv_layer2_conv/bias_0*
dtype0*
_output_shapes
: 
Е
conv_layer2_conv/bias_0HistogramSummaryconv_layer2_conv/bias_0/tagconv_layer2_conv/bias/read*
T0*
_output_shapes
: 
o
cnn_outlayer/kernel_0/tagConst*&
valueB Bcnn_outlayer/kernel_0*
dtype0*
_output_shapes
: 

cnn_outlayer/kernel_0HistogramSummarycnn_outlayer/kernel_0/tagcnn_outlayer/kernel/read*
T0*
_output_shapes
: 
k
cnn_outlayer/bias_0/tagConst*$
valueB Bcnn_outlayer/bias_0*
dtype0*
_output_shapes
: 
y
cnn_outlayer/bias_0HistogramSummarycnn_outlayer/bias_0/tagcnn_outlayer/bias/read*
T0*
_output_shapes
: 

!head_step_fc_setup_1/kernel_0/tagConst*.
value%B# Bhead_step_fc_setup_1/kernel_0*
dtype0*
_output_shapes
: 
Ч
head_step_fc_setup_1/kernel_0HistogramSummary!head_step_fc_setup_1/kernel_0/tag head_step_fc_setup_1/kernel/read*
T0*
_output_shapes
: 
{
head_step_fc_setup_1/bias_0/tagConst*,
value#B! Bhead_step_fc_setup_1/bias_0*
dtype0*
_output_shapes
: 
С
head_step_fc_setup_1/bias_0HistogramSummaryhead_step_fc_setup_1/bias_0/taghead_step_fc_setup_1/bias/read*
T0*
_output_shapes
: 
o
head_step_fc/kernel_0/tagConst*&
valueB Bhead_step_fc/kernel_0*
dtype0*
_output_shapes
: 

head_step_fc/kernel_0HistogramSummaryhead_step_fc/kernel_0/taghead_step_fc/kernel/read*
T0*
_output_shapes
: 
k
head_step_fc/bias_0/tagConst*
dtype0*
_output_shapes
: *$
valueB Bhead_step_fc/bias_0
y
head_step_fc/bias_0HistogramSummaryhead_step_fc/bias_0/taghead_step_fc/bias/read*
T0*
_output_shapes
: 
З
%vect_expand_dense_layer1/kernel_0/tagConst*2
value)B' B!vect_expand_dense_layer1/kernel_0*
dtype0*
_output_shapes
: 
£
!vect_expand_dense_layer1/kernel_0HistogramSummary%vect_expand_dense_layer1/kernel_0/tag$vect_expand_dense_layer1/kernel/read*
_output_shapes
: *
T0
Г
#vect_expand_dense_layer1/bias_0/tagConst*
dtype0*
_output_shapes
: *0
value'B% Bvect_expand_dense_layer1/bias_0
Э
vect_expand_dense_layer1/bias_0HistogramSummary#vect_expand_dense_layer1/bias_0/tag"vect_expand_dense_layer1/bias/read*
T0*
_output_shapes
: 
З
%vect_expand_dense_layer2/kernel_0/tagConst*2
value)B' B!vect_expand_dense_layer2/kernel_0*
dtype0*
_output_shapes
: 
£
!vect_expand_dense_layer2/kernel_0HistogramSummary%vect_expand_dense_layer2/kernel_0/tag$vect_expand_dense_layer2/kernel/read*
T0*
_output_shapes
: 
Г
#vect_expand_dense_layer2/bias_0/tagConst*
dtype0*
_output_shapes
: *0
value'B% Bvect_expand_dense_layer2/bias_0
Э
vect_expand_dense_layer2/bias_0HistogramSummary#vect_expand_dense_layer2/bias_0/tag"vect_expand_dense_layer2/bias/read*
T0*
_output_shapes
: 
m
final_fc_l1/kernel_0/tagConst*%
valueB Bfinal_fc_l1/kernel_0*
dtype0*
_output_shapes
: 
|
final_fc_l1/kernel_0HistogramSummaryfinal_fc_l1/kernel_0/tagfinal_fc_l1/kernel/read*
T0*
_output_shapes
: 
i
final_fc_l1/bias_0/tagConst*#
valueB Bfinal_fc_l1/bias_0*
dtype0*
_output_shapes
: 
v
final_fc_l1/bias_0HistogramSummaryfinal_fc_l1/bias_0/tagfinal_fc_l1/bias/read*
T0*
_output_shapes
: 
m
final_fc_l2/kernel_0/tagConst*%
valueB Bfinal_fc_l2/kernel_0*
dtype0*
_output_shapes
: 
|
final_fc_l2/kernel_0HistogramSummaryfinal_fc_l2/kernel_0/tagfinal_fc_l2/kernel/read*
T0*
_output_shapes
: 
i
final_fc_l2/bias_0/tagConst*#
valueB Bfinal_fc_l2/bias_0*
dtype0*
_output_shapes
: 
v
final_fc_l2/bias_0HistogramSummaryfinal_fc_l2/bias_0/tagfinal_fc_l2/bias/read*
_output_shapes
: *
T0
m
final_fc_l3/kernel_0/tagConst*%
valueB Bfinal_fc_l3/kernel_0*
dtype0*
_output_shapes
: 
|
final_fc_l3/kernel_0HistogramSummaryfinal_fc_l3/kernel_0/tagfinal_fc_l3/kernel/read*
_output_shapes
: *
T0
i
final_fc_l3/bias_0/tagConst*#
valueB Bfinal_fc_l3/bias_0*
dtype0*
_output_shapes
: 
v
final_fc_l3/bias_0HistogramSummaryfinal_fc_l3/bias_0/tagfinal_fc_l3/bias/read*
T0*
_output_shapes
: 
m
final_fc_l4/kernel_0/tagConst*
dtype0*
_output_shapes
: *%
valueB Bfinal_fc_l4/kernel_0
|
final_fc_l4/kernel_0HistogramSummaryfinal_fc_l4/kernel_0/tagfinal_fc_l4/kernel/read*
T0*
_output_shapes
: 
i
final_fc_l4/bias_0/tagConst*#
valueB Bfinal_fc_l4/bias_0*
dtype0*
_output_shapes
: 
v
final_fc_l4/bias_0HistogramSummaryfinal_fc_l4/bias_0/tagfinal_fc_l4/bias/read*
T0*
_output_shapes
: 
c
output/kernel_0/tagConst* 
valueB Boutput/kernel_0*
dtype0*
_output_shapes
: 
m
output/kernel_0HistogramSummaryoutput/kernel_0/tagoutput/kernel/read*
T0*
_output_shapes
: 
_
output/bias_0/tagConst*
valueB Boutput/bias_0*
dtype0*
_output_shapes
: 
g
output/bias_0HistogramSummaryoutput/bias_0/tagoutput/bias/read*
T0*
_output_shapes
: 
\
error_total/tagsConst*
dtype0*
_output_shapes
: *
valueB Berror_total
_
error_totalScalarSummaryerror_total/tagsaccuracy/add_1*
T0*
_output_shapes
: 
h
error_xy_sqrd_out/tagsConst*"
valueB Berror_xy_sqrd_out*
dtype0*
_output_shapes
: 
l
error_xy_sqrd_outScalarSummaryerror_xy_sqrd_out/tagsaccuracy/Mean_1*
_output_shapes
: *
T0
h
error_th_sqrd_out/tagsConst*"
valueB Berror_th_sqrd_out*
dtype0*
_output_shapes
: 
j
error_th_sqrd_outScalarSummaryerror_th_sqrd_out/tagsaccuracy/Mean*
_output_shapes
: *
T0
W
input_img/tagConst*
valueB B	input_img*
dtype0*
_output_shapes
: 
Ж
	input_imgImageSummaryinput_img/tagimg_inpt*
_output_shapes
: *

max_images*
T0*
	bad_colorB:€  €
€
Merge/MergeSummaryMergeSummaryconv_layer1_activationsconv_layer2_activationsconv_layer1_conv/kernel_0conv_layer1_conv/bias_0conv_layer2_conv/kernel_0conv_layer2_conv/bias_0cnn_outlayer/kernel_0cnn_outlayer/bias_0head_step_fc_setup_1/kernel_0head_step_fc_setup_1/bias_0head_step_fc/kernel_0head_step_fc/bias_0!vect_expand_dense_layer1/kernel_0vect_expand_dense_layer1/bias_0!vect_expand_dense_layer2/kernel_0vect_expand_dense_layer2/bias_0final_fc_l1/kernel_0final_fc_l1/bias_0final_fc_l2/kernel_0final_fc_l2/bias_0final_fc_l3/kernel_0final_fc_l3/bias_0final_fc_l4/kernel_0final_fc_l4/bias_0output/kernel_0output/bias_0error_totalerror_xy_sqrd_outerror_th_sqrd_out	input_img*
N*
_output_shapes
: 
x
Merge_1/MergeSummaryMergeSummaryerror_totalerror_xy_sqrd_outerror_th_sqrd_out*
N*
_output_shapes
: 
P

save/ConstConst*
valueB Bmodel*
dtype0*
_output_shapes
: 
ж
save/SaveV2/tensor_namesConst*Щ
valueПBМHBcnn_outlayer/biasBcnn_outlayer/bias/RMSPropBcnn_outlayer/bias/RMSProp_1Bcnn_outlayer/kernelBcnn_outlayer/kernel/RMSPropBcnn_outlayer/kernel/RMSProp_1Bconv_layer1_conv/biasBconv_layer1_conv/bias/RMSPropBconv_layer1_conv/bias/RMSProp_1Bconv_layer1_conv/kernelBconv_layer1_conv/kernel/RMSPropB!conv_layer1_conv/kernel/RMSProp_1Bconv_layer2_conv/biasBconv_layer2_conv/bias/RMSPropBconv_layer2_conv/bias/RMSProp_1Bconv_layer2_conv/kernelBconv_layer2_conv/kernel/RMSPropB!conv_layer2_conv/kernel/RMSProp_1Bfinal_fc_l1/biasBfinal_fc_l1/bias/RMSPropBfinal_fc_l1/bias/RMSProp_1Bfinal_fc_l1/kernelBfinal_fc_l1/kernel/RMSPropBfinal_fc_l1/kernel/RMSProp_1Bfinal_fc_l2/biasBfinal_fc_l2/bias/RMSPropBfinal_fc_l2/bias/RMSProp_1Bfinal_fc_l2/kernelBfinal_fc_l2/kernel/RMSPropBfinal_fc_l2/kernel/RMSProp_1Bfinal_fc_l3/biasBfinal_fc_l3/bias/RMSPropBfinal_fc_l3/bias/RMSProp_1Bfinal_fc_l3/kernelBfinal_fc_l3/kernel/RMSPropBfinal_fc_l3/kernel/RMSProp_1Bfinal_fc_l4/biasBfinal_fc_l4/bias/RMSPropBfinal_fc_l4/bias/RMSProp_1Bfinal_fc_l4/kernelBfinal_fc_l4/kernel/RMSPropBfinal_fc_l4/kernel/RMSProp_1Bhead_step_fc/biasBhead_step_fc/bias/RMSPropBhead_step_fc/bias/RMSProp_1Bhead_step_fc/kernelBhead_step_fc/kernel/RMSPropBhead_step_fc/kernel/RMSProp_1Bhead_step_fc_setup_1/biasB!head_step_fc_setup_1/bias/RMSPropB#head_step_fc_setup_1/bias/RMSProp_1Bhead_step_fc_setup_1/kernelB#head_step_fc_setup_1/kernel/RMSPropB%head_step_fc_setup_1/kernel/RMSProp_1Boutput/biasBoutput/bias/RMSPropBoutput/bias/RMSProp_1Boutput/kernelBoutput/kernel/RMSPropBoutput/kernel/RMSProp_1Bvect_expand_dense_layer1/biasB%vect_expand_dense_layer1/bias/RMSPropB'vect_expand_dense_layer1/bias/RMSProp_1Bvect_expand_dense_layer1/kernelB'vect_expand_dense_layer1/kernel/RMSPropB)vect_expand_dense_layer1/kernel/RMSProp_1Bvect_expand_dense_layer2/biasB%vect_expand_dense_layer2/bias/RMSPropB'vect_expand_dense_layer2/bias/RMSProp_1Bvect_expand_dense_layer2/kernelB'vect_expand_dense_layer2/kernel/RMSPropB)vect_expand_dense_layer2/kernel/RMSProp_1*
dtype0*
_output_shapes
:H
ц
save/SaveV2/shape_and_slicesConst*•
valueЫBШHB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0*
_output_shapes
:H
µ
save/SaveV2SaveV2
save/Constsave/SaveV2/tensor_namessave/SaveV2/shape_and_slicescnn_outlayer/biascnn_outlayer/bias/RMSPropcnn_outlayer/bias/RMSProp_1cnn_outlayer/kernelcnn_outlayer/kernel/RMSPropcnn_outlayer/kernel/RMSProp_1conv_layer1_conv/biasconv_layer1_conv/bias/RMSPropconv_layer1_conv/bias/RMSProp_1conv_layer1_conv/kernelconv_layer1_conv/kernel/RMSProp!conv_layer1_conv/kernel/RMSProp_1conv_layer2_conv/biasconv_layer2_conv/bias/RMSPropconv_layer2_conv/bias/RMSProp_1conv_layer2_conv/kernelconv_layer2_conv/kernel/RMSProp!conv_layer2_conv/kernel/RMSProp_1final_fc_l1/biasfinal_fc_l1/bias/RMSPropfinal_fc_l1/bias/RMSProp_1final_fc_l1/kernelfinal_fc_l1/kernel/RMSPropfinal_fc_l1/kernel/RMSProp_1final_fc_l2/biasfinal_fc_l2/bias/RMSPropfinal_fc_l2/bias/RMSProp_1final_fc_l2/kernelfinal_fc_l2/kernel/RMSPropfinal_fc_l2/kernel/RMSProp_1final_fc_l3/biasfinal_fc_l3/bias/RMSPropfinal_fc_l3/bias/RMSProp_1final_fc_l3/kernelfinal_fc_l3/kernel/RMSPropfinal_fc_l3/kernel/RMSProp_1final_fc_l4/biasfinal_fc_l4/bias/RMSPropfinal_fc_l4/bias/RMSProp_1final_fc_l4/kernelfinal_fc_l4/kernel/RMSPropfinal_fc_l4/kernel/RMSProp_1head_step_fc/biashead_step_fc/bias/RMSProphead_step_fc/bias/RMSProp_1head_step_fc/kernelhead_step_fc/kernel/RMSProphead_step_fc/kernel/RMSProp_1head_step_fc_setup_1/bias!head_step_fc_setup_1/bias/RMSProp#head_step_fc_setup_1/bias/RMSProp_1head_step_fc_setup_1/kernel#head_step_fc_setup_1/kernel/RMSProp%head_step_fc_setup_1/kernel/RMSProp_1output/biasoutput/bias/RMSPropoutput/bias/RMSProp_1output/kerneloutput/kernel/RMSPropoutput/kernel/RMSProp_1vect_expand_dense_layer1/bias%vect_expand_dense_layer1/bias/RMSProp'vect_expand_dense_layer1/bias/RMSProp_1vect_expand_dense_layer1/kernel'vect_expand_dense_layer1/kernel/RMSProp)vect_expand_dense_layer1/kernel/RMSProp_1vect_expand_dense_layer2/bias%vect_expand_dense_layer2/bias/RMSProp'vect_expand_dense_layer2/bias/RMSProp_1vect_expand_dense_layer2/kernel'vect_expand_dense_layer2/kernel/RMSProp)vect_expand_dense_layer2/kernel/RMSProp_1*V
dtypesL
J2H
}
save/control_dependencyIdentity
save/Const^save/SaveV2*
T0*
_class
loc:@save/Const*
_output_shapes
: 
й
save/RestoreV2/tensor_namesConst*Щ
valueПBМHBcnn_outlayer/biasBcnn_outlayer/bias/RMSPropBcnn_outlayer/bias/RMSProp_1Bcnn_outlayer/kernelBcnn_outlayer/kernel/RMSPropBcnn_outlayer/kernel/RMSProp_1Bconv_layer1_conv/biasBconv_layer1_conv/bias/RMSPropBconv_layer1_conv/bias/RMSProp_1Bconv_layer1_conv/kernelBconv_layer1_conv/kernel/RMSPropB!conv_layer1_conv/kernel/RMSProp_1Bconv_layer2_conv/biasBconv_layer2_conv/bias/RMSPropBconv_layer2_conv/bias/RMSProp_1Bconv_layer2_conv/kernelBconv_layer2_conv/kernel/RMSPropB!conv_layer2_conv/kernel/RMSProp_1Bfinal_fc_l1/biasBfinal_fc_l1/bias/RMSPropBfinal_fc_l1/bias/RMSProp_1Bfinal_fc_l1/kernelBfinal_fc_l1/kernel/RMSPropBfinal_fc_l1/kernel/RMSProp_1Bfinal_fc_l2/biasBfinal_fc_l2/bias/RMSPropBfinal_fc_l2/bias/RMSProp_1Bfinal_fc_l2/kernelBfinal_fc_l2/kernel/RMSPropBfinal_fc_l2/kernel/RMSProp_1Bfinal_fc_l3/biasBfinal_fc_l3/bias/RMSPropBfinal_fc_l3/bias/RMSProp_1Bfinal_fc_l3/kernelBfinal_fc_l3/kernel/RMSPropBfinal_fc_l3/kernel/RMSProp_1Bfinal_fc_l4/biasBfinal_fc_l4/bias/RMSPropBfinal_fc_l4/bias/RMSProp_1Bfinal_fc_l4/kernelBfinal_fc_l4/kernel/RMSPropBfinal_fc_l4/kernel/RMSProp_1Bhead_step_fc/biasBhead_step_fc/bias/RMSPropBhead_step_fc/bias/RMSProp_1Bhead_step_fc/kernelBhead_step_fc/kernel/RMSPropBhead_step_fc/kernel/RMSProp_1Bhead_step_fc_setup_1/biasB!head_step_fc_setup_1/bias/RMSPropB#head_step_fc_setup_1/bias/RMSProp_1Bhead_step_fc_setup_1/kernelB#head_step_fc_setup_1/kernel/RMSPropB%head_step_fc_setup_1/kernel/RMSProp_1Boutput/biasBoutput/bias/RMSPropBoutput/bias/RMSProp_1Boutput/kernelBoutput/kernel/RMSPropBoutput/kernel/RMSProp_1Bvect_expand_dense_layer1/biasB%vect_expand_dense_layer1/bias/RMSPropB'vect_expand_dense_layer1/bias/RMSProp_1Bvect_expand_dense_layer1/kernelB'vect_expand_dense_layer1/kernel/RMSPropB)vect_expand_dense_layer1/kernel/RMSProp_1Bvect_expand_dense_layer2/biasB%vect_expand_dense_layer2/bias/RMSPropB'vect_expand_dense_layer2/bias/RMSProp_1Bvect_expand_dense_layer2/kernelB'vect_expand_dense_layer2/kernel/RMSPropB)vect_expand_dense_layer2/kernel/RMSProp_1*
dtype0*
_output_shapes
:H
щ
save/RestoreV2/shape_and_slicesConst*
dtype0*
_output_shapes
:H*•
valueЫBШHB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 
ц
save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices*V
dtypesL
J2H*ґ
_output_shapes£
†::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
ђ
save/AssignAssigncnn_outlayer/biassave/RestoreV2*
use_locking(*
T0*$
_class
loc:@cnn_outlayer/bias*
validate_shape(*
_output_shapes
:d
Є
save/Assign_1Assigncnn_outlayer/bias/RMSPropsave/RestoreV2:1*
validate_shape(*
_output_shapes
:d*
use_locking(*
T0*$
_class
loc:@cnn_outlayer/bias
Ї
save/Assign_2Assigncnn_outlayer/bias/RMSProp_1save/RestoreV2:2*
use_locking(*
T0*$
_class
loc:@cnn_outlayer/bias*
validate_shape(*
_output_shapes
:d
є
save/Assign_3Assigncnn_outlayer/kernelsave/RestoreV2:3*
validate_shape(*
_output_shapes
:	Сd*
use_locking(*
T0*&
_class
loc:@cnn_outlayer/kernel
Ѕ
save/Assign_4Assigncnn_outlayer/kernel/RMSPropsave/RestoreV2:4*
T0*&
_class
loc:@cnn_outlayer/kernel*
validate_shape(*
_output_shapes
:	Сd*
use_locking(
√
save/Assign_5Assigncnn_outlayer/kernel/RMSProp_1save/RestoreV2:5*
use_locking(*
T0*&
_class
loc:@cnn_outlayer/kernel*
validate_shape(*
_output_shapes
:	Сd
Є
save/Assign_6Assignconv_layer1_conv/biassave/RestoreV2:6*
use_locking(*
T0*(
_class
loc:@conv_layer1_conv/bias*
validate_shape(*
_output_shapes
: 
ј
save/Assign_7Assignconv_layer1_conv/bias/RMSPropsave/RestoreV2:7*
T0*(
_class
loc:@conv_layer1_conv/bias*
validate_shape(*
_output_shapes
: *
use_locking(
¬
save/Assign_8Assignconv_layer1_conv/bias/RMSProp_1save/RestoreV2:8*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*(
_class
loc:@conv_layer1_conv/bias
»
save/Assign_9Assignconv_layer1_conv/kernelsave/RestoreV2:9*
T0**
_class 
loc:@conv_layer1_conv/kernel*
validate_shape(*&
_output_shapes
: *
use_locking(
“
save/Assign_10Assignconv_layer1_conv/kernel/RMSPropsave/RestoreV2:10*
use_locking(*
T0**
_class 
loc:@conv_layer1_conv/kernel*
validate_shape(*&
_output_shapes
: 
‘
save/Assign_11Assign!conv_layer1_conv/kernel/RMSProp_1save/RestoreV2:11*
validate_shape(*&
_output_shapes
: *
use_locking(*
T0**
_class 
loc:@conv_layer1_conv/kernel
Ї
save/Assign_12Assignconv_layer2_conv/biassave/RestoreV2:12*
use_locking(*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:
¬
save/Assign_13Assignconv_layer2_conv/bias/RMSPropsave/RestoreV2:13*
use_locking(*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:
ƒ
save/Assign_14Assignconv_layer2_conv/bias/RMSProp_1save/RestoreV2:14*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:*
use_locking(
 
save/Assign_15Assignconv_layer2_conv/kernelsave/RestoreV2:15*
use_locking(*
T0**
_class 
loc:@conv_layer2_conv/kernel*
validate_shape(*&
_output_shapes
: 
“
save/Assign_16Assignconv_layer2_conv/kernel/RMSPropsave/RestoreV2:16*
validate_shape(*&
_output_shapes
: *
use_locking(*
T0**
_class 
loc:@conv_layer2_conv/kernel
‘
save/Assign_17Assign!conv_layer2_conv/kernel/RMSProp_1save/RestoreV2:17*
T0**
_class 
loc:@conv_layer2_conv/kernel*
validate_shape(*&
_output_shapes
: *
use_locking(
∞
save/Assign_18Assignfinal_fc_l1/biassave/RestoreV2:18*
use_locking(*
T0*#
_class
loc:@final_fc_l1/bias*
validate_shape(*
_output_shapes
:2
Є
save/Assign_19Assignfinal_fc_l1/bias/RMSPropsave/RestoreV2:19*
use_locking(*
T0*#
_class
loc:@final_fc_l1/bias*
validate_shape(*
_output_shapes
:2
Ї
save/Assign_20Assignfinal_fc_l1/bias/RMSProp_1save/RestoreV2:20*
use_locking(*
T0*#
_class
loc:@final_fc_l1/bias*
validate_shape(*
_output_shapes
:2
є
save/Assign_21Assignfinal_fc_l1/kernelsave/RestoreV2:21*
validate_shape(*
_output_shapes
:	»2*
use_locking(*
T0*%
_class
loc:@final_fc_l1/kernel
Ѕ
save/Assign_22Assignfinal_fc_l1/kernel/RMSPropsave/RestoreV2:22*
use_locking(*
T0*%
_class
loc:@final_fc_l1/kernel*
validate_shape(*
_output_shapes
:	»2
√
save/Assign_23Assignfinal_fc_l1/kernel/RMSProp_1save/RestoreV2:23*
use_locking(*
T0*%
_class
loc:@final_fc_l1/kernel*
validate_shape(*
_output_shapes
:	»2
∞
save/Assign_24Assignfinal_fc_l2/biassave/RestoreV2:24*
use_locking(*
T0*#
_class
loc:@final_fc_l2/bias*
validate_shape(*
_output_shapes
:(
Є
save/Assign_25Assignfinal_fc_l2/bias/RMSPropsave/RestoreV2:25*
use_locking(*
T0*#
_class
loc:@final_fc_l2/bias*
validate_shape(*
_output_shapes
:(
Ї
save/Assign_26Assignfinal_fc_l2/bias/RMSProp_1save/RestoreV2:26*
validate_shape(*
_output_shapes
:(*
use_locking(*
T0*#
_class
loc:@final_fc_l2/bias
Є
save/Assign_27Assignfinal_fc_l2/kernelsave/RestoreV2:27*
T0*%
_class
loc:@final_fc_l2/kernel*
validate_shape(*
_output_shapes

:2(*
use_locking(
ј
save/Assign_28Assignfinal_fc_l2/kernel/RMSPropsave/RestoreV2:28*
use_locking(*
T0*%
_class
loc:@final_fc_l2/kernel*
validate_shape(*
_output_shapes

:2(
¬
save/Assign_29Assignfinal_fc_l2/kernel/RMSProp_1save/RestoreV2:29*
use_locking(*
T0*%
_class
loc:@final_fc_l2/kernel*
validate_shape(*
_output_shapes

:2(
∞
save/Assign_30Assignfinal_fc_l3/biassave/RestoreV2:30*
T0*#
_class
loc:@final_fc_l3/bias*
validate_shape(*
_output_shapes
:
*
use_locking(
Є
save/Assign_31Assignfinal_fc_l3/bias/RMSPropsave/RestoreV2:31*
use_locking(*
T0*#
_class
loc:@final_fc_l3/bias*
validate_shape(*
_output_shapes
:

Ї
save/Assign_32Assignfinal_fc_l3/bias/RMSProp_1save/RestoreV2:32*
use_locking(*
T0*#
_class
loc:@final_fc_l3/bias*
validate_shape(*
_output_shapes
:

Є
save/Assign_33Assignfinal_fc_l3/kernelsave/RestoreV2:33*
T0*%
_class
loc:@final_fc_l3/kernel*
validate_shape(*
_output_shapes

:(
*
use_locking(
ј
save/Assign_34Assignfinal_fc_l3/kernel/RMSPropsave/RestoreV2:34*
T0*%
_class
loc:@final_fc_l3/kernel*
validate_shape(*
_output_shapes

:(
*
use_locking(
¬
save/Assign_35Assignfinal_fc_l3/kernel/RMSProp_1save/RestoreV2:35*
T0*%
_class
loc:@final_fc_l3/kernel*
validate_shape(*
_output_shapes

:(
*
use_locking(
∞
save/Assign_36Assignfinal_fc_l4/biassave/RestoreV2:36*
use_locking(*
T0*#
_class
loc:@final_fc_l4/bias*
validate_shape(*
_output_shapes
:

Є
save/Assign_37Assignfinal_fc_l4/bias/RMSPropsave/RestoreV2:37*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*#
_class
loc:@final_fc_l4/bias
Ї
save/Assign_38Assignfinal_fc_l4/bias/RMSProp_1save/RestoreV2:38*
use_locking(*
T0*#
_class
loc:@final_fc_l4/bias*
validate_shape(*
_output_shapes
:

Є
save/Assign_39Assignfinal_fc_l4/kernelsave/RestoreV2:39*
use_locking(*
T0*%
_class
loc:@final_fc_l4/kernel*
validate_shape(*
_output_shapes

:


ј
save/Assign_40Assignfinal_fc_l4/kernel/RMSPropsave/RestoreV2:40*
use_locking(*
T0*%
_class
loc:@final_fc_l4/kernel*
validate_shape(*
_output_shapes

:


¬
save/Assign_41Assignfinal_fc_l4/kernel/RMSProp_1save/RestoreV2:41*
T0*%
_class
loc:@final_fc_l4/kernel*
validate_shape(*
_output_shapes

:

*
use_locking(
≤
save/Assign_42Assignhead_step_fc/biassave/RestoreV2:42*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*$
_class
loc:@head_step_fc/bias
Ї
save/Assign_43Assignhead_step_fc/bias/RMSPropsave/RestoreV2:43*
T0*$
_class
loc:@head_step_fc/bias*
validate_shape(*
_output_shapes
:*
use_locking(
Љ
save/Assign_44Assignhead_step_fc/bias/RMSProp_1save/RestoreV2:44*
use_locking(*
T0*$
_class
loc:@head_step_fc/bias*
validate_shape(*
_output_shapes
:
Ї
save/Assign_45Assignhead_step_fc/kernelsave/RestoreV2:45*
T0*&
_class
loc:@head_step_fc/kernel*
validate_shape(*
_output_shapes

:*
use_locking(
¬
save/Assign_46Assignhead_step_fc/kernel/RMSPropsave/RestoreV2:46*
T0*&
_class
loc:@head_step_fc/kernel*
validate_shape(*
_output_shapes

:*
use_locking(
ƒ
save/Assign_47Assignhead_step_fc/kernel/RMSProp_1save/RestoreV2:47*
T0*&
_class
loc:@head_step_fc/kernel*
validate_shape(*
_output_shapes

:*
use_locking(
¬
save/Assign_48Assignhead_step_fc_setup_1/biassave/RestoreV2:48*
use_locking(*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
validate_shape(*
_output_shapes
:
 
save/Assign_49Assign!head_step_fc_setup_1/bias/RMSPropsave/RestoreV2:49*
use_locking(*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
validate_shape(*
_output_shapes
:
ћ
save/Assign_50Assign#head_step_fc_setup_1/bias/RMSProp_1save/RestoreV2:50*
use_locking(*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
validate_shape(*
_output_shapes
:
 
save/Assign_51Assignhead_step_fc_setup_1/kernelsave/RestoreV2:51*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
validate_shape(*
_output_shapes

:*
use_locking(
“
save/Assign_52Assign#head_step_fc_setup_1/kernel/RMSPropsave/RestoreV2:52*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
validate_shape(*
_output_shapes

:*
use_locking(
‘
save/Assign_53Assign%head_step_fc_setup_1/kernel/RMSProp_1save/RestoreV2:53*
validate_shape(*
_output_shapes

:*
use_locking(*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel
¶
save/Assign_54Assignoutput/biassave/RestoreV2:54*
use_locking(*
T0*
_class
loc:@output/bias*
validate_shape(*
_output_shapes
:
Ѓ
save/Assign_55Assignoutput/bias/RMSPropsave/RestoreV2:55*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@output/bias
∞
save/Assign_56Assignoutput/bias/RMSProp_1save/RestoreV2:56*
use_locking(*
T0*
_class
loc:@output/bias*
validate_shape(*
_output_shapes
:
Ѓ
save/Assign_57Assignoutput/kernelsave/RestoreV2:57*
use_locking(*
T0* 
_class
loc:@output/kernel*
validate_shape(*
_output_shapes

:

ґ
save/Assign_58Assignoutput/kernel/RMSPropsave/RestoreV2:58*
use_locking(*
T0* 
_class
loc:@output/kernel*
validate_shape(*
_output_shapes

:

Є
save/Assign_59Assignoutput/kernel/RMSProp_1save/RestoreV2:59*
T0* 
_class
loc:@output/kernel*
validate_shape(*
_output_shapes

:
*
use_locking(
 
save/Assign_60Assignvect_expand_dense_layer1/biassave/RestoreV2:60*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
validate_shape(*
_output_shapes
:d
“
save/Assign_61Assign%vect_expand_dense_layer1/bias/RMSPropsave/RestoreV2:61*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
validate_shape(*
_output_shapes
:d
‘
save/Assign_62Assign'vect_expand_dense_layer1/bias/RMSProp_1save/RestoreV2:62*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
validate_shape(*
_output_shapes
:d
“
save/Assign_63Assignvect_expand_dense_layer1/kernelsave/RestoreV2:63*
validate_shape(*
_output_shapes

:d*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel
Џ
save/Assign_64Assign'vect_expand_dense_layer1/kernel/RMSPropsave/RestoreV2:64*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
validate_shape(*
_output_shapes

:d*
use_locking(
№
save/Assign_65Assign)vect_expand_dense_layer1/kernel/RMSProp_1save/RestoreV2:65*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
validate_shape(*
_output_shapes

:d
 
save/Assign_66Assignvect_expand_dense_layer2/biassave/RestoreV2:66*
validate_shape(*
_output_shapes
:d*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias
“
save/Assign_67Assign%vect_expand_dense_layer2/bias/RMSPropsave/RestoreV2:67*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
validate_shape(*
_output_shapes
:d
‘
save/Assign_68Assign'vect_expand_dense_layer2/bias/RMSProp_1save/RestoreV2:68*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
validate_shape(*
_output_shapes
:d
“
save/Assign_69Assignvect_expand_dense_layer2/kernelsave/RestoreV2:69*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
validate_shape(*
_output_shapes

:dd
Џ
save/Assign_70Assign'vect_expand_dense_layer2/kernel/RMSPropsave/RestoreV2:70*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
validate_shape(*
_output_shapes

:dd
№
save/Assign_71Assign)vect_expand_dense_layer2/kernel/RMSProp_1save/RestoreV2:71*
validate_shape(*
_output_shapes

:dd*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel
‘	
save/restore_allNoOp^save/Assign^save/Assign_1^save/Assign_10^save/Assign_11^save/Assign_12^save/Assign_13^save/Assign_14^save/Assign_15^save/Assign_16^save/Assign_17^save/Assign_18^save/Assign_19^save/Assign_2^save/Assign_20^save/Assign_21^save/Assign_22^save/Assign_23^save/Assign_24^save/Assign_25^save/Assign_26^save/Assign_27^save/Assign_28^save/Assign_29^save/Assign_3^save/Assign_30^save/Assign_31^save/Assign_32^save/Assign_33^save/Assign_34^save/Assign_35^save/Assign_36^save/Assign_37^save/Assign_38^save/Assign_39^save/Assign_4^save/Assign_40^save/Assign_41^save/Assign_42^save/Assign_43^save/Assign_44^save/Assign_45^save/Assign_46^save/Assign_47^save/Assign_48^save/Assign_49^save/Assign_5^save/Assign_50^save/Assign_51^save/Assign_52^save/Assign_53^save/Assign_54^save/Assign_55^save/Assign_56^save/Assign_57^save/Assign_58^save/Assign_59^save/Assign_6^save/Assign_60^save/Assign_61^save/Assign_62^save/Assign_63^save/Assign_64^save/Assign_65^save/Assign_66^save/Assign_67^save/Assign_68^save/Assign_69^save/Assign_7^save/Assign_70^save/Assign_71^save/Assign_8^save/Assign_9
R
save_1/ConstConst*
valueB Bmodel*
dtype0*
_output_shapes
: 
и
save_1/SaveV2/tensor_namesConst*Щ
valueПBМHBcnn_outlayer/biasBcnn_outlayer/bias/RMSPropBcnn_outlayer/bias/RMSProp_1Bcnn_outlayer/kernelBcnn_outlayer/kernel/RMSPropBcnn_outlayer/kernel/RMSProp_1Bconv_layer1_conv/biasBconv_layer1_conv/bias/RMSPropBconv_layer1_conv/bias/RMSProp_1Bconv_layer1_conv/kernelBconv_layer1_conv/kernel/RMSPropB!conv_layer1_conv/kernel/RMSProp_1Bconv_layer2_conv/biasBconv_layer2_conv/bias/RMSPropBconv_layer2_conv/bias/RMSProp_1Bconv_layer2_conv/kernelBconv_layer2_conv/kernel/RMSPropB!conv_layer2_conv/kernel/RMSProp_1Bfinal_fc_l1/biasBfinal_fc_l1/bias/RMSPropBfinal_fc_l1/bias/RMSProp_1Bfinal_fc_l1/kernelBfinal_fc_l1/kernel/RMSPropBfinal_fc_l1/kernel/RMSProp_1Bfinal_fc_l2/biasBfinal_fc_l2/bias/RMSPropBfinal_fc_l2/bias/RMSProp_1Bfinal_fc_l2/kernelBfinal_fc_l2/kernel/RMSPropBfinal_fc_l2/kernel/RMSProp_1Bfinal_fc_l3/biasBfinal_fc_l3/bias/RMSPropBfinal_fc_l3/bias/RMSProp_1Bfinal_fc_l3/kernelBfinal_fc_l3/kernel/RMSPropBfinal_fc_l3/kernel/RMSProp_1Bfinal_fc_l4/biasBfinal_fc_l4/bias/RMSPropBfinal_fc_l4/bias/RMSProp_1Bfinal_fc_l4/kernelBfinal_fc_l4/kernel/RMSPropBfinal_fc_l4/kernel/RMSProp_1Bhead_step_fc/biasBhead_step_fc/bias/RMSPropBhead_step_fc/bias/RMSProp_1Bhead_step_fc/kernelBhead_step_fc/kernel/RMSPropBhead_step_fc/kernel/RMSProp_1Bhead_step_fc_setup_1/biasB!head_step_fc_setup_1/bias/RMSPropB#head_step_fc_setup_1/bias/RMSProp_1Bhead_step_fc_setup_1/kernelB#head_step_fc_setup_1/kernel/RMSPropB%head_step_fc_setup_1/kernel/RMSProp_1Boutput/biasBoutput/bias/RMSPropBoutput/bias/RMSProp_1Boutput/kernelBoutput/kernel/RMSPropBoutput/kernel/RMSProp_1Bvect_expand_dense_layer1/biasB%vect_expand_dense_layer1/bias/RMSPropB'vect_expand_dense_layer1/bias/RMSProp_1Bvect_expand_dense_layer1/kernelB'vect_expand_dense_layer1/kernel/RMSPropB)vect_expand_dense_layer1/kernel/RMSProp_1Bvect_expand_dense_layer2/biasB%vect_expand_dense_layer2/bias/RMSPropB'vect_expand_dense_layer2/bias/RMSProp_1Bvect_expand_dense_layer2/kernelB'vect_expand_dense_layer2/kernel/RMSPropB)vect_expand_dense_layer2/kernel/RMSProp_1*
dtype0*
_output_shapes
:H
ш
save_1/SaveV2/shape_and_slicesConst*•
valueЫBШHB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0*
_output_shapes
:H
љ
save_1/SaveV2SaveV2save_1/Constsave_1/SaveV2/tensor_namessave_1/SaveV2/shape_and_slicescnn_outlayer/biascnn_outlayer/bias/RMSPropcnn_outlayer/bias/RMSProp_1cnn_outlayer/kernelcnn_outlayer/kernel/RMSPropcnn_outlayer/kernel/RMSProp_1conv_layer1_conv/biasconv_layer1_conv/bias/RMSPropconv_layer1_conv/bias/RMSProp_1conv_layer1_conv/kernelconv_layer1_conv/kernel/RMSProp!conv_layer1_conv/kernel/RMSProp_1conv_layer2_conv/biasconv_layer2_conv/bias/RMSPropconv_layer2_conv/bias/RMSProp_1conv_layer2_conv/kernelconv_layer2_conv/kernel/RMSProp!conv_layer2_conv/kernel/RMSProp_1final_fc_l1/biasfinal_fc_l1/bias/RMSPropfinal_fc_l1/bias/RMSProp_1final_fc_l1/kernelfinal_fc_l1/kernel/RMSPropfinal_fc_l1/kernel/RMSProp_1final_fc_l2/biasfinal_fc_l2/bias/RMSPropfinal_fc_l2/bias/RMSProp_1final_fc_l2/kernelfinal_fc_l2/kernel/RMSPropfinal_fc_l2/kernel/RMSProp_1final_fc_l3/biasfinal_fc_l3/bias/RMSPropfinal_fc_l3/bias/RMSProp_1final_fc_l3/kernelfinal_fc_l3/kernel/RMSPropfinal_fc_l3/kernel/RMSProp_1final_fc_l4/biasfinal_fc_l4/bias/RMSPropfinal_fc_l4/bias/RMSProp_1final_fc_l4/kernelfinal_fc_l4/kernel/RMSPropfinal_fc_l4/kernel/RMSProp_1head_step_fc/biashead_step_fc/bias/RMSProphead_step_fc/bias/RMSProp_1head_step_fc/kernelhead_step_fc/kernel/RMSProphead_step_fc/kernel/RMSProp_1head_step_fc_setup_1/bias!head_step_fc_setup_1/bias/RMSProp#head_step_fc_setup_1/bias/RMSProp_1head_step_fc_setup_1/kernel#head_step_fc_setup_1/kernel/RMSProp%head_step_fc_setup_1/kernel/RMSProp_1output/biasoutput/bias/RMSPropoutput/bias/RMSProp_1output/kerneloutput/kernel/RMSPropoutput/kernel/RMSProp_1vect_expand_dense_layer1/bias%vect_expand_dense_layer1/bias/RMSProp'vect_expand_dense_layer1/bias/RMSProp_1vect_expand_dense_layer1/kernel'vect_expand_dense_layer1/kernel/RMSProp)vect_expand_dense_layer1/kernel/RMSProp_1vect_expand_dense_layer2/bias%vect_expand_dense_layer2/bias/RMSProp'vect_expand_dense_layer2/bias/RMSProp_1vect_expand_dense_layer2/kernel'vect_expand_dense_layer2/kernel/RMSProp)vect_expand_dense_layer2/kernel/RMSProp_1*V
dtypesL
J2H
Е
save_1/control_dependencyIdentitysave_1/Const^save_1/SaveV2*
_output_shapes
: *
T0*
_class
loc:@save_1/Const
л
save_1/RestoreV2/tensor_namesConst*Щ
valueПBМHBcnn_outlayer/biasBcnn_outlayer/bias/RMSPropBcnn_outlayer/bias/RMSProp_1Bcnn_outlayer/kernelBcnn_outlayer/kernel/RMSPropBcnn_outlayer/kernel/RMSProp_1Bconv_layer1_conv/biasBconv_layer1_conv/bias/RMSPropBconv_layer1_conv/bias/RMSProp_1Bconv_layer1_conv/kernelBconv_layer1_conv/kernel/RMSPropB!conv_layer1_conv/kernel/RMSProp_1Bconv_layer2_conv/biasBconv_layer2_conv/bias/RMSPropBconv_layer2_conv/bias/RMSProp_1Bconv_layer2_conv/kernelBconv_layer2_conv/kernel/RMSPropB!conv_layer2_conv/kernel/RMSProp_1Bfinal_fc_l1/biasBfinal_fc_l1/bias/RMSPropBfinal_fc_l1/bias/RMSProp_1Bfinal_fc_l1/kernelBfinal_fc_l1/kernel/RMSPropBfinal_fc_l1/kernel/RMSProp_1Bfinal_fc_l2/biasBfinal_fc_l2/bias/RMSPropBfinal_fc_l2/bias/RMSProp_1Bfinal_fc_l2/kernelBfinal_fc_l2/kernel/RMSPropBfinal_fc_l2/kernel/RMSProp_1Bfinal_fc_l3/biasBfinal_fc_l3/bias/RMSPropBfinal_fc_l3/bias/RMSProp_1Bfinal_fc_l3/kernelBfinal_fc_l3/kernel/RMSPropBfinal_fc_l3/kernel/RMSProp_1Bfinal_fc_l4/biasBfinal_fc_l4/bias/RMSPropBfinal_fc_l4/bias/RMSProp_1Bfinal_fc_l4/kernelBfinal_fc_l4/kernel/RMSPropBfinal_fc_l4/kernel/RMSProp_1Bhead_step_fc/biasBhead_step_fc/bias/RMSPropBhead_step_fc/bias/RMSProp_1Bhead_step_fc/kernelBhead_step_fc/kernel/RMSPropBhead_step_fc/kernel/RMSProp_1Bhead_step_fc_setup_1/biasB!head_step_fc_setup_1/bias/RMSPropB#head_step_fc_setup_1/bias/RMSProp_1Bhead_step_fc_setup_1/kernelB#head_step_fc_setup_1/kernel/RMSPropB%head_step_fc_setup_1/kernel/RMSProp_1Boutput/biasBoutput/bias/RMSPropBoutput/bias/RMSProp_1Boutput/kernelBoutput/kernel/RMSPropBoutput/kernel/RMSProp_1Bvect_expand_dense_layer1/biasB%vect_expand_dense_layer1/bias/RMSPropB'vect_expand_dense_layer1/bias/RMSProp_1Bvect_expand_dense_layer1/kernelB'vect_expand_dense_layer1/kernel/RMSPropB)vect_expand_dense_layer1/kernel/RMSProp_1Bvect_expand_dense_layer2/biasB%vect_expand_dense_layer2/bias/RMSPropB'vect_expand_dense_layer2/bias/RMSProp_1Bvect_expand_dense_layer2/kernelB'vect_expand_dense_layer2/kernel/RMSPropB)vect_expand_dense_layer2/kernel/RMSProp_1*
dtype0*
_output_shapes
:H
ы
!save_1/RestoreV2/shape_and_slicesConst*•
valueЫBШHB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0*
_output_shapes
:H
ю
save_1/RestoreV2	RestoreV2save_1/Constsave_1/RestoreV2/tensor_names!save_1/RestoreV2/shape_and_slices*V
dtypesL
J2H*ґ
_output_shapes£
†::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
∞
save_1/AssignAssigncnn_outlayer/biassave_1/RestoreV2*
validate_shape(*
_output_shapes
:d*
use_locking(*
T0*$
_class
loc:@cnn_outlayer/bias
Љ
save_1/Assign_1Assigncnn_outlayer/bias/RMSPropsave_1/RestoreV2:1*
validate_shape(*
_output_shapes
:d*
use_locking(*
T0*$
_class
loc:@cnn_outlayer/bias
Њ
save_1/Assign_2Assigncnn_outlayer/bias/RMSProp_1save_1/RestoreV2:2*
T0*$
_class
loc:@cnn_outlayer/bias*
validate_shape(*
_output_shapes
:d*
use_locking(
љ
save_1/Assign_3Assigncnn_outlayer/kernelsave_1/RestoreV2:3*
use_locking(*
T0*&
_class
loc:@cnn_outlayer/kernel*
validate_shape(*
_output_shapes
:	Сd
≈
save_1/Assign_4Assigncnn_outlayer/kernel/RMSPropsave_1/RestoreV2:4*
validate_shape(*
_output_shapes
:	Сd*
use_locking(*
T0*&
_class
loc:@cnn_outlayer/kernel
«
save_1/Assign_5Assigncnn_outlayer/kernel/RMSProp_1save_1/RestoreV2:5*
use_locking(*
T0*&
_class
loc:@cnn_outlayer/kernel*
validate_shape(*
_output_shapes
:	Сd
Љ
save_1/Assign_6Assignconv_layer1_conv/biassave_1/RestoreV2:6*
T0*(
_class
loc:@conv_layer1_conv/bias*
validate_shape(*
_output_shapes
: *
use_locking(
ƒ
save_1/Assign_7Assignconv_layer1_conv/bias/RMSPropsave_1/RestoreV2:7*
use_locking(*
T0*(
_class
loc:@conv_layer1_conv/bias*
validate_shape(*
_output_shapes
: 
∆
save_1/Assign_8Assignconv_layer1_conv/bias/RMSProp_1save_1/RestoreV2:8*
use_locking(*
T0*(
_class
loc:@conv_layer1_conv/bias*
validate_shape(*
_output_shapes
: 
ћ
save_1/Assign_9Assignconv_layer1_conv/kernelsave_1/RestoreV2:9*
T0**
_class 
loc:@conv_layer1_conv/kernel*
validate_shape(*&
_output_shapes
: *
use_locking(
÷
save_1/Assign_10Assignconv_layer1_conv/kernel/RMSPropsave_1/RestoreV2:10*
use_locking(*
T0**
_class 
loc:@conv_layer1_conv/kernel*
validate_shape(*&
_output_shapes
: 
Ў
save_1/Assign_11Assign!conv_layer1_conv/kernel/RMSProp_1save_1/RestoreV2:11*
T0**
_class 
loc:@conv_layer1_conv/kernel*
validate_shape(*&
_output_shapes
: *
use_locking(
Њ
save_1/Assign_12Assignconv_layer2_conv/biassave_1/RestoreV2:12*
use_locking(*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:
∆
save_1/Assign_13Assignconv_layer2_conv/bias/RMSPropsave_1/RestoreV2:13*
use_locking(*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:
»
save_1/Assign_14Assignconv_layer2_conv/bias/RMSProp_1save_1/RestoreV2:14*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:*
use_locking(
ќ
save_1/Assign_15Assignconv_layer2_conv/kernelsave_1/RestoreV2:15*
validate_shape(*&
_output_shapes
: *
use_locking(*
T0**
_class 
loc:@conv_layer2_conv/kernel
÷
save_1/Assign_16Assignconv_layer2_conv/kernel/RMSPropsave_1/RestoreV2:16*
use_locking(*
T0**
_class 
loc:@conv_layer2_conv/kernel*
validate_shape(*&
_output_shapes
: 
Ў
save_1/Assign_17Assign!conv_layer2_conv/kernel/RMSProp_1save_1/RestoreV2:17*
use_locking(*
T0**
_class 
loc:@conv_layer2_conv/kernel*
validate_shape(*&
_output_shapes
: 
і
save_1/Assign_18Assignfinal_fc_l1/biassave_1/RestoreV2:18*
validate_shape(*
_output_shapes
:2*
use_locking(*
T0*#
_class
loc:@final_fc_l1/bias
Љ
save_1/Assign_19Assignfinal_fc_l1/bias/RMSPropsave_1/RestoreV2:19*
use_locking(*
T0*#
_class
loc:@final_fc_l1/bias*
validate_shape(*
_output_shapes
:2
Њ
save_1/Assign_20Assignfinal_fc_l1/bias/RMSProp_1save_1/RestoreV2:20*
validate_shape(*
_output_shapes
:2*
use_locking(*
T0*#
_class
loc:@final_fc_l1/bias
љ
save_1/Assign_21Assignfinal_fc_l1/kernelsave_1/RestoreV2:21*
T0*%
_class
loc:@final_fc_l1/kernel*
validate_shape(*
_output_shapes
:	»2*
use_locking(
≈
save_1/Assign_22Assignfinal_fc_l1/kernel/RMSPropsave_1/RestoreV2:22*
use_locking(*
T0*%
_class
loc:@final_fc_l1/kernel*
validate_shape(*
_output_shapes
:	»2
«
save_1/Assign_23Assignfinal_fc_l1/kernel/RMSProp_1save_1/RestoreV2:23*
use_locking(*
T0*%
_class
loc:@final_fc_l1/kernel*
validate_shape(*
_output_shapes
:	»2
і
save_1/Assign_24Assignfinal_fc_l2/biassave_1/RestoreV2:24*
use_locking(*
T0*#
_class
loc:@final_fc_l2/bias*
validate_shape(*
_output_shapes
:(
Љ
save_1/Assign_25Assignfinal_fc_l2/bias/RMSPropsave_1/RestoreV2:25*
T0*#
_class
loc:@final_fc_l2/bias*
validate_shape(*
_output_shapes
:(*
use_locking(
Њ
save_1/Assign_26Assignfinal_fc_l2/bias/RMSProp_1save_1/RestoreV2:26*
validate_shape(*
_output_shapes
:(*
use_locking(*
T0*#
_class
loc:@final_fc_l2/bias
Љ
save_1/Assign_27Assignfinal_fc_l2/kernelsave_1/RestoreV2:27*
use_locking(*
T0*%
_class
loc:@final_fc_l2/kernel*
validate_shape(*
_output_shapes

:2(
ƒ
save_1/Assign_28Assignfinal_fc_l2/kernel/RMSPropsave_1/RestoreV2:28*
validate_shape(*
_output_shapes

:2(*
use_locking(*
T0*%
_class
loc:@final_fc_l2/kernel
∆
save_1/Assign_29Assignfinal_fc_l2/kernel/RMSProp_1save_1/RestoreV2:29*
use_locking(*
T0*%
_class
loc:@final_fc_l2/kernel*
validate_shape(*
_output_shapes

:2(
і
save_1/Assign_30Assignfinal_fc_l3/biassave_1/RestoreV2:30*
use_locking(*
T0*#
_class
loc:@final_fc_l3/bias*
validate_shape(*
_output_shapes
:

Љ
save_1/Assign_31Assignfinal_fc_l3/bias/RMSPropsave_1/RestoreV2:31*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*#
_class
loc:@final_fc_l3/bias
Њ
save_1/Assign_32Assignfinal_fc_l3/bias/RMSProp_1save_1/RestoreV2:32*
T0*#
_class
loc:@final_fc_l3/bias*
validate_shape(*
_output_shapes
:
*
use_locking(
Љ
save_1/Assign_33Assignfinal_fc_l3/kernelsave_1/RestoreV2:33*
T0*%
_class
loc:@final_fc_l3/kernel*
validate_shape(*
_output_shapes

:(
*
use_locking(
ƒ
save_1/Assign_34Assignfinal_fc_l3/kernel/RMSPropsave_1/RestoreV2:34*
use_locking(*
T0*%
_class
loc:@final_fc_l3/kernel*
validate_shape(*
_output_shapes

:(

∆
save_1/Assign_35Assignfinal_fc_l3/kernel/RMSProp_1save_1/RestoreV2:35*
T0*%
_class
loc:@final_fc_l3/kernel*
validate_shape(*
_output_shapes

:(
*
use_locking(
і
save_1/Assign_36Assignfinal_fc_l4/biassave_1/RestoreV2:36*
use_locking(*
T0*#
_class
loc:@final_fc_l4/bias*
validate_shape(*
_output_shapes
:

Љ
save_1/Assign_37Assignfinal_fc_l4/bias/RMSPropsave_1/RestoreV2:37*
T0*#
_class
loc:@final_fc_l4/bias*
validate_shape(*
_output_shapes
:
*
use_locking(
Њ
save_1/Assign_38Assignfinal_fc_l4/bias/RMSProp_1save_1/RestoreV2:38*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*#
_class
loc:@final_fc_l4/bias
Љ
save_1/Assign_39Assignfinal_fc_l4/kernelsave_1/RestoreV2:39*
use_locking(*
T0*%
_class
loc:@final_fc_l4/kernel*
validate_shape(*
_output_shapes

:


ƒ
save_1/Assign_40Assignfinal_fc_l4/kernel/RMSPropsave_1/RestoreV2:40*
validate_shape(*
_output_shapes

:

*
use_locking(*
T0*%
_class
loc:@final_fc_l4/kernel
∆
save_1/Assign_41Assignfinal_fc_l4/kernel/RMSProp_1save_1/RestoreV2:41*
validate_shape(*
_output_shapes

:

*
use_locking(*
T0*%
_class
loc:@final_fc_l4/kernel
ґ
save_1/Assign_42Assignhead_step_fc/biassave_1/RestoreV2:42*
use_locking(*
T0*$
_class
loc:@head_step_fc/bias*
validate_shape(*
_output_shapes
:
Њ
save_1/Assign_43Assignhead_step_fc/bias/RMSPropsave_1/RestoreV2:43*
T0*$
_class
loc:@head_step_fc/bias*
validate_shape(*
_output_shapes
:*
use_locking(
ј
save_1/Assign_44Assignhead_step_fc/bias/RMSProp_1save_1/RestoreV2:44*
use_locking(*
T0*$
_class
loc:@head_step_fc/bias*
validate_shape(*
_output_shapes
:
Њ
save_1/Assign_45Assignhead_step_fc/kernelsave_1/RestoreV2:45*
use_locking(*
T0*&
_class
loc:@head_step_fc/kernel*
validate_shape(*
_output_shapes

:
∆
save_1/Assign_46Assignhead_step_fc/kernel/RMSPropsave_1/RestoreV2:46*
T0*&
_class
loc:@head_step_fc/kernel*
validate_shape(*
_output_shapes

:*
use_locking(
»
save_1/Assign_47Assignhead_step_fc/kernel/RMSProp_1save_1/RestoreV2:47*
use_locking(*
T0*&
_class
loc:@head_step_fc/kernel*
validate_shape(*
_output_shapes

:
∆
save_1/Assign_48Assignhead_step_fc_setup_1/biassave_1/RestoreV2:48*
use_locking(*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
validate_shape(*
_output_shapes
:
ќ
save_1/Assign_49Assign!head_step_fc_setup_1/bias/RMSPropsave_1/RestoreV2:49*
use_locking(*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
validate_shape(*
_output_shapes
:
–
save_1/Assign_50Assign#head_step_fc_setup_1/bias/RMSProp_1save_1/RestoreV2:50*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias
ќ
save_1/Assign_51Assignhead_step_fc_setup_1/kernelsave_1/RestoreV2:51*
use_locking(*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
validate_shape(*
_output_shapes

:
÷
save_1/Assign_52Assign#head_step_fc_setup_1/kernel/RMSPropsave_1/RestoreV2:52*
validate_shape(*
_output_shapes

:*
use_locking(*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel
Ў
save_1/Assign_53Assign%head_step_fc_setup_1/kernel/RMSProp_1save_1/RestoreV2:53*
use_locking(*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
validate_shape(*
_output_shapes

:
™
save_1/Assign_54Assignoutput/biassave_1/RestoreV2:54*
use_locking(*
T0*
_class
loc:@output/bias*
validate_shape(*
_output_shapes
:
≤
save_1/Assign_55Assignoutput/bias/RMSPropsave_1/RestoreV2:55*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@output/bias
і
save_1/Assign_56Assignoutput/bias/RMSProp_1save_1/RestoreV2:56*
T0*
_class
loc:@output/bias*
validate_shape(*
_output_shapes
:*
use_locking(
≤
save_1/Assign_57Assignoutput/kernelsave_1/RestoreV2:57*
use_locking(*
T0* 
_class
loc:@output/kernel*
validate_shape(*
_output_shapes

:

Ї
save_1/Assign_58Assignoutput/kernel/RMSPropsave_1/RestoreV2:58*
use_locking(*
T0* 
_class
loc:@output/kernel*
validate_shape(*
_output_shapes

:

Љ
save_1/Assign_59Assignoutput/kernel/RMSProp_1save_1/RestoreV2:59*
use_locking(*
T0* 
_class
loc:@output/kernel*
validate_shape(*
_output_shapes

:

ќ
save_1/Assign_60Assignvect_expand_dense_layer1/biassave_1/RestoreV2:60*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
validate_shape(*
_output_shapes
:d
÷
save_1/Assign_61Assign%vect_expand_dense_layer1/bias/RMSPropsave_1/RestoreV2:61*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
validate_shape(*
_output_shapes
:d
Ў
save_1/Assign_62Assign'vect_expand_dense_layer1/bias/RMSProp_1save_1/RestoreV2:62*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
validate_shape(*
_output_shapes
:d
÷
save_1/Assign_63Assignvect_expand_dense_layer1/kernelsave_1/RestoreV2:63*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
validate_shape(*
_output_shapes

:d
ё
save_1/Assign_64Assign'vect_expand_dense_layer1/kernel/RMSPropsave_1/RestoreV2:64*
validate_shape(*
_output_shapes

:d*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel
а
save_1/Assign_65Assign)vect_expand_dense_layer1/kernel/RMSProp_1save_1/RestoreV2:65*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
validate_shape(*
_output_shapes

:d
ќ
save_1/Assign_66Assignvect_expand_dense_layer2/biassave_1/RestoreV2:66*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
validate_shape(*
_output_shapes
:d*
use_locking(
÷
save_1/Assign_67Assign%vect_expand_dense_layer2/bias/RMSPropsave_1/RestoreV2:67*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
validate_shape(*
_output_shapes
:d
Ў
save_1/Assign_68Assign'vect_expand_dense_layer2/bias/RMSProp_1save_1/RestoreV2:68*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
validate_shape(*
_output_shapes
:d
÷
save_1/Assign_69Assignvect_expand_dense_layer2/kernelsave_1/RestoreV2:69*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
validate_shape(*
_output_shapes

:dd*
use_locking(
ё
save_1/Assign_70Assign'vect_expand_dense_layer2/kernel/RMSPropsave_1/RestoreV2:70*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
validate_shape(*
_output_shapes

:dd
а
save_1/Assign_71Assign)vect_expand_dense_layer2/kernel/RMSProp_1save_1/RestoreV2:71*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
validate_shape(*
_output_shapes

:dd
ж

save_1/restore_allNoOp^save_1/Assign^save_1/Assign_1^save_1/Assign_10^save_1/Assign_11^save_1/Assign_12^save_1/Assign_13^save_1/Assign_14^save_1/Assign_15^save_1/Assign_16^save_1/Assign_17^save_1/Assign_18^save_1/Assign_19^save_1/Assign_2^save_1/Assign_20^save_1/Assign_21^save_1/Assign_22^save_1/Assign_23^save_1/Assign_24^save_1/Assign_25^save_1/Assign_26^save_1/Assign_27^save_1/Assign_28^save_1/Assign_29^save_1/Assign_3^save_1/Assign_30^save_1/Assign_31^save_1/Assign_32^save_1/Assign_33^save_1/Assign_34^save_1/Assign_35^save_1/Assign_36^save_1/Assign_37^save_1/Assign_38^save_1/Assign_39^save_1/Assign_4^save_1/Assign_40^save_1/Assign_41^save_1/Assign_42^save_1/Assign_43^save_1/Assign_44^save_1/Assign_45^save_1/Assign_46^save_1/Assign_47^save_1/Assign_48^save_1/Assign_49^save_1/Assign_5^save_1/Assign_50^save_1/Assign_51^save_1/Assign_52^save_1/Assign_53^save_1/Assign_54^save_1/Assign_55^save_1/Assign_56^save_1/Assign_57^save_1/Assign_58^save_1/Assign_59^save_1/Assign_6^save_1/Assign_60^save_1/Assign_61^save_1/Assign_62^save_1/Assign_63^save_1/Assign_64^save_1/Assign_65^save_1/Assign_66^save_1/Assign_67^save_1/Assign_68^save_1/Assign_69^save_1/Assign_7^save_1/Assign_70^save_1/Assign_71^save_1/Assign_8^save_1/Assign_9
R
save_2/ConstConst*
valueB Bmodel*
dtype0*
_output_shapes
: 
Ж
save_2/StringJoin/inputs_1Const*
dtype0*
_output_shapes
: *<
value3B1 B+_temp_561214f426e842d5bdac831f2b56eb40/part
{
save_2/StringJoin
StringJoinsave_2/Constsave_2/StringJoin/inputs_1*
	separator *
N*
_output_shapes
: 
S
save_2/num_shardsConst*
value	B :*
dtype0*
_output_shapes
: 
^
save_2/ShardedFilename/shardConst*
dtype0*
_output_shapes
: *
value	B : 
Е
save_2/ShardedFilenameShardedFilenamesave_2/StringJoinsave_2/ShardedFilename/shardsave_2/num_shards*
_output_shapes
: 
и
save_2/SaveV2/tensor_namesConst*Щ
valueПBМHBcnn_outlayer/biasBcnn_outlayer/bias/RMSPropBcnn_outlayer/bias/RMSProp_1Bcnn_outlayer/kernelBcnn_outlayer/kernel/RMSPropBcnn_outlayer/kernel/RMSProp_1Bconv_layer1_conv/biasBconv_layer1_conv/bias/RMSPropBconv_layer1_conv/bias/RMSProp_1Bconv_layer1_conv/kernelBconv_layer1_conv/kernel/RMSPropB!conv_layer1_conv/kernel/RMSProp_1Bconv_layer2_conv/biasBconv_layer2_conv/bias/RMSPropBconv_layer2_conv/bias/RMSProp_1Bconv_layer2_conv/kernelBconv_layer2_conv/kernel/RMSPropB!conv_layer2_conv/kernel/RMSProp_1Bfinal_fc_l1/biasBfinal_fc_l1/bias/RMSPropBfinal_fc_l1/bias/RMSProp_1Bfinal_fc_l1/kernelBfinal_fc_l1/kernel/RMSPropBfinal_fc_l1/kernel/RMSProp_1Bfinal_fc_l2/biasBfinal_fc_l2/bias/RMSPropBfinal_fc_l2/bias/RMSProp_1Bfinal_fc_l2/kernelBfinal_fc_l2/kernel/RMSPropBfinal_fc_l2/kernel/RMSProp_1Bfinal_fc_l3/biasBfinal_fc_l3/bias/RMSPropBfinal_fc_l3/bias/RMSProp_1Bfinal_fc_l3/kernelBfinal_fc_l3/kernel/RMSPropBfinal_fc_l3/kernel/RMSProp_1Bfinal_fc_l4/biasBfinal_fc_l4/bias/RMSPropBfinal_fc_l4/bias/RMSProp_1Bfinal_fc_l4/kernelBfinal_fc_l4/kernel/RMSPropBfinal_fc_l4/kernel/RMSProp_1Bhead_step_fc/biasBhead_step_fc/bias/RMSPropBhead_step_fc/bias/RMSProp_1Bhead_step_fc/kernelBhead_step_fc/kernel/RMSPropBhead_step_fc/kernel/RMSProp_1Bhead_step_fc_setup_1/biasB!head_step_fc_setup_1/bias/RMSPropB#head_step_fc_setup_1/bias/RMSProp_1Bhead_step_fc_setup_1/kernelB#head_step_fc_setup_1/kernel/RMSPropB%head_step_fc_setup_1/kernel/RMSProp_1Boutput/biasBoutput/bias/RMSPropBoutput/bias/RMSProp_1Boutput/kernelBoutput/kernel/RMSPropBoutput/kernel/RMSProp_1Bvect_expand_dense_layer1/biasB%vect_expand_dense_layer1/bias/RMSPropB'vect_expand_dense_layer1/bias/RMSProp_1Bvect_expand_dense_layer1/kernelB'vect_expand_dense_layer1/kernel/RMSPropB)vect_expand_dense_layer1/kernel/RMSProp_1Bvect_expand_dense_layer2/biasB%vect_expand_dense_layer2/bias/RMSPropB'vect_expand_dense_layer2/bias/RMSProp_1Bvect_expand_dense_layer2/kernelB'vect_expand_dense_layer2/kernel/RMSPropB)vect_expand_dense_layer2/kernel/RMSProp_1*
dtype0*
_output_shapes
:H
ш
save_2/SaveV2/shape_and_slicesConst*•
valueЫBШHB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0*
_output_shapes
:H
«
save_2/SaveV2SaveV2save_2/ShardedFilenamesave_2/SaveV2/tensor_namessave_2/SaveV2/shape_and_slicescnn_outlayer/biascnn_outlayer/bias/RMSPropcnn_outlayer/bias/RMSProp_1cnn_outlayer/kernelcnn_outlayer/kernel/RMSPropcnn_outlayer/kernel/RMSProp_1conv_layer1_conv/biasconv_layer1_conv/bias/RMSPropconv_layer1_conv/bias/RMSProp_1conv_layer1_conv/kernelconv_layer1_conv/kernel/RMSProp!conv_layer1_conv/kernel/RMSProp_1conv_layer2_conv/biasconv_layer2_conv/bias/RMSPropconv_layer2_conv/bias/RMSProp_1conv_layer2_conv/kernelconv_layer2_conv/kernel/RMSProp!conv_layer2_conv/kernel/RMSProp_1final_fc_l1/biasfinal_fc_l1/bias/RMSPropfinal_fc_l1/bias/RMSProp_1final_fc_l1/kernelfinal_fc_l1/kernel/RMSPropfinal_fc_l1/kernel/RMSProp_1final_fc_l2/biasfinal_fc_l2/bias/RMSPropfinal_fc_l2/bias/RMSProp_1final_fc_l2/kernelfinal_fc_l2/kernel/RMSPropfinal_fc_l2/kernel/RMSProp_1final_fc_l3/biasfinal_fc_l3/bias/RMSPropfinal_fc_l3/bias/RMSProp_1final_fc_l3/kernelfinal_fc_l3/kernel/RMSPropfinal_fc_l3/kernel/RMSProp_1final_fc_l4/biasfinal_fc_l4/bias/RMSPropfinal_fc_l4/bias/RMSProp_1final_fc_l4/kernelfinal_fc_l4/kernel/RMSPropfinal_fc_l4/kernel/RMSProp_1head_step_fc/biashead_step_fc/bias/RMSProphead_step_fc/bias/RMSProp_1head_step_fc/kernelhead_step_fc/kernel/RMSProphead_step_fc/kernel/RMSProp_1head_step_fc_setup_1/bias!head_step_fc_setup_1/bias/RMSProp#head_step_fc_setup_1/bias/RMSProp_1head_step_fc_setup_1/kernel#head_step_fc_setup_1/kernel/RMSProp%head_step_fc_setup_1/kernel/RMSProp_1output/biasoutput/bias/RMSPropoutput/bias/RMSProp_1output/kerneloutput/kernel/RMSPropoutput/kernel/RMSProp_1vect_expand_dense_layer1/bias%vect_expand_dense_layer1/bias/RMSProp'vect_expand_dense_layer1/bias/RMSProp_1vect_expand_dense_layer1/kernel'vect_expand_dense_layer1/kernel/RMSProp)vect_expand_dense_layer1/kernel/RMSProp_1vect_expand_dense_layer2/bias%vect_expand_dense_layer2/bias/RMSProp'vect_expand_dense_layer2/bias/RMSProp_1vect_expand_dense_layer2/kernel'vect_expand_dense_layer2/kernel/RMSProp)vect_expand_dense_layer2/kernel/RMSProp_1*V
dtypesL
J2H
Щ
save_2/control_dependencyIdentitysave_2/ShardedFilename^save_2/SaveV2*
T0*)
_class
loc:@save_2/ShardedFilename*
_output_shapes
: 
£
-save_2/MergeV2Checkpoints/checkpoint_prefixesPacksave_2/ShardedFilename^save_2/control_dependency*
T0*

axis *
N*
_output_shapes
:
Г
save_2/MergeV2CheckpointsMergeV2Checkpoints-save_2/MergeV2Checkpoints/checkpoint_prefixessave_2/Const*
delete_old_dirs(
В
save_2/IdentityIdentitysave_2/Const^save_2/MergeV2Checkpoints^save_2/control_dependency*
T0*
_output_shapes
: 
л
save_2/RestoreV2/tensor_namesConst*Щ
valueПBМHBcnn_outlayer/biasBcnn_outlayer/bias/RMSPropBcnn_outlayer/bias/RMSProp_1Bcnn_outlayer/kernelBcnn_outlayer/kernel/RMSPropBcnn_outlayer/kernel/RMSProp_1Bconv_layer1_conv/biasBconv_layer1_conv/bias/RMSPropBconv_layer1_conv/bias/RMSProp_1Bconv_layer1_conv/kernelBconv_layer1_conv/kernel/RMSPropB!conv_layer1_conv/kernel/RMSProp_1Bconv_layer2_conv/biasBconv_layer2_conv/bias/RMSPropBconv_layer2_conv/bias/RMSProp_1Bconv_layer2_conv/kernelBconv_layer2_conv/kernel/RMSPropB!conv_layer2_conv/kernel/RMSProp_1Bfinal_fc_l1/biasBfinal_fc_l1/bias/RMSPropBfinal_fc_l1/bias/RMSProp_1Bfinal_fc_l1/kernelBfinal_fc_l1/kernel/RMSPropBfinal_fc_l1/kernel/RMSProp_1Bfinal_fc_l2/biasBfinal_fc_l2/bias/RMSPropBfinal_fc_l2/bias/RMSProp_1Bfinal_fc_l2/kernelBfinal_fc_l2/kernel/RMSPropBfinal_fc_l2/kernel/RMSProp_1Bfinal_fc_l3/biasBfinal_fc_l3/bias/RMSPropBfinal_fc_l3/bias/RMSProp_1Bfinal_fc_l3/kernelBfinal_fc_l3/kernel/RMSPropBfinal_fc_l3/kernel/RMSProp_1Bfinal_fc_l4/biasBfinal_fc_l4/bias/RMSPropBfinal_fc_l4/bias/RMSProp_1Bfinal_fc_l4/kernelBfinal_fc_l4/kernel/RMSPropBfinal_fc_l4/kernel/RMSProp_1Bhead_step_fc/biasBhead_step_fc/bias/RMSPropBhead_step_fc/bias/RMSProp_1Bhead_step_fc/kernelBhead_step_fc/kernel/RMSPropBhead_step_fc/kernel/RMSProp_1Bhead_step_fc_setup_1/biasB!head_step_fc_setup_1/bias/RMSPropB#head_step_fc_setup_1/bias/RMSProp_1Bhead_step_fc_setup_1/kernelB#head_step_fc_setup_1/kernel/RMSPropB%head_step_fc_setup_1/kernel/RMSProp_1Boutput/biasBoutput/bias/RMSPropBoutput/bias/RMSProp_1Boutput/kernelBoutput/kernel/RMSPropBoutput/kernel/RMSProp_1Bvect_expand_dense_layer1/biasB%vect_expand_dense_layer1/bias/RMSPropB'vect_expand_dense_layer1/bias/RMSProp_1Bvect_expand_dense_layer1/kernelB'vect_expand_dense_layer1/kernel/RMSPropB)vect_expand_dense_layer1/kernel/RMSProp_1Bvect_expand_dense_layer2/biasB%vect_expand_dense_layer2/bias/RMSPropB'vect_expand_dense_layer2/bias/RMSProp_1Bvect_expand_dense_layer2/kernelB'vect_expand_dense_layer2/kernel/RMSPropB)vect_expand_dense_layer2/kernel/RMSProp_1*
dtype0*
_output_shapes
:H
ы
!save_2/RestoreV2/shape_and_slicesConst*
dtype0*
_output_shapes
:H*•
valueЫBШHB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 
ю
save_2/RestoreV2	RestoreV2save_2/Constsave_2/RestoreV2/tensor_names!save_2/RestoreV2/shape_and_slices*ґ
_output_shapes£
†::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*V
dtypesL
J2H
∞
save_2/AssignAssigncnn_outlayer/biassave_2/RestoreV2*
use_locking(*
T0*$
_class
loc:@cnn_outlayer/bias*
validate_shape(*
_output_shapes
:d
Љ
save_2/Assign_1Assigncnn_outlayer/bias/RMSPropsave_2/RestoreV2:1*
use_locking(*
T0*$
_class
loc:@cnn_outlayer/bias*
validate_shape(*
_output_shapes
:d
Њ
save_2/Assign_2Assigncnn_outlayer/bias/RMSProp_1save_2/RestoreV2:2*
validate_shape(*
_output_shapes
:d*
use_locking(*
T0*$
_class
loc:@cnn_outlayer/bias
љ
save_2/Assign_3Assigncnn_outlayer/kernelsave_2/RestoreV2:3*
use_locking(*
T0*&
_class
loc:@cnn_outlayer/kernel*
validate_shape(*
_output_shapes
:	Сd
≈
save_2/Assign_4Assigncnn_outlayer/kernel/RMSPropsave_2/RestoreV2:4*
T0*&
_class
loc:@cnn_outlayer/kernel*
validate_shape(*
_output_shapes
:	Сd*
use_locking(
«
save_2/Assign_5Assigncnn_outlayer/kernel/RMSProp_1save_2/RestoreV2:5*
use_locking(*
T0*&
_class
loc:@cnn_outlayer/kernel*
validate_shape(*
_output_shapes
:	Сd
Љ
save_2/Assign_6Assignconv_layer1_conv/biassave_2/RestoreV2:6*
use_locking(*
T0*(
_class
loc:@conv_layer1_conv/bias*
validate_shape(*
_output_shapes
: 
ƒ
save_2/Assign_7Assignconv_layer1_conv/bias/RMSPropsave_2/RestoreV2:7*
use_locking(*
T0*(
_class
loc:@conv_layer1_conv/bias*
validate_shape(*
_output_shapes
: 
∆
save_2/Assign_8Assignconv_layer1_conv/bias/RMSProp_1save_2/RestoreV2:8*
use_locking(*
T0*(
_class
loc:@conv_layer1_conv/bias*
validate_shape(*
_output_shapes
: 
ћ
save_2/Assign_9Assignconv_layer1_conv/kernelsave_2/RestoreV2:9*
use_locking(*
T0**
_class 
loc:@conv_layer1_conv/kernel*
validate_shape(*&
_output_shapes
: 
÷
save_2/Assign_10Assignconv_layer1_conv/kernel/RMSPropsave_2/RestoreV2:10*
T0**
_class 
loc:@conv_layer1_conv/kernel*
validate_shape(*&
_output_shapes
: *
use_locking(
Ў
save_2/Assign_11Assign!conv_layer1_conv/kernel/RMSProp_1save_2/RestoreV2:11*
use_locking(*
T0**
_class 
loc:@conv_layer1_conv/kernel*
validate_shape(*&
_output_shapes
: 
Њ
save_2/Assign_12Assignconv_layer2_conv/biassave_2/RestoreV2:12*
use_locking(*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:
∆
save_2/Assign_13Assignconv_layer2_conv/bias/RMSPropsave_2/RestoreV2:13*
use_locking(*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:
»
save_2/Assign_14Assignconv_layer2_conv/bias/RMSProp_1save_2/RestoreV2:14*
use_locking(*
T0*(
_class
loc:@conv_layer2_conv/bias*
validate_shape(*
_output_shapes
:
ќ
save_2/Assign_15Assignconv_layer2_conv/kernelsave_2/RestoreV2:15*
T0**
_class 
loc:@conv_layer2_conv/kernel*
validate_shape(*&
_output_shapes
: *
use_locking(
÷
save_2/Assign_16Assignconv_layer2_conv/kernel/RMSPropsave_2/RestoreV2:16*
T0**
_class 
loc:@conv_layer2_conv/kernel*
validate_shape(*&
_output_shapes
: *
use_locking(
Ў
save_2/Assign_17Assign!conv_layer2_conv/kernel/RMSProp_1save_2/RestoreV2:17*
validate_shape(*&
_output_shapes
: *
use_locking(*
T0**
_class 
loc:@conv_layer2_conv/kernel
і
save_2/Assign_18Assignfinal_fc_l1/biassave_2/RestoreV2:18*
T0*#
_class
loc:@final_fc_l1/bias*
validate_shape(*
_output_shapes
:2*
use_locking(
Љ
save_2/Assign_19Assignfinal_fc_l1/bias/RMSPropsave_2/RestoreV2:19*
use_locking(*
T0*#
_class
loc:@final_fc_l1/bias*
validate_shape(*
_output_shapes
:2
Њ
save_2/Assign_20Assignfinal_fc_l1/bias/RMSProp_1save_2/RestoreV2:20*
T0*#
_class
loc:@final_fc_l1/bias*
validate_shape(*
_output_shapes
:2*
use_locking(
љ
save_2/Assign_21Assignfinal_fc_l1/kernelsave_2/RestoreV2:21*
T0*%
_class
loc:@final_fc_l1/kernel*
validate_shape(*
_output_shapes
:	»2*
use_locking(
≈
save_2/Assign_22Assignfinal_fc_l1/kernel/RMSPropsave_2/RestoreV2:22*
use_locking(*
T0*%
_class
loc:@final_fc_l1/kernel*
validate_shape(*
_output_shapes
:	»2
«
save_2/Assign_23Assignfinal_fc_l1/kernel/RMSProp_1save_2/RestoreV2:23*
validate_shape(*
_output_shapes
:	»2*
use_locking(*
T0*%
_class
loc:@final_fc_l1/kernel
і
save_2/Assign_24Assignfinal_fc_l2/biassave_2/RestoreV2:24*
use_locking(*
T0*#
_class
loc:@final_fc_l2/bias*
validate_shape(*
_output_shapes
:(
Љ
save_2/Assign_25Assignfinal_fc_l2/bias/RMSPropsave_2/RestoreV2:25*
T0*#
_class
loc:@final_fc_l2/bias*
validate_shape(*
_output_shapes
:(*
use_locking(
Њ
save_2/Assign_26Assignfinal_fc_l2/bias/RMSProp_1save_2/RestoreV2:26*
use_locking(*
T0*#
_class
loc:@final_fc_l2/bias*
validate_shape(*
_output_shapes
:(
Љ
save_2/Assign_27Assignfinal_fc_l2/kernelsave_2/RestoreV2:27*
validate_shape(*
_output_shapes

:2(*
use_locking(*
T0*%
_class
loc:@final_fc_l2/kernel
ƒ
save_2/Assign_28Assignfinal_fc_l2/kernel/RMSPropsave_2/RestoreV2:28*
validate_shape(*
_output_shapes

:2(*
use_locking(*
T0*%
_class
loc:@final_fc_l2/kernel
∆
save_2/Assign_29Assignfinal_fc_l2/kernel/RMSProp_1save_2/RestoreV2:29*
use_locking(*
T0*%
_class
loc:@final_fc_l2/kernel*
validate_shape(*
_output_shapes

:2(
і
save_2/Assign_30Assignfinal_fc_l3/biassave_2/RestoreV2:30*
use_locking(*
T0*#
_class
loc:@final_fc_l3/bias*
validate_shape(*
_output_shapes
:

Љ
save_2/Assign_31Assignfinal_fc_l3/bias/RMSPropsave_2/RestoreV2:31*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*#
_class
loc:@final_fc_l3/bias
Њ
save_2/Assign_32Assignfinal_fc_l3/bias/RMSProp_1save_2/RestoreV2:32*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*#
_class
loc:@final_fc_l3/bias
Љ
save_2/Assign_33Assignfinal_fc_l3/kernelsave_2/RestoreV2:33*
use_locking(*
T0*%
_class
loc:@final_fc_l3/kernel*
validate_shape(*
_output_shapes

:(

ƒ
save_2/Assign_34Assignfinal_fc_l3/kernel/RMSPropsave_2/RestoreV2:34*
validate_shape(*
_output_shapes

:(
*
use_locking(*
T0*%
_class
loc:@final_fc_l3/kernel
∆
save_2/Assign_35Assignfinal_fc_l3/kernel/RMSProp_1save_2/RestoreV2:35*
T0*%
_class
loc:@final_fc_l3/kernel*
validate_shape(*
_output_shapes

:(
*
use_locking(
і
save_2/Assign_36Assignfinal_fc_l4/biassave_2/RestoreV2:36*
use_locking(*
T0*#
_class
loc:@final_fc_l4/bias*
validate_shape(*
_output_shapes
:

Љ
save_2/Assign_37Assignfinal_fc_l4/bias/RMSPropsave_2/RestoreV2:37*
use_locking(*
T0*#
_class
loc:@final_fc_l4/bias*
validate_shape(*
_output_shapes
:

Њ
save_2/Assign_38Assignfinal_fc_l4/bias/RMSProp_1save_2/RestoreV2:38*
use_locking(*
T0*#
_class
loc:@final_fc_l4/bias*
validate_shape(*
_output_shapes
:

Љ
save_2/Assign_39Assignfinal_fc_l4/kernelsave_2/RestoreV2:39*
use_locking(*
T0*%
_class
loc:@final_fc_l4/kernel*
validate_shape(*
_output_shapes

:


ƒ
save_2/Assign_40Assignfinal_fc_l4/kernel/RMSPropsave_2/RestoreV2:40*
use_locking(*
T0*%
_class
loc:@final_fc_l4/kernel*
validate_shape(*
_output_shapes

:


∆
save_2/Assign_41Assignfinal_fc_l4/kernel/RMSProp_1save_2/RestoreV2:41*
use_locking(*
T0*%
_class
loc:@final_fc_l4/kernel*
validate_shape(*
_output_shapes

:


ґ
save_2/Assign_42Assignhead_step_fc/biassave_2/RestoreV2:42*
use_locking(*
T0*$
_class
loc:@head_step_fc/bias*
validate_shape(*
_output_shapes
:
Њ
save_2/Assign_43Assignhead_step_fc/bias/RMSPropsave_2/RestoreV2:43*
T0*$
_class
loc:@head_step_fc/bias*
validate_shape(*
_output_shapes
:*
use_locking(
ј
save_2/Assign_44Assignhead_step_fc/bias/RMSProp_1save_2/RestoreV2:44*
T0*$
_class
loc:@head_step_fc/bias*
validate_shape(*
_output_shapes
:*
use_locking(
Њ
save_2/Assign_45Assignhead_step_fc/kernelsave_2/RestoreV2:45*
validate_shape(*
_output_shapes

:*
use_locking(*
T0*&
_class
loc:@head_step_fc/kernel
∆
save_2/Assign_46Assignhead_step_fc/kernel/RMSPropsave_2/RestoreV2:46*
use_locking(*
T0*&
_class
loc:@head_step_fc/kernel*
validate_shape(*
_output_shapes

:
»
save_2/Assign_47Assignhead_step_fc/kernel/RMSProp_1save_2/RestoreV2:47*
use_locking(*
T0*&
_class
loc:@head_step_fc/kernel*
validate_shape(*
_output_shapes

:
∆
save_2/Assign_48Assignhead_step_fc_setup_1/biassave_2/RestoreV2:48*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
validate_shape(*
_output_shapes
:*
use_locking(
ќ
save_2/Assign_49Assign!head_step_fc_setup_1/bias/RMSPropsave_2/RestoreV2:49*
use_locking(*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias*
validate_shape(*
_output_shapes
:
–
save_2/Assign_50Assign#head_step_fc_setup_1/bias/RMSProp_1save_2/RestoreV2:50*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*,
_class"
 loc:@head_step_fc_setup_1/bias
ќ
save_2/Assign_51Assignhead_step_fc_setup_1/kernelsave_2/RestoreV2:51*
use_locking(*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
validate_shape(*
_output_shapes

:
÷
save_2/Assign_52Assign#head_step_fc_setup_1/kernel/RMSPropsave_2/RestoreV2:52*
use_locking(*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
validate_shape(*
_output_shapes

:
Ў
save_2/Assign_53Assign%head_step_fc_setup_1/kernel/RMSProp_1save_2/RestoreV2:53*
T0*.
_class$
" loc:@head_step_fc_setup_1/kernel*
validate_shape(*
_output_shapes

:*
use_locking(
™
save_2/Assign_54Assignoutput/biassave_2/RestoreV2:54*
use_locking(*
T0*
_class
loc:@output/bias*
validate_shape(*
_output_shapes
:
≤
save_2/Assign_55Assignoutput/bias/RMSPropsave_2/RestoreV2:55*
use_locking(*
T0*
_class
loc:@output/bias*
validate_shape(*
_output_shapes
:
і
save_2/Assign_56Assignoutput/bias/RMSProp_1save_2/RestoreV2:56*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@output/bias
≤
save_2/Assign_57Assignoutput/kernelsave_2/RestoreV2:57*
use_locking(*
T0* 
_class
loc:@output/kernel*
validate_shape(*
_output_shapes

:

Ї
save_2/Assign_58Assignoutput/kernel/RMSPropsave_2/RestoreV2:58*
validate_shape(*
_output_shapes

:
*
use_locking(*
T0* 
_class
loc:@output/kernel
Љ
save_2/Assign_59Assignoutput/kernel/RMSProp_1save_2/RestoreV2:59*
use_locking(*
T0* 
_class
loc:@output/kernel*
validate_shape(*
_output_shapes

:

ќ
save_2/Assign_60Assignvect_expand_dense_layer1/biassave_2/RestoreV2:60*
validate_shape(*
_output_shapes
:d*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias
÷
save_2/Assign_61Assign%vect_expand_dense_layer1/bias/RMSPropsave_2/RestoreV2:61*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
validate_shape(*
_output_shapes
:d
Ў
save_2/Assign_62Assign'vect_expand_dense_layer1/bias/RMSProp_1save_2/RestoreV2:62*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer1/bias*
validate_shape(*
_output_shapes
:d
÷
save_2/Assign_63Assignvect_expand_dense_layer1/kernelsave_2/RestoreV2:63*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
validate_shape(*
_output_shapes

:d
ё
save_2/Assign_64Assign'vect_expand_dense_layer1/kernel/RMSPropsave_2/RestoreV2:64*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
validate_shape(*
_output_shapes

:d*
use_locking(
а
save_2/Assign_65Assign)vect_expand_dense_layer1/kernel/RMSProp_1save_2/RestoreV2:65*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer1/kernel*
validate_shape(*
_output_shapes

:d
ќ
save_2/Assign_66Assignvect_expand_dense_layer2/biassave_2/RestoreV2:66*
use_locking(*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
validate_shape(*
_output_shapes
:d
÷
save_2/Assign_67Assign%vect_expand_dense_layer2/bias/RMSPropsave_2/RestoreV2:67*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
validate_shape(*
_output_shapes
:d*
use_locking(
Ў
save_2/Assign_68Assign'vect_expand_dense_layer2/bias/RMSProp_1save_2/RestoreV2:68*
T0*0
_class&
$"loc:@vect_expand_dense_layer2/bias*
validate_shape(*
_output_shapes
:d*
use_locking(
÷
save_2/Assign_69Assignvect_expand_dense_layer2/kernelsave_2/RestoreV2:69*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
validate_shape(*
_output_shapes

:dd*
use_locking(
ё
save_2/Assign_70Assign'vect_expand_dense_layer2/kernel/RMSPropsave_2/RestoreV2:70*
use_locking(*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
validate_shape(*
_output_shapes

:dd
а
save_2/Assign_71Assign)vect_expand_dense_layer2/kernel/RMSProp_1save_2/RestoreV2:71*
T0*2
_class(
&$loc:@vect_expand_dense_layer2/kernel*
validate_shape(*
_output_shapes

:dd*
use_locking(
и

save_2/restore_shardNoOp^save_2/Assign^save_2/Assign_1^save_2/Assign_10^save_2/Assign_11^save_2/Assign_12^save_2/Assign_13^save_2/Assign_14^save_2/Assign_15^save_2/Assign_16^save_2/Assign_17^save_2/Assign_18^save_2/Assign_19^save_2/Assign_2^save_2/Assign_20^save_2/Assign_21^save_2/Assign_22^save_2/Assign_23^save_2/Assign_24^save_2/Assign_25^save_2/Assign_26^save_2/Assign_27^save_2/Assign_28^save_2/Assign_29^save_2/Assign_3^save_2/Assign_30^save_2/Assign_31^save_2/Assign_32^save_2/Assign_33^save_2/Assign_34^save_2/Assign_35^save_2/Assign_36^save_2/Assign_37^save_2/Assign_38^save_2/Assign_39^save_2/Assign_4^save_2/Assign_40^save_2/Assign_41^save_2/Assign_42^save_2/Assign_43^save_2/Assign_44^save_2/Assign_45^save_2/Assign_46^save_2/Assign_47^save_2/Assign_48^save_2/Assign_49^save_2/Assign_5^save_2/Assign_50^save_2/Assign_51^save_2/Assign_52^save_2/Assign_53^save_2/Assign_54^save_2/Assign_55^save_2/Assign_56^save_2/Assign_57^save_2/Assign_58^save_2/Assign_59^save_2/Assign_6^save_2/Assign_60^save_2/Assign_61^save_2/Assign_62^save_2/Assign_63^save_2/Assign_64^save_2/Assign_65^save_2/Assign_66^save_2/Assign_67^save_2/Assign_68^save_2/Assign_69^save_2/Assign_7^save_2/Assign_70^save_2/Assign_71^save_2/Assign_8^save_2/Assign_9
1
save_2/restore_allNoOp^save_2/restore_shard "B
save_2/Const:0save_2/Identity:0save_2/restore_all (5 @F8"
train_op

train/RMSProp"√
trainable_variablesЂ®
У
conv_layer1_conv/kernel:0conv_layer1_conv/kernel/Assignconv_layer1_conv/kernel/read:024conv_layer1_conv/kernel/Initializer/random_uniform:08
В
conv_layer1_conv/bias:0conv_layer1_conv/bias/Assignconv_layer1_conv/bias/read:02)conv_layer1_conv/bias/Initializer/zeros:08
У
conv_layer2_conv/kernel:0conv_layer2_conv/kernel/Assignconv_layer2_conv/kernel/read:024conv_layer2_conv/kernel/Initializer/random_uniform:08
В
conv_layer2_conv/bias:0conv_layer2_conv/bias/Assignconv_layer2_conv/bias/read:02)conv_layer2_conv/bias/Initializer/zeros:08
Г
cnn_outlayer/kernel:0cnn_outlayer/kernel/Assigncnn_outlayer/kernel/read:020cnn_outlayer/kernel/Initializer/random_uniform:08
r
cnn_outlayer/bias:0cnn_outlayer/bias/Assigncnn_outlayer/bias/read:02%cnn_outlayer/bias/Initializer/zeros:08
£
head_step_fc_setup_1/kernel:0"head_step_fc_setup_1/kernel/Assign"head_step_fc_setup_1/kernel/read:028head_step_fc_setup_1/kernel/Initializer/random_uniform:08
Т
head_step_fc_setup_1/bias:0 head_step_fc_setup_1/bias/Assign head_step_fc_setup_1/bias/read:02-head_step_fc_setup_1/bias/Initializer/zeros:08
Г
head_step_fc/kernel:0head_step_fc/kernel/Assignhead_step_fc/kernel/read:020head_step_fc/kernel/Initializer/random_uniform:08
r
head_step_fc/bias:0head_step_fc/bias/Assignhead_step_fc/bias/read:02%head_step_fc/bias/Initializer/zeros:08
≥
!vect_expand_dense_layer1/kernel:0&vect_expand_dense_layer1/kernel/Assign&vect_expand_dense_layer1/kernel/read:02<vect_expand_dense_layer1/kernel/Initializer/random_uniform:08
Ґ
vect_expand_dense_layer1/bias:0$vect_expand_dense_layer1/bias/Assign$vect_expand_dense_layer1/bias/read:021vect_expand_dense_layer1/bias/Initializer/zeros:08
≥
!vect_expand_dense_layer2/kernel:0&vect_expand_dense_layer2/kernel/Assign&vect_expand_dense_layer2/kernel/read:02<vect_expand_dense_layer2/kernel/Initializer/random_uniform:08
Ґ
vect_expand_dense_layer2/bias:0$vect_expand_dense_layer2/bias/Assign$vect_expand_dense_layer2/bias/read:021vect_expand_dense_layer2/bias/Initializer/zeros:08

final_fc_l1/kernel:0final_fc_l1/kernel/Assignfinal_fc_l1/kernel/read:02/final_fc_l1/kernel/Initializer/random_uniform:08
n
final_fc_l1/bias:0final_fc_l1/bias/Assignfinal_fc_l1/bias/read:02$final_fc_l1/bias/Initializer/zeros:08

final_fc_l2/kernel:0final_fc_l2/kernel/Assignfinal_fc_l2/kernel/read:02/final_fc_l2/kernel/Initializer/random_uniform:08
n
final_fc_l2/bias:0final_fc_l2/bias/Assignfinal_fc_l2/bias/read:02$final_fc_l2/bias/Initializer/zeros:08

final_fc_l3/kernel:0final_fc_l3/kernel/Assignfinal_fc_l3/kernel/read:02/final_fc_l3/kernel/Initializer/random_uniform:08
n
final_fc_l3/bias:0final_fc_l3/bias/Assignfinal_fc_l3/bias/read:02$final_fc_l3/bias/Initializer/zeros:08

final_fc_l4/kernel:0final_fc_l4/kernel/Assignfinal_fc_l4/kernel/read:02/final_fc_l4/kernel/Initializer/random_uniform:08
n
final_fc_l4/bias:0final_fc_l4/bias/Assignfinal_fc_l4/bias/read:02$final_fc_l4/bias/Initializer/zeros:08
k
output/kernel:0output/kernel/Assignoutput/kernel/read:02*output/kernel/Initializer/random_uniform:08
Z
output/bias:0output/bias/Assignoutput/bias/read:02output/bias/Initializer/zeros:08"Е
trainы
ш
conv_layer1_activations:0
conv_layer2_activations:0
conv_layer1_conv/kernel_0:0
conv_layer1_conv/bias_0:0
conv_layer2_conv/kernel_0:0
conv_layer2_conv/bias_0:0
cnn_outlayer/kernel_0:0
cnn_outlayer/bias_0:0
head_step_fc_setup_1/kernel_0:0
head_step_fc_setup_1/bias_0:0
head_step_fc/kernel_0:0
head_step_fc/bias_0:0
#vect_expand_dense_layer1/kernel_0:0
!vect_expand_dense_layer1/bias_0:0
#vect_expand_dense_layer2/kernel_0:0
!vect_expand_dense_layer2/bias_0:0
final_fc_l1/kernel_0:0
final_fc_l1/bias_0:0
final_fc_l2/kernel_0:0
final_fc_l2/bias_0:0
final_fc_l3/kernel_0:0
final_fc_l3/bias_0:0
final_fc_l4/kernel_0:0
final_fc_l4/bias_0:0
output/kernel_0:0
output/bias_0:0
error_total:0
error_xy_sqrd_out:0
error_th_sqrd_out:0
input_img:0"C
test;
9
error_total:0
error_xy_sqrd_out:0
error_th_sqrd_out:0"ƒ

train_histµ
≤
conv_layer1_activations:0
conv_layer2_activations:0
conv_layer1_conv/kernel_0:0
conv_layer1_conv/bias_0:0
conv_layer2_conv/kernel_0:0
conv_layer2_conv/bias_0:0
cnn_outlayer/kernel_0:0
cnn_outlayer/bias_0:0
head_step_fc_setup_1/kernel_0:0
head_step_fc_setup_1/bias_0:0
head_step_fc/kernel_0:0
head_step_fc/bias_0:0
#vect_expand_dense_layer1/kernel_0:0
!vect_expand_dense_layer1/bias_0:0
#vect_expand_dense_layer2/kernel_0:0
!vect_expand_dense_layer2/bias_0:0
final_fc_l1/kernel_0:0
final_fc_l1/bias_0:0
final_fc_l2/kernel_0:0
final_fc_l2/bias_0:0
final_fc_l3/kernel_0:0
final_fc_l3/bias_0:0
final_fc_l4/kernel_0:0
final_fc_l4/bias_0:0
output/kernel_0:0
output/bias_0:0"ПW
	variablesБWюV
У
conv_layer1_conv/kernel:0conv_layer1_conv/kernel/Assignconv_layer1_conv/kernel/read:024conv_layer1_conv/kernel/Initializer/random_uniform:08
В
conv_layer1_conv/bias:0conv_layer1_conv/bias/Assignconv_layer1_conv/bias/read:02)conv_layer1_conv/bias/Initializer/zeros:08
У
conv_layer2_conv/kernel:0conv_layer2_conv/kernel/Assignconv_layer2_conv/kernel/read:024conv_layer2_conv/kernel/Initializer/random_uniform:08
В
conv_layer2_conv/bias:0conv_layer2_conv/bias/Assignconv_layer2_conv/bias/read:02)conv_layer2_conv/bias/Initializer/zeros:08
Г
cnn_outlayer/kernel:0cnn_outlayer/kernel/Assigncnn_outlayer/kernel/read:020cnn_outlayer/kernel/Initializer/random_uniform:08
r
cnn_outlayer/bias:0cnn_outlayer/bias/Assigncnn_outlayer/bias/read:02%cnn_outlayer/bias/Initializer/zeros:08
£
head_step_fc_setup_1/kernel:0"head_step_fc_setup_1/kernel/Assign"head_step_fc_setup_1/kernel/read:028head_step_fc_setup_1/kernel/Initializer/random_uniform:08
Т
head_step_fc_setup_1/bias:0 head_step_fc_setup_1/bias/Assign head_step_fc_setup_1/bias/read:02-head_step_fc_setup_1/bias/Initializer/zeros:08
Г
head_step_fc/kernel:0head_step_fc/kernel/Assignhead_step_fc/kernel/read:020head_step_fc/kernel/Initializer/random_uniform:08
r
head_step_fc/bias:0head_step_fc/bias/Assignhead_step_fc/bias/read:02%head_step_fc/bias/Initializer/zeros:08
≥
!vect_expand_dense_layer1/kernel:0&vect_expand_dense_layer1/kernel/Assign&vect_expand_dense_layer1/kernel/read:02<vect_expand_dense_layer1/kernel/Initializer/random_uniform:08
Ґ
vect_expand_dense_layer1/bias:0$vect_expand_dense_layer1/bias/Assign$vect_expand_dense_layer1/bias/read:021vect_expand_dense_layer1/bias/Initializer/zeros:08
≥
!vect_expand_dense_layer2/kernel:0&vect_expand_dense_layer2/kernel/Assign&vect_expand_dense_layer2/kernel/read:02<vect_expand_dense_layer2/kernel/Initializer/random_uniform:08
Ґ
vect_expand_dense_layer2/bias:0$vect_expand_dense_layer2/bias/Assign$vect_expand_dense_layer2/bias/read:021vect_expand_dense_layer2/bias/Initializer/zeros:08

final_fc_l1/kernel:0final_fc_l1/kernel/Assignfinal_fc_l1/kernel/read:02/final_fc_l1/kernel/Initializer/random_uniform:08
n
final_fc_l1/bias:0final_fc_l1/bias/Assignfinal_fc_l1/bias/read:02$final_fc_l1/bias/Initializer/zeros:08

final_fc_l2/kernel:0final_fc_l2/kernel/Assignfinal_fc_l2/kernel/read:02/final_fc_l2/kernel/Initializer/random_uniform:08
n
final_fc_l2/bias:0final_fc_l2/bias/Assignfinal_fc_l2/bias/read:02$final_fc_l2/bias/Initializer/zeros:08

final_fc_l3/kernel:0final_fc_l3/kernel/Assignfinal_fc_l3/kernel/read:02/final_fc_l3/kernel/Initializer/random_uniform:08
n
final_fc_l3/bias:0final_fc_l3/bias/Assignfinal_fc_l3/bias/read:02$final_fc_l3/bias/Initializer/zeros:08

final_fc_l4/kernel:0final_fc_l4/kernel/Assignfinal_fc_l4/kernel/read:02/final_fc_l4/kernel/Initializer/random_uniform:08
n
final_fc_l4/bias:0final_fc_l4/bias/Assignfinal_fc_l4/bias/read:02$final_fc_l4/bias/Initializer/zeros:08
k
output/kernel:0output/kernel/Assignoutput/kernel/read:02*output/kernel/Initializer/random_uniform:08
Z
output/bias:0output/bias/Assignoutput/bias/read:02output/bias/Initializer/zeros:08
І
!conv_layer1_conv/kernel/RMSProp:0&conv_layer1_conv/kernel/RMSProp/Assign&conv_layer1_conv/kernel/RMSProp/read:022conv_layer1_conv/kernel/RMSProp/Initializer/ones:0
∞
#conv_layer1_conv/kernel/RMSProp_1:0(conv_layer1_conv/kernel/RMSProp_1/Assign(conv_layer1_conv/kernel/RMSProp_1/read:025conv_layer1_conv/kernel/RMSProp_1/Initializer/zeros:0
Я
conv_layer1_conv/bias/RMSProp:0$conv_layer1_conv/bias/RMSProp/Assign$conv_layer1_conv/bias/RMSProp/read:020conv_layer1_conv/bias/RMSProp/Initializer/ones:0
®
!conv_layer1_conv/bias/RMSProp_1:0&conv_layer1_conv/bias/RMSProp_1/Assign&conv_layer1_conv/bias/RMSProp_1/read:023conv_layer1_conv/bias/RMSProp_1/Initializer/zeros:0
І
!conv_layer2_conv/kernel/RMSProp:0&conv_layer2_conv/kernel/RMSProp/Assign&conv_layer2_conv/kernel/RMSProp/read:022conv_layer2_conv/kernel/RMSProp/Initializer/ones:0
∞
#conv_layer2_conv/kernel/RMSProp_1:0(conv_layer2_conv/kernel/RMSProp_1/Assign(conv_layer2_conv/kernel/RMSProp_1/read:025conv_layer2_conv/kernel/RMSProp_1/Initializer/zeros:0
Я
conv_layer2_conv/bias/RMSProp:0$conv_layer2_conv/bias/RMSProp/Assign$conv_layer2_conv/bias/RMSProp/read:020conv_layer2_conv/bias/RMSProp/Initializer/ones:0
®
!conv_layer2_conv/bias/RMSProp_1:0&conv_layer2_conv/bias/RMSProp_1/Assign&conv_layer2_conv/bias/RMSProp_1/read:023conv_layer2_conv/bias/RMSProp_1/Initializer/zeros:0
Ч
cnn_outlayer/kernel/RMSProp:0"cnn_outlayer/kernel/RMSProp/Assign"cnn_outlayer/kernel/RMSProp/read:02.cnn_outlayer/kernel/RMSProp/Initializer/ones:0
†
cnn_outlayer/kernel/RMSProp_1:0$cnn_outlayer/kernel/RMSProp_1/Assign$cnn_outlayer/kernel/RMSProp_1/read:021cnn_outlayer/kernel/RMSProp_1/Initializer/zeros:0
П
cnn_outlayer/bias/RMSProp:0 cnn_outlayer/bias/RMSProp/Assign cnn_outlayer/bias/RMSProp/read:02,cnn_outlayer/bias/RMSProp/Initializer/ones:0
Ш
cnn_outlayer/bias/RMSProp_1:0"cnn_outlayer/bias/RMSProp_1/Assign"cnn_outlayer/bias/RMSProp_1/read:02/cnn_outlayer/bias/RMSProp_1/Initializer/zeros:0
Ј
%head_step_fc_setup_1/kernel/RMSProp:0*head_step_fc_setup_1/kernel/RMSProp/Assign*head_step_fc_setup_1/kernel/RMSProp/read:026head_step_fc_setup_1/kernel/RMSProp/Initializer/ones:0
ј
'head_step_fc_setup_1/kernel/RMSProp_1:0,head_step_fc_setup_1/kernel/RMSProp_1/Assign,head_step_fc_setup_1/kernel/RMSProp_1/read:029head_step_fc_setup_1/kernel/RMSProp_1/Initializer/zeros:0
ѓ
#head_step_fc_setup_1/bias/RMSProp:0(head_step_fc_setup_1/bias/RMSProp/Assign(head_step_fc_setup_1/bias/RMSProp/read:024head_step_fc_setup_1/bias/RMSProp/Initializer/ones:0
Є
%head_step_fc_setup_1/bias/RMSProp_1:0*head_step_fc_setup_1/bias/RMSProp_1/Assign*head_step_fc_setup_1/bias/RMSProp_1/read:027head_step_fc_setup_1/bias/RMSProp_1/Initializer/zeros:0
Ч
head_step_fc/kernel/RMSProp:0"head_step_fc/kernel/RMSProp/Assign"head_step_fc/kernel/RMSProp/read:02.head_step_fc/kernel/RMSProp/Initializer/ones:0
†
head_step_fc/kernel/RMSProp_1:0$head_step_fc/kernel/RMSProp_1/Assign$head_step_fc/kernel/RMSProp_1/read:021head_step_fc/kernel/RMSProp_1/Initializer/zeros:0
П
head_step_fc/bias/RMSProp:0 head_step_fc/bias/RMSProp/Assign head_step_fc/bias/RMSProp/read:02,head_step_fc/bias/RMSProp/Initializer/ones:0
Ш
head_step_fc/bias/RMSProp_1:0"head_step_fc/bias/RMSProp_1/Assign"head_step_fc/bias/RMSProp_1/read:02/head_step_fc/bias/RMSProp_1/Initializer/zeros:0
«
)vect_expand_dense_layer1/kernel/RMSProp:0.vect_expand_dense_layer1/kernel/RMSProp/Assign.vect_expand_dense_layer1/kernel/RMSProp/read:02:vect_expand_dense_layer1/kernel/RMSProp/Initializer/ones:0
–
+vect_expand_dense_layer1/kernel/RMSProp_1:00vect_expand_dense_layer1/kernel/RMSProp_1/Assign0vect_expand_dense_layer1/kernel/RMSProp_1/read:02=vect_expand_dense_layer1/kernel/RMSProp_1/Initializer/zeros:0
њ
'vect_expand_dense_layer1/bias/RMSProp:0,vect_expand_dense_layer1/bias/RMSProp/Assign,vect_expand_dense_layer1/bias/RMSProp/read:028vect_expand_dense_layer1/bias/RMSProp/Initializer/ones:0
»
)vect_expand_dense_layer1/bias/RMSProp_1:0.vect_expand_dense_layer1/bias/RMSProp_1/Assign.vect_expand_dense_layer1/bias/RMSProp_1/read:02;vect_expand_dense_layer1/bias/RMSProp_1/Initializer/zeros:0
«
)vect_expand_dense_layer2/kernel/RMSProp:0.vect_expand_dense_layer2/kernel/RMSProp/Assign.vect_expand_dense_layer2/kernel/RMSProp/read:02:vect_expand_dense_layer2/kernel/RMSProp/Initializer/ones:0
–
+vect_expand_dense_layer2/kernel/RMSProp_1:00vect_expand_dense_layer2/kernel/RMSProp_1/Assign0vect_expand_dense_layer2/kernel/RMSProp_1/read:02=vect_expand_dense_layer2/kernel/RMSProp_1/Initializer/zeros:0
њ
'vect_expand_dense_layer2/bias/RMSProp:0,vect_expand_dense_layer2/bias/RMSProp/Assign,vect_expand_dense_layer2/bias/RMSProp/read:028vect_expand_dense_layer2/bias/RMSProp/Initializer/ones:0
»
)vect_expand_dense_layer2/bias/RMSProp_1:0.vect_expand_dense_layer2/bias/RMSProp_1/Assign.vect_expand_dense_layer2/bias/RMSProp_1/read:02;vect_expand_dense_layer2/bias/RMSProp_1/Initializer/zeros:0
У
final_fc_l1/kernel/RMSProp:0!final_fc_l1/kernel/RMSProp/Assign!final_fc_l1/kernel/RMSProp/read:02-final_fc_l1/kernel/RMSProp/Initializer/ones:0
Ь
final_fc_l1/kernel/RMSProp_1:0#final_fc_l1/kernel/RMSProp_1/Assign#final_fc_l1/kernel/RMSProp_1/read:020final_fc_l1/kernel/RMSProp_1/Initializer/zeros:0
Л
final_fc_l1/bias/RMSProp:0final_fc_l1/bias/RMSProp/Assignfinal_fc_l1/bias/RMSProp/read:02+final_fc_l1/bias/RMSProp/Initializer/ones:0
Ф
final_fc_l1/bias/RMSProp_1:0!final_fc_l1/bias/RMSProp_1/Assign!final_fc_l1/bias/RMSProp_1/read:02.final_fc_l1/bias/RMSProp_1/Initializer/zeros:0
У
final_fc_l2/kernel/RMSProp:0!final_fc_l2/kernel/RMSProp/Assign!final_fc_l2/kernel/RMSProp/read:02-final_fc_l2/kernel/RMSProp/Initializer/ones:0
Ь
final_fc_l2/kernel/RMSProp_1:0#final_fc_l2/kernel/RMSProp_1/Assign#final_fc_l2/kernel/RMSProp_1/read:020final_fc_l2/kernel/RMSProp_1/Initializer/zeros:0
Л
final_fc_l2/bias/RMSProp:0final_fc_l2/bias/RMSProp/Assignfinal_fc_l2/bias/RMSProp/read:02+final_fc_l2/bias/RMSProp/Initializer/ones:0
Ф
final_fc_l2/bias/RMSProp_1:0!final_fc_l2/bias/RMSProp_1/Assign!final_fc_l2/bias/RMSProp_1/read:02.final_fc_l2/bias/RMSProp_1/Initializer/zeros:0
У
final_fc_l3/kernel/RMSProp:0!final_fc_l3/kernel/RMSProp/Assign!final_fc_l3/kernel/RMSProp/read:02-final_fc_l3/kernel/RMSProp/Initializer/ones:0
Ь
final_fc_l3/kernel/RMSProp_1:0#final_fc_l3/kernel/RMSProp_1/Assign#final_fc_l3/kernel/RMSProp_1/read:020final_fc_l3/kernel/RMSProp_1/Initializer/zeros:0
Л
final_fc_l3/bias/RMSProp:0final_fc_l3/bias/RMSProp/Assignfinal_fc_l3/bias/RMSProp/read:02+final_fc_l3/bias/RMSProp/Initializer/ones:0
Ф
final_fc_l3/bias/RMSProp_1:0!final_fc_l3/bias/RMSProp_1/Assign!final_fc_l3/bias/RMSProp_1/read:02.final_fc_l3/bias/RMSProp_1/Initializer/zeros:0
У
final_fc_l4/kernel/RMSProp:0!final_fc_l4/kernel/RMSProp/Assign!final_fc_l4/kernel/RMSProp/read:02-final_fc_l4/kernel/RMSProp/Initializer/ones:0
Ь
final_fc_l4/kernel/RMSProp_1:0#final_fc_l4/kernel/RMSProp_1/Assign#final_fc_l4/kernel/RMSProp_1/read:020final_fc_l4/kernel/RMSProp_1/Initializer/zeros:0
Л
final_fc_l4/bias/RMSProp:0final_fc_l4/bias/RMSProp/Assignfinal_fc_l4/bias/RMSProp/read:02+final_fc_l4/bias/RMSProp/Initializer/ones:0
Ф
final_fc_l4/bias/RMSProp_1:0!final_fc_l4/bias/RMSProp_1/Assign!final_fc_l4/bias/RMSProp_1/read:02.final_fc_l4/bias/RMSProp_1/Initializer/zeros:0

output/kernel/RMSProp:0output/kernel/RMSProp/Assignoutput/kernel/RMSProp/read:02(output/kernel/RMSProp/Initializer/ones:0
И
output/kernel/RMSProp_1:0output/kernel/RMSProp_1/Assignoutput/kernel/RMSProp_1/read:02+output/kernel/RMSProp_1/Initializer/zeros:0
w
output/bias/RMSProp:0output/bias/RMSProp/Assignoutput/bias/RMSProp/read:02&output/bias/RMSProp/Initializer/ones:0
А
output/bias/RMSProp_1:0output/bias/RMSProp_1/Assignoutput/bias/RMSProp_1/read:02)output/bias/RMSProp_1/Initializer/zeros:0*Ђ
serving_defaultЧ
4
	head_step'
head_step_inpt:0€€€€€€€€€
.
wrench$
wrench_inpt:0€€€€€€€€€
,
twist#
twist_inpt:0€€€€€€€€€
4
map_img)

img_inpt:0€€€€€€€€€kk/
pose'
output/BiasAdd:0€€€€€€€€€tensorflow/serving/predict