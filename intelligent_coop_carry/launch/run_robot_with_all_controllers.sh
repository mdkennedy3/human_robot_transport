#!/bin/bash

#Stop all current nodes
sudo stop robot

#Bringup new tmux and launch the local packages for controllers
tmux new -s fetch_bringup 'roslaunch fetch_bringup fetch.launch' 
#tmux send -t fetch_bringup.0 roslaunch fetch_bringup fetch.launch
#roslaunch fetch_bringup fetch.launch

#To check the running controllers
#roscd robot_controllers_interface/scripts
#./get_controller_state.py



