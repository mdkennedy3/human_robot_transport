#!/usr/bin/env python
import rospy
import numpy as np
import math
import copy
import geometry_msgs.msg
from geometry_msgs.msg import WrenchStamped, Vector3Stamped, TwistStamped, Vector3, Twist, PoseStamped, Pose, Point
#pub msgs:
from intelligent_coop_carry.msg import HumanIntent, PoseTwist, Head, DynamicalTerms, GripperWrench
#subscribed msgs:
from people_msgs.msg import PositionMeasurement, PositionMeasurementArray
from sensor_msgs.msg import JointState
from nav_msgs.msg import Odometry  #to compare human pose to robot pose
from tf import TransformListener, TransformerROS
import scipy
from scipy import linalg
import threading
from visualization_msgs.msg import Marker, MarkerArray

class HumanIntentClass(object):
  """listens to human intent vectors and pubs in single message"""
  def __init__(self):
    self.thread_lock = threading.Lock()
    self.human_intent_pub = rospy.Publisher("/human_intent",HumanIntent,latch=True, queue_size=1)
    self.tf_lookup_time_diff = 0.3
    self.tf = TransformListener(interpolate=True, cache_time=rospy.Duration(1))
    self.tf_ros = TransformerROS()
    self.person_position_filter = {"angle":45.0, "distance":1.0}
    self.t_prev_sys_odom = None
    self.T_prev_sys_odom = None
    self.system_pose_twist = None
    self.previous_step = None
    self.human_intent = HumanIntent()
    #create a reset bool for each message to wait until new messages are in for each before publishing
    self.step_bool=False
    self.head_bool = False
    self.system_pose_twist_bool = False
    self.wrench_bool = False
    # rate for cpu usage
    self.rate = rospy.Rate(2) #hz  (play with this for system cpu performance may have to add it back)

    #for dynamical qddot calculation
    self.qdot_prev = None


    #For visualizations
    #Step Visual
    self.person_position_pub = rospy.Publisher("/person_position",MarkerArray,queue_size=1)
    self.person_gripper_vector_pub = rospy.Publisher("/human_gripper_vector",MarkerArray,queue_size=1)
    self.gripper_pointer_pub = rospy.Publisher("/gripper_point",MarkerArray,queue_size=1)
    self.person_step_vector = rospy.Publisher("/person_step_vector",MarkerArray,queue_size=1)
    #System Pose/Twist Visual
    self.system_pose_pub = rospy.Publisher("/system_pose_vis",MarkerArray,queue_size=1)
    self.system_twist_pub = rospy.Publisher("/system_twist_vis",MarkerArray,queue_size=1)
    #system Head Pose
    self.head_pose_vis_pub = rospy.Publisher("/head_pose_vis",MarkerArray,queue_size=1)
    #Gripper Force/Torque
    self.gripper_wrench_vis_pub = rospy.Publisher("/gripper_wrench_vis",MarkerArray,queue_size=1)
  def Marker_defined(self,marker_obj=None,header=None,frame_id=None,marker_id=None,marker_type=None,scale=[0.2,0.2,0.2],color=[1,1,1,1],pose_xyz=None, points=None,text=None):
    marker_obj.header = header
    marker_obj.header.frame_id = frame_id
    marker_obj.id = marker_id
    marker_obj.type = marker_type
    marker_obj.scale.x = scale[0]
    marker_obj.scale.y = scale[1]
    marker_obj.scale.z = scale[2]
    marker_obj.color.r = color[0]
    marker_obj.color.g = color[1]
    marker_obj.color.b = color[2]
    marker_obj.color.a = color[3]
    if pose_xyz:
      marker_obj.pose.position.x =  pose_xyz[0]
      marker_obj.pose.position.y =  pose_xyz[1]
      marker_obj.pose.position.z =  pose_xyz[2]
    if points:
      marker_obj.points = points
    if text:
      marker_obj.text = text
    return marker_obj

  def step_callback(self,data):
    # Have to figure out if the sensed person is in front of you (or chair etc)
    #1. obtain the transform from gripper to base (this provides R_gripper_to_base and vect_gripper_frame), so use the first row of R for x_G in baseframe and -vect for position of gripper in base frame
    # frame base_link vs map
    # print "ready to sub"
    # print "map frame exist",self.tf.frameExists("map")
    # if self.tf.frameExists("gripper_link") and self.tf.frameExists("map"):
    try:
      # t = self.tf.getLatestCommonTime("map","gripper_link")
      gripper_position, gripper_quaternion = self.tf.lookupTransform("map","gripper_link", rospy.Time())  #(target_frame,source_frame,time) R_source_to_target v_(frame:target)_vector(target_to_source) as the point should move
      # gripper_position, gripper_quaternion = self.tf.lookupTransform(target_frame='map', source_frame='gripper_link', time=rospy.Time.now()-rospy.Duration(self.tf_lookup_time_diff))
      T = self.tf_ros.fromTranslationRotation(gripper_position,gripper_quaternion)  #4x4 numpy mat
      x_gripper = np.matrix(T[:3,0]) #R is R_frame_to_base hence x in frame is x_B in base which is first column
      v_gripper = np.matrix(T[:3,3]) #switching vector sign as it is point from frame to base
      person_obj = None
      for idx in range(len(data.people)):
        #2. Now obtain the transform from the step to the base
        person_obj = data.people[idx]
        step_frame = person_obj.header.frame_id
        person_name = person_obj.name
        person_id = person_obj.object_id
        # t = self.tf.getLatestCommonTime("map",step_frame)
        position, quaternion = self.tf.lookupTransform("map",step_frame, rospy.Time())
        # position, quaternion = self.tf.lookupTransform(target_frame='map', source_frame=step_frame, time=rospy.Time.now()-rospy.Duration(self.tf_lookup_time_diff))
        T_base_person = self.tf_ros.fromTranslationRotation(position, quaternion)  #4x4 numpy mat
        T_base_person = np.matrix(T_base_person)
        person_pose_map = T_base_person*np.matrix([person_obj.pos.x, person_obj.pos.y, person_obj.pos.z, 1.0]).T
        person_pose_map = person_pose_map[:3]
        #3. Now given the person position in same frame (base_link), now subtract s-p_endeff to get a vector and normalize, 
        #then take dot product between this and gripper frame, then if angle is within set tolerance and magnitude is within 
        #tolerance then accept this as the persons position
        person_vect = np.matrix([person_pose_map.item(i) - v_gripper.item(i) for i in range(3)])
        person_vect_xy = np.matrix([person_pose_map.item(i) - v_gripper.item(i) for i in range(2)])
        x_gripper_xy = np.matrix([x_gripper.item(i) for i in range(2)]) 
        x_gripper_xy_normed = np.divide(x_gripper_xy,np.linalg.norm(x_gripper_xy))
        person_vect_normed = np.divide(person_vect,np.linalg.norm(person_vect))
        person_vect_xy_normed = np.divide(person_vect_xy,np.linalg.norm(person_vect_xy))
        person_vect_magnitude = np.linalg.norm(person_vect_xy)
        dot_prod = person_vect_xy_normed*x_gripper_xy_normed.T
        if (dot_prod >= np.cos(self.person_position_filter["angle"]*np.pi/180.)) and (person_vect_magnitude <= self.person_position_filter["distance"]):
          print "publishing Markers"
          person_position = Marker()
          person_position = self.Marker_defined(marker_obj=person_position,header=person_obj.header,frame_id="/map",marker_id=42,marker_type=2,scale=[0.2,0.2,0.2],color=[1,0,0,1],pose_xyz=[person_pose_map.item(0),person_pose_map.item(1),person_pose_map.item(2)])
          person_position_text = Marker()
          person_position_text = self.Marker_defined(marker_obj=person_position_text,header=person_obj.header,frame_id="/map",marker_id=43,marker_type=9,scale=[0.2,0.2,0.2],color=[1,1,1,1],pose_xyz=[person_pose_map.item(0)+0.3,person_pose_map.item(1),person_pose_map.item(2)],text="Person Position")
          person_position_array = MarkerArray()
          person_position_array.markers = [person_position,person_position_text]
          person_gripper_vector = Marker()
          pgv_start = Point(); pgv_end = Point()
          pgv_start.x = v_gripper.item(0); pgv_start.y = v_gripper.item(1); pgv_start.z = v_gripper.item(2)
          pgv_end.x = person_pose_map.item(0); pgv_end.y = person_pose_map.item(1); pgv_end.z = person_pose_map.item(2)
          pgv_list = [pgv_start,pgv_end]
          person_gripper_vector = self.Marker_defined(marker_obj=person_gripper_vector,header=person_obj.header,frame_id="/map",marker_id=44,marker_type=0,scale=[0.05,0.1 ,0.1 ],color=[0,1,0,1],points=pgv_list)
          person_gripper_vector_text = Marker()
          person_gripper_vector_text = self.Marker_defined(marker_obj=person_gripper_vector_text,header=person_obj.header,frame_id="/map",marker_id=45,marker_type=9,scale=[0.2,0.2,0.2],color=[1,1,1,1],pose_xyz=[pgv_end.x,pgv_end.y,pgv_end.z],text="Person gripper")
          person_gripper_vector_array = MarkerArray()
          person_gripper_vector_array.markers = [person_gripper_vector,person_gripper_vector_text]
          gripper_pointer = Marker()
          gp_start =  Point(); gp_end = Point()
          gp_start.x = v_gripper.item(0); gp_start.y = v_gripper.item(1); gp_start.z = v_gripper.item(2)
          gp_end.x = v_gripper.item(0) + x_gripper.item(0); gp_end.y = v_gripper.item(1) + x_gripper.item(1); gp_end.z = v_gripper.item(2) + x_gripper.item(2)
          gp_list = [gp_start, gp_end]
          gripper_pointer = self.Marker_defined(marker_obj=gripper_pointer,header=person_obj.header,frame_id="/map",marker_id=46,marker_type=0,scale=[0.05,0.1 ,0.1 ],color=[0,0,1,1],points=gp_list)
          gripper_pointer_text = Marker()
          gripper_pointer_text = self.Marker_defined(marker_obj=gripper_pointer_text,header=person_obj.header,frame_id="/map",marker_id=47,marker_type=9,scale=[0.2,0.2,0.2],color=[1,1,1,1],pose_xyz=[gp_end.x,gp_end.y,gp_end.z],text="gripper pointer")
          gripper_pointer_array = MarkerArray()
          gripper_pointer_array.markers = [gripper_pointer_text, gripper_pointer]
          self.person_position_pub.publish(person_position_array) 
          self.person_gripper_vector_pub.publish(person_gripper_vector_array) 
          self.gripper_pointer_pub.publish(gripper_pointer_array) 
          #Send message this can be published
          self.step_bool=True
          # break #this breaks with the person object being the usable feet info
          def step_diff_calc(step_pose_new=None,step_pose_prev=None,header=None):
            step_vector = Vector3Stamped()
            step_vector.header = header
            step_vector.vector.x = step_pose_new.item(0) - step_pose_prev.item(0)
            step_vector.vector.y = step_pose_new.item(1) - step_pose_prev.item(1)
            return step_vector
          if type(person_obj) != type(None):
            if type(self.previous_step) == type(None):
              #no steps have been recorded, so record, return the vector as zero
              curr_step_pose = person_pose_map
              step_vector = Vector3Stamped()
              self.step_bool=False
            else:
              #at least one step has already been recorded, use the difference to find the vector
              curr_step_pose = person_pose_map
              #calc difference in poses
              step_vector = step_diff_calc(step_pose_new=curr_step_pose,step_pose_prev=self.previous_step,header=data.header)
              step_vector_pub=Marker()
              step_start = Point(); step_end = Point()
              step_start.x = self.previous_step.item(0); step_start.y = self.previous_step.item(1); step_start.z = self.previous_step.item(2)
              step_end.x = self.previous_step.item(0) + step_vector.vector.x;  step_end.y = self.previous_step.item(1) + step_vector.vector.y;  step_end.z = self.previous_step.item(2) + step_vector.vector.z; 
              step_list = [step_start,step_end]
              step_vector_pub = self.Marker_defined(marker_obj=step_vector_pub,header=person_obj.header,frame_id="/map",marker_id=50,marker_type=0,scale=[0.05,0.1 ,0.1 ],color=[1,0,0,1],points=step_list)
              step_vector_pub_text = Marker()
              step_vector_pub_text = self.Marker_defined(marker_obj=step_vector_pub_text,header=person_obj.header,frame_id="/map",marker_id=51,marker_type=9,scale=[0.2,0.2,0.2],color=[1,1,1,1],pose_xyz=[step_start.x,step_start.y,step_start.z],text="step vector")
              step_vector_pub_array = MarkerArray()
              step_vector_pub_array.markers = [step_vector_pub, step_vector_pub_text]
              self.person_step_vector.publish(step_vector_pub_array)
            self.previous_step = curr_step_pose
            self.thread_lock.acquire()
            self.human_intent.step_vector = step_vector
            self.pub_human_intent() #check if all can be published
            self.thread_lock.release()
          else:
            rospy.loginfo("no person detected")
          # self.rate.sleep()  #only need this for one message (the fastest)
    except:
      rospy.loginfo("step transform not found")

  def system_odom(self,data):
    #we want the odometry of the center of the object being held, however we allow joints states to trigger the callback as oppose to tf
    try:
      t = self.tf.getLatestCommonTime("base_combined_link","map")
      base_combined_position, base_combined_quaternion = self.tf.lookupTransform("map","base_combined_link", t)# rospy.Time())
      # print "found transformation",base_combined_position, base_combined_quaternion
      T = self.tf_ros.fromTranslationRotation(base_combined_position, base_combined_quaternion)  #4x4 numpy mat
      T = np.matrix(T) #not default and array doesn't work for matrix multiplication
      if type(self.t_prev_sys_odom) != type(None):
        dt = t.to_sec() - self.t_prev_sys_odom.to_sec()
        if dt == 0.0: dt = None
      else:
        dt = None
      if type(self.t_prev_sys_odom) != type(None) and type(self.T_prev_sys_odom) != type(None) and type(dt) != None:
        Tdiff = T*np.linalg.inv(self.T_prev_sys_odom) #this rot diff is in B frame
        Rdiff = Tdiff[:3,:3]
        vdiff = T[:3,3] - self.T_prev_sys_odom[:3,3] #verified this, this is a vector from the first frame to the second
        rdiff_vect_angle = np.matrix([scipy.linalg.logm(Rdiff)[2,1],scipy.linalg.logm(Rdiff)[0,2],scipy.linalg.logm(Rdiff)[0,1]]).T
        rdiff_angle = np.linalg.norm(rdiff_vect_angle) 
        rdiff_axis = np.divide(rdiff_vect_angle,rdiff_angle)
        #Obtain final vector quantities in world frame
        omega_sys = np.divide(rdiff_vect_angle,dt)
        vel_sys = np.divide(vdiff,float(dt))
        #prescribe twist
        system_twist = Twist()
        system_twist.linear.x = vel_sys.item(0)
        system_twist.linear.y = vel_sys.item(1)
        system_twist.linear.z = vel_sys.item(2)
        system_twist.angular.x = omega_sys.item(0)
        system_twist.angular.y = omega_sys.item(1)
        system_twist.angular.z = omega_sys.item(2)
        #prescribe pose
        system_pose = Pose()
        #negative on position as it is frame to base in base frame (this is base to frame in base frame), the rotation however is Frame to Base (vector in frame through this is vector in base, 
        #to switch we just flip the vector sign(x,y,z))
        system_pose.position.x =  base_combined_position[0]
        system_pose.position.y =  base_combined_position[1]
        system_pose.position.z =  base_combined_position[2]
        system_pose.orientation.x =  base_combined_quaternion[0]
        system_pose.orientation.y =  base_combined_quaternion[1]
        system_pose.orientation.z =  base_combined_quaternion[2]
        system_pose.orientation.w =  base_combined_quaternion[3]
        #make our custom message and save
        system_pose_twist = PoseTwist()
        system_pose_twist.header = data.header
        system_pose_twist.twist = system_twist
        system_pose_twist.pose = system_pose
        self.thread_lock.acquire()
        self.human_intent.system_pose_twist = system_pose_twist
        self.system_pose_twist_bool = True
        self.pub_human_intent() #check if all can be published
        self.thread_lock.release()
        #Now visualize the twist and pose vectors
        #Twist
        # system_vis_position = [system_pose.position.x,system_pose.position.y,system_pose.position.z]
        #linear comp
        system_twist_linear_marker = Marker()        
        twist_vect_start = Point(); twist_vect_end = Point()
        twist_vect_start.x = system_pose.position.x; 
        twist_vect_start.y = system_pose.position.y; 
        twist_vect_start.z = system_pose.position.z
        twist_vect_end.x = twist_vect_start.x + system_twist.linear.x;  
        twist_vect_end.y = twist_vect_start.y  + system_twist.linear.y;  
        twist_vect_end.z = twist_vect_start.z  + system_twist.linear.z; 
        twist_vect_list = [twist_vect_start,twist_vect_end]
        system_twist_linear_marker = self.Marker_defined(marker_obj=system_twist_linear_marker,header=data.header,frame_id="/map",marker_id=1,marker_type=0,scale=[0.05,0.1 ,0.1 ],color=[0,1,0,1],points=twist_vect_list)
        system_twist_linear_marker_text = Marker()
        system_twist_linear_marker_text = self.Marker_defined(marker_obj=system_twist_linear_marker_text,header=data.header,frame_id="/map",marker_id=2,marker_type=9,scale=[0.2,0.2,0.2],color=[0,1,0,1],pose_xyz=[system_pose.position.x,system_pose.position.y,system_pose.position.z],text="sys twist linear")
        #angular comp
        system_twist_angular_marker = Marker()
        twist_ang_end = Point()
        twist_ang_end.x = twist_vect_start.x + system_twist.angular.x;  
        twist_ang_end.y = twist_vect_start.y + system_twist.angular.y;  
        twist_ang_end.z = twist_vect_start.z + system_twist.angular.z; 
        twist_ang_list = [twist_vect_start,twist_ang_end]
        system_twist_angular_marker = self.Marker_defined(marker_obj=system_twist_angular_marker,header=data.header,frame_id="/map",marker_id=3,marker_type=0,scale=[0.05,0.1 ,0.1 ],color=[0,0,1,1],points=twist_ang_list)
        system_twist_angular_marker_text = Marker()
        system_twist_angular_marker_text = self.Marker_defined(marker_obj=system_twist_angular_marker_text,header=data.header,frame_id="/map",marker_id=4,marker_type=9,scale=[0.2,0.2,0.2],color=[0,0,1,1],pose_xyz=[system_pose.position.x+0.2,system_pose.position.y,system_pose.position.z],text="sys twist angular")
        system_twist_pub_array = MarkerArray()
        system_twist_pub_array.markers = [system_twist_linear_marker, system_twist_linear_marker_text,system_twist_angular_marker,system_twist_angular_marker_text]
        self.system_twist_pub.publish(system_twist_pub_array)
        #Pose
        pose_vect_scale = 0.2
        x_sys = T*np.matrix([pose_vect_scale*1,0,0,0]).T
        y_sys = T*np.matrix([0,pose_vect_scale*1,0,0]).T
        z_sys = T*np.matrix([0,0,pose_vect_scale*1,0]).T
        pose_vect_start = Point(); pose_vect_end_x = Point(); pose_vect_end_y = Point(); pose_vect_end_z = Point()
        pose_vect_start.x = system_pose.position.x; pose_vect_start.y = system_pose.position.y; pose_vect_start.z = system_pose.position.z; 
        pose_vect_end_x.x = pose_vect_start.x + x_sys.item(0); pose_vect_end_x.y = pose_vect_start.y + x_sys.item(1); pose_vect_end_x.z = pose_vect_start.z + x_sys.item(2)
        pose_vect_end_y.x = pose_vect_start.x + y_sys.item(0); pose_vect_end_y.y = pose_vect_start.y + y_sys.item(1); pose_vect_end_y.z = pose_vect_start.z + y_sys.item(2)
        pose_vect_end_z.x = pose_vect_start.x + z_sys.item(0); pose_vect_end_z.y = pose_vect_start.y + z_sys.item(1); pose_vect_end_z.z = pose_vect_start.z + z_sys.item(2)
        pose_vect_x_list = [pose_vect_start,pose_vect_end_x]; pose_vect_y_list = [pose_vect_start,pose_vect_end_y]; pose_vect_z_list = [pose_vect_start,pose_vect_end_z];
        sys_x_vect_marker = Marker()
        sys_x_vect_marker = self.Marker_defined(marker_obj=sys_x_vect_marker,header=data.header,frame_id="/map",marker_id=1,marker_type=0,scale=[0.02,0.05 ,0.05 ],color=[1,0,0,1],points=pose_vect_x_list)
        sys_y_vect_marker = Marker()
        sys_y_vect_marker = self.Marker_defined(marker_obj=sys_y_vect_marker,header=data.header,frame_id="/map",marker_id=2,marker_type=0,scale=[0.02,0.05 ,0.05 ],color=[0,1,0,1],points=pose_vect_y_list)
        sys_z_vect_marker = Marker()
        sys_z_vect_marker = self.Marker_defined(marker_obj=sys_z_vect_marker,header=data.header,frame_id="/map",marker_id=3,marker_type=0,scale=[0.02,0.05 ,0.05 ],color=[0,0,1,1],points=pose_vect_z_list)
        system_pose_marker_text = Marker()
        system_pose_marker_text = self.Marker_defined(marker_obj=system_pose_marker_text,header=data.header,frame_id="/map",marker_id=4,marker_type=9,scale=[0.2,0.2,0.2],color=[1,0,0,1],pose_xyz=[system_pose.position.x-0.2,system_pose.position.y+0.2,system_pose.position.z+0.1],text="sys pose")
        system_pose_pub_array = MarkerArray()
        system_pose_pub_array.markers = [sys_x_vect_marker,sys_y_vect_marker,sys_z_vect_marker,system_pose_marker_text]
        self.system_pose_pub.publish(system_pose_pub_array)
      else:
        pass
      # print "made it here",t,T
      self.t_prev_sys_odom = t
      self.T_prev_sys_odom = T
      # print "assigned dt, T", t, T
      self.rate.sleep()  #only need this for one message (the fastest)
    except:
      rospy.loginfo("system odom transform not found")

  def head_callback(self,data):
    #while this message is the correct trigger, nothing is stored in /head_pose message, instead I have to use the transforms
    try:
      t = self.tf.getLatestCommonTime("head_origin","map")
      head_position, head_quaternion = self.tf.lookupTransform("map","head_origin", t)
      T_frame_to_map = self.tf_ros.fromTranslationRotation(head_position, head_quaternion)  #4x4 numpy mat
      T_frame_to_map = np.matrix(T_frame_to_map) #not default and array doesn't work for matrix multiplication
      T_map_to_frame = np.linalg.inv(T_frame_to_map) #this is map to frame
      head_vect = T_map_to_frame[0,:3]  #this is the base to frame
      head_obj = Head()
      head_obj.header = data.header
      head_obj.head_pose.position.x = head_position[0]
      head_obj.head_pose.position.y = head_position[1]
      head_obj.head_pose.position.z = head_position[2]
      head_obj.head_pose.orientation.x = head_quaternion[0]
      head_obj.head_pose.orientation.y = head_quaternion[1]
      head_obj.head_pose.orientation.z = head_quaternion[2]
      head_obj.head_pose.orientation.w = head_quaternion[3]
      head_obj.head_vect.x = head_vect.item(0)
      head_obj.head_vect.y = head_vect.item(1)
      head_obj.head_vect.z = head_vect.item(2)
      #visualize the head pose and vector
      head_position_marker = Marker()
      head_position_list = [head_position[0],head_position[1],head_position[2]]
      head_position_marker = self.Marker_defined(marker_obj=head_position_marker,header=data.header,frame_id="/map",marker_id=1,marker_type=2,scale=[0.05,0.05,0.05],color=[1,0,0,1],pose_xyz=head_position_list)
      head_pose_vector_marker = Marker()
      head_start_position = Point(); head_end_position = Point()
      head_start_position.x = head_position[0]
      head_start_position.y = head_position[1]
      head_start_position.z = head_position[2]
      head_vect_scale = 0.3
      head_end_position.x = head_position[0] + head_vect.item(0)*head_vect_scale
      head_end_position.y = head_position[1] + head_vect.item(1)*head_vect_scale
      head_end_position.z = head_position[2] + head_vect.item(2)*head_vect_scale
      head_pose_vect_list = [head_start_position, head_end_position]
      head_pose_vector_marker = self.Marker_defined(marker_obj=head_pose_vector_marker,header=data.header,frame_id="/map",marker_id=2,marker_type=0,scale=[0.02,0.05 ,0.05 ],color=[1,0,0,1],points=head_pose_vect_list)
      head_pose_vector_text = Marker()
      head_pose_vector_text = self.Marker_defined(marker_obj=head_pose_vector_text,header=data.header,frame_id="/map",marker_id=3,marker_type=9,scale=[0.2,0.2,0.2],color=[1,0,0,1],pose_xyz=head_position_list,text="Head pose")
      head_pose_pub_marker_array = MarkerArray()
      head_pose_pub_marker_array.markers = [head_position_marker,head_pose_vector_marker,head_pose_vector_text]
      self.head_pose_vis_pub.publish(head_pose_pub_marker_array)
      self.thread_lock.acquire()
      self.human_intent.head_vector = head_obj
      self.head_bool = True
      self.pub_human_intent() #check if all can be published
      self.thread_lock.release()
      # self.rate.sleep()  #only need this for one message (the fastest)
    except:
      rospy.loginfo("head transform not found")

  def wrench_callback(self,data):
    #given the dynamical terms, use the effort
    J = []
    for r in data.jacobian:
      J.append(r.row)
    J = np.matrix(np.vstack(J))

    inertia_mat = []
    for r in data.inertia:
      inertia_mat.append(r.row)
    inertia_mat = np.matrix(np.vstack(inertia_mat))


    if type(self.qdot_prev) != type(None):
      if data.header.stamp.to_sec() - self.qdot_prev["t"] > 0.0:
        qddot = np.matrix([(data.q[idx]- self.qdot_prev["q"][idx])/(data.header.stamp.to_sec() - self.qdot_prev["t"]) for idx in range(len(data.q))]).T
      else:
        qddot = np.matrix([0.0 for idx in range(len(data.q))]).T
    else:
      qddot = np.matrix([0.0 for idx in range(len(data.q))]).T
      self.qdot_prev = {"q":[],"t":0.0}
    inertial_terms = inertia_mat*qddot
    self.qdot_prev["q"] =copy.copy(data.q)
    self.qdot_prev["t"] = copy.copy(data.header.stamp.to_sec())

    effort = np.matrix([ t for t in data.effort]).T
    manip_measure = np.sqrt(np.linalg.det(J*J.T))
    gravity = np.matrix(data.gravity).T
    coriolis = np.matrix(data.coriolis).T
    wrench_ext = data.wrench_ext #np.linalg.pinv(J.T)*effort #- gravity - coriolis - inertial_terms
    #Add in the other terms during real operation
    #Now use the transform to get this wrench in world (map) frame

    try:
      joint_frame = "torso_lift_link" #"shoulder_lift_link" #"wrist_roll_joint"  (but show it acting at the shoulder lift joint)
      t_shoulder = self.tf.getLatestCommonTime("map",joint_frame)
      shoulder_position, shoulder_quaternion = self.tf.lookupTransform("map",joint_frame, rospy.Time())
      T_shoulder = self.tf_ros.fromTranslationRotation(shoulder_position, shoulder_quaternion)  #4x4 numpy mat
      T_shoulder = np.matrix(T_shoulder) #not default and array doesn't work for matrix multiplication
      joint_frame_end_eff = "gripper_link" #"wrist_roll_link"
      # t_end_eff = self.tf.getLatestCommonTime("map",joint_frame_end_eff)
      end_eff_position, end_eff_quaternion = self.tf.lookupTransform("map",joint_frame_end_eff, t_shoulder)
      T_end_eff = self.tf_ros.fromTranslationRotation(end_eff_position, end_eff_quaternion)  #4x4 numpy mat
      T_end_eff = np.matrix(T_end_eff) #not default and array doesn't work for matrix multiplication
      end_eff_position = T_end_eff[:3,3]
      f_shoulder = np.matrix([wrench_ext.wrench.force.x,wrench_ext.wrench.force.y,wrench_ext.wrench.force.z]).T
      torque_shoulder= np.matrix([wrench_ext.wrench.torque.x,wrench_ext.wrench.torque.y,wrench_ext.wrench.torque.z]).T
      # f_world = T_shoulder *np.vstack([f_shoulder,1])
      # f_world = f_world[:3]
      f_world = f_shoulder

      # torque_world = T_shoulder *np.vstack([torque_shoulder,1])
      # torque_world = torque_world[:3]
      torque_world = torque_shoulder

      gripper_wrench_obj = GripperWrench()
      gripper_wrench_obj.header = data.header
      gripper_wrench_obj.header.frame_id = "/map"
      gripper_wrench_obj.wrench.force.x = f_world.item(0)
      gripper_wrench_obj.wrench.force.y = f_world.item(1)
      gripper_wrench_obj.wrench.force.z = f_world.item(2)
      gripper_wrench_obj.wrench.torque.x = torque_world.item(0)
      gripper_wrench_obj.wrench.torque.y = torque_world.item(1)
      gripper_wrench_obj.wrench.torque.z = torque_world.item(2)
      gripper_wrench_obj.jac_manip = manip_measure
      print "stored all values ready to visualize"
      #Visualization
      force_marker = Marker()
      gripper_start_point = Point()
      gripper_start_point.x = end_eff_position.item(0);
      gripper_start_point.y = end_eff_position.item(1);
      gripper_start_point.z = end_eff_position.item(2)
      force_end_point = Point()
      force_scale = 0.01
      force_end_point.x = gripper_start_point.x + force_scale*gripper_wrench_obj.wrench.force.x;
      force_end_point.y = gripper_start_point.y + force_scale*gripper_wrench_obj.wrench.force.y;
      force_end_point.z = gripper_start_point.z + force_scale*gripper_wrench_obj.wrench.force.z;
      force_point_list=[gripper_start_point, force_end_point]
      force_marker = self.Marker_defined(marker_obj=force_marker,header=data.header,frame_id="/map",marker_id=1,marker_type=0,scale=[0.02,0.05 ,0.05 ],color=[1,0,0,1],points=force_point_list)
      torque_marker = Marker()
      torque_end_point = Point()
      torque_scale = 0.1
      torque_end_point.x = gripper_start_point.x + torque_scale*gripper_wrench_obj.wrench.torque.x;
      torque_end_point.y = gripper_start_point.y + torque_scale*gripper_wrench_obj.wrench.torque.y;
      torque_end_point.z = gripper_start_point.z + torque_scale*gripper_wrench_obj.wrench.torque.z;
      torque_point_list=[gripper_start_point, torque_end_point]
      torque_marker = self.Marker_defined(marker_obj=torque_marker,header=data.header,frame_id="/map",marker_id=2,marker_type=0,scale=[0.02,0.05 ,0.05 ],color=[0,1,0,1],points=torque_point_list)
      gripper_pose_torque_list = [end_eff_position.item(0)+0.1,end_eff_position.item(1),end_eff_position.item(2)]
      gripper_pose_force_list = [end_eff_position.item(0),end_eff_position.item(1),end_eff_position.item(2)]
      force_text = Marker()
      force_text = self.Marker_defined(marker_obj=force_text,header=data.header,frame_id="/map",marker_id=3,marker_type=9,scale=[0.2,0.2,0.2],color=[1,0,0,1],pose_xyz=gripper_pose_force_list,text="force")
      torque_text = Marker()
      torque_text = self.Marker_defined(marker_obj=torque_text,header=data.header,frame_id="/map",marker_id=4,marker_type=9,scale=[0.2,0.2,0.2],color=[0,1,0,1],pose_xyz=gripper_pose_torque_list,text="torque")
      gripper_wrench_vis_pub_marker_array = MarkerArray()
      gripper_wrench_vis_pub_marker_array.markers = [force_marker, torque_marker,force_text,torque_text]
      self.gripper_wrench_vis_pub.publish(gripper_wrench_vis_pub_marker_array)
      #now send it as a message
      self.thread_lock.acquire()
      self.human_intent.gripper_wrench = gripper_wrench_obj
      self.wrench_bool = True
      self.pub_human_intent() #check if all can be published
      self.thread_lock.release()
      # self.rate.sleep()  #only need this for one message (the fastest)
    except:
      rospy.loginfo("wrench transform not found")

  def pub_human_intent(self):
    # print "trying to pub human intent"
    print "intent bools:",self.step_bool, self.head_bool, self.system_pose_twist_bool, self.wrench_bool
    if self.step_bool and self.head_bool and self.system_pose_twist_bool and self.wrench_bool:
      #pub human intent
      print "starting publish"
      print "message to publish:", self.human_intent
      # self.thread_lock.acquire()
      print "inside thread lock"
      self.human_intent_pub.publish(self.human_intent)
      print "finished publishing"
      self.step_bool=False; self.head_bool = False; self.system_pose_twist_bool = False; self.wrench_bool = False #reset the bools after publishing
      print "reset bools:"
      print "bools:",self.step_bool, self.head_bool, self.system_pose_twist_bool, self.wrench_bool
      # self.thread_lock.release()
    else:
      pass
  def subscribers(self):
    #call subscribers for each topic (wrench:f,t; head, step, system_twist:v,omega)
    rospy.Subscriber("/people_tracker_measurements",PositionMeasurementArray,self.step_callback,queue_size=1) #steps
    rospy.Subscriber("/joint_states",JointState,self.system_odom,queue_size=1)  #system twist
    rospy.Subscriber("/head_pose",PoseStamped,self.head_callback,queue_size=1) #head
    rospy.Subscriber("/dynamical_terms",DynamicalTerms,self.wrench_callback,queue_size=1) #wrench
    rospy.spin()
def main():
  rospy.init_node("pub_human_intent")
  cls_obj = HumanIntentClass()
  cls_obj.subscribers()




if __name__ == '__main__':
  main()
