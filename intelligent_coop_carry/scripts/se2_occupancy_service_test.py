#!/usr/bin/env python
"""
Author: Monroe Kennedy III
Date: 6/28/17
All rights reserved
"""

# from __future__ import print_function

import numpy as np, math
import rospy
from nav_msgs.msg import OccupancyGrid, Odometry
from sensor_msgs.msg import PointCloud
from geometry_msgs.msg import Point
from geometry_msgs.msg import Twist
from intelligent_coop_carry.srv import Se2_occ_pts
import intelligent_coop_carry.srv
import threading


class Se2_occupancy_class(object):
    """This class assists in determining the occupied SE2 configruation space for a planer robot"""
    def __init__(self):
        self.occupied_pts_R2 = PointCloud()
        self.map = None
        self.map_bool = False
        self.robot_pose = False
        self.se2_occ_serv = None
        # self.map_sub = None
        # self.robot_sub = None
        self.service_client_running = False
        self.thread_lock = threading.Lock()

    def ros_init(self):
        # self.map_sub = rospy.Subscriber('/map', OccupancyGrid, self.map_callback, queue_size=1)
        self.robot_sub = rospy.Subscriber('/odom', Odometry, self.robot_pose_callback, queue_size=1)


    def generate_hull_pts(self):
        xval = 0.4; yval = 0.2
        #these don't have to be square
        pt1 = Point(); pt1.x = xval; pt1.y = yval; pt1.z = 0;
        pt2 = Point(); pt2.x = -xval; pt2.y = yval; pt2.z = 0;
        pt3 = Point(); pt3.x = -xval; pt3.y = -yval; pt3.z = 0;
        pt4 = Point(); pt4.x = xval; pt4.y = -yval; pt4.z = 0;
        system_hull_pts = [pt1, pt2, pt3, pt4]
        return system_hull_pts

    def se2_occupancy_client(self, robot_twist): 
        #, robot_pose, robot_twist):
        #map,robot_pose,system_hull_pts,sensor_radius, theta_step_num
        rospy.wait_for_service('se2_occupancy_service'); rospy.loginfo("Service Acknowledged")
        self.service_client_running = True

        system_hull_pts = self.generate_hull_pts()

        #if map and robot pose are obtained, and service is available, then query 
        try:
            
            request = intelligent_coop_carry.srv.Se2_occ_ptsRequest()
            request.sensor_radius = 3.0  #meters should be the units
            request.system_hull_pts = system_hull_pts
            request.theta_step_num = int(10)

            self.thread_lock.acquire()
            #These should be published from bayes filter later
            robot_twist = robot_twist#.twist
            # request.avg_system_speed = np.sqrt(robot_twist.linear.x**2 + robot_twist.linear.y**2 + robot_twist.linear.z**2)
            request.robot_twist = robot_twist
            self.thread_lock.release()


            response = self.se2_occ_serv(request)
            # response.occ_pts; response.free_pts
            self.service_client_running = False
            return response
        except rospy.ServiceException, e:
            self.service_client_running = False
            print "Service call failed: %s"%e

    # def map_callback(self,req):
    #     #self.map_sub.unregister()

    #     #this function quieres the map
    #     self.thread_lock.acquire()
    #     self.map = req
    #     self.map_bool = True
    #     self.thread_lock.release()
    #     # rospy.sleep(0.5)
    #     rospy.loginfo("recieved map")
    #     # if (type(self.robot_pose) != bool):
    #     #     resp = self.se2_occupancy_client()
    #     #     rospy.loginfo("response obtained")
    #     #     # self.map = False




    def robot_pose_callback(self,req):
        #self.robot_sub.unregister()
        self.thread_lock.acquire()
        robot_pose = req.pose.pose #this is geometry_msgs/Pose type
        robot_twist = req.twist.twist
        self.thread_lock.release()
        if not self.service_client_running:
            # print "trying to access, with twist: ", robot_twist
            self.t = threading.Thread(target=self.se2_occupancy_client, args=(robot_twist,))
            self.t.daemon = True
            self.t.start()


def main():
    cls_obj = Se2_occupancy_class()
    rospy.init_node('se2_occupancy_service_test') #instantiate node

    rospy.wait_for_service('/se2_occupancy_service')
    cls_obj.se2_occ_serv = rospy.ServiceProxy('/se2_occupancy_service', Se2_occ_pts)
    cls_obj.ros_init()

    rospy.spin()


    # while not rospy.is_shutdown():
    #     cls_obj.se2_occupancy_client()

if __name__ == '__main__':
    main()