#!/usr/bin/env python2
import numpy as np, math
import rospy
from geometry_msgs.msg import Twist
import curses, time




def main():

    def input_char():
        try:
            win = curses.initscr()
            # win.addstr(0, 0, message)
            while True: 
                ch = win.getch()
                if ch in range(32, 127): break
                time.sleep(0.05)
        except: raise
        finally:
            curses.endwin()
        return chr(ch)

    rospy.init_node('key_to_twist')

    #set max speeds: 
    v_thresh = 0.6
    w_thresh = 20.0


    #cmd_vel msg
    cmd_vel = Twist()
    #cmd_vel topic
    vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1, latch=True)




    print "Use w,a,s,d to drive, x to stop"
    while not rospy.is_shutdown():
        c = input_char()
        if c in [' ','x']:
            #all stop
            cmd_vel.linear.x = 0.0; cmd_vel.linear.y = 0.0; cmd_vel.linear.z = 0.0
            cmd_vel.angular.x = 0.0; cmd_vel.angular.y = 0.0; cmd_vel.angular.z = 0.0
            print "\nall stop"
        elif c in ['w']:
            if np.abs(cmd_vel.linear.x) <= v_thresh:
                cmd_vel.linear.x += 0.1
            else:
                cmd_vel.linear.x = v_thresh
        elif c in ['s']:
            if np.abs(cmd_vel.linear.x) <= v_thresh:
                cmd_vel.linear.x -= 0.1
            else:
                cmd_vel.linear.x = -v_thresh
        elif c in ['a']:
            if np.abs(cmd_vel.angular.z) <= w_thresh:
                cmd_vel.angular.z += 5.0
            else:
                cmd_vel.angular.z = w_thresh
        elif c in ['d']:
            if np.abs(cmd_vel.angular.z) <= w_thresh:
                cmd_vel.angular.z -= 5.0
            else:
                cmd_vel.angular.z = -w_thresh

        #publish
        vel_pub.publish(cmd_vel)



if __name__ == '__main__':
    main()