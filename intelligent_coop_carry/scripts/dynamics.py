#!/usr/bin/env python

from __future__ import division#, print_function
import rospy
import numpy as np
import PyKDL as kdl
from sensor_msgs.msg import JointState
from urdf_parser_py.urdf import Robot
from pykdl_utils.kdl_parser import kdl_tree_from_urdf_model
from pykdl_utils.kdl_kinematics import KDLKinematics
from pykdl_utils.kdl_kinematics import kdl_to_mat
from pykdl_utils.kdl_kinematics import joint_kdl_to_list
from pykdl_utils.kdl_kinematics import joint_list_to_kdl

from intelligent_coop_carry.msg import DynamicalTerms, Matrow




class Dynamical_Terms(object):
  """Dynamical terms for I(q)\ddot{q} + C(\dot{q},q) + V(q) = tau + J^T f_ext"""
  def __init__(self, base_link="torso_lift_link", end_link="wrist_roll_link"):
    self.base_link = base_link
    self.end_link = end_link
    self.urdf = Robot.from_parameter_server()

    self.kdl_tree = kdl_tree_from_urdf_model(self.urdf)
    self.chain = self.kdl_tree.getChain(base_link, end_link)
    self._fk_kdl = kdl.ChainFkSolverPos_recursive(self.chain)
    self._ik_v_kdl = kdl.ChainIkSolverVel_pinv(self.chain)
    self._ik_p_kdl = kdl.ChainIkSolverPos_NR(self.chain, self._fk_kdl, self._ik_v_kdl)
    self._jac_kdl = kdl.ChainJntToJacSolver(self.chain)
    self._dyn_kdl = kdl.ChainDynParam(self.chain, kdl.Vector(0,0,-9.81))
    self.kdl_kin = KDLKinematics(self.urdf, base_link, end_link)
    self.num_joints = self.kdl_kin.num_joints
    self.joint_names = self.kdl_kin.get_joint_names()

    self.dynamical_pub = rospy.Publisher('dynamical_terms',DynamicalTerms,queue_size=1)

    self.rate = rospy.Rate(100) #hz

  def kdl_to_vect(self,kdl_vect):
    vect = [kdl_vect[i] for i in range(np.max([len(np.arange(kdl_vect.columns())),len(np.arange(kdl_vect.rows()))])) ]
    return vect

  def cart_inertia(self, q=[]):
    H = self.inertia(q)
    J = self.jacobian(q)
    return np.linalg.inv(J * np.linalg.inv(H) * J.T)

  def inertia(self, q=[]):
    h_kdl = kdl.JntSpaceInertiaMatrix(self.num_joints)
    self._dyn_kdl.JntToMass(joint_list_to_kdl(q), h_kdl)
    return kdl_to_mat(h_kdl)

  def jacobian(self, q=[]):
    j_kdl = kdl.Jacobian(self.num_joints)
    q_kdl = joint_list_to_kdl(q)
    self._jac_kdl.JntToJac(q_kdl, j_kdl)
    # keep kdl format m[i,j] 
    return kdl_to_mat(j_kdl)

  def coriolis(self,q=[], qdot=[]):
    q = q #list
    qdot = qdot #list
    q_cori = [0.0 for idx in range(len(q))]
    q_kdl = joint_list_to_kdl(q)
    qdot_kdl = joint_list_to_kdl(qdot)
    q_cori_kdl = joint_list_to_kdl(q_cori)
    self._dyn_kdl.JntToCoriolis(q_kdl, qdot_kdl, q_cori_kdl)
    return q_cori_kdl

  def gravity(self,q=[]):
    q_grav = [0.0 for idx in range(len(q))]
    q_kdl = joint_list_to_kdl(q)
    q_grav_kdl = joint_list_to_kdl(q_grav)
    self._dyn_kdl.JntToGravity(q_kdl,q_grav_kdl)
    # print "the q and the gravity:", q, q_grav_kdl
    return q_grav_kdl


  def joint_state_callback(self,req):
    if "shoulder_pan_joint" in req.name:
      #this msg contains info for the arm
      q = []
      dq = []
      effort = []
      for joint_name in self.joint_names:
        idx = req.name.index(joint_name)
        q.append(req.position[idx])
        dq.append(req.velocity[idx])
        effort.append(req.effort[idx])
      self.generate_dynamical_message(q=q,dq=dq,effort=effort, joint_names = self.joint_names)
    else:
      pass


  def generate_dynamical_message(self,q=None,dq=None,effort=None, joint_names=None):
    dynamics_msg = DynamicalTerms()

    J = self.jacobian(q=q)
    for i in range(J.shape[0]):
      r = Matrow()
      for j in range(J.shape[1]):
        r.row.append(J[i,j])
      dynamics_msg.jacobian.append(r)

    I = self.inertia(q=q)
    for i in range(I.shape[0]):
      r = Matrow()
      for j in range(I.shape[1]):
        r.row.append(I[i,j])
      dynamics_msg.inertia.append(r)

    C = self.coriolis(q=q,qdot=dq)
    dynamics_msg.coriolis = [c for c in C]


    G = self.gravity(q=q)
    dynamics_msg.gravity = [g for g in G]

    dynamics_msg.q = [qi for qi in q]
    dynamics_msg.qdot = [qdi for qdi in dq]
    dynamics_msg.effort = [e for e in effort]

    dynamics_msg.joint_names = joint_names

    self.dynamical_pub.publish(dynamics_msg)
    self.rate.sleep()

def main():
  rospy.init_node('Dynamics')
  base_link = "torso_lift_link"# this does all 7 joints, for just 6 joints:  "shoulder_pan_link"
  end_link = "wrist_roll_link"
  cls_obj = Dynamical_Terms()
  # in joint state callback, find the index of each joint string, from joint names, then use that to extract the sensed position, velocity and effort for each joint (as the order is messed up for entire system)
  #node with a set rate publishing: [q,dq/dt,effort,J,I,C,G]
  sub = rospy.Subscriber('/joint_states',JointState,cls_obj.joint_state_callback)
  rospy.spin()



if __name__ == '__main__':
  main()
