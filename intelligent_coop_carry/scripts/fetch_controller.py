#!/usr/bin/env python
import rospy
import numpy as np
import copy
from intelligent_coop_carry.srv import * #SwitchController, SwitchControllerRequest, SwitchControllerResponse
from intelligent_coop_carry.msg import DynamicalTerms

from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState
import PyKDL as kdl
from urdf_parser_py.urdf import Robot
from pykdl_utils.kdl_parser import kdl_tree_from_urdf_model
from pykdl_utils.kdl_kinematics import KDLKinematics
from pykdl_utils.kdl_kinematics import kdl_to_mat
from pykdl_utils.kdl_kinematics import joint_kdl_to_list
from pykdl_utils.kdl_kinematics import joint_list_to_kdl



class FetchController(object):
  """docstring for FetchController"""
  def __init__(self,base_link="torso_lift_link", end_link="gripper_link"):
    self.arm_control_pub = rospy.Publisher("/arm_controller/torque_control_arm/command",JointTrajectory,latch=False,queue_size=1)
    self.base_diff_pub = rospy.Publisher("/base_controller/command",Twist,latch=True,queue_size=1)
    self.qdot_prev = None
    self.base_link = base_link
    self.end_link = end_link
    self.urdf = Robot.from_parameter_server()
    self.kdl_tree = kdl_tree_from_urdf_model(self.urdf)
    self.chain = self.kdl_tree.getChain(base_link, end_link)
    self._fk_kdl = kdl.ChainFkSolverPos_recursive(self.chain)
    self._ik_v_kdl = kdl.ChainIkSolverVel_pinv(self.chain)
    self._ik_p_kdl = kdl.ChainIkSolverPos_NR(self.chain, self._fk_kdl, self._ik_v_kdl)
    self._jac_kdl = kdl.ChainJntToJacSolver(self.chain)
    self._dyn_kdl = kdl.ChainDynParam(self.chain, kdl.Vector(0,0,-9.81))
    self.kdl_kin = KDLKinematics(self.urdf, base_link, end_link)
    self.num_joints = self.kdl_kin.num_joints
    self.joint_names = self.kdl_kin.get_joint_names()
    self.dynamical_pub = rospy.Publisher('dynamical_terms',DynamicalTerms,queue_size=1)
    self.rate = rospy.Rate(100) #hz

  def kdl_to_vect(self,kdl_vect):
    vect = [kdl_vect[i] for i in range(np.max([len(np.arange(kdl_vect.columns())),len(np.arange(kdl_vect.rows()))])) ]
    return vect

  def cart_inertia(self, q=[]):
    H = self.inertia(q)
    J = self.jacobian(q)
    return np.linalg.inv(J * np.linalg.inv(H) * J.T)

  def inertia(self, q=[]):
    h_kdl = kdl.JntSpaceInertiaMatrix(self.num_joints)
    self._dyn_kdl.JntToMass(joint_list_to_kdl(q), h_kdl)
    return kdl_to_mat(h_kdl)

  def jacobian(self, q=[]):
    j_kdl = kdl.Jacobian(self.num_joints)
    q_kdl = joint_list_to_kdl(q)
    self._jac_kdl.JntToJac(q_kdl, j_kdl)
    # keep kdl format m[i,j] 
    return kdl_to_mat(j_kdl)

  def coriolis(self,q=[], qdot=[]):
    q = q #list
    qdot = qdot #list
    q_cori = [0.0 for idx in range(len(q))]
    q_kdl = joint_list_to_kdl(q)
    qdot_kdl = joint_list_to_kdl(qdot)
    q_cori_kdl = joint_list_to_kdl(q_cori)
    self._dyn_kdl.JntToCoriolis(q_kdl, qdot_kdl, q_cori_kdl)
    return q_cori_kdl

  def gravity(self,q=[]):
    q_grav = [0.0 for idx in range(len(q))]
    q_kdl = joint_list_to_kdl(q)
    q_grav_kdl = joint_list_to_kdl(q_grav)
    self._dyn_kdl.JntToGravity(q_kdl,q_grav_kdl)
    # print "the q and the gravity:", q, q_grav_kdl
    return q_grav_kdl


  def joint_state_callback(self,req):
    if "shoulder_pan_joint" in req.name:
      #this msg contains info for the arm
      q = []
      dq = []
      effort = []
      for joint_name in self.joint_names:
        idx = req.name.index(joint_name)
        q.append(req.position[idx])
        dq.append(req.velocity[idx])
        effort.append(req.effort[idx])
      self.new_gravity_comp_arm(q=q,dq=dq,effort=effort, joint_names = self.joint_names, header=req.header)
    else:
      pass


  def load_required_controllers(self):
    #load torque arm and diff base
    rospy.wait_for_service('switch_robot_controllers')
    switch_controller_request = SwitchControllerRequest()
    switch_controller_request.arm_base_both = "arm"
    switch_controller_request.arm_control = "torque"
    try:
      switch_serv = rospy.ServiceProxy('switch_robot_controllers', SwitchController)
      switch_serv_resp = switch_serv.call(switch_controller_request)
      rospy.loginfo("Switch service finished with condition: %s, and loaded controllers: %s"%(str(switch_serv_resp.success),str(switch_serv_resp.active_controllers)))
    except rospy.ServiceException, e:
      print "Service call failed: %s"%e

  def Matrow_to_matrix(self,matrow):
    Mat = []
    for r in matrow:
      Mat.append(r.row)
    Mat = np.matrix(np.vstack(Mat))
    return Mat

  def new_gravity_comp_arm(self,q=None,dq=None,effort=None, joint_names = None, header=None):
    q = q
    qdot = dq
    J = self.jacobian(q=q)
    I = self.inertia(q=q)
    C = self.coriolis(q=q,qdot=dq)
    G = self.gravity(q=q)
    gravity = G
    coriolis = C
    # applied_effort = effort
    # J = self.Matrow_to_matrix(req.jacobian)
    # inertia_mat = self.Matrow_to_matrix(req.inertia)
    jnt_names = joint_names
    torque = gravity
    torque_dict = dict(zip(jnt_names,torque))
    #command the torques
    joint_command = JointTrajectory()
    joint_command.header = header
    joint_traj_point = JointTrajectoryPoint()
    joint_command.joint_names = [] #this is all joints to command
    joint_traj_point.effort = [] #these are torques to command to matched joint names
    for jnt_name in jnt_names:
      joint_command.joint_names.append(jnt_name)
      jnt_cmd_torque = torque_dict[jnt_name]#+joint_friction[jnt_name]*np.sign()
      joint_traj_point.effort.append(jnt_cmd_torque)
      # rospy.loginfo("%s commanded torque of %f"%(jnt_name,joint_traj_point.effort[-1]))
    joint_command.points = [joint_traj_point]
    self.arm_control_pub.publish(joint_command)





  def gravity_comp_arm(self,req):
    q = req.q
    qdot = req.qdot
    gravity = req.gravity
    coriolis = req.coriolis
    applied_effort = req.effort
    J = self.Matrow_to_matrix(req.jacobian)
    inertia_mat = self.Matrow_to_matrix(req.inertia)
    jnt_names = req.joint_names

    #M(q)*qddot
    if type(self.qdot_prev) != type(None):
      if req.header.stamp.to_sec() - self.qdot_prev["t"] > 0.0:
        qddot = np.matrix([(req.q[idx]- self.qdot_prev["q"][idx])/(req.header.stamp.to_sec() - self.qdot_prev["t"]) for idx in range(len(req.q))]).T
      else:
        qddot = np.matrix([0.0 for idx in range(len(req.q))]).T
    else:
      qddot = np.matrix([0.0 for idx in range(len(req.q))]).T
      self.qdot_prev = {"q":[],"t":0.0}
    inertial_terms = inertia_mat*qddot
    self.qdot_prev["q"] =copy.copy(req.q)
    self.qdot_prev["t"] = copy.copy(req.header.stamp.to_sec())

    friction_jnts_names = ["shoulder_pan_joint",
                          "shoulder_lift_joint",
                          "upperarm_roll_joint",
                          "elbow_flex_joint",
                          "forearm_roll_joint",
                          "wrist_flex_joint",
                          "wrist_roll_joint"]
    joint_friction_vals =[5.3, 13.5, 10.0, 10.0, 1.0, 0.1, 1.2]

    joint_friction = dict(zip(friction_jnts_names,joint_friction_vals))


    #Obtain Frictional Terms


    #Obtain torques
    torque = gravity
    torque_dict = dict(zip(jnt_names,torque))

    #command the torques
    joint_command = JointTrajectory()
    joint_command.header = req.header
    joint_traj_point = JointTrajectoryPoint()
    joint_command.joint_names = [] #this is all joints to command
    joint_traj_point.effort = [] #these are torques to command to matched joint names
    for jnt_name in jnt_names:
      joint_command.joint_names.append(jnt_name)
      jnt_cmd_torque = torque_dict[jnt_name]#+joint_friction[jnt_name]*np.sign()
      joint_traj_point.effort.append(jnt_cmd_torque)
      # rospy.loginfo("%s commanded torque of %f"%(jnt_name,joint_traj_point.effort[-1]))
    joint_command.points = [joint_traj_point]
    self.arm_control_pub.publish(joint_command)

    # rospy.loginfo("commanded torque")


  def gravity_comp_arm_callback(self):
    # rospy.Subscriber('/dynamical_terms',DynamicalTerms,self.gravity_comp_arm,queue_size=1)
    rospy.Subscriber('/joint_states',JointState,self.joint_state_callback, queue_size=1)
    rospy.spin()

  def control_arm_to_compliant_configs(self):
    '''
    This controller goes to one of two compliant joint space poses, 
    alternating between them based on the shoulder_pan_joint angle.
    The switching between these configs is is a hybrid policy to 
    avoid chattering between joint space poses, and is compliant about these poses.
    The switching is based on the angle of the shoulder_pan_joint, 
    hence is a callback of that joint state.
    '''
    pass

  def command_shoulder_pan_joint(self):
    '''
    This function can be listening to a desired shoulder 
    angle message, and the control_arm_to_compliant configs 
    should be listenting to the shoulder_pan_joint angle.
    '''
    pass

  def control_subscribers(self):
    #Start commanding torques for compliant joints 
    # rospy.Subsciber("",,queue_size=1)
    #listen for commands to turning joints
    # rospy.Subsciber("",,queue_size=1)
    pass

def main():
  rospy.init_node("fetch_controller")
  cls_obj = FetchController()

  #Make the controllers available 
  cls_obj.load_required_controllers()

  #test gravity control with friction compensation
  cls_obj.gravity_comp_arm_callback()

  #Command to complaint poses
  # cls_obj.control_subscribers()





if __name__ == '__main__':
  main()