#!/usr/bin/env python2
import numpy as np, math
import rospy, rospkg
import sys, getopt
from intera_core_msgs.msg import JointCommand  #to publish types of joint commands

#my classes
from sawyer_compliant_demo.sawyer_control import sawyer_cntrl_cls 

from sensor_msgs.msg import JointState





class Compliant_class(object):    
    def __init__(self, mode_dict):
        self.sawyer_cntrl_cls = sawyer_cntrl_cls.Sawyer_Control() #velocity control, has a sawyer_cntrl_cls.cmd_vel(self,omega, joints)   

        #determine which mode to enter
        self.cmd_joint_torques = {'right_j0':[], 'right_j1':[], 'right_j2':[], 'right_j3':[], 'right_j4':[], 'right_j5':[], 'right_j6':[]}



    def control_thread(self):
        #Obtain States
        self.cntrl_var_theta = self.subs_var_theta  #locking is occuring on subs_var_mh (to avoid memory accessing issues)


        #Calculate omega input
        """Set the correct input after w call """
        self.t_curr = rospy.Time.now().to_sec()
        self.dt_curr = self.t_curr - self.t_start
        #Calculate theta wrt the upwrite orientation
        theta = self.cntrl_var_theta - self.theta_start
        h = self.cntrl_var_mh
        dh = self.cntrl_var_mh_dot
        omega, t_start_new = self.pouring_calculations.calc_omega(self.dt_curr, theta, h, dh) #omega = -0.5; 
        if type(t_start_new) != list:
            self.t_start = t_start_new
            print "time reset, dt:", self.dt_curr 
        print "\n Desired omega", omega, " theta: ", theta
        # rospy.loginfo("Desired omega %f", omega)
        joint = 'right_j6' 
        #Command omega
        if self.dt_curr > self.final_mh_time:
            #if outside of trajectory time, then just command zero omega
            print "\n Operation complete, w=0 now"
            omega = 0.0; 
        #use dict
        self.sawyer_cntrl_cls.cmd_joints(omega, joint)

        self.mh_plot.append(h)
        self.mh_dot_plot.append(dh)
        self.theta_plot.append(theta)
        self.omega_plot.append(omega)
        self.time_plot.append(self.t_curr - self.time_plot_start)




    def joint_state_callback(self,req):
        self.subs_var_theta = req.position[req.name.index('right_j6')] #locking is occuring on subs_var_mh (to avoid memory accessing issues)        
        vel = req.velocity[req.name.index('right_j6')]
        torque = req.effort[req.name.index('right_j6')]

        joint_names = ['right_j0', 'right_j1', 'right_j2', 'right_j3', 'right_j4', 'right_j5', 'right_j6']

        joint_position_list = [req.position[req.name.index('right_j0')],
                                    req.position[req.name.index('right_j1')],
                                    req.position[req.name.index('right_j2')],
                                    req.position[req.name.index('right_j3')],
                                    req.position[req.name.index('right_j4')],
                                    req.position[req.name.index('right_j5')],
                                    req.position[req.name.index('right_j6')]]

        self.joint_position_state = dict(zip(joint_names,joint_position_list))

        # self.joint_states= req


        self.control_thread()

    def init_subscribers(self):
        print "subscriber node"
        self.t_start = rospy.Time.now().to_sec()
        self.theta_sub = rospy.Subscriber('/robot/joint_states',JointState, self.joint_state_callback) #joint angle callback
        rospy.spin()


def main(argv):
    print "main script"


    rospy.init_node('compliant arm')

    jnt_start_pt = 
    mode_dict = {'task_space_bool':0, 'Kp_gain':1, 'joint_set_pt_list'}

    comp_obj = Compliant_class(mode_dict)
    #init_threads
    comp_obj.init_subscribers()
    #when everything completes, plot
    # pouring_main_obj.plot_results()




if __name__ == '__main__':
    main(sys.argv[1:])
