#!/usr/bin/env python
import rospy
from tf import TransformListener

def main():
  rospy.init_node('test')

  tf = TransformListener(interpolate=True, cache_time=rospy.Duration(1))


  tf_lookup_time_diff = 0.3


  while not rospy.is_shutdown():
    print "frame exists",
    #print tf.frameExists("/base_link") 
    #print tf.frameExists("/map")


    try:
    	gripper_position, gripper_quaternion = tf.lookupTransform('map', 'gripper_link', rospy.Time.now()-rospy.Duration(tf_lookup_time_diff))

    	print "gripper pos,quat", gripper_position, gripper_quaternion

    	t = tf.getLatestCommonTime("map","gripper_link")

    	gripper_position, gripper_quaternion = tf.lookupTransform('map', 'gripper_link',t)


    	print "2nd method gripper pos,quat", gripper_position, gripper_quaternion


    	gripper_position, gripper_quaternion = tf.lookupTransform('map', 'gripper_link',rospy.Time())

    	print "3rd method gripper pos,quat", gripper_position, gripper_quaternion
    except:
        print('An error occurred.')

if __name__ == '__main__':
  main()
