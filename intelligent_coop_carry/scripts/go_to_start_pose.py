#!/usr/bin/env python2
import numpy as np, math
import rospy, time
import sys, getopt
#from intelligent_coop_carry.sawyer_control import sawyer_cntrl_cls 
import rospy
from moveit_msgs.msg import MoveItErrorCodes
from moveit_python import MoveGroupInterface, PlanningSceneInterface
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion


def main():
    rospy.init_node('go_to_start_node')

    # Create move group interface for a fetch robot
    move_group = MoveGroupInterface("arm_with_torso", "base_link")
    move_group.setPlannerId("RRTConnectkConfigDefault")
    # Define ground plane
    # This creates objects in the planning scene that mimic the ground
    # If these were not in place gripper could hit the ground
    planning_scene = PlanningSceneInterface("base_link")  #the frame with which all things are respect to
    planning_scene.removeCollisionObject("the_ground")
    rospy.sleep(1.0)
    planning_scene.addCube("the_ground", 4, 0.0, 0.0, -2.0)

    joint_names = ["torso_lift_joint", "shoulder_pan_joint",
                   "shoulder_lift_joint", "upperarm_roll_joint",
                   "elbow_flex_joint", "forearm_roll_joint",
                   "wrist_flex_joint", "wrist_roll_joint"]

    # joint_poses = [0.37198886275291443, 0.7008262168212891, 0.8200358130004883, -2.1211289460235596, 2.18931578638916, -2.9295489297576904, 1.2221419805603027, 0.8765614851539612]

    #Config A:
    # joint_poses = [ 0.36182644963264465, 0.5721750259399414, 0.7424468994140625, -2.1253304481506348, 1.887946605682373, -2.929136276245117, 0.9644904136657715, 0.7389952540397644]

    #Config B:
    '''
    joint_poses = [0.36182644963264465,
                   -0.5978689193725586,
                    0.7359275817871094,
                     2.14373779296875,
                    1.7487378120422363,
                     2.880049467086792,
                    0.8482909202575684,
                    2.3884081840515137]
    '''

    joint_poses = [0.385966032743454, 
                 0.026844501495361328, 
                 0.8459906578063965, 
                 -3.0526223182678223, 
                 2.0436458587646484, 
                 0.046786416321992874, 
                 -1.162757396697998, 
                 1.5332138538360596]



    # Plans the joints in joint_names to angles in pose
    move_group.moveToJointPosition(joint_names, joint_poses, wait=False)
    # Since we passed in wait=False above we need to wait here
    move_group.get_move_action().wait_for_result()
    result = move_group.get_move_action().get_result()


    if result:
        # Checking the MoveItErrorCode
        if result.error_code.val == MoveItErrorCodes.SUCCESS:
            rospy.loginfo("Disco!")
        else:
            # If you get to this point please search for:
            # moveit_msgs/MoveItErrorCodes.msg
            rospy.logerr("Arm goal in state: %s",
                         move_group.get_move_action().get_state())
    else:
        rospy.logerr("MoveIt! failure no result returned.")

    # This stops all arm movement goals
    # It should be called when a program is exiting so movement stops
    move_group.get_move_action().cancel_all_goals()


if __name__ == '__main__':
    main()


'''
Fetch grasp pose:
name: ['l_wheel_joint', 'r_wheel_joint', 'torso_lift_joint', 'bellows_joint', 'head_pan_joint', 'head_tilt_joint', 'shoulder_pan_joint', 'shoulder_lift_joint', 'upperarm_roll_joint', 'elbow_flex_joint', 'forearm_roll_joint', 'wrist_flex_joint', 'wrist_roll_joint']
position: [-0.37489673495292664, 0.7330383062362671, 0.38797256350517273, 0.184, -0.08384180068969727, -0.006135482894897459, 0.5554814826293946, 1.0232881285217286, -1.9803868825012207, 2.117218961743164, -2.6618694768615723, 1.0986564153747558, 0.8013960941856384]
velocity: [1.1920928955078125e-07, -1.1920928955078125e-07, 2.980232238769531e-07, 1.4901161193847656e-07, 0.0007462501525878906, -0.00015413761138916016, 0.0002815723419189453, -5.996227264404297e-05, 0.00013971328735351562, 0.00023746490478515625, 0.00012946128845214844, 0.0006656646728515625, 3.6656856536865234e-05]
effort: [0.00019872188568115234, -0.00015485286712646484, -43.875, 0.0, -0.0040130615234375, -0.08123779296875, 0.154052734375, -23.03125, -8.9765625, 15.1171875, 3.7578125, -3.318359375, 1.0380859375]
'''
'''
Possible planners:
    - SBLkConfigDefault
    - ESTkConfigDefault
    - LBKPIECEkConfigDefault
    - BKPIECEkConfigDefault
    - KPIECEkConfigDefault
    - RRTkConfigDefault
    - RRTConnectkConfigDefault
    - RRTstarkConfigDefault
    - TRRTkConfigDefault
    - PRMkConfigDefault
    - PRMstarkConfigDefault
'''
