#pragma once
#include <Eigen/Dense>



template <typename Derived>
typename Derived::PlainObject pseudoInverse(Eigen::MatrixBase<Derived> const &m)
{
  // JacobiSVD: thin U and V are only available when your matrix has a dynamic
  // number of columns.
  constexpr auto flags = (Derived::ColsAtCompileTime == Eigen::Dynamic) ?
                             (Eigen::ComputeThinU | Eigen::ComputeThinV) :
                             (Eigen::ComputeFullU | Eigen::ComputeFullV);
  Eigen::JacobiSVD<typename Derived::PlainObject> m_svd(m, flags);
  // std::cout << "singular values: " << m_svd.singularValues().transpose()
  //           << "\n";
  return m_svd.solve(Derived::PlainObject::Identity(m.rows(), m.rows()));
}




