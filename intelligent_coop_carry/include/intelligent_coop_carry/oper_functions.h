#pragma once //This checks to make sure includes aren't done more than once

#define PI 3.14159265

Eigen::Matrix2d RotMat(double th)
{
Eigen::Matrix2d Mat;
Mat << std::cos(th), std::sin(th),
       -std::sin(th), std::cos(th);
return Mat;
}

double angle_diff(double ang1, double ang2)
{
Eigen::Matrix2d M1 = RotMat(ang1);
Eigen::Matrix2d M2 = RotMat(ang2);
Eigen::Matrix2d Mdiff = M1*M2.transpose();
double ang_diff = std::atan2(Mdiff(0,1), Mdiff(0,0));
return ang_diff;
}



