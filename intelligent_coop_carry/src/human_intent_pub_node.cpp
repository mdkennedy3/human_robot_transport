/*
* Copyright (c) <2019>, <Monroe Kennedy III>
* All rights reserved.
*/
#include<gp_human_pred/basic_ros_include.h>
#include <gp_human_pred/gridmap_include.h>
#include <gp_human_pred/extract_intent_features_body_frame.h>
#include <gp_human_pred/visualize_intent.h>
//Bring in native messages
#include <intelligent_coop_carry_msgs/IntentQuantitiesLocal.h>
#include <intelligent_coop_carry_msgs/DynamicalTerms.h>
//Bring in sensor messages
#include <std_msgs/Header.h>
#include <gp_human_pred/WrenchArduino.h>
#include <geometry_msgs/PoseStamped.h>
#include <people_msgs/PositionMeasurementArray.h>
#include <people_msgs/PositionMeasurement.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/WrenchStamped.h>
#include <sensor_msgs/JointState.h>
//Bring in visualization messages
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

class HumanIntent
{
  public:
    HumanIntent(ros::NodeHandle &nh);
    ~HumanIntent();

    void WrenchCallback(const intelligent_coop_carry_msgs::DynamicalTerms& msg);
    void OdomCallback(const nav_msgs::Odometry& msg);
    void HeadPoseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
    void FootPoseCallback(const people_msgs::PositionMeasurementArray::ConstPtr& msg);
    void Publish_Viable_Person_Position(const people_msgs::PositionMeasurement& viable_person);
    void LaserScanCallback(const sensor_msgs::LaserScan::ConstPtr& msg);

    void PubHumanIntent(std_msgs::Header& header);

    ros::Publisher local_intent_quantities_publisher;
    ros::Publisher local_map_publisher;

  private:
    //class obj 
    humanintentcls::TransformIntentQuantities trans_intent_quant_obj_;
    VisualizeHumanIntentClass vis_human_intent_obj_; 
    //
    ros::NodeHandle pnh_;
    //Subscribe to topics
    ros::Subscriber wrench_sub_;
    ros::Subscriber sys_odom_sub_;
    ros::Subscriber laser_sub_;
    ros::Subscriber head_pose_sub_;
    ros::Subscriber foot_pose_sub_;    
    //Publisher
    ros::Publisher human_transport_pub_;
    ros::Publisher RH_applied_wrench_;
    ros::Publisher LH_applied_wrench_;
    ros::Publisher Net_applied_wrench_;
    ros::Publisher viable_person_step_visual_;
    //Variables
    geometry_msgs::WrenchStamped wrench_msg_output_;
    sensor_msgs::LaserScan scan_msg_output_;
    geometry_msgs::PoseStamped head_pose_msg_output_;
    people_msgs::PositionMeasurement foot_pose_msg_output_;
    bool foot_pose_populated_ever_;
    geometry_msgs::WrenchStamped follower_wrench_calculated_output_;
    double person_laser_dx_max_;
    double person_laser_dy_max_;
    nav_msgs::Odometry sys_odom_;
    bool sys_odom_populated_;  
    std::string  system_frame_name_;
    std::string  world_frame_name_;
    std::string laser_frame_name_;
    float local_obstacle_radius_;
    int pose_header_seq_;
    bool debug_msgs_bool_;
    geometry_msgs::TransformStamped system_pose_prev_; //used for manually finding velocity of system frame
    double system_pose_diff_time_;
    double system_pose_diff_prev_time_;
    bool prev_sys_pose_exists_;
    bool transform_bool_odom_;
    geometry_msgs::TransformStamped  system_pose; //pose of system in world frame
    //Tf in case needed
    tf2_ros::Buffer tfBuffer;
    std::unique_ptr<tf2_ros::TransformListener> tfListener;
    tf2_ros::TransformBroadcaster tfbroadcast;
    //debug checker
    ros::Publisher wrench_checker_publisher_;
    ros::Publisher system_pose_checker_publisher_;
    ros::Publisher step_checker_publisher_;
    ros::Publisher head_checker_publisher_;
    ros::Publisher head_step_pose_vect_publisher_;
};

HumanIntent::HumanIntent(ros::NodeHandle &nh) : pnh_(nh)
{
  ROS_INFO("constructor");
  //Estabilish the transformer
  tfListener.reset(new tf2_ros::TransformListener(tfBuffer));
  //Subscribers
  wrench_sub_ = pnh_.subscribe("/dynamical_terms",1,&HumanIntent::WrenchCallback,this);
  sys_odom_sub_ = pnh_.subscribe("/odom_combined",1,&HumanIntent::OdomCallback,this); //was joint states, note that this is for the base, if the base moves this will update regardless of the held object, handle this (transform to gripper)
  laser_sub_ = pnh_.subscribe("/base_scan",1,&HumanIntent::LaserScanCallback,this);
  head_pose_sub_ = pnh_.subscribe("/head_pose",1,&HumanIntent::HeadPoseCallback,this);
  foot_pose_sub_ = pnh_.subscribe("/leg_tracker_measurements",1,&HumanIntent::FootPoseCallback,this);
  //Publishers
  local_intent_quantities_publisher = pnh_.advertise<intelligent_coop_carry_msgs::IntentQuantitiesLocal>("local_intent_quantities",1);
  local_map_publisher = pnh_.advertise<sensor_msgs::Image>("local_map_pub",1);
  //Visualize outputs
  Net_applied_wrench_ = nh.advertise<geometry_msgs::WrenchStamped>("/Net_applied_wrench",1);
  viable_person_step_visual_ = nh.advertise<visualization_msgs::Marker>("/viable_person_vis",1); 
  //Establish this bool to use when filtering ppl, to determine if the name should be checked
  foot_pose_populated_ever_ = false;
  //Initialize certain variables
  person_laser_dx_max_ = 1.5;//0.7; //Meters ([0,dx]) //Make the max distance position can be from laser to be considered valid
  person_laser_dy_max_ = 0.5; //Meters (either side [-dy,dy])
  sys_odom_populated_ = false;
  local_obstacle_radius_ = 4.0; //meters (radius around system to consider obstacles)
  system_pose_diff_time_ = 1.0; //seconds (intialize)
  system_pose_diff_prev_time_ = ros::Time::now().toSec();//start time
  prev_sys_pose_exists_ = false;
  transform_bool_odom_ = false;
  pose_header_seq_ = 0;
  pnh_.param<std::string>("world_frame_name", world_frame_name_, "map");
  pnh_.param<std::string>("system_frame_name", system_frame_name_, "base_combined_link");
  pnh_.param<std::string>("laser_frame_name", laser_frame_name_, "laser_link");
  pnh_.param<bool>("debug_msgs", debug_msgs_bool_,true);
  //preset headers
  head_pose_msg_output_.header.frame_id = world_frame_name_;
  foot_pose_msg_output_.header.frame_id = world_frame_name_;
}

HumanIntent::~HumanIntent()
{}


void HumanIntent::PubHumanIntent(std_msgs::Header& header)
{
  //When this callback returns, utilize all values interior and also call local map evaluator, then transform the quanities and publish for use
  //0. generate converted types
  geometry_msgs::WrenchStamped local_wrench;
  // nav_msgs::Odometry local_odom;
  geometry_msgs::TwistStamped local_odom_twist;
  //Initial vectors in world frame must first be obtained
  geometry_msgs::Vector3Stamped step_vect_world_frame;
  geometry_msgs::Vector3Stamped head_vect_world_frame;
  //When returned, the following will have been scaled by the twist vector after being normalized
  geometry_msgs::Vector3Stamped local_step_vect;
  geometry_msgs::Vector3Stamped local_head_vect;
  //For map
  grid_map::GridMap local_gridmap_worldframe({"layer"});
  grid_map::GridMap robo_frame_local_map({"layer"});
  grid_map_msgs::GridMap robo_frame_local_map_msg;
  sensor_msgs::Image local_map_img_robo_frame;
  //1. query system odom and system transform
  if(sys_odom_populated_ == false || transform_bool_odom_ == false)
  {
    ROS_WARN("Unable to obtain transform from system to world");
    return; //sys odom hasn't been filled or transform not available
  }
  ROS_WARN("Obtained transform from system to world");
  //Transform all to local frame
  //2. Convert human step to vector then 
  bool prev_step_pop = trans_intent_quant_obj_.UpdateStepVector(foot_pose_msg_output_, step_vect_world_frame);
  if(prev_step_pop==false)
  {
    return; //two steps have not yet been observed, return
    ROS_WARN("Two steps have not been observed in intent_regulator");
  }
  trans_intent_quant_obj_.ConvertHeadPoseToVector(head_pose_msg_output_, head_vect_world_frame);
  //3. Obtain Local map
  bool map_obtained = trans_intent_quant_obj_.LocalMapGenerator(sys_odom_, local_gridmap_worldframe, local_obstacle_radius_);
  if (map_obtained == false) 
  {
    return; //no map, exit
  }
  //4. Convert all intent quantities to body fixed frame
  trans_intent_quant_obj_.ObtainLocalStepVect(step_vect_world_frame, local_step_vect, system_pose);
  trans_intent_quant_obj_.ObtainLocalHeadVect(head_vect_world_frame, local_head_vect, system_pose);
  trans_intent_quant_obj_.ObtainLocalWrenchVect(wrench_msg_output_, local_wrench, system_pose);
  trans_intent_quant_obj_.ObtainLocalTwist(sys_odom_, local_odom_twist, system_pose);
  trans_intent_quant_obj_.ObtainLocalMap(local_gridmap_worldframe, robo_frame_local_map, local_map_img_robo_frame, system_pose);
  grid_map::GridMapRosConverter::toMessage(robo_frame_local_map, robo_frame_local_map_msg); //grid_map::GridMapRosConverter::fromOccupancyGrid(*msg, "layer", map);
  //5. Scale the head/step vectors by the twist
  trans_intent_quant_obj_.PlanarVectorScaledByTwist(local_step_vect, local_odom_twist);
  trans_intent_quant_obj_.PlanarVectorScaledByTwist(local_head_vect, local_odom_twist);
  //6. construct all quantities into a message (with pose in world frame) and publish
  intelligent_coop_carry_msgs::IntentQuantitiesLocal local_intent_quanities_msg;
  local_intent_quanities_msg.header = header;
  local_intent_quanities_msg.header.frame_id = system_frame_name_;
  local_intent_quanities_msg.system_pose = system_pose;
  local_intent_quanities_msg.local_step_vect = local_step_vect;
  local_intent_quanities_msg.local_head_vect = local_head_vect;
  local_intent_quanities_msg.local_wrench = local_wrench;
  local_intent_quanities_msg.local_twist = local_odom_twist;
  local_intent_quanities_msg.local_gridmap = robo_frame_local_map_msg;
  local_intent_quanities_msg.local_gridmap_img = local_map_img_robo_frame;
  //7. Publish
  local_intent_quantities_publisher.publish(local_intent_quanities_msg);
  local_map_publisher.publish(local_map_img_robo_frame);
  //Visualize Vectors
  if(debug_msgs_bool_)
  {
    wrench_checker_publisher_.publish(local_wrench);
    system_pose_checker_publisher_.publish(system_pose);
    step_checker_publisher_.publish(local_step_vect);
    head_checker_publisher_.publish(local_head_vect);
    vis_human_intent_obj_.VisualizeSystemPoseHeadStepVectors(system_pose,local_step_vect,local_head_vect,local_odom_twist, head_step_pose_vect_publisher_);
  }
  //reset bools
  if(sys_odom_populated_)
  {
    sys_odom_populated_ = false; //reset
  }
  if(transform_bool_odom_)
  {
    transform_bool_odom_ = false;
  }

}

void HumanIntent::WrenchCallback(const intelligent_coop_carry_msgs::DynamicalTerms& msg)
{
  wrench_msg_output_ = msg.wrench_ext; //pass wrench  
  Net_applied_wrench_.publish(wrench_msg_output_);
}

void HumanIntent::OdomCallback(const nav_msgs::Odometry& msg)
{
  //Odom message for the combined system, we care about the odometry for the carried load
  //First get current transform of frame "base_combined_link" in "map" frame  (note in odom message)
  sys_odom_.header = msg.header;
  sys_odom_.header.frame_id = world_frame_name_;
  sys_odom_.child_frame_id = system_frame_name_;
  // geometry_msgs::TransformStamped  system_pose; //pose of system in world frame
  transform_bool_odom_ = trans_intent_quant_obj_.ObtainTransform(world_frame_name_, system_frame_name_, system_pose);
  if(transform_bool_odom_ == false)
  {
    ROS_WARN("Unable to obtain transform from system to world");
    return; //sys odom hasn't been filled or transform not available
  }
  //Pose was obtained successfully:
  //Use this to define current pose (position/angle)
  sys_odom_.pose.pose.position.x = system_pose.transform.translation.x;
  sys_odom_.pose.pose.position.y = system_pose.transform.translation.y;
  sys_odom_.pose.pose.position.z = system_pose.transform.translation.z;
  sys_odom_.pose.pose.orientation.x = system_pose.transform.rotation.x;
  sys_odom_.pose.pose.orientation.y = system_pose.transform.rotation.y;
  sys_odom_.pose.pose.orientation.z = system_pose.transform.rotation.z;
  sys_odom_.pose.pose.orientation.w = system_pose.transform.rotation.w;
  //Update iterative time (for dt to get velocities)
  system_pose_diff_time_ = ros::Time::now().toSec() -  system_pose_diff_prev_time_;
  system_pose_diff_prev_time_ = ros::Time::now().toSec();
  //Now obtain velocities: 
  if(prev_sys_pose_exists_)
  {
    //previous pose exists, manually differentiate for the twist
    sys_odom_.twist.twist.linear.x = (system_pose.transform.translation.x - system_pose_prev_.transform.translation.x)/system_pose_diff_time_;
    sys_odom_.twist.twist.linear.y = (system_pose.transform.translation.y - system_pose_prev_.transform.translation.y)/system_pose_diff_time_;
    sys_odom_.twist.twist.linear.z = (system_pose.transform.translation.z - system_pose_prev_.transform.translation.z)/system_pose_diff_time_;
    //Convert to Eigen affine matricies
    Eigen::Affine3d sys_pose_eig = tf2::transformToEigen(system_pose);
    Eigen::Affine3d sys_pose_prev_eig = tf2::transformToEigen(system_pose_prev_);
    //Get rotational diff between the frames
    Eigen::Affine3d pose_diff_eig = sys_pose_eig.inverse() * sys_pose_prev_eig; // want current pose in prev frame (rotation describes)
    //Extract rotational vector
    Eigen::AngleAxisd rot_diff(pose_diff_eig.rotation());
    //Put this vector into the world frame
    Eigen::Vector3d diff_rot_vect =  sys_pose_prev_eig.rotation() * rot_diff.angle() * rot_diff.axis();//This multiplis the diff vect which is in prev frame to world frame
    //Diff this vector with time to get the angular velocity vector
    sys_odom_.twist.twist.angular.x = (diff_rot_vect(0))/system_pose_diff_time_;
    sys_odom_.twist.twist.angular.y = (diff_rot_vect(1))/system_pose_diff_time_;
    sys_odom_.twist.twist.angular.z = (diff_rot_vect(2))/system_pose_diff_time_;
  }
  prev_sys_pose_exists_ = true; //needs to be set true once
  system_pose_prev_ = system_pose;
  sys_odom_populated_ = true;
  // PubHumanIntent(sys_odom_.header); //collects all variables through class
}


void HumanIntent::HeadPoseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  //Collect msg and populate variable
  //The message just has this as the x-axis in the head-origin frame, instead populate this in the world frame (mocap), but allow it to be triggered here
  geometry_msgs::TransformStamped head_origin_tf;
  std::string head_frame = "head_origin";
  bool tf_success = trans_intent_quant_obj_.ObtainTransform(world_frame_name_, head_frame, head_origin_tf);
  if(tf_success) //Only proceed if tf's were obtained to perform calculations
  {
    //obtain the msg output (header file primary)
    head_pose_msg_output_ = *msg;
    //Make the frame correct
    head_pose_msg_output_.header.frame_id = world_frame_name_;
    //Populate with correct transform
    head_pose_msg_output_.pose.position.x = head_origin_tf.transform.translation.x;
    head_pose_msg_output_.pose.position.y = head_origin_tf.transform.translation.y;
    head_pose_msg_output_.pose.position.z = head_origin_tf.transform.translation.z;
    head_pose_msg_output_.pose.orientation = head_origin_tf.transform.rotation;
  }//send head origin if tf call successful
}
void HumanIntent::FootPoseCallback(const people_msgs::PositionMeasurementArray::ConstPtr& msg)
{
  /*
  Collect msg, filter to feet positions in laser-scan frame that are directly 
  in front (present variables for x,y), and report that position (and name) 
  If there are more than one individual in the space, then if one of them has 
  same name as previous value, then store the one with the same 'person' name
  */
  people_msgs::PositionMeasurementArray pos_meas_array = *msg;
  //1. Check that the list of people is not empty
  bool viable_person_found = false;
  if(pos_meas_array.people.size()>0)
  {
    //2. Loop through the msg array of people, storing those that meet criteria, (if none meet criteria, then simply do not update the pose variable)
    people_msgs::PositionMeasurementArray pos_meas_array_filtered;
    people_msgs::PositionMeasurement viable_person; //The most viable person candidate
    //Obtain transform from default frame of message to laser frame (using first element frame id)
    std::string curr_step_frame = pos_meas_array.people[0].header.frame_id;
    geometry_msgs::TransformStamped step_frame_tf;
    bool tf_success = trans_intent_quant_obj_.ObtainTransform(laser_frame_name_, curr_step_frame, step_frame_tf);
    if(tf_success == false)
    {
      ROS_WARN("No transformation from step frame to laser frame");
      return; 
    }
    //tf was success:
    Eigen::Affine3d step_frame_tf_eig = tf2::transformToEigen(step_frame_tf); //get TF mat
    for(int i=0; i<pos_meas_array.people.size(); i++){
      people_msgs::PositionMeasurement curr_person = pos_meas_array.people[i]; //Current person
      //Make this a vector and rotate into laser frame
      Eigen::Vector3d curr_person_original_frame, curr_person_laser_frame;
      curr_person_original_frame[0] = curr_person.pos.x;
      curr_person_original_frame[1] = curr_person.pos.y;
      curr_person_original_frame[2] = curr_person.pos.z;
      curr_person_laser_frame = step_frame_tf_eig *curr_person_original_frame;
      curr_person.pos.x = curr_person_laser_frame[0];
      curr_person.pos.y = curr_person_laser_frame[1];
      curr_person.pos.z = curr_person_laser_frame[2];
      curr_person.header.frame_id = laser_frame_name_;
      //Check if in laser frame a) x is in [0,dx] and b) y in [-dy,dy]
      if((curr_person.pos.x>0.0) && (curr_person.pos.x < person_laser_dx_max_) && (curr_person.pos.y > -person_laser_dy_max_) && (curr_person.pos.y < person_laser_dy_max_) ){
        pos_meas_array_filtered.people.push_back(curr_person); //If it satisfies the filtering condition, then add it to the filtered array
      }
      //If this set is non-empty then proceed
      if(pos_meas_array_filtered.people.size()>0)
      {
        viable_person_found = true;
        viable_person = pos_meas_array_filtered.people[0]; //default to first in list, but update if same name is found
        if(foot_pose_populated_ever_){
          std::string prev_name = foot_pose_msg_output_.name; //previous name
          //was previously populated, now check to see if there is a name that was viable (this is not guaranteed, just highly probable that same name will persist)
          for (int jdx=0; jdx<pos_meas_array_filtered.people.size(); jdx++){
            std::string test_name = pos_meas_array_filtered.people[jdx].name;
            if (prev_name.compare(test_name) == 0){
              //These are the same string
              viable_person = pos_meas_array_filtered.people[jdx];
              break;//exit because same string was found
            }
          }
        }else{
          foot_pose_populated_ever_ = true; //there was something in the list, so make this true for next time
        }
      }else{
        foot_pose_populated_ever_ = false; //Reset this, as all viable candidates were lost
      }
    }
    /*
    3. (if this isn't first trial) then compare names of valid members with previous names, 
    if there is a match , then retain that as the person, if there is no match then take 
    one with smallest y value (as person should be centered on y axis)
    */
    if(viable_person_found){
      foot_pose_msg_output_ = viable_person;
      Publish_Viable_Person_Position(viable_person); //visualize the viable person pose
    }    
  }
}
void HumanIntent::Publish_Viable_Person_Position(const people_msgs::PositionMeasurement& viable_person)
{
  //Publish the viable person position (convert from person msg to marker type)
  visualization_msgs::Marker marker;
  marker.header = viable_person.header;
  marker.id = std::atoi(viable_person.object_id.c_str()) ;// std::stoi(viable_person.object_id);
  marker.type=2;
  marker.pose.position.x = viable_person.pos.x;
  marker.pose.position.y = viable_person.pos.y;
  marker.pose.position.z = viable_person.pos.z;
  marker.scale.x = 0.20;
  marker.scale.y = 0.20;
  marker.scale.z = 0.20;
  marker.color.a = 1.0; //marker transparency
  marker.color.r = 0.0;
  marker.color.g = 0.0;
  marker.color.b = 1.0;
  marker.lifetime = ros::Duration(5);
  //Now publish the marker
  viable_person_step_visual_.publish(marker);
}
void HumanIntent::LaserScanCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
  //Collect msg and populate variable
  scan_msg_output_ = *msg; // * de-references the pointer msg
  //Call the publish node to populate the new message from this subscriber
  PubHumanIntent(scan_msg_output_.header); //collects all variables through class
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "human_intent_pub_node");
  ROS_INFO("HumanIntent_node_running");
  ros::NodeHandle nh("~");
  HumanIntent human_intent_obj(nh);
  ros::spin();
  return 0;
}

