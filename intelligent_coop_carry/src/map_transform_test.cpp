#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>

int main(int argc, char** argv){
  ros::init(argc, argv, "my_tf_listener");

  ros::NodeHandle node;

  tf::TransformListener listener;

  ros::Publisher head_pose = node.advertise<geometry_msgs::Pose>("head_map_tf",10);

  ros::Rate rate(10.0);
  while (node.ok()){
    tf::StampedTransform transform;
    try{
      listener.lookupTransform("/head_origin", "/map", ros::Time::now(), transform);
      //listener.lookupTransform("/base_link", "/map", ros::Time::now(), transform);
    }
    catch (tf::TransformException &ex) {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
      continue;
    }

    geometry_msgs::Pose head_pose_obj;
    
    head_pose_obj.position.x = transform.getOrigin().x();
    head_pose_obj.position.y = transform.getOrigin().y();
    head_pose_obj.position.z = transform.getOrigin().z();
    
    head_pose_obj.orientation.x = transform.getRotation().x();
    head_pose_obj.orientation.y = transform.getRotation().y();
    head_pose_obj.orientation.z = transform.getRotation().z();
    head_pose_obj.orientation.w = transform.getRotation().w();

    head_pose.publish(head_pose_obj);

    rate.sleep();
  }
  return 0;
};
