/*
* Copyright (c) <2016>, <Dinesh Thakur>
* All rights reserved.
*
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*   1. Redistributions of source code must retain the above copyright notice,
*   this list of conditions and the following disclaimer.
*
*   2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
*   3. Neither the name of the University of Pennsylvania nor the names of its
*   contributors may be used to endorse or promote products derived from this
*   software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "ros/ros.h"
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/GetMap.h>
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>
#include <grid_map_cv/grid_map_cv.hpp>

class GetCostmap
{
public:
  GetCostmap();
  void costCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg);
private:

  ros::NodeHandle nh_;
  ros::Publisher cost_pub_;
  ros::Subscriber cost_sub_;

  double resolution_;

};

GetCostmap::GetCostmap()
{
  nh_ = ros::NodeHandle("~");
  nh_.param("resolution", resolution_, 0.1);
  cost_pub_ = nh_.advertise<grid_map_msgs::GridMap>("/gridmap_subsampled", 1);
  cost_sub_ = nh_.subscribe<nav_msgs::OccupancyGrid>("/move_base/local_costmap/costmap", 10, boost::bind(&GetCostmap::costCallback, this, _1));
}

void GetCostmap::costCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{

  grid_map::GridMap map({"layer"});
  grid_map::GridMapRosConverter::fromOccupancyGrid(*msg, "layer", map);
  //map.setPosition(grid_map::Position(req.map.info.origin.position.x, req.map.info.origin.position.y));
  ROS_INFO("Origin %g %g, Res: %g, size: %g, %g m (%d, %d cells)", map.getPosition().x(), map.getPosition().y(), map.getResolution(),
    map.getLength().x(), map.getLength().y(), map.getSize()(0), map.getSize()(1));


  // Change resoltion of grid map.
  grid_map::GridMap modifiedMap;
  grid_map::GridMapCvProcessing::changeResolution(map, modifiedMap, resolution_);
  ROS_INFO("Modified Origin %g %g, Res: %g, size: %g, %g m (%d, %d cells)", modifiedMap.getPosition().x(), modifiedMap.getPosition().y(), modifiedMap.getResolution(),
    modifiedMap.getLength().x(), modifiedMap.getLength().y(), modifiedMap.getSize()(0), modifiedMap.getSize()(1));

  // Publish grid map.
  grid_map_msgs::GridMap message;
  grid_map::GridMapRosConverter::toMessage(modifiedMap, message);

  //Publish the pointcloud
  cost_pub_.publish(message);

}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "get_costmap");

  GetCostmap GetCostmap;
  ros::spin();
}