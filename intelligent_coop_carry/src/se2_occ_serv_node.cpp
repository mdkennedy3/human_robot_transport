/*
* Copyright (c) <2017>, <Monroe Kennedy III>
* All rights reserved.
*/
#include <iostream>
#include "ros/ros.h"
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/PolygonStamped.h>

#include <nav_msgs/GetMap.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/OccupancyGrid.h>

#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/ChannelFloat32.h> //For point cloud channels, (e.g. cost at each point)
#include <cmath>
#include <map> //dictionary equivalent
#include <algorithm> // for std::min(a,b) = a

#include <intelligent_coop_carry/Se2_occ_pts.h> //Service Struct

#define M_PI 3.14159265358979323846

//OMPL: 
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/geometric/planners/rrt/RRT.h>

#include <Eigen/Dense>
#include <unsupported/Eigen/MatrixFunctions>


#include <ompl/config.h>
namespace ob = ompl::base;
namespace og = ompl::geometric;


//For TF messages
#include <tf/transform_listener.h>


/*
TODO: 
1. make radial and theta bounding functions (radial functions) a function of the absolute value of omega, vel and force (add force) [magnitude]

*/


class MyStateSpace : public ob::SE2StateSpace
{
  public:

  double  distance (const ob::State *state1, const ob::State *state2) const {
    double dist1 = ob::CompoundStateSpace::distance(state1, state2);

    const ompl::base::SE2StateSpace::StateType *se2_state1 = state1->as<ompl::base::SE2StateSpace::StateType>();    
    double x1 = (*se2_state1).getX();
    double y1 = (*se2_state1).getY();
    double yaw1 = (*se2_state1).getYaw();
            
    const ompl::base::SE2StateSpace::StateType *se2_state2 = state2->as<ompl::base::SE2StateSpace::StateType>();
    double x2 = (*se2_state2).getX();
    double y2 = (*se2_state2).getY();
    double yaw2 = (*se2_state2).getYaw();
    

    float c1 = std::cos(yaw1);
    float s1 = std::sin(yaw1);
    float c2 = std::cos(yaw2);
    float s2 = std::sin(yaw2);
    float c_delta = c1*c2 + s1*s2;
    float s_delta = c1*s2 - c2*s1;
    float angle_diff = std::atan2(s_delta, c_delta);


    double dist2 = std::sqrt(std::pow(x1 - x2,2) + std::pow(y1 - y2,2)) + std::abs(angle_diff);
    // std::cout << " --- d1: " << dist1 << " d2: " << dist2 << std::endl;
                    
    return dist2;
  }
};



class Se2_Occ
{
  public:
    Se2_Occ();
    bool serviceCallback(intelligent_coop_carry::Se2_occ_pts::Request &req, intelligent_coop_carry::Se2_occ_pts::Response  &res);

    std::vector<geometry_msgs::Point> rotate_hull_pts(std::vector<geometry_msgs::Point> hull_pts, double theta, geometry_msgs::Point32 point_center);

    bool current_system_pose(geometry_msgs::Pose &system_pose);
    bool convex_hull_display(geometry_msgs::PolygonStamped &cvx_hull_polygon,std::vector<geometry_msgs::Point> system_hull_pts, geometry_msgs::Pose sys_pose); //Plot system hull points


    //cost functional calculation funcitons
    float dot_product_operator(geometry_msgs::Vector3 v1, geometry_msgs::Vector3 v2);
    geometry_msgs::Point cross_product_operator(geometry_msgs::Point v1, geometry_msgs::Point v2);
    float radial_sigmoid_fnct(float vel_curr, float sensor_radius);
    float radial_sigmoid_fnct_theta(float ang_vel); 
    float vect_norm(geometry_msgs::Vector3 vect);
    float squared_norm_diff(geometry_msgs::Vector3 v1, geometry_msgs::Vector3 v2);

    bool human_intention_cost(sensor_msgs::PointCloud &free_cells_pc, geometry_msgs::Pose sys_pose, geometry_msgs::Vector3 step_vector, geometry_msgs::Vector3 force_vector, geometry_msgs::Twist sys_twist);
    bool obstacle_dist_cost(sensor_msgs::PointCloud &free_cells_pc, sensor_msgs::PointCloud &occ_cells_pc);
    bool radial_attraction_cost(sensor_msgs::PointCloud &free_cells_pc, geometry_msgs::Pose system_pose, geometry_msgs::Twist sys_twist , double sensor_radius); 
    // bool path_cost(sensor_msgs::PointCloud &free_cells_pc, geometry_msgs::Pose robot_pose);
    bool summed_cost_funct(sensor_msgs::PointCloud &free_cells_pc);
    geometry_msgs::Point find_optimal_goal_pose(sensor_msgs::PointCloud &free_cells_pc);

    geometry_msgs::Point convert_geometry_msgs_pt32_to_pt(geometry_msgs::Point32 initial_point);
    geometry_msgs::Point32 convert_geometry_msgs_pt_to_pt32(geometry_msgs::Point initial_point);

    geometry_msgs::Vector3 rotation_difference(Eigen::MatrixXd Rtip, Eigen::MatrixXd Rtail);
    float zaxis_angle_diff(float theta_tip, float theta_tail);


    //For path planning
    nav_msgs::Path path_plan(sensor_msgs::PointCloud &free_cells_pc, sensor_msgs::PointCloud &occ_cells_pc, geometry_msgs::Pose robot_pose, geometry_msgs::Point goal_pose, double sensor_radius);
    bool isStateValid(const ob::State *state);



  private:
    float float_sign(float value); //cause the built in std:signbit returns a bool not +1,-1 floats

    ros::NodeHandle nh_;
    ros::ServiceServer se2_occ_pts_service_;
    ros::Publisher pc_occ_pub_;
    ros::Publisher pc_free_pub_;
    ros::Publisher opt_path_pub_;
    ros::Publisher opt_path_point_;

    ros::Publisher convex_hull_display_pub_;

    //add publisher for grid map
    ros::Publisher grid_map_local_pub_;


    std::map<std::string,double> hi_weights; //weights for intention vectors
    double obstacle_cost_sigma = 1.0; //Set the obstacle sigma here
    double obstacle_cost_gain = 1.0; //Set the obstacle gain here
    double radial_cost_gain = 1.0; //Set radial cost gain here
    double radial_cost_sigma = 1.0; //Set sigma for radial attraction cost

    //Make the point clouds global variables for ease of use with OMPL state checker
    sensor_msgs::PointCloud free_pc_global; 
    sensor_msgs::PointCloud occ_pc_global; 
    geometry_msgs::Pose robot_pose_curr_for_ompl;
    float sensor_radius_global;

    grid_map::GridMap local_layer_map_;
    std::vector<double> theta_list_global_;

    tf::TransformListener tf_listener_;

    // double occupied_cell_threshold_ = 0.5
};

//Constructor
Se2_Occ::Se2_Occ()
{
  ROS_INFO("Service is ready to Play!");
  nh_ = ros::NodeHandle("~");
  pc_occ_pub_ = nh_.advertise<sensor_msgs::PointCloud>("/pc_occ", 1);
  pc_free_pub_ = nh_.advertise<sensor_msgs::PointCloud>("/pc_free", 1);
  opt_path_pub_ = nh_.advertise<nav_msgs::Path>("/opt_path",1);
  opt_path_point_ = nh_.advertise<geometry_msgs::PointStamped>("/opt_path_point",1);

  convex_hull_display_pub_ = nh_.advertise<geometry_msgs::PolygonStamped>("/cvx_hull_display",1);

  grid_map_local_pub_ = nh_.advertise<grid_map_msgs::GridMap>("grid_map", 1, true);


  hi_weights["force_vector"] = 0.3;
  hi_weights["step_vector"] = 0.3;
  hi_weights["velocity_vector"] = 0.3;
  hi_weights["angular_vector"] = 0.1;  //This magnitude should change the importance of direction on the cost of cells (radians vs meters) 
  hi_weights["orient_vector"] = 0.1;

  se2_occ_pts_service_ = nh_.advertiseService("/se2_occupancy_service", &Se2_Occ::serviceCallback, this); //This is equivalent to self in python
}

std::vector<geometry_msgs::Point>  Se2_Occ::rotate_hull_pts(std::vector<geometry_msgs::Point> hull_pts, double theta, geometry_msgs::Point32 point_center){
  //Hull points are list of geometry::points that comes in
  std::vector<geometry_msgs::Point> rotated_hull_pts;
  for(int hull_idx = 0; hull_idx < hull_pts.size(); hull_idx++ ){
    geometry_msgs::Point rotated_point;
    double hull_curr_pt_x = hull_pts[hull_idx].x;
    double hull_curr_pt_y = hull_pts[hull_idx].y;
    rotated_point.x = cos(theta)*hull_curr_pt_x + sin(theta)*hull_curr_pt_y + point_center.x;
    rotated_point.y = -sin(theta)*hull_curr_pt_x + cos(theta)*hull_curr_pt_y + point_center.y;
    rotated_hull_pts.push_back(rotated_point);
  }
  return rotated_hull_pts;
}

bool Se2_Occ::current_system_pose(geometry_msgs::Pose &system_pose){
  tf::StampedTransform transform;
  try{
    //from this _1 to this _2
    // tf_listener_.lookupTransform("/base_link", "/map", ros::Time(0), transform);
    tf_listener_.lookupTransform("/map", "/base_link", ros::Time(0), transform);
  }
  catch (tf::TransformException ex){
    ROS_ERROR("%s",ex.what());
    ros::Duration(1.0).sleep();
    return false;
  }
  // ros::spinOnce(); //get the transform
  //Put this into a geometry_msgs pose form
  // geometry_msgs::PoseStamped system_pose;
  // tf::PoseStampedTFToMsg(transform, system_pose);
  // geometry_msgs::Pose system_pose;

  tf::poseTFToMsg(transform, system_pose);
  return true;
}


bool Se2_Occ::convex_hull_display(geometry_msgs::PolygonStamped &cvx_hull_polygon,std::vector<geometry_msgs::Point> system_hull_pts, geometry_msgs::Pose sys_pose){

  std::vector<geometry_msgs::Point32> polygon_pt_list;
  for(int index = 0; index < system_hull_pts.size(); ++index){
    geometry_msgs::Point32 curr_pt;
    //could just use quaternion rotation form and do this properly (for 3D rotations)
    float sys_theta = 2*std::atan2(sys_pose.orientation.z,sys_pose.orientation.w);
    curr_pt.x = std::cos(sys_theta)*system_hull_pts[index].x + -std::sin(sys_theta)*system_hull_pts[index].y  + sys_pose.position.x;
    curr_pt.y = std::sin(sys_theta)*system_hull_pts[index].x + std::cos(sys_theta)*system_hull_pts[index].y  + sys_pose.position.y;
    curr_pt.z = system_hull_pts[index].z  + sys_pose.position.z;
    polygon_pt_list.push_back(curr_pt);
  }
  cvx_hull_polygon.polygon.points = polygon_pt_list;
  convex_hull_display_pub_.publish(cvx_hull_polygon); //Just publish here
}



bool Se2_Occ::serviceCallback(intelligent_coop_carry::Se2_occ_pts::Request &req, intelligent_coop_carry::Se2_occ_pts::Response  &res){
  //Set frames for returned point clouds
  // res.occ_pts.header.frame_id = req.map.header.frame_id;
  // res.free_pts.header.frame_id = req.map.header.frame_id;


  //Access system pose:
  geometry_msgs::Pose robot_pose;

  bool obtained_pose = current_system_pose(robot_pose); //re-assign

  if (!obtained_pose){
    return false;
  }

  //Get the map
  // boost::shared_ptr<geometry_msgs::PoseStamped const>   req_map;
  boost::shared_ptr<const nav_msgs::OccupancyGrid_<std::allocator<void> > > req_map_ptr;
  req_map_ptr = ros::topic::waitForMessage<nav_msgs::OccupancyGrid>("/map", ros::Duration(1));


  if(req_map_ptr == NULL){
    ROS_ERROR("Robot map not published on /map");
    return false;
  }

  nav_msgs::OccupancyGrid req_map = *req_map_ptr;

  res.occ_pts.header = req_map.header;
  res.free_pts.header = req_map.header;
  /* 1. Read in terms */
  grid_map::GridMap map({"layer"});//1a. Read in the map
  grid_map::GridMapRosConverter::fromOccupancyGrid(req_map, "layer", map);
  double sensor_radius = req.sensor_radius;//1b. Read in sensor radius
  double local_map_width = 2.0*sensor_radius;
  // geometry_msgs::Pose robot_pose = req.robot_pose;//1c. Read in robot current pose
  std::vector<geometry_msgs::Point> system_hull_pts = req.system_hull_pts; //1d. Read in hull points (in body fixed frame)
  int theta_step_num = req.theta_step_num; //1e. Read in theta steps (number of steps)
  // float avg_system_speed = req.avg_system_speed;
  //SYSTEM TWIST should come in on filter
  //This should ultimately come from listening to a message
  geometry_msgs::Twist robot_twist = req.robot_twist;


  //Plot the hull points shifted to current robot pose
  geometry_msgs::PolygonStamped cvx_hull_polygon;
  cvx_hull_polygon.header = req_map.header;
  bool pub_polygon_bool = convex_hull_display(cvx_hull_polygon, system_hull_pts,  robot_pose);





  // geometry_msgs::Vector3 system_orientation_vector = req.sys_orient_vect; 
  float map_resolution = req_map.info.resolution;
  //Define the local grid
  local_layer_map_.setFrameId(req_map.header.frame_id);
  // map.setFrameId(map.getFrameId());
  std::cout << "map resolution: " << map_resolution << std::endl;
  local_layer_map_.setGeometry(grid_map::Length(local_map_width, local_map_width), map_resolution, grid_map::Position(robot_pose.position.x,robot_pose.position.y));
  //local_layer_map_.move(Position(0,0));

  grid_map::GridMap emptymap({"layer"});
  // emptymap.setFrameId("localmap");
  emptymap.setFrameId(req_map.header.frame_id);
  emptymap.setGeometry(grid_map::Length(local_map_width, local_map_width), map_resolution, grid_map::Position(robot_pose.position.x,robot_pose.position.y));
  //emptymap.move(Position(0,0));

  //Set all cells as unoccupied
  for (grid_map::GridMapIterator it(emptymap); !it.isPastEnd(); ++it) {
    emptymap.at("layer", *it) = 1.0;
  }


  //Make a theta vector based on stepsize
  double theta_step = (2*M_PI)/((float)theta_step_num);
  double theta_new = -M_PI;
  std::vector<double> theta_list;
  // theta_list.push_back(theta_new);

  double iter_mult = 1.0;
  for(int theta_iter=0; theta_iter <= theta_step_num; theta_iter++){
    theta_list.push_back(theta_new);
    theta_new += theta_step;
    iter_mult += 1.0;

    std::stringstream sstm;
    sstm << "layer_" << theta_iter;
    std::string layer_name = sstm.str();

    local_layer_map_.add(layer_name, emptymap["layer"]);
  }
  const std::vector< std::string > var_test = local_layer_map_.getLayers();
  //Make the theta list available for all functions in class (especially for state checking snap to grid)
  theta_list_global_ = theta_list;

  /*2. Make list of all points within radius of interest */
  grid_map::Position center(robot_pose.position.x, robot_pose.position.y); //Center of radius of interest is around the current pose of the robot
  // ROS_INFO("Origin %g %g, Res: %g, size: %g, %g m (%d, %d cells)", map.getPosition().x(), map.getPosition().y(), map.getResolution(), map.getLength().x(), map.getLength().y(), map.getSize()(0), map.getSize()(1));
  for (grid_map::SpiralIterator iterator(map, center, sensor_radius); !iterator.isPastEnd(); ++iterator){
    //For each point within that radius, store the x,y,th value in the point cloud, and within the data
    for( int theta_iterator=0; theta_iterator < theta_list.size() ; theta_iterator++){
      //Obtain the point value and position
      float curr_point_value = map.at("layer",*iterator); //Store the value at the current point (-1 is unobserved, 0 is free, 1 is occupied)
      if(curr_point_value > 1.0){
        curr_point_value = curr_point_value/100.0; //Convert a 100 scale to 0-1 //This deserves further attention, b/c then points near 0 in this case are treated as occupied
      }
      grid_map::Position ps; map.getPosition(*iterator, ps); //access the position of the current iterator
      geometry_msgs::Point32 curr_pt;
      curr_pt.x = ps.x(); curr_pt.y = ps.y(); curr_pt.z = theta_list[theta_iterator];
      //For the given angle, rotate the hull points which are in body fixed frame, then add them to the curr point position to obtain them in world frame
      std::vector<geometry_msgs::Point> rotated_hull_pts = Se2_Occ::rotate_hull_pts(system_hull_pts, theta_list[theta_iterator], curr_pt); //Use point of interest as center, and verticies to define a new iterator, if all points in this region are free, then store this point in free list, else store in occupied list

      /* 3. For all free points in region of interst: iterate through theta (where number of theta steps are specified) [-pi:th_step:pi] */
      if(curr_point_value == 0){
        //If point is free, then perform (hull check), if all points in hull check are free, then add this point to free list, else add to occupied list
        grid_map::Polygon polygon;
        polygon.setFrameId(map.getFrameId());
        for(int poly_idx=0; poly_idx<rotated_hull_pts.size(); poly_idx++){
          double xval = rotated_hull_pts[poly_idx].x;
          double yval = rotated_hull_pts[poly_idx].y;
          polygon.addVertex(grid_map::Position( xval, yval)); //add each rotated vertex
        }
        //Now iterate through points in this sub-polygon, if any are occupied, then this cell is bad (minkowski), otherwise this cell is free at this orientation
        bool cell_occupied = false;
        for (grid_map::PolygonIterator sub_iterator(map, polygon); !sub_iterator.isPastEnd(); ++sub_iterator) {
          if (map.at("layer", *sub_iterator) != 0){
            //sub point is occupied
            cell_occupied = true;
            continue;
          }
          // ROS_INFO("value is %ld",map.at("type", *sub_iterator));
          // ros::Duration duration(0.02);
          // duration.sleep();
        }
        //Check occupancy and add to appropriate point cloud
        if(cell_occupied == false){
          //add to free list
          res.free_pts.points.push_back(curr_pt);
          // ROS_INFO("A point was added to the free list");

          //Add this free cell to the local layered map here:
          std::stringstream sstm;
          sstm << "layer_" << theta_iterator;
          std::string layer_name = sstm.str();

          grid_map::Index local_layer_index;
          // bool interior_pt = local_layer_map_.getIndex(ps, local_layer_index); // Pass the current point in the world map frame to get the current index in local frame
          bool interior_pt = local_layer_map_.getIndex(ps, local_layer_index); // Pass the current point in the world map frame to get the current index in local frame
          // std::cout << "interior_pt bool:" << interior_pt << std::endl;

          if (interior_pt){
            local_layer_map_.at(layer_name, local_layer_index) = 0.0;
            // std::cout << "checked interior point" << std::endl;
          }else{
            // std::cout << "did not check interior point" << std::endl;
          }
          
          
        }else{
          //add to occupied list
          res.occ_pts.points.push_back(curr_pt);
          // ROS_INFO("A point was added to the occ list");
        }
      }else{
        //If point is occupied, add to occupied point cloud immediately
        res.occ_pts.points.push_back(curr_pt);
        // ROS_INFO("A point was added to the  HARD occ list");
      }
    }
  }
  //Calculate Costs on Point clouds
  
  geometry_msgs::Vector3 step_vector; step_vector.x = 0.0; step_vector.y = 1.0; step_vector.z = 0.0;
  geometry_msgs::Vector3 force_vector; force_vector.x = 0.0; force_vector.y = 1.0; force_vector.z = 0.0;
  // geometry_msgs::Point velocity_vector; velocity_vector.x = 1.0; velocity_vector.y = 0.0; velocity_vector.z = 0.0;
  geometry_msgs::Twist sys_twist = robot_twist;

  //THIS MUST COME FROM BAYSIAN FILTER ON SYSTEM POSE
  // geometry_msgs::Vector3 system_orientation_vector; 
  // system_orientation_vector.x = robot_pose.orientation.w+ robot_pose.orientation.x - robot_pose.orientation.y - robot_pose.orientation.z; //projecting unit x axis vector on current rotation
  // system_orientation_vector.y = 2*(robot_pose.orientation.x*robot_pose.orientation.y + robot_pose.orientation.x*robot_pose.orientation.z); 
  // system_orientation_vector.z = 0.0;

  ROS_INFO("Reached the point for calculating costs");

  //Calculate Objective Function costs at every free point
  ROS_INFO("human intention costs"); 
  human_intention_cost(res.free_pts, robot_pose, step_vector,  force_vector, sys_twist); //This should be system twist etc //confirmed working
  ROS_INFO("obstacle costs");
  obstacle_dist_cost(res.free_pts, res.occ_pts); //confirmed working, finds points farthest from all obstacles (tested alone)
  ROS_INFO("radial attraction costs");
  radial_attraction_cost(res.free_pts, robot_pose, sys_twist, sensor_radius); //Should visualize this circle in real time

  // path_cost(&res.free_pts, robot_pose);
  
  summed_cost_funct(res.free_pts);
  //find the optimal point to plan to in SE(2)
  ROS_INFO("Costs have been found and summed");
  
  geometry_msgs::Point optimal_goal_pose = find_optimal_goal_pose(res.free_pts);
  geometry_msgs::PointStamped optimal_goal_pose_stamped;
  optimal_goal_pose_stamped.header = req_map.header;
  optimal_goal_pose_stamped.point = optimal_goal_pose;

  //*****************************************************
  //PREPUB for debugging
  opt_path_point_.publish(optimal_goal_pose_stamped);
  //Publish the pointclouds and valid path
  pc_occ_pub_.publish(res.occ_pts);
  pc_free_pub_.publish(res.free_pts);
  //publish the local gridmap:
  grid_map_msgs::GridMap local_map_message;
  grid_map::GridMapRosConverter::toMessage(local_layer_map_, local_map_message);
  grid_map_local_pub_.publish(local_map_message);
  //*****************************************************
  
  //populate private point cloud variable for ease of use with OMPL state checker
  free_pc_global = res.free_pts; 
  occ_pc_global = res.occ_pts;
  robot_pose_curr_for_ompl = robot_pose; //this is so that it is accessible from the state checker
  sensor_radius_global = sensor_radius;
  

  ROS_INFO("Running OMPL path planner");
  //Use planner to plan to point collision free
  nav_msgs::Path valid_path = path_plan(res.free_pts, res.occ_pts, robot_pose, optimal_goal_pose, sensor_radius);

  ROS_INFO("Path obtained");

  //Return the current optimal path
  res.opt_path = valid_path;

  // //Publish the pointclouds and valid path
  // pc_occ_pub_.publish(res.occ_pts);
  // pc_free_pub_.publish(res.free_pts);
  // opt_path_point_.publish(optimal_goal_pose_stamped);
  opt_path_pub_.publish(res.opt_path);

  // //publish the local gridmap:
  // grid_map_msgs::GridMap local_map_message;
  // grid_map::GridMapRosConverter::toMessage(local_layer_map_, local_map_message);
  // grid_map_local_pub_.publish(local_map_message);
  

  return true;
}

//Operations on point clouds (use pointers passing to conserve memory), each cost makes a channel vector of scalar values
//1. Create function to calculate dot product from two vectors
float Se2_Occ::dot_product_operator(geometry_msgs::Vector3 v1, geometry_msgs::Vector3 v2){
  //return v1 dot v2
  float v1_norm = vect_norm(v1);
  float v2_norm = vect_norm(v2);
  float min_norm_thresh = 1e-4;
  float dot_prod_normed;
  if((v1_norm < min_norm_thresh) || (v2_norm < min_norm_thresh)){
    dot_prod_normed = 0.0;
  }else{
    float dot_prod = v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
    dot_prod_normed = dot_prod/(v1_norm*v2_norm);
  }
  return dot_prod_normed;
}
geometry_msgs::Point Se2_Occ::cross_product_operator(geometry_msgs::Point v1, geometry_msgs::Point v2){
  // v1 x v2
  geometry_msgs::Point vfinal;
  vfinal.x = -v1.z*v2.y + v1.y*v2.z;
  vfinal.y = v1.z*v2.x - v1.x*v2.z;
  vfinal.z = -v1.y*v2.x + v1.x*v2.y;
  return vfinal;
}
float Se2_Occ::vect_norm(geometry_msgs::Vector3 vect){
  float summed_square = std::pow(vect.x,2)+ std::pow(vect.y,2)+ std::pow(vect.z,2);
  return std::pow(summed_square, 0.5);
}
float Se2_Occ::squared_norm_diff(geometry_msgs::Vector3 v1, geometry_msgs::Vector3 v2){
  //this is ||v1 - v2||**2
  geometry_msgs::Vector3 diff_vect;
  diff_vect.x = v1.x - v2.x; diff_vect.y = v1.y - v2.y; diff_vect.z = v1.z - v2.z;
  float diff_vect_norm = vect_norm(diff_vect);
  return std::pow(diff_vect_norm,2);
}
geometry_msgs::Point Se2_Occ::convert_geometry_msgs_pt32_to_pt(geometry_msgs::Point32 initial_point){
  geometry_msgs::Point return_point;
  return_point.x = (float) initial_point.x;
  return_point.y = (float) initial_point.y;
  return_point.z = (float) initial_point.z;
  return return_point;
}
geometry_msgs::Point32 Se2_Occ::convert_geometry_msgs_pt_to_pt32(geometry_msgs::Point initial_point){
  geometry_msgs::Point32 return_point;
  return_point.x = initial_point.x;
  return_point.y = initial_point.y;
  return_point.z = initial_point.z;
  return return_point;
}

//2. Create function to calculate radial sigmoid function
float Se2_Occ::radial_sigmoid_fnct(float vel_curr, float sensor_radius){
  float Rmax = sensor_radius; // radius max:  (3meter)  multiple *10 to see drastic effect by itself
  float rad_curr = Rmax/(1 + 50*std::exp(-2*vel_curr)); //starts at 0.059m at v=0m/s, then for v>= 4m/s, r = 3m for a 4meter sensor radius
  return rad_curr;
}


float Se2_Occ::radial_sigmoid_fnct_theta(float ang_vel){
  //This sigmoid function is bounded by pi, with minimal delta angle approximately 0
  float theta_max = M_PI;
  float theta_rad_curr = theta_max/(1 + 50*std::exp(-2*ang_vel)); //min value is 0.062 rad (3deg), and 3.14rad/s is 2.9rad
  return theta_rad_curr;
}

geometry_msgs::Vector3 Se2_Occ::rotation_difference(Eigen::MatrixXd Rtip, Eigen::MatrixXd Rtail){
  Eigen::MatrixXd Rdiff = Rtip*Rtail.transpose();
  Eigen::MatrixXd skew_mat = Rdiff.log(); //Matrix log to return the skew sym matrix container vector whose norm is the angle
  geometry_msgs::Vector3 rotate_vector_raw;
  rotate_vector_raw.x = skew_mat(2,1);
  rotate_vector_raw.y = skew_mat(0,2);
  rotate_vector_raw.z = skew_mat(1,0);
  return rotate_vector_raw;
}

float Se2_Occ::zaxis_angle_diff(float theta_tip, float theta_tail){
  //This function takes in two float angles about the z-axis and calculates the scalar angular difference between them 
  //First construct rotation matrices from the angles and find rotational difference
  /*
  Eigen::MatrixXd Rtip(3,3); 
  Rtip(0,0) = std::cos(theta_tip);  Rtip(0,1) = std::sin(theta_tip); Rtip(0,2) = 0.0;
  Rtip(1,0) = -std::sin(theta_tip); Rtip(1,1) = std::cos(theta_tip); Rtip(1,2) = 0.0;
  Rtip(2,0) = 0.0;                  Rtip(2,1) = 0.0;                 Rtip(2,2) = 1.0;

  Eigen::MatrixXd Rtail(3,3);
  Rtail(0,0) = std::cos(theta_tail);  Rtail(0,1) = std::sin(theta_tail); Rtail(0,2) = 0.0;
  Rtail(1,0) = -std::sin(theta_tail); Rtail(1,1) = std::cos(theta_tail); Rtail(1,2) = 0.0;
  Rtail(2,0) = 0.0;                   Rtail(2,1) = 0.0;                  Rtail(2,2) = 1.0;

  geometry_msgs::Vector3 angle_diff_vect = rotation_difference(Rtip, Rtail); //Find rotational difference
  float angle_diff = vect_norm(angle_diff_vect);
  */
  //The above method why correct and robust is extremely slow for point by point comparison

  //for R2 = Rdelta * R1, then tip is R2, tail is R1

  float c1 = std::cos(theta_tail);
  float s1 = std::sin(theta_tail);
  float c2 = std::cos(theta_tip);
  float s2 = std::sin(theta_tip);
  float c_delta = c1*c2 + s1*s2;
  float s_delta = c1*s2 - c2*s1;
  float angle_diff = std::atan2(s_delta, c_delta);

  return angle_diff;
}

float Se2_Occ::float_sign(float value){
  float return_value;
  if(value >= 0){
    return_value = 1.0;
  }else{
    return_value = -1.0;
  }
  return return_value;
}

//3. Create function to calculate intention_cost (person intention)
bool Se2_Occ::human_intention_cost(sensor_msgs::PointCloud &free_cells_pc, geometry_msgs::Pose sys_pose, geometry_msgs::Vector3 step_vector, geometry_msgs::Vector3 force_vector, geometry_msgs::Twist sys_twist){
  /* Terms: 
    free cells: point cloud for free points
    robot pose: pose of the system
    Intention Vectors: step_vector, force_vector, velocity_vector, system_orientation_vector  (all of these are baysian filters on these vectors published on a message)
    weights for each term: hi_weights["force_vector"] = 1.0; hi_weights["step_vector"] = 1.0; hi_weights["velocity_vector"] = 1.0; hi_weights["angular_vector"] = 0.5 //weights for intention vectors
  */
  // either * or & works, but * references the first address and should be faster for large repeated references

  sensor_msgs::ChannelFloat32 human_intent_cost;
  human_intent_cost.name = "human_intent_cost";

  geometry_msgs::Vector3 linear_velocity = sys_twist.linear;

  for(int hic_index= 0; hic_index < free_cells_pc.points.size(); hic_index++){
    geometry_msgs::Point32 curr_pt = free_cells_pc.points[hic_index];
    //For the current voxel, calculate the projection from each vector
    //Handle the linear portion (x,y)
    geometry_msgs::Vector3 vect_sys_pose_to_voxel;//calculate rob pose vect voxel
    vect_sys_pose_to_voxel.x = curr_pt.x - sys_pose.position.x;
    vect_sys_pose_to_voxel.y = curr_pt.y - sys_pose.position.y;
    //Take dot product with each vector (all in world frame), then sum all values
    float pt_vect_dot_force = dot_product_operator(vect_sys_pose_to_voxel, force_vector); // ROS_INFO("force dot product: %f",pt_vect_dot_force);
    float pt_vect_dot_step = dot_product_operator(vect_sys_pose_to_voxel, step_vector); // ROS_INFO("step dot product: %f",pt_vect_dot_step);
    float pt_vect_dot_vel = dot_product_operator(vect_sys_pose_to_voxel, linear_velocity); // ROS_INFO("vel dot product: %f",pt_vect_dot_vel);

    //This is handled correctly:
    geometry_msgs::Vector3 system_orientation_vector; 
    system_orientation_vector.x = sys_pose.orientation.w+ sys_pose.orientation.x - sys_pose.orientation.y - sys_pose.orientation.z; 
    system_orientation_vector.y = 2*(sys_pose.orientation.x*sys_pose.orientation.y - sys_pose.orientation.x*sys_pose.orientation.z); 
    //linear portion of orientation (affects selection of x,y that are in "front")
    float pt_vect_dot_orientation = dot_product_operator(vect_sys_pose_to_voxel, system_orientation_vector); // ROS_INFO("orient dot product: %f",pt_vect_dot_orientation);
    //To attract to the correct angle theta, we use the diff_angle calculated below (as the sys_pose is filtered)

    //Now handle the orientation terms (theta), only angular velocity contributes to this term use: hi_weights["angular_vector"]
    float vox_theta = curr_pt.z; //z is theta
    float sys_theta = 2*std::atan2(sys_pose.orientation.z,sys_pose.orientation.w); //Returns theta in [-pi,pi]
    // std::cout << "system theta before: " << sys_theta << std::endl;
    if(sys_theta > M_PI){
      sys_theta -= 2*M_PI;
    } 
    if(sys_theta < -M_PI){
      sys_theta += 2*M_PI;
    }

    // std::cout << "system theta: " << sys_theta << std::endl;
    float diff_angle = zaxis_angle_diff(vox_theta, sys_theta) ; //This can be any sign, and operation is (vox_theta - sys_theta)
    float system_z_twist = sys_twist.angular.z;
    float angular_intention_value = -float_sign(system_z_twist)*float_sign(diff_angle) + std::abs(diff_angle); //hence if they are aligned then this value is negative (needed for finding min cost), and positive if they are not aligned
    // float angular_intention_value = 10.0*std::abs(diff_angle); //hence if they are aligned then this value is negative (needed for finding min cost), and positive if they are not aligned
    //The last term penalizes angles far from the current esimate of the system pose

    //Now obtain the summed cost of all intention terms: 
    float summed_cost = -(hi_weights["force_vector"]*pt_vect_dot_force + hi_weights["step_vector"]*pt_vect_dot_step + hi_weights["velocity_vector"]*pt_vect_dot_vel + hi_weights["orient_vector"]*pt_vect_dot_orientation);
    // ROS_INFO("summed dot product: %f",summed_cost);


    float final_cost = summed_cost + hi_weights["angular_vector"]*angular_intention_value ; //first is projection of force,step, velocity, and current heading
    human_intent_cost.values.push_back(final_cost); //Add this value to teh channel
  }
  //Now add this channel to the point cloud
  free_cells_pc.channels.push_back(human_intent_cost);
  return true;
}

//4. Create function to calculate obstacle_cost (distance from obstacles)
bool Se2_Occ::obstacle_dist_cost(sensor_msgs::PointCloud &free_cells_pc, sensor_msgs::PointCloud &occ_cells_pc){
  sensor_msgs::ChannelFloat32 obs_cost_chan;
  obs_cost_chan.name = "obs_cost";
  for(int free_idx = 0; free_idx < free_cells_pc.points.size(); free_idx++){
    // geometry_msgs::Point free_pt = (geometry_msgs::Point)free_cells_pc.points[free_idx];
    geometry_msgs::Point32 free_pt = free_cells_pc.points[free_idx];
    geometry_msgs::Vector3 free_pt_xy; free_pt_xy.x = free_pt.x; free_pt_xy.y = free_pt.y;
    float free_pt_theta = free_pt.z;
    double curr_pt_obs_score = 0.0;
    for(int occ_idx=0; occ_idx < occ_cells_pc.points.size(); occ_idx++){
      //For every free point, caluclate the summed occupied metric 
      geometry_msgs::Point32 occ_pt = occ_cells_pc.points[occ_idx];
      geometry_msgs::Vector3 occ_pt_xy; occ_pt_xy.x = occ_pt.x;  occ_pt_xy.y = occ_pt.y;
      float occ_pt_theta = occ_pt.z;
      //Linear term (x,y): 
      float pt_diff_norm_squared = squared_norm_diff(free_pt_xy, occ_pt_xy);
      //Angular term (theta):

      //SPEED THIS UP:
      float theta_diff = zaxis_angle_diff(free_pt_theta, occ_pt_theta);
      float theta_diff_abs = std::abs(theta_diff); //could square, but this term has no direct relation to meters, hence use weights to relate it

      float occ_free_guassian_cost = obstacle_cost_gain* std::exp(-(pt_diff_norm_squared + hi_weights["angular_vector"]*theta_diff_abs)/(std::pow(obstacle_cost_sigma,2.0)));
      curr_pt_obs_score += occ_free_guassian_cost; //sum the cost form all the obstacles
    }
    obs_cost_chan.values.push_back(curr_pt_obs_score);
  }
  free_cells_pc.channels.push_back(obs_cost_chan);
  return true;
}


//5. Create function to calculate radial_attraction_cost
bool Se2_Occ::radial_attraction_cost(sensor_msgs::PointCloud &free_cells_pc, geometry_msgs::Pose system_pose, geometry_msgs::Twist sys_twist , double sensor_radius){
  /* FIX THIS EVERYWHERE, WE NEED SYSTEM POSE, NOT ROBOT POSE */
  sensor_msgs::ChannelFloat32 radial_cost_chan;
  radial_cost_chan.name = "radial_cost";
  for(int index_pts = 0; index_pts < free_cells_pc.points.size(); index_pts++){
    //For each point, find norm difference from that point to the system center (currently system_pose)
    geometry_msgs::Point32 curr_pt = free_cells_pc.points[index_pts]; 
    geometry_msgs::Vector3 curr_pt_xy;  curr_pt_xy.x = curr_pt.x; curr_pt_xy.y = curr_pt.y;
    float curr_pt_theta = curr_pt.z;
    // geometry_msgs::Point curr_pt = convert_geometry_msgs_pt32_to_pt(curr_pt_32);
    geometry_msgs::Point32 system_pose_pt32 = convert_geometry_msgs_pt_to_pt32(system_pose.position);
    geometry_msgs::Vector3 system_pose_pt32_xy;  system_pose_pt32_xy.x =system_pose_pt32.x;   system_pose_pt32_xy.y =system_pose_pt32.y;
    float system_pose_pt32_theta =system_pose_pt32.z;

    //obtain the system speed as a scalar
    float system_velocity_speed = vect_norm(sys_twist.linear);

    //Linear terms:
    float pt_diff_norm_squared = squared_norm_diff(curr_pt_xy,system_pose_pt32_xy);
    float pt_diff_norm = std::pow(pt_diff_norm_squared, 0.5);

    float attraction_radius = radial_sigmoid_fnct(system_velocity_speed, sensor_radius);//Find the current radius of the attraction circle
    float rad_diff_normed = std::abs((pt_diff_norm - attraction_radius)); 
    // ROS_INFO("Radial diff abs: %f",rad_diff_normed);

    //Angular term
    float theta_diff = zaxis_angle_diff(curr_pt_theta, system_pose_pt32_theta);
    float attraction_radius_theta = radial_sigmoid_fnct_theta(sys_twist.angular.z);
    float theta_sigmoid_cost_diff = std::abs(std::abs(theta_diff) - attraction_radius_theta);

    float sigma_squared = std::pow(radial_cost_sigma, 2.0);
    float radial_cost = radial_cost_gain * std::exp((rad_diff_normed + hi_weights["angular_vector"]*theta_sigmoid_cost_diff)/(sigma_squared));
    // ROS_INFO("Radial Cost: %f",radial_cost);
    //Add this cost to the channel
    radial_cost_chan.values.push_back(radial_cost);
  }

  free_cells_pc.channels.push_back(radial_cost_chan);
  //Note that there is a missing square in the written notation!
  return true;
}

//6. Create function to calculate path_cost (either position/velocity from current pose to that position, but using trail of viable points (e.g. dijkstra))
// bool Se2_Occ::path_cost(sensor_msgs::PointCloud &free_cells_pc, geometry_msgs::Pose robot_pose){
//   //TODO: Possibly remove this
//   return true;
// }

//7. Create function to sum all other costs, to be used in final planner: summed_cost
bool Se2_Occ::summed_cost_funct(sensor_msgs::PointCloud &free_cells_pc){
  sensor_msgs::ChannelFloat32 summed_net_cost;
  summed_net_cost.name = "summed_net_cost";
  for (int chan_idx = 0; chan_idx < free_cells_pc.channels.size(); chan_idx++){
    for (int chan_val_idx = 0; chan_val_idx < free_cells_pc.channels[chan_idx].values.size(); chan_val_idx++){
      if (chan_idx == 0 && (summed_net_cost.values.size() < free_cells_pc.channels[0].values.size()) ){
        summed_net_cost.values.push_back(free_cells_pc.channels[chan_idx].values[chan_val_idx]); //if first round
      }else{
        summed_net_cost.values[chan_val_idx] = free_cells_pc.channels[chan_idx].values[chan_val_idx]; //else if its already populated, then add to previous value
      }
    }
  }
  free_cells_pc.channels.push_back(summed_net_cost);
  return true;
}

//8. Find the optimal goal pose given the sum
geometry_msgs::Point Se2_Occ::find_optimal_goal_pose(sensor_msgs::PointCloud &free_cells_pc){

  //First find the summed list indicie
  int summed_channel_idx = free_cells_pc.channels.size() -1;
  for (int idx=(free_cells_pc.channels.size() -1); idx >= 0; --idx){
    if(free_cells_pc.channels[idx].name == "summed_net_cost"){
      summed_channel_idx = idx;
      continue;
    }
  }

  //Verified that summed net cost was accessed correctly
  // ROS_INFO("summed channel idx: %d, and name: %s",summed_channel_idx, free_cells_pc.channels[summed_channel_idx].name );
  // std::cout << "channel name: " << free_cells_pc.channels[summed_channel_idx].name << std::endl;

  //In this channel, find the minimum value and corresponding index, which points to the most optimal point, for equivalently optimal points, the last checked is used
  float min_score = 1e100;
  int min_index = 0;

  for (int value_index = 0; value_index < free_cells_pc.channels[summed_channel_idx].values.size(); value_index++){
    //query current value
    float curr_score = (float) free_cells_pc.channels[summed_channel_idx].values[value_index];
    if(curr_score < min_score){
      min_score = curr_score;
      min_index = value_index;
    }
  }
  // ROS_INFO("summed channel idx: %d, and name: %s",summed_channel_idx, free_cells_pc.channels[summed_channel_idx].name );
  //Obtain the optimal goal point

  geometry_msgs::Point optimal_goal_point;
  optimal_goal_point.x = (float) free_cells_pc.points[min_index].x;
  optimal_goal_point.y = (float) free_cells_pc.points[min_index].y;
  optimal_goal_point.z = (float) free_cells_pc.points[min_index].z;
  // std::cout << "the value returned is: " << free_cells_pc.points[min_index].x << ", " << free_cells_pc.points[min_index].y<< ", " << free_cells_pc.points[min_index].z <<  std::endl;
  // optimal_goal_point.z = M_PI;
  if (optimal_goal_point.z == M_PI){
    optimal_goal_point.z -= 0.01;
  }
  if (optimal_goal_point.z == -M_PI){
    optimal_goal_point.z += 0.01;
  }

  //Was having issues with M_PI as goal state, planner wasn't planning

  return optimal_goal_point;
}
bool Se2_Occ::isStateValid(const ob::State *state){
  //TODO: FILL THIS OUT
  //With world bounds, treat the robot pose as the center of the world, and if points are outside sensor radius, or outside theta (For z-axis), then reject, else accept, 
  //Global Variables: free_pc_global occ_pc_global robot_pose_curr_for_ompl, sensor_radius_global

  const ompl::base::SE2StateSpace::StateType *se2_state = state->as<ompl::base::SE2StateSpace::StateType>();
  double x1 = (*se2_state).getX();
  double y1 = (*se2_state).getY();
  double yaw1 = (*se2_state).getYaw();


  //Check proximity to occupied states cells
  //1. Find the closest theta
  int curr_theta_idx = 0;
  int theta_min_diff = 1e2;
  for(int theta_idx=0; theta_idx < theta_list_global_.size(); theta_idx++){
    if(std::abs(yaw1-theta_list_global_[theta_idx]) < theta_min_diff){      
      theta_min_diff = std::abs(yaw1-theta_list_global_[theta_idx]);
      curr_theta_idx = theta_idx;
    }
  }
  // float theta_snap_curr = theta_list_global_[curr_theta_idx]; //The closet theta to the estimated value, to use to obtain the correct layer for comparison
  //2. generate unique string from that theta
  std::stringstream sstm;
  sstm << "layer_" << curr_theta_idx;
  std::string layer_name = sstm.str();


  //3. given the x,y, get the iterator
  // if(local_layer_map_({layer_name}).isInside(grid_map::Position(x1,y1))){
  //   std::cout << "point is interior to local grid" << std::endl;
  // }
  if (local_layer_map_.atPosition(layer_name, grid_map::Position(x1,y1)) > 0.0){
    // std::cout << "Point was occupied" << std::endl;
    return false;
  }else{
    // std::cout << "Point was free" << std::endl;
    return true;
  }
}

//9. Calculate the optimal path (sample based planner, rrt-connect)
nav_msgs::Path Se2_Occ::path_plan(sensor_msgs::PointCloud &free_cells_pc, sensor_msgs::PointCloud &occ_cells_pc, geometry_msgs::Pose robot_pose, geometry_msgs::Point goal_pose, double sensor_radius){

  //INSTEAD OF SHIFTING THIS TO EVERY POINT, EVERY POINT FROM THE POINT CLOUD CAN BE SUBTRACTED FROM THE ROBOT POSE, 


  // construct the state space we are planning in
  //ob::StateSpacePtr space(new  ompl::base::SE2StateSpace());
  MyStateSpace *cs = new MyStateSpace();

  // set the bounds for the R^2 part of SE(2)  

  float xmin = robot_pose.position.x - sensor_radius;
  float xmax = robot_pose.position.x + sensor_radius;
  float ymin = robot_pose.position.y - sensor_radius;
  float ymax = robot_pose.position.y + sensor_radius;
  // float theta_min = -M_PI;
  // float theta_max = M_PI;
  
  ob::RealVectorBounds bounds1(2);
  bounds1.setLow(0,xmin); //first is dimension, second is value
  bounds1.setHigh(0,xmax);
  bounds1.setLow(1,ymin);
  bounds1.setHigh(1,ymax);

  // ob::RealVectorBounds bounds2(1);
  // bounds2.setLow(0,theta_min);
  // bounds2.setHigh(0,theta_max);

  //std::cout << cs->getSubspaceCount() << std::endl;  
  ompl::base::StateSpacePtr s1 = cs->getSubspace(0);  
  s1->as<ob::RealVectorStateSpace>()->setBounds(bounds1);

  //ompl::base::StateSpacePtr s2 = cs->getSubspace(1);  
  //s2->as<ob::SO2StateSpace>()->setBounds(bounds2);


  // put the pointer to the state space in a shared pointer
  ompl::base::StateSpacePtr space(cs);


  // construct an instance of  space information from this state space
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
    
  // set state validity checking for this space
  si->setStateValidityChecker(boost::bind(&Se2_Occ::isStateValid, this, _1));


  //Set Start position
  ob::ScopedState<> start(space);
  // start.random();
  start[0] = robot_pose.position.x;
  start[1] = robot_pose.position.y;
  start[2] = robot_pose.position.z;


  //Set Goal position
  ob::ScopedState<> goal(space);
  // goal.random();
  goal[0] = goal_pose.x;
  goal[1] = goal_pose.y;
  goal[2] = goal_pose.z;

  // create a problem instance
  ob::ProblemDefinitionPtr pdef(new ob::ProblemDefinition(si));

  // set the start and goal states
  pdef->setStartAndGoalStates(start, goal);

  // create a planner for the defined space
  ob::PlannerPtr planner(new og::RRTConnect(si));

  // set the problem we are trying to solve for the planner
  planner->setProblemDefinition(pdef);

  // perform setup steps for the planner
  planner->setup();

  // planner->as<og::RRTConnect>()->setRange(0.01);
  planner->as<og::RRTConnect>()->setRange(M_PI/20.0);

  // print the settings for this space
  si->printSettings(std::cout);

  // print the problem settings
  pdef->print(std::cout);

  // attempt to solve the problem within one second of planning time
  ob::PlannerStatus solved = planner->solve(10.0);

  //Set the following path to the solution found using ompl
  nav_msgs::Path valid_path;
  valid_path.header = free_cells_pc.header;

  if (solved)
  {
    // get the goal representation from the problem definition (not the same as the goal state)
    // and inquire about the found path
    ob::PathPtr path = pdef->getSolutionPath();
    std::cout << "Found solution:" << std::endl;
    
    // print the path to screen
    //path->print(std::cout);
        
    
    //std::shared_ptr<og::PathGeometric> path_ptr = std::static_pointer_cast<og::PathGeometric>(pdef->getSolutionPath());

    boost::shared_ptr<og::PathGeometric> path_ptr = boost::static_pointer_cast<og::PathGeometric>(pdef->getSolutionPath());
    //Print the path
    // path_ptr->printAsMatrix(std::cout);    

    std::vector< ob::State * > path_states = path_ptr->getStates();

    for(int i = 0; i < path_states.size(); i++)
    {
      ob::State* current = path_states[i];
        const ompl::base::SE2StateSpace::StateType *se2_state = current->as<ompl::base::SE2StateSpace::StateType>();
        double x1 = (*se2_state).getX();
        double y1 = (*se2_state).getY();
        double yaw1 = (*se2_state).getYaw();
        std::cout <<  (*se2_state).getX() << " " << (*se2_state).getY() << " " << (*se2_state).getYaw() << std::endl;

        //Add current point to nav msgs path
        geometry_msgs::PoseStamped curr_pose;
        curr_pose.header = free_cells_pc.header;
        curr_pose.pose.position.x = (*se2_state).getX();
        curr_pose.pose.position.y = (*se2_state).getY();
        curr_pose.pose.position.z = (*se2_state).getYaw();

        curr_pose.pose.orientation.x = 0; curr_pose.pose.orientation.y = 0; 
        curr_pose.pose.orientation.z = sin((*se2_state).getYaw()/2.0); curr_pose.pose.orientation.w = cos((*se2_state).getYaw()/2.0); 
        valid_path.poses.push_back(curr_pose);
    }
  }
  else
    std::cout << "No solution found" << std::endl;


  return valid_path;
}





int main(int argc, char **argv)
{
  ros::init(argc, argv, "se2_occ_server");
  ROS_INFO("Server node running");

  /* FIX THIS EVERYWHERE, WE NEED SYSTEM POSE, NOT ROBOT POSE */

  Se2_Occ se2_occ; //Call the class, service callback is in the constructor, and looped by the ros spin
  ros::spin();
  return 0;
}
