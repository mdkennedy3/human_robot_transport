import sympy as sp
import numpy as np
import scipy
from scipy import linalg 
from scipy import sparse
import time
import cvxopt
# from cvxopt import matrix, solvers
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


class Optimization_test(object):
    def __init__(self):
        self.p1 = [0,0,1,-1]; #top positive z, plane point of p1
        self.p2 = [0,0,-1,-1];#bottom, neg z, plane point of p2
        self.p3 = [1,0,0,-1]; #front (positve x),  plane point of p3
        self.p4 = [-1,0,0,-1];#back (negative x),  plane point of p4
        self.p5 = [0,1,0,-1]; #right (pos y), plane point of p5
        self.p6 = [0,-1,0,-1];#left (neg y), plane point of p6
        self.planes = np.array([self.p1,self.p2,self.p3,self.p4,self.p5,self.p6])
        #%Second Set of Constraints
        self.p1_second = [0,0,1,-3]; #%top positive z, plane point of p1
        self.p2_second = [0,0,-1,1]; #%bottom, neg z, plane point of p2
        self.p3_second = [1,0,0,-3]; #%front (positve x),  plane point of p3
        self.p4_second = [-1,0,0,1]; #%back (negative x),  plane point of p4
        self.p5_second = [0,1,0,-3]; #%right (pos y), plane point of p5
        self.p6_second = [0,-1,0,1]; #%left (neg y), plane point of p6
        self.second_planes = np.array([self.p1_second,self.p2_second,self.p3_second,self.p4_second,self.p5_second,self.p6_second]);

        #%Third Set of Constraints
        self.p1_third = [0,0,1,-3]; #%top positive z, plane point of p1
        self.p2_third = [0,0,-1,-1]; #%bottom, neg z, plane point of p2
        self.p3_third = [1,0,0,-3]; #%front (positve x),  plane point of p3
        self.p4_third = [-1,0,0,-1]; #%back (negative x),  plane point of p4
        self.p5_third = [0,1,0,-3]; #%right (pos y), plane point of p5
        self.p6_third = [0,-1,0,-1]; #%left (neg y), plane point of p6
        self.third_planes = np.array([self.p1_third,self.p2_third,self.p3_third,self.p4_third,self.p5_third,self.p6_third]);



        #%Critical Points
        self.x0 = np.matrix([-1,-1,-1]);
        self.x1 = np.matrix([1,1,1]);
        self.x2 = np.matrix([3,3,3]);
        #%Intermediate Points
        self.xt1 = np.matrix([0,0.5,0.5]);
        self.xt2 = np.matrix([2,1.5,1.5]);
        #%Critical Times
        self.t0 = 0;
        self.t_mid1 = 0.25;
        self.t1 = 0.5;
        self.t_mid2 = 0.75;
        self.t2 = 1;
        self.Ti_discrete = [self.t0,self.t1,self.t2];
        self.poly_order = 10

        # num_chords = 6  #this is determined in code
        # '''These should not be evenly spaced, they should be based on chord length to total path length '''
        # self.Ti_discrete = np.linspace(0,1,num_chords)

    def time_check(self,str,time_start):
        time_stop = time.time()
        diff_time = time_stop - time_start
        print str," code runs in: ", diff_time

    #Required in any polynomial solver
    def poly_order_fnct(self,t,order,poly_type):
        pos_time = []
        vel_time = []
        acc_time = []
        jerk_time = []
        for idx in range(order):
            exp = (order - idx)
            pos_time.append(t**exp)
            vel_time.append(exp*t**(exp-1))
            if exp >= 2:
                acc_time.append(exp*(exp-1)*t**(exp-2))
            else:
                acc_time.append(0.)

            if exp >=3:
                jerk_time.append(exp*(exp-1)*(exp-2)*t**(exp-3))
            else:
                jerk_time.append(0.)

        pos_time.append(1.)
        vel_time.append(0.)
        acc_time.append(0.)
        jerk_time.append(0.)
        pos_time = scipy.sparse.block_diag([pos_time,pos_time,pos_time]).toarray()
        vel_time = scipy.sparse.block_diag([vel_time,vel_time,vel_time]).toarray()
        acc_time = scipy.sparse.block_diag([acc_time,acc_time,acc_time]).toarray()
        jerk_time = scipy.sparse.block_diag([jerk_time,jerk_time,jerk_time]).toarray()

        # print "pos var: ", pos_time,"vel_time", vel_time
        if poly_type in "pos":
            return pos_time
        elif poly_type in "vel":
            return vel_time
        elif poly_type in "acc":
            return acc_time
        elif poly_type in "jerk":
            return jerk_time
        elif poly_type in "all":
            #ie access with v[:,0] for pos 
            return [pos_time, vel_time, acc_time, jerk_time]
    def Get_Time_matricies(self):
        self.T_hat_3diff = []
        self.T_hat_2diff = []
        self.T_hat_diff = []
        self.T_hat = []
        self.int_time_steps = 100 #time points to use along a chord (these points will respect the constraints of cvx)
        for idx in range(len(self.Ti_discrete)-1):
            Ti_int = np.linspace(self.Ti_discrete[idx], self.Ti_discrete[idx+1], self.int_time_steps) #get time in this region

            # T_diff_step = scipy.sparse.block_diag((np.array([self.poly_order_fnct(t,7,"vel") for t in Ti_int])))
            # T_diff_step = T_diff_step.toarray()
            # print "shape of T_diff_step: ", np.shape(T_diff_step)
            # T_step = scipy.sparse.block_diag((np.array([self.poly_order_fnct(t,7,"pos") for t in Ti_int])))
            # T_step = T_step.toarray()

            # T_diff_step = scipy.sparse.block_diag((np.array([self.poly_order_fnct(t,7,"vel") for t in Ti_int]))).toarray()
            # T_step = scipy.sparse.block_diag((np.array([self.poly_order_fnct(t,7,"pos") for t in Ti_int]))).toarray()

            T_diff_step = np.vstack(np.array([self.poly_order_fnct(t,self.poly_order,"vel") for t in Ti_int]))
            T_step = np.vstack(np.array([self.poly_order_fnct(t,self.poly_order,"pos") for t in Ti_int]))

            T_2diff_step = np.vstack(np.array([self.poly_order_fnct(t,self.poly_order,"acc") for t in Ti_int]))
            T_3diff_step = np.vstack(np.array([self.poly_order_fnct(t,self.poly_order,"jerk") for t in Ti_int]))

            #2nd diff
            if len(self.T_hat_2diff) == 0:
                self.T_hat_2diff = T_2diff_step
            else:
                self.T_hat_2diff = scipy.sparse.block_diag((self.T_hat_2diff,T_2diff_step))
                self.T_hat_2diff = self.T_hat_2diff.toarray()
            #3rd diff
            if len(self.T_hat_3diff) == 0:
                self.T_hat_3diff = T_3diff_step
            else:
                self.T_hat_3diff = scipy.sparse.block_diag((self.T_hat_3diff,T_3diff_step))
                self.T_hat_3diff = self.T_hat_3diff.toarray()

            #here before: 
            if len(self.T_hat_diff) == 0:
                self.T_hat_diff = T_diff_step
            else:
                self.T_hat_diff = scipy.sparse.block_diag((self.T_hat_diff,T_diff_step))
                self.T_hat_diff = self.T_hat_diff.toarray()

            if len(self.T_hat) == 0:
                self.T_hat = T_step
            else:
                self.T_hat = scipy.sparse.block_diag((self.T_hat,T_step))
                self.T_hat = self.T_hat.toarray()
        print "size of T_hat",np.shape(self.T_hat), "\n size of T_hat_diff", np.shape(self.T_hat_diff)
    def QP_Solve(self,H,f,A,b,Aeq,Beq):
        # """QP Solver
        # Args: 
        #     H
        #     f
        #     A
        #     b
        #     Aeq
        #     Beq
        # Returns: 
        # x from x'Hx + fx subj to Ax <= b and Aeq x = b 
        # """
        print "shape: H ", np.shape(H), "\n shape f: ", np.shape(f),"\n shape A: ", np.shape(A), "\n shape b: ", np.shape(b), "\n shape Aeq: ", np.shape(Aeq), "\n shape Beq:", np.shape(Beq)
        Q = cvxopt.matrix(H.toarray())
        p = cvxopt.matrix(f)
        G = cvxopt.matrix(A)
        bfinal =  np.matrix(b).T.astype(np.double)
        # print "b: ", bfinal 
        h = cvxopt.matrix(bfinal)
        # print "h: ", h
        A = cvxopt.matrix(Aeq)
        b = cvxopt.matrix(Beq)
        sol = cvxopt.solvers.qp(Q, p, G, h, A, b)
        # sol = cvxopt.solvers.qp(Q, p) #failes with message: ValueError: Rank(A) < p or Rank([P; A; G]) < n
        self.soln_coeff = np.matrix(sol['x'])
        print "solution: ", self.soln_coeff, "its type: ", type(self.soln_coeff)
    def Plot_poly(self):
        print "print curves"

        set_points = np.vstack([self.x0, self.x1, self.x2, self.xt1, self.xt2])

        traj_points_list = np.matrix(self.T_hat)*self.soln_coeff
        traj_points = np.reshape(traj_points_list,[len(traj_points_list)/3,3])

        # print "shape of set points: ", np.shape(set_points)
        print "shape of traj points: ", np.shape(traj_points), "type of pts: ", type(traj_points)
        # print "shape of traj list: ", np.shape(traj_points_list)

        fig = plt.figure(); ax = fig.add_subplot(111, projection='3d')
        c = 'r'; m = '*'

        xs = set_points[:,0]; ys = set_points[:,1]; zs = set_points[:,2]

        xs_set = traj_points[:,0]; ys_set = traj_points[:,1]; zs_set = traj_points[:,2]
        ax.scatter(xs.tolist(),ys.tolist(),zs.tolist(),c=c,marker=m)

        c = 'b';m = '^'; ax.scatter(xs_set.tolist(),ys_set.tolist(),zs_set.tolist(),c=c,marker=m)
        ax.set_xlabel('X axis'); ax.set_ylabel('Y axis'); ax.set_zlabel('Z axis');
        plt.show()
    #Specific to this example
    def Get_plane_constraints(self):
        self.P_tilde = []  #just make block diag of planes (check size to make sure its the size of P (coefficient))
        self.b_tilde = []
        print "shape of T_hat: ",np.round(np.shape(self.T_hat)[0]/3), "\n int time steps: ", self.int_time_steps
        for idx in range(np.round(np.shape(self.T_hat)[0]/3)):
            if idx <= self.int_time_steps:
                # b_vect = self.planes[:,3]
                b_vect = self.third_planes[:,3]
                self.b_tilde.append(b_vect)
                if len(self.P_tilde) == 0:
                    # self.P_tilde = self.planes[:,:3]
                    self.P_tilde = self.third_planes[:,:3]
                else:
                    # self.P_tilde = scipy.sparse.block_diag([self.P_tilde,self.planes[:,:3]]).toarray()
                    self.P_tilde = scipy.sparse.block_diag([self.P_tilde,self.third_planes[:,:3]]).toarray()
            else:
                # b_vect = self.second_planes[:,3]
                b_vect = self.third_planes[:,3]
                self.b_tilde.append(b_vect)
                # self.P_tilde = scipy.sparse.block_diag([self.P_tilde,self.second_planes[:,:3]]).toarray()
                self.P_tilde = scipy.sparse.block_diag([self.P_tilde,self.third_planes[:,:3]]).toarray()

        self.b_tilde = np.hstack(self.b_tilde)
        self.P_tilde = np.vstack(self.P_tilde)
        # print " b_tilde: ", self.b_tilde, "its length: ", len(self.b_tilde), "length of P_tilde:", np.shape(np.matrix(self.P_tilde))
    def Boundary_conditions(self):
        t0 = self.Ti_discrete[0]; t1 = self.Ti_discrete[1]; t2 = self.Ti_discrete[2]; t_mid1 = self.t_mid1; t_mid2 = self.t_mid2

        pos_t0 = np.array(self.poly_order_fnct(t0,self.poly_order,"pos"))
        pos_tmid1 = self.poly_order_fnct(t_mid1,self.poly_order,"pos")
        pos_t1 = self.poly_order_fnct(t1,self.poly_order,"pos")
        pos_tmid2 = self.poly_order_fnct(t_mid2,self.poly_order,"pos")
        pos_t2 = self.poly_order_fnct(t2,self.poly_order,"pos")

        vel_t0 = self.poly_order_fnct(t0,self.poly_order,"vel")
        vel_t1 = self.poly_order_fnct(t1,self.poly_order,"vel")
        vel_t2 = self.poly_order_fnct(t2,self.poly_order,"vel")

        acc_t1 = self.poly_order_fnct(t1,self.poly_order,"acc")


        zero_mat = np.zeros(np.array(pos_t0.shape))
        ones_mat = np.array(self.poly_order_fnct(1,self.poly_order,"pos"))
        # print "pos_t0",pos_t0,"its shape: ",np.shape(pos_t0)[:],"\n zeros: ",zero_mat, "\n pos_tmid1: ",pos_tmid1
        self.whole_traj_bc = np.array(np.vstack([np.hstack([pos_t0, zero_mat]),
                        np.hstack([pos_t1, -pos_t1]),
                        np.hstack([zero_mat, pos_t2]),
                        np.hstack([pos_tmid1, zero_mat]),
                        np.hstack([zero_mat, pos_tmid2])]))
                        # np.hstack([ones_mat, ones_mat]),

        self.whole_traj_bc_bvect = np.array(np.vstack([self.x0.T,
                                        np.zeros([3,1]),
                                        self.x2.T,
                                        self.xt1.T,
                                        self.xt2.T]))
                                        # np.zeros([3,1]),

        # self.whole_traj_bc = np.array(np.vstack([np.hstack([pos_t0, zero_mat]),
        #                 np.hstack([pos_t1, -pos_t1]),
        #                 np.hstack([zero_mat, pos_t2])]))


        # self.whole_traj_bc_bvect = np.array(np.vstack([self.x0.T,
        #                                 np.zeros([3,1]),
        #                                 self.x2.T]))

        # self.diff_traj_bc = np.array(np.hstack([vel_t1, -vel_t1]))
        # self.diff_traj_bc_bvect = np.array(np.zeros([3,1]))

        self.diff_traj_bc = np.array(np.vstack( [np.hstack([vel_t1, -vel_t1]), np.hstack([acc_t1, -acc_t1]) ] ))
        self.diff_traj_bc_bvect = np.array(np.zeros([6,1]))


    #Phrase problem
    def Define_Terms(self):
        time_start = time.time()
        total_time_start = time.time()

        # t = sp.symbols("t")

        # pos_var = []
        # vel_var = []
        # for idx in range(self.poly_order):
        #     exponent = self.poly_order - idx
        #     tc = t**exponent
        #     td = exponent*t**(exponent - 1)
        #     pos_var.append(tc)
        #     vel_var.append(td)
        # pos_var.append(1.)
        # Ti_sym = scipy.linalg.block_diag(pos_var,pos_var,pos_var)
        # Ti_diff_sym = scipy.linalg.block_diag(vel_var,vel_var,vel_var)

        """TIME CHECK """
        self.time_check("Step 1 ",time_start); time_start = time.time()

        #"get time matricies"
        self.Get_Time_matricies()

        '''Plane terms for points '''
        self.Get_plane_constraints()


        self.time_check("Step 2 ",time_start);time_start = time.time()


        T_hat_mat = np.array(np.double(self.T_hat))
        T_hat_diff = np.array(np.double(self.T_hat_diff))
        T_hat_diff_left = scipy.sparse.csr_matrix(self.T_hat_diff.transpose())
        T_hat_diff_right =scipy.sparse.csr_matrix(self.T_hat_diff)


        T_hat_2diff_left = scipy.sparse.csr_matrix(self.T_hat_2diff.transpose())
        T_hat_2diff_right =scipy.sparse.csr_matrix(self.T_hat_2diff)

        T_hat_3diff_left = scipy.sparse.csr_matrix(self.T_hat_3diff.transpose())
        T_hat_3diff_right =scipy.sparse.csr_matrix(self.T_hat_3diff)



        print "size of T hat diff: ", np.shape(self.T_hat_diff), "its type: ", type(self.T_hat_diff)
        # H = T_hat_diff_left*T_hat_diff_right
        H = T_hat_3diff_left*T_hat_3diff_right



        f = np.zeros([np.shape(H)[0],1])
        P_tilde_left = scipy.sparse.csr_matrix(self.P_tilde)
        A = np.matrix(P_tilde_left*T_hat_mat)
        b = -self.b_tilde
        #now set equalities: 
        self.Boundary_conditions()
        Aeq = np.vstack([self.whole_traj_bc,self.diff_traj_bc])
        Beq = np.vstack([self.whole_traj_bc_bvect,self.diff_traj_bc_bvect])

        time_start_qp = time.time()
        self.QP_Solve(H,f,A,b,Aeq,Beq)
        self.time_check("QP time: ", time_start_qp)

        # print "\n H shape: ", np.shape(H), "\n A shape: ", np.shape(A), "\n b: ", b
        #add other terms
        self.time_check("Step 3",time_start)

        
        self.time_check("Total time: ",total_time_start)

        self.Plot_poly()





def main():
    opt_obj = Optimization_test()
    opt_obj.Define_Terms()

if __name__ == '__main__':
    main()



