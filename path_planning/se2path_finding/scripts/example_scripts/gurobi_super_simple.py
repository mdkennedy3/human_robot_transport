import sympy as sp
import numpy as np
import scipy
from scipy import linalg, sparse
from se2path_finding.qp_solving_support import poly_qp
from gurobipy import *

# from cvxopt import matrix, solvers
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

class gurobi_simple(object):
    def __init__(self):
        self.P1 = [0,1,-0.7] #stay below y=1
        self.P2 = [0,-1,-0.7] #stay above y = -1
        self.start = [0,0]
        self.goal = [10,0.5]

        self.mid_pt = [5,-0.5]

        self.poly_order = 20 #works as low as order 4
        self.poly_qp_obj = poly_qp.QP_poly_cls()


    def Solve(self):
        #define model variable
        m = Model("qp")

        #Define variables
        alpha_list = []
        for idx in range(int(2*(self.poly_order+1))):
            alpha_list.append(m.addVar(lb=-GRB.INFINITY))
        alpha_vect = np.matrix(alpha_list).T
        m.update()

        #Define objective
        ti= 0; tf = 1
        tmat = np.matrix(self.poly_qp_obj.poly_order(self.poly_order,"Tmat","vel",ti,tf))
        # Tmat = np.matrix(scipy.linalg.block_diag(tmat,tmat,tmat))
        Tmat = np.matrix(scipy.linalg.block_diag(tmat,tmat))
        print "alphavect: ", alpha_vect, " Tmat: ", Tmat
        obj_elem = alpha_vect.T*Tmat*alpha_vect
        obj  = obj_elem.tolist()[0][0]
        m.setObjective(obj)
        m.update()


        #Set Equalities
        tvect_0 = self.poly_qp_obj.poly_order(self.poly_order,"Tvect","pos",ti).T; tvect_0 = np.matrix(scipy.linalg.block_diag(tvect_0,tvect_0))
        tvect_1 = self.poly_qp_obj.poly_order(self.poly_order,"Tvect","pos",tf).T; tvect_1 = np.matrix(scipy.linalg.block_diag(tvect_1,tvect_1))

        p0 = tvect_0*alpha_vect
        p1 = tvect_1*alpha_vect
        m.addConstr(p0.item(0), GRB.EQUAL, self.start[0])
        m.addConstr(p0.item(1), GRB.EQUAL, self.start[1])
        m.addConstr(p1.item(0), GRB.EQUAL, self.goal[0])
        m.addConstr(p1.item(1), GRB.EQUAL, self.goal[1])
        m.update()

        tmid = tf/2.
        tvect_05 = self.poly_qp_obj.poly_order(self.poly_order,"Tvect","pos",tmid).T; tvect_05 = np.matrix(scipy.linalg.block_diag(tvect_05,tvect_05))
        pmid = tvect_05*alpha_vect
        m.addConstr(pmid.item(0), GRB.EQUAL, self.mid_pt[0])
        m.addConstr(pmid.item(1), GRB.EQUAL, self.mid_pt[1])
        m.update()


        #Set Inequalities
        Ti_discrete = np.linspace(ti,tf,100)
        for time in range(1,len(Ti_discrete)-1):
            tvect = self.poly_qp_obj.poly_order(self.poly_order, "Tvect","pos",time).T; tvect = np.matrix(scipy.linalg.block_diag(tvect,tvect))
            point = tvect*alpha_vect
            m.addConstr(self.P1[1]*point.item(1), GRB.LESS_EQUAL, -self.P1[2])
            m.addConstr(self.P2[1]*point.item(1), GRB.LESS_EQUAL, -self.P2[2])
            # m.addConstr(-point.item(1), GRB.LESS_EQUAL, 0.7)
            # m.addConstr( point.item(1), GRB.LESS_EQUAL, 0.7)
            m.update()
        
        #optimize
        m.optimize()

        try:
            #to debug and see which rows are infeasible
            m.computeIIS()
            m.write('gurobi_simple_toy_problem_crash.ilp')
        except:
            pass

        #make vector of coefficients
        coefficients = []
        for v in m.getVars():
            print('%s %g' % (v.varName, v.x))
            coefficients.append(v.x)

        print "coeff:", coefficients
        time = np.linspace(ti,tf,100)
        x = []
        y = []
        for idx in time:
            tvect = self.poly_qp_obj.poly_order(self.poly_order,"Tvect","pos",idx).T; tvect = np.matrix(scipy.linalg.block_diag(tvect,tvect))
            # print "tvect: ", tvect
            point_curr = tvect*np.matrix(coefficients).T
            x.append(point_curr.item(0))
            y.append(point_curr.item(1))


        fig = plt.figure()
        ax = fig.add_subplot(111)

        ax = plt.gca()
        ax.set_xlim([-1,11])
        ax.set_ylim([-2,2])
        c = 'r'; m = '*'



        plt.plot(x,y,'ro')
        # print "x",x,"\n y:",y
        
        xs = [self.start[0], self.goal[0], self.mid_pt[0]]
        ys = [self.start[1], self.goal[1], self.mid_pt[1]]
        ax.scatter(xs,ys,c=c,marker=m)

        plt.show()





def main():
    solver_obj = gurobi_simple()
    solver_obj.Solve()


if __name__ == '__main__':
    main()