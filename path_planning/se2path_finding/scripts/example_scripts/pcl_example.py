import numpy as np
import pcl
import math
#For plotting
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

pts = np.array([[0,0,0],
                [0.5,0.5,0.5],
                [1,1,1],
                [2,2,2],
                [0.5,0,2],
                [2.1,2.2,1.5],
                [3,-1,1],
                [3,1,2],
                [1,2,3],
                [3,2,1],
                [2.1,2.2,2.5],
                [2,2.7,2.8]], dtype=np.float32)

leaf = np.array([0.6,0.6,0.6],dtype=np.float32)
pc = pcl.PointCloud()
pc.from_array(pts)


vox_filt = pc.make_voxel_grid_filter()
vox_filt.set_leaf_size(leaf[0],leaf[1],leaf[2])
filtered_pts = vox_filt.filter()
filtered_pts_to_array = filtered_pts.to_array()
print ("original pts: ", pts)
print ("filtered pts: ", filtered_pts_to_array)
print "pts in pc: ", pc.to_array()

fig = plt.figure(); ax = fig.add_subplot(111, projection='3d') # ax = fig.add_subplot(111)
c = 'b'; m = '^'; xs = pts[:,0].T;  ys = pts[:,1].T;  zs = pts[:,2].T; ax.scatter(xs, ys, zs, c=c, marker=m);
# ax2 = fig.add_subplot(132, projection='3d') # ax = fig.add_subplot(111)
# c = 'b'; m = '^'; xs = pts[:,0].T;  ys = pts[:,1].T;  zs = pts[:,2].T; ax2.scatter(xs, ys, zs, c=c, marker=m);
# c = 'r'; m = 'o'; xs = filtered_pts_to_array[:,0].T;  ys = filtered_pts_to_array[:,1].T;  zs = filtered_pts_to_array[:,2].T; ax2.scatter(xs, ys, zs, c=c, marker=m); ax2.set_xlabel('X axis'); ax2.set_ylabel('Y axis'); ax2.set_zlabel('theta axis'); 
# ax3 = fig.add_subplot(133, projection='3d') # ax = fig.add_subplot(111)
# c = 'r'; m = 'o'; xs = filtered_pts_to_array[:,0].T;  ys = filtered_pts_to_array[:,1].T;  zs = filtered_pts_to_array[:,2].T; ax3.scatter(xs, ys, zs, c=c, marker=m); ax3.set_xlabel('X axis'); ax3.set_ylabel('Y axis'); ax3.set_zlabel('theta axis'); 



cloud = pc.make_octree(0.5)
# min_x, min_y, min_z, max_x, max_y, max_z = -10,-10,-3*math.pi,10,10,3*math.pi
# print "bounding: ",min_x, min_y, min_z, max_x, max_y, max_z
# cloud.define_bounding_box(min_x, min_y, min_z, max_x, max_y, max_z)
#define_bounding_box(self, double min_x, double min_y, double min_z, double max_x, double max_y, double max_z):  
#Define bounding box for octree. Bounding box cannot be changed once the octree contains elements.

#generate pts in cloud
cloud.add_points_from_input_cloud()

#get the voxel centers (based on resolution I presume)
vox_cent = np.array(cloud.get_occupied_voxel_centers())
print "vox cent: ", vox_cent
#Now find closest point to a point in the array
pt1 = np.array([-0.5,-0.5,-0.5],dtype=np.float32)
pt2 = np.array([9.1,9.1,9.1],dtype=np.float32)

c='g'; m = 'o'; xs = vox_cent[:,0]; ys = vox_cent[:,1]; zs = vox_cent[:,2]; ax.scatter(xs, ys, zs, c=c, marker=m)
c='m'; m = '*'; xs = pt1[0]; ys = pt1[1]; zs = pt1[2]; ax.scatter(xs, ys, zs, c=c, marker=m)
c='m'; m = '*'; xs = pt2[0]; ys = pt2[1]; zs = pt2[2]; ax.scatter(xs, ys, zs, c=c, marker=m)

ax.set_xlabel('X axis'); ax.set_ylabel('Y axis'); ax.set_zlabel('theta axis'); 
print "is voxel filled at pt: ",pt1,"\n Answer: ",cloud.is_voxel_occupied_at_point(pt1)
print "is voxel filled at pt: ",pt2,"\n Answer: ",cloud.is_voxel_occupied_at_point(pt2)

plt.show()
