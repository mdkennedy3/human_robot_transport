#!/usr/bin/env python
import rospy
import numpy as np
import numpy.matlib
import time
import tf
import math
import pcl 
#initial path planning
from ompl import base as ob
from ompl import geometric as og
from ompl import control, tools, util

#For plotting
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from se2path_finding.ts_math_operations import ts_math_oper


#required for ompl distance sensing with self defined states

class MySE2(ob.SE2StateSpace):
    def distance(self,state1, state2):
        #print state1.getX(), state2.getX()
        distxy = np.sqrt((state1.getX()-state2.getX())**2 + (state1.getY()-state2.getY())**2)
        ang1 = state1.getYaw()
        ang2 = state2.getYaw()
        R1 = np.matrix([[math.cos(ang1), -math.sin(ang1)],[math.sin(ang1), math.cos(ang1)]])
        R2 = np.matrix([[math.cos(ang2), -math.sin(ang2)],[math.sin(ang2), math.cos(ang2)]])
        Rdiff = R2*R1.T
        ang_diff = math.atan2(Rdiff[1,0],Rdiff[0,0])
        final_dist = distxy + ang_diff 
        return final_dist

class ompl_support(object):
    def isStateValid(self,state):
        # "state" is of type SE2StateInternal, so we don't need to use the "()"
        # operator.
        #
        # Some arbitrary condition on the state (note that thanks to
        # dynamic type checking we can just call getX() and do not need
        # to convert state to an SE2State.)
        # print "state:", state.getX(),state.getY(),state.getYaw()
        curr_state_xy = np.matrix([state.getX(),state.getY()])
        curr_state_yaw = np.matrix([state.getYaw()])
        #See if it is within tolerance distance to occupied cells, if it is not, then it is valid, otherwise it is not
        #Check if norm of state tested
        self.occupied_coord_xy 
        self.occupied_coord_yaw
        # Rdiff = R2*R1.T = A*B
        th1 = np.matlib.repmat(curr_state_yaw,np.shape(self.occupied_coord_yaw)[0],np.shape(self.occupied_coord_yaw)[1])
        th2 = self.occupied_coord_yaw
        a11 = np.cos(th2)
        a12 = -np.sin(th2)
        a21 = np.sin(th2)
        a22 = np.cos(th2)
        b11 = np.cos(th1)
        b21 = -np.sin(th1)
        # print "shapes: a11", np.shape(a11),"a21", np.shape(a21),"b11", np.shape(b11),"b21", np.shape(b21)
        '''check yaw '''
        yaw_diff = np.arctan2(np.multiply(a21,b11) + np.multiply(a22,b21),np.multiply(a11,b11) + np.multiply(a12,b21))

        print "first check, shape of xy: ", np.shape(self.occupied_coord_xy)
        print "dim 1 of occupied: ", self.occupied_coord_xy[:,0]
        print "dim 2 of occupied: ", self.occupied_coord_xy[:,1]
        print "first curr state: ", curr_state_xy.item(0)
        print "second curr state: ", curr_state_xy.item(1)

        xdiff = np.matlib.repmat(curr_state_xy.item(0),np.shape(self.occupied_coord_xy)[0],1) - self.occupied_coord_xy[:,0]
        # print "shapes first: ", np.shape(np.matlib.repmat(curr_state_xy.item(1),np.shape(self.occupied_coord_xy)[0],np.shape(self.occupied_coord_xy)[1])) #," second: ", np.shape(self.occupied_coord_xy[:,1])
        ydiff = np.matlib.repmat(curr_state_xy.item(1),np.shape(self.occupied_coord_xy)[0],1) - self.occupied_coord_xy[:,1]

        world_step_factor = 0.9
        dimx_tol = world_step_factor*self.world.steps['x_steps']
        dimy_tol = world_step_factor*self.world.steps['y_steps']
        dimtheta_tol = world_step_factor*self.world.steps['theta_steps']

        v0 = np.where(abs(xdiff) <= dimx_tol)
        v1 = np.where(abs(ydiff) <= dimy_tol)
        v2 = np.where(abs(yaw_diff) <= dimtheta_tol)
        #now with minimum, find overlap between all three lists
        interior_pt_indicies = list(set(v0[0].tolist()[0]) & set(v1[0].tolist()[0]) & set(v2[0].tolist()[0]))

        print "shape of xdiff, ydiff, yaw_diff", xdiff.shape, ydiff.shape, yaw_diff.shape
        print "type of xdiff, ydiff, yaw_diff", type(xdiff), type(ydiff), type(yaw_diff)
        print "diffs in x: ",xdiff[interior_pt_indicies]
        print "diffs in y: ",ydiff[interior_pt_indicies]
        print "diffs in yaw: ",yaw_diff[interior_pt_indicies]
        print "interior pt indicies", interior_pt_indicies
        print "wtf: ", self.world.world_discretized.shape
        print "occupied cell bool: ", np.where(np.array(self.world.occupied_cell_bool)==1)
        print "occ pts coord: ", np.array(self.world.occupied_cell_bool)[interior_pt_indicies]

        print "problem points: ", self.world.world_discretized[self.world.occupied_cell_bool[interior_pt_indicies],:]
        print "curr point:",curr_state_xy,curr_state_yaw
        #Something is wrong with access, distances are not right
        stop 
        #if true then state valid, else if false state invalid
        return len(interior_pt_indicies) == 0

    def convert_path_to_list(self,path):
        x = []; y = []; yaw = []
        path_list_first = {'x':[],'y':[],'yaw':[]}
        for idx in range(len(path.getStates())):
            x.append(path.getStates()[idx].getX())
            y.append(path.getStates()[idx].getY())
            yaw.append(path.getStates()[idx].getYaw())
        # path_list_first = np.vstack(x,y,yaw)
        path_list_first['x'] = x
        path_list_first['y'] = y
        path_list_first['yaw'] = yaw
        return path_list_first

    def setup_ompl_problem_world_space(self,world, start_pose, goal_pose,time_to_solve):
        self.world = world
        occ_ind = np.where(np.array(self.world.occupied_cell_bool) == 1)
        self.occupied_coord = self.world.world_discretized[occ_ind]
        # print "occupied coord before we get started: ", np.shape(self.occupied_coord), "type: ", type(self.occupied_coord)
        self.occupied_coord_xy = self.occupied_coord[:,:2]
        self.occupied_coord_yaw  = self.occupied_coord[:,2]

        bounds = ob.RealVectorBounds(2)
        bounds.setLow(0,world.extents['xmin'])
        bounds.setLow(1,world.extents['ymin'])

        bounds.setHigh(0,world.extents['xmax'])
        bounds.setHigh(1,world.extents['ymax'])

        #space = ob.SE2StateSpace()
        space = MySE2()
        space.setBounds(bounds)

        start = ob.State(space)
        # we can pick a random start state...
        # start.random()
        # ... or set specific values
        start().setX(start_pose.item(0))
        start().setY(start_pose.item(1))
        start().setYaw(start_pose.item(2))

        goal = ob.State(space)
        # we can pick a random goal state...
        goal().setX(goal_pose.item(0))
        goal().setY(goal_pose.item(1))
        goal().setYaw(goal_pose.item(2))

        si = ob.SpaceInformation(space)
        si.setStateValidityChecker(ob.StateValidityCheckerFn(self.isStateValid))

        pd = ob.ProblemDefinition(si)
        pd.setStartAndGoalStates(start,goal)

        planner = og.RRTConnect(si)
        planner.setProblemDefinition(pd)
        planner.setup()

        solved = planner.solve(time_to_solve)


        if solved:
            path = pd.getSolutionPath()
            ps = og.PathSimplifier(si)
            ps.smoothBSpline(path) #makes bspline path out of path variable
            path_list = self.convert_path_to_list(path)
        else:
            path_list = []

        #access elements of path_list with dict calls to 'x','y','yaw'
        return path_list



class virtual_obstacles(object):
    def __init__(self,x1,y1,x2,y2,world):
        #This class makes artificial obstacles given the bounding corners
        #the first corner (x1,y1), and opposite corner (x2,y2), let x1<x2 & y1<y2
        self.corner1 = [x1,y1]; self.corner2 = [x2,y2]
        # xstep = world.steps['x_steps']/2.; ystep = world.steps['y_steps']/2.  #was division by 3
        xstep = 2*world.steps['x_steps']; ystep = 2*world.steps['y_steps']
        x_num,x_rem = divmod(abs(x2-x1),xstep); y_num,y_rem = divmod(abs(y2-y1),ystep)
        x_coord = np.linspace(x1,x2,x_num); y_coord = np.linspace(y1,y2,y_num)  
        if x_rem != 0: np.append(x_coord,x2)
        if y_rem != 0: np.append(y_coord,y2)
        obstacle_side_1 = []; obstacle_side_2 = []; obstacle_side_3 = []; obstacle_side_4 = []
        for jdx in range(len(y_coord)): point = [x1, y_coord[jdx]]; obstacle_side_1.append(point)
        for jdx in range(len(y_coord)): point = [x2, y_coord[jdx]]; obstacle_side_2.append(point)
        for jdx in range(len(x_coord)): point = [x_coord[jdx],y1]; obstacle_side_3.append(point)
        for jdx in range(len(x_coord)): point = [x_coord[jdx],y2]; obstacle_side_4.append(point)
        pts = np.matrix(np.vstack([obstacle_side_1,obstacle_side_2,obstacle_side_3,obstacle_side_4]))

        obs_list = []
        for idx in x_coord:
            for jdx in y_coord:
                obs_list.append([idx,jdx])
        pts = np.matrix(obs_list)  #fill in obstacles densly (filter will handle data size)


        self.points = pts
        #obstacle points plot:
        # fig = plt.figure(); ax = fig.add_subplot(111);c = 'r'; m = 'o'; xs = pts[:,0].T; ys = pts[:,1].T; xs = xs.tolist()[0]; ys = ys.tolist()[0]; ax.scatter(xs, ys, c=c, marker=m); plt.show()
        #real sensor has 270 readings per sweep, that times number of convex hull verticies, then times the number of steps of theta
class world_info(object):
    def __init__(self,xmin,xmax,ymin,ymax,x_steps,y_steps,theta_steps):
        self.extents = {'xmin':xmin,'xmax':xmax,'ymin':ymin,'ymax':ymax}
        self.steps = {'x_steps':x_steps,'y_steps':y_steps, 'theta_steps':theta_steps}
        self.world_discretized = []
        self.occupied_cell_bool = []  #0 is free, 1 is occupied
        self.x_coord = []
        self.y_coord = []
        self.th_coord = []


class Assistive_Carry_Path_Finder(object):
    """docstring for Assistive_Carry_Path_Finder"""
    def __init__(self):
        # super(Assistive_Carry_Path_Finder, self).__init__()  %can be used to call functions from parent class:: class Child(Parent) to cause inheritence
        self.transported_object= []
        self.ts_math_oper = ts_math_oper.ts_math_oper_fncts()
        self.world = []
        self.start_pose = []
        self.goal_pose = []

    def generate_virtual_obstacles(self):
        #obstacles generated in code here. Must be run after world has been generated
        x1,y1,x2,y2 = -10,0,10,10;  virt_obs1 = virtual_obstacles(x1,y1,x2,y2,self.world);
        x1,y1,x2,y2 = -12,-20,10,-10; virt_obs2 = virtual_obstacles(x1,y1,x2,y2,self.world); 
        # print('corners for obs1:',x1,y1,' and ',x2,y2); print('corners for obs2:',x1,y1,' and ',x2,y2);
        # x1,y1,x2,y2 = x1,y1,x2,y2; virt_obs3 = virtual_obstacles(x1,y1,x2,y2,self.world.extents)
        self.virtual_obstacles = {'virt_obs1':virt_obs1,'virt_obs2':virt_obs2};#,'virt_obs3':virt_obs3}
        # pts = np.vstack([virt_obs1.points,virt_obs2.points])
        # fig = plt.figure(); ax = fig.add_subplot(111);c = 'r'; m = 'o'; xs = pts[:,0].T; ys = pts[:,1].T; xs = xs.tolist()[0]; ys = ys.tolist()[0]; ax.scatter(xs, ys, c=c, marker=m); plt.show()


    def minkowski_object_points(self,show_mink_pts):
        #this function takes in sensed data and adds minkowski sum to points for given orientations of the object
        print('number of obstacles: ', len(self.virtual_obstacles))
        object_pts = np.vstack([self.virtual_obstacles['virt_obs1'].points,self.virtual_obstacles['virt_obs2'].points]) #These are all points
        test= self.virtual_obstacles['virt_obs1'].points
        minkowski_pts = []
        for idx in range(len(self.world.th_coord)):
            theta = self.world.th_coord[idx]
            R = np.matrix([[math.cos(theta), -math.sin(theta)],[math.sin(theta), math.cos(theta)]])
            vects_rotated = R*self.transported_object.vert.T
            for jdx in range(vects_rotated.shape[1]):
                vect_curr = vects_rotated[:,jdx]
                vects_rotated_mat = np.matlib.repmat(vect_curr.T,len(object_pts),1)
                theta_vect = np.matrix(np.matlib.repmat(theta,len(object_pts),1))
                mink_xy  = object_pts + vects_rotated_mat
                mink_pts = np.concatenate((mink_xy,theta_vect),axis=1)
                minkowski_pts.append(mink_pts)
        #long Nx2 matrix of points that are on top of sensed points
        self.minkowski_pts = np.vstack(minkowski_pts)
        #AT THIS POINT I SHOULD PLOT TO VISUALIZE OBSTACLE POINT, WORLD AND minkowski_pts
        if show_mink_pts:
            fig = plt.figure(); ax = fig.add_subplot(111, projection='3d') # ax = fig.add_subplot(111)
            #minkowski points
            c = 'b'; m = '^'; xs = self.minkowski_pts[:,0].T; xs = xs.tolist()[0]; ys = self.minkowski_pts[:,1].T; ys = ys.tolist()[0]; zs = self.minkowski_pts[:,2].T; zs = zs.tolist()[0]; ax.scatter(xs, ys, zs, c=c, marker=m);
            #obstacle points
            c = 'r'; m = 'o'; xs = object_pts[:,0].T; xs = xs.tolist()[0]; ys = object_pts[:,1].T; ys = ys.tolist()[0]; zs = np.zeros(len(xs)).tolist(); ax.scatter(xs, ys, zs, c=c, marker=m); ax.set_xlabel('X axis'); ax.set_ylabel('Y axis'); ax.set_zlabel('theta axis'); 
            plt.show()
        #NOTE: in real sensor case, all currently sensed points get the minkowski vectors

    def discretize_world(self,xmin,xmax,ymin,ymax,x_steps,y_steps,theta_steps):
        self.world = world_info(xmin,xmax,ymin,ymax,x_steps,y_steps,theta_steps)  #[xmin,xmax;ymin,ymax]  (angle is 0:2pi)
        #discretize the world, given the number of steps in x,y, theta
        x_num,x_rem = divmod(abs(xmax-xmin),x_steps); y_num,y_rem = divmod(abs(ymax-ymin),y_steps); th_num,th_rem = divmod((2*math.pi),theta_steps)
        x_coord = np.linspace(self.world.extents['xmin'],self.world.extents['xmax'],x_num); y_coord = np.linspace(self.world.extents['ymin'],self.world.extents['ymax'],y_num);  th_coord = np.linspace(0,2*math.pi,th_num)
        if x_rem != 0: np.append(x_coord,xmax)
        if y_rem != 0: np.append(y_coord,ymax)
        if th_rem != 0: np.append(th_coord,math.pi)
        world_discretized = []; occupied_coord = []
        for idx in range(len(x_coord)):
            for jdx in range(len(y_coord)):
                for kdx in range(len(th_coord)):
                    point = [x_coord[idx],y_coord[jdx], th_coord[kdx]]
                    world_discretized.append(point)
                    occupied_coord.append(0)
        world_discretized = np.matrix(np.vstack(world_discretized))
        occupied_coord = occupied_coord
        self.world.world_discretized = world_discretized
        self.world.occupied_cell_bool = occupied_coord
        self.world.x_coord = x_coord; self.world.y_coord = y_coord; self.world.th_coord = th_coord

    def get_object_pose(self,object_start_pose,object_goal_pose):
        # Get the object pose, either from a msg or manually
        class transported_object(Assistive_Carry_Path_Finder):
            def __init__(self,object_start_pose,object_goal_pose):
                self.start_pose = object_start_pose
                self.pose = object_start_pose
                self.vert = np.matrix([[2,0],[-1,1],[-1,-1]]) #verticies of convex object when orientation is 0deg
                self.goal_pose = object_goal_pose
        self.transported_object= transported_object(object_start_pose,object_goal_pose)

        #Now make option to get pose in world frame using tf with mocap for real robot
        # object_pose_obj = self.ts_math_oper.TF_btw_frames(rob_tf,world_tf)
        # object_pose_vect = self.ts_math_oper.tf_to_mat(object_pose_obj)
        # R = self.ts_math_oper.quat_to_rot(object_pose_vect[3:])
        # th = math.atan2(R[1,0],R[0,0]) #atan2 takes in the signs, and this will produce value of theta between [-pi,pi]
        # self.object_pose = np.matrix([object_pose_vect[0],object_pose_vect[1],th])

    def point_assignment(self,point,point_list,dim0_tol,dim1_tol,dim2_tol):
        #This function takes in a point, and a list of points, and determines which points from the list are within all tolerances to the given point, returning their indicies in the point list:
        point_dim0_vect = np.matlib.repmat(point[0,0],point_list.shape[0],1); dim_0_diff = point_list[:,0] - point_dim0_vect
        # abs_diff = abs(dim_0_diff)
        # print('min element of dim diff:',abs_diff.min())
        # print('tolerance: ', dim0_tol)
        point_dim1_vect = np.matlib.repmat(point[0,1],point_list.shape[0],1); dim_1_diff = point_list[:,1] - point_dim1_vect
        point_dim2_vect = np.matlib.repmat(point[0,2],point_list.shape[0],1); dim_2_diff = point_list[:,2] - point_dim2_vect
        v0 = np.where(abs(dim_0_diff) <= dim0_tol)
        v1 = np.where(abs(dim_1_diff) <= dim1_tol)
        v2 = np.where(abs(dim_2_diff) <= dim2_tol)
        #now with minimum, find overlap between all three lists
        interior_pt_indicies = list(set(v0[0].tolist()[0]) & set(v1[0].tolist()[0]) & set(v2[0].tolist()[0]))




        # if len(interior_pt_indicies) != 0:
        #     print "interior indicies", interior_pt_indicies
        # if len(interior_pt_indicies) == 0:
        #     temp_list = abs(dim_1_diff)
            # print('min of list v1: ',np.amin(temp_list))
            # print('v1 threshold: ', dim1_tol)
            # print('\n No Intersection: \n')
            # print('v0,length:',len(v0[0].tolist()[0]))
            # print('v1,length:',len(v1[0].tolist()[0]))
            # print('v2,length:',len(v2[0].tolist()[0]))

        return interior_pt_indicies

    def get_occupied_points(self):
        ''' Given world cells, obstacle locations, define a list for 
        world points that assigns that point as either free or occupied, 
        based on whether an obstacle point is within cell distance of that cell '''
        #for artificial data collect points given from known map for real data, query the sensor to fill the map with percentage occupied. Make list of 'active' obstacle points
        active_obstacle_pts = self.minkowski_pts.tolist()  #With real sensor, this list of mink points is much smaller and is used to fill occ/unocc pts
        active_obstacle_pts_mat = np.matrix(active_obstacle_pts)
        # print "active obstacle pts: ", active_obstacle_pts_mat.shape
        # print "x extents: ", active_obstacle_pts_mat[:,0].min(),",",active_obstacle_pts_mat[:,0].max()
        # print "y extents: ", active_obstacle_pts_mat[:,1].min(),",",active_obstacle_pts_mat[:,1].max()
        # print "z extents: ", active_obstacle_pts_mat[:,2].min(),",",active_obstacle_pts_mat[:,2].max()

        pts_xmin, pts_xmax = active_obstacle_pts_mat[:,0].min(),active_obstacle_pts_mat[:,0].max()
        pts_ymin, pts_ymax = active_obstacle_pts_mat[:,1].min(),active_obstacle_pts_mat[:,1].max()
        pts_zmin, pts_zmax = active_obstacle_pts_mat[:,2].min(),active_obstacle_pts_mat[:,2].max()

        world_discretized_pts = self.world.world_discretized  #Numpy matrix Mx3
        #Indexing through the obstacle list. Find world point that is closest
        #While the list of active points is not empty:

        world_pts_to_consider = []

        for x,y,z in world_discretized_pts.tolist():
            if x >= pts_xmin and x <= pts_xmax and y >= pts_ymin and y <= pts_ymax and z >= pts_zmin and z <= pts_zmax:
                world_pts_to_consider.append([x,y,z])
        world_pts_to_consider = np.matrix(world_pts_to_consider)
        # print "size of those to consider: ", world_pts_to_consider.shape
        world_discretized_pts = world_pts_to_consider

        first_pts = world_discretized_pts


        #Filter the points: 
        # print "length of active_obstacle_pts before filter: ", len(active_obstacle_pts)
        pts = np.float32(np.array(active_obstacle_pts))
        world_step_factor = np.float32(0.9)
        leaf = np.array([np.float32(world_step_factor*self.world.steps['x_steps']),
                        np.float32(world_step_factor*self.world.steps['y_steps']),
                        np.float32(world_step_factor*self.world.steps['theta_steps'])],dtype=np.float32)
        pc = pcl.PointCloud()
        pc.from_array(pts)
        vox_filt = pc.make_voxel_grid_filter()
        vox_filt.set_leaf_size(leaf[0],leaf[1],leaf[2])
        filtered_pts = vox_filt.filter()
        active_obstacle_pts = np.matrix(filtered_pts.to_array()).tolist()
        self.filtered_mink_pts = np.matrix(active_obstacle_pts)
        # print "length of active_obstacle_pts after filter: ", len(active_obstacle_pts)



        while len(active_obstacle_pts) != 0 and not rospy.is_shutdown():
            #Find other points in obstacle list that are within bounds of the world point, and remove them from active list
            obs_point = np.matrix(active_obstacle_pts.pop()) #get last element of active list, removing it
            obs_point_rep = np.matlib.repmat(np.matrix(obs_point),world_discretized_pts.shape[0],1)
            index_occupied = np.sum(np.square(world_discretized_pts-obs_point_rep),1).argmin()
            self.world.occupied_cell_bool[index_occupied] = 1
            # print "idx: ", index_occupied, "set to one: ",self.world.occupied_cell_bool[index_occupied] 
            # print "length of active list: ", len(active_obstacle_pts)

        # print "occ bool cells: ",self.world.occupied_cell_bool, "\ntype: ",type(self.world.occupied_cell_bool)
        # print "OCCUPIED BOOL CELL: ", np.where(np.array(self.world.occupied_cell_bool) == 1)

        # print "first pts: ", first_pts
        # pts = np.matrix(first_pts)
        # print "type: ", type(world_discretized_pts)
        # print "shape: " , world_discretized_pts.shape
        indicies = np.where(np.array(self.world.occupied_cell_bool)==1)
        # print "active indicies: ", indicies[0]
        # print "type before:",type(self.world.occupied_cell_bool)
        pts = world_discretized_pts[indicies[0],:]

        self.hold_pts = pts

        # fig = plt.figure(); ax = fig.add_subplot(121, projection='3d'); ax2 = fig.add_subplot(122, projection='3d')
        # #world pts: 
        # c = 'r'; m = 'o'; xs = pts[:,0]; xs = xs.tolist(); ys = pts[:,1]; ys = ys.tolist(); zs = pts[:,2]; zs = zs.tolist(); ax.scatter(xs, ys, zs, c=c, marker=m)
        # c = 'r'; m = 'o'; xs = pts[:,0]; xs = xs.tolist(); ys = pts[:,1]; ys = ys.tolist(); zs = pts[:,2]; zs = zs.tolist(); ax2.scatter(xs, ys, zs, c=c, marker=m)
        # #mink pts
        # c = 'b'; m = '^'; xs = self.minkowski_pts[:,0].T; xs = xs.tolist()[0]; ys = self.minkowski_pts[:,1].T; ys = ys.tolist()[0]; zs = self.minkowski_pts[:,2].T; zs = zs.tolist()[0]; ax.scatter(xs, ys, zs, c=c, marker=m);
        # # c = 'b'; m = '^'; xs = self.minkowski_pts[:,0].T; xs = xs.tolist()[0]; ys = self.minkowski_pts[:,1].T; ys = ys.tolist()[0]; zs = self.minkowski_pts[:,2].T; zs = zs.tolist()[0]; ax2.scatter(xs, ys, zs, c=c, marker=m);
        # #mink pts filtered: 
        # c = 'g'; m = '^'; xs = self.filtered_mink_pts[:,0].tolist(); ys = self.filtered_mink_pts[:,1].tolist(); zs = self.filtered_mink_pts[:,2].tolist(); ax.scatter(xs,ys,zs,c=c,marker=m)
        # # c = 'g'; m = '^'; xs = filtered_mink_pts[:,0].tolist(); ys = filtered_mink_pts[:,1].tolist(); zs = filtered_mink_pts[:,2].tolist(); ax2.scatter(xs,ys,zs,c=c,marker=m)
        # #obstacle points
        # # object_pts = np.vstack([self.virtual_obstacles['virt_obs1'].points,self.virtual_obstacles['virt_obs2'].points]) #These are all points
        # # c = 'r'; m = 'o'; xs = object_pts[:,0].T; xs = xs.tolist()[0]; ys = object_pts[:,1].T; ys = ys.tolist()[0]; zs = np.zeros(len(xs)).tolist(); ax.scatter(xs, ys, zs, c=c, marker=m);
        
        # #use later:
        # # c = 'g'; m = 'o'
        # # xs = self.initial_path[:,0]; ys = self.initial_path[:,1]; zs = self.initial_path[:,2]
        # # print "xs: ",xs
        # # ax2.scatter(xs.tolist(),ys.tolist(),zs.tolist(),c=c,marker=m)

        # ax.set_xlabel('X axis'); ax.set_ylabel('Y axis'); ax.set_zlabel('theta axis'); ax2.set_xlabel('X axis'); ax2.set_ylabel('Y axis'); ax2.set_zlabel('theta axis'); 
        # plt.show()

        """NOTE THAT THIS MAY RUN LONG (7sec) IN FIRST PASS, BUT WITH ONLY SMALL NEW DATA THIS WILL BE FASTER (ORDER OF 2-3sec or less) """



    def obtain_initial_viable_path(self):
        print('in this function obtain a path from method such as A*,RRT or manual, that QP expands on')
        #Path is defined as a list of verticies that contain the start and goal position at the top and bottom of the list respectively

        #Define path manually
        # self.initial_path = np.matrix([[-20.,-20.,0.],
        #                                 [-20.,-5.,30.*math.pi/180],
        #                                 [20,-5.,60.*math.pi/180],
        #                                 [20.,20.,90.*math.pi/180]])

        #RRT connect** (design with toy variables to make sure it works)
        self.ompl_support = ompl_support() #supporting functions 

        time_to_solve = 10.0
        self.path_dict = self.ompl_support.setup_ompl_problem_world_space(self.world, self.transported_object.start_pose, self.transported_object.goal_pose, time_to_solve)

        self.initial_path = np.matrix(np.vstack([self.path_dict['x'],self.path_dict['y'],self.path_dict['yaw']])).T

        print "initial path: ", self.initial_path


        # occ_ind = np.where(np.array(self.world.occupied_cell_bool) == 1)
        # pts = self.world.world_discretized[occ_ind]


        pts = self.hold_pts 

        fig = plt.figure(); ax = fig.add_subplot(121, projection='3d'); ax2 = fig.add_subplot(122, projection='3d')
        #world pts: 
        c = 'r'; m = 'o'; xs = pts[:,0]; xs = xs.tolist(); ys = pts[:,1]; ys = ys.tolist(); zs = pts[:,2]; zs = zs.tolist(); ax.scatter(xs, ys, zs, c=c, marker=m)
        c = 'r'; m = 'o'; xs = pts[:,0]; xs = xs.tolist(); ys = pts[:,1]; ys = ys.tolist(); zs = pts[:,2]; zs = zs.tolist(); ax2.scatter(xs, ys, zs, c=c, marker=m)
        #mink pts
        c = 'b'; m = '^'; xs = self.minkowski_pts[:,0].T; xs = xs.tolist()[0]; ys = self.minkowski_pts[:,1].T; ys = ys.tolist()[0]; zs = self.minkowski_pts[:,2].T; zs = zs.tolist()[0]; ax.scatter(xs, ys, zs, c=c, marker=m);
        #mink pts filtered: 
        c = 'g'; m = '^'; xs = self.filtered_mink_pts[:,0].tolist(); ys = self.filtered_mink_pts[:,1].tolist(); zs = self.filtered_mink_pts[:,2].tolist(); ax.scatter(xs,ys,zs,c=c,marker=m)
        #use later:
        c = 'g'; m = 'o'
        xs = self.initial_path[:,0]; ys = self.initial_path[:,1]; zs = self.initial_path[:,2]
        print "xs: ",xs
        ax2.scatter(xs.tolist(),ys.tolist(),zs.tolist(),c=c,marker=m)

        ax.set_xlabel('X axis'); ax.set_ylabel('Y axis'); ax.set_zlabel('theta axis'); ax2.set_xlabel('X axis'); ax2.set_ylabel('Y axis'); ax2.set_zlabel('theta axis'); 
        plt.show()



def main():
    rospy.init_node('Path_finder')
    time_start = time.time()
    cls_obj = Assistive_Carry_Path_Finder()
    # xmin,xmax,ymin,ymax = -10,10,-20,20
    xmin,xmax,ymin,ymax = -30,30,-30,30
    x_steps,y_steps,theta_steps = 1,1,10*math.pi/180  #in meters and radians
    cls_obj.discretize_world(xmin,xmax,ymin,ymax,x_steps,y_steps,theta_steps)
    #define object to be carried:
    object_goal_pose = np.matrix([20,20,90*math.pi/180])
    object_start_pose = np.matrix([-20,-20,0])
    cls_obj.get_object_pose(object_start_pose,object_goal_pose)
    #define virtual obstacles:
    cls_obj.generate_virtual_obstacles()
    #Generate minkowski for static virtual objects (same for real sensed points, just adjusted and in the loop)
    show_mink_pts = False
    cls_obj.minkowski_object_points(show_mink_pts)



    cls_obj.get_occupied_points()
    cls_obj.obtain_initial_viable_path()

    time_stop = time.time()
    diff_time = time_stop - time_start
    print "code runs in: ", diff_time, " seconds"
    # while not rospy.is_shutdown():
    #     loop while not shutdown
    #     print 'in while loop'
    #     print arg


if __name__ == '__main__':
    main()