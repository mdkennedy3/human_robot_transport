#!/usr/bin/env python
import rospy
import iiwa_msgs.msg as iiwa 
from sensor_msgs.msg  import JointState


import sys
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg as geom_msgs

#IN terminal that this is run in (or in the launch file)
# export ROS_NAMESPACE=/iiwa


def main():

    def jointposCallback(jp):
        print "\n\n\n joint position: ",  jp.position
        print "\n joint velocity: ",  jp.velocity

    moveit_commander.roscpp_initialize(sys.argv)#this is always necessary

    rospy.init_node('python_iiwa_test')


    #should query state, but issue is: Robot semantic description not found. Did you forget to define or remap '/robot_description_semantic'?
    robot = moveit_commander.RobotCommander()
    print"current state from moveit_commander: ",robot.get_current_state()
    scene = moveit_commander.PlanningSceneInterface()
    group = moveit_commander.MoveGroupCommander("manipulator") 

    #plan with random position
    # rand_goal_pose= group.get_random_joint_values()
    # path_to_rand_goal = group.plan(rand_goal_pose)
    # print "path to random pose: ", path_to_rand_goal

    #Plan to a given pose: 
    pose_target = geom_msgs.Pose()
    pose_target.orientation.w = 0.707068357078
    pose_target.orientation.x = 6.4378231585e-05
    pose_target.orientation.y = -0.707145199351
    pose_target.orientation.z = 3.61860014377e-05
    pose_target.position.x = -0.066
    pose_target.position.y = -0.159
    pose_target.position.z = 1.169

    #to visualize trajectory
    display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',moveit_msgs.msg.DisplayTrajectory)
    rospy.sleep(10) #let rviz init

    #set the pose target
    group.set_pose_target(pose_target)
    group.set_planner_id("RRTConnectkConfigDefault")
    plan1 = group.plan(); rospy.sleep(5) #gives rviz a chance to display plan 1

    #Display the planned trajectory: 

    disp_traj = moveit_msgs.msg.DisplayTrajectory()
    disp_traj.trajectory_start = robot.get_current_state()
    disp_traj.trajectory.append(plan1)
    display_trajectory_publisher.publish(disp_traj) #display the traj
    rospy.sleep(5) #wait 5 seconds




    '''So move to a desired position in cartesian, then move wrist to desired orientation and iterate in joint space for pouring motion!!!! '''


    # msg_str = "/iiwa/joint_states"; #or : # msg_str=  "/iiwa/state/JointPosition"
    # print "msg string is: ", msg_str
    # sub = rospy.Subscriber(msg_str,JointState,jointposCallback)
    # print "Subscriber called"
    # rospy.spin()

    moveit_commander.roscpp_shutdown()

if __name__ == '__main__':
    main()