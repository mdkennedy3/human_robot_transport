import yaml
data = {"a":[1,2,3],"b":[4,5,6]}; data = [data,data]
p = {"r":1}; data = [data,p]

with open('testdata.yml','w') as outfile:
    print "writing yaml file"
    outfile.write(yaml.dump(data, default_flow_style=True))

with open('testdata.yml','r') as stream:
    try:
        print "\n reading yaml file: \n"
        print(yaml.load(stream))
    except yaml.YAMLError as exc:
        print(exc)
