import sympy as sp
import numpy as np
import scipy
from scipy import linalg 
from scipy import sparse
import time
import cvxopt


from se2path_finding.qp_solving_support import poly_qp

from gurobipy import *

# from cvxopt import matrix, solvers
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt





class Optimization_test(object):
    def __init__(self):
        self.p1 = [0,0,1,-1]; #top positive z, plane point of p1
        self.p2 = [0,0,-1,-1];#bottom, neg z, plane point of p2
        self.p3 = [1,0,0,-1]; #front (positve x),  plane point of p3
        self.p4 = [-1,0,0,-1];#back (negative x),  plane point of p4
        self.p5 = [0,1,0,-1]; #right (pos y), plane point of p5
        self.p6 = [0,-1,0,-1];#left (neg y), plane point of p6
        self.planes = np.array([self.p1,self.p2,self.p3,self.p4,self.p5,self.p6])
        #%Second Set of Constraints
        # self.p1_second = [0,0,1,-3]; #%top positive z, plane point of p1
        # self.p2_second = [0,0,-1,1]; #%bottom, neg z, plane point of p2
        # self.p3_second = [1,0,0,-3]; #%front (positve x),  plane point of p3
        # self.p4_second = [-1,0,0,1]; #%back (negative x),  plane point of p4
        # self.p5_second = [0,1,0,-3]; #%right (pos y), plane point of p5
        # self.p6_second = [0,-1,0,1]; #%left (neg y), plane point of p6

        self.p1_second = [0,0,1,-3]; #%top positive z, plane point of p1
        self.p2_second = [0,0,-1,1]; #%bottom, neg z, plane point of p2
        self.p3_second = [1,0,0,-3]; #%front (positve x),  plane point of p3
        self.p4_second = [-1,0,0,1]; #%back (negative x),  plane point of p4
        self.p5_second = [0,1,0,-3]; #%right (pos y), plane point of p5
        self.p6_second = [0,-1,0,1]; #%left (neg y), plane point of p6
        self.second_planes = np.array([self.p1_second,self.p2_second,self.p3_second,self.p4_second,self.p5_second,self.p6_second]);

        #%Third Set of Constraints
        self.p1_third = [0,0,1,-3]; #%top positive z, plane point of p1
        self.p2_third = [0,0,-1,-1]; #%bottom, neg z, plane point of p2
        self.p3_third = [1,0,0,-3]; #%front (positve x),  plane point of p3
        self.p4_third = [-1,0,0,-1]; #%back (negative x),  plane point of p4
        self.p5_third = [0,1,0,-3]; #%right (pos y), plane point of p5
        self.p6_third = [0,-1,0,-1]; #%left (neg y), plane point of p6
        self.third_planes = np.array([self.p1_third,self.p2_third,self.p3_third,self.p4_third,self.p5_third,self.p6_third]);



        #%Critical Points
        self.x0 = np.matrix([-1,-1,-1]);
        self.x1 = np.matrix([1,1,1]);
        self.x2 = np.matrix([3,3,3]);
        #%Intermediate Points
        self.xt1 = np.matrix([0,0.5,0.5]);
        self.xt2 = np.matrix([2,1.5,1.5]);
        #%Critical Times
        self.t0 = 0;
        self.t_mid1 = 0.25;
        self.t1 = 0.5;
        self.t_mid2 = 0.75;
        self.t2 = 1;
        self.Ti_discrete = [self.t0,self.t1,self.t2];
        self.poly_order = 3

        # num_chords = 6  #this is determined in code
        # '''These should not be evenly spaced, they should be based on chord length to total path length '''
        # self.Ti_discrete = np.linspace(0,1,num_chords)

    def time_check(self,str,time_start):
        time_stop = time.time()
        diff_time = time_stop - time_start
        print str," code runs in: ", diff_time

    #Required in any polynomial solver
    def poly_order_fnct(self,t,order,poly_type):
        pos_time = []
        vel_time = []
        acc_time = []
        jerk_time = []
        for idx in range(order):
            exp = (order - idx)
            pos_time.append(t**exp)
            vel_time.append(exp*t**(exp-1))
            if exp >= 2:
                acc_time.append(exp*(exp-1)*t**(exp-2))
            else:
                acc_time.append(0.)

            if exp >=3:
                jerk_time.append(exp*(exp-1)*(exp-2)*t**(exp-3))
            else:
                jerk_time.append(0.)

        pos_time.append(1.)
        vel_time.append(0.)
        acc_time.append(0.)
        jerk_time.append(0.)
        pos_time = scipy.sparse.block_diag([pos_time,pos_time,pos_time]).toarray()
        vel_time = scipy.sparse.block_diag([vel_time,vel_time,vel_time]).toarray()
        acc_time = scipy.sparse.block_diag([acc_time,acc_time,acc_time]).toarray()
        jerk_time = scipy.sparse.block_diag([jerk_time,jerk_time,jerk_time]).toarray()

        # print "pos var: ", pos_time,"vel_time", vel_time
        if poly_type in "pos":
            return pos_time
        elif poly_type in "vel":
            return vel_time
        elif poly_type in "acc":
            return acc_time
        elif poly_type in "jerk":
            return jerk_time
        elif poly_type in "all":
            #ie access with v[:,0] for pos 
            return [pos_time, vel_time, acc_time, jerk_time]
    def Get_Time_matricies(self):
        self.T_hat_3diff = []
        self.T_hat_2diff = []
        self.T_hat_diff = []
        self.T_hat = []
        self.int_time_steps = 200 #time points to use along a chord (these points will respect the constraints of cvx)
        for idx in range(len(self.Ti_discrete)-1):
            Ti_int = np.linspace(self.Ti_discrete[idx], self.Ti_discrete[idx+1], self.int_time_steps) #get time in this region

            # T_diff_step = scipy.sparse.block_diag((np.array([self.poly_order_fnct(t,7,"vel") for t in Ti_int])))
            # T_diff_step = T_diff_step.toarray()
            # print "shape of T_diff_step: ", np.shape(T_diff_step)
            # T_step = scipy.sparse.block_diag((np.array([self.poly_order_fnct(t,7,"pos") for t in Ti_int])))
            # T_step = T_step.toarray()

            # T_diff_step = scipy.sparse.block_diag((np.array([self.poly_order_fnct(t,7,"vel") for t in Ti_int]))).toarray()
            # T_step = scipy.sparse.block_diag((np.array([self.poly_order_fnct(t,7,"pos") for t in Ti_int]))).toarray()

            T_diff_step = np.vstack(np.array([self.poly_order_fnct(t,self.poly_order,"vel") for t in Ti_int]))
            T_step = np.vstack(np.array([self.poly_order_fnct(t,self.poly_order,"pos") for t in Ti_int]))

            T_2diff_step = np.vstack(np.array([self.poly_order_fnct(t,self.poly_order,"acc") for t in Ti_int]))
            T_3diff_step = np.vstack(np.array([self.poly_order_fnct(t,self.poly_order,"jerk") for t in Ti_int]))

            #2nd diff
            if len(self.T_hat_2diff) == 0:
                self.T_hat_2diff = T_2diff_step
            else:
                self.T_hat_2diff = scipy.sparse.block_diag((self.T_hat_2diff,T_2diff_step))
                self.T_hat_2diff = self.T_hat_2diff.toarray()
            #3rd diff
            if len(self.T_hat_3diff) == 0:
                self.T_hat_3diff = T_3diff_step
            else:
                self.T_hat_3diff = scipy.sparse.block_diag((self.T_hat_3diff,T_3diff_step))
                self.T_hat_3diff = self.T_hat_3diff.toarray()

            #here before: 
            if len(self.T_hat_diff) == 0:
                self.T_hat_diff = T_diff_step
            else:
                self.T_hat_diff = scipy.sparse.block_diag((self.T_hat_diff,T_diff_step))
                self.T_hat_diff = self.T_hat_diff.toarray()

            if len(self.T_hat) == 0:
                self.T_hat = T_step
            else:
                self.T_hat = scipy.sparse.block_diag((self.T_hat,T_step))
                self.T_hat = self.T_hat.toarray()
        print "size of T_hat",np.shape(self.T_hat), "\n size of T_hat_diff", np.shape(self.T_hat_diff)
    def QP_Solve(self,H,f,A,b,Aeq,Beq):
        # """QP Solver
        # Args: 
        #     H
        #     f
        #     A
        #     b
        #     Aeq
        #     Beq
        # Returns: 
        # x from x'Hx + fx subj to Ax <= b and Aeq x = b 
        # """
        print "shape: H ", np.shape(H), "\n shape f: ", np.shape(f),"\n shape A: ", np.shape(A), "\n shape b: ", np.shape(b), "\n shape Aeq: ", np.shape(Aeq), "\n shape Beq:", np.shape(Beq)
        Q = cvxopt.matrix(H.toarray())
        self.Q = Q
        p = cvxopt.matrix(f)
        G = cvxopt.matrix(A)
        bfinal =  np.matrix(b).T.astype(np.double)
        # print "b: ", bfinal 
        h = cvxopt.matrix(bfinal)
        # print "h: ", h
        A = cvxopt.matrix(Aeq)
        b = cvxopt.matrix(Beq)
        sol = cvxopt.solvers.qp(Q, p, G, h, A, b)
        # sol = cvxopt.solvers.qp(Q, p) #failes with message: ValueError: Rank(A) < p or Rank([P; A; G]) < n
        self.soln_coeff = np.matrix(sol['x'])
        print "solution: ", self.soln_coeff, "its type: ", type(self.soln_coeff)
    def Plot_poly_cvxopt(self):
        print "print curves"

        set_points = np.vstack([self.x0, self.x1, self.x2, self.xt1, self.xt2])

        traj_points_list = np.matrix(self.T_hat)*self.soln_coeff
        traj_points = np.reshape(traj_points_list,[len(traj_points_list)/3,3])

        # print "shape of set points: ", np.shape(set_points)
        print "shape of traj points: ", np.shape(traj_points), "type of pts: ", type(traj_points)
        # print "shape of traj list: ", np.shape(traj_points_list)

        self.same_fig = plt.figure(); self.ax = self.same_fig.add_subplot(111, projection='3d')
        c = 'r'; m = '*'

        xs = set_points[:,0]; ys = set_points[:,1]; zs = set_points[:,2]

        xs_set = traj_points[:,0]; ys_set = traj_points[:,1]; zs_set = traj_points[:,2]
        self.ax.scatter(xs.tolist(),ys.tolist(),zs.tolist(),c=c,marker=m)

        c = 'b';m = '^'; self.ax.scatter(xs_set.tolist(),ys_set.tolist(),zs_set.tolist(),c=c,marker=m)
        self.ax.set_xlabel('X axis'); self.ax.set_ylabel('Y axis'); self.ax.set_zlabel('Z axis');
        # plt.show()


    #Specific to this example
    def Get_plane_constraints(self):
        self.P_tilde = []  #just make block diag of planes (check size to make sure its the size of P (coefficient))
        self.b_tilde = []
        print "shape of T_hat: ",np.round(np.shape(self.T_hat)[0]/3), "\n int time steps: ", self.int_time_steps
        for idx in range(np.round(np.shape(self.T_hat)[0]/3)):
            if idx <= self.int_time_steps:
                # b_vect = self.planes[:,3]
                b_vect = self.third_planes[:,3]
                self.b_tilde.append(b_vect)
                if len(self.P_tilde) == 0:
                    # self.P_tilde = self.planes[:,:3]
                    self.P_tilde = self.third_planes[:,:3]
                else:
                    # self.P_tilde = scipy.sparse.block_diag([self.P_tilde,self.planes[:,:3]]).toarray()
                    self.P_tilde = scipy.sparse.block_diag([self.P_tilde,self.third_planes[:,:3]]).toarray()
            else:
                # b_vect = self.second_planes[:,3]
                b_vect = self.third_planes[:,3]
                self.b_tilde.append(b_vect)
                # self.P_tilde = scipy.sparse.block_diag([self.P_tilde,self.second_planes[:,:3]]).toarray()
                self.P_tilde = scipy.sparse.block_diag([self.P_tilde,self.third_planes[:,:3]]).toarray()

        self.b_tilde = np.hstack(self.b_tilde)
        self.P_tilde = np.vstack(self.P_tilde)
        # print " b_tilde: ", self.b_tilde, "its length: ", len(self.b_tilde), "length of P_tilde:", np.shape(np.matrix(self.P_tilde))
    def Boundary_conditions(self):
        t0 = self.Ti_discrete[0]; t1 = self.Ti_discrete[1]; t2 = self.Ti_discrete[2]; t_mid1 = self.t_mid1; t_mid2 = self.t_mid2

        pos_t0 = np.array(self.poly_order_fnct(t0,self.poly_order,"pos"))
        pos_tmid1 = self.poly_order_fnct(t_mid1,self.poly_order,"pos")
        pos_t1 = self.poly_order_fnct(t1,self.poly_order,"pos")
        pos_tmid2 = self.poly_order_fnct(t_mid2,self.poly_order,"pos")
        pos_t2 = self.poly_order_fnct(t2,self.poly_order,"pos")

        vel_t0 = self.poly_order_fnct(t0,self.poly_order,"vel")
        vel_t1 = self.poly_order_fnct(t1,self.poly_order,"vel")
        vel_t2 = self.poly_order_fnct(t2,self.poly_order,"vel")

        acc_t1 = self.poly_order_fnct(t1,self.poly_order,"acc")


        zero_mat = np.zeros(np.array(pos_t0.shape))
        ones_mat = np.array(self.poly_order_fnct(1,self.poly_order,"pos"))
        # print "pos_t0",pos_t0,"its shape: ",np.shape(pos_t0)[:],"\n zeros: ",zero_mat, "\n pos_tmid1: ",pos_tmid1
        # self.whole_traj_bc = np.array(np.vstack([np.hstack([pos_t0, zero_mat]),
        #                 np.hstack([pos_t1, -pos_t1]),
        #                 np.hstack([zero_mat, pos_t2]),
        #                 np.hstack([pos_tmid1, zero_mat]),
        #                 np.hstack([zero_mat, pos_tmid2])]))


        # self.whole_traj_bc_bvect = np.array(np.vstack([self.x0.T,
        #                                 np.zeros([3,1]),
        #                                 self.x2.T,
        #                                 self.xt1.T,
        #                                 self.xt2.T]))



        self.whole_traj_bc = np.array(np.vstack([np.hstack([pos_t0, zero_mat]),
                        np.hstack([pos_t1, -pos_t1]),
                        np.hstack([zero_mat, pos_t2])]))


        self.whole_traj_bc_bvect = np.array(np.vstack([self.x0.T,
                                        np.zeros([3,1]),
                                        self.x2.T]))




        self.diff_traj_bc = np.array(np.vstack( [np.hstack([vel_t1, -vel_t1]), np.hstack([acc_t1, -acc_t1]) ] ))
        self.diff_traj_bc_bvect = np.array(np.zeros([6,1]))


    #Phrase problem
    def Solve_with_cvx_opt(self):
        time_start = time.time()
        total_time_start = time.time()

        """TIME CHECK """
        self.time_check("Step 1 ",time_start); time_start = time.time()

        #"get time matricies"
        self.Get_Time_matricies()

        '''Plane terms for points '''
        self.Get_plane_constraints()


        self.time_check("Step 2 ",time_start);time_start = time.time()


        T_hat_mat = np.array(np.double(self.T_hat))
        T_hat_diff = np.array(np.double(self.T_hat_diff))
        T_hat_diff_left = scipy.sparse.csr_matrix(self.T_hat_diff.transpose())
        T_hat_diff_right =scipy.sparse.csr_matrix(self.T_hat_diff)

        T_hat_2diff_left = scipy.sparse.csr_matrix(self.T_hat_2diff.transpose())
        T_hat_2diff_right =scipy.sparse.csr_matrix(self.T_hat_2diff)

        T_hat_3diff_left = scipy.sparse.csr_matrix(self.T_hat_3diff.transpose())
        T_hat_3diff_right =scipy.sparse.csr_matrix(self.T_hat_3diff)

        print "size of T hat diff: ", np.shape(self.T_hat_diff), "its type: ", type(self.T_hat_diff)
        # H = T_hat_diff_left*T_hat_diff_right
        H = T_hat_3diff_left*T_hat_3diff_right

        self.H = H


        f = np.zeros([np.shape(H)[0],1])
        P_tilde_left = scipy.sparse.csr_matrix(self.P_tilde)
        A = np.matrix(P_tilde_left*T_hat_mat)
        b = -self.b_tilde
        #now set equalities: 
        self.Boundary_conditions()
        Aeq = np.vstack([self.whole_traj_bc,self.diff_traj_bc])
        Beq = np.vstack([self.whole_traj_bc_bvect,self.diff_traj_bc_bvect])

        self.Aeq = Aeq
        self.Beq = Beq

        time_start_qp = time.time()
        self.QP_Solve(H,f,A,b,Aeq,Beq)
        self.time_check("QP time: ", time_start_qp)

        # print "\n H shape: ", np.shape(H), "\n A shape: ", np.shape(A), "\n b: ", b
        #add other terms
        self.time_check("Step 3",time_start)


        self.time_check("Total time: ",total_time_start)

        # self.Plot_poly_cvxopt()




    def Plot_gurobi_poly(self,poly_order,space_dim):
        self.same_fig = plt.figure(); self.ax = self.same_fig.add_subplot(111, projection='3d')
        self.ax.set_xlabel('X axis'); self.ax.set_ylabel('Y axis'); self.ax.set_zlabel('Z axis');


        self.gurobi_soln
        real_time = np.linspace(self.Ti_discrete[0],self.Ti_discrete[-1],100)
        path_pts = []
        for time_idx in range(real_time.shape[0]):
            time = real_time[time_idx]; #print "\n time: ", time
            coeff_idx = 0
            for idx in range(len(self.Ti_discrete)):
                # print "current TI dis: ", self.Ti_discrete[idx+1]
                if time > self.Ti_discrete[idx+1]:
                    coeff_idx = idx+1
                    # print "should change: ", coeff_idx
                else:
                    break
            Coeff_curr = np.matrix(self.gurobi_soln["coeff"][coeff_idx-1]).T
            # dt = time-self.Ti_discrete[coeff_idx]; print "time", time, "Ti:",self.Ti_discrete[coeff_idx+1] , "dt: ", dt, " coeff: ", coeff_idx
            dt = time - np.sum(self.gurobi_soln["time_mat"][:(coeff_idx+1)]); #print "time",time,"current diff ", np.sum(self.gurobi_soln["time_mat"][:(coeff_idx+1)]), "dt: ", dt, " coeff: ", coeff_idx
            # print "total time mat",self.gurobi_soln["time_mat"]
            Time_mat = self.obtain_time_matrix_gurobi(dt,poly_order,0,space_dim)
            curr_pt = Time_mat*Coeff_curr
            path_pts.append(curr_pt)
        path_pts = np.matrix(np.hstack(path_pts)).T
        c = 'r'; m = '*'
        xs_set = path_pts[:,0]; ys_set = path_pts[:,1]; zs_set = path_pts[:,2]
        # print "path points: ", path_pts
        c = 'r';m = 'o'; self.ax.scatter(xs_set.tolist(),ys_set.tolist(),zs_set.tolist(),c=c,marker=m)
        # self.ax.set_xlabel('X axis'); self.ax.set_ylabel('Y axis'); self.ax.set_zlabel('Z axis');
        plt.show()



    def Solve_with_gurobi(self):
        print "solving with gurobi"
        #GIVENS: 
        num_chords = 2
        poly_order = self.poly_order
        Ti_discrete = self.Ti_discrete  
        chord_sub_division = 100
        spacial_dimension = 3

        inequality_constraints = [self.planes,self.second_planes]  
        start_pose = self.x0
        goal_pose = self.x2


        T_hat = np.matrix(self.T_hat)
        alpha_vector,alpha_mat,Ti_0_to_t = self.Gurobi_solver(spacial_dimension,num_chords,poly_order,Ti_discrete,chord_sub_division,inequality_constraints,start_pose, goal_pose)
        self.gurobi_soln = {"coeff":alpha_mat,"time_mat":Ti_0_to_t}#coefficients
        # self.gurobi_path_pts = T_hat * np.matrix(coefficients).T
        self.Plot_gurobi_poly(poly_order,spacial_dimension)


    def obtain_time_matrix_gurobi(self,time, poly_order, diff_order, space_dim):
        if diff_order == 0:
            tvect = self.poly_qp_obj.poly_order(poly_order, "Tvect","pos",time).T
            tvect = tvect.tolist()[0]
            tvect= np.double(tvect)
            Tmat = self.Tmat_ndim(tvect,space_dim)

        elif diff_order == 1:
            tvect = self.poly_qp_obj.poly_order(poly_order, "Tvect","vel",time).T
            tvect = tvect.tolist()[0]
            tvect= np.double(tvect)
            Tmat = self.Tmat_ndim(tvect,space_dim)

        elif diff_order == 2:
            tvect = self.poly_qp_obj.poly_order(poly_order, "Tvect","acc",time).T
            tvect = tvect.tolist()[0]
            tvect= np.double(tvect)
            Tmat = self.Tmat_ndim(tvect,space_dim)

        return Tmat

    def Tmat_ndim(self,tvect,space_dim):
        Tmat = []
        for idx in range(int(space_dim)):
            Tmat.append(tvect)
        Tmat = scipy.sparse.block_diag(Tmat)
        Tmat = Tmat.todense()
        Tmat = np.matrix(Tmat)
        return Tmat


    def Gurobi_solver(self,spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose):
        #'''expects start, goal to be matricies np '''
        print "\nGurobi Trajecotry Generation...\n"
        """Get time in correct format"""
        Ti_0_to_t = [0]
        for idx in range(len(Ti_discrete)-1):
            dt = Ti_discrete[idx+1] - Ti_discrete[idx]
            Ti_0_to_t.append(dt)
        """Create a new model"""
        m = Model("qp")
        #Define poly object for getting the objective function
        self.poly_qp_obj = poly_qp.QP_poly_cls()
        #Usage: # ti= 0.3; tf = 0.6; # T = cls.poly_order(3,"Tmat","vel",ti,tf)  #produces the integrated:   int_ti^tf Q for alpha' Q alpha; # print "Tmat for vel, 3rd order poly, sub ti= 0.3, tf = 0.6:" ,T
        """Dynamically store variables (coefficients)"""
        alpha_list = []
        for idx in range(int(num_chords*((poly_order+1)*3))):
            alpha_list.append(m.addVar(lb=-GRB.INFINITY))
        alpha_array = np.array(alpha_list)
        alpha_mat = alpha_array.reshape(num_chords,np.max(alpha_array.shape)/(num_chords))
        # Integrate new variables
        alpha_vect = np.matrix(alpha_array).T
        m.update()
        """Set Objectives"""
        obj_list = []
        for idx in range(len(Ti_0_to_t)-1):
            alpha_vect = np.matrix(alpha_mat[idx]).T 
            ti= 0.; tf = Ti_0_to_t[idx+1]
            tmat = np.double(np.matrix(self.poly_qp_obj.poly_order(poly_order,"Tmat","vel",ti,tf)))
            # Tmat = np.matrix(scipy.linalg.block_diag(tmat,tmat,tmat))   #THIS CAN BE GENERALIZED TO N DIMENSIONS LATER!!!!!
            Tmat = self.Tmat_ndim(tmat,spacial_dimension)
            obj_elem = alpha_vect.T*Tmat*alpha_vect
            obj_elem  = obj_elem.tolist()[0][0]
            obj_list.append(obj_elem)
        obj = np.sum(obj_list)
        m.setObjective(obj) #Set the objective function 
        m.update()
        """Set Equalities"""
        t = Ti_0_to_t[0]#set start pose equalities
        Tmat_pos = self.obtain_time_matrix_gurobi(t, poly_order, 0,spacial_dimension)
        Tmat_vel = self.obtain_time_matrix_gurobi(t, poly_order, 1,spacial_dimension)
        coeff_pose = np.matrix(Tmat_pos)*np.matrix(alpha_mat[0]).T
        coeff_vel = np.matrix(Tmat_vel)*np.matrix(alpha_mat[0]).T
        for index in range(coeff_pose.shape[0]):
            m.addConstr(coeff_pose.item(index), GRB.EQUAL, start_pose.item(index))
            m.update()
        for index in range(coeff_vel.shape[0]):
            m.addConstr(coeff_vel.item(index), GRB.EQUAL, 0.0)
            m.update()
        t = Ti_0_to_t[-1]#set goal pose equalities
        Tmat_pos = self.obtain_time_matrix_gurobi(t, poly_order, 0,spacial_dimension)
        Tmat_vel = self.obtain_time_matrix_gurobi(t, poly_order, 1,spacial_dimension)
        coeff_pose = np.matrix(Tmat_pos)*np.matrix(alpha_mat[-1]).T
        coeff_vel = np.matrix(Tmat_vel)*np.matrix(alpha_mat[-1]).T
        for index in range(coeff_pose.shape[0]):
            m.addConstr(coeff_pose.item(index), GRB.EQUAL, goal_pose.item(index))
            m.update()
        for index in range(coeff_vel.shape[0]):
            m.addConstr(coeff_vel.item(index), GRB.EQUAL, 0.0)
            m.update()


        """Set Inequality Constraints"""
        for idx in range(len(inequality_constraints)):
            A_curr = inequality_constraints[idx] #list of inequality constraints for the idx'th chord
            coeff_curr = alpha_mat[idx].T #get the current coefficients for this chord
            ti = Ti_0_to_t[idx+1]
            time_sub_divide =np.linspace(0,ti,chord_sub_division) 
            for time in range(1,int(len(Ti_0_to_t)-1)):
                Tmat_pos = self.obtain_time_matrix_gurobi(time,poly_order,0,spacial_dimension) #order, space_dim
                point = Tmat_pos*np.matrix(coeff_curr).T

                for kdx in range(len(A_curr)):
                    ineq_var = np.matrix(A_curr[kdx][:point.shape[0]])*point
                    ineq_var = ineq_var.tolist()[0][0]
                    ineq_b_val = A_curr[kdx][-1]
                    m.addConstr(ineq_var,GRB.LESS_EQUAL,-ineq_b_val)
                    m.update()

        """Optimize"""
        m.optimize()
        """Prep debud"""
        try:
            #to debug and see which rows are infeasible
            m.computeIIS()
            m.write('gurobi_problem_crash.ilp')
        except:
            pass

        """Send solution as a vector"""
        alpha_vector = []
        for v in m.getVars():
            print('%s %g' % (v.varName, v.x))
            alpha_vector.append(v.x)
        alpha_vector = np.array(alpha_vector)
        alpha_mat = alpha_vector.reshape(num_chords,np.max(alpha_vector.shape)/(num_chords))
        #???Generate Path points (possibly use old matrix as it is the same)??
        return alpha_vector,alpha_mat,Ti_0_to_t





def main():
    opt_obj = Optimization_test()
    opt_obj.Solve_with_cvx_opt()

    self = opt_obj

    opt_obj.Solve_with_gurobi()

if __name__ == '__main__':
    main()



