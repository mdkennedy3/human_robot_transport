from ompl import base as ob
from ompl import geometric as og
from ompl import control, tools, util
import math
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


class MySE2(ob.SE2StateSpace):
    def distance(self,state1, state2):
        #print state1.getX(), state2.getX()
        distxy = np.sqrt((state1.getX()-state2.getX())**2 + (state1.getY()-state2.getY())**2)
        ang1 = state1.getYaw()
        ang2 = state2.getYaw()
        R1 = np.matrix([[math.cos(ang1), -math.sin(ang1)],[math.sin(ang1), math.cos(ang1)]])
        R2 = np.matrix([[math.cos(ang2), -math.sin(ang2)],[math.sin(ang2), math.cos(ang2)]])
        Rdiff = R2*R1.T
        ang_diff = math.atan2(Rdiff[1,0],Rdiff[0,0])
        final_dist = distxy + ang_diff 
        return final_dist


def isStateValid(state):
    # "state" is of type SE2StateInternal, so we don't need to use the "()"
    # operator.
    #
    # Some arbitrary condition on the state (note that thanks to
    # dynamic type checking we can just call getX() and do not need
    # to convert state to an SE2State.)
    # print "state:", state.getX(),state.getY(),state.getYaw()
    curr_state_xy = np.matrix([state.getX(),state.getY()])
    curr_state_yaw = np.matrix([state.getYaw()])
    #See if it is within tolerance distance to occupied cells, if it is not, then it is valid, otherwise it is not
    #Check if norm of state tested

    return True


def convert_path_to_list(path):
    x = []; y = []; yaw = []
    path_list_first = {'x':[],'y':[],'yaw':[]}
    for idx in range(len(path.getStates())):
        x.append(path.getStates()[idx].getX())
        y.append(path.getStates()[idx].getY())
        yaw.append(path.getStates()[idx].getYaw())
    # path_list_first = np.vstack(x,y,yaw)
    path_list_first['x'] = x
    path_list_first['y'] = y
    path_list_first['yaw'] = yaw
    return path_list_first


def plan():
    # """RRT: """ # #control.RRT # #og.RRTConnect
    bounds = ob.RealVectorBounds(2) 
    bounds.setLow(0,-10)
    bounds.setLow(1,-10)

    bounds.setHigh(0,10)
    bounds.setHigh(1,10)

    #space = ob.SE2StateSpace()
    space = MySE2()
    space.setBounds(bounds)

    start = ob.State(space)
    # we can pick a random start state...
    # start.random()
    # ... or set specific values
    start().setX(0)
    start().setY(0)
    start().setYaw(0)

    goal = ob.State(space)
    # we can pick a random goal state...
    goal().setX(5)
    goal().setY(5)
    goal().setYaw(math.pi/2.)

    si = ob.SpaceInformation(space)
    si.setStateValidityChecker(ob.StateValidityCheckerFn(isStateValid))

    pd = ob.ProblemDefinition(si)

    pd.setStartAndGoalStates(start,goal)

    planner = og.RRTConnect(si)
    #planner = og.RRTstar(si)

    planner.setProblemDefinition(pd)
    planner.setup()

    solved = planner.solve(10.0)

    #interesting: 
    # ps = og.PathSimplifier(si)
    # ps.smoothBSpline(path) #makes bspline path out of path variable



    if solved:
        # try to shorten the path
        #ss.simplifySolution()
        # print the simplified path
        #print ss.getSolutionPath()
        path = pd.getSolutionPath()

        '''Need to extract states from path '''
        # print path

        path_list_first=convert_path_to_list(path)
        print "first path list: ", path_list_first

        #interesting: 
        ps = og.PathSimplifier(si)
        ps.smoothBSpline(path) #makes bspline path out of path variable
        # print path  #now is a bspline
        path_list_second=convert_path_to_list(path)
        print "second path list: ", path_list_second


        #now plot the path
        fig = plt.figure(); ax = fig.add_subplot(121, projection='3d'); ax2 = fig.add_subplot(122, projection='3d')
        c = 'r'; m = 'o'; xs = path_list_first['x']; ys = path_list_first['y']; zs = path_list_first['yaw']; ax.scatter(xs, ys, zs, c=c, marker=m)
        c = 'r'; m = 'o'; xs = path_list_second['x']; ys = path_list_second['y']; zs = path_list_second['yaw']; ax2.scatter(xs, ys, zs, c=c, marker=m)
        ax.set_xlabel('X axis'); ax.set_ylabel('Y axis'); ax.set_zlabel('theta axis'); ax2.set_xlabel('X axis'); ax2.set_ylabel('Y axis'); ax2.set_zlabel('theta axis'); 
        
        plt.show()



if __name__ == "__main__":
    plan()
