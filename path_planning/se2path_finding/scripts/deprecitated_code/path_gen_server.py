#!/usr/bin/env python2
import rospy
import numpy as np
import numpy.matlib
import time
import tf
import math
import pcl 
#initial path planning
from ompl import base as ob
from ompl import geometric as og
from ompl import control, tools, util

#For plotting
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from se2path_finding.ts_math_operations import ts_math_oper
from se2path_finding.ompl_support import seed_path_ompl
from se2path_finding.path_plotting_lib import  plot_path_plan_lib
# from se2path_finding.ts_math_operations import plot_path_plan_lib #figure this out to clean up code

from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
# from geometry_msgs.msg import Point
from geometry_msgs.msg import Point32 as geomPoint
from geometry_msgs.msg import Point, Quaternion, PoseStamped
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import PointCloud

from se2path_finding.msg import ListMsg

#For optimization: 
import scipy
from scipy import linalg 
from scipy import sparse
import cvxopt

#for service
from se2path_finding.srv import *

from se2path_finding.qp_solving_support import cvx_gurobi_solver, poly_qp
from se2path_finding.convex_corridor import cvx_corridor_alg


class world_info(object):
    def __init__(self,xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps):
        self.extents = {'xmin':xmin,'xmax':xmax,'ymin':ymin,'ymax':ymax,"th_min":theta_min,"th_max":theta_max}
        self.steps = {'x_steps':x_steps,'y_steps':y_steps, 'theta_steps':theta_steps}
        self.world_discretized = []
        self.occupied_cell_bool = []  #0 is free, 1 is occupied
        self.x_coord = []
        self.y_coord = []
        self.th_coord = []


class Path_finder_class(object):
    def __init__(self,path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub):
        self.Plotting_debugging_rviz = plot_path_plan_lib.Plotting_debugging_rviz(path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub)
        self.world = []
        self.start_pose = []
        self.goal_pose = []
        self.obstacle_list = []

    def discretize_world(self,xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps):
        self.world = world_info(xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps)  #[xmin,xmax;ymin,ymax]  (angle is 0:2pi)
        #discretize the world, given the number of steps in x,y, theta
        th_num,th_rem = divmod(abs(theta_max-theta_min),theta_steps)
        th_coord = np.linspace(theta_min,theta_max,th_num)
        self.world.th_coord = th_coord

    def get_object_pose(self,object_start_pose,object_goal_pose):
        # Get the object pose, either from a msg or manually
        class transported_object(object):
            def __init__(self,object_start_pose,object_goal_pose, graph_scale):
                self.start_pose = object_start_pose
                self.pose = object_start_pose
                # self.vert = np.matrix([[2,0],[-1,1],[-1,-1]]) #verticies of convex object when orientation is 0deg
                # self.vert = np.matrix([[0.3048,0.2032],[0.3048,-0.2032],[-0.3048,-0.2032],[-0.3048,0.2032]]) #verticies of convex object when orientation is 0deg, youbot is 2ft in length, 16in in width, units here meters
                # add_pts1 = np.matrix([[0.3048,0.], [0.,-0.2032],[0.,0.2032], [-0.3048,0.]])
                # self.vert = np.vstack([self.vert, add_pts1])
                '''JUST DO ALL CALCULATIONS IN CM!!!!!!!!!!!!!!!!'''
                # scale = 100.
                scale = graph_scale
                # x1,x2,y1,y2 = -0.3048, 0.3048, -0.2032,0.2032
                x1,x2,y1,y2 = -0.3048*scale, 0.3048*scale, -0.2032*scale,0.2032*scale #cm or m
                x_coord = np.linspace(np.min([x1,x2]),np.max([x1,x2]),10)
                y_coord = np.linspace(np.min([y1,y2]),np.max([y1,y2]),10)
                obj_side_1 = []; obj_side_2 = []; obj_side_3 = []; obj_side_4 = []
                for jdx in range(len(y_coord)): point = [x1, y_coord[jdx]]; obj_side_1.append(point)
                for jdx in range(len(y_coord)): point = [x2, y_coord[jdx]]; obj_side_2.append(point)
                for jdx in range(len(x_coord)): point = [x_coord[jdx],y1]; obj_side_3.append(point)
                for jdx in range(len(x_coord)): point = [x_coord[jdx],y2]; obj_side_4.append(point)

                self.vert = np.matrix(np.vstack([obj_side_1,obj_side_2,obj_side_3,obj_side_4]))
                self.goal_pose = object_goal_pose
        self.transported_object= transported_object(object_start_pose,object_goal_pose,self.graph_scale)

    #NEEDED:
    def minkowski_object_points(self):
        #this function takes in sensed data and adds minkowski sum to points for given orientations of the object
        object_pts = np.array(self.obstacle_list.tolist())
        # print "obj pts: ", object_pts
        # test= self.virtual_obstacles['virt_obs1'].points
        minkowski_pts = []
        for idx in range(len(self.world.th_coord)):
            theta = self.world.th_coord[idx]
            R = np.matrix([[math.cos(theta), -math.sin(theta)],[math.sin(theta), math.cos(theta)]])
            vects_rotated = R*self.transported_object.vert.T
            for jdx in range(vects_rotated.shape[1]):
                vect_curr = vects_rotated[:,jdx]
                vects_rotated_mat = np.matlib.repmat(vect_curr.T,len(object_pts),1)
                theta_vect = np.matrix(np.matlib.repmat(theta,len(object_pts),1))
                mink_xy  = object_pts + vects_rotated_mat
                mink_pts = np.concatenate((mink_xy,theta_vect),axis=1)
                minkowski_pts.append(mink_pts)
        #long Nx2 matrix of points that are on top of sensed points
        # print "\n mink points: ", minkowski_pts
        self.minkowski_pts = np.vstack(minkowski_pts)
        #AT THIS POINT I SHOULD PLOT TO VISUALIZE OBSTACLE POINT, WORLD AND minkowski_pts

        return self.minkowski_pts
    #NEEDED:
    def get_occupied_points(self):
        ''' Given world cells, obstacle locations, define a list for 
        world points that assigns that point as either free or occupied, 
        based on whether an obstacle point is within cell distance of that cell '''
        #for artificial data collect points given from known map for real data, query the sensor to fill the map with percentage occupied. Make list of 'active' obstacle points
        active_obstacle_pts = self.minkowski_pts.tolist()  #With real sensor, this list of mink points is much smaller and is used to fill occ/unocc pts

        #Filter the points: 
        pts = np.float32(np.array(active_obstacle_pts))
        world_step_factor = np.float32(0.9)
        leaf = np.array([np.float32(world_step_factor*self.world.steps['x_steps']),
                         np.float32(world_step_factor*self.world.steps['y_steps']),
                         np.float32(world_step_factor*self.world.steps['theta_steps'])],dtype=np.float32)
        self.pc_init = pcl.PointCloud()
        self.pc_init.from_array(pts)
        vox_filt = self.pc_init.make_voxel_grid_filter()
        vox_filt.set_leaf_size(leaf[0],leaf[1],leaf[2])
        filtered_pts = vox_filt.filter()
        active_obstacle_pts = np.matrix(filtered_pts.to_array()).tolist()
        self.filtered_mink_pts = np.matrix(active_obstacle_pts)

        self.pc = pcl.PointCloud()
        self.pc.from_array(np.array(np.float32(active_obstacle_pts)))

        '''Until you can figure out '''
        # self.cloud = self.pc.make_octree(min(leaf))
        self.cloud = self.pc.make_octree(max(leaf))
        # self.cloud = filtered_pts.make_octree(max(leaf)) #destroys original filtered_pts and other objects
        self.cloud.add_points_from_input_cloud()

    def obtain_initial_viable_path(self):
        # print('in this function obtain a path from method such as A*,RRT or manual, that QP expands on')
        #Path is defined as a list of verticies that contain the start and goal position at the top and bottom of the list respectively
        #RRT connect** (design with toy variables to make sure it works)
        self.ompl_support1 = seed_path_ompl.ompl_support() #supporting functions 
        self.ompl_support2 = seed_path_ompl.ompl_support() #supporting functions 
        self.ompl_support3 = seed_path_ompl.ompl_support() #supporting functions 
        th_init = self.transported_object.goal_pose.item(2) #the inital theta (between -pi, pi)
        th_lower = th_init - 2*math.pi
        th_higher = th_init + 2*math.pi
        goal_init = self.transported_object.goal_pose
        goal_lower = np.matrix([self.transported_object.goal_pose.item(0),self.transported_object.goal_pose.item(1),th_lower])
        goal_higher = np.matrix([self.transported_object.goal_pose.item(0),self.transported_object.goal_pose.item(1),th_higher])
        time_to_solve = 10.0
        path_low = self.ompl_support1.setup_ompl_problem_world_space(self.world,self.cloud, self.transported_object.start_pose, goal_lower, time_to_solve)
        path_init = self.ompl_support2.setup_ompl_problem_world_space(self.world,self.cloud, self.transported_object.start_pose, goal_init, time_to_solve)
        path_high = self.ompl_support3.setup_ompl_problem_world_space(self.world,self.cloud, self.transported_object.start_pose, goal_higher, time_to_solve)
        #convert to list:
        print "low path: ", path_low
        path_low = np.matrix(np.vstack([path_low['x'],path_low['y'],path_low['yaw']])).T
        path_init = np.matrix(np.vstack([path_init['x'],path_init['y'],path_init['yaw']])).T
        path_high = np.matrix(np.vstack([path_high['x'],path_high['y'],path_high['yaw']])).T
        # self.path_dict = self.ompl_support.setup_ompl_problem_world_space(self.world,self.cloud, self.transported_object.start_pose, self.transported_object.goal_pose, time_to_solve)

        #possibly make dict of three paths
        self.paths_to_all_possible_goals = {'inital_path':path_init, "lower_path":path_low, "higher_path":path_high} #inital path is for goal between -pi:pi, lower is -3pi:-pi, higher is pi:3pi
        try: 
            self.initial_path = path_init
        except:
            print "initial path didnt work"
            pass

    def convex_region_finder(self,chord_subdivision):
        # print "this segment finds the convex region for the path"
        #Iterate through the initial path, segmenting path and finding convex regions for each segment 
        self.cvx_plane_constraints_dict = dict()
        for path_key in self.paths_to_all_possible_goals.keys():
            initial_path = self.paths_to_all_possible_goals[path_key]
            cvx_plane_constraints = []
            #1. iterate through each segment
            for idx_path in range(len(initial_path)-1):
                path_pt1 = np.matrix(initial_path[idx_path])
                path_pt2 = np.matrix(initial_path[idx_path+1])
                #Get vector direction between the two segment points: 
                chord_vect = path_pt2 - path_pt1 #vector from path point 1 to 2
                chord_length = np.linalg.norm(chord_vect) #norm of vec
                chord_vect_normed = np.divide(chord_vect,chord_length) #normed vec
                #2. generate pts along chord to find convex planes
                xpos_chord = np.matrix(np.linspace(path_pt1.item(0),path_pt2.item(0),chord_subdivision))
                ypos_chord = np.matrix(np.linspace(path_pt1.item(1),path_pt2.item(1),chord_subdivision))
                zpos_chord = np.matrix(np.linspace(path_pt1.item(2),path_pt2.item(2),chord_subdivision))
                chord_pts = np.vstack([xpos_chord,ypos_chord,zpos_chord]).T
                #3. first rank the world points in order of closeness to the center of the 
                chord_centroid = path_pt1 + 0.5*chord_length*chord_vect_normed #=p1 + v12*0.5 where v12 = p2 - p1
                occupied_voxels = np.matrix(self.cloud.get_occupied_voxel_centers()) #Use occupied world voxels:::self.filtered_mink_pts  #Nx3
                repeated_chord_centroid = np.matlib.repmat(chord_centroid,np.shape(occupied_voxels)[0],1)
                diff = occupied_voxels - repeated_chord_centroid
                norm_diff_btw_mink_and_chord_cent = np.sqrt(np.sum(np.square(diff),1))
                sorted_occ_vox_pts = np.argsort(norm_diff_btw_mink_and_chord_cent.T) #needs it to be a row to sort
                sorted_occ_vox_pts_list = sorted_occ_vox_pts.tolist()[0] #convert to list Nx1
                occupied_voxels_list = occupied_voxels.tolist() #convert to list
                norm_diff_btw_mink_and_chord_cent_list = norm_diff_btw_mink_and_chord_cent.tolist()
                world_bounding_box = self.get_world_boundaries()
                plane_dict={'Plane_pts':[], 'Plane_equations':[],'Plane_signs':[]}
                plane_dict['Plane_pts'] = np.array(np.vstack([path_pt1,path_pt2])) #store the chord points used to get the corresponding planes
                #4. Now, iterating through the world points, find the points on the chord closest and create hyperplanes which remove other obstacle points
                scale = 5.2; color = [1,0,0]; self.Plotting_debugging_rviz.Plot_scatter_list(plane_dict['Plane_pts'].tolist(),scale,color); self.Plotting_debugging_rviz.Plot_obstacles(occupied_voxels_list) #pass point cloud of obstacle
                vox_init_size = len(sorted_occ_vox_pts_list)
                while len(sorted_occ_vox_pts_list) > 0 and not rospy.is_shutdown():
                    #continue until all mink points are removed
                    min_idx = sorted_occ_vox_pts_list.pop(0) #remove that value from the ranked list
                    world_vox = occupied_voxels_list.pop(min_idx)
                    corresponding_norm_from_centroid = norm_diff_btw_mink_and_chord_cent_list.pop(min_idx) #keep this list for book keeping required to get new sorted at end of each while
                    #5. Using selected point find point on chord that is closest to it
                    #NEGATIVE BELOW SO ARROWS POINT OUT
                    diff_chord_pts_and_world_vox = -(chord_pts - np.matlib.repmat(world_vox,np.shape(chord_pts)[0],1)) #get the differences, these vectors go from teh world voxel to the chord points
                    norms_chord_pts_and_world_vox = np.sqrt(np.sum(np.square(diff_chord_pts_and_world_vox),1)) #get the norms of the above
                    sorted_norms_chord_pts_and_world_vox = np.array(np.argsort(norms_chord_pts_and_world_vox.T)) #get the order of norms to find the closest point
                    closest_chord_point = chord_pts[sorted_norms_chord_pts_and_world_vox[0][0]] #get closest point on chord to the world voxel of interest
                    vector_world_vox_to_chord_pt = diff_chord_pts_and_world_vox[sorted_norms_chord_pts_and_world_vox[0][0]] #retain the vector from the world voxel to this closest chord point
                    #6. Define the plane with normal vector pointing toward the chord, find the d value, and add this to plane list
                    d =  -np.matrix(vector_world_vox_to_chord_pt)*np.matrix(world_vox).T #this is the value of d, the sign of the chord direction must still be checked
                    plane_vector = np.hstack([ vector_world_vox_to_chord_pt ,d])
                    #7. Use this to find sign of all point on chord 
                    ''' (ensure they all have the same sign, else throw msg) '''
                    viable_sign = np.sign(plane_vector* np.vstack([closest_chord_point.T,np.matrix([1.])]))
                    plane_dict['Plane_equations'].append(plane_vector.tolist()[0])
                    plane_dict['Plane_signs'].append(viable_sign.tolist()[0])
                    occupied_voxels_list_mat = np.matrix(occupied_voxels_list)
                    #8. Use this to then exclude all world points that don't share the same sign (pop these indices); [repeating until list of world points is empty]
                    try: 
                        occupied_voxels_list_mat_plane_signs = np.sign(np.hstack([occupied_voxels_list_mat,np.matrix(np.ones([np.shape(occupied_voxels_list_mat)[0],1]))])*plane_vector.T)
                    except:
                        self.Plotting_debugging_rviz.Plot_vectors(world_vox,closest_chord_point.tolist()[0])
                        self.Plotting_debugging_rviz.Plot_planes(plane_vector,np.matrix(world_vox))
                        '''BETTER THING TO DO IS CHECK IF WORLD_VOX IS LAST ELEMENT OF OCCUPIED CELLS AND IF IT IS, THEN JUST STOP AND BREAK HERE '''
                        break
                    occupied_voxels_ind_valid_from_plane = np.where(occupied_voxels_list_mat_plane_signs == viable_sign.tolist()[0][0])[0]
                    if  occupied_voxels_ind_valid_from_plane.size == 0:
                        break
                    if len(occupied_voxels_ind_valid_from_plane.tolist()[0]) != 0:
                        occupied_voxels_list_temp = np.matrix(np.array(occupied_voxels_list)[occupied_voxels_ind_valid_from_plane])
                        norm_diff_btw_mink_and_chord_cent_list_temp = np.matrix(np.array(norm_diff_btw_mink_and_chord_cent_list)[occupied_voxels_ind_valid_from_plane])
                        occupied_voxels_list = occupied_voxels_list_temp.tolist()
                        sorted_occ_vox_pts_list_temp = np.argsort(norm_diff_btw_mink_and_chord_cent_list_temp) #needs it to be a row to sort
                        sorted_occ_vox_pts_list = sorted_occ_vox_pts_list_temp.tolist()[0] #convert to list Nx1
                        norm_diff_btw_mink_and_chord_cent_list = norm_diff_btw_mink_and_chord_cent_list_temp.T.tolist()
                    else:
                        self.Plotting_debugging_rviz.Plot_vectors(world_vox,closest_chord_point.tolist()[0])
                        self.Plotting_debugging_rviz.Plot_planes(plane_vector,np.matrix(world_vox))
                        break
                    #plot the planes: 
                    self.Plotting_debugging_rviz.Plot_vectors(world_vox,closest_chord_point.tolist()[0])
                    self.Plotting_debugging_rviz.Plot_planes(plane_vector,np.matrix(world_vox))
                #9. Now add these plane info to larger list
                plane_dict['Plane_equations'] = np.vstack([plane_dict['Plane_equations'], world_bounding_box]) 
                plane_dict['Plane_signs'] = np.vstack([plane_dict['Plane_signs'], np.matrix(np.sign(world_bounding_box[:,-1])).T.tolist()])
                cvx_plane_constraints.append(plane_dict) #may have to put in list: append([plane_dict])
            self.cvx_plane_constraints_dict[path_key] = cvx_plane_constraints
        return self.cvx_plane_constraints_dict

    #For CVX Solving: 

    def get_world_boundaries(self):


        #this has been vetted, not only are they added for each chord, but the sign has also been triple checked
        frame_sign = -1 

        top = [0,0,1,frame_sign*np.abs(self.world.extents["th_max"])]; #%top positive z, plane point of p1
        bottom = [0,0,-1,frame_sign*np.abs(self.world.extents["th_min"])]; #%bottom, neg z, plane point of p2
        front =[1,0,0,frame_sign*np.abs(self.world.extents["xmax"])]; #%front (positve x),  plane point of p3
        back = [-1,0,0,frame_sign*np.abs(self.world.extents["xmin"])]; #%back (negative x),  plane point of p4
        right  = [0,1,0,frame_sign*np.abs(self.world.extents["ymax"])]; #%right (pos y), plane point of p5
        left  = [0,-1,0,frame_sign*np.abs(self.world.extents["ymin"])]; #%left (neg y), plane point of p6


        world_bounding_box = np.array([top,bottom,front,back,right,left])

        return world_bounding_box

    def poly_order_fnct(self,t,order,poly_type):
        pos_time = []
        vel_time = []
        acc_time = []
        jerk_time = []
        for idx in range(order):
            exp = (order - idx)
            pos_time.append(t**exp)
            vel_time.append(exp*t**(exp-1))
            if exp >= 2:
                acc_time.append(exp*(exp-1)*t**(exp-2))
            else:
                acc_time.append(0.)

            if exp >=3:
                jerk_time.append(exp*(exp-1)*(exp-2)*t**(exp-3))
            else:
                jerk_time.append(0.)

        pos_time.append(1.)
        vel_time.append(0.)
        acc_time.append(0.)
        jerk_time.append(0.)
        pos_time = scipy.sparse.block_diag([pos_time,pos_time,pos_time]).toarray()
        vel_time = scipy.sparse.block_diag([vel_time,vel_time,vel_time]).toarray()
        acc_time = scipy.sparse.block_diag([acc_time,acc_time,acc_time]).toarray()
        jerk_time = scipy.sparse.block_diag([jerk_time,jerk_time,jerk_time]).toarray()

        # print "pos var: ", pos_time,"vel_time", vel_time
        if poly_type in "pos":
            return pos_time
        elif poly_type in "vel":
            return vel_time
        elif poly_type in "acc":
            return acc_time
        elif poly_type in "jerk":
            return jerk_time
        elif poly_type in "all":
            #ie access with v[:,0] for pos 
            return [pos_time, vel_time, acc_time, jerk_time]

    def Get_Time_matricies(self, Ti_discrete, int_time_steps,poly_order):
        # ''' 
        # Ti_discrete: discrete times from 0:1 whose increment is scaled time to be at each chord point
        # int_time_steps: the division of times along each chord to be used for optimization set points (e.g. 100 steps)
        # '''
        self.T_hat_diff = []
        self.T_hat = []
        self.T_hat_3diff = []
        self.T_hat_2diff = []
        # self.int_time_steps = 100 #time points to use along a chord (these points will respect the constraints of cvx)
        for idx in range(len(Ti_discrete)-1):
            Ti_int = np.linspace(Ti_discrete[idx], Ti_discrete[idx+1], int_time_steps) #get time in this region
            self.Time_normed_list.append(Ti_int)
            T_step  = np.vstack(np.array([self.poly_order_fnct(t,poly_order,"pos") for t in Ti_int]))
            T_diff_step  = np.vstack(np.array([self.poly_order_fnct(t,poly_order,"vel") for t in Ti_int]))
            T_2diff_step = np.vstack(np.array([self.poly_order_fnct(t,poly_order,"acc") for t in Ti_int]))
            T_3diff_step = np.vstack(np.array([self.poly_order_fnct(t,poly_order,"jerk") for t in Ti_int]))
            #2nd diff
            if len(self.T_hat_2diff) == 0:
                self.T_hat_2diff = T_2diff_step
            else:
                self.T_hat_2diff = scipy.sparse.block_diag((self.T_hat_2diff,T_2diff_step))
                self.T_hat_2diff = self.T_hat_2diff.toarray()
            #3rd diff
            if len(self.T_hat_3diff) == 0:
                self.T_hat_3diff = T_3diff_step
            else:
                self.T_hat_3diff = scipy.sparse.block_diag((self.T_hat_3diff,T_3diff_step))
                self.T_hat_3diff = self.T_hat_3diff.toarray()
            if len(self.T_hat_diff) == 0:
                self.T_hat_diff = T_diff_step
            else:
                self.T_hat_diff = scipy.sparse.block_diag((self.T_hat_diff,T_diff_step))
                self.T_hat_diff = self.T_hat_diff.toarray()
            if len(self.T_hat) == 0:
                self.T_hat = T_step
            else:
                self.T_hat = scipy.sparse.block_diag((self.T_hat,T_step))
                self.T_hat = self.T_hat.toarray()
        return self.T_hat, self.T_hat_diff, self.T_hat_2diff, self.T_hat_3diff

    def QP_inequality_constraints(self,T_hat, chord_subdivision):
        #This finds the inequality constraint terms Ptilde * That *[coeff] <= bvect
        b_vect_constraint_inequality = []
        P_tilde_constraint_inequality = []
        self.cvx_plane_constraints
        for idx in range(len(self.cvx_plane_constraints)):
            P_tilde_component = []
            P_tilde_bvect_component = []
            for jdx in range(chord_subdivision):
                if len(P_tilde_component) == 0:
                    P_tilde_component = np.vstack(self.cvx_plane_constraints[idx]["Plane_equations"])[:,:3]
                else:
                    planes = np.vstack(self.cvx_plane_constraints[idx]["Plane_equations"])[:,:3]
                    P_tilde_component = scipy.sparse.block_diag([P_tilde_component,planes]).toarray()
                P_tilde_bvect_component.append(np.array(np.matrix(-np.vstack(self.cvx_plane_constraints[idx]["Plane_equations"])[:,3]).T))
            b_vect_constraint_inequality.append(np.vstack(P_tilde_bvect_component))
            if len(P_tilde_constraint_inequality) == 0:
                P_tilde_constraint_inequality = P_tilde_component
            else:
                P_tilde_constraint_inequality =  scipy.sparse.block_diag([P_tilde_constraint_inequality,np.array(np.vstack(P_tilde_component))]).toarray() 
        b_vect_constraint_inequality = np.array(np.vstack(b_vect_constraint_inequality))
        A_constraint_inequality = np.matrix(scipy.sparse.csr_matrix(P_tilde_constraint_inequality)*np.array(np.double(T_hat)))
        return A_constraint_inequality, b_vect_constraint_inequality

    def Set_boundary_conditions(self,Ti_discrete, poly_order):
        pos_vel_constraints = np.zeros([6*len(Ti_discrete), (poly_order+1)*3*(len(Ti_discrete)-1)]); pos_vel_constraints_bvect= []
        BC = {"Aeq":[],"beq":[]}
        Phat = [] #variable that holds list of point matricies with pos,vel for each critical point
        for idx in range(len(Ti_discrete)):
            jdx = idx - 1
            pos_t = self.poly_order_fnct(Ti_discrete[idx], poly_order,"pos")
            vel_t = self.poly_order_fnct(Ti_discrete[idx], poly_order,"vel")
            Phat = np.vstack([pos_t,vel_t])
            zero_mat = np.zeros([6,pos_t.shape[1]])
            Pbar = []
            bvect = []
            if idx == 0:
                Pbar = Phat
                for kdx in range(len(Ti_discrete)-2):
                    print "dim of pbar: ", np.shape(Pbar), "dim of zmat: ", np.shape(zero_mat)
                    Pbar = np.hstack([Pbar, zero_mat])
                bvect = np.vstack([np.matrix(self.cvx_plane_constraints[0]["Plane_pts"][0,:]).T.tolist(), np.zeros([3,1])])
            elif idx == (len(Ti_discrete)-1):
                Pbar = zero_mat
                for kdx in range(1,len(Ti_discrete)-2):
                    Pbar = np.hstack([Pbar, zero_mat])
                Pbar = np.hstack([Pbar,  Phat])
                bvect = np.vstack([np.matrix(self.cvx_plane_constraints[-1]["Plane_pts"][1,:]).T.tolist(),np.zeros([3,1])])
            else:
                bvect= np.zeros([6,1])
                num_cols_left = np.shape(pos_t)[1]*len(range(0,idx-1))
                zero_mat_left = np.zeros([6,num_cols_left])
                P_eq = np.hstack([Phat, -Phat])
                num_cols_right = np.shape(pos_t)[1]*len(range(idx+1,(len(Ti_discrete)-1)))
                zero_mat_right = np.zeros([6,num_cols_right])
                Pbar = np.hstack([zero_mat_left, P_eq, zero_mat_right])

            BC["Aeq"].append(Pbar)
            BC["beq"].append(bvect)

        BC["Aeq"] = np.vstack(BC["Aeq"])
        BC["beq"] = np.vstack(BC["beq"])

        return BC

    def QP_Solve(self,H,f,A,b,Aeq,Beq):
        # """QP Solver
        # Args: H ,f ,A ,b ,Aeq ,Beq
        # Returns: 
        # x from x'Hx + fx subj to Ax <= b and Aeq x = b 
        # """
        # print "shape: H ", np.shape(H), "\n shape f: ", np.shape(f),"\n shape A: ", np.shape(A), "\n shape b: ", np.shape(b), "\n shape Aeq: ", np.shape(Aeq), "\n shape Beq:", np.shape(Beq)
        Q = cvxopt.matrix(H.toarray())
        p = cvxopt.matrix(f)
        G = cvxopt.matrix(A)
        bfinal =  np.matrix(b).astype(np.double)
        # print "b: ", bfinal 
        h = cvxopt.matrix(bfinal)
        # print "problem: ", b, "\n its type: ", type(b)
        # h = cvxopt.matrix(b)
        # print "h: ", h
        A = cvxopt.matrix(Aeq)
        b = cvxopt.matrix(Beq)
        sol = cvxopt.solvers.qp(Q, p, G, h, A, b)
        # sol = cvxopt.solvers.qp(Q, p)
        self.soln_coeff = np.matrix(sol['x'])
        # print "solution: ", self.soln_coeff, "its type: ", type(self.soln_coeff)
        #convert to list of point
        traj_points_list = np.matrix(self.T_hat)*self.soln_coeff
        cvx_traj_points = np.reshape(traj_points_list,[len(traj_points_list)/3,3])
        # print "trajectory points: ", cvx_traj_points


        soln_coeff = np.array(self.soln_coeff.reshape(len(self.soln_coeff)/(3*(self.poly_order+1)),3*(self.poly_order+1))).tolist()
        #the above has been verified, it is correctly reshaped
        # print "poly order: ", self.poly_order, "coeff list: ", soln_coeff #, "\n its shape:", soln_coeff.shape

        return cvx_traj_points, soln_coeff

    def convex_path_solver(self,chord_subdivision, poly_order,cvx_plane_constraints):
        print "this segment uses cvx to find the optimal path"
        #plane_dict={'Plane_pts':[], 'Plane_equations':[],'Plane_signs':[]}
        self.cvx_plane_constraints = cvx_plane_constraints
        print "\n number of chords ", len(self.cvx_plane_constraints)

        self.poly_order = poly_order

        #1. Divide up segments in order to determine relative time requirements for each chord (using ratio of chord lengths to total distance)
        """note that a upper time limit on every point can be imposed by putting limit on velocity on each dimension for every point"""

        chord_segment_lengths = {"lengths":[], "total_length":[]}
        for idx in range(len(self.cvx_plane_constraints)):
            # print "\n\n pts: ", self.cvx_plane_constraints[idx]["Plane_pts"], "\n plane eqns: ", self.cvx_plane_constraints[idx]["Plane_equations"], "\n plane signs: ", self.cvx_plane_constraints[idx]["Plane_signs"]
            li = np.linalg.norm([self.cvx_plane_constraints[idx]["Plane_pts"][0] - self.cvx_plane_constraints[idx]["Plane_pts"][1]])
            # print "\n length of ith chord", li
            chord_segment_lengths["lengths"].append(li)
        chord_segment_lengths["lengths"] = np.array(np.double(chord_segment_lengths["lengths"]))
        chord_segment_lengths["total_length"] = np.sum(chord_segment_lengths["lengths"])
        # print "list of chord segment lengths: ", chord_segment_lengths
        

        self.Time_normed_list = []

        #2. With this segmentation build the time matricies (for n'th order polynomial and m chords this builds a (3*m)x(3*(n+1)) time matrix)
        Ti_discrete = [0]# np.divide(chord_segment_lengths["lengths"],chord_segment_lengths["total_length"])
        for idx in range(len(chord_segment_lengths["lengths"])):
            Ti_discrete.append(chord_segment_lengths["lengths"][idx]/chord_segment_lengths["total_length"] + Ti_discrete[-1])

        T_hat, T_hat_diff, T_hat_2diff, T_hat_3diff = self.Get_Time_matricies(Ti_discrete, chord_subdivision, poly_order)

        # self.Time_normed_list = np.unique(np.hstack(self.Time_normed_list))
        self.Time_normed_list = np.hstack(self.Time_normed_list)

        #3. Get plane bounding matricies
        A_constraint_inequality, b_vect_constraint_inequality  = self.QP_inequality_constraints(T_hat, chord_subdivision)
        # print "A_constraint :",np.shape(A_constraint_inequality),"\n b constraint: ",np.shape(b_vect_constraint_inequality)
        
        """be careful about shape here, to get correct stacking and diagonalization """

        # b_vect_constraint_inequality.append(self.cvx_plane_constraints[idx]["Plane_equations"][:,3])
        # P_tilde_constraint_inequality.append(self.cvx_plane_constraints[idx]["Plane_equations"][:,:3])

        T_hat_mat = np.array(np.double(T_hat))
        T_hat_diff = np.array(np.double(T_hat_diff))
        T_hat_diff_left = scipy.sparse.csr_matrix(T_hat_diff.transpose())
        T_hat_diff_right =scipy.sparse.csr_matrix(T_hat_diff)



        T_hat_2diff_left = scipy.sparse.csr_matrix(self.T_hat_2diff.transpose())
        T_hat_2diff_right =scipy.sparse.csr_matrix(self.T_hat_2diff)

        T_hat_3diff_left = scipy.sparse.csr_matrix(self.T_hat_3diff.transpose())
        T_hat_3diff_right =scipy.sparse.csr_matrix(self.T_hat_3diff)


        # print "size of T hat diff: ", np.shape(self.T_hat_diff), "its type: ", type(self.T_hat_diff)
        H = T_hat_diff_left*T_hat_diff_right
        # H = T_hat_2diff_left*T_hat_2diff_right
        # H = T_hat_3diff_left*T_hat_3diff_right

        f = np.zeros([np.shape(H)[0],1])
        # P_tilde_left = scipy.sparse.csr_matrix(P_tilde_constraint_inequality)
        A =  A_constraint_inequality  #np.matrix(P_tilde_left*T_hat_mat)
        b = b_vect_constraint_inequality #-b_vect_constraint_inequality

        #now set equalities: 
        bound_cond = self.Set_boundary_conditions( Ti_discrete, poly_order)
        

        Aeq = bound_cond["Aeq"] #np.vstack([self.whole_traj_bc,self.diff_traj_bc])
        Beq = bound_cond["beq"]#np.vstack([self.whole_traj_bc_bvect,self.diff_traj_bc_bvect])
        # print "A equality: ", np.shape(Aeq), " beq: ", np.shape(Beq)
        
        self.cvx_traj_points,soln_coeff = self.QP_Solve(H,f,A,b,Aeq,Beq)
        #put this on a message and plot in rviz (in different color)
        scale = 3.2; color = [1,1,1]; self.Plotting_debugging_rviz.Plot_cvx_path(self.cvx_traj_points.tolist(),scale,color)

        # print "\n\n ROOT ISSUE: len of Ti_discrete:", len(Ti_discrete), "vs length of coeff matrix: ", len(soln_coeff)
        # print "\n\n"

        return self.cvx_traj_points, self.Time_normed_list, Ti_discrete, soln_coeff

    def get_polynomial_arc_length(self,path_pts):
        print "the type is: ",type(path_pts)

        arc_length = 0
        for idx in range(np.shape(path_pts)[0]-1):
            init_pt = path_pts[idx,:]; next_pt = path_pts[idx+1,:]
            arc_length = arc_length + np.linalg.norm(next_pt-init_pt)
        return arc_length


    def path_input_function(self,req):
        print "Variables: \n Start: ", req.start, "\n Goal: ", req.goal, "\n Obstacle list: ", req.obstacle_list, "\n World points: ", req.world
        #obstacle list is array Nx3 of obstacle points



        theta_min,theta_max = -3*math.pi, 3*math.pi
        self.start_pose = np.matrix([req.start.x, req.start.y, req.start.theta])
        self.goal_pose = np.matrix([req.goal.x, req.goal.y, req.goal.theta])
        self.obstacle_list = np.matrix(np.vstack([req.obstacle_list.x,req.obstacle_list.y])).T

        self.graph_scale = req.graph_scale

        self.discretize_world(req.world.xmin,req.world.xmax,req.world.ymin,req.world.ymax,theta_min,theta_max, req.world.xsteps ,req.world.ysteps,req.world.thsteps)#xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps
        self.get_object_pose(self.start_pose,self.goal_pose)
        show_mink_pts = False
        self.minkowski_object_points()
        self.get_occupied_points()
        # show_initial_path_bool = False #display the initial math and minkowski pts
        self.obtain_initial_viable_path()
        chord_subdivision = 50 #100 #subdivide chords into these many pts, should be 100
        """loop through dictionary elements here """
        cvx_constraint_dict = self.convex_region_finder(chord_subdivision)

        # cls_obj.paths_to_all_possible_goals.keys()
        poly_order = 3 #3 with vel, 5 with acc, #7 with jerk #order of polynomials
        cvx_paths_dict = dict()
        for path_key in cvx_constraint_dict.keys():
            cvx_plane_constraints=cvx_constraint_dict[path_key]
            path, time_vect,Ti_discrete, soln_coeff = self.convex_path_solver(chord_subdivision,poly_order, cvx_plane_constraints)
            path_arc_length = self.get_polynomial_arc_length(path)
            cvx_paths_dict[path_key] = [path, path_arc_length, time_vect,soln_coeff, Ti_discrete]
            print "path arc length: ", path_arc_length


        list = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),0]
        times = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),2]
        final_coeff = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),3]
        final_Ti_discrete = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),4]
        list = np.array(list.tolist())
        # print "list shape: ",np.shape(list), "list type: ", type(list)
        # list_unique = np.vstack({tuple(row) for row in list})
        time_unique, unique_ind  = np.unique(times, return_index = True)
        list_unique = list[unique_ind,:]
        # print  "\n unique list: ", list_unique, "\n unique times: ", time_unique, "\n list type: ", type(list_unique)

        trajectory = [list_unique, time_unique]

        print "\n\n ROOT ISSUE (FINAL PATH): len of Ti_discrete:", len(final_Ti_discrete), "vs length of coeff matrix: ", len(final_coeff)


        # points = np.array([[1.,2.,3.],[4.,5.,6.],[7.,8.,9.]]) #list type
        point_list = []
        for idx in range(list_unique.shape[0]):
            poses = PoseStamped()
            poses.pose.position.x = list_unique[idx,0]
            poses.pose.position.y = list_unique[idx,1]
            poses.pose.position.z = 0.0
            poses.pose.orientation.w = np.cos(list_unique[idx,2]/2.)
            poses.pose.orientation.z = np.sin(list_unique[idx,2]/2.)
            poses.pose.orientation.x = 0.0
            poses.pose.orientation.y = 0.0
            poses.header.stamp = rospy.Time.from_sec(np.float(time_unique[idx]))  #note that this is 1x10^9 sec, divide by this value to get seconds and add this to inital time when executing trajectory goes form 
            point_list.append(poses)

        trajectory  = PathPlanResponse() #autogenerated type
        trajectory.traj.nav_traj.poses =  point_list #np.append([],np.ndarray.tolist(np.asarray(  traj_list )))
        trajectory.traj.normed_time = final_Ti_discrete
        trajectory.traj.poly_order = poly_order



        coeff_row_list = []
        for jdx in range(len(final_coeff)):
            coeff_row_list.append(ListMsg())

        for jdx in range(len(final_coeff)):
            # idx = len(final_coeff) - (jdx+1)
            coeff_row_list[jdx].list_row = final_coeff[jdx]

        trajectory.traj.path_coeff = coeff_row_list

        print " \ntraj path coeff msg: ",trajectory.traj.path_coeff
        # stop

        return trajectory





def Path_finder_server():
    rospy.init_node('path_finder_server')

    path_topic = "/path_points"; path_points_pub = rospy.Publisher(path_topic,MarkerArray,queue_size = 1)
    cvx_path_topic = "/cvx_path_points"; cvx_path_points_pub = rospy.Publisher(cvx_path_topic,MarkerArray,queue_size = 1)
    topic = "/points_visualization"; obstacle_points_pub = rospy.Publisher(topic,PointCloud,queue_size = 1)
    vector_topic = "/vectors_drawn"; vector_pub = rospy.Publisher(vector_topic,Marker,queue_size = 1)
    plane_topic = "/planes_drawn"; planes_pub = rospy.Publisher(plane_topic,Marker,queue_size = 1)
    # plane_topic = "/planes_drawn"; planes_pub = rospy.Publisher(plane_topic,MarkerArray,queue_size = 1)
    valid_occ_topic = "/valid_points_visualization"; valid_obstacle_points_pub = rospy.Publisher(valid_occ_topic,PointCloud,queue_size = 1)


    time_start = time.time()
    cls_obj = Path_finder_class(path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub)





    #SERVICE
    s = rospy.Service('Path_finder', PathPlan, cls_obj.path_input_function)
    print "Ready to path plan"
    rospy.spin()






if __name__ == "__main__":
    Path_finder_server()