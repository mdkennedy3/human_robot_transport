#!/usr/bin/env python2
import rospy
import sys
import numpy as np
import math
import numpy.matlib
import yaml
import time
import tf
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
# from geometry_msgs.msg import Point
from geometry_msgs.msg import Point32 as geomPoint
from sensor_msgs.msg import PointCloud
from geometry_msgs.msg import Twist, Vector3
#for service
from se2path_finding.srv import *
from se2path_finding.msg import *
from se2path_finding.path_plotting_lib import  plot_path_plan_lib
import scipy
from scipy import linalg 
from scipy import sparse
#for getting odometry
from nav_msgs.msg import Odometry as Odom

from geometry_msgs.msg import Pose2D


class virtual_obstacles(object):
    def __init__(self,x1,y1,x2,y2,x_steps,y_steps):
        #This class makes artificial obstacles given the bounding corners
        #the first corner (x1,y1), and opposite corner (x2,y2), let x1<x2 & y1<y2
        self.corner1 = [x1,y1]; self.corner2 = [x2,y2]
        # xstep = world.steps['x_steps']/2.; ystep = world.steps['y_steps']/2.  #was division by 3
        xstep = 2*x_steps; ystep = 2*y_steps
        x_num,x_rem = divmod(abs(x2-x1),xstep); y_num,y_rem = divmod(abs(y2-y1),ystep)
        x_coord = np.linspace(x1,x2,x_num); y_coord = np.linspace(y1,y2,y_num)  
        if x_rem != 0: np.append(x_coord,x2)
        if y_rem != 0: np.append(y_coord,y2)
        obstacle_side_1 = []; obstacle_side_2 = []; obstacle_side_3 = []; obstacle_side_4 = []
        for jdx in range(len(y_coord)): point = [x1, y_coord[jdx]]; obstacle_side_1.append(point)
        for jdx in range(len(y_coord)): point = [x2, y_coord[jdx]]; obstacle_side_2.append(point)
        for jdx in range(len(x_coord)): point = [x_coord[jdx],y1]; obstacle_side_3.append(point)
        for jdx in range(len(x_coord)): point = [x_coord[jdx],y2]; obstacle_side_4.append(point)
        
        # try:
        pts = np.matrix(np.vstack([obstacle_side_1,obstacle_side_2,obstacle_side_3,obstacle_side_4]))
        # print "pt type: ", type(pts)
        # except:
        # print "xcoord: ", x_coord, "\n ycoord: ", y_coord, "y1, y2", y1,y2, " y_num: ", y_num, "y_step", ystep, "y rem: ", y_rem
        # print "failed pts: ", np.shape(np.array(obstacle_side_1)), "\n 2: ", np.shape(obstacle_side_2), "\n 3: ", np.shape(obstacle_side_3), "\n 4: ", np.shape(obstacle_side_4)
        obs_list = []
        for idx in x_coord:
            for jdx in y_coord:
                obs_list.append([idx,jdx])
        pts = np.matrix(obs_list)  #fill in obstacles densly (filter will handle data size)


        self.points = pts
        # print "got here: ", self.points
        #obstacle points plot:
        # fig = plt.figure(); ax = fig.add_subplot(111);c = 'r'; m = 'o'; xs = pts[:,0].T; ys = pts[:,1].T; xs = xs.tolist()[0]; ys = ys.tolist()[0]; ax.scatter(xs, ys, c=c, marker=m); plt.show()
        #real sensor has 270 readings per sweep, that times number of convex hull verticies, then times the number of steps of theta

class Path_class(object):
    def __init__(self,obs_cube_pub):
        self.Plotting_rviz = plot_path_plan_lib.Plot_general_markers_rviz(obs_cube_pub)

    def vertical_list(self,obstacle_map,x_steps, y_steps):
        class obs_type(object):
            def __init__(self):
                self.x = []
                self.y = []
        virt_obs_list = []
        # print "given obs map: ", obstacle_map
        for idx in range(len(obstacle_map)):
            x1 = obstacle_map[idx][0];  y1 = obstacle_map[idx][1];  x2 = obstacle_map[idx][2];  y2 = obstacle_map[idx][3]
            vobs = virtual_obstacles(x1,y1,x2,y2,x_steps,y_steps) 
            vobs = vobs.points.tolist()
            virt_obs_list.append(vobs)
        virt_obs = np.vstack(virt_obs_list)
        obstacles_list  = np.array(virt_obs)
        obstacle_obj = obs_type()
        # print "failing here: obslist: ",obstacles_list
        obstacle_obj.x = list(obstacles_list[:,0])
        obstacle_obj.y = list(obstacles_list[:,1])
        return obstacle_obj


    def Path_service_func(self, *arg_params):
        rospy.wait_for_service('Path_finder')
        if len(arg_params) >= 1:
            #this can't be done unless already initialized
            try: 
                self.Path_service
            except:
                print "didn't initialize..."
            Path_request = PathPlanRequest() #this type has the fields and can be passed directly
            new_goal = arg_params[0]
            self.goal = Pose2D()
            goal.x = new_goal[0]
            goal.y = new_goal[1]
            goal.theta = new_goal[2] #betwen -pi:pi
            if len(arg_params) >= 2:
                obstacle_points = arg_params[1] #must be list type
                self.obstacles = obstacle_points #np.append([],np.ndarray.tolist(np.asarray(  obstacle_points )))  #Nx2 list of obstacle points (occupied positions)
            Path_request.start = self.start; Path_request.goal = self.goal; Path_request.world = self.world_bound; Path_request.obstacle_list = self.obstacles
            self.trajectory = self.Path_service(Path_request) #returns a list of points in se2 [x,y,th] and normed times [ti]: [[x,y,th]_Nx3,[ti]_Nx1] for N path points
        else:
            try:
                #define service type
                self.Path_service = rospy.ServiceProxy('Path_finder', PathPlan)
                Path_request = PathPlanRequest() #this type has the fields and can be passed directly
                #define start pose: 
                self.start = Pose2D() #consists of float 64
                '''get this from odom '''
                # graph_scale = 100. #100 #convert all to cm
                graph_scale = 100. #100 #convert all to cm
                self.graph_scale = graph_scale
                self.start.x = 0.0*graph_scale #cm
                self.start.y = 0.0*graph_scale
                self.start.theta = 0.   #betwen -pi:pi
                #define goal pose:
                self.goal = Pose2D()
                self.goal.x = 7.0*graph_scale  #7
                self.goal.y = -1.0*graph_scale #-1
                self.goal.theta = 90.*math.pi/180 #betwen -pi:pi

                # self.goal.x = -4.0*100.
                # self.goal.y = 1.25*100.
                # self.goal.theta = math.pi #betwen -pi:pi
                



                #define world boundary: 
                self.world_bound = World_boundary()
                #mocap space is ~50ftx20ft  in meters: 15.24x6.096
                # self.world_bound.xmin = -30.0; self.world_bound.xmax = 30.0; self.world_bound.ymin = -30.0; self.world_bound.ymax = 30.0
                self.world_bound.xmin = -7.62*graph_scale #cm
                self.world_bound.xmax = 7.62*graph_scale #cm
                self.world_bound.ymin = -3.048*graph_scale #cm
                self.world_bound.ymax = 3.048*graph_scale #cm


                ''' consider using angles in degrees for'''
                x_steps,y_steps,theta_steps = .1*graph_scale,.1*graph_scale,10*math.pi/180  #in meters and radians
                self.world_bound.xsteps = x_steps
                self.world_bound.ysteps = y_steps
                self.world_bound.thsteps = theta_steps


                Obstacle_map_1 = [[2.*graph_scale,1.0*graph_scale,4.*graph_scale,2.*graph_scale],
                                  [2.*graph_scale,-2.*graph_scale,4.*graph_scale,-1.0*graph_scale],
                                  [5.*graph_scale,-1.*graph_scale,6.*graph_scale,1.*graph_scale],
                                  [5.*graph_scale,-2.5*graph_scale,6.*graph_scale,-2.*graph_scale]]


                Obstacle_map_2 = [[-2.3*graph_scale, -1.8*graph_scale, -2.*graph_scale, 1.8 *graph_scale],
                                  [-5.5*graph_scale, 1.5*graph_scale, -2.*graph_scale, 2.*graph_scale],
                                  [-4.5*graph_scale, 0.4*graph_scale, -5.*graph_scale, 1.5*graph_scale],
                                  [-5.*graph_scale, 0.4*graph_scale, -3.*graph_scale, 1.*graph_scale],
                                  [-5.5*graph_scale, -2.*graph_scale, -4.2*graph_scale, -0.4*graph_scale],
                                  [-4.2*graph_scale, -2.*graph_scale, -2.*graph_scale, -1.8*graph_scale],
                                  [-7.*graph_scale,-2.*graph_scale,-6.8*graph_scale,2.*graph_scale]]




                use_yaml = True

                if use_yaml:
                    file_name = '/home/kmonroe/kuka7dof_ws/src/human_robot_transport/se2path_finding/scripts/obs_map_data.yaml'
                    with open(file_name, 'r') as f:
                        obs_map_test = yaml.load(f)
                    # keys = ['obs_8', 'obs_2', 'obs_3', 'obs_1', 'obs_6', 'obs_7', 'obs_4', 'obs_5']



                    obstacle_map_curr = []
                    for key in obs_map_test.keys():
                        if key not in "goal_pose":
                            row =np.dot(graph_scale, np.hstack(obs_map_test[key]))
                            # print "adding: ",row, "type: ", type(row)
                            obstacle_map_curr.append(row.tolist())
                    # print "obs map curr: ", obstacle_map_curr

                    self.goal.x = obs_map_test["goal_pose"][0]*graph_scale  #7
                    self.goal.y = obs_map_test["goal_pose"][1]*graph_scale #-1
                    self.goal.theta = obs_map_test["goal_pose"][2] #betwen -pi:pi

                    Obstacle_map_1 = obstacle_map_curr





                """get obstacle object given list of points, where each row of the map is the corners of the map """

                obstacle_points = self.vertical_list(Obstacle_map_1,x_steps, y_steps)
                self.obstacle_points_list = Obstacle_map_1



                #send to service
                Path_request.obstacle_list.x = obstacle_points.x;Path_request.obstacle_list.y = obstacle_points.y
                Path_request.start = self.start; Path_request.goal = self.goal; Path_request.world = self.world_bound; 
                Path_request.graph_scale = self.graph_scale
                print "sending graph scale:",Path_request.graph_scale
                self.trajectory = self.Path_service(Path_request) #returns a list of points in se2 [x,y,th] and normed times [ti]: [[x,y,th]_Nx3,[ti]_Nx1] for N path points
                print "trajectory returned: ", self.trajectory
                return self.trajectory
            except rospy.ServiceException, e:
                print "Service call failed: %s"%e


    def plot_obstacle_cubes(self):
        print "ready to plot obstacle cubes:"

        self.obstacle_points_list = np.dot(1/self.graph_scale,np.array(self.obstacle_points_list)).tolist()
        self.Plotting_rviz.plot_object_cubes(self.obstacle_points_list)




class Youbot_control(object):
    def __init__(self,traj,final_path_pub,scale_to_divide,cmd_vel_pub,des_pos_pub,cvx_hull_path_pub):
        self.traj = traj
        self.Plotting_rviz = plot_path_plan_lib.Plot_general_markers_rviz(final_path_pub)
        self.scale_to_divide = scale_to_divide
        self.init_time = rospy.Time.now().to_sec()
        self.time_scale = 5*60. #order of magnitude, which is the max time given to complete the trajectory
        self.coeff_idx = 0
        self.cmd_vel_pub = cmd_vel_pub
        self.Plotting_rviz_des_pos_pub = plot_path_plan_lib.Plot_general_markers_rviz(des_pos_pub)
        self.Plotting_rviz_cvx_hull_path_pub = plot_path_plan_lib.Plot_general_markers_rviz(cvx_hull_path_pub)

        x1,x2,y1,y2 = -0.3048, 0.3048, -0.2032,0.2032 #cm or m
        self.robot_cvx_hull_pts = {"x1":x1,"x2":x2,"y1":y1,"y2":y2}

        # self.youbot_vel_max = {"vx":0.6,"vy":0.6,"vth":30.*math.pi/180.}
        # self.youbot_vel_max = {"vx":1.5,"vy":1.5,"vth":0.4}
        self.youbot_vel_max = {"vx":15,"vy":15,"vth":40}
        # self.youbot_vel_max = {"vx":.5,"vy":.5,"vth":0.2}

    def reparameterzied_trajectory(self,path_obj,time_list,path_coeff_init):
        #expects
        #time_list: (type: list), with [0,i,i+1,...,1] (normed time)
        #path_obj: type nav_msgs_Path
        path_lengths = []
        for idx in range(len(time_list)-1):
            t_count_min = time_list[idx]
            t_count_max = time_list[idx+1]
            local_arc_length = 0
            for idx in range(1,len(path_obj)):
                if (path_obj[idx].header.stamp.to_sec() > t_count_min) and (path_obj[idx].header.stamp.to_sec() < t_count_max):

                    ang1 = np.arctan2(path_obj[idx].pose.orientation.z , path_obj[idx].pose.orientation.w)*2. 
                    ang2 = np.arctan2(path_obj[idx-1].pose.orientation.z , path_obj[idx-1].pose.orientation.w)*2. 
                    R1 = np.matrix([[np.cos(ang1), -np.sin(ang1)],[np.sin(ang1), np.cos(ang1)]])
                    R2 = np.matrix([[np.cos(ang2), -np.sin(ang2)],[np.sin(ang2), np.cos(ang2)]])
                    Rdiff = R2*R1.T
                    ang_diff = np.arctan2(Rdiff[1,0],Rdiff[0,0])
                    local_arc_length = local_arc_length + np.sqrt(np.sum( (path_obj[idx].pose.position.x - path_obj[idx-1].pose.position.x)**2 + (path_obj[idx].pose.position.y - path_obj[idx-1].pose.position.y)**2) + ang_diff**2)
                    # local_arc_length = local_arc_length + np.sqrt(np.sum( (path_obj[idx].pose.position.x - path_obj[idx-1].pose.position.x)**2 + (path_obj[idx].pose.position.y - path_obj[idx-1].pose.position.y)**2) )
            path_lengths.append(local_arc_length)

        new_time_list = [0]
        time_ratio_normed = 0
        for jdx in range(len(time_list)-1):
            time_ratio_normed = time_ratio_normed + np.double(path_lengths[jdx])/np.sum(path_lengths) 
            # print "arc lengths: ", path_lengths, " this ratio: ",time_ratio_normed, "for idx ", jdx
            new_time_list.append(time_ratio_normed)
        # print "times returned: ", new_time_list


        #Now find the boundary conditions
        #input: 
        #-coeff list: path_coeff_init
        #-init_normed_time: time_list
        #output:
        #-new coeff list

        new_coeff_list = []
        for idx in range(len(time_list)-1):
            #get time matrix for first
            t_i_init = time_list[idx]; t_i_init_pos = np.matrix([t_i_init**3,t_i_init**2,t_i_init,1]); t_i_init_vel = np.matrix([3**t_i_init**2,2*t_i_init,1,0])
            Tmat_init_i_pos = scipy.linalg.block_diag(t_i_init_pos,t_i_init_pos,t_i_init_pos); Tmat_init_i_vel = scipy.linalg.block_diag(t_i_init_vel,t_i_init_vel,t_i_init_vel)
            t_j_init = time_list[idx+1]; t_j_init_pos = np.matrix([t_j_init**3,t_j_init**2,t_j_init,1]); t_j_init_vel = np.matrix([3**t_j_init**2,2*t_j_init,1,0])
            Tmat_init_j_pos = scipy.linalg.block_diag(t_j_init_pos,t_j_init_pos,t_j_init_pos); Tmat_init_j_vel = scipy.linalg.block_diag(t_j_init_vel,t_j_init_vel,t_j_init_vel)
            Tmat_init_i = np.vstack([Tmat_init_i_pos,Tmat_init_i_vel]); Tmat_init_j = np.vstack([Tmat_init_j_pos,Tmat_init_j_vel]); Tmat_init = np.vstack([Tmat_init_i,Tmat_init_j])

            t_i_final = new_time_list[idx]; t_i_final_pos = np.matrix([t_i_final**3,t_i_final**2,t_i_final,1]); t_i_final_vel = np.matrix([3**t_i_final**2,2*t_i_final,1,0])
            Tmat_final_i_pos = scipy.linalg.block_diag(t_i_final_pos,t_i_final_pos,t_i_final_pos); Tmat_final_i_vel = scipy.linalg.block_diag(t_i_final_vel,t_i_final_vel,t_i_final_vel)
            t_j_final = new_time_list[idx+1]; t_j_final_pos = np.matrix([t_j_final**3,t_j_final**2,t_j_final,1]); t_j_final_vel = np.matrix([3**t_j_final**2,2*t_j_final,1,0])
            Tmat_final_j_pos = scipy.linalg.block_diag(t_j_final_pos,t_j_final_pos,t_j_final_pos); Tmat_final_j_vel = scipy.linalg.block_diag(t_j_final_vel,t_j_final_vel,t_j_final_vel)
            Tmat_final_i = np.vstack([Tmat_final_i_pos,Tmat_final_i_vel]);
            Tmat_final_j = np.vstack([Tmat_final_j_pos,Tmat_final_j_vel])
            Tmat_final = np.vstack([Tmat_final_i,Tmat_final_j])

            current_coeff_vector = np.matrix(path_coeff_init[idx].list_row).T
            
            pos_vel_constraint_vector = Tmat_init*current_coeff_vector
            new_coeff_curr = np.linalg.pinv(Tmat_final)*pos_vel_constraint_vector
            # print "new shape of new_coeff_curr: ", new_coeff_curr.shape
            new_coeff_curr = new_coeff_curr.T[0]
            new_coeff_curr = new_coeff_curr.tolist()[0]
            # print "shape of Tmat_init: ", Tmat_init.shape, "\n shape of current_coeff_vector", current_coeff_vector.shape
            # print "\n new coeff: ", new_coeff_curr
            new_coeff_list.append(new_coeff_curr)


        return new_time_list, new_coeff_list

    def plot_final_path_r2(self):
        print "plotting path in R2"
        nav_msg_traj_path = self.traj.traj.nav_traj.poses
        self.Plotting_rviz.plot_final_path(nav_msg_traj_path,self.scale_to_divide)

    def plot_robot_cvx_hull_path_r2(self):
        print "plotting cvx hull path in R2"
        nav_msg_traj_path = self.traj.traj.nav_traj.poses

        self.Plotting_rviz_cvx_hull_path_pub.plot_robot_cvx_boundary_at_path_pts(nav_msg_traj_path,self.scale_to_divide,self.robot_cvx_hull_pts)


    def poly_order_fnct(self,t,order,poly_type):
        pos_time = []
        vel_time = []
        acc_time = []
        jerk_time = []
        for idx in range(order):
            exp = (order - idx)
            pos_time.append(t**exp)
            vel_time.append(exp*t**(exp-1))
            if exp >= 2:
                acc_time.append(exp*(exp-1)*t**(exp-2))
            else:
                acc_time.append(0.)

            if exp >=3:
                jerk_time.append(exp*(exp-1)*(exp-2)*t**(exp-3))
            else:
                jerk_time.append(0.)

        pos_time.append(1.)
        vel_time.append(0.)
        acc_time.append(0.)
        jerk_time.append(0.)
        pos_time = scipy.sparse.block_diag([pos_time,pos_time,pos_time]).toarray()
        vel_time = scipy.sparse.block_diag([vel_time,vel_time,vel_time]).toarray()
        acc_time = scipy.sparse.block_diag([acc_time,acc_time,acc_time]).toarray()
        jerk_time = scipy.sparse.block_diag([jerk_time,jerk_time,jerk_time]).toarray()

        # print "pos var: ", pos_time,"vel_time", vel_time
        if poly_type in "pos":
            return pos_time
        elif poly_type in "vel":
            return vel_time
        elif poly_type in "acc":
            return acc_time
        elif poly_type in "jerk":
            return jerk_time
        elif poly_type in "all":
            #ie access with v[:,0] for pos 
            return [pos_time, vel_time, acc_time, jerk_time]


    def odom_callback(self,odom):
        theta_odom = np.arctan2(odom.pose.pose.orientation.z , odom.pose.pose.orientation.w)*2. 
        if theta_odom > math.pi: 
            theta_odom = theta_odom - 2*math.pi
        elif theta_odom < -math.pi: 
            theta_odom = theta_odom + 2*math.pi

        # print "\n pose odom: x: ", odom.pose.pose.position.x," y: ", odom.pose.pose.position.y," theta: ",  theta_odom
        # print "\n vel odom: x: ", odom.twist.twist.linear.x," y: ", odom.twist.twist.linear.y," theta: ",  odom.twist.twist.angular.z

        self.curr_time = rospy.Time.now().to_sec()
        diff_time = self.curr_time -self.init_time 
        time_ratio = diff_time/self.time_scale

        #reparameterize for smooth consistent speed throughout trajectory:
        nag_msg_path = self.traj.traj.nav_traj.poses
        normed_time_list = self.traj.traj.normed_time
        new_time_list,new_coeff_list = self.reparameterzied_trajectory(nag_msg_path,normed_time_list,self.traj.traj.path_coeff)
        # print "\n\n New time list: ", new_time_list, "\n new coeff list: ", new_coeff_list

        current_path_coeff = np.matrix(self.traj.traj.path_coeff[self.coeff_idx].list_row).T
        normed_time = self.traj.traj.normed_time

        """REPARAMETERIZED TRAJ: """
        # current_path_coeff = np.matrix(new_coeff_list[self.coeff_idx]).T
        # normed_time = new_time_list


        #get current pose (check what time bracket your in)
        self.coeff_idx = 0
        for idx in range(len(normed_time)-1):
            if time_ratio > normed_time[idx]:
                self.coeff_idx = idx

        if time_ratio > 1.0:
            time_ratio = 1.0
        # print "time ratio: ", time_ratio, "diff time: ", diff_time, "max time: ", self.time_scale

        time_basis_vect = [1]
        for idx in range(int(self.traj.traj.poly_order)):
            t = time_ratio**(idx+1)
            time_basis_vect.insert(0,t)

        t = time_ratio#self.traj.traj.normed_time[self.coeff_idx+1]
        poly_type = "pos"
        poly_order = int(self.traj.traj.poly_order)
        vel_time_basis_vect = self.poly_order_fnct(t,poly_order,"vel")

        pos_Time_matrix = scipy.linalg.block_diag(time_basis_vect,time_basis_vect,time_basis_vect)
        # print  "\n time ratio: ", time_ratio
        curr_des_pose = np.matrix(pos_Time_matrix)*current_path_coeff
        curr_des_vel = np.matrix(vel_time_basis_vect)*current_path_coeff
        theta_des = curr_des_pose.item(2)

        if theta_des > math.pi: 
            theta_des = theta_des - 2*math.pi
        elif theta_des < -math.pi: 
            theta_des = theta_des + 2*math.pi

        curr_des_pose = np.matrix([[curr_des_pose.item(0)/self.scale_to_divide],[curr_des_pose.item(1)/self.scale_to_divide],[theta_des]])
        curr_des_vel = np.matrix([[curr_des_vel.item(0)/self.scale_to_divide],[curr_des_vel.item(1)/self.scale_to_divide],[curr_des_vel.item(2)]])
        # print"\n desired positions at current time: ", curr_des_pose


        #Control Law:
        curr_pose = np.matrix([[odom.pose.pose.position.x],[odom.pose.pose.position.y],[theta_odom]])
        curr_vel = np.matrix([[odom.twist.twist.linear.x],[odom.twist.twist.linear.y],[odom.twist.twist.angular.z]])

        R_th_des = np.matrix([[np.cos(theta_des), -np.sin(theta_des), 0],[np.sin(theta_des), np.cos(theta_des),0],[0,0,1]]) 
        R_th_act = np.matrix([[np.cos(theta_odom), -np.sin(theta_odom), 0],[np.sin(theta_odom), np.cos(theta_odom),0],[0,0,1]])
        #input velocities
        gains = {"Kp":[],"Kd":[],"Ki":[]}
        gains["Kp"] = np.matrix(np.diag(10*np.ones(3))); gains["Kd"] = np.matrix(np.diag(.1*np.ones(3))); gains["Ki"] = np.matrix(np.diag(0.01*np.ones(3)))


        ang1 = theta_odom
        ang2 = theta_des
        R1 = np.matrix([[np.cos(ang1), -np.sin(ang1)],[np.sin(ang1), np.cos(ang1)]])
        R2 = np.matrix([[np.cos(ang2), -np.sin(ang2)],[np.sin(ang2), np.cos(ang2)]])
        Rdiff = R2*R1.T
        ang_diff = np.arctan2(Rdiff[1,0],Rdiff[0,0])

        error_pose = curr_des_pose - curr_pose
        error_pose = np.matrix([[error_pose.item(0)],[error_pose.item(1)],[ang_diff]])

        error_vel = curr_des_vel - curr_vel
        # error_vel = - curr_vel

        # vel_input = R_th_des.T*curr_des_vel +
        vel_input = R_th_act.T*(gains["Kp"]*error_pose + gains["Kd"]*error_vel)

        print "\ncurrent pose: ", curr_pose, " desired pose: ", curr_des_pose
        print "\npose_err: ", error_pose, "\n vel_input: ", vel_input

        cmd_twist = Twist()

        if np.abs(vel_input.item(0)) > self.youbot_vel_max["vx"]:
            print "maxed out speed: ",np.sign(vel_input.item(0))*self.youbot_vel_max["vx"]
            cmd_twist.linear.x = np.sign(vel_input.item(0))*self.youbot_vel_max["vx"]
        else:
            cmd_twist.linear.x = vel_input.item(0)

        if np.abs(vel_input.item(1)) > self.youbot_vel_max["vy"]:
            cmd_twist.linear.y = np.sign(vel_input.item(1))*self.youbot_vel_max["vy"]
        else:
            cmd_twist.linear.y = vel_input.item(1)
        if np.abs(vel_input.item(2)) > self.youbot_vel_max["vth"]:
            cmd_twist.angular.z = np.sign(vel_input.item(2))*self.youbot_vel_max["vth"]
        else:
            cmd_twist.angular.z = vel_input.item(2)
        # cmd_twist.linear.x = 0.1
        # cmd_twist.linear.y = 0.
        # cmd_twist.angular.z = 0.

        print "commanded x:",cmd_twist.linear.x, " y: ",cmd_twist.linear.y," th:", cmd_twist.angular.z

        self.cmd_vel_pub.publish(cmd_twist)
        # r = rospy.Rate(10) # 10hz
        # r.sleep()
        print "\n\n\n"


        self.Plotting_rviz_des_pos_pub.plot_desired_pos_r2(curr_des_pose)



def main():
    rospy.init_node("main_node")
    obs_cube_topic = "/obstacle_cubes"; obs_cube_pub = rospy.Publisher(obs_cube_topic, MarkerArray, queue_size = 1)
    cmd_vel_topic = "/cmd_vel"; cmd_vel_pub = rospy.Publisher(cmd_vel_topic,Twist,queue_size = 1) #has components Vector3: linear, angular *.x,*.y,*.z, for omni base I control lin.x,lin.y,ang.z together

    des_pos_topic = "/des_position"; des_pos_pub = rospy.Publisher(des_pos_topic, Marker, queue_size = 1)

    cvx_hull_path_topoic = "/cvx_hull_path"; cvx_hull_path_pub = rospy.Publisher(cvx_hull_path_topoic,MarkerArray, queue_size = 1)

    path_cls_obj = Path_class(obs_cube_pub)
    traj = path_cls_obj.Path_service_func()
    path_cls_obj.plot_obstacle_cubes()

    scale_to_divide = path_cls_obj.graph_scale

    final_path_r2_topic = "/gamma_rays"; final_path_drawn_pub  = rospy.Publisher(final_path_r2_topic, Marker, queue_size = 1)
    
    cntrl_cls_obj = Youbot_control(traj,final_path_drawn_pub,scale_to_divide,cmd_vel_pub,des_pos_pub,cvx_hull_path_pub)
    # while not rospy.is_shutdown():
    r = rospy.Rate(5) # 10hz
    for idx in range(3):
        cntrl_cls_obj.plot_final_path_r2()
        cntrl_cls_obj.plot_robot_cvx_hull_path_r2()
        r.sleep()
        if rospy.is_shutdown():
            break 




    print "press enter to execute path, any other key then enter to abort"
    key = sys.stdin.read(1)
    if key in '\n':
        print "\n Executing Trajectory!"
        cntrl_cls_obj.init_time = rospy.Time.now().to_sec()
        cntrl_cls_obj.time_scale = 20.  #max time to complete trajectory
        print "start time is: ", cntrl_cls_obj.init_time
        # while not rospy.is_shutdown():
        robot_odom_topic = "/odom"; rospy.Subscriber(robot_odom_topic, Odom, cntrl_cls_obj.odom_callback, queue_size = 1)
        rospy.spin()
    else:
        print "main script finished"


        


if __name__ == '__main__':
    main()