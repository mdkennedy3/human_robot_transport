import scipy.signal
import numpy as np
import sympy as sp


class QP_poly_cls(object):

    def poly_mat(self,f,a):
        Mat = []
        for ci in a.tolist():
            row = []
            for cj in a.tolist():
                row.append(0.5*sp.diff(f,ci[0],cj[0]))
            # print "row: ", row
            Mat.append(row)
        return Mat
    def poly_order(self,order,*args):
        a_big_list = sp.var('a12,a11,a10,a9,a8,a7,a6,a5,a4,a3,a2,a1,a0')
        t = sp.Symbol('t')
        tmat = [1]
        for idx in range(order):
            jdx = idx+1
            tmat.insert(0,t**jdx)
        a_list = np.matrix(a_big_list[-(order+1):]).T
        tmat = np.matrix(tmat)
        p = tmat*a_list; p = p.item(0)

        if len(args) >0:
            if args[0] == "Tmat":
                if args[1] == "vel":
                    pd = sp.diff(p,t)
                    f = sp.expand(scipy.signal.convolve(pd,pd))
                    f = sp.integrate(f,t)
                    mat = self.poly_mat(f,a_list)
                    T_obj = np.matrix(mat)
                elif args[1] == "acc":
                    pd = sp.diff(p,t)
                    pdd = sp.diff(pd,t)
                    f = sp.expand(scipy.signal.convolve(pdd,pdd))
                    f = sp.integrate(f,t)
                    mat = self.poly_mat(f,a_list)
                    T_obj = np.matrix(mat)
                elif args[1] == "jerk":
                    pd = sp.diff(p,t)
                    pdd = sp.diff(pd,t)
                    pddd = sp.diff(pdd,t)
                    f = sp.expand(scipy.signal.convolve(pddd,pddd))
                    f = sp.integrate(f,t)
                    mat = self.poly_mat(f,a_list)
                    T_obj = np.matrix(mat)
                if len(args) == 3:
                    for i in range(T_obj.shape[0]):
                        for j in range(T_obj.shape[1]):
                            # print "should sub: ",T_obj[i,j].subs(t,args[2])
                            T_obj[i,j] = T_obj[i,j].subs(t,args[2])
                if len(args) == 4:
                    # T_obj_ti = np.zeros(T_obj.shape)
                    # T_obj_tf = np.zeros(T_obj.shape)
                    T_obj_ti = sp.Matrix(T_obj).subs(t,args[2])
                    T_obj_tf = sp.Matrix(T_obj).subs(t,args[3])
                    # for i in range(T_obj.shape[0]):
                    #     for j in range(T_obj.shape[1]):
                    #         # print "should sub: ",T_obj[i,j].subs(t,args[2])
                    #         T_obj_ti[i,j] = T_obj_ti[i,j].subs(t,args[2])
                    # for i in range(T_obj.shape[0]):
                    #     for j in range(T_obj.shape[1]):
                    #         # print "should sub: ",T_obj[i,j].subs(t,args[2])
                    #         # print "\n before sub: ", T_obj_tf[i,j]
                    #         T_obj_tf[i,j] = T_obj_tf[i,j].subs(t,args[3])
                    #         # print "after sub: ", T_obj_tf[i,j].subs(t,args[3])
                    T_obj = T_obj_tf - T_obj_ti
                    # print "\n T_obj_tf:",T_obj_tf," and T_obj_ti: ",T_obj_ti, "ti: ", args[2], "tf: ", args[3]
                return T_obj
            elif args[0] == "Tvect":
                tvect = []
                for ci in a_list.tolist():
                    tvect.append(sp.diff(p,ci[0]))
                    tvect_diff = []
                    for idx in range(len(tvect)): tvect_diff.append(sp.diff(tvect[idx],t))
                    tvect_ddiff = []
                    for idx in range(len(tvect)): tvect_ddiff.append(sp.diff(tvect[idx],t))
                    tvect_dddiff = []
                    for idx in range(len(tvect)): tvect_dddiff.append(sp.diff(tvect[idx],t))
                if args[1] == "pos":
                    T_obj = np.matrix(tvect).T
                elif args[1] == "vel":
                    T_obj = np.matrix(tvect_diff).T
                elif args[1] == "acc":
                    T_obj = np.matrix(tvect_ddiff).T
                elif args[1] == "jerk":
                    T_obj = np.matrix(tvect_dddiff).T
                return T_obj




def main():
    cls = QP_poly_cls()

    T = cls.poly_order(3,"Tvect","pos")
    print "positions for 3rd order poly:" ,T
    T = cls.poly_order(3,"Tvect","vel")
    print "positions for 3rd order poly:" ,T
    T = cls.poly_order(3,"Tvect","acc")
    print "positions for 3rd order poly:" ,T
    T = cls.poly_order(3,"Tvect","jerk")
    print "positions for 3rd order poly:" ,T


    """This Tmat is of form: a^t T a for coeff a = [a^n,a^n-1,...a0], which means these substituded with the Ti can be used to build the Q and diagonally stack them """
    T = cls.poly_order(3,"Tmat","vel")
    print "Tmat for vel, 3rd order poly:" ,T
    T = cls.poly_order(3,"Tmat","acc")
    print "Tmat for acc, 3rd order poly:" ,T
    T = cls.poly_order(3,"Tmat","jerk")
    print "Tmat for jerk, 3rd order poly:" ,T

    ti= 0.3
    T = cls.poly_order(3,"Tmat","vel",ti)
    print "Tmat for vel, 3rd order poly, sub ti= 0.3:" ,T

    ti= 0.6
    T = cls.poly_order(3,"Tmat","vel",ti)
    print "Tmat for vel, 3rd order poly, sub ti= 0.6:" ,T


    #produces different result that 0:0.3, but to be expected, your connecting at different points in traj, not required to start from zero
    ti= 0.3; tf = 0.6
    T = cls.poly_order(3,"Tmat","vel",ti,tf)
    print "Tmat for vel, 3rd order poly, sub ti= 0.3, tf = 0.6:" ,T





if __name__ == '__main__':
    main()