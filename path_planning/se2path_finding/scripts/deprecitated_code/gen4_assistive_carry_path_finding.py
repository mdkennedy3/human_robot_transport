#!/usr/bin/env python2
import rospy
import numpy as np
import numpy.matlib
import time
import tf
import math
import pcl 
#initial path planning
from ompl import base as ob
from ompl import geometric as og
from ompl import control, tools, util

#For plotting
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from se2path_finding.ts_math_operations import ts_math_oper
# from se2path_finding.ts_math_operations import plot_path_plan_lib #figure this out to clean up code

from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
# from geometry_msgs.msg import Point
from geometry_msgs.msg import Point32 as geomPoint
from sensor_msgs.msg import PointCloud

#For optimization: 
import scipy
from scipy import linalg 
from scipy import sparse
import cvxopt


class Plotting_debugging_rviz(object):
    def __init__(self,path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub):
        self.topic_name = "/vis_pts" #e.g. "/neck"
        self.marker_array_world_pts = PointCloud()
        self.marker_array_path_pts = MarkerArray()
        # self.marker_array_planes = MarkerArray()
        # self.vectors_plots = Marker()
        self.path_points_pub = path_points_pub
        self.cvx_path_points_pub  = cvx_path_points_pub
        self.obstacle_points_pub = obstacle_points_pub
        self.vector_pub =vector_pub
        self.planes_pub = planes_pub
        self.valid_occ_topic = valid_occ_topic
        self.scatter_idx = 0
        # self.planes_idx = 0
        self.planes_initialized = False

    def Plot_obstacles(self,cloud):
        self.marker_array_world_pts.header.frame_id = self.topic_name
        for idx in range(len(cloud)):
            pt = cloud[idx]
            point = geomPoint()
            point.x = pt[0]
            point.y = pt[1]
            point.z = pt[2]
            self.marker_array_world_pts.points.append(point)
        self.obstacle_points_pub.publish(self.marker_array_world_pts)

    def Plot_scatter_list(self,pts,scale,color):
        for idx in range(len(pts)):
            pt_curr = pts[idx]#; scale = 1.0; #color = [0,0,1] 
            scale_passed = [scale,scale,scale]
            marker_type = "sphere"
            marker = self.plot_marker_rviz(pt_curr,scale_passed,color,marker_type)
            marker.id = self.scatter_idx; self.scatter_idx = self.scatter_idx + 1;# print "scatter idx" ,self.scatter_idx
            self.marker_array_path_pts.markers.append(marker)
        self.path_points_pub.publish(self.marker_array_path_pts)
            #check type of pt, convert to list if necessary

    def Plot_cvx_path(self,pts,scale,color):
        for idx in range(len(pts)):
            pt_curr = pts[idx]#; scale = 1.0; #color = [0,0,1] 
            scale_passed = [scale,scale,scale]
            marker_type = "sphere"
            marker = self.plot_marker_rviz(pt_curr,scale_passed,color,marker_type)
            marker.id = self.scatter_idx; self.scatter_idx = self.scatter_idx + 1;# print "scatter idx" ,self.scatter_idx
            self.marker_array_path_pts.markers.append(marker)
        self.cvx_path_points_pub.publish(self.marker_array_path_pts)
            #check type of pt, convert to list if necessary


    def hat_map(self,vec):
        if type(vec) == np.matrixlib.defmatrix.matrix: v1 = vec.item(0); v2 = vec.item(1); v3 = vec.item(2)
        elif type(vec) == list: v1 = vec[0]; v2 = vec[1]; v3 = vec[2]
        S = np.matrix([[0,-v3,v2],[v3,0,-v1],[-v2,v1,0]])
        return S

    def rotation_given_two_vectors(self,v1,v2):
        #this function gets the rotation matrix given to vectors: rotation from v1 to v2
        # print "v1 and v2 passed to rotation finder: ", v1, v2
        v1_normed = np.divide(v1,np.linalg.norm(v1)); v2_normed = np.divide(v2,np.linalg.norm(v2));
        theta = math.acos(v1_normed.T*v2_normed) #angle between two vectors
        omega = self.hat_map(v1_normed)*v2_normed
        omega = np.divide(omega,np.linalg.norm(omega))
        omega_hat = self.hat_map(omega)
        R = np.matrix(np.eye(3)) + np.multiply(math.sin(theta),omega_hat) + np.multiply((1 - math.cos(theta)), omega_hat**2)
        # print "ROTATION CHECK: ", v2_normed, " and ", R*v1_normed
        #rotation method validated
        return R

    def Plot_planes(self,plane_equation,plane_centroid):
        # print "calc planes"
        # print "plane eqn: ", plane_equation, " its type: ", type(plane_equation)
        # print "plane_centroid: ", plane_centroid, " its type: ", type(plane_centroid)
        scale = 1.0
        v1 = np.matrix([scale,0.,0.]).T; ang2 =120.*math.pi/180.; ang3 =240.*math.pi/180.;  
        R2 = np.matrix([[math.cos(ang2),-math.sin(ang2),0.],[math.sin(ang2), math.cos(ang2),0.],[0.,0.,1.]]); R3 = np.matrix([[math.cos(ang3),-math.sin(ang3),0.],[math.sin(ang3), math.cos(ang3),0.],[0.,0.,1.]])
        v2 = R2*v1; v3 = R3*v1
        plane_tri_vector = np.vstack([v1.T,v2.T,v3.T]) #assumes plane is x,y plane and plane vector is z vector: [0,0,1]
        # print "plane tri-vector: ", plane_tri_vector
        init_vector = np.matrix([0.,0.,1.]).T
        final_vector = plane_equation[0,:3].T
        # print "init and final vectors: ", init_vector, final_vector
        R_z_to_vec = self.rotation_given_two_vectors(init_vector, final_vector)
        # print "rotation matrix: ", R_z_to_vec, "determinant: ", np.linalg.det(R_z_to_vec)
        v1_rotated = R_z_to_vec*v1; v2_rotated = R_z_to_vec*v2; v3_rotated = R_z_to_vec*v3; #rotate these vectors
        plane_tri_vector = np.vstack([v1_rotated.T,v2_rotated.T,v3_rotated.T])
        final_plane_vectors = plane_tri_vector + np.matlib.repmat(plane_centroid,3,1)
        # print "final plane vectors: ", plane_tri_vector#final_plane_vectors
        augmented_vectors =  np.hstack([final_plane_vectors,np.matrix(np.ones([3,1]))])
        color = [0.,0.,1.]
        self.Plot_scatter_list(plane_centroid.tolist(), 0.6,color) #to plot verticies of triangle
        #make triangular plane points: 
        pt1 =  geomPoint(); pt1.x = final_plane_vectors[0,0];pt1.y = final_plane_vectors[0,1];pt1.z = final_plane_vectors[0,2]
        pt2 =  geomPoint(); pt2.x = final_plane_vectors[1,0];pt2.y = final_plane_vectors[1,1];pt2.z = final_plane_vectors[1,2]
        pt3 =  geomPoint(); pt3.x = final_plane_vectors[2,0];pt3.y = final_plane_vectors[2,1];pt3.z = final_plane_vectors[2,2]
        
        if self.planes_initialized == False:
            self.plane_marker = Marker()
            self.plane_marker.header.frame_id = self.topic_name
            self.plane_marker.points.append(pt1);self.plane_marker.points.append(pt2);self.plane_marker.points.append(pt3)
            self.plane_marker.type = self.plane_marker.TRIANGLE_LIST
            color = [0.,1.,0.]
            self.plane_marker.color.a = 1.0 #may want to increase transparancy
            self.plane_marker.color.r = color[0]
            self.plane_marker.color.g = color[1]
            self.plane_marker.color.b = color[2]
            scale = [1.,1.,1.]
            self.plane_marker.scale.x = scale[0] #may be different though
            self.plane_marker.scale.y = scale[1]
            self.plane_marker.scale.z = scale[2]
            # self.plane_marker.id = self.planes_idx; self.planes_idx = self.planes_idx + 1; print "planes idx" ,self.planes_idx
            # self.marker_array_planes.markers.append(plane_marker)
            self.planes_initialized = True
        else:
            self.plane_marker.points.append(pt1);self.plane_marker.points.append(pt2);self.plane_marker.points.append(pt3)
        self.planes_pub.publish(self.plane_marker)
        # self.planes_pub.publish(self.marker_array_planes)

    def Plot_vectors(self,pt_start,pt_end):
        # diff = np.array(pt_end - pt_start)[0]
        pt_start = np.array(pt_start)
        pt_end = np.array(pt_end)
        # print "pts: ", pt_start, pt_end
        pt1 = geomPoint(); pt2 = geomPoint()
        pt1.x = pt_start[0]; pt1.y = pt_start[1]; pt1.z = pt_start[2]
        pt2.x = pt_end[0]; pt2.y = pt_end[1]; pt2.z = pt_end[2]

        marker_type = "vector"
        color = [1.,0.,0.]
        vector_marker =  Marker()
        vector_marker.header.frame_id =self.topic_name #= self.plot_marker_rviz(pt_start,diff,color,marker_type)
        vector_marker.points.append(pt1)
        vector_marker.points.append(pt2)
        scale = [.1,.2,.5]
        vector_marker.scale.x = scale[0] #may be different though
        vector_marker.scale.y = scale[1]
        vector_marker.scale.z = scale[2]
        vector_marker.color.a = 1.0  #transparency?
        vector_marker.color.r = color[0]
        vector_marker.color.g = color[1]
        vector_marker.color.b = color[2]
        # vector_marker.pose.position.x = pt_start[0]
        # vector_marker.pose.position.y = pt_start[1]
        # vector_marker.pose.position.z = pt_start[2]
        # vector_marker.pose.orientation.w = 1.0

        self.vector_pub.publish(vector_marker)

    def plot_marker_rviz(self,point,scale,color,m_type):
        # print "make points"
        marker = Marker()
        marker.header.frame_id =self.topic_name
        if m_type in "vector":
            marker.type = marker.ARROW
        else:
            marker.type = marker.SPHERE
            marker.pose.orientation.w = 1.0
        marker.action = marker.ADD
        # marker.lifetime = 400 #time its alive
        marker.scale.x = scale[0] #may be different though
        marker.scale.y = scale[1]
        marker.scale.z = scale[2]
        marker.color.a = 1.0  #transparency?
        marker.color.r = color[0]
        marker.color.g = color[1]
        marker.color.b = color[2]
        
        # print "the point: ", point
        marker.pose.position.x = point[0]
        marker.pose.position.y = point[1]
        marker.pose.position.z = point[2]

        return marker

#required for ompl distance sensing with self defined states
class MySE2(ob.SE2StateSpace):
    def distance(self,state1, state2):
        #print state1.getX(), state2.getX()
        # distxy = np.sqrt((state1.getX()-state2.getX())**2 + (state1.getY()-state2.getY())**2)
        # ang1 = state1.getYaw()
        # ang2 = state2.getYaw()
        ang1 = state1.getZ()
        ang2 = state2.getZ()
        # R1 = np.matrix([[math.cos(ang1), -math.sin(ang1)],[math.sin(ang1), math.cos(ang1)]])
        # R2 = np.matrix([[math.cos(ang2), -math.sin(ang2)],[math.sin(ang2), math.cos(ang2)]])
        # Rdiff = R2*R1.T
        # # ang_diff = ang2 - ang1
        # ang_diff = math.atan2(Rdiff[1,0],Rdiff[0,0])
        # final_dist = distxy + ang_diff 
        dist = np.sqrt((state1.getX()-state2.getX())**2 
                        + (state1.getY()-state2.getY())**2
                        + (ang2 - ang1)**2)
        return dist
class ompl_support(object):
    def isStateValid(self,state):
        # test_point = np.array([state.getX(),state.getY(),state.getYaw()],dtype=np.float32)
        test_point = np.array([state[0],state[1],state[2]],dtype=np.float32)
        # print "Test point: ",test_point," occupied bool: ",self.cloud.is_voxel_occupied_at_point(test_point)
        return not self.cloud.is_voxel_occupied_at_point(test_point)

    def convert_path_to_list(self,path):
        x = []; y = []; yaw = []
        path_list_first = {'x':[],'y':[],'yaw':[]}
        for idx in range(len(path.getStates())):
            x.append(path.getStates()[idx][0])
            y.append(path.getStates()[idx][1])
            yaw.append(path.getStates()[idx][2])
        # path_list_first = np.vstack(x,y,yaw)
        path_list_first['x'] = x
        path_list_first['y'] = y
        path_list_first['yaw'] = yaw
        return path_list_first

    def setup_ompl_problem_world_space(self,world, cloud,start_pose, goal_pose,time_to_solve):
        self.world = world
        self.cloud = cloud
        # occ_ind = np.where(np.array(self.world.occupied_cell_bool) == 1)
        # self.occupied_coord = self.world.world_discretized[occ_ind]
        # print "occupied coord before we get started: ", np.shape(self.occupied_coord), "type: ", type(self.occupied_coord)
        # self.occupied_coord_xy = self.occupied_coord[:,:2]
        # self.occupied_coord_yaw  = self.occupied_coord[:,2]

        bounds = ob.RealVectorBounds(3)
        bounds.setLow(0,world.extents['xmin'])
        bounds.setLow(1,world.extents['ymin'])
        bounds.setLow(2,world.extents['th_min'])


        # print "world extents: ", world.extents, "requested goal: ", goal_pose, "requested start: ", start_pose

        bounds.setHigh(0,world.extents['xmax'])
        bounds.setHigh(1,world.extents['ymax'])
        bounds.setHigh(2,world.extents['th_max'])

        # print "world bounds: ", world.extents

        #space = ob.SE2StateSpace()
        # space = ob.SE3StateSpace()
        # space = MySE2()
        space = ob.RealVectorStateSpace(3)
        space.setBounds(bounds)

        start = ob.State(space)
        # we can pick a random start state...
        # start.random()
        # ... or set specific values
        start[0] = np.double(start_pose.item(0))
        start[1] = np.double(start_pose.item(1))
        start[2] = np.double(start_pose.item(2))

        goal = ob.State(space)
        # we can pick a random goal state...
        # goal.random()
        goal[0] = goal_pose.item(0)
        goal[1] = goal_pose.item(1)
        goal[2] = goal_pose.item(2)

        # goal().setX(goal_pose.item(0))
        # goal().setY(goal_pose.item(1))
        # # goal().setYaw(goal_pose.item(2))
        # goal().setZ(goal_pose.item(2))

        # print "start: ", start_pose, "\n goal: ", goal_pose

        si = ob.SpaceInformation(space)
        si.setStateValidityChecker(ob.StateValidityCheckerFn(self.isStateValid))

        pd = ob.ProblemDefinition(si)
        pd.setStartAndGoalStates(start,goal)

        planner = og.RRTConnect(si)
        planner.setProblemDefinition(pd)
        planner.setup()

        solved = planner.solve(time_to_solve)

        # print "tried to solve:" , solved

        if solved:
            path = pd.getSolutionPath()
            # ps = og.PathSimplifier(si)
            # ps.smoothBSpline(path) #makes bspline path out of path variable
            path_list = self.convert_path_to_list(path)
        else:
            path_list = []

        #access elements of path_list with dict calls to 'x','y','yaw'
        return path_list


class virtual_obstacles(object):
    def __init__(self,x1,y1,x2,y2,world):
        #This class makes artificial obstacles given the bounding corners
        #the first corner (x1,y1), and opposite corner (x2,y2), let x1<x2 & y1<y2
        self.corner1 = [x1,y1]; self.corner2 = [x2,y2]
        # xstep = world.steps['x_steps']/2.; ystep = world.steps['y_steps']/2.  #was division by 3
        xstep = 2*world.steps['x_steps']; ystep = 2*world.steps['y_steps']
        x_num,x_rem = divmod(abs(x2-x1),xstep); y_num,y_rem = divmod(abs(y2-y1),ystep)
        x_coord = np.linspace(x1,x2,x_num); y_coord = np.linspace(y1,y2,y_num)  
        if x_rem != 0: np.append(x_coord,x2)
        if y_rem != 0: np.append(y_coord,y2)
        obstacle_side_1 = []; obstacle_side_2 = []; obstacle_side_3 = []; obstacle_side_4 = []
        for jdx in range(len(y_coord)): point = [x1, y_coord[jdx]]; obstacle_side_1.append(point)
        for jdx in range(len(y_coord)): point = [x2, y_coord[jdx]]; obstacle_side_2.append(point)
        for jdx in range(len(x_coord)): point = [x_coord[jdx],y1]; obstacle_side_3.append(point)
        for jdx in range(len(x_coord)): point = [x_coord[jdx],y2]; obstacle_side_4.append(point)
        pts = np.matrix(np.vstack([obstacle_side_1,obstacle_side_2,obstacle_side_3,obstacle_side_4]))

        obs_list = []
        for idx in x_coord:
            for jdx in y_coord:
                obs_list.append([idx,jdx])
        pts = np.matrix(obs_list)  #fill in obstacles densly (filter will handle data size)


        self.points = pts
        #obstacle points plot:
        # fig = plt.figure(); ax = fig.add_subplot(111);c = 'r'; m = 'o'; xs = pts[:,0].T; ys = pts[:,1].T; xs = xs.tolist()[0]; ys = ys.tolist()[0]; ax.scatter(xs, ys, c=c, marker=m); plt.show()
        #real sensor has 270 readings per sweep, that times number of convex hull verticies, then times the number of steps of theta
class world_info(object):
    def __init__(self,xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps):
        self.extents = {'xmin':xmin,'xmax':xmax,'ymin':ymin,'ymax':ymax,"th_min":theta_min,"th_max":theta_max}
        self.steps = {'x_steps':x_steps,'y_steps':y_steps, 'theta_steps':theta_steps}
        self.world_discretized = []
        self.occupied_cell_bool = []  #0 is free, 1 is occupied
        self.x_coord = []
        self.y_coord = []
        self.th_coord = []


class Assistive_Carry_Path_Finder(object):
    """docstring for Assistive_Carry_Path_Finder"""
    def __init__(self,path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub):
        # super(Assistive_Carry_Path_Finder, self).__init__()  %can be used to call functions from parent class:: class Child(Parent) to cause inheritence
        self.transported_object= []
        self.ts_math_oper = ts_math_oper.ts_math_oper_fncts()
        self.world = []
        self.start_pose = []
        self.goal_pose = []
        # self.points_pub = points_pub
        self.Plotting_debugging_rviz = Plotting_debugging_rviz(path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub)

    def generate_virtual_obstacles(self):
        #obstacles generated in code here. Must be run after world has been generated
        x1,y1,x2,y2 = -10,0,10,10;  virt_obs1 = virtual_obstacles(x1,y1,x2,y2,self.world);
        x1,y1,x2,y2 = -12,-20,10,-10; virt_obs2 = virtual_obstacles(x1,y1,x2,y2,self.world); 
        # print('corners for obs1:',x1,y1,' and ',x2,y2); print('corners for obs2:',x1,y1,' and ',x2,y2);
        # x1,y1,x2,y2 = x1,y1,x2,y2; virt_obs3 = virtual_obstacles(x1,y1,x2,y2,self.world.extents)
        self.virtual_obstacles = {'virt_obs1':virt_obs1,'virt_obs2':virt_obs2};#,'virt_obs3':virt_obs3}
        # pts = np.vstack([virt_obs1.points,virt_obs2.points])
        # fig = plt.figure(); ax = fig.add_subplot(111);c = 'r'; m = 'o'; xs = pts[:,0].T; ys = pts[:,1].T; xs = xs.tolist()[0]; ys = ys.tolist()[0]; ax.scatter(xs, ys, c=c, marker=m); plt.show()
        return self.virtual_obstacles

    def minkowski_object_points(self,show_mink_pts):
        #this function takes in sensed data and adds minkowski sum to points for given orientations of the object
        print('number of obstacles: ', len(self.virtual_obstacles))
        object_pts = np.vstack([self.virtual_obstacles['virt_obs1'].points,self.virtual_obstacles['virt_obs2'].points]) #These are all points
        # object_pts = np.vstack([self.virtual_obstacles['virt_obs1'].points])

        # test= self.virtual_obstacles['virt_obs1'].points
        minkowski_pts = []
        for idx in range(len(self.world.th_coord)):
            theta = self.world.th_coord[idx]
            R = np.matrix([[math.cos(theta), -math.sin(theta)],[math.sin(theta), math.cos(theta)]])
            vects_rotated = R*self.transported_object.vert.T
            for jdx in range(vects_rotated.shape[1]):
                vect_curr = vects_rotated[:,jdx]
                vects_rotated_mat = np.matlib.repmat(vect_curr.T,len(object_pts),1)
                theta_vect = np.matrix(np.matlib.repmat(theta,len(object_pts),1))
                mink_xy  = object_pts + vects_rotated_mat
                mink_pts = np.concatenate((mink_xy,theta_vect),axis=1)
                minkowski_pts.append(mink_pts)
        #long Nx2 matrix of points that are on top of sensed points
        self.minkowski_pts = np.vstack(minkowski_pts)
        #AT THIS POINT I SHOULD PLOT TO VISUALIZE OBSTACLE POINT, WORLD AND minkowski_pts
        if show_mink_pts:
            fig = plt.figure(); ax = fig.add_subplot(111, projection='3d') # ax = fig.add_subplot(111)
            #minkowski points
            c = 'b'; m = '^'; xs = self.minkowski_pts[:,0].T; xs = xs.tolist()[0]; ys = self.minkowski_pts[:,1].T; ys = ys.tolist()[0]; zs = self.minkowski_pts[:,2].T; zs = zs.tolist()[0]; ax.scatter(xs, ys, zs, c=c, marker=m);
            #obstacle points
            c = 'r'; m = 'o'; xs = object_pts[:,0].T; xs = xs.tolist()[0]; ys = object_pts[:,1].T; ys = ys.tolist()[0]; zs = np.zeros(len(xs)).tolist(); ax.scatter(xs, ys, zs, c=c, marker=m); ax.set_xlabel('X axis'); ax.set_ylabel('Y axis'); ax.set_zlabel('theta axis'); 
            plt.show()
        #NOTE: in real sensor case, all currently sensed points get the minkowski vectors
        return self.minkowski_pts

    def discretize_world(self,xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps):
        self.world = world_info(xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps)  #[xmin,xmax;ymin,ymax]  (angle is 0:2pi)
        #discretize the world, given the number of steps in x,y, theta
        th_num,th_rem = divmod(abs(theta_max-theta_min),theta_steps)
        th_coord = np.linspace(theta_min,theta_max,th_num)
        self.world.th_coord = th_coord

    def get_object_pose(self,object_start_pose,object_goal_pose):
        # Get the object pose, either from a msg or manually
        class transported_object(Assistive_Carry_Path_Finder):
            def __init__(self,object_start_pose,object_goal_pose):
                self.start_pose = object_start_pose
                self.pose = object_start_pose
                self.vert = np.matrix([[2,0],[-1,1],[-1,-1]]) #verticies of convex object when orientation is 0deg
                self.goal_pose = object_goal_pose
        self.transported_object= transported_object(object_start_pose,object_goal_pose)

        #Now make option to get pose in world frame using tf with mocap for real robot
        # object_pose_obj = self.ts_math_oper.TF_btw_frames(rob_tf,world_tf)
        # object_pose_vect = self.ts_math_oper.tf_to_mat(object_pose_obj)
        # R = self.ts_math_oper.quat_to_rot(object_pose_vect[3:])
        # th = math.atan2(R[1,0],R[0,0]) #atan2 takes in the signs, and this will produce value of theta between [-pi,pi]
        # self.object_pose = np.matrix([object_pose_vect[0],object_pose_vect[1],th])

    def get_occupied_points(self):
        ''' Given world cells, obstacle locations, define a list for 
        world points that assigns that point as either free or occupied, 
        based on whether an obstacle point is within cell distance of that cell '''
        #for artificial data collect points given from known map for real data, query the sensor to fill the map with percentage occupied. Make list of 'active' obstacle points
        active_obstacle_pts = self.minkowski_pts.tolist()  #With real sensor, this list of mink points is much smaller and is used to fill occ/unocc pts

        #Filter the points: 
        pts = np.float32(np.array(active_obstacle_pts))
        world_step_factor = np.float32(0.9)
        leaf = np.array([np.float32(world_step_factor*self.world.steps['x_steps']),
                         np.float32(world_step_factor*self.world.steps['y_steps']),
                         np.float32(world_step_factor*self.world.steps['theta_steps'])],dtype=np.float32)
        self.pc_init = pcl.PointCloud()
        self.pc_init.from_array(pts)
        vox_filt = self.pc_init.make_voxel_grid_filter()
        vox_filt.set_leaf_size(leaf[0],leaf[1],leaf[2])
        filtered_pts = vox_filt.filter()
        active_obstacle_pts = np.matrix(filtered_pts.to_array()).tolist()
        self.filtered_mink_pts = np.matrix(active_obstacle_pts)

        self.pc = pcl.PointCloud()
        self.pc.from_array(np.array(np.float32(active_obstacle_pts)))

        '''Until you can figure out '''
        # self.cloud = self.pc.make_octree(min(leaf))
        self.cloud = self.pc.make_octree(max(leaf))
        self.cloud.add_points_from_input_cloud()

    def obtain_initial_viable_path(self,show_initial_path_bool):
        # print('in this function obtain a path from method such as A*,RRT or manual, that QP expands on')
        #Path is defined as a list of verticies that contain the start and goal position at the top and bottom of the list respectively

        #RRT connect** (design with toy variables to make sure it works)
        self.ompl_support1 = ompl_support() #supporting functions 
        self.ompl_support2 = ompl_support() #supporting functions 
        self.ompl_support3 = ompl_support() #supporting functions 


        th_init = self.transported_object.goal_pose.item(2) #the inital theta (between -pi, pi)
        th_lower = th_init - 2*math.pi
        th_higher = th_init + 2*math.pi

        # if theta_select == "init":
        #     theta = th_init
        # elif theta_select == "lower":
        #     theta = th_lower
        # elif theta_select == "higher":
        #     theta = th_higher


        goal_init = self.transported_object.goal_pose
        goal_lower = np.matrix([self.transported_object.goal_pose.item(0),self.transported_object.goal_pose.item(1),th_lower])
        goal_higher = np.matrix([self.transported_object.goal_pose.item(0),self.transported_object.goal_pose.item(1),th_higher])

        time_to_solve = 10.0

        path_low = self.ompl_support1.setup_ompl_problem_world_space(self.world,self.cloud, self.transported_object.start_pose, goal_lower, time_to_solve)
        path_init = self.ompl_support2.setup_ompl_problem_world_space(self.world,self.cloud, self.transported_object.start_pose, goal_init, time_to_solve)
        path_high = self.ompl_support3.setup_ompl_problem_world_space(self.world,self.cloud, self.transported_object.start_pose, goal_higher, time_to_solve)
        #convert to list:
        # print "low path: ", path_low
        path_low = np.matrix(np.vstack([path_low['x'],path_low['y'],path_low['yaw']])).T
        path_init = np.matrix(np.vstack([path_init['x'],path_init['y'],path_init['yaw']])).T
        path_high = np.matrix(np.vstack([path_high['x'],path_high['y'],path_high['yaw']])).T
        # self.path_dict = self.ompl_support.setup_ompl_problem_world_space(self.world,self.cloud, self.transported_object.start_pose, self.transported_object.goal_pose, time_to_solve)

        #possibly make dict of three paths
        self.paths_to_all_possible_goals = {'inital_path':path_init, "lower_path":path_low, "higher_path":path_high} #inital path is for goal between -pi:pi, lower is -3pi:-pi, higher is pi:3pi
        try: 
            self.initial_path = path_init
        except:
            print "initial path didnt work"
            pass

        # occ_ind = np.where(np.array(self.world.occupied_cell_bool) == 1)
        # pts = self.world.world_discretized[occ_ind]

        # if show_initial_path_bool:
        #     fig = plt.figure(); ax = fig.add_subplot(111, projection='3d'); #ax2 = fig.add_subplot(122, projection='3d')
        #     #world pts: 
        #     c = 'r'; m = 'o'; xs = pts[:,0]; xs = xs.tolist(); ys = pts[:,1]; ys = ys.tolist(); zs = pts[:,2]; zs = zs.tolist(); ax.scatter(xs, ys, zs, c=c, marker=m)
        #     # c = 'r'; m = 'o'; xs = pts[:,0]; xs = xs.tolist(); ys = pts[:,1]; ys = ys.tolist(); zs = pts[:,2]; zs = zs.tolist(); ax2.scatter(xs, ys, zs, c=c, marker=m)
        #     #mink pts
        #     c = 'b'; m = '^'; xs = self.minkowski_pts[:,0].T; xs = xs.tolist()[0]; ys = self.minkowski_pts[:,1].T; ys = ys.tolist()[0]; zs = self.minkowski_pts[:,2].T; zs = zs.tolist()[0]; ax.scatter(xs, ys, zs, c=c, marker=m);
        #     #mink pts filtered: 
        #     c = 'g'; m = '^'; xs = self.filtered_mink_pts[:,0].tolist(); ys = self.filtered_mink_pts[:,1].tolist(); zs = self.filtered_mink_pts[:,2].tolist(); ax.scatter(xs,ys,zs,c=c,marker=m)
        #     #use later:
        #     c = 'r'; m = '*'
        #     xs = self.initial_path[:,0]; ys = self.initial_path[:,1]; zs = self.initial_path[:,2]
        #     # print "xs: ",xs
        #     ax.scatter(xs.tolist(),ys.tolist(),zs.tolist(),c=c,marker=m)

        #     ax.set_xlabel('X axis'); ax.set_ylabel('Y axis'); ax.set_zlabel('theta axis');# ax2.set_xlabel('X axis'); ax2.set_ylabel('Y axis'); ax2.set_zlabel('theta axis'); 
        #     plt.show()

    def convex_region_finder(self,chord_subdivision):
        # print "this segment finds the convex region for the path"
        #Iterate through the initial path, segmenting path and finding convex regions for each segment 
        self.cvx_plane_constraints_dict = dict()
        for path_key in self.paths_to_all_possible_goals.keys():
            initial_path = self.paths_to_all_possible_goals[path_key]

            cvx_plane_constraints = []
            #1. iterate through each segment
            for idx_path in range(len(initial_path)-1):
                path_pt1 = np.matrix(initial_path[idx_path])
                path_pt2 = np.matrix(initial_path[idx_path+1])
                #Get vector direction between the two segment points: 
                chord_vect = path_pt2 - path_pt1 #vector from path point 1 to 2
                chord_length = np.linalg.norm(chord_vect) #norm of vec
                chord_vect_normed = np.divide(chord_vect,chord_length) #normed vec

                #2. generate pts along chord to find convex planes
                xpos_chord = np.matrix(np.linspace(path_pt1.item(0),path_pt2.item(0),chord_subdivision))
                ypos_chord = np.matrix(np.linspace(path_pt1.item(1),path_pt2.item(1),chord_subdivision))
                zpos_chord = np.matrix(np.linspace(path_pt1.item(2),path_pt2.item(2),chord_subdivision))
                chord_pts = np.vstack([xpos_chord,ypos_chord,zpos_chord]).T

                #3. first rank the world points in order of closeness to the center of the 
                chord_centroid = path_pt1 + 0.5*chord_length*chord_vect_normed #=p1 + v12*0.5 where v12 = p2 - p1
                occupied_voxels = np.matrix(self.cloud.get_occupied_voxel_centers()) #Use occupied world voxels:::self.filtered_mink_pts  #Nx3
                repeated_chord_centroid = np.matlib.repmat(chord_centroid,np.shape(occupied_voxels)[0],1)
                diff = occupied_voxels - repeated_chord_centroid
                norm_diff_btw_mink_and_chord_cent = np.sqrt(np.sum(np.square(diff),1))
                # norm_diff_btw_mink_and_chord_cent = np.sum(np.square(diff),1)

                sorted_occ_vox_pts = np.argsort(norm_diff_btw_mink_and_chord_cent.T) #needs it to be a row to sort
                sorted_occ_vox_pts_list = sorted_occ_vox_pts.tolist()[0] #convert to list Nx1
                occupied_voxels_list = occupied_voxels.tolist() #convert to list
                norm_diff_btw_mink_and_chord_cent_list = norm_diff_btw_mink_and_chord_cent.tolist()
                world_bounding_box = self.get_world_boundaries()
                plane_dict={'Plane_pts':[], 'Plane_equations':[],'Plane_signs':[]}
                

                plane_dict['Plane_pts'] = np.array(np.vstack([path_pt1,path_pt2])) #store the chord points used to get the corresponding planes

                #4. Now, iterating through the world points, find the points on the chord closest and create hyperplanes which remove other obstacle points
                scale = .2; color = [1,0,0]; self.Plotting_debugging_rviz.Plot_scatter_list(plane_dict['Plane_pts'].tolist(),scale,color); self.Plotting_debugging_rviz.Plot_obstacles(occupied_voxels_list) #pass point cloud of obstacle
                vox_init_size = len(sorted_occ_vox_pts_list)
                while len(sorted_occ_vox_pts_list) > 0 and not rospy.is_shutdown():
                    #continue until all mink points are removed
                    min_idx = sorted_occ_vox_pts_list.pop(0) #remove that value from the ranked list
                    world_vox = occupied_voxels_list.pop(min_idx)
                    corresponding_norm_from_centroid = norm_diff_btw_mink_and_chord_cent_list.pop(min_idx) #keep this list for book keeping required to get new sorted at end of each while
                    #5. Using selected point find point on chord that is closest to it
                    #NEGATIVE BELOW SO ARROWS POINT OUT
                    diff_chord_pts_and_world_vox = -(chord_pts - np.matlib.repmat(world_vox,np.shape(chord_pts)[0],1)) #get the differences, these vectors go from teh world voxel to the chord points
                    norms_chord_pts_and_world_vox = np.sqrt(np.sum(np.square(diff_chord_pts_and_world_vox),1)) #get the norms of the above
                    sorted_norms_chord_pts_and_world_vox = np.array(np.argsort(norms_chord_pts_and_world_vox.T)) #get the order of norms to find the closest point
                    closest_chord_point = chord_pts[sorted_norms_chord_pts_and_world_vox[0][0]] #get closest point on chord to the world voxel of interest
                    vector_world_vox_to_chord_pt = diff_chord_pts_and_world_vox[sorted_norms_chord_pts_and_world_vox[0][0]] #retain the vector from the world voxel to this closest chord point

                    #6. Define the plane with normal vector pointing toward the chord, find the d value, and add this to plane list
                    d =  -np.matrix(vector_world_vox_to_chord_pt)*np.matrix(world_vox).T #this is the value of d, the sign of the chord direction must still be checked
                    plane_vector = np.hstack([ vector_world_vox_to_chord_pt ,d])
                    #7. Use this to find sign of all point on chord 
                    ''' (ensure they all have the same sign, else throw msg) '''
                    viable_sign = np.sign(plane_vector* np.vstack([closest_chord_point.T,np.matrix([1.])]))
                    plane_dict['Plane_equations'].append(plane_vector.tolist()[0])
                    plane_dict['Plane_signs'].append(viable_sign.tolist()[0])
                    occupied_voxels_list_mat = np.matrix(occupied_voxels_list)
                    #8. Use this to then exclude all world points that don't share the same sign (pop these indices); [repeating until list of world points is empty]
                    try: 
                        occupied_voxels_list_mat_plane_signs = np.sign(np.hstack([occupied_voxels_list_mat,np.matrix(np.ones([np.shape(occupied_voxels_list_mat)[0],1]))])*plane_vector.T)
                    except:
                        self.Plotting_debugging_rviz.Plot_vectors(world_vox,closest_chord_point.tolist()[0])
                        self.Plotting_debugging_rviz.Plot_planes(plane_vector,np.matrix(world_vox))
                        '''BETTER THING TO DO IS CHECK IF WORLD_VOX IS LAST ELEMENT OF OCCUPIED CELLS AND IF IT IS, THEN JUST STOP AND BREAK HERE '''
                        break
                    occupied_voxels_ind_valid_from_plane = np.where(occupied_voxels_list_mat_plane_signs == viable_sign.tolist()[0][0])[0]
                    if  occupied_voxels_ind_valid_from_plane.size == 0:
                        # print "finished!!!!!!!!!!!!!"
                        break
                    if len(occupied_voxels_ind_valid_from_plane.tolist()[0]) != 0:
                        occupied_voxels_list_temp = np.matrix(np.array(occupied_voxels_list)[occupied_voxels_ind_valid_from_plane])
                        norm_diff_btw_mink_and_chord_cent_list_temp = np.matrix(np.array(norm_diff_btw_mink_and_chord_cent_list)[occupied_voxels_ind_valid_from_plane])
                        occupied_voxels_list = occupied_voxels_list_temp.tolist()
                        sorted_occ_vox_pts_list_temp = np.argsort(norm_diff_btw_mink_and_chord_cent_list_temp) #needs it to be a row to sort
                        sorted_occ_vox_pts_list = sorted_occ_vox_pts_list_temp.tolist()[0] #convert to list Nx1
                        norm_diff_btw_mink_and_chord_cent_list = norm_diff_btw_mink_and_chord_cent_list_temp.T.tolist()
                    else:
                        self.Plotting_debugging_rviz.Plot_vectors(world_vox,closest_chord_point.tolist()[0])
                        self.Plotting_debugging_rviz.Plot_planes(plane_vector,np.matrix(world_vox))
                        break
                    #plot the planes: 
                    self.Plotting_debugging_rviz.Plot_vectors(world_vox,closest_chord_point.tolist()[0])
                    self.Plotting_debugging_rviz.Plot_planes(plane_vector,np.matrix(world_vox))
                    # rospy.sleep(.5) #just to visualize
                #9. Now add these plane info to larger list
                # print "what plane equations look like: ", plane_dict['Plane_equations'], "\n\n new world boundaries: ", world_bounding_box
                # stop
                plane_dict['Plane_equations'] = np.vstack([plane_dict['Plane_equations'], world_bounding_box]) 
                # print "previous plane signs: ", plane_dict['Plane_signs'], "\n budning signs", np.matrix(np.sign(world_bounding_box[:,-1])).T.tolist()
                plane_dict['Plane_signs'] = np.vstack([plane_dict['Plane_signs'], np.matrix(np.sign(world_bounding_box[:,-1])).T.tolist()])

                # print "world bounding box: ", world_bounding_box, "last col: ",np.array(world_bounding_box)[:,-1]
                # plane_dict['Plane_equations'] = world_bounding_box 
                # plane_dict['Plane_signs'] = np.sign(world_bounding_box[:,-1])


                # print "plane equations: ", plane_dict['Plane_equations']

                cvx_plane_constraints.append(plane_dict) #may have to put in list: append([plane_dict])
            self.cvx_plane_constraints_dict[path_key] = cvx_plane_constraints
        return self.cvx_plane_constraints_dict

    #For CVX Solving: 

    def get_world_boundaries(self):

        top = [0,0,1,-np.abs(self.world.extents["th_max"])]; #%top positive z, plane point of p1
        bottom = [0,0,-1,-np.abs(self.world.extents["th_min"])]; #%bottom, neg z, plane point of p2
        front =[1,0,0,-np.abs(self.world.extents["xmax"])]; #%front (positve x),  plane point of p3
        back = [-1,0,0,-np.abs(self.world.extents["xmin"])]; #%back (negative x),  plane point of p4
        right  = [0,1,0,-np.abs(self.world.extents["ymax"])]; #%right (pos y), plane point of p5
        left  = [0,-1,0,-np.abs(self.world.extents["ymin"])]; #%left (neg y), plane point of p6

        world_bounding_box = np.array([top,bottom,front,back,right,left])

        return world_bounding_box

    def poly_order_fnct(self,t,order,poly_type):
        pos_time = []
        vel_time = []
        acc_time = []
        jerk_time = []
        for idx in range(order):
            exp = (order - idx)
            pos_time.append(t**exp)
            vel_time.append(exp*t**(exp-1))
            if exp >= 2:
                acc_time.append(exp*(exp-1)*t**(exp-2))
            else:
                acc_time.append(0.)

            if exp >=3:
                jerk_time.append(exp*(exp-1)*(exp-2)*t**(exp-3))
            else:
                jerk_time.append(0.)

        pos_time.append(1.)
        vel_time.append(0.)
        acc_time.append(0.)
        jerk_time.append(0.)
        pos_time = scipy.sparse.block_diag([pos_time,pos_time,pos_time]).toarray()
        vel_time = scipy.sparse.block_diag([vel_time,vel_time,vel_time]).toarray()
        acc_time = scipy.sparse.block_diag([acc_time,acc_time,acc_time]).toarray()
        jerk_time = scipy.sparse.block_diag([jerk_time,jerk_time,jerk_time]).toarray()

        # print "pos var: ", pos_time,"vel_time", vel_time
        if poly_type in "pos":
            return pos_time
        elif poly_type in "vel":
            return vel_time
        elif poly_type in "acc":
            return acc_time
        elif poly_type in "jerk":
            return jerk_time
        elif poly_type in "all":
            #ie access with v[:,0] for pos 
            return [pos_time, vel_time, acc_time, jerk_time]

    def Get_Time_matricies(self, Ti_discrete, int_time_steps,poly_order):
        # ''' 
        # Ti_discrete: discrete times from 0:1 whose increment is scaled time to be at each chord point
        # int_time_steps: the division of times along each chord to be used for optimization set points (e.g. 100 steps)
        # '''
        self.T_hat_diff = []
        self.T_hat = []
        self.T_hat_3diff = []
        self.T_hat_2diff = []
        # self.int_time_steps = 100 #time points to use along a chord (these points will respect the constraints of cvx)
        for idx in range(len(Ti_discrete)-1):
            Ti_int = np.linspace(Ti_discrete[idx], Ti_discrete[idx+1], int_time_steps) #get time in this region

            self.Time_normed_list.append(Ti_int)

            T_diff_step = np.vstack(np.array([self.poly_order_fnct(t,poly_order,"vel") for t in Ti_int]))
            T_step = np.vstack(np.array([self.poly_order_fnct(t,poly_order,"pos") for t in Ti_int]))

            T_2diff_step = np.vstack(np.array([self.poly_order_fnct(t,poly_order,"acc") for t in Ti_int]))
            T_3diff_step = np.vstack(np.array([self.poly_order_fnct(t,poly_order,"jerk") for t in Ti_int]))

            #2nd diff
            if len(self.T_hat_2diff) == 0:
                self.T_hat_2diff = T_2diff_step
            else:
                self.T_hat_2diff = scipy.sparse.block_diag((self.T_hat_2diff,T_2diff_step))
                self.T_hat_2diff = self.T_hat_2diff.toarray()
            #3rd diff
            if len(self.T_hat_3diff) == 0:
                self.T_hat_3diff = T_3diff_step
            else:
                self.T_hat_3diff = scipy.sparse.block_diag((self.T_hat_3diff,T_3diff_step))
                self.T_hat_3diff = self.T_hat_3diff.toarray()


            if len(self.T_hat_diff) == 0:
                self.T_hat_diff = T_diff_step
            else:
                self.T_hat_diff = scipy.sparse.block_diag((self.T_hat_diff,T_diff_step))
                self.T_hat_diff = self.T_hat_diff.toarray()
            if len(self.T_hat) == 0:
                self.T_hat = T_step
            else:
                self.T_hat = scipy.sparse.block_diag((self.T_hat,T_step))
                self.T_hat = self.T_hat.toarray()
        # print "size of T_hat",np.shape(self.T_hat), "\n size of T_hat_diff", np.shape(self.T_hat_diff)
        return self.T_hat, self.T_hat_diff, self.T_hat_2diff, self.T_hat_3diff

    def QP_inequality_constraints(self,T_hat, chord_subdivision):
        #This finds the inequality constraint terms Ptilde * That *[coeff] <= bvect
        b_vect_constraint_inequality = []
        P_tilde_constraint_inequality = []
        self.cvx_plane_constraints
        for idx in range(len(self.cvx_plane_constraints)):
            P_tilde_component = []
            P_tilde_bvect_component = []
            for jdx in range(chord_subdivision):
                # P_tilde_component.append(np.vstack(self.cvx_plane_constraints[idx]["Plane_equations"])[:,:3])
                if len(P_tilde_component) == 0:
                    P_tilde_component = np.vstack(self.cvx_plane_constraints[idx]["Plane_equations"])[:,:3]
                else:
                    planes = np.vstack(self.cvx_plane_constraints[idx]["Plane_equations"])[:,:3]
                    P_tilde_component = scipy.sparse.block_diag([P_tilde_component,planes]).toarray()
                P_tilde_bvect_component.append(np.array(np.matrix(-np.vstack(self.cvx_plane_constraints[idx]["Plane_equations"])[:,3]).T))
                # print "\n\n\nPlane equation:",P_tilde_component, "\n plane sign: ", P_tilde_bvect_component
            b_vect_constraint_inequality.append(np.vstack(P_tilde_bvect_component))
            if len(P_tilde_constraint_inequality) == 0:
                # P_tilde_constraint_inequality = np.array(np.vstack(P_tilde_component))
                P_tilde_constraint_inequality = P_tilde_component
            else:
                P_tilde_constraint_inequality =  scipy.sparse.block_diag([P_tilde_constraint_inequality,np.array(np.vstack(P_tilde_component))]).toarray() 
            # print "\nfinal b_vect: ", len(b_vect_constraint_inequality),"\n final plane matrix", np.shape(P_tilde_constraint_inequality),"\n original convex plane number: ", np.shape(self.cvx_plane_constraints[idx]["Plane_equations"])

        b_vect_constraint_inequality = np.array(np.vstack(b_vect_constraint_inequality))
        # print "\nfinal b_vect: ", len(b_vect_constraint_inequality),"\n final plane matrix", np.shape(P_tilde_constraint_inequality),"\n original convex plane number: ", np.shape(self.cvx_plane_constraints[idx]["Plane_equations"])
        # print "\n number of chords ", len(self.cvx_plane_constraints)
        # print "\nTypes: ", type(b_vect_constraint_inequality), " types ", type(P_tilde_constraint_inequality)
        # print "the planes shape: ",np.shape(P_tilde_constraint_inequality)
        # print "shape of T_hat: ",np.shape(T_hat) 
        A_constraint_inequality = np.matrix(scipy.sparse.csr_matrix(P_tilde_constraint_inequality)*np.array(np.double(T_hat)))
        # print "shape of the A constraint: ", np.shape(A_constraint_inequality)
        return A_constraint_inequality, b_vect_constraint_inequality

    def Set_boundary_conditions(self,Ti_discrete, poly_order):
        # print "set boundary conditions given points"
        pos_vel_constraints = np.zeros([6*len(Ti_discrete), (poly_order+1)*3*(len(Ti_discrete)-1)]); pos_vel_constraints_bvect= []
        # print "initial shape of constraint matrix: ", np.shape(pos_vel_constraints)
        BC = {"Aeq":[],"beq":[]}
        Phat = [] #variable that holds list of point matricies with pos,vel for each critical point
        for idx in range(len(Ti_discrete)):
            jdx = idx - 1
            pos_t = self.poly_order_fnct(Ti_discrete[idx], poly_order,"pos")
            vel_t = self.poly_order_fnct(Ti_discrete[idx], poly_order,"vel")

            Phat = np.vstack([pos_t,vel_t])

            # zero_mat = np.zeros(np.array(pos_t.shape))
            zero_mat = np.zeros([6,pos_t.shape[1]])

            Pbar = []
            bvect = []
            if idx == 0:
                # Pbar = pos_t
                Pbar = Phat

                for kdx in range(len(Ti_discrete)-2):
                    print "dim of pbar: ", np.shape(Pbar), "dim of zmat: ", np.shape(zero_mat)
                    Pbar = np.hstack([Pbar, zero_mat])
                # bvect = np.matrix(self.cvx_plane_constraints[0]["Plane_pts"][0,:]).T.tolist()
                bvect = np.vstack([np.matrix(self.cvx_plane_constraints[0]["Plane_pts"][0,:]).T.tolist(), np.zeros([3,1])])
                # print "Pbar at 0: ", Pbar,"its shape: ",np.shape(Pbar), "\n bvect at 0: ", bvect
                # print "shape at 0: ", np.shape(Pbar), "\n first Pbar: ", Pbar
            elif idx == (len(Ti_discrete)-1):
                # print "idx: ", idx, " length of Ti:", len(Ti_discrete)
                # for kdx in range(len(Ti_discrete)-2):
                #     Pbar = np.hstack([Pbar, zero_mat])
                
                Pbar = zero_mat
                for kdx in range(1,len(Ti_discrete)-2):
                    Pbar = np.hstack([Pbar, zero_mat])
                # Pbar = np.hstack([Pbar,  pos_t])
                Pbar = np.hstack([Pbar,  Phat])

                # bvect = np.matrix(self.cvx_plane_constraints[-1]["Plane_pts"][1,:]).T.tolist()
                bvect = np.vstack([np.matrix(self.cvx_plane_constraints[-1]["Plane_pts"][1,:]).T.tolist(),np.zeros([3,1])])
                
                # print "final bvect: ", bvect
                # # print "Pbar at end: ", Pbar,"its shape: ",np.shape(Pbar), "\n bvect at end: ", bvect
                # print "shape at end: ", np.shape(Pbar)
            else:
                # Pbar = zero_mat
                # kdx = []
                # for kdx in range(len(range(1,idx))):
                #     print "entered first loop"," kdx: ", kdx
                #     Pbar = np.hstack([Pbar, zero_mat])
                # print "kdx", kdx, "range: ",range(1,idx)

                # # print "idx: ", idx
                # # stop_mid
                # Pbar = np.hstack([Pbar,np.hstack([pos_t, -pos_t])])
                # # stop_mid
                # for mdx in range(len(range(idx+1,len(Ti_discrete)-2))):
                #     print "entered second loop kdx: ", mdx, "idx +1: ", idx+1, " Ti -2:", len(Ti_discrete) -2
                #     Pbar = np.hstack([Pbar, zero_mat])
                # # print "Pbar at mid1: ", Pbar,"its shape: ",np.shape(Pbar), "\n bvect at mid1: ", bvect
                # # stop_mid
                # print "mdx", mdx, "range: ",range(idx+1,len(Ti_discrete)-2)

                # bvect= np.zeros([3,1])
                bvect= np.zeros([6,1])

                num_cols_left = np.shape(pos_t)[1]*len(range(0,idx-1))
                # zero_mat_left = np.zeros([3,num_cols_left])
                zero_mat_left = np.zeros([6,num_cols_left])
                
                # P_eq = np.hstack([pos_t, -pos_t])
                P_eq = np.hstack([Phat, -Phat])

                num_cols_right = np.shape(pos_t)[1]*len(range(idx+1,(len(Ti_discrete)-1)))
                # zero_mat_right = np.zeros([3,num_cols_right])
                zero_mat_right = np.zeros([6,num_cols_right])
                Pbar = np.hstack([zero_mat_left, P_eq, zero_mat_right])

            BC["Aeq"].append(Pbar)
            BC["beq"].append(bvect)

        BC["Aeq"] = np.vstack(BC["Aeq"])
        BC["beq"] = np.vstack(BC["beq"])



        #     Pbar = np.vstack([pos_t,vel_t])
        #     n= 6; m=3*(poly_order+1)
        #     if idx == 0 or idx == (len(Ti_discrete)-1):
        #         Phat.append(Pbar)
        #         if idx == 0:
        #             pos_vel_constraints[n*idx:n*idx+n,m*idx:m*idx+m] = Pbar
        #             point = np.matrix(self.cvx_plane_constraints[0]["Plane_pts"][0,:]).T.tolist()
        #             print "first point: ", point," time: ", Ti_discrete[idx]
        #             pos_vel_constraints_bvect.append(np.array( np.vstack([point,np.zeros([3,1])  ]) )) 
        #         else:
        #             pos_vel_constraints[n*idx:n*idx+n,m*jdx:m*jdx+m] = Pbar
        #             point = np.matrix(self.cvx_plane_constraints[-1]["Plane_pts"][1,:]).T.tolist()
        #             print "last point: ", point," time: ", Ti_discrete[idx]
        #             pos_vel_constraints_bvect.append(np.array( np.vstack([point,np.zeros([3,1])]) ))
        #     else:
        #         Phat.append(-Pbar)
        #         pos_vel_constraints[n*idx:n*idx+n,m*idx:m*idx+m] = -Pbar
        #         pos_vel_constraints[n*idx:n*idx+n,m*jdx:m*jdx+m] =  Pbar
        #         pos_vel_constraints_bvect.append(np.zeros([6,1]))
        # pos_vel_constraints_bvect = np.vstack(pos_vel_constraints_bvect)
        # # print "shape of W: ", np.shape(pos_vel_constraints), "\n bvect: ", np.shape(pos_vel_constraints_bvect)
        # BC["Aeq"] = pos_vel_constraints; BC["beq"] = pos_vel_constraints_bvect


        return BC

    def QP_Solve(self,H,f,A,b,Aeq,Beq):
        # """QP Solver
        # Args: H ,f ,A ,b ,Aeq ,Beq
        # Returns: 
        # x from x'Hx + fx subj to Ax <= b and Aeq x = b 
        # """
        # print "shape: H ", np.shape(H), "\n shape f: ", np.shape(f),"\n shape A: ", np.shape(A), "\n shape b: ", np.shape(b), "\n shape Aeq: ", np.shape(Aeq), "\n shape Beq:", np.shape(Beq)
        Q = cvxopt.matrix(H.toarray())
        p = cvxopt.matrix(f)
        G = cvxopt.matrix(A)
        bfinal =  np.matrix(b).astype(np.double)
        # print "b: ", bfinal 
        h = cvxopt.matrix(bfinal)
        # print "problem: ", b, "\n its type: ", type(b)
        # h = cvxopt.matrix(b)
        # print "h: ", h
        A = cvxopt.matrix(Aeq)
        b = cvxopt.matrix(Beq)
        sol = cvxopt.solvers.qp(Q, p, G, h, A, b)
        # sol = cvxopt.solvers.qp(Q, p)
        self.soln_coeff = np.matrix(sol['x'])
        # print "solution: ", self.soln_coeff, "its type: ", type(self.soln_coeff)
        #convert to list of point
        traj_points_list = np.matrix(self.T_hat)*self.soln_coeff
        cvx_traj_points = np.reshape(traj_points_list,[len(traj_points_list)/3,3])
        # print "trajectory points: ", cvx_traj_points
        return cvx_traj_points

    def convex_path_solver(self,chord_subdivision, poly_order,cvx_plane_constraints):
        print "this segment uses cvx to find the optimal path"
        #plane_dict={'Plane_pts':[], 'Plane_equations':[],'Plane_signs':[]}
        self.cvx_plane_constraints = cvx_plane_constraints
        print "\n number of chords ", len(self.cvx_plane_constraints)


        #1. Divide up segments in order to determine relative time requirements for each chord (using ratio of chord lengths to total distance)
        """note that a upper time limit on every point can be imposed by putting limit on velocity on each dimension for every point"""

        chord_segment_lengths = {"lengths":[], "total_length":[]}
        for idx in range(len(self.cvx_plane_constraints)):
            # print "\n\n pts: ", self.cvx_plane_constraints[idx]["Plane_pts"], "\n plane eqns: ", self.cvx_plane_constraints[idx]["Plane_equations"], "\n plane signs: ", self.cvx_plane_constraints[idx]["Plane_signs"]
            li = np.linalg.norm([self.cvx_plane_constraints[idx]["Plane_pts"][0] - self.cvx_plane_constraints[idx]["Plane_pts"][1]])
            # print "\n length of ith chord", li
            chord_segment_lengths["lengths"].append(li)
        chord_segment_lengths["lengths"] = np.array(np.double(chord_segment_lengths["lengths"]))
        chord_segment_lengths["total_length"] = np.sum(chord_segment_lengths["lengths"])
        # print "list of chord segment lengths: ", chord_segment_lengths
        

        self.Time_normed_list = []

        #2. With this segmentation build the time matricies (for n'th order polynomial and m chords this builds a (3*m)x(3*(n+1)) time matrix)
        Ti_discrete = [0]# np.divide(chord_segment_lengths["lengths"],chord_segment_lengths["total_length"])
        for idx in range(len(chord_segment_lengths["lengths"])):
            Ti_discrete.append(chord_segment_lengths["lengths"][idx]/chord_segment_lengths["total_length"] + Ti_discrete[-1])
        # print "Ti_discrete: ", Ti_discrete

        # T_hat, T_hat_diff = self.Get_Time_matricies(Ti_discrete, chord_subdivision, poly_order)
        T_hat, T_hat_diff, T_hat_2diff, T_hat_3diff = self.Get_Time_matricies(Ti_discrete, chord_subdivision, poly_order)
        # print "T_hat", np.shape(T_hat)


        # self.Time_normed_list = np.unique(np.hstack(self.Time_normed_list))
        self.Time_normed_list = np.hstack(self.Time_normed_list)

        #3. Get plane bounding matricies
        A_constraint_inequality, b_vect_constraint_inequality  = self.QP_inequality_constraints(T_hat, chord_subdivision)
        # print "A_constraint :",np.shape(A_constraint_inequality),"\n b constraint: ",np.shape(b_vect_constraint_inequality)
        
        """be careful about shape here, to get correct stacking and diagonalization """

        # b_vect_constraint_inequality.append(self.cvx_plane_constraints[idx]["Plane_equations"][:,3])
        # P_tilde_constraint_inequality.append(self.cvx_plane_constraints[idx]["Plane_equations"][:,:3])

        T_hat_mat = np.array(np.double(T_hat))
        T_hat_diff = np.array(np.double(T_hat_diff))
        T_hat_diff_left = scipy.sparse.csr_matrix(T_hat_diff.transpose())
        T_hat_diff_right =scipy.sparse.csr_matrix(T_hat_diff)



        T_hat_2diff_left = scipy.sparse.csr_matrix(self.T_hat_2diff.transpose())
        T_hat_2diff_right =scipy.sparse.csr_matrix(self.T_hat_2diff)

        T_hat_3diff_left = scipy.sparse.csr_matrix(self.T_hat_3diff.transpose())
        T_hat_3diff_right =scipy.sparse.csr_matrix(self.T_hat_3diff)


        # print "size of T hat diff: ", np.shape(self.T_hat_diff), "its type: ", type(self.T_hat_diff)
        H = T_hat_diff_left*T_hat_diff_right
        # H = T_hat_2diff_left*T_hat_2diff_right
        # H = T_hat_3diff_left*T_hat_3diff_right

        f = np.zeros([np.shape(H)[0],1])
        # P_tilde_left = scipy.sparse.csr_matrix(P_tilde_constraint_inequality)
        A =  A_constraint_inequality  #np.matrix(P_tilde_left*T_hat_mat)
        b = b_vect_constraint_inequality #-b_vect_constraint_inequality

        #now set equalities: 
        bound_cond = self.Set_boundary_conditions( Ti_discrete, poly_order)
        

        Aeq = bound_cond["Aeq"] #np.vstack([self.whole_traj_bc,self.diff_traj_bc])
        Beq = bound_cond["beq"]#np.vstack([self.whole_traj_bc_bvect,self.diff_traj_bc_bvect])
        # print "A equality: ", np.shape(Aeq), " beq: ", np.shape(Beq)
        
        self.cvx_traj_points = self.QP_Solve(H,f,A,b,Aeq,Beq)
        #put this on a message and plot in rviz (in different color)
        scale = .2; color = [1,1,1]; self.Plotting_debugging_rviz.Plot_cvx_path(self.cvx_traj_points.tolist(),scale,color)

        return self.cvx_traj_points, self.Time_normed_list


    def get_polynomial_arc_length(self,path_pts):
        print "the type is: ",type(path_pts)

        arc_length = 0
        for idx in range(np.shape(path_pts)[0]-1):
            init_pt = path_pts[idx,:]; next_pt = path_pts[idx+1,:]
            arc_length = arc_length + np.linalg.norm(next_pt-init_pt)
        return arc_length


def main():
    rospy.init_node('Path_finder')

    path_topic = "/path_points"; path_points_pub = rospy.Publisher(path_topic,MarkerArray,queue_size = 1)
    cvx_path_topic = "/cvx_path_points"; cvx_path_points_pub = rospy.Publisher(cvx_path_topic,MarkerArray,queue_size = 1)
    topic = "/points_visualization"; obstacle_points_pub = rospy.Publisher(topic,PointCloud,queue_size = 1)
    vector_topic = "/vectors_drawn"; vector_pub = rospy.Publisher(vector_topic,Marker,queue_size = 1)
    plane_topic = "/planes_drawn"; planes_pub = rospy.Publisher(plane_topic,Marker,queue_size = 1)
    # plane_topic = "/planes_drawn"; planes_pub = rospy.Publisher(plane_topic,MarkerArray,queue_size = 1)

    valid_occ_topic = "/valid_points_visualization"; valid_obstacle_points_pub = rospy.Publisher(valid_occ_topic,PointCloud,queue_size = 1)


    time_start = time.time()
    cls_obj = Assistive_Carry_Path_Finder(path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub)
    # xmin,xmax,ymin,ymax = -10,10,-20,20
    xmin,xmax,ymin,ymax = -30,30,-30,30
    theta_min,theta_max = -3*math.pi,3*math.pi
    x_steps,y_steps,theta_steps = 1,1,10*math.pi/180  #in meters and radians
    cls_obj.discretize_world(xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps)
    #define object to be carried:
    object_goal_pose = np.matrix([20,20,90*math.pi/180])
    object_start_pose = np.matrix([-20,-20,0])
    cls_obj.get_object_pose(object_start_pose,object_goal_pose)

    #define virtual obstacles:
    virt_obs = cls_obj.generate_virtual_obstacles()
    #Generate minkowski for static virtual objects (same for real sensed points, just adjusted and in the loop)
    show_mink_pts = False
    time_mink_start = time.time()
    mink_pts = cls_obj.minkowski_object_points(show_mink_pts)
    time_mink_end = time.time()
    mink_time_diff = time_mink_end - time_mink_start

    #get an initial path 
    cls_obj.get_occupied_points()
    show_initial_path_bool = False #display the initial math and minkowski pts
    """This function is the in, repeat 3x """
    cls_obj.obtain_initial_viable_path(show_initial_path_bool)


    #find the convex region of inital path, then find the optimal path
    cvx_time_start = time.time()
    chord_subdivision = 50 #subdivide chords into these many pts, should be 100
    """loop through dictionary elements here """
    cvx_constraint_dict = cls_obj.convex_region_finder(chord_subdivision)
    cvx_time_end = time.time(); cvx_time_diff= cvx_time_end- cvx_time_start

    '''3order poly for vel; 4th for acc; 5th for jerk; higher than specified may result in rank deficency '''

    # cls_obj.paths_to_all_possible_goals.keys()
    poly_order = 3#6 with acc, #7 with jerk #order of polynomials
    cvx_paths_dict = dict()
    for path_key in cvx_constraint_dict.keys():
        cvx_plane_constraints=cvx_constraint_dict[path_key]
        path, time_vect = cls_obj.convex_path_solver(chord_subdivision,poly_order, cvx_plane_constraints)
        path_arc_length = cls_obj.get_polynomial_arc_length(path)
        cvx_paths_dict[path_key] = [path, path_arc_length, time_vect]
        print "path arc length: ", path_arc_length

    time_stop = time.time()
    diff_time = time_stop - time_start
    print "code runs in: ", diff_time, " seconds","\n cvx code runs in: ", cvx_time_diff, "\n thats percentage time: ", cvx_time_diff/diff_time * 100.,"%\n","mink code time: ",mink_time_diff 
    #perhaps run a loop which runs the entire function recording my time for different paths, to get stats on my function performance

    #return the shortest path: 
    print "path lengths: ", np.array(cvx_paths_dict.values())[:,1], "minimum: ", np.argmin(np.array(cvx_paths_dict.values())[:,1]), "\n corresponding list: ", np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),:]

    list = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),0]
    times = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),2]
    list = np.array(list.tolist())
    print "list shape: ",np.shape(list), "list type: ", type(list)
    # list_unique = np.vstack({tuple(row) for row in list})
    time_unique, unique_ind  = np.unique(times, return_index = True)
    list_unique = list[unique_ind,:]
    print "length initially: ", len(list), "length finally: ", len(list_unique), "length of time list: ", len(time_unique), "\n unique list: ", list_unique, "\n unique times: ", time_unique

    trajectory = [list_unique, time_unique]

    # return list_unique, time_unique

if __name__ == '__main__':
    main()