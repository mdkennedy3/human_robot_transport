from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np


class obstacle():
    def __init__(self):
        self.pos = []
        self.angle = []
        self.verts = []

class mink_plots(object):
    def __init__(self):
        self.plt = plt.figure() #fig handle

    def make_obstacles(self):
        obs1 = obstacle(); obs2 = obstacle(); obs3 = obstacle()
        obs1.pos = []; obs1.angle = 0*(math.pi/180.); obs1.verts = [[0,0],[1,1]];
        obs2.pos = []; obs2.angle = 0*(math.pi/180.); obs2.verts = [];
        obs3.pos = []; obs3.angle = 0*(math.pi/180.); obs3.verts = [];







def main():
    print "main fnct"
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # X = np.arange(-5, 5, 0.25)
    # Y = np.arange(-5, 5, 0.25)
    # X, Y = np.meshgrid(X, Y)
    # R = np.sqrt(X**2 + Y**2)
    # Z = np.sin(R)
    # surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
    #                        linewidth=0, antialiased=False)
    # ax.set_zlim(-1.01, 1.01)
    # ax.zaxis.set_major_locator(LinearLocator(10))
    # ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    # fig.colorbar(surf, shrink=0.5, aspect=5)
    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(0, np.pi, 100)

    x = 10 * np.outer(np.cos(u), np.sin(v))
    y = 10 * np.outer(np.sin(u), np.sin(v))
    z = 10 * np.outer(np.ones(np.size(u)), np.cos(v))
    x = []
    ax.plot_surface(x, y, z, rstride=4, cstride=4, color='b')


    plt.show()


if __name__ == '__main__':
    main()
