#!/usr/bin/env python2
import time
import sys
import rospy
import numpy as np
import numpy.matlib
import time
import tf
import yaml
import math
import pcl 
#initial path planning
from ompl import base as ob
from ompl import geometric as og
from ompl import control, tools, util

#For plotting
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point32 as geomPoint
from geometry_msgs.msg import Point, Quaternion, PoseStamped
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import PointCloud


#For optimization: 
import scipy
from scipy import linalg 
from scipy import sparse
# import cvxopt

#for service
from se2path_finding.msg import ListMsg
from se2path_finding.srv import *
from se2path_finding.qp_solving_support import cvx_gurobi_solver, poly_qp
from se2path_finding.convex_corridor import cvx_corridor_alg
from se2path_finding.ompl_support import seed_path_ompl
from se2path_finding.path_plotting_lib import  plot_path_plan_lib

from se2path_finding.manual_paths import  manual_seed_paths #Manual_paths_cls(). *monroe1_g0, *monroe1_g_neg_pi,  *monroe2, *monroe3



class world_info(object):
    def __init__(self,xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps):
        self.extents = {'xmin':xmin,'xmax':xmax,'ymin':ymin,'ymax':ymax,"th_min":theta_min,"th_max":theta_max}
        self.steps = {'x_steps':x_steps,'y_steps':y_steps, 'theta_steps':theta_steps}
        self.world_discretized = []
        self.occupied_cell_bool = []  #0 is free, 1 is occupied
        self.x_coord = []
        self.y_coord = []
        self.th_coord = []


class Path_finder_class(object):
    def __init__(self,path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub):
        self.Plotting_debugging_rviz = plot_path_plan_lib.Plotting_debugging_rviz(path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub)
        self.world = []
        self.start_pose = []
        self.goal_pose = []
        self.obstacle_list = []
        self.obstacle_list_ompl = []

        """FOR MANUAL PATHS """
        self.manual_seed_paths = manual_seed_paths.Manual_paths_cls()
        self.manual_path = []
        # self.manual_path = self.manual_seed_paths.monroe1_g0()
        # self.manual_path = self.manual_seed_paths.monroe2()
        self.manual_path = self.manual_seed_paths.monroe3()
        # self.manual_path = self.manual_seed_paths.monroe_open()

    #Required, checked
    def discretize_world(self,xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps):
        self.world = world_info(xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps)  #[xmin,xmax;ymin,ymax]  (angle is 0:2pi)
        #discretize the world, given the number of steps in x,y, theta
        th_num,th_rem = divmod(abs(theta_max-theta_min),theta_steps)
        th_coord = np.linspace(theta_min,theta_max,th_num)
        self.world.th_coord = th_coord
    def get_object_pose(self,object_start_pose,object_goal_pose):
        # Get the object pose, either from a msg or manually
        class transported_object(object):
            def __init__(self,object_start_pose,object_goal_pose, graph_scale):
                self.start_pose = object_start_pose
                self.pose = object_start_pose
                scale = graph_scale
                # x1,x2,y1,y2 = -0.3048, 0.3048, -0.2032,0.2032
                x1,x2,y1,y2 = -0.3048*scale, 0.3048*scale, -0.2032*scale,0.2032*scale #cm or m
                # x1,x2,y1,y2 = -0.2048*scale, 0.2048*scale, -0.1032*scale,0.1032*scale #cm or m
                x_coord = np.linspace(np.min([x1,x2]),np.max([x1,x2]),3)
                y_coord = np.linspace(np.min([y1,y2]),np.max([y1,y2]),3)
                obj_side_1 = []; obj_side_2 = []; obj_side_3 = []; obj_side_4 = []
                for jdx in range(len(y_coord)): point = [x1, y_coord[jdx]]; obj_side_1.append(point)
                for jdx in range(len(y_coord)): point = [x2, y_coord[jdx]]; obj_side_2.append(point)
                for jdx in range(len(x_coord)): point = [x_coord[jdx],y1]; obj_side_3.append(point)
                for jdx in range(len(x_coord)): point = [x_coord[jdx],y2]; obj_side_4.append(point)

                self.vert = np.matrix(np.vstack([obj_side_1,obj_side_2,obj_side_3,obj_side_4]))
                self.goal_pose = object_goal_pose
        self.transported_object= transported_object(object_start_pose,object_goal_pose,self.graph_scale)
    def minkowski_object_points(self):
        #this function takes in sensed data and adds minkowski sum to points for given orientations of the object
        object_pts = np.array(self.obstacle_list.tolist())
        print "object_pts size:", sys.getsizeof(object_pts)
        minkowski_pts = []
        # mink_layered = []
        for idx in range(len(self.world.th_coord)):
            theta = self.world.th_coord[idx]
            R = np.matrix([[math.cos(theta), -math.sin(theta)],[math.sin(theta), math.cos(theta)]])
            vects_rotated = R*self.transported_object.vert.T
            # mink_xy_list_layer = []
            for jdx in range(vects_rotated.shape[1]):
                vect_curr = vects_rotated[:,jdx]
                vects_rotated_mat = np.matlib.repmat(vect_curr.T,len(object_pts),1)
                theta_vect = np.matrix(np.matlib.repmat(theta,len(object_pts),1))
                mink_xy  = object_pts + vects_rotated_mat
                mink_pts = np.concatenate((mink_xy,theta_vect),axis=1)
                mink_pts_obj = np.concatenate((object_pts,theta_vect),axis=1)
                minkowski_pts.append(mink_pts_obj)
                minkowski_pts.append(mink_pts)
            #     mink_xy_list_layer.append(mink_pts)
            # mink_layered.append(np.vstack(mink_xy_list_layer)) #for storing layers
        #long Nx2 matrix of points that are on top of sensed points
        # print "\n mink points: ", minkowski_pts
        self.minkowski_pts = np.vstack(minkowski_pts)
        # print "minkowski_pts size:", sys.getsizeof(self.minkowski_pts)


        """FOR OMPL USE A DENSER MAP """
        object_pts_ompl = np.array(self.obstacle_list_ompl.tolist())
        mink_layered = []
        for idx in range(len(self.world.th_coord)):
            theta = self.world.th_coord[idx]
            R = np.matrix([[math.cos(theta), -math.sin(theta)],[math.sin(theta), math.cos(theta)]])
            vects_rotated = R*self.transported_object.vert.T
            mink_xy_list_layer = []
            for jdx in range(vects_rotated.shape[1]):
                vect_curr = vects_rotated[:,jdx]
                vects_rotated_mat = np.matlib.repmat(vect_curr.T,len(object_pts_ompl),1)
                theta_vect = np.matrix(np.matlib.repmat(theta,len(object_pts_ompl),1))
                mink_xy  = object_pts_ompl + vects_rotated_mat
                mink_pts = np.concatenate((mink_xy,theta_vect),axis=1)
                mink_pts_obj = np.concatenate((object_pts_ompl,theta_vect),axis=1)
                mink_xy_list_layer.append(mink_pts)
            mink_layered.append(np.vstack(mink_xy_list_layer)) #for storing layers

        object_pts_ompl = None #clean
        self.obstacle_list_ompl = None
        self.mink_pts_layered = {"mink_xy":mink_layered, "theta":self.world.th_coord,"point_cloud":[]} #every element of mink_xy_list contains all occupied points and the corresponding theta
        print "minkowski_pts_layered size:", sys.getsizeof(self.mink_pts_layered)
        #AT THIS POINT I SHOULD PLOT TO VISUALIZE OBSTACLE POINT, WORLD AND minkowski_pts

        return self.minkowski_pts
    def get_occupied_points(self):
        ''' Given world cells, obstacle locations, define a list for 
        world points that assigns that point as either free or occupied, 
        based on whether an obstacle point is within cell distance of that cell '''
        #for artificial data collect points given from known map for real data, query the sensor to fill the map with percentage occupied. Make list of 'active' obstacle points
        start = time.time()
        #active_obstacle_pts = self.minkowski_pts.tolist()  #With real sensor, this list of mink points is much smaller and is used to fill occ/unocc pts
        
        #Filter the points: 
        # print 1, time.time() - start
        #pts = np.float32(np.array(active_obstacle_pts))
        world_step_factor = np.float32(0.9)
        leaf = np.array([np.float32(world_step_factor*self.world.steps['x_steps']),
                         np.float32(world_step_factor*self.world.steps['y_steps']),
                         np.float32(world_step_factor*self.world.steps['theta_steps'])],dtype=np.float32)
        # print 2, time.time() - start
        pc_init = pcl.PointCloud()
        pc_init.from_array(np.float32(self.minkowski_pts))
        # print 3, time.time() - start
        #filter points
        vox_filt = pc_init.make_voxel_grid_filter()
        vox_filt.set_leaf_size(leaf[0],leaf[1],leaf[2])
        filtered_pts = vox_filt.filter()
        # print 4, time.time() - start
        #final filtered points
        self.active_obstacle_pts = np.matrix(filtered_pts.to_array()).tolist()
        # self.filtered_mink_pts = np.matrix(active_obstacle_pts)
        # print 5, time.time() - start
        #send it back in point cloud
        pc = pcl.PointCloud()
        pc.from_array(np.array(np.float32(self.active_obstacle_pts)))
        # print 6, time.time() - start
        '''Until you can figure out '''
        self.cloud = pc.make_octree(max(leaf))
        # print 7, time.time() - start
        # self.cloud = self.pc.make_octree(leaf[2]) #use theta always? no or else collisions may occur, best is if the step size scale is comparable
        self.cloud.add_points_from_input_cloud()
        # print 8, time.time() - start

        pc_cloud_list = []
        for idx in range(len(self.mink_pts_layered["theta"])):
            # print 9, idx, time.time() - start
            pc_init_list = pcl.PointCloud()
            pc_init_list.from_list(self.mink_pts_layered["mink_xy"][idx].tolist())
            #filter points
            filtered_pts = pc_init_list
            cloud = filtered_pts.make_octree(max([leaf[0],leaf[1]]))
            cloud.add_points_from_input_cloud()
            pc_cloud_list.append(cloud)
        #Pass the below to the ompl to check which theta its closest too, then if xy are viable states to handle inability to make non cubic octrees
        self.mink_pts_layered["point_cloud"] = pc_cloud_list
        # print 'size of pc_cloud_list:',sys.getsizeof(pc_cloud_list)

    def obtain_initial_viable_path(self):
        # print('in this function obtain a path from method such as A*,RRT or manual, that QP expands on')
        #Path is defined as a list of verticies that contain the start and goal position at the top and bottom of the list respectively
        #RRT connect** (design with toy variables to make sure it works)
        
        """Uncomment """
        if len(self.manual_path) == 0:
            self.ompl_support1 = seed_path_ompl.ompl_support() #supporting functions 
            self.ompl_support2 = seed_path_ompl.ompl_support() #supporting functions 
            self.ompl_support3 = seed_path_ompl.ompl_support() #supporting functions 
            th_init = self.transported_object.goal_pose.item(2) #the inital theta (between -pi, pi)
            th_lower = th_init - 2*math.pi
            th_higher = th_init + 2*math.pi
            goal_init = self.transported_object.goal_pose
            goal_lower = np.matrix([self.transported_object.goal_pose.item(0),self.transported_object.goal_pose.item(1),th_lower])
            goal_higher = np.matrix([self.transported_object.goal_pose.item(0),self.transported_object.goal_pose.item(1),th_higher])
            time_to_solve = 10.0

            # self.mink_pts_layered

            # path_low = self.ompl_support1.setup_ompl_problem_world_space(self.world,self.cloud, self.transported_object.start_pose, goal_lower, time_to_solve)
            # path_init = self.ompl_support2.setup_ompl_problem_world_space(self.world,self.cloud, self.transported_object.start_pose, goal_init, time_to_solve)
            # path_high = self.ompl_support3.setup_ompl_problem_world_space(self.world,self.cloud, self.transported_object.start_pose, goal_higher, time_to_solve)
            path_low = self.ompl_support1.setup_ompl_problem_world_space(self.world,self.mink_pts_layered, self.transported_object.start_pose, goal_lower, time_to_solve)
            path_init = self.ompl_support2.setup_ompl_problem_world_space(self.world,self.mink_pts_layered, self.transported_object.start_pose, goal_init, time_to_solve)
            path_high = self.ompl_support3.setup_ompl_problem_world_space(self.world,self.mink_pts_layered, self.transported_object.start_pose, goal_higher, time_to_solve)
            #convert to list:
            path_low = np.matrix(np.vstack([path_low['x'],path_low['y'],path_low['yaw']])).T
            path_init = np.matrix(np.vstack([path_init['x'],path_init['y'],path_init['yaw']])).T
            path_high = np.matrix(np.vstack([path_high['x'],path_high['y'],path_high['yaw']])).T
        else:
            path_low = self.manual_path
            path_init = self.manual_path
            path_high = self.manual_path


        #possibly make dict of three paths
        self.paths_to_all_possible_goals = {'inital_path':path_init, "lower_path":path_low, "higher_path":path_high} #inital path is for goal between -pi:pi, lower is -3pi:-pi, higher is pi:3pi
        try: 
            self.initial_path = path_init
        except:
            print "initial path didnt work"
            pass
        

        #delete when finished
        self.mink_pts_layered = None
        return self.paths_to_all_possible_goals


    def get_world_boundaries(self):


        #this has been vetted, not only are they added for each chord, but the sign has also been triple checked
        frame_sign = -1 

        top = [0,0,1,frame_sign*np.abs(self.world.extents["th_max"])]; #%top positive z, plane point of p1
        bottom = [0,0,-1,frame_sign*np.abs(self.world.extents["th_min"])]; #%bottom, neg z, plane point of p2
        front =[1,0,0,frame_sign*np.abs(self.world.extents["xmax"])]; #%front (positve x),  plane point of p3
        back = [-1,0,0,frame_sign*np.abs(self.world.extents["xmin"])]; #%back (negative x),  plane point of p4
        right  = [0,1,0,frame_sign*np.abs(self.world.extents["ymax"])]; #%right (pos y), plane point of p5
        left  = [0,-1,0,frame_sign*np.abs(self.world.extents["ymin"])]; #%left (neg y), plane point of p6


        world_bounding_box = [top,bottom,front,back,right,left]

        return world_bounding_box


    def convex_region_finder(self):
        # print "this segment finds the convex region for the path"
        #Iterate through the initial path, segmenting path and finding convex regions for each segment 
        #find paths corridors
        # obstacle_pts = self.filtered_mink_pts.tolist(); obstacle_pts = np.double(obstacle_pts) #self.cloud.get_occupied_voxel_centers()
        # obstacle_pts = self.cloud.get_occupied_voxel_centers(); obstacle_pts = np.double(obstacle_pts) #self.cloud.get_occupied_voxel_centers()
        # print "saved minkowski obstacle pts"; # file_name = 'obs_minkowski.yaml'; # with open(file_name, 'w') as outfile:; #     outfile.write( yaml.dump(obstacle_pts, default_flow_style=True))

        self.cvx_plane_constraints_dict = dict()
        obstacle_pts = self.active_obstacle_pts; obstacle_pts = np.double(obstacle_pts) #self.cloud.get_occupied_voxel_centers()
        world_boundary = self.get_world_boundaries()
        self.Plotting_debugging_rviz.Plot_obstacles(obstacle_pts)

        if len(self.manual_path) > 0:

            path_pts = self.manual_path 
            cvx_plane_constraints = self.cvx_corridor_alg.Corridor_generator(path_pts,obstacle_pts, world_boundary)
            for path_key in self.paths_to_all_possible_goals.keys():
                #Find convex corridors
                plane_dict={'Plane_equations':[], 'Path_pts':[]}
                plane_dict["Plane_equations"] = cvx_plane_constraints
                plane_dict["Path_pts"] = path_pts
                self.cvx_plane_constraints_dict[path_key] = plane_dict
                print "found corridors for ", path_key

        else:

            for path_key in self.paths_to_all_possible_goals.keys():
                path_pts = self.paths_to_all_possible_goals[path_key]; path_pts = np.double(path_pts)
                #Find convex corridors
                plane_dict={'Plane_equations':[], 'Path_pts':[]}
                cvx_plane_constraints = self.cvx_corridor_alg.Corridor_generator(path_pts,obstacle_pts, world_boundary)
                plane_dict["Plane_equations"] = cvx_plane_constraints
                plane_dict["Path_pts"] = path_pts
                self.cvx_plane_constraints_dict[path_key] = plane_dict
                print "found corridors for ", path_key

        return self.cvx_plane_constraints_dict






    def hide(self):
        print "hide"
        # def convex_region_finder(self):
        #     chord_subdivision = self.chord_subdivision
        #     # print "this segment finds the convex region for the path"
        #     #Iterate through the initial path, segmenting path and finding convex regions for each segment 
        #     self.cvx_plane_constraints_dict = dict()
        #     for path_key in self.paths_to_all_possible_goals.keys():
        #         initial_path = self.paths_to_all_possible_goals[path_key]
        #         cvx_plane_constraints = []
        #         plane_dict_minor={'Plane_equations':[], 'Path_pts':[]}
        #         plane_eqn_list = []
        #         #1. iterate through each segment
        #         for idx_path in range(len(initial_path)-1):
        #             path_pt1 = np.matrix(initial_path[idx_path])
        #             path_pt2 = np.matrix(initial_path[idx_path+1])
        #             #Get vector direction between the two segment points: 
        #             chord_vect = path_pt2 - path_pt1 #vector from path point 1 to 2
        #             chord_length = np.linalg.norm(chord_vect) #norm of vec
        #             chord_vect_normed = np.divide(chord_vect,chord_length) #normed vec
        #             #2. generate pts along chord to find convex planes
        #             xpos_chord = np.matrix(np.linspace(path_pt1.item(0),path_pt2.item(0),chord_subdivision))
        #             ypos_chord = np.matrix(np.linspace(path_pt1.item(1),path_pt2.item(1),chord_subdivision))
        #             zpos_chord = np.matrix(np.linspace(path_pt1.item(2),path_pt2.item(2),chord_subdivision))
        #             chord_pts = np.vstack([xpos_chord,ypos_chord,zpos_chord]).T
        #             #3. first rank the world points in order of closeness to the center of the 
        #             chord_centroid = path_pt1 + 0.5*chord_length*chord_vect_normed #=p1 + v12*0.5 where v12 = p2 - p1
        #             # occupied_voxels = np.matrix(self.cloud.get_occupied_voxel_centers()) #Use occupied world voxels:::self.filtered_mink_pts  #Nx3
        #             occupied_voxels = np.matrix(self.active_obstacle_pts); #obstacle_pts = np.double(obstacle_pts) #self.cloud.get_occupied_voxel_centers()

        #             repeated_chord_centroid = np.matlib.repmat(chord_centroid,np.shape(occupied_voxels)[0],1)
        #             diff = occupied_voxels - repeated_chord_centroid
        #             norm_diff_btw_mink_and_chord_cent = np.sqrt(np.sum(np.square(diff),1))
        #             sorted_occ_vox_pts = np.argsort(norm_diff_btw_mink_and_chord_cent.T) #needs it to be a row to sort
        #             sorted_occ_vox_pts_list = sorted_occ_vox_pts.tolist()[0] #convert to list Nx1
        #             occupied_voxels_list = occupied_voxels.tolist() #convert to list
        #             norm_diff_btw_mink_and_chord_cent_list = norm_diff_btw_mink_and_chord_cent.tolist()
        #             world_bounding_box = self.get_world_boundaries()

        #             plane_dict={'Plane_pts':[], 'Plane_equations':[],'Plane_signs':[]}
        #             plane_dict['Plane_pts'] = np.array(np.vstack([path_pt1,path_pt2])) #store the chord points used to get the corresponding planes
        #             #4. Now, iterating through the world points, find the points on the chord closest and create hyperplanes which remove other obstacle points
        #             scale = .1; color = [1,0,0]; self.Plotting_debugging_rviz.Plot_scatter_list(plane_dict['Plane_pts'].tolist(),scale,color); self.Plotting_debugging_rviz.Plot_obstacles(occupied_voxels_list) #pass point cloud of obstacle
        #             vox_init_size = len(sorted_occ_vox_pts_list)
        #             while len(sorted_occ_vox_pts_list) > 0 and not rospy.is_shutdown():
        #                 #continue until all mink points are removed
        #                 min_idx = sorted_occ_vox_pts_list.pop(0) #remove that value from the ranked list
        #                 world_vox = occupied_voxels_list.pop(min_idx)
        #                 corresponding_norm_from_centroid = norm_diff_btw_mink_and_chord_cent_list.pop(min_idx) #keep this list for book keeping required to get new sorted at end of each while
        #                 #5. Using selected point find point on chord that is closest to it
        #                 #NEGATIVE BELOW SO ARROWS POINT OUT
        #                 diff_chord_pts_and_world_vox = -(chord_pts - np.matlib.repmat(world_vox,np.shape(chord_pts)[0],1)) #get the differences, these vectors go from teh world voxel to the chord points
        #                 norms_chord_pts_and_world_vox = np.sqrt(np.sum(np.square(diff_chord_pts_and_world_vox),1)) #get the norms of the above
        #                 sorted_norms_chord_pts_and_world_vox = np.array(np.argsort(norms_chord_pts_and_world_vox.T)) #get the order of norms to find the closest point
        #                 closest_chord_point = chord_pts[sorted_norms_chord_pts_and_world_vox[0][0]] #get closest point on chord to the world voxel of interest
        #                 vector_world_vox_to_chord_pt = diff_chord_pts_and_world_vox[sorted_norms_chord_pts_and_world_vox[0][0]] #retain the vector from the world voxel to this closest chord point
        #                 #6. Define the plane with normal vector pointing toward the chord, find the d value, and add this to plane list
        #                 d =  -np.matrix(vector_world_vox_to_chord_pt)*np.matrix(world_vox).T #this is the value of d, the sign of the chord direction must still be checked
        #                 plane_vector = np.hstack([ vector_world_vox_to_chord_pt ,d])
        #                 #7. Use this to find sign of all point on chord 
        #                 ''' (ensure they all have the same sign, else throw msg) '''
        #                 viable_sign = np.sign(plane_vector* np.vstack([closest_chord_point.T,np.matrix([1.])]))
        #                 plane_dict['Plane_equations'].append(plane_vector.tolist()[0])
        #                 plane_dict['Plane_signs'].append(viable_sign.tolist()[0])
        #                 occupied_voxels_list_mat = np.matrix(occupied_voxels_list)
        #                 #8. Use this to then exclude all world points that don't share the same sign (pop these indices); [repeating until list of world points is empty]
        #                 try: 
        #                     occupied_voxels_list_mat_plane_signs = np.sign(np.hstack([occupied_voxels_list_mat,np.matrix(np.ones([np.shape(occupied_voxels_list_mat)[0],1]))])*plane_vector.T)
        #                 except:
        #                     self.Plotting_debugging_rviz.Plot_vectors(world_vox,closest_chord_point.tolist()[0])
        #                     self.Plotting_debugging_rviz.Plot_planes(plane_vector,np.matrix(world_vox))
        #                     '''BETTER THING TO DO IS CHECK IF WORLD_VOX IS LAST ELEMENT OF OCCUPIED CELLS AND IF IT IS, THEN JUST STOP AND BREAK HERE '''
        #                     break
        #                 occupied_voxels_ind_valid_from_plane = np.where(occupied_voxels_list_mat_plane_signs == viable_sign.tolist()[0][0])[0]
        #                 if  occupied_voxels_ind_valid_from_plane.size == 0:
        #                     break
        #                 if len(occupied_voxels_ind_valid_from_plane.tolist()[0]) != 0:
        #                     occupied_voxels_list_temp = np.matrix(np.array(occupied_voxels_list)[occupied_voxels_ind_valid_from_plane])
        #                     norm_diff_btw_mink_and_chord_cent_list_temp = np.matrix(np.array(norm_diff_btw_mink_and_chord_cent_list)[occupied_voxels_ind_valid_from_plane])
        #                     occupied_voxels_list = occupied_voxels_list_temp.tolist()
        #                     sorted_occ_vox_pts_list_temp = np.argsort(norm_diff_btw_mink_and_chord_cent_list_temp) #needs it to be a row to sort
        #                     sorted_occ_vox_pts_list = sorted_occ_vox_pts_list_temp.tolist()[0] #convert to list Nx1
        #                     norm_diff_btw_mink_and_chord_cent_list = norm_diff_btw_mink_and_chord_cent_list_temp.T.tolist()
        #                 else:
        #                     self.Plotting_debugging_rviz.Plot_vectors(world_vox,closest_chord_point.tolist()[0])
        #                     self.Plotting_debugging_rviz.Plot_planes(plane_vector,np.matrix(world_vox))
        #                     break
        #                 #plot the planes: 
        #                 self.Plotting_debugging_rviz.Plot_vectors(world_vox,closest_chord_point.tolist()[0])
        #                 self.Plotting_debugging_rviz.Plot_planes(plane_vector,np.matrix(world_vox))
        #             #9. Now add these plane info to larger list
        #             plane_dict['Plane_equations'] = np.vstack([plane_dict['Plane_equations'], world_bounding_box]) 
        #             plane_eqn_list.append(plane_dict['Plane_equations'])
        #             # plane_dict['Plane_signs'] = np.vstack([plane_dict['Plane_signs'], np.matrix(np.sign(world_bounding_box[:,-1])).T.tolist()])
        #             cvx_plane_constraints.append(plane_dict) #may have to put in list: append([plane_dict])
        #         # self.cvx_plane_constraints_dict[path_key] = cvx_plane_constraints

        #         plane_dict_minor["Plane_equations"] = plane_eqn_list
        #         plane_dict_minor["Path_pts"] = initial_path
        #         self.cvx_plane_constraints_dict[path_key] = plane_dict_minor
                

        #     return self.cvx_plane_constraints_dict






    def convex_path_solver(self,chord_subdivision,poly_order, time_scale,cvx_plane_constraints, *iterate_paths):

        #Find smooth n'th order trajectories that respect the constraints
        spacial_dimension = 3 #perhaps make variable in the future
        
        # print "problem here: ", cvx_plane_constraints
        num_chords = len(cvx_plane_constraints["Path_pts"])-1
        if len(self.manual_path) == 0:
            path_pts = cvx_plane_constraints["Path_pts"]
        else:
            path_pts = self.manual_path

        if type(path_pts) == list:
            scale = .1; color = [1,0,0]; self.Plotting_debugging_rviz.Plot_scatter_list(path_pts,scale,color)
        else:
            scale = .1; color = [1,0,0]; self.Plotting_debugging_rviz.Plot_scatter_list(path_pts.tolist(),scale,color)

        ''' NEW ADDITION FOR SCALING WORLD TO HELP WITH TIME DISCRETIZATIONS'''
        th_scaling = 0.1/(10*math.pi/180.)




        chord_segment_lengths = {"lengths":[], "total_length":[]}#1. Divide up segments in order to determine relative time requirements for each chord (using ratio of chord lengths to total distance) 

        if len(iterate_paths) == 0:

            for idx in range(len(path_pts)-1):
                # li = np.linalg.norm([cvx_plane_constraints["Path_pts"][idx+1] - cvx_plane_constraints["Path_pts"][idx]])
                # li = np.sqrt(np.sum((cvx_plane_constraints["Path_pts"][idx+1][0] - cvx_plane_constraints["Path_pts"][idx][0])**2 + 
                #                     (cvx_plane_constraints["Path_pts"][idx+1][1] - cvx_plane_constraints["Path_pts"][idx][1])**2 + 
                #                     (th_scaling*cvx_plane_constraints["Path_pts"][idx+1][2] - th_scaling*cvx_plane_constraints["Path_pts"][idx][2])**2)) #linalg.norm([ - cvx_plane_constraints["Path_pts"][idx]])
                li = np.sqrt(np.sum((path_pts[idx+1][0] - path_pts[idx][0])**2 + 
                                    (path_pts[idx+1][1] - path_pts[idx][1])**2 + 
                                    (th_scaling*path_pts[idx+1][2] - th_scaling*path_pts[idx][2])**2)) #linalg.norm([ - cvx_plane_constraints["Path_pts"][idx]])
                chord_segment_lengths["lengths"].append(li)

        else:
            chord_segment_lengths["lengths"] = time_scale*iterate_paths[0]
            print "assigned new lengths: ", chord_segment_lengths["lengths"]




        chord_segment_lengths["lengths"] = np.array(np.double(chord_segment_lengths["lengths"])); chord_segment_lengths["total_length"] = np.sum(chord_segment_lengths["lengths"])
        #With this segmentation build the time matricies (for n'th order polynomial and m chords this builds a (3*m)x(3*(n+1)) time matrix)
        Ti_discrete = [0]# np.divide(chord_segment_lengths["lengths"],chord_segment_lengths["total_length"])
        for idx in range(len(chord_segment_lengths["lengths"])):
            Ti_discrete.append(chord_segment_lengths["lengths"][idx]/chord_segment_lengths["total_length"] + Ti_discrete[-1])



        '''CHANGED THE TIME SCALE COEFFICIENT HERE '''

        Ti_discrete = np.double(Ti_discrete)
        # Ti_discrete = np.dot(time_scale,Ti_discrete)
        print "\n\nnew Time discrete: ", Ti_discrete



        chord_sub_division = chord_subdivision #100
        inequality_constraints = cvx_plane_constraints["Plane_equations"]
        start_pose = self.start_pose #path_pts[0]
        goal_pose = self.goal_pose #path_pts[-1]
        optimization_order = 1 #derivative: 1:vel, 2:acc, 3:jerk, 4:snap etc
        #just debug alignment and such issues
        # print "inequality constraints passed: ", inequality_constraints

        print "\n\n elements passed to solver: " 
        print "\n spacial dim", spacial_dimension
        print "\n num_chords", num_chords
        print "\n poly order: ", poly_order
        print "\n Ti_discrete:", Ti_discrete
        print "\n chord_sub_division: ", chord_sub_division


        alpha_mat,Ti_0_to_t = self.cvx_gurobi_solver.Gurobi_solver(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose,optimization_order)
        print "solved trajectory"

        self.gurobi_soln = {"coeff":alpha_mat,"time_mat":Ti_0_to_t}
        # fig = plt.figure(); ax = fig.add_subplot(111, projection='3d')
        # ax.set_xlabel('X axis'); ax.set_ylabel('Y axis');ax.set_zlabel('Z axis')
        real_time = np.linspace(Ti_discrete[0],Ti_discrete[-1],200)
        # print "soln times: ", Ti_0_to_t,"soln coefficients: ", np.array(alpha_mat).shape
        traj_path_pts = []

        path_seg_lengths = np.zeros(len(Ti_discrete)-1)

        for time_idx in range(real_time.shape[0]):
            time_val = real_time[time_idx]; 
            coeff_idx = 0
            for idx in range(len(Ti_discrete)):
                if time_val > Ti_discrete[idx+1]:
                    coeff_idx = idx+1
                else:
                    break
            # Coeff_curr = np.matrix(self.gurobi_soln["coeff"][coeff_idx+1]).T
            Coeff_curr = np.matrix(self.gurobi_soln["coeff"][coeff_idx]).T

            dt = time_val - np.sum(self.gurobi_soln["time_mat"][:(coeff_idx+1)]);# print "\n\ntime",time_val,"current diff ", np.sum(self.gurobi_soln["time_mat"][:(coeff_idx+1)]), "dt: ", dt, " coeff: ", coeff_idx, "last idx of coeff list: ", len(alpha_mat)-1
            print "\nti_o_to_t", Ti_0_to_t, 
            Time_mat = self.cvx_gurobi_solver.obtain_time_matrix_gurobi(dt,poly_order,0,spacial_dimension)
            curr_pt = Time_mat*Coeff_curr
            traj_path_pts.append(curr_pt)

            if len(traj_path_pts) > 1:
                # print "\n\nthe coeff:", coeff_idx, "traj points: ", traj_path_pts
                # print "first traj point: ", np.array(traj_path_pts[-2]), "second: ",np.array(traj_path_pts[-1])
                path_seg_lengths[coeff_idx] =   np.sqrt(np.sum(  (traj_path_pts[-1].item(0) - traj_path_pts[-2].item(0))**2 + 
                                                (traj_path_pts[-1].item(1) - traj_path_pts[-2].item(1))**2 + 
                                                (th_scaling*(traj_path_pts[-1].item(2) - traj_path_pts[-2].item(2)))**2))
                # np.linalg.norm(traj_path_pts[-1] - traj_path_pts[-2])
                # print "curr length: ",path_seg_lengths 



        traj_path_pts = np.matrix(np.hstack(traj_path_pts)).T
        # print "ti_o_to_t", Ti_0_to_t, "\n final point: ", curr_pt, "goal pose: ", goal_pose, "\n final coefficients", Coeff_curr.T.tolist(), "\n\nall coefficients: ", alpha_mat
        print "\n number of elemnets in Ti_discrete: ", len(Ti_discrete), "number of elements in coeff list: ", len(alpha_mat), "\n Ti_discrete: ", Ti_discrete, "Ti_0_to_t", Ti_0_to_t
        # stop
        #plot the 3 trajectory paths
        # scale = 1.
        

        # scale = .1; color = [1,1,1]; self.Plotting_debugging_rviz.Plot_cvx_path(traj_path_pts.tolist(),scale,color)


        '''NEW: FIND THE DISCRETE PATH TIMES FOR ITERATIVE TIME DISCRETIZATION '''
        new_path_discrete = []
        Coeff_curr = np.matrix(self.gurobi_soln["coeff"][0]).T
        dt = 0
        Time_mat = self.cvx_gurobi_solver.obtain_time_matrix_gurobi(dt,poly_order,0,spacial_dimension)
        prev_pt = Time_mat*Coeff_curr
        # new_path_discrete.append(curr_pt)
        for time_idx in range(1,len(Ti_discrete)):
            t_curr = Ti_discrete[time_idx]
            Coeff_curr = np.matrix(self.gurobi_soln["coeff"][time_idx-1]).T
            dt = t_curr - np.sum(self.gurobi_soln["time_mat"][:(time_idx)])
            Time_mat = self.cvx_gurobi_solver.obtain_time_matrix_gurobi(dt,poly_order,0,spacial_dimension)
            curr_pt = Time_mat*Coeff_curr
            dist_diff = np.sqrt(np.sum(  (curr_pt.item(0) - prev_pt.item(0))**2 + 
                                                (curr_pt.item(1) - prev_pt.item(1))**2 + 
                                                (th_scaling*(curr_pt.item(2) -prev_pt.item(2)))**2))
            new_path_discrete.append(dist_diff)
            prev_pt = curr_pt

        path_seg_lengths = np.array(new_path_discrete)





        path = np.array(traj_path_pts)
        time_vect = real_time
        soln_coeff = alpha_mat 
        return  path, time_vect,Ti_discrete, soln_coeff, Ti_0_to_t, path_seg_lengths

    #Post Processing:
    def get_polynomial_arc_length(self,path_pts):
        print "the type is: ",type(path_pts)

        arc_length = 0
        for idx in range(np.shape(path_pts)[0]-1):
            init_pt = path_pts[idx,:]; next_pt = path_pts[idx+1,:]
            arc_length = arc_length + np.linalg.norm(next_pt-init_pt)
        return arc_length


    def path_input_function(self,req):
        # print "Variables: \n Start: ", req.start, "\n Goal: ", req.goal, "\n Obstacle list: ", req.obstacle_list, "\n World points: ", req.world

        self.cvx_corridor_alg = cvx_corridor_alg.CVX_corridor_decomp()#.Corridor_generator(Path,Obstacles)
        self.cvx_gurobi_solver = cvx_gurobi_solver.Gurobi_solver_cls()#.Gurobi_solver(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose)
        self.poly_qp = poly_qp.QP_poly_cls()#.poly_order


        theta_min,theta_max = -3*math.pi, 3*math.pi
        self.start_pose = np.matrix([req.start.x, req.start.y, req.start.theta])
        self.goal_pose = np.matrix([req.goal.x, req.goal.y, req.goal.theta])
        self.obstacle_list = np.matrix(np.vstack([req.obstacle_list.x,req.obstacle_list.y])).T
        self.obstacle_list_ompl = np.matrix(np.vstack([req.obstacle_list_ompl.x,req.obstacle_list_ompl.y])).T
        self.graph_scale = req.graph_scale
        time_scale = req.time_scale
        ########### required #################
        self.discretize_world(req.world.xmin,req.world.xmax,req.world.ymin,req.world.ymax,theta_min,theta_max, req.world.xsteps ,req.world.ysteps,req.world.thsteps)#xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps
        self.get_object_pose(self.start_pose,self.goal_pose)
        print "\nperforming minkowski sum"
        self.minkowski_object_points()
        print "\nget_ocupied_points"
        self.get_occupied_points()
        print "\nobtaining seed paths"
        self.obtain_initial_viable_path()
        ######################################

        chord_subdivision = int(req.chord_subdivision)# 50 #100 #subdivide chords into these many pts, should be 100
        self.chord_subdivision  = chord_subdivision
        #get cvx corridor planes for each path
        print "\nfinding convex corridors"
        cvx_constraint_dict = self.convex_region_finder()


        # cls_obj.paths_to_all_possible_goals.keys()
        poly_order = int(req.poly_order) #3 with vel, 5 with acc, #7 with jerk #order of polynomials
        cvx_paths_dict = dict()


        cvx_plane_constraints=cvx_constraint_dict[cvx_constraint_dict.keys()[0]]
        path, time_vect,Ti_discrete, soln_coeff, Ti_0_to_t, path_seg_lengths = self.convex_path_solver(chord_subdivision,poly_order,time_scale, cvx_plane_constraints)

        if len(self.manual_path) == 0:
            for path_key in cvx_constraint_dict.keys():
                cvx_plane_constraints=cvx_constraint_dict[path_key]
                print "optimizing for path ", path_key
                path_arc_length = 1e12
                path = []; time_vect = []; soln_coeff = []; Ti_discrete = []; Ti_0_to_t =[]; seed_path = []
                try:
                    path, time_vect,Ti_discrete, soln_coeff, Ti_0_to_t = self.convex_path_solver(chord_subdivision,poly_order,time_scale, cvx_plane_constraints)
                    scale = .1; color = [1,1,1]; self.Plotting_debugging_rviz.Plot_cvx_path(path.tolist(),scale,color)
                    path_arc_length = self.get_polynomial_arc_length(path)
                    seed_path = cls_obj.paths_to_all_possible_goals[path_key]
                except:
                    pass
                cvx_paths_dict[path_key] = [path, path_arc_length, time_vect,soln_coeff, Ti_discrete, Ti_0_to_t, seed_path]
                print "path arc length: ", path_arc_length
        else:
            '''First attempt and re-time parameterization '''
            # for k_idx in range(5):
            #     path, time_vect,Ti_discrete, soln_coeff, Ti_0_to_t, path_seg_lengths = self.convex_path_solver(chord_subdivision,poly_order,time_scale, cvx_plane_constraints, path_seg_lengths)
            path_arc_length = self.get_polynomial_arc_length(path)
            scale = .1; color = [1,1,1]; self.Plotting_debugging_rviz.Plot_cvx_path(path.tolist(),scale,color)
            seed_path = self.manual_path
            for path_key in cvx_constraint_dict.keys():
                cvx_plane_constraints=cvx_constraint_dict[path_key]
                print "optimizing for path ", path_key
                cvx_paths_dict[path_key] = [path, path_arc_length, time_vect,soln_coeff, Ti_discrete, Ti_0_to_t, seed_path]
                print "path arc length: ", path_arc_length





        list = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),0]
        times = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),2]
        final_coeff = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),3]
        final_Ti_discrete = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),4]
        final_Ti_0_to_t_discrete = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),5]
        final_seed_path = np.array(cvx_paths_dict.values())[np.argmin(np.array(cvx_paths_dict.values())[:,1]),6]
        list = np.array(list.tolist())
        # time_unique, unique_ind  = np.unique(times, return_index = True) #unneccessary
        # list_unique = list[unique_ind,:]
        list_unique = list
        time_unique = times

        trajectory = [list_unique, time_unique]

        print "\n\n\nfinal path: ", list_unique,  " times: ", times,# "\n Lengths compared: time:",len(times), ", path: ", len(list), "final path:", len(list_unique)

        # print "\n\n ROOT ISSUE (FINAL PATH): len of Ti_discrete:", len(final_Ti_discrete), "vs length of coeff matrix: ", len(final_coeff)


        # points = np.array([[1.,2.,3.],[4.,5.,6.],[7.,8.,9.]]) #list type
        point_list = []
        for idx in range(list_unique.shape[0]):
            poses = PoseStamped()
            poses.pose.position.x = list_unique[idx,0]
            poses.pose.position.y = list_unique[idx,1]
            poses.pose.position.z = 0.0
            poses.pose.orientation.w = np.cos(list_unique[idx,2]/2.)
            poses.pose.orientation.z = np.sin(list_unique[idx,2]/2.)
            poses.pose.orientation.x = 0.0
            poses.pose.orientation.y = 0.0
            poses.header.stamp = rospy.Time.from_sec(np.float(time_unique[idx]))  #note that this is 1x10^9 sec, divide by this value to get seconds and add this to inital time when executing trajectory goes form 
            point_list.append(poses)

        trajectory  = PathPlanResponse() #autogenerated type
        trajectory.traj.nav_traj.poses =  point_list #np.append([],np.ndarray.tolist(np.asarray(  traj_list )))
        trajectory.traj.normed_time = final_Ti_discrete
        trajectory.traj.optimized_time = final_Ti_0_to_t_discrete
        trajectory.traj.poly_order = poly_order



        print "\nfinal coefficients: ", final_coeff, "final optimized time: ", final_Ti_0_to_t_discrete

        coeff_row_list = []
        for jdx in range(len(final_coeff)):
            coeff_row_list.append(ListMsg())

        for jdx in range(len(final_coeff)):
            # idx = len(final_coeff) - (jdx+1)
            coeff_row_list[jdx].list_row = final_coeff[jdx]

        trajectory.traj.path_coeff = coeff_row_list


        seed_row_list = []
        for jdx in range(len(final_seed_path)):
            seed_row_list.append(ListMsg())
        for jdx in range(len(final_seed_path)):
            seed_row_list[jdx].list_row = final_seed_path[jdx]
        trajectory.traj.seed_path = seed_row_list




        print " \ntraj path coeff msg: ",trajectory.traj.path_coeff
        # stop
        print "Request was: start: ", req.start, ", goal", req.goal
        return trajectory





def Path_finder_server():
    rospy.init_node('path_finder_server')
    path_topic = "/path_points"; path_points_pub = rospy.Publisher(path_topic,MarkerArray,queue_size = 1)
    cvx_path_topic = "/cvx_path_points"; cvx_path_points_pub = rospy.Publisher(cvx_path_topic,MarkerArray,queue_size = 1)
    topic = "/points_visualization"; obstacle_points_pub = rospy.Publisher(topic,PointCloud,queue_size = 1)
    vector_topic = "/vectors_drawn"; vector_pub = rospy.Publisher(vector_topic,Marker,queue_size = 1)
    plane_topic = "/planes_drawn"; planes_pub = rospy.Publisher(plane_topic,Marker,queue_size = 1)
    valid_occ_topic = "/valid_points_visualization"; valid_obstacle_points_pub = rospy.Publisher(valid_occ_topic,PointCloud,queue_size = 1)


    time_start = time.time()
    cls_obj = Path_finder_class(path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub)





    #SERVICE
    s = rospy.Service('Path_finder', PathPlan, cls_obj.path_input_function)
    print "Ready to path plan"
    rospy.spin()






if __name__ == "__main__":
    Path_finder_server()
