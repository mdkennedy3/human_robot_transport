import rospy
import numpy as np
import rosbag 
import math
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True


class Plot_data(object):
    def __init__(self,bagname):
        self.traj = {"msg":[]}
        self.odom = {"msg":[],"time":[]}
        self.bag = rosbag.Bag(bagname)
        self.control_time_scale = 120


    def extract_odom(self):
        print "extracting odom"
        for topic, msg, t in self.bag.read_messages(topics=['odom']):
            self.odom["msg"].append(msg)
            self.odom["time"].append(t)



    def extract_traj(self):
        print "extracting traj"
        for topic, msg, t in self.bag.read_messages(topics=['traj']):
            self.traj["msg"].append(msg)
            print "traj collected at time: ", t
        #accessed with: cls_obj.traj["msg"][0].normed_time::se2path_finding/TrajMsg 


    def make_make_plots(self):
        #seed path
        self.seed_path = []
        for idx in range(len(self.traj["msg"][0].seed_path)):
            self.seed_path.append(list(self.traj["msg"][0].seed_path[idx].list_row) )
        self.seed_path = np.array(self.seed_path)
        #normed time
        self.Ti = np.array(self.traj["msg"][0].normed_time)  #make plot with these as they will be easy to see

        #R3 path
        self.R3_path = []
        for idx in range(len(self.traj["msg"][0].nav_traj.poses)):
            x = self.traj["msg"][0].nav_traj.poses[idx].pose.position.x
            y = self.traj["msg"][0].nav_traj.poses[idx].pose.position.y
            theta_path = np.arctan2(self.traj["msg"][0].nav_traj.poses[idx].pose.orientation.z , self.traj["msg"][0].nav_traj.poses[idx].pose.orientation.w)*2. 
            if theta_path > math.pi: 
                theta_path = theta_path - 2*math.pi
            elif theta_path < -math.pi: 
                theta_path = theta_path + 2*math.pi
            th = theta_path
            t = self.traj["msg"][0].nav_traj.poses[idx].header.stamp.to_sec()  #in seconds, this time is normed to 1
            path_pt = [x,y,th,t]
            self.R3_path.append(path_pt)
        self.R3_path = np.array(self.R3_path)


        #Actual Odometry
        self.Odom = [] #[x,y,th,t]
        t0_odom = self.odom["time"][0].to_sec()
        for idx in range(len(self.odom["msg"])):
            x = self.odom["msg"][idx].pose.pose.position.x
            y = self.odom["msg"][idx].pose.pose.position.y
            theta_odom = np.arctan2(self.odom["msg"][idx].pose.pose.orientation.z , self.odom["msg"][idx].pose.pose.orientation.w)*2. 
            if theta_odom > math.pi: 
                theta_odom = theta_odom - 2*math.pi
            elif theta_odom < -math.pi: 
                theta_odom = theta_odom + 2*math.pi
            th = theta_odom
            t = self.odom["time"][idx].to_sec() - t0_odom
            tnormed = t/self.control_time_scale
            path_pt = [x,y,th,t, tnormed]
            self.Odom.append(path_pt)
        self.Odom = np.array(self.Odom)



        fig1 = plt.figure(); ax1 = fig1.add_subplot(111)
        ax1.set_xlabel('$t$ (normed)', fontsize=20); ax1.set_ylabel('$X$ (m)', fontsize=25)
        c = 'b'; m = '-'; ax1.plot(self.Odom[:,4],self.Odom[:,0],'-b', label="$X_{real}$")
        c = 'r';m = 'o';  ax1.scatter(self.Ti,self.seed_path[:,0],c=c,marker=m, label="$X_{seed}$")
        c = 'g'; m = '*';  ax1.plot(self.R3_path[:,3],self.R3_path[:,0],'--g', label="$X_{des}$")

        ax1.legend(loc=2,fontsize=20)

        # plt.axis('equal')
        ax1.set_xticks(np.arange(0,1,.1))
        ax1.set_yticks(np.arange(0,7,1))
        plt.grid()
        # plt.show()



        fig2 = plt.figure(); ax2 = fig2.add_subplot(111)
        ax2.set_xlabel('$t$ (normed)', fontsize=20); ax2.set_ylabel('$Y$ (m)', fontsize=25)
        c = 'b'; m = '*'; ax2.plot(self.Odom[:,4],self.Odom[:,1],'-b', label="$Y_{real}$")
        c = 'r';m = 'o'; ax2.scatter(self.Ti,self.seed_path[:,1],c=c,marker=m, label="$Y_{seed}$")
        c = 'g'; m = '^'; ax2.plot(self.R3_path[:,3],self.R3_path[:,1],'--g', label="$Y_{des}$")
        # plt.axis('equal')
        ax2.set_xticks(np.arange(0,1,.1))
        ax2.set_yticks(np.arange(0,7,1))
        plt.grid()
        ax2.legend(loc=2,fontsize=20)
        # plt.show()



        fig3 = plt.figure(); ax3 = fig3.add_subplot(111)
        ax3.set_xlabel('$t$ (normed)', fontsize=20); ax3.set_ylabel('$\\theta (rad)$', fontsize=30)
        
        c = 'b'; m = '*'; ax3.plot(self.Odom[:,4],self.Odom[:,2],'-b', label="$\\theta_{real}$")
        c = 'r';m = 'o'; ax3.scatter(self.Ti,self.seed_path[:,2],c=c,marker=m, label="$\\theta_{seed}$")
        c = 'g'; m = '^';ax3.plot(self.R3_path[:,3],self.R3_path[:,2],'--g', label="$\\theta_{des}$")
        # plt.axis('equal')
        ax3.set_xticks(np.arange(0,1,.1))
        ax3.set_yticks(np.arange(-math.pi,math.pi,1))
        plt.grid()
        ax3.legend(loc=2,fontsize=20)




        plt.show()










def main():
    # rospy.init_node('bag_reader')
        
    bagname = "monroe1_g0.bag"
    cls_obj = Plot_data(bagname)
    cls_obj.extract_odom()
    cls_obj.extract_traj()
    cls_obj.make_make_plots()

    cls_obj.bag.close()

if __name__ == '__main__':
    main()