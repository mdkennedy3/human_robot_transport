#!/usr/bin/env python2
import rospy
import numpy as np
import numpy.matlib
import time
import tf
import math
import pcl 
import scipy
from scipy import linalg,sparse
from se2path_finding.qp_solving_support import cvx_gurobi_solver, poly_qp
from se2path_finding.convex_corridor import cvx_corridor_alg

# from cvxopt import matrix, solvers
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

class Pipline_class(object):
    def __init__(self):
        self.cvx_corridor_alg = cvx_corridor_alg.CVX_corridor_decomp()#.Corridor_generator(Path,Obstacles)
        #Path: (list/array) with k rows (for k pts in path), and n dimensions in each row
        #Obstacles: (list/array) with m rows (for m obstacle points), and n dim in each row
        # Output: 
        # Alist: (list of lists each containing rows with elements of n-dimensional hyperplanes)
        
        self.cvx_gurobi_solver = cvx_gurobi_solver.Gurobi_solver_cls()#.Gurobi_solver(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose)
        #INPUT: 
        #1. spacial_dimension (e.g. 3) 
        #2. num_chords (number of seed path chords) 
        #3. poly_order (polynomial order) 
        #4. Ti_discrete (list of times from 0:1) 
        #5. chord_sub_division (how many points to check in optimization for a single chord to be within inequality constraints)
        #6. inequality_constraints: list of lists containing rows with planes
        #7. start_pose (n dim start position) (assumes start velocity of zero)
        #8. goal_pose (n dim start position) (assumes goal velocity of zero)
        #OUTPUT:
        #1. alpha_mat, [[coeff_chord_1],..,[coeff_chord_(k-1)]] (list of k-1 coefficients) correlated to the time matrix Ti_0_to_t
        #2. Ti_0_to_t: [0,dt_1,dt_2,...] k values

        self.poly_qp = poly_qp.QP_poly_cls()#.poly_order
        # ti= 0.6# T = cls.poly_order(3,"Tmat","vel",ti)






    def generate_path_and_obstacles_two_dim(self):
        #generate path and obstacle points in 2D
        self.path_pts = [[-5,-6],[-5,-2],[0,2],[5,8]]; self.path_pts = np.double(self.path_pts)
        self.obstacle_pts = [[-7,-8],[-8,-2],[0,-2],[-5,2],[2.5,2],[0,4],[8,8],[5,10]]; self.obstacle_pts = np.double(self.obstacle_pts)
        self.obstacle_pts = [[-7,-8],[-8,-2],[0,-2],[-4,-2],[-5,2],[2.5,2],[0,4],[8,8],[5,10]]; self.obstacle_pts = np.double(self.obstacle_pts)

        # self.path_pts = [[-5,-6],[-5,6],[10,6]]; self.path_pts = np.double(self.path_pts)
        # self.obstacle_pts = [[-5,-8],[0,-6],[-10,-6],[0,4],[-10,4],[-10,8],[-5,8],[10,8],[10,4],[15,6],[-10,6]]; self.obstacle_pts = np.double(self.obstacle_pts)

        world_boundary = [[1,0,-15],[-1,0,-15],[0,1,-15],[0,-1,-15]]; world_boundary = np.double(world_boundary)

        #Find convex corridors
        Alist = self.cvx_corridor_alg.Corridor_generator(self.path_pts,self.obstacle_pts,world_boundary)

        #Find smooth n'th order trajectories that respect the constraints
        spacial_dimension = 2
        num_chords = int(len(self.path_pts)-1)

        '''KEEP MINIMIZING JERK IN MIND '''
        poly_order = 3
        Ti_discrete = np.double([0,0.33,.66,1]); self.Ti_discrete = Ti_discrete
        time_scale = 10  #0.01 for p^5
        Ti_discrete = np.double([0,0.33*time_scale,.66*time_scale,1*time_scale]); self.Ti_discrete = Ti_discrete
        # Ti_discrete = np.double([0,0.6,1]); self.Ti_discrete = Ti_discrete
        chord_sub_division = 100
        inequality_constraints = Alist
        start_pose = self.path_pts[0]
        goal_pose = self.path_pts[-1]
        optimization_order = 3 #derivative: 1:vel, 2:acc, 3:jerk, 4:snap etc
        #just debug alignment and such issues
        alpha_mat,Ti_0_to_t = self.cvx_gurobi_solver.Gurobi_solver(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose,optimization_order)
        print "solved trajectory"


        # print "\n\nPlanes: ", Alist , "len: ", len(Alist)
        # for idx in range(len(Alist)):
        #     print "\n Alist for this chord: ", Alist[idx]

        # print "\n\n"

        #Now plot the traj
        self.gurobi_soln = {"coeff":alpha_mat,"time_mat":Ti_0_to_t}

        # fig = plt.figure(); ax = fig.add_subplot(111, projection='3d')
        fig = plt.figure(); ax = fig.add_subplot(111)
        ax.set_xlabel('X axis'); ax.set_ylabel('Y axis')
        real_time = np.linspace(self.Ti_discrete[0],self.Ti_discrete[-1],200)
        # print "soln times: ", Ti_0_to_t,"soln coefficients: ", alpha_mat
        traj_path_pts = []
        for time_idx in range(real_time.shape[0]):
            time = real_time[time_idx]; 
            coeff_idx = 0
            for idx in range(len(self.Ti_discrete)):
                if time > self.Ti_discrete[idx+1]:
                    coeff_idx = idx+1
                else:
                    break
            Coeff_curr = np.matrix(self.gurobi_soln["coeff"][coeff_idx]).T
            dt = time - np.sum(self.gurobi_soln["time_mat"][:(coeff_idx+1)]); #print "\n\ntime",time,"current diff ", np.sum(self.gurobi_soln["time_mat"][:(coeff_idx+1)]), "dt: ", dt, " coeff: ", coeff_idx

            Time_mat = self.cvx_gurobi_solver.obtain_time_matrix_gurobi(dt,poly_order,0,spacial_dimension)
            curr_pt = Time_mat*Coeff_curr
            
            traj_path_pts.append(curr_pt)
        # print "\n\ntime",time,"current diff ", np.sum(self.gurobi_soln["time_mat"][:(coeff_idx+1)]), "dt: ", dt, " coeff: ", coeff_idx
        # print "\ncurr pts: ",curr_pt, "\n its Tmat: ", Time_mat, "\n its coeff: ", Coeff_curr, "\n all coeffs: ", self.gurobi_soln["coeff"], "\n the index that called it: ",coeff_idx-1
        traj_path_pts = np.matrix(np.hstack(traj_path_pts)).T
        print "\n number of elemnets in Ti_discrete: ", len(Ti_discrete), "number of elements in coeff list: ", len(alpha_mat), "\n Ti_discrete: ", Ti_discrete, "Ti_0_to_t", Ti_0_to_t
        c = 'b'; m = '*'
        ax.scatter(self.path_pts[:,0],self.path_pts[:,1],c=c,marker=m)

        c = 'g'; m = '^'
        ax.scatter(self.obstacle_pts[:,0],self.obstacle_pts[:,1],c=c,marker=m)


        xs_set = traj_path_pts[:,0]; ys_set = traj_path_pts[:,1]
        c = 'r';m = 'o'; ax.scatter(xs_set.tolist(),ys_set.tolist(),c=c,marker=m)
        ax.set_xlabel('X axis'); ax.set_ylabel('Y axis')
        plt.axis('equal')
        ax.set_xticks(np.arange(-20,20,1))
        ax.set_yticks(np.arange(-20,20,1))
        plt.grid()
        # plt.show()




    def generate_path_and_obstacles_three_dim(self):
        #generate path and obstacle points in 3D
        # self.path_pts = [[-10,10,-10],[-10,10,10],[10,10,10],[10,-10,10]];self.path_pts = np.double(self.path_pts)
        # # self.obstacle_pts = [[-10,5,0],[0,10,0],[10,10,5],[5,5,10],[10,0,15],[10,0,0]];self.obstacle_pts = np.double(self.obstacle_pts)
        # self.obstacle_pts = [[-10,5,0],[0,10,0],[10,10,5],[5,5,10],[10,0,15],[10,0,0],[-5,9,10],[-5,9,0],[-5,11,10],[-5,11,0],[-11,9,10],[-11,9,0],[-9,11,10],[-9,11,0]];self.obstacle_pts = np.double(self.obstacle_pts)

        self.path_pts = [[-10,10,-10],[-10,10,10],[10,10,10],[10,-10,10]]; self.path_pts = np.double(self.path_pts)
        self.obstacle_pts = [[-11,9,-10],
                             [-9,9,-10],
                             [-9,11,-10],
                             [-11,11,-10],
                             [-11,9,5],
                             [-9,9,5],

                             [-9,11,5],
                             [-11,11,5],
                             [-11,9,11],
                             [-9,9,11],
                             [-9,11,11],
                             [-11,11,11],

                             [9.5,9.5,9.5],
                             [10.5,9,9],
                             [11,11,9],
                             [9,11,9],
                             [9.5,9.5,10.5],
                             [10.5,9.5,10.1],
                             
                             [11,11,11],
                             [9,11,11],
                             [9,-10,9],
                             [11,-10,9],
                             [11,-10,11],
                             [9,-10,11],
                             
                             [-10,10,11],
                             [-10,9,-10],
                             [-10,11,-10],
                             [-9,10,-10],
                             [-11,10,-10],
                             [-9,9,10],
                             
                             [-9,10,9],
                             [-9,10,11],
                             [-9,11,10],
                             [10,10.5,10]]; self.obstacle_pts = np.double(self.obstacle_pts)


        world_boundary = [[1,0,0,-18],[-1,0,0,-18],[0,1,0,-18],[0,-1,0,-18],[0,0,1,-18],[0,0,-1,-18]]; world_boundary = np.double(world_boundary)

        #Find convex corridors
        Alist = self.cvx_corridor_alg.Corridor_generator(self.path_pts,self.obstacle_pts, world_boundary)

        #Find smooth n'th order trajectories that respect the constraints
        spacial_dimension = 3
        num_chords = int(len(self.path_pts)-1)
        poly_order = 3  #3rd works okay
        time_scale = 1  #0.01 for p^5
        Ti_discrete = np.double([0,0.33*time_scale,.66*time_scale,1*time_scale]); self.Ti_discrete = Ti_discrete
        chord_sub_division = 100
        inequality_constraints = Alist
        start_pose = self.path_pts[0]
        goal_pose = self.path_pts[-1]
        optimization_order = 1 #derivative: 1:vel, 2:acc, 3:jerk, 4:snap etc
        #just debug alignment and such issues
        alpha_mat,Ti_0_to_t = self.cvx_gurobi_solver.Gurobi_solver(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose,optimization_order)
        print "solved trajectory"


        self.gurobi_soln = {"coeff":alpha_mat,"time_mat":Ti_0_to_t}
        fig = plt.figure(); ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel('X axis'); ax.set_ylabel('Y axis');ax.set_zlabel('Z axis')
        real_time = np.linspace(self.Ti_discrete[0],self.Ti_discrete[-1],200)
        print "soln times: ", Ti_0_to_t,"soln coefficients: ", np.array(alpha_mat).shape
        traj_path_pts = []
        for time_idx in range(real_time.shape[0]):
            time = real_time[time_idx]; 
            coeff_idx = 0
            for idx in range(len(self.Ti_discrete)):
                if time > self.Ti_discrete[idx+1]:
                    coeff_idx = idx+1
                else:
                    break
            Coeff_curr = np.matrix(self.gurobi_soln["coeff"][coeff_idx]).T
            dt = time - np.sum(self.gurobi_soln["time_mat"][:(coeff_idx+1)]); #print "\n\ntime",time,"current diff ", np.sum(self.gurobi_soln["time_mat"][:(coeff_idx+1)]), "dt: ", dt, " coeff: ", coeff_idx
            Time_mat = self.cvx_gurobi_solver.obtain_time_matrix_gurobi(dt,poly_order,0,spacial_dimension)
            print "Time mat shape: ", Time_mat.shape, " coeff shape: ", Coeff_curr.shape
            curr_pt = Time_mat*Coeff_curr
            traj_path_pts.append(curr_pt)
        # print "\n\ntime",time,"current diff ", np.sum(self.gurobi_soln["time_mat"][:(coeff_idx+1)]), "dt: ", dt, " coeff: ", coeff_idx
        # print "\ncurr pts: ",curr_pt, "\n its Tmat: ", Time_mat, "\n its coeff: ", Coeff_curr, "\n all coeffs: ", self.gurobi_soln["coeff"], "\n the index that called it: ",coeff_idx-1
        traj_path_pts = np.matrix(np.hstack(traj_path_pts)).T
        print "\n number of elemnets in Ti_discrete: ", len(Ti_discrete), "number of elements in coeff list: ", len(alpha_mat), "\n Ti_discrete: ", Ti_discrete, "Ti_0_to_t", Ti_0_to_t
        c = 'b'; m = '*'
        ax.scatter(self.path_pts[:,0],self.path_pts[:,1],self.path_pts[:,2],c=c,marker=m)

        c = 'g'; m = '^'
        ax.scatter(self.obstacle_pts[:,0],self.obstacle_pts[:,1],self.obstacle_pts[:,2],c=c,marker=m)


        xs_set = traj_path_pts[:,0]; ys_set = traj_path_pts[:,1]; zs_set = traj_path_pts[:,2]
        c = 'r';m = 'o'; ax.scatter(xs_set.tolist(),ys_set.tolist(),zs_set.tolist(),c=c,marker=m)
        plt.axis('equal')
        ax.set_xticks(np.arange(-20,20,1))
        ax.set_yticks(np.arange(-20,20,1))
        ax.set_zticks(np.arange(-20,20,1))
        plt.grid()
        print "the list of planes for every chord: ", Alist
        for idx in range(len(Alist)):
            print "\n chord: ",idx,"its Alist: ",Alist[idx]
        plt.show()





def main(): 
    print "this is a simple test to test the integration of the lastest solver and gurobi qp solver"
    cls_obj = Pipline_class()
    cls_obj.generate_path_and_obstacles_two_dim()
    cls_obj.generate_path_and_obstacles_three_dim()

if __name__ == "__main__":
    main()

