#!/usr/bin/env python2
import rospy
import sys
import numpy as np
import math
import numpy.matlib
import time
import tf
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
# from geometry_msgs.msg import Point
from geometry_msgs.msg import Point32 as geomPoint
from sensor_msgs.msg import PointCloud
from geometry_msgs.msg import Twist, Vector3
#for service
from se2path_finding.srv import *
from se2path_finding.msg import *
from se2path_finding.path_plotting_lib import  plot_path_plan_lib
import scipy
from scipy import linalg 
from scipy import sparse
#for getting odometry
from nav_msgs.msg import Odometry as Odom

from geometry_msgs.msg import Pose2D




class Youbot_control(object):
    def __init__(self,cmd_vel_pub):
        self.cmd_vel_pub = cmd_vel_pub
        self.youbot_vel_max = {"vx":1.5,"vy":1.5,"vth":0.4}



    def odom_callback(self,odom):
        theta_odom = np.arctan2(odom.pose.pose.orientation.z , odom.pose.pose.orientation.w)*2. 
        if theta_odom > math.pi: 
            theta_odom = theta_odom - 2*math.pi
        elif theta_odom < -math.pi: 
            theta_odom = theta_odom + 2*math.pi

        curr_des_pose = np.matrix([[0.],[0.],[0.]])
        curr_des_vel = np.matrix([[0.],[0.],[0.]])



        #Control Law:
        curr_pose = np.matrix([[odom.pose.pose.position.x],[odom.pose.pose.position.y],[theta_odom]])
        curr_vel = np.matrix([[odom.twist.twist.linear.x],[odom.twist.twist.linear.y],[odom.twist.twist.angular.z]])

        theta_des = 0.
        R_th_des = np.matrix([[np.cos(theta_des), -np.sin(theta_des), 0],[np.sin(theta_des), np.cos(theta_des),0],[0,0,1]]) 
        R_th_act = np.matrix([[np.cos(theta_odom), -np.sin(theta_odom), 0],[np.sin(theta_odom), np.cos(theta_odom),0],[0,0,1]])
        #input velocities
        gains = {"Kp":[],"Kd":[],"Ki":[]}
        gains["Kp"] = np.matrix(np.diag(1*np.ones(3))); gains["Kd"] = np.matrix(np.diag(0.*np.ones(3))); gains["Ki"] = np.matrix(np.diag(0.01*np.ones(3)))


        ang1 = theta_odom
        ang2 = theta_des
        R1 = np.matrix([[np.cos(ang1), -np.sin(ang1)],[np.sin(ang1), np.cos(ang1)]])
        R2 = np.matrix([[np.cos(ang2), -np.sin(ang2)],[np.sin(ang2), np.cos(ang2)]])
        Rdiff = R2*R1.T
        ang_diff = np.arctan2(Rdiff[1,0],Rdiff[0,0])

        error_pose = curr_des_pose - curr_pose
        error_pose = np.matrix([[error_pose.item(0)],[error_pose.item(1)],[ang_diff]])
        error_vel = curr_des_vel - curr_vel

        # vel_input = R_th_des.T*curr_des_vel +
        vel_input = R_th_act.T*(gains["Kp"]*error_pose + gains["Kd"]*error_vel)


        cmd_twist = Twist()

        if np.abs(vel_input.item(0)) > self.youbot_vel_max["vx"]:
            cmd_twist.linear.x = np.sign(vel_input.item(0))*self.youbot_vel_max["vx"]
        else:
            cmd_twist.linear.x = vel_input.item(0)
        if np.abs(vel_input.item(1)) > self.youbot_vel_max["vy"]:
            cmd_twist.linear.y = np.sign(vel_input.item(1))*self.youbot_vel_max["vy"]
        else:
            cmd_twist.linear.y = vel_input.item(1)
        if np.abs(vel_input.item(2)) > self.youbot_vel_max["vth"]:
            cmd_twist.angular.z = np.sign(vel_input.item(2))*self.youbot_vel_max["vth"]
        else:
            cmd_twist.angular.z = vel_input.item(2)

        print "error norm: ", np.linalg.norm(error_pose)
        # print "commanded x:",cmd_twist.linear.x, " y: ",cmd_twist.linear.y," th:", cmd_twist.angular.z

        if np.abs(error_pose.item(0)) < 0.005 and np.abs(error_pose.item(1)) < 0.005 and np.abs(error_pose.item(2)) < 0.05:
            cmd_twist= Twist()
            self.cmd_vel_pub.publish(cmd_twist)
            reason = "youbot back at base"
            rospy.signal_shutdown(reason)


        self.cmd_vel_pub.publish(cmd_twist)
        r = rospy.Rate(10) # 10hz
        r.sleep()
        print "\n\n\n"

def main():
    rospy.init_node("return_node")
    obs_cube_topic = "/obstacle_cubes"; obs_cube_pub = rospy.Publisher(obs_cube_topic, MarkerArray, queue_size = 1)
    cmd_vel_topic = "/cmd_vel"; cmd_vel_pub = rospy.Publisher(cmd_vel_topic,Twist,queue_size = 1) #has components Vector3: linear, angular *.x,*.y,*.z, for omni base I control lin.x,lin.y,ang.z together


    cntrl_cls_obj = Youbot_control(cmd_vel_pub)

    robot_odom_topic = "/odom"; rospy.Subscriber(robot_odom_topic, Odom, cntrl_cls_obj.odom_callback, queue_size = 1)
    # robot_odom_topic = "/qualisys/youbot/odom"; rospy.Subscriber(robot_odom_topic, Odom, cntrl_cls_obj.odom_callback, queue_size = 1)
    rospy.spin()

if __name__ == '__main__':
    main()

