import numpy as np
import yaml
import math
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


class Obstacle_maps(object):
    def get_vectors_of_pts(self,sensor_reading,pts_spacing):
        obstacle_pts = []
        for idx in range(len(sensor_reading)):
            #pluck out every set of vectors
            vector_pts = sensor_reading[idx]
            xi = vector_pts[0]
            xj = vector_pts[1]
            vij = xj - xi; vij_norm = np.linalg.norm(vij)
            space_step,rem = divmod(vij_norm,pts_spacing)
            vij_normed = np.divide(vij,vij_norm)
            obstacle_pts.append(xi)
            obstacle_pts.append(xj)
            for kdx in range(int(space_step)):
                pt_append = xi + np.dot(vij_normed, (kdx+1)*pts_spacing)
                obstacle_pts.append(pt_append)
        obstacle_pts = np.vstack(obstacle_pts)
        return obstacle_pts




    def subhrajit_map(self):
        X = [[-2.,-3.],
             [-2.,-1.],
             [-2.,1.],
             [-0.6,1.],
             [2.,-1.],
             [4.,-1.],
             [4.,-3.],
             [6.,-1.8],
             [6.,0.5],
             [5.,1.5],
             [5.,3.],
             [6.5,3.],
             [7.,3.],
             [6.5,-3.],
             [6.,-3.],
             [6.5,-2.5]] 

        x1 = X[0]; x2 = X[1]; x3 = X[2]; x4 = X[3]; x5 = X[4]; x6 = X[5]; x7 = X[6]; x8 = X[7]; x9 = X[8]; x10 = X[9]; x11 = X[10]; x12 = X[11]; x13 = X[12]; x14 = X[13]; x15 = X[14]; x16 = X[15]

        A = [x4[0],x2[1]]; print "A",A
        B = x4; print "B",B
        C = x5; print "C",C
        D = [x5[0],x9[1]]; print "D",D
        E = [x10[0],x4[1]]; print "E",E
        F = x10; print "F",F
        G = [x12[0],x10[1]]; print "G",G
        H = x9; print "H",H
        I = x16; print "I",I
        J = [x8[0],x16[1]]; print "J",J
        K = x8; print "K",K
        L = [x9[0],x6[1]]; print "L",L
        M = [x6[0],x8[1]]; print "M",M
        N = x6; print "N",N

        l1 = [A,B]
        l2 = [A,C]
        l3 = [C,D]
        l4 = [D,H]
        l5 = [H,L]
        l6 = [L,N]
        l7 = [N,M]
        l8 = [M,K]
        l9 = [K,J]
        l10 = [J,I]
        l11 = [I,G]
        l12 = [G,F]
        l13 = [F,E]
        l14 = [E,B]

        sensor_readings =np.array([l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14])

        sensor = self.get_vectors_of_pts(sensor_readings,.051)

        fig = plt.figure(); ax = fig.add_subplot(111)
        ax.set_xlabel('X axis'); ax.set_ylabel('Y axis')
        c = 'b'; m = '*'
        ax.scatter(sensor[:,0],sensor[:,1],c=c,marker=m)
        ax.set_xticks(np.arange(-7,7,.5))
        ax.set_yticks(np.arange(-3,3,0.5))
        plt.grid()
        plt.show()

        print "initial sensor readigns: ", len(sensor_readings), "print final sensor readings: ", len(sensor)
        #x8 was : [6.,-1.5],
        for idx in range(len(X)):
            print "\n x",idx+1,": ",X[idx]
        points = X
        goal_pose = [4.5,-1.4, 0.]
        # goal_pose = [4.5,-1.4, math.pi]

        obs_map = { "sensor_reading":sensor, "goal_pose":goal_pose,"obs_1":[points[2-1],points[4-1]],"obs_2":[points[1-1],points[6-1]],"obs_3":[points[3-1],points[11-1]], "obs_4":[points[5-1],points[9-1]], "obs_5":[points[10-1],points[12-1]], "obs_6":[points[14-1],points[13-1]], "obs_7":[points[7-1],points[8-1]], "obs_8":[points[15-1],points[16-1]]}
        return obs_map

    def subhrajit_map_second(self):
            X = [[-2.,-3.],
                 [-2.,-1.],
                 [-2.,1.],
                 [-0.6,1.],
                 [2.,-1.],
                 [4.,-1.],
                 [4.,-3.],
                 [5.5,-2.],

                 [5.6,0.],
                 
                 [5.,1.5],
                 [5.,3.],
                 [6.5,3.],
                 [7.,3.],
                 [6.5,-3.],
                 [5.5,-3.],
                 [6.5,-2.5]]
            #x8 was : [6.,-1.5],
            for idx in range(len(X)):
                print "\n x",idx+1,": ",X[idx]
            points = X
            goal_pose = [4.5,-1.5, math.pi]

            obs_map = {"sensor_reading":[],"goal_pose":goal_pose,"obs_1":[points[2-1],points[4-1]],"obs_2":[points[1-1],points[6-1]],"obs_3":[points[3-1],points[11-1]], "obs_4":[points[5-1],points[9-1]], "obs_5":[points[10-1],points[12-1]], "obs_6":[points[14-1],points[13-1]], "obs_7":[points[7-1],points[8-1]], "obs_8":[points[15-1],points[16-1]]}
            return obs_map


    def map_stone_hedge(self):
        obs1 = [[-3.,-2.],[-.5,2.]]
        obs2 = [[-0.5,-2.],[4.6,-0.5]]
        obs3 = [[-0.5,0.5],[3.5,2.]]
        obs4 = [[4.6,-2.],[5.8,2.]]
        obs5 = [[6.7,-2.],[7.,2.]]
        obs6 = [[obs4[1][0],obs4[0][1]],[obs5[0][0],-1.8]]
        goal_pose = [6.25,0.,math.pi/2.]
        obs_map = {"sensor_reading":[],"goal_pose":goal_pose,"obs_1":obs1, "obs_2":obs2, "obs_3":obs3, "obs_4":obs4, "obs_5":obs5, "obs_6":obs6}
        return obs_map





    def monroe_map1(self):
        corridor_dist = 0.7 #0.8 worked really well
        X = np.double([[-2,-3], #1
                       [-2,-1], #2
                       [-2,1], #3
                       [-0.6,1], #4
                       [2,-1],#5
                       [4,-1],#6
                       [4,-3],#7
                       [6,-1.8],#8  #second value
                       [5.7,0.2],#9 (5.7,0.2) which gives spacing of .8
                       [5,1.5],#10
                       [5,3],#11
                       [6.5,3],#12
                       [7,3],#13
                       [6.5,-3],#14
                       [6,-3],#15
                       [6.5,-2.5]] ) #16

        X[7,1] = X[4,1] - corridor_dist ; X[7,0] = X[11,0]-corridor_dist; #point 8
        X[8,0]=  X[11,0]-corridor_dist; X[8,1] = X[3,1] - corridor_dist #point 9
        X[14,0] = X[7,0]

        x1 = X[0]; x2 = X[1]; x3 = X[2]; x4 = X[3]; x5 = X[4]; x6 = X[5]; x7 = X[6]; x8 = X[7]; x9 = X[8]; x10 = X[9]; x11 = X[10]; x12 = X[11]; x13 = X[12]; x14 = X[13]; x15 = X[14]; x16 = X[15]

        A = [x4[0],x2[1]]; print "A",A
        B = x4; print "B",B
        C = x5; print "C",C
        D = [x5[0],x9[1]]; print "D",D
        E = [x10[0],x4[1]]; print "E",E
        F = x10; print "F",F
        G = [x12[0],x10[1]]; print "G",G
        H = x9; print "H",H
        I = x16; print "I",I
        J = [x8[0],x16[1]]; print "J",J
        K = x8; print "K",K
        L = [x9[0],x6[1]]; print "L",L
        M = [x6[0],x8[1]]; print "M",M
        N = x6; print "N",N

        l1 = [A,B]
        l2 = [A,C]
        l3 = [C,D]
        l4 = [D,H]
        l5 = [H,L]
        l6 = [L,N]
        l7 = [N,M]
        l8 = [M,K]
        l9 = [K,J]
        l10 = [J,I]
        l11 = [I,G]
        l12 = [G,F]
        l13 = [F,E]
        l14 = [E,B]

        sensor_readings =np.array([l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14])

        pt_count = .051
        sensor = self.get_vectors_of_pts(sensor_readings,pt_count)

        goal_pose = [4.5,-1.4, 0.]

        fig = plt.figure(); ax = fig.add_subplot(111)
        ax.set_xlabel('X axis'); ax.set_ylabel('Y axis')
        c = 'b'; m = '*'
        ax.scatter(sensor[:,0],sensor[:,1],c=c,marker=m)
        ax.scatter(goal_pose[0],goal_pose[1],c='r',marker='^')
        ax.set_xticks(np.arange(-7,7,.5))
        ax.set_yticks(np.arange(-3,3,0.5))
        plt.grid()
        plt.axis('equal')
        plt.show()

        print "initial sensor readigns: ", len(sensor_readings), "print final sensor readings: ", len(sensor)
        #x8 was : [6.,-1.5],
        for idx in range(len(X)):
            print "\n x",idx+1,": ",X[idx]
        points = X
        
        # goal_pose = [4.5,-1.4, math.pi]

        obs_map = { "sensor_reading":sensor, "goal_pose":goal_pose,"obs_1":[points[2-1],points[4-1]],"obs_2":[points[1-1],points[6-1]],"obs_3":[points[3-1],points[11-1]], "obs_4":[points[5-1],points[9-1]], "obs_5":[points[10-1],points[12-1]], "obs_6":[points[14-1],points[13-1]], "obs_7":[points[7-1],points[8-1]], "obs_8":[points[15-1],points[16-1]]}
        return obs_map


    def monroe2(self):
        X = np.double([[-.5,.35], #x1
                     [2,.35], #x2
                     [2,3], #x3
                     [6,3], #x4
                     [6,-.35], #x5
                     [5.3,-.35], #x6
                     [5.3,1.5], #x7
                     [5,1.5], #x8
                     [5,2.3], #x9
                     [2.9,2.3], #x10
                     [2.9,-.4], #x11
                     [-.5,-.4]]) #x12

        l12 = [X[0],X[1]]
        l23 = [X[1],X[2]]
        l34 = [X[2],X[3]]
        l45 = [X[3],X[4]]
        l56 = [X[4],X[5]]
        l67 = [X[5],X[6]]
        l78 = [X[6],X[7]]
        l89 = [X[7],X[8]]
        l910 = [X[8],X[9]]
        l1011 = [X[9],X[10]]
        l1112 = [X[10],X[11]]
        l121 = [X[11],X[0]]

        sensor_readings = [l12, l23, l34, l45, l56, l67, l78, l89, l910, l1011, l1112, l121]

        obs1 = np.double([[-2,.35],[2,3]])
        obs2 = np.double([[-2,-.4],[-.5,.35]])
        obs3 = np.double([[-2,-2],[6,-.35]])
        obs4 = np.double([[2.9,-.4],[5.3,1.5]])
        obs5 = np.double([[2.9,1.5],[5,2.3]])
        obs6 = np.double([[6,-2],[7,3]])
        obs7 = np.double([[-2,3],[7,3.5]])

        goal_pose = [5.65,.5, -math.pi/2.]


        sensor = self.get_vectors_of_pts(sensor_readings,.051)

        fig = plt.figure(); ax = fig.add_subplot(111)
        ax.set_xlabel('X axis'); ax.set_ylabel('Y axis')
        c = 'b'; m = '*'
        ax.scatter(sensor[:,0],sensor[:,1],c=c,marker=m)
        ax.scatter(goal_pose[0],goal_pose[1],c='r',marker='^')
        ax.set_xticks(np.arange(-7,7,.5))
        ax.set_yticks(np.arange(-3,3,0.5))
        plt.grid()
        plt.axis('equal')
        plt.show()

        obs_map = { "sensor_reading":sensor, "goal_pose":goal_pose,"obs1":obs1,"obs2":obs2,"obs3":obs3,"obs4":obs4,"obs5":obs5,"obs6":obs6,"obs7":obs7}
        return obs_map



    def monroe3(self):
        X = np.double([[2,3],#x1
             [2,0],#x2
             [2.3,0],#x3
             [2.3,3],#x4
             [4,-3,],#x5
             [4,2.3],#x6
             [5.2,2.3],#x7
             [5.2,-3],#x8
             [6,3],#x9
             [6,-2],#x10
             [7,-2],#x11
             [7,3],#x12
             [4,-1],#x13
             [-.6,-1],#x14
             [-.6,.6],#x15
             [2,.6]])#x16

        l12 = [X[0],X[1]]
        l23 = [X[1],X[2]]
        l34 = [X[2],X[3]]

        l56 = [X[4],X[5]]
        l67 = [X[5],X[6]]

        l78 = [X[6],X[7]]
        l910 = [X[8],X[9]]
        l1011 = [X[9],X[10]]
        l1112 = [X[10],X[11]]

        l1314 = [X[12],X[13]]
        l1415 = [X[13],X[14]]
        l1516 = [X[14],X[15]]

        sensor_readings = [l12, l23, l34, l56, l67,l78, l910, l1011, l1112]

        sensor = self.get_vectors_of_pts(sensor_readings,.051)



        obs1 = [X[1],X[3]]
        obs2 = [X[4],X[6]]
        obs3 = [X[9],X[11]]
        # obs4 = np.double([[-.6,-3],[4,-1]])
        # obs5 = np.double([[-2,-3],[-.6,3]])
        # obs6 = np.double([[-0.6,0.6],[2,3]])

        goal_pose = [5.6, -1.,math.pi/2.]


        fig = plt.figure(); ax = fig.add_subplot(111)
        ax.set_xlabel('X axis'); ax.set_ylabel('Y axis')
        c = 'b'; m = '*'
        ax.scatter(sensor[:,0],sensor[:,1],c=c,marker=m)
        ax.scatter(goal_pose[0],goal_pose[1],c='r',marker='^')
        ax.set_xticks(np.arange(-7,7,.5))
        ax.set_yticks(np.arange(-3,3,0.5))
        plt.grid()
        
        plt.show()

        obs_map = { "sensor_reading":sensor, "goal_pose":goal_pose,"obs1":obs1,"obs2":obs2,"obs3":obs3}
        print  "this was changed!!!"
        return obs_map



    def monroe_open_path(self):
        X = np.double([[1,3],#x1
             [1,1],#x2
             [4,1],#x3
             [4,3],#x4
             [1,-3,],#x5
             [1,-1],#x6
             [4,-1],#x7
             [4,-3]])#x8

        l12 = [X[0],X[1]]
        l23 = [X[1],X[2]]
        l34 = [X[2],X[3]]

        l56 = [X[4],X[5]]
        l67 = [X[5],X[6]]
        l78 = [X[6],X[7]]

        sensor_readings = [l12, l23, l34, l56, l67,l78]

        sensor = self.get_vectors_of_pts(sensor_readings,.051)



        obs1 = [X[1],X[3]]
        obs2 = [X[4],X[6]]


        goal_pose = [5.0, 0.,math.pi]
        # goal_pose = [6.0, 3.,math.pi]


        fig = plt.figure(); ax = fig.add_subplot(111)
        ax.set_xlabel('X axis'); ax.set_ylabel('Y axis')
        c = 'b'; m = '*'
        ax.scatter(sensor[:,0],sensor[:,1],c=c,marker=m)
        ax.scatter(goal_pose[0],goal_pose[1],c='r',marker='^')
        ax.set_xticks(np.arange(-7,7,.5))
        ax.set_yticks(np.arange(-3,3,0.5))
        plt.grid()
        
        plt.show()

        obs_map = { "sensor_reading":sensor, "goal_pose":goal_pose,"obs1":obs1,"obs2":obs2}
        print  "this was changed!!!"
        return obs_map






    """save all the data to a bag, and open in separate """
    # def Paper_plots(self):
    #     print "paper plots"

    # 1. plot 3 figures for time paramterized x,y,th separately that contains 


def main():
    print " "
    obs_map_cls = Obstacle_maps()

    # obs_map_1 = obs_map_cls.subhrajit_map()
    # obs_map_1 = obs_map_cls.subhrajit_map_second()
    #obs_map_1 = obs_map_cls.map_stone_hedge()


    # obs_map_1 = obs_map_cls.monroe_map1()
    # obs_map_1 = obs_map_cls.monroe2() #check goal state
    obs_map_1 = obs_map_cls.monroe3()

    # obs_map_1 = obs_map_cls.monroe_open_path()




    file_name = 'obs_map_data.yaml'

    with open(file_name, 'w') as outfile:
        outfile.write( yaml.dump(obs_map_1, default_flow_style=True))

    with open(file_name, 'r') as f:
        obs_map_test = yaml.load(f)
    print "obs map test: ", obs_map_test#["obs_1"]



if __name__ == '__main__':
    main()
