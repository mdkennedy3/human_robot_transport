import numpy as np
import math

class Manual_paths_cls(object):
    """docstring for Manual_paths_cls"""
    def monroe1_g0(self):
        # monroe1: goal [4.5,-1.4,0]
        path = np.array([[0,0,0],
                         [1.5,0.65,0],
                         [5,.65,0],
                         [6.15,1,math.pi/2.],
                         [6.15,-1.3,math.pi/2.],
                         [5.6,-1.4,(10.*math.pi/180.)],
                         [4.5,-1.4,0]])
        path = np.double(path)
        return path
        
    def monroe1_g_neg_pi(self):
        # monroe1: goal [4.5,-1.4,-pi]
        path = np.array([[0,0,0],
                         [1.5,0.65,-math.pi],
                         [5,0.65,-math.pi],
                         [6.15,1,-math.pi/2.],
                         [6.15,-1.1,-math.pi/2],
                         [6.15,-1.4,(-120*math.pi/180)],
                         [4.5,-1.4,-math.pi]])# [3.5,0.65,-math.pi],
        path = np.double(path)
        return path


    def monroe2(self):
        # monroe2: goal
        # path = np.array([[0,0,0],
        #                  [2.45,0,0],
        #                  [2.45,2.65,0],
        #                  [5.65,2.65,0],
        #                  [5.65,1.5,-math.pi/2],
        #                  [5.65,0.5,-math.pi/2]])
        path = np.array([[0,0,0],
                 [2.35,0,0],
                 [2.35,2.65,0],
                 [5.65,2.65,0],
                 [5.65,1.5,-math.pi/2],
                 [5.65,0.5,-math.pi/2]])
        path = np.double(path)
        return path
        



    def monroe3(self):
        # monroe3: goal [5.6,-1,math.pi/2]
        # path = np.array([[0,0,0],
        #                  [2.2,-.5,0],
        #                  [3,0,math.pi/2],
        #                  [3,2.65,math.pi/2],
        #                  [5.65,2.65,math.pi/2],
        #                  [5.65,1,math.pi/2],
        #                  [5.6,-1,math.pi/2]])

        path = np.array([[0,0,0],
                         [2.2,-.7,0],
                         [3,0,math.pi/2],
                         [3.5,3,math.pi/2],
                         [5.65,2.65,math.pi/2],
                         [5.65,1,math.pi/2],
                         [5.6,-1,math.pi/2]])

        path = np.double(path)
        return path

    def monroe_open(self):
        path = np.array([[0,0,0],
                         # [1,0,math.pi/5],
                         # [2,0,math.pi*(2/5.)],
                         [3,0,math.pi*(3/5.)],
                         # [4,0,math.pi*(4/5.)],
                         [5.0, 0.,math.pi],])

        # path = np.array([[0,0,0],
        #                  # [1,0,math.pi/5],
        #                  # [2,0,math.pi*(2/5.)],
        #                  [4,0,math.pi*(3/5.)],
        #                  [5,1,math.pi*(4/5.)],
        #                  [6.0, 2.,math.pi],])

        path = np.double(path)
        return path





