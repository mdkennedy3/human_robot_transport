#!/usr/bin/env python2

#Author: Monroe Kennedy III: kmonroe@seas.upenn.edu
#Date: 9/2/16
#Copyright: All rights reserved, please contact author before any usage. 
#University of Pennsylvania
#Summary: This is a python implementation of the 'Generalized Convex Corridor Decomposition'

import time
import numpy as np
import math
import rospy
import baxter_interface
import argparse
import struct
import sys
import tf


class CVX_corridor_decomp(object):
    """This is a python implementation of the Generalized Convex Corridor Decomposition"""
    # def __init__(self, arg):
    #     # super(CVX_corridor_decomp, self).__init__()
    #     self.arg = arg

    def Corridor_generator(self,Path,Obstacles,*args):
        """
        Input:
        1. Path: (list/array) with k rows (for k pts in path), and n dimensions in each row
        2. Obstacles: (list/array) with m rows (for m obstacle points), and n dim in each row
        3. world_planes: (list/array) with world planes to be added as constraints for each chord
        Output: 
        Alist: (list of lists each containing rows with elements of n-dimensional hyperplanes)
        """

        world_planes = []
        if len(args) > 0:
            world_planes.append(args[0])

        return_pts_bool = False
        if len(args) > 1:
            #just send value (e.g. 1)
            return_pts_bool = True




        start_time = time.time()

        Aset = []
        obs_pts_active_set = []
        for idx in range(len(Path)-1):
            lvect = np.array(Path[idx+1]) - np.array(Path[idx]); lvect_normed = np.divide(lvect,np.linalg.norm(lvect))
            gamma1 = np.ones([1,len(Path[idx])]) - np.array(Path[idx]); gamma1_normed = np.divide(gamma1,np.linalg.norm(gamma1))
            if np.abs(np.sum(gamma1_normed - lvect_normed)) < 0.0001:
                gamma1 = np.hstack([np.ones([1,len(Path[idx])-1]),np.array([[-1]])]) - np.array(Path[idx]); gamma1_normed = np.divide(gamma1,np.linalg.norm(gamma1))


            gamma1_perp = gamma1 - (np.matrix(lvect_normed)*np.matrix(gamma1).T)*lvect_normed; gamma1_perp_normed = np.divide(gamma1_perp,np.linalg.norm(gamma1_perp))


            #print "idx",idx," 1: ", time.time() - start_time

            Plane1 = np.hstack([np.matrix(gamma1_perp_normed), -np.matrix(gamma1_perp_normed)*np.matrix(Path[idx]).T])
            k = 0
            a_prime = 1 #just initialize
            while True:
                if np.abs(Plane1.item(k)) > 0.001:
                    a_prime = Plane1.item(k)
                    break
                else:
                    k += 1
                if k > np.max(gamma1_normed.shape):
                    print "no viable coefficient found, in cvx_alg"
                    break


            gamma2 = np.zeros(gamma1_normed.shape);

            b1 = -np.matrix(gamma1_normed)*np.matrix(Path[idx]).T
            if np.abs(b1) > 0.0001:
                qk = np.divide(-b1,a_prime)
                gamma2[0,k] = qk 
            else:
                k2 = k+1
                qk = 1
                while True:
                    if np.abs(Plane1.item(k2)) > 0.001:
                        a_prime2 = Plane1.item(k2)
                        break
                    else:
                        k2 += 1
                    if k2 > np.max(gamma1_normed.shape):
                        print "no viable coefficient found, in cvx_alg"
                        break
                qk2 = np.divide((-b1 - a_prime),a_prime2)
                gamma2[0,k] = 1
                gamma2[0,k2] = qk2

            gamma2 = gamma2 - np.array(Path[idx]); gamma2_normed = np.divide(gamma2, np.linalg.norm(gamma2))

            gamma2_perp = gamma2 - (np.matrix(lvect_normed)*np.matrix(gamma2).T)*lvect_normed; gamma2_perp_normed = np.divide(gamma2_perp,np.linalg.norm(gamma2_perp))

            Plane2 = np.hstack([np.matrix(gamma2_perp_normed), -np.matrix(gamma2_perp_normed)*np.matrix(Path[idx]).T])

            Plane_stack = np.matrix(np.vstack([Plane1, Plane2]))

            #print "idx",idx," 2: ", time.time() - start_time


            obs_mat = np.matrix(Obstacles)
            ranking_bar = Plane_stack * np.vstack([ obs_mat.T, np.ones([1,obs_mat.shape[0]])])
            ranking = np.abs(np.sum(ranking_bar,0))

            ranking_temp = ranking.tolist()[0]
            #store this
            rank_list = ranking.argsort().tolist()[0]
            obstacle_list_temp = obs_mat.tolist()


            #print "idx",idx," 3: ", time.time() - start_time

            A = []
            obs_pts_active_chord = []
            # print "world planes: ", world_planes
            # A = list(world_planes[0])
            if len(world_planes) > 0:
                A= list(world_planes[0])
            while len(ranking_temp) > 0:
                # rank_idx = rank_list_temp.pop(0)
                obs_pt = np.matrix(obstacle_list_temp[rank_list[0]])
                obs_pts_active_chord.append(obs_pt)
                # obs_pt = np.matrix(obstacle_list_temp[rank_idx])
                obs_vect = obs_pt - np.matrix(Path[idx])
                obs_vect_perp = obs_vect - (np.matrix(lvect_normed)*obs_vect.T)*lvect_normed
                if np.linalg.norm(obs_vect - obs_vect_perp) > np.linalg.norm(lvect):
                    obs_vect_perp = obs_pt - np.matrix(Path[idx+1])
                elif np.sign(np.matrix(lvect_normed)*obs_vect.T) == -1: 
                    obs_vect_perp = obs_pt - np.matrix(Path[idx])
                obs_vect_perp_normed = np.divide(obs_vect_perp,np.linalg.norm(obs_vect_perp))
                obs_Plane = np.hstack([obs_vect_perp_normed,-obs_vect_perp_normed*obs_pt.T])
                A.append(obs_Plane.tolist()[0])
                obs_signs_bar = (obs_Plane*np.vstack([np.matrix(obstacle_list_temp).T,np.ones([1,len(obstacle_list_temp)])])).tolist()[0]
                obs_signs = np.sign(obs_signs_bar).tolist()
                #obstacle_list_temp[obs_signs == -1]
                obstacle_list_temp = np.array(obstacle_list_temp)[np.argwhere(np.array(obs_signs) == -1).transpose()[0].tolist()].tolist()
                ranking_temp = np.array(ranking_temp)[np.argwhere(np.array(obs_signs) == -1).transpose()[0].tolist()]
                rank_list = ranking_temp.argsort().tolist()
                


            #print "idx",idx," 4", time.time() - start_time
            #print "A after everything: ", A
            Aset.append(A)
            obs_pts_active_set.append(obs_pts_active_chord)

        if return_pts_bool:
            return Aset,obs_pts_active_set
        else:
            return Aset





def main():
    cvx_obj = CVX_corridor_decomp()

    #4D:
    path = [[0,0,0,0],[.5,.5,.5,.5],[1,1,1,1]]
    obstacle = [[0,0,1,0],[0,0,-1,0],[1,0,0,0],[-1,0,0,0],[0,1,0,0],[0,-1,0,0],[0,0,1,0],[0,0,-1,0]]

    #3D
    path = [[0,0,0],[.5,.5,.5],[1,1,1]]
    obstacle = [[0,0,1],[0,0,-1],[1,0,0],[-1,0,0],[0,1,0],[0,-1,0],[0,0,1],[0,0,-1]]

    #2D
    # path = [[0,0],[.5,.5],[1,1]]
    # obstacle = [[1,0],[-1,0],[0,1],[0,-1]]


    Aset = cvx_obj.Corridor_generator(path,obstacle)



if __name__ == '__main__':
    main()









