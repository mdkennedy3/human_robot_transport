import numpy as np
import numpy.matlib
import time
import tf
import math
import sys
import pcl 
from se2path_finding.astar_support import astar
from se2path_finding.astar_support import my_astar



class astar_support(object):
    def __init__(self,world, pt_cloud,start_pose, goal_pose):
        self.world = world
        self.pt_cloud_obj = pt_cloud
        # self.mink_pts_layered = pt_cloud
        print "pt cloud: ", pt_cloud#.keys()
        # self.pt_cloud = pt_cloud['point_cloud'][0].get_occupied_voxel_centers()
        self.pt_cloud = pt_cloud.get_occupied_voxel_centers()
        self.world_bounds = [[world.extents['xmin'],world.extents['xmax']],
                        [world.extents['ymin'],world.extents['ymax']],
                        [world.extents['th_min'],world.extents['th_max']]]
        # world.steps = {'x_steps':x_steps,'y_steps':y_steps, 'theta_steps':theta_steps}

        self.start_pose = start_pose
        self.goal_pose = goal_pose
        self.debug = False

        #gets stuck at point: 3.66, -1.04, 1.57

        self.limit= 1e6#int(1e3*len(self.pt_cloud)*len(pt_cloud.keys()))
        # self.limit = int( len(np.arange(world.extents['xmin'],world.extents['xmax']+self.world.steps['x_steps'],self.world.steps['x_steps']))*
        #                   len(np.arange(world.extents['ymin'],world.extents['ymax']+self.world.steps['y_steps'],self.world.steps['y_steps']))*
        #                   len(np.arange(world.extents['th_min'],world.extents['th_max']+self.world.steps['theta_steps'],self.world.steps['theta_steps']))/2. )
        print "limit:", self.limit


        self.path = self.Find_seed_path()
        #must be a list (or array) p_kxn
        # path_to_return = np.array(path)
        print "path: ", self.path 


    def get_path(self):
        # trunctated_path = []
        # path = np.array(self.path)
        # last_pt = path[0]
        # trunctated_path.append(last_pt)
        # test_step = 0.3*np.min([self.world.steps['x_steps'],self.world.steps['y_steps'],self.world.steps['theta_steps']])

        # for idx in range(1,path.shape[0]):
        #     #test if pts between 
        #     pt_curr = path[idx]
        #     vect = pt_curr - last_pt; vect_norm = np.linalg.norm(vect)


        #     idx = iter(xrange(maxint))
        #     fail = False
        #     while false and idx < maxint-1 for idx in range(100):
        #         try:
        #         test_point = last_pt + np.dot(vect_norm,test_step)
        #         self.pt_cloud_obj.is_voxel_occupied_at_point(test_point)
        #         if fail == True:
        #             break
        #         except:
        #             break

        return np.array(self.path)


    def Find_seed_path(self):
        #DONE:
        def debug(nodes):
            self.nodes = nodes
        def goal(pos):
            pos_array = np.array(pos)
            goal_pose_array = np.array(self.goal_pose.tolist()[0])
            xdiff = np.abs(goal_pose_array[0] - pos_array[0])
            ydiff = np.abs(goal_pose_array[1] - pos_array[1])
            thdiff = np.abs(goal_pose_array[2] - pos_array[2])
            goal_bool = xdiff < self.world.steps['x_steps']/2. and ydiff < self.world.steps['y_steps']/2. and thdiff < self.world.steps['theta_steps']/2.
            if goal_bool:
                print "it thinks: ", pos, "is close enough to ", goal_pose_array,"\n xdiff: ", xdiff, ", ydiff: ", ydiff, " thdiff: ", thdiff, "\n min of all:",np.min([self.world.steps['x_steps']/2. , self.world.steps['y_steps']/2. , self.world.steps['theta_steps']/2.])
            return  goal_bool


        def cost(from_pos, to_pos):
            diff = np.array(from_pos) - np.array(to_pos)
            cost_fnct =np.linalg.norm(diff)
            #I could penalize the rotation less if desired, but note that in current form as the units are meters and radians, rotations will be penalized more
            return cost_fnct
        def estimate(pos):
            #heuristic calcluation
            diff =  np.array(self.goal_pose) - np.array(pos)
            cost_fnct = 1*np.linalg.norm(diff)
            # print "queried cost: ", cost_fnct
            # sys.stdout.write(" queried cost: \r%f" %cost_fnct)
            return cost_fnct

        def neighbors(pos):
            # print "calling neighbors..."
            # self.pt_cloud = np.array(self.pt_cloud)
            pos_array = np.array(pos)
            #every pos has at most 26 neighbors, generate them now
            neighbors_list = []
            # print "pose_array", pos_array, "elem 0:", pos_array[0]
            x_steps = [pos_array[0]-self.world.steps['x_steps'],pos_array[0], pos_array[0] + self.world.steps['x_steps']]
            y_steps = [pos_array[1]-self.world.steps['y_steps'],pos_array[1], pos_array[1] + self.world.steps['y_steps']]
            theta_steps = [pos_array[2]-self.world.steps['theta_steps'],pos_array[2],pos_array[2] + self.world.steps['theta_steps']]


            # occ = self.pt_cloud; xdiff = self.world.steps['x_steps']; ydiff = self.world.steps['y_steps']; zdiff = self.world.steps['theta_steps']
            neighbors_list = [ tuple(np.array([x_steps[idx],y_steps[jdx],theta_steps[kdx]]))   
            for idx in range(3) for jdx in range(3) for kdx in range(3) 
            if x_steps[idx] < self.world.extents['xmax'] and x_steps[idx] > self.world.extents['xmin'] and 
            y_steps[jdx] < self.world.extents['ymax']  and y_steps[jdx] > self.world.extents['ymin'] and 
            theta_steps[kdx] < self.world.extents['th_max'] and theta_steps[kdx] > self.world.extents['th_min'] if not (jdx==1 and idx==1 and kdx==1) 
            if not self.pt_cloud_obj.is_voxel_occupied_at_point(np.array(np.array([x_steps[idx],y_steps[idx],theta_steps[kdx]])))]
            # if not self.mink_pts_layered["point_cloud"][ np.argmin(np.abs(self.mink_pts_layered["theta"]-theta_steps[kdx]))].is_voxel_occupied_at_point(np.array([x_steps[idx],y_steps[jdx],self.mink_pts_layered["theta"][ np.argmin(np.abs(self.mink_pts_layered["theta"]-theta_steps[kdx]))]],dtype=np.float32))]
            

            # layered_one_line_test = self.mink_pts_layered["point_cloud"][ np.argmin(np.abs(self.mink_pts_layered["theta"]-theta_steps[kdx]))].is_voxel_occupied_at_point(np.array([x_steps[idx],y_steps[jdx],self.mink_pts_layered["theta"][ np.argmin(np.abs(self.mink_pts_layered["theta"]-theta_steps[kdx]))]],dtype=np.float32))

            neigh_len = len(neighbors_list)
            # sys.stdout.write(" length of neighbor list: \r%d" %neigh_len)
            # stop

            return neighbors_list


        start_pose = tuple(np.array(self.start_pose).tolist()[0])
        # goal = tuple(np.array(self.goal_pose).tolist()[0])
        self.astar = astar.Astar_class()
        path = self.astar.astar_calc(start_pose, neighbors, goal, 0, cost, estimate, self.limit, debug)

        self.my_astar = my_astar.My_Astar_class()
        path = self.my_astar.astar_calc(start_pose, neighbors, goal, cost, estimate)

        return path


#example
# import numpy as np
# v = np.linspace(1,10,10)
# occ = np.vstack([v,v,v])
# occ = occ.transpose()
# xdiff = 0.5; ydiff = 0.5; zdiff = 0.5
# # pt = np.array([0.1,0.1,0.1]) 
# # pt = np.array([1.1,1.1,1.1]) 
# pt = np.array([11.1,1.1,1.1])
# closebool = [ np.abs(pt[0] -occ[idx,0])<xdiff and  np.abs(pt[1] -occ[idx,1]) < ydiff and np.abs(pt[2] -occ[idx,2]) < zdiff for idx in range(occ.shape[0]) ] 
# cell_free = "free"
# if np.sum(closebool):
#     cell_free = "occupied"
# print cell_free
