import numpy as np, scipy.spatial.ckdtree as kd
import numpy.matlib
import time
import tf
import math
#initial path planning
from ompl import base as ob
from ompl import geometric as og
from ompl import control, tools, util



#required for ompl distance sensing with self defined states
# class MySE2(ob.SE2StateSpace):
#     def distance(self,state1, state2):
#         #print state1.getX(), state2.getX()
#         # distxy = np.sqrt((state1.getX()-state2.getX())**2 + (state1.getY()-state2.getY())**2)
#         # ang1 = state1.getYaw()
#         # ang2 = state2.getYaw()
#         ang1 = state1.getZ()
#         ang2 = state2.getZ()
#         # R1 = np.matrix([[math.cos(ang1), -math.sin(ang1)],[math.sin(ang1), math.cos(ang1)]])
#         # R2 = np.matrix([[math.cos(ang2), -math.sin(ang2)],[math.sin(ang2), math.cos(ang2)]])
#         # Rdiff = R2*R1.T
#         # # ang_diff = ang2 - ang1
#         # ang_diff = math.atan2(Rdiff[1,0],Rdiff[0,0])
#         # final_dist = distxy + ang_diff
#         dist = np.sqrt((state1.getX()-state2.getX())**2
#                         + (state1.getY()-state2.getY())**2
#                         + (ang2 - ang1)**2)
#         return dist
class ompl_support(object):
    # def isStateValid(self,state):
    #     # test_point = np.array([state.getX(),state.getY(),state.getYaw()],dtype=np.float32)
    #     test_point = np.array([state[0],state[1],state[2]],dtype=np.float32)
    #     # print "Test point: ",test_point," occupied bool: ",self.cloud.is_voxel_occupied_at_point(test_point)
    #     if np.abs(state[2]) > 3*math.pi:
    #         return False
    #     return not self.cloud.is_voxel_occupied_at_point(test_point)

    def isStateValid(self, state):
        # self.mink_pts_layered = {"mink_xy":mink_layered, "theta":self.world.th_coord,"point_cloud":[]}

        # theta_list = self.mink_pts["theta"]
        # theta_test = state[2]
        #
        # theta_arg_index = np.argmin(np.abs(theta_list-theta_test)) #find nearest index
        # theta_arg = theta_list[theta_arg_index]
        #
        # cloud_curr = self.mink_pts["point_cloud"][theta_arg_index]
        # test_point = np.array([state[0],state[1],theta_arg],dtype=np.float32)
        # if np.abs(state[2]) > 3*math.pi:
        #     return False
        #
        # return not cloud_curr.is_voxel_occupied_at_point(test_point)
        # TODO just check distance to origin instead of adding inner and outer blocking points
        # TODO can scale for more accuracy to "squeeze" angles closer to actual cylinder
        state_list = []
        state_list.append((state[0]))
        state_list.append((state[1]))
        state_list.append((state[2]))
        state_list.append((state[3]))
        #
        # kd_points = self.mink_pts_kd.query_ball_point(state_list, .25)
        # if len(kd_points) > 0:
        #     return False
        # print "alpha"
        # print state[2]
        # print "beta"
        # print state[3]
        norm = np.sqrt((state[2])**2 + (state[3])**2)
        # print "norm"
        # print norm
        if norm >1.2 or norm < .8:
            return False
        dd, ii = self.mink_pts_kd.query(state_list)
        # print "nearest neighbor"
        # print ii
        # print self.mink_pts_kd.data[ii]
        # print "state"
        # print state_list
        # print "dd"
        # print dd
        if dd <.5:
            #if np.isclose(state[0],self.mink_pts_kd.data[ii][0],atol=.1) and np.isclose(state[1], self.mink_pts_kd.data[ii][1], atol=.1):
            #if math.fabs(state[0] - self.mink_pts_kd.data[ii][0]) <.5 and math.fabs(state[1] - self.mink_pts_kd.data[ii][1]) <.5:
                return False
        return True


        


    def convert_path_to_list(self,path):
        x = []; y = []; alpha = []; beta = []
        path_list_first = {'x':[],'y':[],'alpha':[], 'beta':[]}
        for idx in range(len(path.getStates())):
            x.append(path.getStates()[idx][0])
            y.append(path.getStates()[idx][1])
            alpha.append(path.getStates()[idx][2])
            beta.append(path.getStates()[idx][3])
        # path_list_first = np.vstack(x,y,yaw)
        path_list_first['x'] = x
        path_list_first['y'] = y
        path_list_first['alpha'] = alpha
        path_list_first['beta'] = beta
        return path_list_first

    def setup_ompl_problem_world_space(self,world, mink_pts,start_pose, goal_pose,time_to_solve):
        self.world = world
        # self.cloud = cloud
        #print mink_pts
        #mink_pts[:, 0:1] = np.round(mink_pts[:, 0:1], 0)
        self.mink_pts = mink_pts
        self.mink_pts_kd = kd.cKDTree(mink_pts)
        # occ_ind = np.where(np.array(self.world.occupied_cell_bool) == 1)
        # self.occupied_coord = self.world.world_discretized[occ_ind]
        # # print "occupied coord before we get started: ", np.shape(self.occupied_coord), "type: ", type(self.occupied_coord)
        # self.occupied_coord_xy = self.occupied_coord[:,:2]
        # self.occupied_coord_yaw  = self.occupied_coord[:,2]

        bounds = ob.RealVectorBounds(4)
        bounds.setLow(0, world['xmin'])
        bounds.setLow(1, world['ymin'])
        bounds.setLow(2, world['amin'])
        bounds.setLow(3, world['bmin'])


        # print "world extents: ", world.extents, "requested goal: ", goal_pose, "requested start: ", start_pose

        bounds.setHigh(0,world['xmax'])
        bounds.setHigh(1,world['ymax'])
        bounds.setHigh(2,world['amax'])
        bounds.setHigh(3,world['bmax'])

        # print "world bounds: ", world.extents

        #space = ob.SE2StateSpace()
        # space = ob.SE3StateSpace()
        # space = MySE2()
        space = ob.RealVectorStateSpace(4)
        space.setBounds(bounds)

        start = ob.State(space)
        # we can pick a random start state...
        # start.random()
        # ... or set specific values
        start[0] = np.double(start_pose[0, 0])
        start[1] = np.double(start_pose[1, 0])
        start[2] = np.double(start_pose[2, 0])
        start[3] = np.double(start_pose[3, 0])

        goal = ob.State(space)
        # we can pick a random goal state...
        # goal.random()
        goal[0] = np.double(goal_pose[0, 0])
        goal[1] = np.double(goal_pose[1, 0])
        goal[2] = np.double(goal_pose[2, 0])
        goal[3] = np.double(goal_pose[3, 0])

        # goal().setX(goal_pose.item(0))
        # goal().setY(goal_pose.item(1))
        # # goal().setYaw(goal_pose.item(2))
        # goal().setZ(goal_pose.item(2))

        # print "start: ", start_pose, "\n goal: ", goal_pose

        si = ob.SpaceInformation(space)
        si.setStateValidityChecker(ob.StateValidityCheckerFn(self.isStateValid))

        pd = ob.ProblemDefinition(si)
        pd.setStartAndGoalStates(start,goal)

        planner = og.RRTConnect(si)
        planner.setProblemDefinition(pd)
        planner.setup()

        solved = planner.solve(time_to_solve)

        # print "tried to solve:" , solved

        if solved:
            path = pd.getSolutionPath()
            # ps = og.PathSimplifier(si)
            # ps.smoothBSpline(path) #makes bspline path out of path variable
            path_list = self.convert_path_to_list(path)
        else:
            path_list = {}

        #access elements of path_list with dict calls to 'x','y','yaw'
        return path_list

