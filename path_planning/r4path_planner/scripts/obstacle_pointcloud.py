# num_points = 0
# for point in range(0, num_points):
	# Block outer and inner grid points

	# Test each open grid for a collision with several random orientations within that gird

	# If no collision, do nothing. If a collision randomly place obstacle points all along those coordinates in the rotation space

import numpy as np
import matplotlib.pyplot as plt

class obstacle_pointcloud:
    def __init__(self, grid_size, density, random_scale = .1):
        '''
        Constructor
        '''
        self.grid_size = grid_size
        self.density = density
        self.random_scale = random_scale

    def ray_cast(self, a, b):
        v = np.zeros((self.density, 2))
        v[:, 0] = a
        v[:, 1] = b
        v /= np.linalg.norm(v[0, :])
        perp = np.copy(v)
        temp = perp[:, 0].copy()
        perp[:, 0] = perp[:, 1].copy()
        perp[:, 1] = -temp
        v *= self.grid_size
        rand_points = np.random.rand(self.density)
        v[:, 0] = rand_points*v[:, 0]
        v[:, 1] = rand_points*v[:, 1]
        print v

        rand_points = np.random.rand(self.density)
        rand_points -= .5
        perp[:, 0] = self.random_scale*rand_points*perp[:, 0]
        perp[:, 1] = self.random_scale*rand_points*perp[:, 1]

        v += perp
        print v

        plt.plot(v[:, 0], v[:, 1], 'ro')
        plt.axis([-self.grid_size, self.grid_size, -self.grid_size, self.grid_size])
        plt.grid(True)
        plt.show()







