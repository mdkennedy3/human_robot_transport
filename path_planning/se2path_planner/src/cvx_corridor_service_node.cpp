/*
* Copyright (c) <2016>, <Dinesh Thakur>
* All rights reserved.
*
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*   1. Redistributions of source code must retain the above copyright notice,
*   this list of conditions and the following disclaimer.
*
*   2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
*   3. Neither the name of the University of Pennsylvania nor the names of its
*   contributors may be used to endorse or promote products derived from this
*   software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "ros/ros.h"
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Point.h>
#include <nav_msgs/GetMap.h>
#include <nav_msgs/Path.h>
#include <tf/transform_datatypes.h>

#include <list>
#include <cmath>        // std::acos(double)
#include <se2path_planner/Mink3d.h>

#include <sbpl/headers.h>

#include <exception>

#include <Eigen/Core>

#include "se2path_planner/CvxCorr.h"

struct RankedPoint
{
  geometry_msgs::Point32 pt;
  double rank;
};

bool compFirst(const RankedPoint & a, const RankedPoint & b) { return a.rank < b.rank; }

class CvxCorridor
{
public:
  CvxCorridor();
  ~CvxCorridor();
  void publishStates();
private:
  bool serviceCallback(se2path_planner::CvxCorr::Request &req, se2path_planner::CvxCorr::Response  &res);

  ros::NodeHandle nh_;

  //ros::Subscriber path_sub_;
  //ros::ServiceClient map_client_, mink3d_client_;
  ros::ServiceServer cvx_server_;
};

CvxCorridor::CvxCorridor()
{
  nh_ = ros::NodeHandle("~");

  //mink3d_client_ = nh_.serviceClient<se2path_planner::Mink3d>("/mink3d");
  //path_sub_ = nh_.subscribe<nav_msgs::Path>("/se2path_planner_node/path", 1, boost::bind(&CvxCorridor::goalCallback, this, _1));
  cvx_server_ = nh_.advertiseService<se2path_planner::CvxCorr::Request, se2path_planner::CvxCorr::Response>("/cvx_corridor", boost::bind(&CvxCorridor::serviceCallback, this, _1, _2));

}

CvxCorridor::~CvxCorridor()
{

}

bool CvxCorridor::serviceCallback(se2path_planner::CvxCorr::Request  &req, se2path_planner::CvxCorr::Response &res)
{
  ros::Time begin = ros::Time::now();

  // Iterate through every path point (1:N-1)
  int n_poses = req.seed_path.poses.size();
  const int n_dimension = 3;
  typedef Eigen::Matrix<double, 1, n_dimension> NDimArray;

  sensor_msgs::PointCloud pc = req.obstacles;

  ROS_INFO("Number of path points %d, obs points %d", n_poses, pc.points.size());
  for(int i = 0; i < n_poses - 1; i++)
  {
    ROS_INFO("chord %d", i+1);

    NDimArray current_point, next_point;
    // current_point << req.seed_path.poses[i].pose.position.x, req.seed_path.poses[i].pose.position.y, tf::getYaw(req.seed_path.poses[i].pose.orientation);
    // next_point << req.seed_path.poses[i+1].pose.position.x, req.seed_path.poses[i+1].pose.position.y, tf::getYaw(req.seed_path.poses[i].pose.orientation);

    current_point << req.seed_path.poses[i].pose.position.x, req.seed_path.poses[i].pose.position.y, req.seed_path.poses[i].pose.position.z;
    next_point << req.seed_path.poses[i+1].pose.position.x, req.seed_path.poses[i+1].pose.position.y, req.seed_path.poses[i].pose.position.z;

    NDimArray l_vect, l_vect_normed;
    l_vect = next_point - current_point;
    l_vect_normed = l_vect.normalized();

    NDimArray gamma1, gamma1_normed;
    gamma1 = Eigen::MatrixXd::Ones(1,n_dimension) - current_point;
    gamma1_normed = gamma1.normalized();

    //check if gamma is aligned with l_vec
    double dot_prod = gamma1_normed.dot(l_vect_normed);
    double angle = std::acos(dot_prod);
    ROS_INFO_STREAM("dot " <<  dot_prod << " angle" << angle);

    if( (dot_prod > 0.98) | (dot_prod < -0.98))
    {
      gamma1 = Eigen::MatrixXd::Ones(1,n_dimension);
      gamma1(0,n_dimension-1) = -1.0;
      gamma1 = gamma1 - current_point;
      gamma1_normed = gamma1.normalized();
    }

    NDimArray gamma1_perp, gamma1_perp_normed;
    gamma1_perp = gamma1 - ((l_vect_normed*gamma1.transpose())*l_vect_normed);
    gamma1_perp_normed = gamma1_perp.normalized();

    Eigen::Matrix<double, 1, n_dimension + 1> Plane1;
    Plane1 << gamma1_perp_normed, -(gamma1_perp_normed*current_point.transpose());

    int k = 0;
    double a_prime = 1.0; //#just initialize

    for(int j = 0; j < n_dimension; j++)
    {
      double val = Plane1(0,j);
      if(std::fabs(val) > 0.001)
      {
        a_prime = val;
        break;
      }
      k += 1;
      if(k == n_dimension)
        ROS_ERROR("No viable coefficient found, in cvx_alg");
    }

    NDimArray gamma2, gamma2_normed;
    gamma2.setZero();

    double neg_b1 = (gamma1_perp_normed*current_point.transpose());
    double qk = neg_b1/a_prime;
    double a_prime2 = 1.0;

    if(std::fabs(neg_b1) > 0.0001)
    {
      gamma2(0,k) = qk;
    }
    else
    {
      int k2 = k+1;
      qk = 1.0;

      for(int j = k2; j < n_dimension; j++)
      {
        double val = Plane1(0,j);
        if(std::fabs(val) > 0.001)
        {
          a_prime2 = val;
          break;
        }
        k2 += 1;
        if(k2 == n_dimension)
          ROS_ERROR("No viable coefficient found, in cvx_alg");

        double qk2 = (neg_b1 - a_prime)/a_prime2;
        gamma2(0,k) = 1;
        gamma2(0,k2) = qk2;
      }
    }

    gamma2 = gamma2 - current_point;
    gamma2_normed = gamma2.normalized();

    ROS_INFO_STREAM("l_vect_normed " << l_vect_normed);
    ROS_INFO_STREAM("gamma2_normed " << gamma2_normed);

    dot_prod = gamma2_normed.dot(l_vect_normed);
    angle = std::acos(dot_prod);
    ROS_INFO_STREAM("dot " <<  dot_prod << " angle" << angle << " " << ((dot_prod > 0.98) | (dot_prod < -0.98)));

    if((dot_prod > 0.98) | (dot_prod < -0.98))
    {
      gamma2(0,n_dimension-1) = -1.0;
      gamma2_normed = gamma2.normalized();
    }

    NDimArray gamma2_perp, gamma2_perp_normed;
    gamma2_perp = gamma2 - (l_vect_normed*gamma2.transpose())*l_vect_normed;
    gamma2_perp_normed = gamma2_perp.normalized();

    Eigen::Matrix<double, 1, n_dimension + 1> Plane2;
    Plane2 << gamma2_perp_normed, (- gamma2_perp_normed*current_point.transpose());

    ROS_INFO_STREAM("a_prime " << a_prime);
    ROS_INFO_STREAM("a_prime2 " << a_prime2);

    ROS_INFO_STREAM("neg_b1 " << neg_b1 << " k " << k);

    ROS_INFO_STREAM("gamma1 " << gamma1);
    ROS_INFO_STREAM("gamma2 " << gamma2);

    ROS_INFO_STREAM("gamma1_perp " << gamma1_perp);
    ROS_INFO_STREAM("gamma2_perp " << gamma2_perp);

    ROS_INFO_STREAM("gamma1_perp_normed " << gamma1_perp_normed);
    ROS_INFO_STREAM("gamma2_perp_normed " << gamma2_perp_normed);

    ROS_INFO_STREAM("Plane1 " << Plane1);
    ROS_INFO_STREAM("Plane2 " << Plane2);

    //Rank all points in distance from center of chord
    std::list<RankedPoint> ranked_list;
    for(int j = 0; j < pc.points.size(); j++)
    {
      geometry_msgs::Point32 pt = pc.points[j];

      Eigen::Matrix<double, 1, n_dimension + 1> pt_vec;
      pt_vec << pt.x, pt.y, pt.z, 1;

      double d1 = Plane1 * pt_vec.transpose();
      double d2 = Plane2 * pt_vec.transpose();

      double rank = fabs(d1) + fabs(d2);
      RankedPoint rp;
      rp.pt = pt;
      rp.rank = rank;
      ranked_list.push_back(rp);
      //ROS_INFO_STREAM(" R " << rp.rank << " " << rp.pt.x <<  " " << rp.pt.y << " "  << rp.pt.z );
    }
    ranked_list.sort(compFirst);

    se2path_planner::PlaneCvx all_planes;

    auto it = ranked_list.begin();
    while(it != ranked_list.end())
    {
      RankedPoint rp = *it;

      Eigen::Matrix<double, 1, n_dimension> obs_pt;
      obs_pt << rp.pt.x, rp.pt.y, rp.pt.z;

      NDimArray obs_vect, obs_vect_perp, obs_vect_perp_normed;

      obs_vect = obs_pt - current_point;
      obs_vect_perp = obs_vect - (l_vect_normed * obs_vect.transpose())*l_vect_normed;

      if((obs_vect - obs_vect_perp).norm() > l_vect.norm())
        obs_vect_perp = obs_pt - next_point;
      else
      {
        if(l_vect_normed*obs_vect.transpose() <= 0)
          obs_vect_perp = obs_pt - current_point;
      }

      obs_vect_perp_normed = obs_vect_perp.normalized();
      Eigen::Matrix<double, 1, n_dimension + 1> obs_plane;
      obs_plane << obs_vect_perp_normed, (- obs_vect_perp_normed*obs_pt.transpose());

      se2path_planner::Plane pl;
      //Append generated plane
      for(int ii = 0; ii < n_dimension + 1; ii++)
        pl.plane.push_back(obs_plane(0,ii));

      all_planes.planes.push_back(pl);

      auto it2 = std::next(it);
      //ROS_ERROR_STREAM(obs_plane);

      //Remove points on other side of plane
      while(it2 != ranked_list.end())
      {
        RankedPoint rp2 = *it2;

        Eigen::Matrix<double, 1, n_dimension + 1> obs_pt2;
        obs_pt2 << rp2.pt.x, rp2.pt.y, rp2.pt.z, 1;

        double obs_signs_bar = obs_plane * obs_pt2.transpose();
        if(obs_signs_bar >= 0.0)
        {
          it2 = ranked_list.erase(it2);
        }
        else
          ++it2;
      }

      it = ranked_list.erase(it);
    }

    //Add the planes for current chord to the service response
    res.corridors.push_back(all_planes);
  }
  ros::Duration dur = ros::Time::now() - begin;
  ROS_INFO("Took %g sec", dur.toSec());
  ROS_INFO("done");

  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "cvx_corridor");

  CvxCorridor cvx_corridor;
  ros::Rate loop_rate(2);

  while (ros::ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  }
}
