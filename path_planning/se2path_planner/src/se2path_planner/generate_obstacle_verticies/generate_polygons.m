%This script is used to generate obstacles to test
clear all, close all, clc
figure(1); axis([-7,7,-3,3]);

num_polys = 2;  %number of obstacles
poly_list = cellmat;
for (idx = 1:num_polys) 
    poly = impoly;
    poly_list{idx} = poly;    
end

for (idx = 1:num_polys)
   disp(' Current:   ');     
    poly_list{idx}.getPosition
end

