from heapq import heappush, heappop
import numpy as np
import math

class node(object):
    def __init__(self, pose, goal, start, *parent):
        self.pose=pose
        self.parent = []
        if len(parent) > 0:
            self.parent = parent[0]
        self.h = np.linalg.norm(goal - pose)
        self.g = np.linalg.norm(start - pose)
        self.f = self.g + self.h



class My_Astar_class(object):
    def astar_calc(self, start, neighbors, goal, cost, estimate):
        print "solve astar given functions"
        # start_pose: ndim state (list or array)
        # goal:ndim state (list or array)
        # neighbors: function returning allowable neighbors of a point
        # cost: cost of moving bewteen quiered points
        # estimate: heuristic from queried point to goal
        path = []
        came_from = []
        closed_set = []
        start_node = node(start, goal, start)
        open_set = [start_node]

        gscore = [[start_node], 0]


        '''IM NOT DOING THE GSCORING CORRECTLY, I THINK THIS METHOD MAY JUST NEED THE ENTIRE MAP AND DO IT ROBUST AND CORRECT '''


        while len(open_set) >0:
            curr_node = open_set.pop(np.argmin([open_set[idx].f for idx in range(len(open_set))]))
            closed_set.append(curr_node)
            if np.linalg.norm(curr_node - goal) < 0.01: return self.reconstruct_path(came_from, curr_node)

            for neighbor in neighbors(curr_node.pose):
                neigh_bool = sum([neighbor == closed_set[idx].pose for idx in range(len(closed_set))])
                if neigh_bool > 0:
                    continue
                tent_gscore = curr_node.g + np.linalg.norm(curr_node.pose - neighbor)
                gscore_neigh = np.inf
                if not sum([neighbor == gscore[idx][0] for idx in range(len(gscore))]):
                    gscore.append([[neighbor], np.inf])

                if sum([neighbor == open_set[idx].pose for idx in range(len(open_set))]) < 1:
                    neigh_node = node(neighbor, goal, start, curr_node)
                    open_set.append(neigh_node)
                    gscore_neigh = neigh_node.g
                else:
                    gscore_neigh = neigh_node.g

                if tent_gscore >= gscore[np.argmax([neighbor == gscore[idx][0] for idx in range(len(gscore))])][1]:
                    continue


                if len(came_from) > 0 and sum([neighbor == came_from[idx][0] for idx in range(len(came_from))]):
                    came_from[np.argmax([neighbor == came_from[idx][0] for idx in range(len(came_from))])][0] = curr_node
                else:
                    came_from.append([[curr_node],[neigh_node]])

                if sum([neighbor == gscore[idx][0] for idx in range(len(gscore))]):
                    gscore[np.argmax([neighbor == gscore[idx][0] for idx in range(len(gscore))])][1] = tent_gscore



    def reconstruct_path(self, came_from, curr_node):
        path = []
        return path


        return path
