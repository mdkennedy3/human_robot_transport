import scipy.signal
import numpy as np
import sympy as sp


class QP_poly_cls(object):

    def poly_mat(self,f,a):
        Mat = []
        for ci in a.tolist():
            row = []
            for cj in a.tolist():
                row.append(0.5*sp.diff(f,ci[0],cj[0]))
            # print "row: ", row
            Mat.append(row)
        return Mat
    def poly_order(self,order,*args):
        a_big_list_str = 'a0'
        for idx in range(int(order)):
            a_big_list_str = 'a'+str(idx+1)+','+a_big_list_str
        a_big_list = sp.var(a_big_list_str)
        # a_big_list = sp.var('a12,a11,a10,a9,a8,a7,a6,a5,a4,a3,a2,a1,a0')
        t = sp.Symbol('t')
        tmat = [1]
        for idx in range(order):
            jdx = idx+1
            tmat.insert(0,t**jdx)
        a_list = np.matrix(a_big_list[-(order+1):]).T
        tmat = np.matrix(tmat)
        p = tmat*a_list; p = p.item(0)

        if len(args) >0:
            if args[0] == "Tmat":
                if type(args[1]) == str:
                    if args[1] == "vel":
                        pd = sp.diff(p,t)
                        f = sp.expand(scipy.signal.convolve(pd,pd))
                        f = sp.integrate(f,t)
                        mat = self.poly_mat(f,a_list)
                        T_obj = np.matrix(mat)
                    elif args[1] == "acc":
                        pd = sp.diff(p,t)
                        pdd = sp.diff(pd,t)
                        f = sp.expand(scipy.signal.convolve(pdd,pdd))
                        f = sp.integrate(f,t)
                        mat = self.poly_mat(f,a_list)
                        T_obj = np.matrix(mat)
                    elif args[1] == "jerk":
                        pd = sp.diff(p,t)
                        pdd = sp.diff(pd,t)
                        pddd = sp.diff(pdd,t)
                        f = sp.expand(scipy.signal.convolve(pddd,pddd))
                        f = sp.integrate(f,t)
                        mat = self.poly_mat(f,a_list)
                        T_obj = np.matrix(mat)
                    if len(args) == 3:
                        T_obj = sp.Matrix(T_obj).subs(t,args[2])
                elif type(args[1]) == int or type(args[1]) == float:

                    # pd_iter = sp.diff(p,t)
                    # for der_index in range(1,int(args[1])):
                    #     #get the higher order derivative
                    #     pd_iter = sp.diff(pd_iter,t)
                    pd_iter = p
                    for der_index in range(0,int(args[1])):
                        #get the higher order derivative
                        pd_iter = sp.diff(pd_iter,t)

                    f = sp.expand(scipy.signal.convolve(pd_iter,pd_iter))
                    f = sp.integrate(f,t)
                    mat = self.poly_mat(f,a_list)
                    T_obj = np.matrix(mat)
                if len(args) == 4:
                    T_obj_ti = sp.Matrix(T_obj).subs(t,args[2])
                    T_obj_tf = sp.Matrix(T_obj).subs(t,args[3])
                    T_obj = T_obj_tf - T_obj_ti

                return T_obj




            elif args[0] == "Tvect":
                tvect = []
                for ci in a_list.tolist():
                    tvect.append(sp.diff(p,ci[0]))
                    tvect_diff = []
                    for idx in range(len(tvect)): tvect_diff.append(sp.diff(tvect[idx],t))
                    tvect_ddiff = []
                    for idx in range(len(tvect)): tvect_ddiff.append(sp.diff(tvect[idx],t))
                    tvect_dddiff = []
                    for idx in range(len(tvect)): tvect_dddiff.append(sp.diff(tvect[idx],t))
                if args[1] == "pos":
                    T_obj = np.matrix(tvect).T
                elif args[1] == "vel":
                    T_obj = np.matrix(tvect_diff).T
                elif args[1] == "acc":
                    T_obj = np.matrix(tvect_ddiff).T
                elif args[1] == "jerk":
                    T_obj = np.matrix(tvect_dddiff).T
                if len(args) == 3:
                    T_obj = sp.Matrix(T_obj).subs(t,args[2])
                    T_obj = np.matrix(T_obj)
                return T_obj



"""Usage: """
# T = cls.poly_order(3,"Tvect","pos")
# print "positions for 3rd order poly:" ,T
# T = cls.poly_order(3,"Tvect","vel")
# print "positions for 3rd order poly:" ,T
# T = cls.poly_order(3,"Tvect","acc")
# print "positions for 3rd order poly:" ,T
# T = cls.poly_order(3,"Tvect","jerk")
# print "positions for 3rd order poly:" ,T


# """This Tmat is of form: a^t T a for coeff a = [a^n,a^n-1,...a0], which means these substituded with the Ti can be used to build the Q and diagonally stack them """
# T = cls.poly_order(3,"Tmat","vel")
# print "Tmat for vel, 3rd order poly:" ,T
# T = cls.poly_order(3,"Tmat","acc")
# print "Tmat for acc, 3rd order poly:" ,T
# T = cls.poly_order(3,"Tmat","jerk")
# print "Tmat for jerk, 3rd order poly:" ,T

# ti= 0.3
# T = cls.poly_order(3,"Tmat","vel",ti)
# print "Tmat for vel, 3rd order poly, sub ti= 0.3:" ,T

# ti= 0.6
# T = cls.poly_order(3,"Tmat","vel",ti)
# print "Tmat for vel, 3rd order poly, sub ti= 0.6:" ,T


# #produces different result that 0:0.3, but to be expected, your connecting at different points in traj, not required to start from zero
# ti= 0.3; tf = 0.6
# T = cls.poly_order(3,"Tmat","vel",ti,tf)
# print "Tmat for vel, 3rd order poly, sub ti= 0.3, tf = 0.6:" ,T


