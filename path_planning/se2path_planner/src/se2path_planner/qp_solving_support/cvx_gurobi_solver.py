import sympy as sp
import numpy as np
import scipy
from scipy import linalg 
from scipy import sparse
import time
from gurobipy import *
from se2path_planner.qp_solving_support import poly_qp


class Gurobi_solver_cls(object):
    def __init__(self):
        #Define poly object for getting the objective function
        self.poly_qp_obj = poly_qp.QP_poly_cls()

    def obtain_time_matrix_gurobi(self,time, poly_order, diff_order, space_dim):
        if diff_order == 0:
            tvect = self.poly_qp_obj.poly_order(poly_order, "Tvect","pos",time).T
            tvect = tvect.tolist()[0]
            tvect= np.double(tvect)
            Tmat = self.Tmat_ndim(tvect,space_dim)

        elif diff_order == 1:
            tvect = self.poly_qp_obj.poly_order(poly_order, "Tvect","vel",time).T
            tvect = tvect.tolist()[0]
            tvect= np.double(tvect)
            Tmat = self.Tmat_ndim(tvect,space_dim)

        elif diff_order == 2:
            tvect = self.poly_qp_obj.poly_order(poly_order, "Tvect","acc",time).T
            tvect = tvect.tolist()[0]
            tvect= np.double(tvect)
            Tmat = self.Tmat_ndim(tvect,space_dim)

        return Tmat

    def Tmat_ndim(self,tvect,space_dim):
        Tmat = []
        for idx in range(int(space_dim)):
            Tmat.append(tvect)
        Tmat = scipy.sparse.block_diag(Tmat)
        Tmat = Tmat.todense()
        Tmat = np.matrix(Tmat)
        return Tmat


    def Gurobi_solver(self,spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose, *minimization_derivative):
        #INPUT: 
        #1. spacial_dimension (e.g. 3) 
        #2. num_chords (number of seed path chords) 
        #3. poly_order (polynomial order) 
        #4. Ti_discrete (list of times from 0:1) 
        #5. chord_sub_division (how many points to check in optimization for a single chord to be within inequality constraints)
        #6. inequality_constraints: list of lists containing rows with planes
        #7. start_pose (n dim start position) (assumes start velocity of zero)
        #8. goal_pose (n dim start position) (assumes goal velocity of zero)
        #9* min deriviative: type of deriviative (1 is vel, 2 is acc, 3 is jerk)
        #OUTPUT:
        #1. alpha_mat, [[coeff_chord_1],..,[coeff_chord_(k-1)]] (list of k-1 coefficients) correlated to the time matrix Ti_0_to_t
        #2. Ti_0_to_t: [0,dt_1,dt_2,...] k values

        start_pose = np.matrix(start_pose);goal_pose = np.matrix(goal_pose)
        print "\nGurobi Trajectory Generation...\n"
        """Get time in correct format"""
        Ti_0_to_t = [0]
        for idx in range(len(Ti_discrete)-1):
            dt = Ti_discrete[idx+1] - Ti_discrete[idx]
            Ti_0_to_t.append(dt)
        """Create a new model"""
        m = Model("qp")

        #Usage: # ti= 0.3; tf = 0.6; # T = cls.poly_order(3,"Tmat","vel",ti,tf)  #produces the integrated:   int_ti^tf Q for alpha' Q alpha; # print "Tmat for vel, 3rd order poly, sub ti= 0.3, tf = 0.6:" ,T
        """Dynamically store variables (coefficients)"""
        alpha_list = []
        for idx in range(int(num_chords*((poly_order+1)*spacial_dimension))):
            alpha_list.append(m.addVar(lb=-GRB.INFINITY))
        alpha_array = np.array(alpha_list)
        alpha_mat = alpha_array.reshape(num_chords,np.max(alpha_array.shape)/(num_chords))
        # Integrate new variables
        alpha_vect = np.matrix(alpha_array).T
        m.update()
        """Set Objectives"""
        obj_list = []
        for idx in range(len(Ti_0_to_t)-1):
            print "alpha mat: ", alpha_mat.shape, "curr idx: ", idx
            alpha_vect = np.matrix(alpha_mat[idx]).T 
            ti= 0.; tf = Ti_0_to_t[idx+1]
            if len(minimization_derivative) == 0:
                tmat = np.double(np.matrix(self.poly_qp_obj.poly_order(poly_order,"Tmat","vel",ti,tf)))
            else:
                tmat = np.double(np.matrix(self.poly_qp_obj.poly_order(poly_order,"Tmat",int(minimization_derivative[0]),ti,tf)))
            Tmat = self.Tmat_ndim(tmat,spacial_dimension)
            obj_elem = alpha_vect.T*Tmat*alpha_vect
            obj_elem  = obj_elem.tolist()[0][0]
            obj_list.append(obj_elem)
        obj = np.sum(obj_list)
        obj=0.0
        m.setObjective(obj) #Set the objective function 
        m.update()
        """Set Equalities"""
        t = Ti_0_to_t[0]#set start pose equalities
        Tmat_pos = self.obtain_time_matrix_gurobi(t, poly_order, 0,spacial_dimension)
        Tmat_vel = self.obtain_time_matrix_gurobi(t, poly_order, 1,spacial_dimension)
        coeff_pose = np.matrix(Tmat_pos)*np.matrix(alpha_mat[0]).T
        coeff_vel = np.matrix(Tmat_vel)*np.matrix(alpha_mat[0]).T
        for index in range(coeff_pose.shape[0]):
            m.addConstr(coeff_pose.item(index), GRB.EQUAL, start_pose.item(index))
            m.update()
        for index in range(coeff_vel.shape[0]):
            m.addConstr(coeff_vel.item(index), GRB.EQUAL, 0.0)
            m.update()
        t = Ti_0_to_t[-1]#set goal pose equalities
        Tmat_pos = self.obtain_time_matrix_gurobi(t, poly_order, 0,spacial_dimension)
        Tmat_vel = self.obtain_time_matrix_gurobi(t, poly_order, 1,spacial_dimension)
        coeff_pose = np.matrix(Tmat_pos)*np.matrix(alpha_mat[-1]).T
        coeff_vel = np.matrix(Tmat_vel)*np.matrix(alpha_mat[-1]).T
        for index in range(coeff_pose.shape[0]):
            m.addConstr(coeff_pose.item(index), GRB.EQUAL, goal_pose.item(index))
            m.update()
        for index in range(coeff_vel.shape[0]):
            m.addConstr(coeff_vel.item(index), GRB.EQUAL, 0.0)
            m.update()
        #set the intermediate connection equalities
        if len(Ti_0_to_t) > 2:
            for idx in range(int(len(Ti_0_to_t)-2)):
                t_chord_i = Ti_0_to_t[idx+1] #end of last chord
                t_chord_j = 0.0 #beginning of next chord

                Tmat_pos_i = self.obtain_time_matrix_gurobi(t_chord_i, poly_order, 0,spacial_dimension)
                Tmat_vel_i = self.obtain_time_matrix_gurobi(t_chord_i, poly_order, 1,spacial_dimension)
                Tmat_acc_i = self.obtain_time_matrix_gurobi(t_chord_i, poly_order, 2,spacial_dimension)

                Tmat_pos_j = self.obtain_time_matrix_gurobi(t_chord_j, poly_order, 0,spacial_dimension)
                Tmat_vel_j = self.obtain_time_matrix_gurobi(t_chord_j, poly_order, 1,spacial_dimension)
                Tmat_acc_j = self.obtain_time_matrix_gurobi(t_chord_j, poly_order, 2,spacial_dimension)

                coeff_pose_i = np.matrix(Tmat_pos_i)*np.matrix(alpha_mat[idx]).T
                coeff_vel_i = np.matrix(Tmat_vel_i)*np.matrix(alpha_mat[idx]).T
                coeff_acc_i = np.matrix(Tmat_acc_i)*np.matrix(alpha_mat[idx]).T
                coeff_pose_j = np.matrix(Tmat_pos_j)*np.matrix(alpha_mat[idx+1]).T
                coeff_vel_j = np.matrix(Tmat_vel_j)*np.matrix(alpha_mat[idx+1]).T
                coeff_acc_j = np.matrix(Tmat_acc_j)*np.matrix(alpha_mat[idx+1]).T

                # if len(inequality_constraints) > 0:
                #     A_i = inequality_constraints[idx]
                #     A_j = inequality_constraints[idx+1]

                #     A_list = [A_i,A_j]

                #     for index in range(coeff_pose_i.shape[0]):
                #         m.addConstr(coeff_pose_i.item(index), GRB.EQUAL, coeff_pose_j.item(index))
                #         m.addConstr(coeff_vel_i.item(index), GRB.EQUAL, coeff_vel_j.item(index))
                #         # m.addConstr(coeff_acc_i.item(index), GRB.EQUAL, coeff_acc_j.item(index))  #could add now, there was a typo before
                #         # m.addConstr(coeff_acc_i.item(index), GRB.EQUAL, 0.0)
                #         # m.addConstr(coeff_acc_j.item(index), GRB.EQUAL, 0.0)
                #         # if index in [0,1]:
                #         #     m.addConstr(coeff_vel_i.item(index), GRB.LESS_EQUAL, 10)
                #         #     m.addConstr(coeff_vel_j.item(index),GRB.LESS_EQUAL, 10)
                #         #     m.addConstr(coeff_vel_i.item(index), GRB.GREATER_EQUAL, -10)
                #         #     m.addConstr(coeff_vel_j.item(index),GRB.GREATER_EQUAL, -10)
                #         #now make both points interior to both Ai,Aj

                #         for A_curr in A_list:
                #             for kdx in range(len(A_curr)):
                #                 #ensure point from chord i is in bounded region
                #                 ineq_var = np.matrix(A_curr[kdx][:coeff_pose_i.shape[0]])*coeff_pose_i
                #                 ineq_var = ineq_var.tolist()[0][0]
                #                 ineq_b_val = A_curr[kdx][-1]
                #                 m.addConstr(ineq_var,GRB.LESS_EQUAL,-ineq_b_val)
                #                 #ensure point from chord j is in bounded region
                #                 ineq_var = np.matrix(A_curr[kdx][:coeff_pose_j.shape[0]])*coeff_pose_j
                #                 ineq_var = ineq_var.tolist()[0][0]
                #                 ineq_b_val = A_curr[kdx][-1]
                #                 m.addConstr(ineq_var,GRB.LESS_EQUAL,-ineq_b_val)
        m.update()



        # """Set Inequality Constraints"""
        # for idx in range(len(inequality_constraints)):
        #     A_curr = inequality_constraints[idx] #list of inequality constraints for the idx'th chord
        #     coeff_curr = alpha_mat[idx].T #get the current coefficients for this chord
        #     ti = Ti_0_to_t[idx+1]
        #     time_sub_divide =np.linspace(0,ti,chord_sub_division) 
        #     # for time in range(1,int(len(Ti_0_to_t)-1)):
        #     for time_idx in range(len(time_sub_divide)):
        #         # Tmat_pos = self.obtain_time_matrix_gurobi(time,poly_order,0,spacial_dimension) #order, space_dim
        #         Tmat_pos = self.obtain_time_matrix_gurobi(time_sub_divide[time_idx],poly_order,0,spacial_dimension) #order, space_dim
        #         point = Tmat_pos*np.matrix(coeff_curr).T
        #         for kdx in range(len(A_curr)):
        #             ineq_var = np.matrix(A_curr[kdx][:point.shape[0]])*point
        #             ineq_var = ineq_var.tolist()[0][0]
        #             ineq_b_val = A_curr[kdx][-1]
        #             # print "\n\n Aset: ", idx, ", Acurr: ", A_curr, ", ineq: ", ineq_var,", bval: ", ineq_b_val
        #             m.addConstr(ineq_var,GRB.LESS_EQUAL,-ineq_b_val)
        # m.update()

                #PUT LIMITS ON ACCELERATION OR CURVATURE::: DIDN"T WORK
                # Tmat_acc = self.obtain_time_matrix_gurobi(time_sub_divide[time_idx],poly_order,2,spacial_dimension) #order, space_dim
                # point_acc = Tmat_acc*np.matrix(coeff_curr).T
                # for index in range(point_acc.shape[0]):
                #     m.addConstr(point_acc.item(index), GRB.LESS_EQUAL, 3.0) #SET THIS IN ARGUMENT
                #     m.update()

        """Optimize"""
        m.optimize()
        """Prep debug"""
        try:
            #to debug and see which rows are infeasible
            m.computeIIS()
            m.write('gurobi_problem_crash.ilp')
        except:
            pass

        """Send solution as a vector"""
        alpha_vector = []
        print "\n coefficient vars", m.getVars(), " \n end of list\n"
        for v in m.getVars():
            print('%s %g' % (v.varName, v.x))
            alpha_vector.append(v.x)
        alpha_vector = np.array(alpha_vector)
        alpha_mat = alpha_vector.reshape(num_chords,np.max(alpha_vector.shape)/(num_chords))
        alpha_mat  = alpha_mat.tolist()
        #???Generate Path points (possibly use old matrix as it is the same)??
        return alpha_mat,Ti_0_to_t


