import sympy as sp
import numpy as np
import scipy
#!/usr/bin/env python2
import time, copy
import numpy as np, math; import sympy as sp; import rospy
from gurobipy import *; import copy, scipy; from scipy import linalg
from scipy import sparse
#For visualization
import rospy
from nav_msgs.msg import Path
from geometry_msgs.msg import Pose, Point, Quaternion, PoseStamped
import scipy; from scipy import linalg
from visualization_msgs.msg import Marker, MarkerArray



class Optimizer(object):
    """docstring for Optimizer"""
    def __init__(self, spacial_dimension,num_chords,poly_order,Ti_discrete, chord_subdivide,inequality_constraints,start_pose, goal_pose, *minimization_derivative):
        self.start_pose = start_pose
        self.goal_pose = goal_pose
        self.poly_order = poly_order
        self.num_chords = num_chords
        self.spacial_dimension = spacial_dimension
        self.chord_subdivide = chord_subdivide
        self.inequality_constraints = inequality_constraints
        self.Ti_discrete = Ti_discrete
        self.m = Model("qp")

        #Eventually add in variable derivative objective with minimization_derivative

        #for visualization
        self.path_pub = rospy.Publisher('/se2path_optimized',Path, queue_size=1, latch=True)
        self.plane_pub = rospy.Publisher('/cvx_planes', MarkerArray, queue_size=1, latch=True)
        self.point_step_pub = rospy.Publisher('/opt_path_pts', MarkerArray, queue_size=1, latch=True)
        self.marker_array = MarkerArray()
        self.pt_step_pub_marker_array = MarkerArray()
        self.poly_order = 3
        self.marker_ids = 0
        self.pt_marker_id = 0

    def time_basis(self,t,poly_order):
        t_basis = []
        for idx in range(poly_order+1): t_basis.append(t**idx)
        return t_basis
        
    def create_time_components(self):
        # self.Ti_discrete = [0]
        # for idx in range(self.num_chords): self.Ti_discrete.append(self.Ti_discrete[-1] + 1./self.num_chords)      
        self.Ti_0_to_t = [0]; 
        for idx in range(len(self.Ti_discrete)-1): dt = self.Ti_discrete[idx+1] - self.Ti_discrete[idx]; self.Ti_0_to_t.append(dt)
        self.t = sp.Symbol('t'); self.t_basis = []; 
        for idx in range(self.poly_order+1): self.t_basis.append(self.t**idx)

    def define_coefficients(self):
        #Add coefficients
        """ Define Coefficients """
        self.coeff = []
        for kdx in range(self.num_chords):
            #chord level
            chord_list = []
            for jdx in range(self.spacial_dimension):
                #dimension level e.g. (x,y,z,...)
                ceoff_poly_list = []
                for idx in range(self.poly_order+1):
                    #ceoff level e.g. (c0 + c1*t + ... cn*t**n)
                    ceoff_poly_list.append(self.m.addVar(lb=-GRB.INFINITY))
                chord_list.append(ceoff_poly_list)
            self.coeff.append(chord_list)
        self.m.update() #update before defining objective function

    def define_objective_fnct_mat(self):
        """ Define Objective Function """
        J_test = []
        T_vect = sp.Matrix(self.t_basis); 
        Td_vect = sp.diff(T_vect,self.t) 
        Td_conv = Td_vect*Td_vect.T #outer product, producing convolution matrix
        Qd_conv = sp.integrate(Td_conv,self.t) #integrate wrt t
        for kdx in range(self.num_chords):
            for jdx in range(self.spacial_dimension):
                Qd_conv_curr = copy.copy(Qd_conv) #deep copy to not overwrite
                Qr  = Qd_conv_curr.subs(self.t,self.Ti_0_to_t[kdx+1]) #substitute integrated convolution
                for ii in range(self.poly_order+1):
                    for jj in range(self.poly_order+1):
                        J_test.append(Qr[ii,jj] *self.coeff[kdx][jdx][ii] * self.coeff[kdx][jdx][jj])       
        Jsum = sum(J_test)
        # print "this is the objective funct: ", Jsum
        self.m.setObjective(Jsum, GRB.MINIMIZE)

    def define_objective_fnct_conv(self):
        """ Define Objective Function """
        Jconv = 0.0
        for kdx in range(self.num_chords):
            for jdx in range(self.spacial_dimension):
                polynomial = sum([self.coeff[kdx][jdx][idx]*sp.diff(self.t_basis[idx]).subs(self.t,self.Ti_0_to_t[kdx+1]) for idx in range(self.poly_order+1)])
                Jconv += scipy.convolve(polynomial,polynomial)[0]   
        self.m.setObjective(Jconv, GRB.MINIMIZE)

    def define_equality_constriants(self):
        """ Define Equality Constraints """
        #Start Pose  k=1 (first chord)
        for jdx in range(self.spacial_dimension):
            p_t0 = 0.0; pd_t0 = 0.0
            tvect_subs = sp.Matrix(self.t_basis).subs(self.t,0.0)
            tdvect_subs = sp.diff(sp.Matrix(self.t_basis),self.t).subs(self.t,0.0)
            for idx in range(self.poly_order+1): 
                p_t0 += self.coeff[0][jdx][idx]* tvect_subs[idx]  #polynomial for the j'th dimension
                pd_t0 += self.coeff[0][jdx][idx] * tdvect_subs[idx]    
            self.m.addConstr(p_t0, GRB.EQUAL, self.start_pose[jdx]) #set start position equal to start_pose
            self.m.addConstr(pd_t0, GRB.EQUAL, 0.0) #set start velocity to zero
            self.m.update()
        #Connect Position/Velocity of all intermediate chords
        if len(self.Ti_0_to_t) > 2:
            for kdx in range(self.num_chords-1):
            # for kdx in range(int(len(Ti_0_to_t)-2)):
                for jdx in range(self.spacial_dimension):
                    pk_i = 0.; pdk_i = 0. #current chord
                    pk_j = 0.; pdk_j = 0.#next chord                      
                    t_chord_i = self.Ti_0_to_t[kdx+1] #end of last chord
                    t_chord_j = 0.0 #beginning of next chord              
                    for idx in range(self.poly_order+1): 
                        pk_i  += self.coeff[kdx][jdx][idx]  *sp.Matrix(copy.copy(self.t_basis))[idx].subs(self.t,t_chord_i);                                             
                        pk_j  += self.coeff[kdx+1][jdx][idx]*sp.Matrix(copy.copy(self.t_basis))[idx].subs(self.t,t_chord_j)   #polynomial for the j'th dimension                    
                        pdk_i += self.coeff[kdx][jdx][idx]  *sp.diff(sp.Matrix(copy.copy(self.t_basis))[idx],self.t).subs(self.t,t_chord_i);                              
                        pdk_j += self.coeff[kdx+1][jdx][idx]*sp.diff(sp.Matrix(copy.copy(self.t_basis))[idx],self.t).subs(self.t,t_chord_j) #obtain derviatives      
                    self.m.addConstr( pk_i, GRB.EQUAL,  pk_j) #set pos equal at chord connections
                    self.m.addConstr(pdk_i, GRB.EQUAL, pdk_j) #set vel equal at chord connections
                    self.m.update()
                #Now ensure that both points lie within overlapping convex regions
                if len(self.inequality_constraints) > 0:
                    for kdx_add in range(2):                        
                        for mdx in range(len(self.inequality_constraints[kdx+kdx_add].planes)):
                            t_basis_i = self.time_basis(t_chord_i,self.poly_order)
                            t_basis_j = self.time_basis(t_chord_j,self.poly_order)
                            
                            poly_vect_i =[sum([self.coeff[kdx][jdx][idx]*t_basis_i[idx] for idx in range(self.poly_order+1)]) for jdx in range(self.spacial_dimension)]
                            poly_vect_i.append(1)
                            
                            poly_vect_j =[sum([self.coeff[kdx+1][jdx][idx]*t_basis_j[idx] for idx in range(self.poly_order+1)]) for jdx in range(self.spacial_dimension)]
                            poly_vect_j.append(1)
                            
                            plane_curr = self.inequality_constraints[kdx+kdx_add].planes[mdx].plane
                            plane = np.matrix(plane_curr)
                            #define inequality variable
                            ineq_var_i = plane*np.matrix(poly_vect_i).T
                            ineq_var_i = ineq_var_i.tolist()[0][0]

                            ineq_var_j = plane*np.matrix(poly_vect_j).T
                            ineq_var_j = ineq_var_j.tolist()[0][0]

                            # print "ineq var1 to add: ", ineq_var
                            self.m.addConstr(ineq_var_i, GRB.LESS_EQUAL, 0)
                            self.m.addConstr(ineq_var_j, GRB.LESS_EQUAL, 0)
                            self.m.update()


        #Goal Pose k = num_chords
        for jdx in range(self.spacial_dimension):
            p_tf = 0.
            pd_tf = 0.
            for idx in range(self.poly_order+1): 
                p_tf  += self.coeff[self.num_chords-1][jdx][idx]*sp.Matrix(self.t_basis)[idx].subs(self.t,self.Ti_0_to_t[-1])  #polynomial for the j'th dimension
                pd_tf += self.coeff[self.num_chords-1][jdx][idx]*sp.diff(sp.Matrix(self.t_basis)[idx],self.t).subs(self.t,self.Ti_0_to_t[-1]) #polynomial for the j'th dimension
            self.m.addConstr(p_tf, GRB.EQUAL, self.goal_pose[jdx]) #set start position equal to start_pose
            self.m.addConstr(pd_tf, GRB.EQUAL, 0.0) #set start velocity to zero
        self.m.update() #update equality constraints


    def optimize_and_return(self):
        self.m.optimize()

        alpha_vector = []
        # print "\n coefficient vars", self.m.getVars(), " \n end of list\n"
        for v in self.m.getVars():
            # print('%s %g' % (v.varName, v.x))
            alpha_vector.append(v.x)
        alpha_vector = np.array(alpha_vector)
        alpha_mat = alpha_vector.reshape(self.num_chords,np.max(alpha_vector.shape)/(self.num_chords))
        self.alpha_mat  = alpha_mat.tolist()
        return self.alpha_mat, self.Ti_0_to_t

    def disp_main_points(self):
        #display the critical points of the polynomials
        pose_list = []
        pose_list.append(self.start_pose)
        for idx in range(len(self.Ti_0_to_t)-1):
            tcurr = self.Ti_0_to_t[idx+1]
            dt_vect = [tcurr**mdx for mdx in range(self.poly_order+1)]; dt_list = [dt_vect for mdx in range(self.spacial_dimension)]
            Tmat = scipy.sparse.block_diag(np.array(dt_list))
            coeff_vect = np.matrix(self.alpha_mat[idx]).T      
            pose_curr= Tmat * coeff_vect; pose_list.append(pose_curr.T.tolist()[0])
        pmat = np.matrix(pose_list)
        # print "\n\n Pose list: ",pmat

    
    def inequality_constraint_with_struct(self):    
        # self.inequality_constraints = self.build_constraint_struct()
        if len(self.inequality_constraints) > 0:
            for kdx in range(self.num_chords):
                t_sub = np.linspace(0,self.Ti_0_to_t[kdx+1],self.chord_subdivide)
                if kdx == 0: t_sub = t_sub[1:]
                if kdx == self.num_chords-1: t_sub = t_sub[:-2]
                for time_idx in t_sub:
                    for mdx in range(len(self.inequality_constraints[kdx].planes)):
                        t_basis = self.time_basis(time_idx,self.poly_order)
                        poly_vect =[sum([self.coeff[kdx][jdx][idx]*t_basis[idx] for idx in range(self.poly_order+1)]) for jdx in range(self.spacial_dimension)]
                        poly_vect.append(1)
                        # print "tbasis: ", np.matrix(t_basis), "\n pvect", np.matrix(poly_vect).T                  
                        plane_curr = self.inequality_constraints[kdx].planes[mdx].plane
                        plane = np.matrix(plane_curr)
                        #define inequality variable
                        ineq_var = plane*np.matrix(poly_vect).T
                        ineq_var = ineq_var.tolist()[0][0]
                        # print "ineq var1 to add: ", ineq_var
                        self.m.addConstr(ineq_var, GRB.LESS_EQUAL, 0)
                        self.m.update()
  

    def display_optimal_path(self):
        # self.poly_order = poly_order
        print "display the optimal path in rviz"
        path_list = Path(); path_list.header.frame_id = '/map'
        pose_list = []; pose_list.append(self.start_pose)
        for iidx in range(len(self.Ti_0_to_t)):
            if iidx == 0: 
                idx = 0
                tcurr = self.Ti_0_to_t[idx]
            elif iidx > 0:
                idx = iidx - 1 
                tcurr = self.Ti_0_to_t[idx+1]

            dt_vect = [tcurr**mdx for mdx in range(self.poly_order+1)]; dt_list = [dt_vect for mdx in range(self.spacial_dimension)]
            Tmat = scipy.sparse.block_diag(np.array(dt_list))
            coeff_vect = np.matrix(self.alpha_mat[idx]).T      
            pose_curr= Tmat * coeff_vect; pose_list.append(pose_curr.T.tolist()[0])
        
            pose = [0,0,0]
            # print "pre pose: ", pose
            for ii in range(self.spacial_dimension): pose[ii] = pose_curr.item(ii)
            # print "pose pose: ", pose
            flag = 1
            marker_curr= self.generate_pt_marker(pose,flag)
            self.pt_step_pub_marker_array.markers.append(marker_curr)
            pose_stamped = PoseStamped()
            pose_stamped.header.frame_id = '/map'
            # print "current pos", pose_curr, " current reported pose: ", pose
            pose_stamped.pose = Pose(Point(pose[0],pose[1],pose[2]),Quaternion(0,0,0,1))
            path_list.poses.append(pose_stamped)




        pmat = np.matrix(pose_list)
        print "\n\n Pose list in rviz: ",pmat
        self.path_pub.publish(path_list)
        self.point_step_pub.publish(self.pt_step_pub_marker_array)
        print "published path"

    def generate_pt_marker(self, point, flag):
        pt_marker = Marker()
        pt_marker.header.frame_id = "map"
        pt_marker.header.stamp = rospy.Time()
        pt_marker.id = self.pt_marker_id; self.pt_marker_id += 1
        pt_marker.type = 2
        pt_marker.pose.position.x = point[0]
        pt_marker.pose.position.y = point[1]
        pt_marker.pose.position.z = point[2]
        pt_marker.pose.orientation.x = 0.
        pt_marker.pose.orientation.y = 0.
        pt_marker.pose.orientation.z = 0.
        pt_marker.pose.orientation.w = 1.
        pt_marker.scale.x = 0.05
        pt_marker.scale.y = 0.05
        pt_marker.scale.z = 0.05
        pt_marker.color.a = 1.0
        if flag == 0:
            pt_marker.color.r = 1.0
            pt_marker.color.g = 1.0
            pt_marker.color.b = .0
        else:
            pt_marker.color.r = 1.0
            pt_marker.color.g = .0
            pt_marker.color.b = .0
        return pt_marker


    def run_optimization(self, *visualize_bool):    
        self.create_time_components()
        self.define_coefficients()
        self.define_objective_fnct_mat()  #convolution via matrix
        # opt_cls.define_objective_fnct_conv() #convolutino via fnct call
        self.define_equality_constriants()
        self.inequality_constraint_with_struct()
        alpha_mat, Ti_0_to_t = self.optimize_and_return()
        opt_path = {"coeff":alpha_mat, "T":Ti_0_to_t}
        if len(visualize_bool) > 0:
            if visualize_bool[0] == True:
                self.display_optimal_path()
                self.disp_main_points()
        return opt_path










class Gurobi_Solver_Simple(object):
    """docstring for Gurobi_Class_Simple"""

    def time_basis(self,t,poly_order):
        t_basis = []
        for idx in range(poly_order+1): t_basis.append(t**idx)
        return t_basis

    def Gurobi_solver(self,spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose, *minimization_derivative):
        """
        INPUT: 
        1. spacial_dimension (e.g. 3) 
        2. num_chords (number of seed path chords) 
        3. poly_order (polynomial order) 
        4. Ti_discrete (list of times from 0:1) 
        5. chord_sub_division (how many points to check in optimization for a single chord to be within inequality constraints)
        6. inequality_constraints: list of lists containing rows with planes:: NEW VERSION EXPECTS TYPE: A.corridor[].planes[].plane[]
        7. start_pose (n dim start position) (assumes start velocity of zero)
        8. goal_pose (n dim start position) (assumes goal velocity of zero)
        9* min deriviative: type of deriviative (1 is vel, 2 is acc, 3 is jerk)
        OUTPUT:
        1. alpha_mat, [[coeff_chord_1],..,[coeff_chord_(k-1)]] (list of k-1 coefficients) correlated to the time matrix Ti_0_to_t
        2. Ti_0_to_t: [0,dt_1,dt_2,...] k values
        """

        print "\nGurobi Trajectory Generation...\n"
        """Get time in correct format: [0,dt1, dt2, ..., dtk]"""
        Ti_0_to_t = [0]
        for idx in range(len(Ti_discrete)-1): dt = Ti_discrete[idx+1] - Ti_discrete[idx]; Ti_0_to_t.append(dt)
        # print "\n\n Ti_0_t: ", Ti_0_to_t
        """Create a new model"""
        m = Model("qp")
        #Define symbolic time, this is requried for an 1. Integration operation 2. Ease of substitutiton 
        t = sp.Symbol('t'); t_basis = []; 
        #Define time Basis (currently standard)        
        for idx in range(poly_order+1): t_basis.append(t**idx)

        """ Define Coefficients """
        coeff = []
        for kdx in range(num_chords):
            #chord level
            chord_list = []
            for jdx in range(spacial_dimension):
                #dimension level e.g. (x,y,z,...)
                ceoff_poly_list = []
                for idx in range(poly_order+1):
                    #ceoff level e.g. (c0 + c1*t + ... cn*t**n)
                    ceoff_poly_list.append(m.addVar(lb=-GRB.INFINITY))
                    # ceoff_poly_list.append(m.addVar())
                chord_list.append(ceoff_poly_list)
            coeff.append(chord_list)
        m.update() #update before defining objective function

        """ Define Objective Function """
        J = 0.0
        J_test = []
        T_vect = sp.Matrix(t_basis) #convolve single dim of basis
        Td_vect = sp.diff(T_vect,t) #differentiate convolution
        if len(minimization_derivative) != 0:
            #in the event higher order derivatives are desired
            for idx in range(int(minimization_derivative[0])-1):
                Td_vect = sp.diff(Td_vect,t) #differentiate convolution
        Td_conv = Td_vect*Td_vect.T #outer product, producing convolution matrix
        Qd_conv = sp.integrate(Td_conv,t) #integrate wrt t
        Jconv = 0.0
        for kdx in range(num_chords):
            for jdx in range(spacial_dimension):
                Qd_conv_curr = copy.copy(Qd_conv) #deep copy to not overwrite
                Qr  = Qd_conv_curr.subs(t,Ti_0_to_t[kdx+1]) #substitute integrated convolution
                obj = 0.0
                for ii in range(poly_order+1):
                    for jj in range(poly_order+1):
                        obj += Qr[ii,jj] *coeff[kdx][jdx][ii] * coeff[kdx][jdx][jj]   #note the swapping of jj, ii  s.t. this is Qij*ai*aj
                        # J_test.append(Qr[ii,jj] *coeff[kdx][jdx][ii] * coeff[kdx][jdx][jj])
                    J_test.append(coeff[kdx][jdx][ii] * coeff[kdx][jdx][ii])
                J += obj  #need to find equivalent library to integrate a symbolic variable in c++ (possibly symbolicc++)
                #Using convolve function
                polynomial = sum([coeff[kdx][jdx][idx]*sp.diff(t_basis[idx]).subs(t,Ti_0_to_t[kdx+1]) for idx in range(poly_order+1)])
                # print "\npolynomial:", polynomial
                Jconv += scipy.convolve(polynomial,polynomial)[0]

                # print "\n\n curr J:", J
                # print " \n\n Jconv: ", Jconv
                # print "\n\nare they equal", J == Jconv
        
        Jsum = sum(J_test)
        # Jconv = 1.0
        m.setObjective(Jconv, GRB.MINIMIZE)
        # m.setObjective(Jsum, GRB.MINIMIZE)
        # J = 1.#*coeff[0][0][1]*coeff[0][0][1]
        # m.setObjective(J)
        m.update() #update optmizer with objective function



        """ Define Equality Constraints """
        #Start Pose  k=1 (first chord)
        for jdx in range(spacial_dimension):
            # p = 0
            p_t0 = 0.0
            pd_t0 = 0.0
            tvect_subs = sp.Matrix(copy.copy(t_basis)).subs(t,0.0)
            tdvect_subs = sp.diff(sp.Matrix(copy.copy(t_basis) ),t).subs(t,0.0)
            for idx in range(poly_order+1): 
                p_t0 += coeff[0][jdx][idx]* tvect_subs[idx]  #polynomial for the j'th dimension
                pd_t0 += coeff[0][jdx][idx] * tdvect_subs[idx]    
            m.addConstr(p_t0, GRB.EQUAL, start_pose[jdx]) #set start position equal to start_pose
            m.addConstr(pd_t0, GRB.EQUAL, 0.0) #set start velocity to zero
            m.update()
        #Connect Position/Velocity of all intermediate chords

        if len(Ti_0_to_t) > 2:
            for kdx in range(num_chords-1):
            # for kdx in range(int(len(Ti_0_to_t)-2)):
                for jdx in range(spacial_dimension):
                    pk_i = 0.; pdk_i = 0. #current chord
                    pk_j = 0.; pdk_j = 0.#next chord  
                    
                    t_chord_i = Ti_0_to_t[kdx+1] #end of last chord
                    t_chord_j = 0.0 #beginning of next chord              
                    for idx in range(poly_order+1): 
                        pk_i  += coeff[kdx][jdx][idx]  *sp.Matrix(copy.copy(t_basis))[idx].subs(t,t_chord_i);                                             
                        pk_j  += coeff[kdx+1][jdx][idx]*sp.Matrix(copy.copy(t_basis))[idx].subs(t,t_chord_j)   #polynomial for the j'th dimension                    

                        pdk_i += coeff[kdx][jdx][idx]  *sp.diff(sp.Matrix(copy.copy(t_basis))[idx],t).subs(t,t_chord_i);                              
                        pdk_j += coeff[kdx+1][jdx][idx]*sp.diff(sp.Matrix(copy.copy(t_basis))[idx],t).subs(t,t_chord_j) #obtain derviatives      

                    # print "\n pk", pk, "  pdk ", pdk
                    # print "\n pk1", pk1, "  pdk1 ", pdk1

                    # print "current dt: ", t_chord_i, " at index: ", kdx+1

                    m.addConstr( pk_i, GRB.EQUAL,  pk_j) #set pos equal at chord connections
                    # m.addConstr(pdk_i, GRB.EQUAL, pdk_j) #set vel equal at chord connections

                    # m.addConstr(pdk_i, GRB.EQUAL, 0.0) #set vel equal at chord connections
                    # m.addConstr(pdk_j, GRB.EQUAL, 0.0) #set vel equal at chord connections




                    # ADD INEQUALITY CONSTRAINT THAT POSITIONS MUST REMAIN IN OVERLAPPING CONVEX REGIONS
                    if len(inequality_constraints) > 0:
                        for bi_chords in range(2):
                            #do this for consecutive planes
                            for mdx in range(len(inequality_constraints[kdx+bi_chords].planes)):
                                ineq_var_k = 0.  #add constraints to keep connection points between both cvx boundaries
                                ineq_var_k1 = 0.  #add constraints to keep connection points between both cvx boundaries                            
                                for jdx in range(spacial_dimension):
                                    pk = 0.; pdk = 0.; pk1 = 0.; pdk1 = 0.;
                                    for idx in range(poly_order+1): 
                                        pk += coeff[kdx][jdx][idx]*sp.Matrix(copy.copy(t_basis))[idx].subs(t,Ti_0_to_t[kdx+1]);  pdk +=coeff[kdx][jdx][idx]*sp.diff(sp.Matrix(copy.copy(t_basis))[idx],t).subs(t,Ti_0_to_t[kdx+1]);       
                                        pk1  += coeff[kdx+1][jdx][idx]*sp.Matrix(copy.copy(t_basis))[idx].subs(t,0.0);  pdk1 += coeff[kdx+1][jdx][idx]*sp.diff(sp.Matrix(copy.copy(t_basis))[idx],t).subs(t,0.0); 
                                    
                                    try:
                                        ineq_var_k += inequality_constraints[kdx+bi_chords].planes[mdx].plane[jdx]*pk; 
                                        ineq_var_k1 += inequality_constraints[kdx+bi_chords].planes[mdx].plane[jdx]*pk1;
                                    except:
                                        print "\nkdx, mdx, jdx", kdx+bi_chords," " ,mdx," " , jdx #, "\n ineq const: ", inequality_constraints
                                        print "\nienq kdx: ", inequality_constraints[kdx+bi_chords]
                                        print "\nienq mdx: ", inequality_constraints[kdx+bi_chords].planes[mdx]
                                        print "\nienq jdx: ", inequality_constraints[kdx+bi_chords].planes[mdx].plane[jdx]

                                ineq_var_k +=  inequality_constraints[kdx+bi_chords].planes[mdx].plane[spacial_dimension];  
                                ineq_var_k1 +=  inequality_constraints[kdx+bi_chords].planes[mdx].plane[spacial_dimension]                  
                                m.addConstr(ineq_var_k ,GRB.LESS_EQUAL, 0.0); m.addConstr(ineq_var_k1 ,GRB.LESS_EQUAL, 0.0)
                    m.update()

        #Goal Pose k = num_chords
        for jdx in range(spacial_dimension):
            p_tf = 0.
            pd_tf = 0.
            # print "final dt: ", Ti_0_to_t[-1], " at index ", len(Ti_0_to_t)-1

            for idx in range(poly_order+1): 
                p_tf  += coeff[num_chords-1][jdx][idx]*sp.Matrix(copy.copy(t_basis) )[idx].subs(t,Ti_0_to_t[-1])  #polynomial for the j'th dimension
                pd_tf += coeff[num_chords-1][jdx][idx]*sp.diff(sp.Matrix(copy.copy(t_basis))[idx],t).subs(t,Ti_0_to_t[-1]) #polynomial for the j'th dimension
            m.addConstr(p_tf, GRB.EQUAL, goal_pose[jdx]) #set start position equal to start_pose
            m.addConstr(pd_tf, GRB.EQUAL, 0.0) #set start velocity to zero
        m.update() #update equality constraints

        """ Define Inequality Constraints """ #(either standard (most efficient), or Bezier (elegant/complete with guarantees, but over conservative))
        #I DETERMINED THAT THE ISSUE IS NOT RELATED TO THESE INEQUALLITY CONSTRAINTS
        if len(inequality_constraints) > 0:                
            for kdx in range(num_chords):
                #partition each time different [0,dt_i] into chord_sub_division parts
                ti = Ti_0_to_t[kdx+1]  #current time differential (dt)                
                time_sub_divide =np.linspace(0,ti,chord_sub_division) 
                # for time in range(1,int(len(Ti_0_to_t)-1)):
                for time_idx in range(len(time_sub_divide)):
                    #inequality_constraints[].planes[].plane[]      
                    # print "kdx: ", kdx, " constraints: ", inequality_constraints      
                    for mdx in range(len(inequality_constraints[kdx].planes)):
                        ineq_var = 0.
                        # print "\n complete plane: ", inequality_constraints[kdx].planes[mdx].plane
                        p_list= []
                        for jdx in range(spacial_dimension):
                            # p = 0.                        
                            # for idx in range(poly_order+1): p += coeff[kdx][jdx][idx]*sp.Matrix(copy.copy(t_basis))[idx].subs(t,time_sub_divide[time_idx]) #polynomial for the j'th dimension
                            # # p_list.append(p)
                            # ineq_var += inequality_constraints[kdx].planes[mdx].plane[jdx]*p                     
                            # print "dimension, ", jdx, " Aj: ", inequality_constraints[kdx].planes[mdx].plane[jdx]

                            tcurr = np.matrix(self.time_basis(time_sub_divide[time_idx],poly_order)).T# np.matrix(sp.Matrix(t_basis).subs(t,time_sub_divide[time_idx])).T
                            pcurr = np.matrix(coeff[kdx][jdx])* tcurr
                            pcurr = pcurr.tolist()[0][0]
                            p_list.append(pcurr)

                            print "current time vector: ", tcurr, " current coeff vector", np.matrix(coeff[kdx][jdx]), " current poly: ", pcurr

                        p_list.append(1.)
                        p_vect = np.matrix(p_list).T
                        constraint_vect = np.matrix(inequality_constraints[kdx].planes[mdx].plane)
                        print "\np_vect: ", p_vect, " \nconstraint vect: ", constraint_vect
                        ineq_value = constraint_vect*p_vect
                        ineq_value = ineq_value.tolist()[0][0]
                        print  " ineq_value: ", ineq_value
                        # m.addConstr(ineq_var ,GRB.LESS_EQUAL, -inequality_constraints[kdx].planes[mdx].plane[spacial_dimension])
                        # ineq_var +=  inequality_constraints[kdx].planes[mdx].plane[spacial_dimension] #-1 is last element, could also use (spacial_dimension + 1)
                        # print "last element added: ", inequality_constraints[kdx].planes[mdx].plane[spacial_dimension]                    
                        # m.addConstr(ineq_var ,GRB.LESS_EQUAL, 0.0)                        
                        # m.addConstr(ineq_var ,GRB.LESS_EQUAL, -inequality_constraints[kdx].planes[mdx].plane[-1])

                        m.addConstr(ineq_value ,GRB.LESS_EQUAL, 0.0)
        m.update()
        
        # C0 = coeff[0][0][0]
        # C1 = coeff[0][0][1]
        # C2 = coeff[0][0][2]
        # C3 = coeff[0][0][3]

        # C12 = coeff[1][0][0]
        # C13 = coeff[1][0][1]
        # C14 = coeff[1][0][2]
        # C15 = coeff[1][0][3]

        # print "\nThese are the coefficients of interest: ", C0,C1,C2,C3,C12,C13,C14,C15


        # ineq_value1 =  -C0 -1    # ineq_value:  <gurobi.LinExpr: -1.0 + -1.0 C0 + -0.0 C1 + -0.0 C2 + -0.0 C3 + 0.0 C4 + 0.0 C5 + 0.0 C6 + 0.0 C7 + 0.0 C8 + 0.0 C9 + 0.0 C10 + 0.0 C11>
        # ineq_value2 = -1.0 + -1.0 *C0  -0.25 *C1  -0.0625 *C2  -0.015625*C3  #ineq_value:  <gurobi.LinExpr: -1.0 + -1.0 C0 + -0.25 C1 + -0.0625 C2 + -0.015625 C3 + 0.0 C4 + 0.0 C5 + 0.0 C6 + 0.0 C7 + 0.0 C8 + 0.0 C9 + 0.0 C10 + 0.0 C11>
        # ineq_value3 = -1.0 + -1.0 *C0 + -0.5* C1 + -0.25* C2 + -0.125 *C3   #ineq_value:  <gurobi.LinExpr: -1.0 + -1.0 C0 + -0.5 C1 + -0.25 C2 + -0.125 C3 + 0.0 C4 + 0.0 C5 + 0.0 C6 + 0.0 C7 + 0.0 C8 + 0.0 C9 + 0.0 C10 + 0.0 C11>
        # ineq_value4 = -1.0 + -1.0 *C12  #ineq_value:  <gurobi.LinExpr: -1.0 + -1.0 C12 + -0.0 C13 + -0.0 C14 + -0.0 C15 + 0.0 C16 + 0.0 C17 + 0.0 C18 + 0.0 C19 + 0.0 C20 + 0.0 C21 + 0.0 C22 + 0.0 C23>
        # ineq_value5 = -1.0 + -1.0* C12 + -0.25 *C13 + -0.0625 *C14 + -0.015625 *C15  #ineq_value:  <gurobi.LinExpr: -1.0 + -1.0 C12 + -0.25 C13 + -0.0625 C14 + -0.015625 C15 + 0.0 C16 + 0.0 C17 + 0.0 C18 + 0.0 C19 + 0.0 C20 + 0.0 C21 + 0.0 C22 + 0.0 C23>
        # ineq_value6 = -1.0 + -1.0 *C12 + -0.5 *C13 + -0.25 *C14 + -0.125 *C15 #ineq_value:  <gurobi.LinExpr: -1.0 + -1.0 C12 + -0.5 C13 + -0.25 C14 + -0.125 C15 + 0.0 C16 + 0.0 C17 + 0.0 C18 + 0.0 C19 + 0.0 C20 + 0.0 C21 + 0.0 C22 + 0.0 C23>

        
        # m.addConstr(ineq_value1 ,GRB.LESS_EQUAL, 0)
        # m.addConstr(ineq_value2 ,GRB.LESS_EQUAL, 0)
        # m.addConstr(ineq_value3 ,GRB.LESS_EQUAL, 0)
        # m.addConstr(ineq_value4 ,GRB.LESS_EQUAL, 0)
        # m.addConstr(ineq_value5 ,GRB.LESS_EQUAL, 0)
        # m.addConstr(ineq_value6 ,GRB.LESS_EQUAL, 0)

        # # ineq_value = c0 + -c4 -0.5
        # # m.addConstr(ineq_value ,GRB.LESS_EQUAL, 0.0)
        # m.update()

        # p_vect:  [[<gurobi.LinExpr: C0 + 0.0 C1 + 0.0 C2 + 0.0 C3>]
        #  [<gurobi.LinExpr: C4 + 0.0 C5 + 0.0 C6 + 0.0 C7>]
        #  [<gurobi.LinExpr: C8 + 0.0 C9 + 0.0 C10 + 0.0 C11>]
        #  [1.0]]  
        # constraint vect:  [[-1.  0.  0. -1.]]
        #  ineq_value:  <gurobi.LinExpr: -1.0 + -1.0 C0 + -0.0 C1 + -0.0 C2 + -0.0 C3 + 0.0 C4 + 0.0 C5 + 0.0 C6 + 0.0 C7 + 0.0 C8 + 0.0 C9 + 0.0 C10 + 0.0 C11>



        print "\n\nconstraint planes: ", inequality_constraints



        """ Optimize and Return""" #(coefficients and dt vector)
        m.optimize()

        """Prep debug"""
        # try:
        #     #to debug and see which rows are infeasible
        #     m.computeIIS()
        #     m.write('gurobi_problem_crash.ilp')
        # except:
        #     pass

        alpha_vector = []
        # print "\n coefficient vars", m.getVars(), " \n end of list\n"
        for v in m.getVars():
            print('%s %g' % (v.varName, v.x))
            alpha_vector.append(v.x)
        alpha_vector = np.array(alpha_vector)
        alpha_mat = alpha_vector.reshape(num_chords,np.max(alpha_vector.shape)/(num_chords))
        alpha_mat  = alpha_mat.tolist()

        return alpha_mat,Ti_0_to_t
