#!/usr/bin/env python
"""
This script illustrates the equivalence in calculating the convolution 
of two polynomials either with convolution function or using matricies.
"""

import numpy as np, sympy as sp
import scipy; from scipy import linalg

def main():
    #Define order of polynomial to test, solutions will be symbolic
    poly_order = 3 
    #Setup basis and coefficients
    t = sp.Symbol('t'); t_basis = []; coeff = []
    for idx in range(poly_order+1): t_basis.append(t**idx);  coeff.append(sp.Symbol('c'+str(idx)))
    #Define polynomial:
    polynomial = sum([t_basis[idx]*coeff[idx] for idx in range(poly_order+1)]); print "\n Polynomial: ", polynomial
    #Find Convolution:
    convolve_from_poly = sp.expand(scipy.convolve(polynomial,polynomial)[0])
    #Get same convolution from matrix form
    convolve_from_matrix = sp.Matrix(coeff).T * sp.Matrix(t_basis)*sp.Matrix(t_basis).T * sp.Matrix(coeff)
    convolve_from_matrix = sp.expand(convolve_from_matrix.tolist()[0][0])
    #Display Results
    print "\n Convolution fron convolve function: ", convolve_from_poly, "\n vs convolve from matrix: ", convolve_from_matrix
    #Determine if the two forms are equal
    print "\n Are they equal?", convolve_from_poly == convolve_from_matrix

if __name__ == '__main__':
    main()