#!/usr/bin/env python2
import numpy as np, math; import sympy as sp; import rospy
from gurobipy import *; import copy, scipy; from scipy import linalg
from scipy import sparse
#For visualization
from nav_msgs.msg import Path
from geometry_msgs.msg import Pose, Point, Quaternion, PoseStamped
import scipy; from scipy import linalg
import rospy
from visualization_msgs.msg import Marker, MarkerArray

from se2path_planner.qp_solving_support import simple_cvx_gurobi_solver



class Optimizer(object):
    """docstring for Optimizer"""
    def __init__(self, spacial_dimension,num_chords,poly_order,Ti_discrete, chord_subdivide,inequality_constraints,start_pose, goal_pose, *minimization_derivative):
        self.start_pose = start_pose
        self.goal_pose = goal_pose
        self.poly_order = poly_order
        self.num_chords = num_chords
        self.spacial_dimension = spacial_dimension
        self.chord_subdivide = chord_subdivide
        self.inequality_constraints = inequality_constraints
        self.Ti_discrete = Ti_discrete
        self.m = Model("qp")

        #Eventually add in variable derivative objective with minimization_derivative

        #for visualization
        self.path_pub = rospy.Publisher('/se2path_optimized',Path, queue_size=1, latch=True)
        self.plane_pub = rospy.Publisher('/cvx_planes', MarkerArray, queue_size=1, latch=True)
        self.point_step_pub = rospy.Publisher('/opt_path_pts', MarkerArray, queue_size=1, latch=True)
        self.marker_array = MarkerArray()
        self.pt_step_pub_marker_array = MarkerArray()
        self.poly_order = 3
        self.marker_ids = 0
        self.pt_marker_id = 0

    def time_basis(self,t,poly_order):
        t_basis = []
        for idx in range(poly_order+1): t_basis.append(t**idx)
        return t_basis
        
    def create_time_components(self):
        # self.Ti_discrete = [0]
        # for idx in range(self.num_chords): self.Ti_discrete.append(self.Ti_discrete[-1] + 1./self.num_chords)      
        self.Ti_0_to_t = [0]; 
        for idx in range(len(self.Ti_discrete)-1): dt = self.Ti_discrete[idx+1] - self.Ti_discrete[idx]; self.Ti_0_to_t.append(dt)
        self.t = sp.Symbol('t'); self.t_basis = []; 
        for idx in range(self.poly_order+1): self.t_basis.append(self.t**idx)

    def define_coefficients(self):
        #Add coefficients
        """ Define Coefficients """
        self.coeff = []
        for kdx in range(self.num_chords):
            #chord level
            chord_list = []
            for jdx in range(self.spacial_dimension):
                #dimension level e.g. (x,y,z,...)
                ceoff_poly_list = []
                for idx in range(self.poly_order+1):
                    #ceoff level e.g. (c0 + c1*t + ... cn*t**n)
                    ceoff_poly_list.append(self.m.addVar(lb=-GRB.INFINITY))
                chord_list.append(ceoff_poly_list)
            self.coeff.append(chord_list)
        self.m.update() #update before defining objective function

    def define_objective_fnct_mat(self):
        """ Define Objective Function """
        J_test = []
        T_vect = sp.Matrix(self.t_basis); 
        Td_vect = sp.diff(T_vect,self.t) 
        Td_conv = Td_vect*Td_vect.T #outer product, producing convolution matrix
        Qd_conv = sp.integrate(Td_conv,self.t) #integrate wrt t
        for kdx in range(self.num_chords):
            for jdx in range(self.spacial_dimension):
                Qd_conv_curr = copy.copy(Qd_conv) #deep copy to not overwrite
                Qr  = Qd_conv_curr.subs(self.t,self.Ti_0_to_t[kdx+1]) #substitute integrated convolution
                for ii in range(self.poly_order+1):
                    for jj in range(self.poly_order+1):
                        J_test.append(Qr[ii,jj] *self.coeff[kdx][jdx][ii] * self.coeff[kdx][jdx][jj])       
        Jsum = sum(J_test)
        print "this is the objective funct: ", Jsum
        self.m.setObjective(Jsum, GRB.MINIMIZE)

    def define_objective_fnct_conv(self):
        """ Define Objective Function """
        Jconv = 0.0
        for kdx in range(self.num_chords):
            for jdx in range(self.spacial_dimension):
                polynomial = sum([self.coeff[kdx][jdx][idx]*sp.diff(self.t_basis[idx]).subs(self.t,self.Ti_0_to_t[kdx+1]) for idx in range(self.poly_order+1)])
                Jconv += scipy.convolve(polynomial,polynomial)[0]   
        self.m.setObjective(Jconv, GRB.MINIMIZE)

    def define_equality_constriants(self):
        """ Define Equality Constraints """
        #Start Pose  k=1 (first chord)
        for jdx in range(self.spacial_dimension):
            p_t0 = 0.0; pd_t0 = 0.0
            tvect_subs = sp.Matrix(self.t_basis).subs(self.t,0.0)
            tdvect_subs = sp.diff(sp.Matrix(self.t_basis),self.t).subs(self.t,0.0)
            for idx in range(self.poly_order+1): 
                p_t0 += self.coeff[0][jdx][idx]* tvect_subs[idx]  #polynomial for the j'th dimension
                pd_t0 += self.coeff[0][jdx][idx] * tdvect_subs[idx]    
            self.m.addConstr(p_t0, GRB.EQUAL, self.start_pose[jdx]) #set start position equal to start_pose
            self.m.addConstr(pd_t0, GRB.EQUAL, 0.0) #set start velocity to zero
            self.m.update()
        #Connect Position/Velocity of all intermediate chords
        if len(self.Ti_0_to_t) > 2:
            for kdx in range(self.num_chords-1):
            # for kdx in range(int(len(Ti_0_to_t)-2)):
                for jdx in range(self.spacial_dimension):
                    pk_i = 0.; pdk_i = 0. #current chord
                    pk_j = 0.; pdk_j = 0.#next chord                      
                    t_chord_i = self.Ti_0_to_t[kdx+1] #end of last chord
                    t_chord_j = 0.0 #beginning of next chord              
                    for idx in range(self.poly_order+1): 
                        pk_i  += self.coeff[kdx][jdx][idx]  *sp.Matrix(copy.copy(self.t_basis))[idx].subs(self.t,t_chord_i);                                             
                        pk_j  += self.coeff[kdx+1][jdx][idx]*sp.Matrix(copy.copy(self.t_basis))[idx].subs(self.t,t_chord_j)   #polynomial for the j'th dimension                    
                        pdk_i += self.coeff[kdx][jdx][idx]  *sp.diff(sp.Matrix(copy.copy(self.t_basis))[idx],self.t).subs(self.t,t_chord_i);                              
                        pdk_j += self.coeff[kdx+1][jdx][idx]*sp.diff(sp.Matrix(copy.copy(self.t_basis))[idx],self.t).subs(self.t,t_chord_j) #obtain derviatives      
                    self.m.addConstr( pk_i, GRB.EQUAL,  pk_j) #set pos equal at chord connections
                    self.m.addConstr(pdk_i, GRB.EQUAL, pdk_j) #set vel equal at chord connections
                    self.m.update()
                #Now ensure that both points lie within overlapping convex regions
                if len(self.inequality_constraints) > 0:
                    for kdx_add in range(2):                        
                        for mdx in range(len(self.inequality_constraints[kdx+kdx_add].planes)):
                            t_basis_i = self.time_basis(t_chord_i,self.poly_order)
                            t_basis_j = self.time_basis(t_chord_j,self.poly_order)
                            
                            poly_vect_i =[sum([self.coeff[kdx][jdx][idx]*t_basis_i[idx] for idx in range(self.poly_order+1)]) for jdx in range(self.spacial_dimension)]
                            poly_vect_i.append(1)
                            
                            poly_vect_j =[sum([self.coeff[kdx+1][jdx][idx]*t_basis_j[idx] for idx in range(self.poly_order+1)]) for jdx in range(self.spacial_dimension)]
                            poly_vect_j.append(1)
                            
                            plane_curr = self.inequality_constraints[kdx+kdx_add].planes[mdx].plane
                            plane = np.matrix(plane_curr)
                            #define inequality variable
                            ineq_var_i = plane*np.matrix(poly_vect_i).T
                            ineq_var_i = ineq_var_i.tolist()[0][0]

                            ineq_var_j = plane*np.matrix(poly_vect_j).T
                            ineq_var_j = ineq_var_j.tolist()[0][0]

                            # print "ineq var1 to add: ", ineq_var
                            self.m.addConstr(ineq_var_i, GRB.LESS_EQUAL, 0)
                            self.m.addConstr(ineq_var_j, GRB.LESS_EQUAL, 0)
                            self.m.update()


        #Goal Pose k = num_chords
        for jdx in range(self.spacial_dimension):
            p_tf = 0.
            pd_tf = 0.
            for idx in range(self.poly_order+1): 
                p_tf  += self.coeff[self.num_chords-1][jdx][idx]*sp.Matrix(self.t_basis)[idx].subs(self.t,self.Ti_0_to_t[-1])  #polynomial for the j'th dimension
                pd_tf += self.coeff[self.num_chords-1][jdx][idx]*sp.diff(sp.Matrix(self.t_basis)[idx],self.t).subs(self.t,self.Ti_0_to_t[-1]) #polynomial for the j'th dimension
            self.m.addConstr(p_tf, GRB.EQUAL, self.goal_pose[jdx]) #set start position equal to start_pose
            self.m.addConstr(pd_tf, GRB.EQUAL, 0.0) #set start velocity to zero
        self.m.update() #update equality constraints


    def add_objective_manually(self):
        #testing for s=[0], g=[1], 2 chords, poly_order=2, failed
        c0 = self.coeff[0][0][0]
        c1 = self.coeff[0][0][1]
        c2 = self.coeff[0][0][2]
        c3 = self.coeff[1][0][0]
        c4 = self.coeff[1][0][1]
        c5 = self.coeff[1][0][2]

        J = 0.5*(c1*c1 + c4*c4 + c1*c2 + c4*c5) + 1./6.*(c2*c2+c5*c5)
        print "new Objective: ", J
        print "these are the coefficients: ", self.coeff
        self.m.setObjective(J)

    def add_constriants_manually(self):
        c0 = self.coeff[0][0][0]
        c1 = self.coeff[0][0][1]
        c2 = self.coeff[0][0][2]
        c3 = self.coeff[1][0][0]
        c4 = self.coeff[1][0][1]
        c5 = self.coeff[1][0][2]
        self.m.addConstr(c0, GRB.EQUAL, 0.0)
        self.m.addConstr(c0+0.5*c1+0.25*c2 - c3, GRB.EQUAL, 0.0)
        self.m.addConstr(c1+c2-c4 , GRB.EQUAL, 0.0)
        self.m.addConstr(c3+0.5*c4 + 0.25*c5 - 1, GRB.EQUAL, 0.0)
        self.m.update()


    def optimize_and_return(self):
        self.m.optimize()

        alpha_vector = []
        print "\n coefficient vars", self.m.getVars(), " \n end of list\n"
        for v in self.m.getVars():
            print('%s %g' % (v.varName, v.x))
            alpha_vector.append(v.x)
        alpha_vector = np.array(alpha_vector)
        alpha_mat = alpha_vector.reshape(self.num_chords,np.max(alpha_vector.shape)/(self.num_chords))
        self.alpha_mat  = alpha_mat.tolist()
        return self.alpha_mat, self.Ti_0_to_t

    def disp_main_points(self):
        #display the critical points of the polynomials
        pose_list = []
        pose_list.append(self.start_pose)
        for idx in range(len(self.Ti_0_to_t)-1):
            tcurr = self.Ti_0_to_t[idx+1]
            dt_vect = [tcurr**mdx for mdx in range(self.poly_order+1)]; dt_list = [dt_vect for mdx in range(self.spacial_dimension)]
            Tmat = scipy.sparse.block_diag(np.array(dt_list))
            coeff_vect = np.matrix(self.alpha_mat[idx]).T      
            pose_curr= Tmat * coeff_vect; pose_list.append(pose_curr.T.tolist()[0])
        pmat = np.matrix(pose_list)
        print "\n\n Pose list: ",pmat

    def inequality_constraint(self):
        plane1 = [1]
        plane2 = [-1]
        for idx in range(self.spacial_dimension-1): plane1.append(0); plane2.append(0)
        plane1.append(-10) # plane = np.matrix([1,0,0,-10])
        plane2.append(-10) # plane = np.matrix([1,0,0,-10])
        plane1 = np.matrix(plane1); plane2 = np.matrix(plane2)
        for kdx in range(self.num_chords):
            t_sub = np.linspace(0,self.Ti_0_to_t[kdx+1],self.chord_subdivide)
            if kdx == 0: t_sub = t_sub[1:]
            if kdx == self.num_chords-1: t_sub = t_sub[:-2]
            for time_idx in t_sub:
                t_basis = self.time_basis(time_idx,self.poly_order)
                poly_vect =[sum([self.coeff[kdx][jdx][idx]*t_basis[idx] for idx in range(self.poly_order+1)]) for jdx in range(self.spacial_dimension)]
                poly_vect.append(1)
                print "tbasis: ", np.matrix(t_basis), "\n pvect", np.matrix(poly_vect).T
                ineq_var1 = plane1*np.matrix(poly_vect).T
                ineq_var1 = ineq_var1.tolist()[0][0]
                print "ineq var1 to add: ", ineq_var1
                self.m.addConstr(ineq_var1, GRB.LESS_EQUAL, 0)

                ineq_var2 = plane2*np.matrix(poly_vect).T
                ineq_var2 = ineq_var2.tolist()[0][0]
                print "ineq var2 to add: ", ineq_var2
                self.m.addConstr(ineq_var2, GRB.LESS_EQUAL, 0)
                self.m.update()



    def inequality_constraint_with_struct(self):    
        # self.inequality_constraints = self.build_constraint_struct()
        # print "ineualit const: ", self.inequality_constraints
        if len(self.inequality_constraints) > 0:
            for kdx in range(self.num_chords):
                t_sub = np.linspace(0,self.Ti_0_to_t[kdx+1],self.chord_subdivide)
                if kdx == 0: t_sub = t_sub[1:]
                if kdx == self.num_chords-1: t_sub = t_sub[:-2]
                for time_idx in t_sub:
                    for mdx in range(len(self.inequality_constraints[kdx].planes)):
                        t_basis = self.time_basis(time_idx,self.poly_order)
                        poly_vect =[sum([self.coeff[kdx][jdx][idx]*t_basis[idx] for idx in range(self.poly_order+1)]) for jdx in range(self.spacial_dimension)]
                        poly_vect.append(1)
                        print "tbasis: ", np.matrix(t_basis), "\n pvect", np.matrix(poly_vect).T                  
                        plane_curr = self.inequality_constraints[kdx].planes[mdx].plane
                        plane = np.matrix(plane_curr)
                        #define inequality variable
                        ineq_var = plane*np.matrix(poly_vect).T
                        ineq_var = ineq_var.tolist()[0][0]
                        print "ineq var1 to add: ", ineq_var
                        self.m.addConstr(ineq_var, GRB.LESS_EQUAL, 0)
                        self.m.update()

    
    def build_constraint_struct(self):        
        class corridor_cls(object):
            """docstring for corridor_cls"""
            def __init__(self):
                self.planes = []
                        
        class planes(object):
            def __init__(self):
                self.plane = []

        plane1 = [1]
        plane2 = [-1]
        for idx in range(self.spacial_dimension-1): plane1.append(0); plane2.append(0)
        plane1.append(-10) # plane = np.matrix([1,0,0,-10])
        plane2.append(-10) # plane = np.matrix([1,0,0,-10])
        
        cobj = corridor_cls()
        pobj1 = planes(); pobj1.plane = plane1
        pobj2 = planes(); pobj2.plane = plane2
        cobj.planes.append(pobj1)
        cobj.planes.append(pobj2)

        inequality_constraints = []
        for idx in range(self.num_chords):
            #for each chord
            inequality_constraints.append(cobj)

        # inequality_constraints[kdx].planes[mdx].plane[spacial_dimension]
        return inequality_constraints



    def gen_basis_vect(self,t, poly_order):
        t_basis = []
        for idx in range(poly_order+1): t_basis.append(t**idx)
        return np.matrix(t_basis)

    def gen_basis_vect_derivative(self,t, poly_order):
        t_basis = []
        tt = sp.Symbol('tt')
        for idx in range(poly_order+1): t_basis.append(tt**idx)
        tb = sp.diff(sp.Matrix(t_basis)).subs(tt,t).T.tolist()[0]
        # print "tb: ", tb

        return np.matrix(tb)

    def display_optimal_path(self):
        # self.poly_order = poly_order
        print "display the optimal path in rviz"
        path_list = Path(); path_list.header.frame_id = '/map'
        pose_list = []; pose_list.append(self.start_pose)
        for iidx in range(len(self.Ti_0_to_t)):
            if iidx == 0: 
                idx = 0
                tcurr = self.Ti_0_to_t[idx]
            elif iidx > 0:
                idx = iidx - 1 
                tcurr = self.Ti_0_to_t[idx+1]


            dt_vect = [tcurr**mdx for mdx in range(self.poly_order+1)]; dt_list = [dt_vect for mdx in range(self.spacial_dimension)]
            Tmat = scipy.sparse.block_diag(np.array(dt_list))
            coeff_vect = np.matrix(self.alpha_mat[idx]).T      
            pose_curr= Tmat * coeff_vect; pose_list.append(pose_curr.T.tolist()[0])
        
            pose = [0,0,0]
            print "pre pose: ", pose
            for ii in range(self.spacial_dimension): pose[ii] = pose_curr.item(ii)
            print "pose pose: ", pose
            flag = 1
            marker_curr= self.generate_pt_marker(pose,flag)
            self.pt_step_pub_marker_array.markers.append(marker_curr)
            pose_stamped = PoseStamped()
            pose_stamped.header.frame_id = '/map'
            print "current pos", pose_curr, " current reported pose: ", pose
            pose_stamped.pose = Pose(Point(pose[0],pose[1],pose[2]),Quaternion(0,0,0,1))
            path_list.poses.append(pose_stamped)




        pmat = np.matrix(pose_list)
        print "\n\n Pose list in rviz: ",pmat
        self.path_pub.publish(path_list)
        self.point_step_pub.publish(self.pt_step_pub_marker_array)
        print "published path"

    def generate_pt_marker(self, point, flag):
        pt_marker = Marker()
        pt_marker.header.frame_id = "map"
        pt_marker.header.stamp = rospy.Time()
        pt_marker.id = self.pt_marker_id; self.pt_marker_id += 1
        pt_marker.type = 2
        pt_marker.pose.position.x = point[0]
        pt_marker.pose.position.y = point[1]
        pt_marker.pose.position.z = point[2]
        pt_marker.pose.orientation.x = 0.
        pt_marker.pose.orientation.y = 0.
        pt_marker.pose.orientation.z = 0.
        pt_marker.pose.orientation.w = 1.
        pt_marker.scale.x = 0.05
        pt_marker.scale.y = 0.05
        pt_marker.scale.z = 0.05
        pt_marker.color.a = 1.0
        if flag == 0:
            pt_marker.color.r = 1.0
            pt_marker.color.g = 1.0
            pt_marker.color.b = .0
        else:
            pt_marker.color.r = 1.0
            pt_marker.color.g = .0
            pt_marker.color.b = .0
        return pt_marker


    def run_optimization(self, *visualize_bool):    
        self.create_time_components()
        self.define_coefficients()
        self.define_objective_fnct_mat()  #convolution via matrix
        # opt_cls.define_objective_fnct_conv() #convolutino via fnct call
        self.define_equality_constriants()
        self.inequality_constraint_with_struct()
        alpha_mat, Ti_0_to_t = self.optimize_and_return()
        opt_path = {"coeff":alpha_mat, "T":Ti_0_to_t}
        if len(visualize_bool) > 0:
            if visualize_bool[0] == True:
                self.display_optimal_path()
                self.disp_main_points()
        return opt_path






class build_constraint_obj(object):
    
    def __init__(self, num_chords, spacial_dimension, poly_order, start_pose, goal_pose,  chord_subdivide):
        self.start_pose = start_pose
        self.goal_pose = goal_pose
        self.poly_order = poly_order
        self.num_chords = num_chords
        self.spacial_dimension = spacial_dimension
        self.chord_subdivide = chord_subdivide
        
    def build_constraint_struct(self):        
        class corridor_cls(object):
            """docstring for corridor_cls"""
            def __init__(self):
                self.planes = []
                        
        class planes(object):
            def __init__(self):
                self.plane = []

        plane1 = [1]
        plane2 = [-1]
        for idx in range(self.spacial_dimension-1): plane1.append(0); plane2.append(0)
        plane1.append(-10) # plane = np.matrix([1,0,0,-10])
        plane2.append(-10) # plane = np.matrix([1,0,0,-10])
        
        plane3 = [0,1,0,-10]

        cobj = corridor_cls()
        pobj1 = planes(); pobj1.plane = plane1
        pobj2 = planes(); pobj2.plane = plane2
        pobj3 = planes(); pobj3.plane = plane3
        cobj.planes.append(pobj1)
        cobj.planes.append(pobj2)
        cobj.planes.append(pobj3)

        inequality_constraints = []
        for idx in range(self.num_chords):
            #for each chord
            inequality_constraints.append(cobj)

        # inequality_constraints[kdx].planes[mdx].plane[spacial_dimension]
        return inequality_constraints



def main():

    rospy.init_node('test_for_plot')
    #Set Parameters
    num_chords = 3
    poly_order = 3
    spacial_dimension = 3
    chord_sub_division = 3
    Ti_discrete = [0,0.3,0.6,1]

    if spacial_dimension == 1: start_pose = [0]; goal_pose = [1.]
    elif spacial_dimension == 2: start_pose = [0,0]; goal_pose = [0,1.]
    elif spacial_dimension == 3: start_pose = [0,0,0]; goal_pose = [0,0,1.]

    build_obj = build_constraint_obj(num_chords, spacial_dimension, poly_order, start_pose, goal_pose, chord_sub_division)
    inequality_constraints = build_obj.build_constraint_struct()




    #Current Scripts answer: 
    # opt_cls = Optimizer(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose)
    # vis_bool = True
    # opt_cls.run_optimization(vis_bool)




    #Answer from simple code
    solver_obj = simple_cvx_gurobi_solver.Optimizer(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose)    
    vis_bool = True
    solver_obj.run_optimization(vis_bool)

    rospy.spin()





if __name__ == '__main__':
    main()