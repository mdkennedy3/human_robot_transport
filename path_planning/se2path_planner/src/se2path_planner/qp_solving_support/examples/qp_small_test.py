import numpy as np, math; import sympy as sp
from gurobipy import *; import copy, scipy; from scipy import linalg

class Cases(object):
    """docstring for Cases"""
    def make_basis(self,t,poly_order):
        t_b = []
        for idx in range(poly_order+1): t_b.append(t**idx)
        return t_b
    def spelled_out(self):        
        m = Model("qp")
        # coeff = []
        # for idx in range(6): coeff.append(m.addVar(lb=-GRB.INFINITY))
        c0 = m.addVar(name="c0", lb=-GRB.INFINITY)
        c1 = m.addVar(name="c1", lb=-GRB.INFINITY)
        c2 = m.addVar(name="c2", lb=-GRB.INFINITY)
        c3 = m.addVar(name="c3", lb=-GRB.INFINITY)
        c4 = m.addVar(name="c4", lb=-GRB.INFINITY)
        c5 = m.addVar(name="c5", lb=-GRB.INFINITY)
        m.update()
        J = 0.5*(c1*c1 + c4*c4 + c1*c2 + c4*c5) + 1./6.*(c2*c2+c5*c5)
        m.setObjective(J)
        m.update()
        m.addConstr(c0, GRB.EQUAL, 0.0)
        m.addConstr(c0+0.5*c1+0.25*c2 - c3, GRB.EQUAL, 0.0)
        m.addConstr(c1+c2-c4 , GRB.EQUAL, 0.0)
        m.addConstr(c3+0.5*c4 + 0.25*c5 - 1, GRB.EQUAL, 0.0)
        m.update()

        #add inequality constraints: 
        A = np.matrix([-1,1]).T; b=np.matrix([-10,-20]).T
        # ti_vect = np.matrix(self.make_basis(0,2))
        # tf_vect = np.matrix(self.make_basis(0.5,2))
        ti_vect = np.matrix(self.make_basis(0.2,2))
        tf_vect = np.matrix(self.make_basis(0.4,2))
        tm_vect = np.matrix(self.make_basis(0.3,2))

        coeff = np.matrix([c0,c1,c2,c3,c4,c5]).T
        

        Abig = np.matrix( scipy.linalg.block_diag(A,A,A,A))
        Bbig = np.matrix(np.vstack([b,b,b,b]))

        # Abig = np.matrix( scipy.linalg.block_diag(A,A))
        # Bbig = np.matrix(np.vstack([b,b]))
        tstack = np.vstack([ti_vect,tf_vect])
        # tstack = tm_vect
        Tmat = np.matrix(scipy.linalg.block_diag(tstack,tstack))

        Afinal = Abig*Tmat
        Bfinal = Bbig

        aa = Afinal*coeff
        bb = Bfinal


        self.aa = aa.tolist()
        self.bb= bb.tolist()
        m.update()
        # print "aa: ", self.aa, " bb: ", self.bb

        for idx in range(len(self.aa)):
            m.addConstr(self.aa[idx][0], GRB.LESS_EQUAL, -self.bb[idx][0])
            m.update()

        m.optimize()
        # print "\n coefficient vars", m.getVars(), " \n end of list\n"
        for v in m.getVars():
            print('%s %g' % (v.varName, v.x))

        # print "pass to matlab: A", Afinal, "\n and b: ", Bfinal

    def list_coeffs(self):
        m = Model("qp")
        coeff = []
        for idx in range(6): coeff.append(m.addVar(lb=-GRB.INFINITY))
        m.update()
        J = 0.5*(coeff[1]*coeff[1] + coeff[4]*coeff[4] + coeff[1]*coeff[2] + coeff[4]*coeff[5]) + 1./6.*(coeff[2]*coeff[2]+coeff[5]*coeff[5])
        m.setObjective(J)
        m.update()
        m.addConstr(coeff[0], GRB.EQUAL, 0.0)
        m.addConstr(coeff[0]+0.5*coeff[1]+0.25*coeff[2] - coeff[3], GRB.EQUAL, 0.0)
        m.addConstr(coeff[1]+coeff[2]-coeff[4] , GRB.EQUAL, 0.0)
        m.addConstr(coeff[3]+0.5*coeff[4] + 0.25*coeff[5] - 1, GRB.EQUAL, 0.0)
        m.update()

        m.optimize()
        # print "\n coefficient vars", m.getVars(), " \n end of list\n"
        for v in m.getVars():
            print('%s %g' % (v.varName, v.x))






def main():
    case_obj = Cases()
    case_obj.spelled_out()
    # print "\n\n now list coeffs: \n\n"
    # case_obj.list_coeffs()




if __name__ == '__main__':
    main()



"""
%matlab code 1D:
tconv = [0,0,0; 0, 0.5,0.25;0, 0.25,1/6]
H = blkdiag(tconv,tconv)
Aeq = [1,0,0,0,0,0; 1,.5,.25,-1,0,0;0,1,1,0,-1,0;0,0,0,1,.5,.25]
beq = [0;0;0;1]

f = [0;0;0;0;0;0]
x = quadprog(H,f,[],[],Aeq,beq)

#matlab still didnt like this or the boundary points
AA = [[-1.   -0.2  -0.04  0.    0.    0.  ]
 [ 1.    0.2   0.04  0.    0.    0.  ]
 [-1.   -0.4  -0.16  0.    0.    0.  ]
 [ 1.    0.4   0.16  0.    0.    0.  ]
 [ 0.    0.    0.   -1.   -0.2  -0.04]
 [ 0.    0.    0.    1.    0.2   0.04]
 [ 0.    0.    0.   -1.   -0.4  -0.16]
 [ 0.    0.    0.    1.    0.4   0.16]] 

b = [-1;-2;-1;-2;-1;-2;-1;-2;]
x = quadprog(H,f,A,b,Aeq,beq)
"""



