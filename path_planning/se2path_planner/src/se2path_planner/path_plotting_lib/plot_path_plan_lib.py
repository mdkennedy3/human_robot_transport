#!/usr/bin/env python
import rospy
import numpy as np
import numpy.matlib
import math
#For plotting
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from se2path_finding.ts_math_operations import ts_math_oper
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point32 as geomPoint
from geometry_msgs.msg import Point
from sensor_msgs.msg import PointCloud


class Plot_general_markers_rviz(object):
    def __init__(self,pub_topic):
        self.pub_topic = pub_topic
        self.marker_array_obs_cube_pts = MarkerArray()

    def plot_marker_rviz(self,point,scale,color,m_type,frame_id,*additional_arg):
        # print "make points"
        marker = Marker()
        marker.header.frame_id = frame_id
        marker.type= m_type  # cube: 1; sphere 2; cylinder 3; etc
        marker.action = marker.ADD
        if len(scale) == 1:
            marker.scale.x = scale[0] #may be different 
        else:
            marker.scale.x = scale[0] #may be different 
            marker.scale.y = scale[1]
            marker.scale.z = scale[2]

        marker.color.a = 1.0  #transparency
        # print "color list: ", color
        marker.color.r = color[0]
        marker.color.g = color[1]
        marker.color.b = color[2]
        
        # print "the point: ", point
        marker.pose.position.x = point[0]
        marker.pose.position.y = point[1]
        marker.pose.position.z = point[2]
        if len(additional_arg) == 1:
            #specify the orientation
            marker.pose.orientation.w = additional_arg[0][0]
            marker.pose.orientation.x = additional_arg[0][1]
            marker.pose.orientation.y = additional_arg[0][2]
            marker.pose.orientation.z = additional_arg[0][3]
        if len(additional_arg) == 2:
            marker.pose.orientation.w = additional_arg[0][0]
            marker.pose.orientation.x = additional_arg[0][1]
            marker.pose.orientation.y = additional_arg[0][2]
            marker.pose.orientation.z = additional_arg[0][3]

            point_list = []
            for idx in range(len(additional_arg[1])):
                point_list.append(Point())

            for idx in range(len(additional_arg[1])):
                point_list[idx].x = additional_arg[1][idx][0]
                point_list[idx].y = additional_arg[1][idx][1]
                point_list[idx].z = additional_arg[1][idx][2]
            marker.points = point_list
            

        return marker


    def plot_desired_pos_r2(self,pose,*frame_arg):
        x = pose.item(0)
        y = pose.item(1)
        z = 0.
        point = [x,y,z]
        scale = [0.3,0.3,0.3]
        color = [0,.5,0]
        if len(frame_arg) == 0:
            frame_id = "/odom"
        else:
            frame_id = frame_arg[0] 
        m_type = 2
        marker = self.plot_marker_rviz(point,scale,color,m_type,frame_id)
        marker.id = 1
        self.pub_topic.publish(marker)


    def plot_desired_pos_r3(self,pose,*frame_arg):
        x = pose.item(0)
        y = pose.item(1)
        z = pose.item(2)
        point = [x,y,z]
        scale = [0.3,0.3,0.3]
        color = [0,.5,0]
        if len(frame_arg) == 0:
            frame_id = "/vis_pts"
        else:
            frame_id = frame_arg[0] 
        m_type = 2
        marker = self.plot_marker_rviz(point,scale,color,m_type,frame_id)
        marker.id = 1
        self.pub_topic.publish(marker)







    def plot_object_cubes(self,object_list,*frame_arg):
        #expects object list: [[x11,y11,x12,y12],[x21,y21,x22,y22],...[xi1,yi1,xi2,yi2]]
        # frame_id: "\world_frame"
        scale = 1.0
        
        # color = [0.3,0.3,0.3]
        # print "object list: ", object_list
        color = [0.,0.5,0.]
        marker_type = 1
        if len(frame_arg) == 0:
            frame_id = "/odom"
        else:
            frame_id = frame_arg[0]
        for idx in range(len(object_list)):
            obs_coords = object_list[idx]
            x1 = obs_coords[0];y1 = obs_coords[1];x2 = obs_coords[2];y2 = obs_coords[3]
            xcenter = (x1 + x2)/2.; ycenter = (y1 + y2)/2.; 
            x_scale = np.max([x1,x2]) - np.min([x1,x2])
            y_scale = np.max([y1,y2]) - np.min([y1,y2])
            print "\nobstacle scales idx:", idx," x:",x_scale, " y:", y_scale, "obs coord:", obs_coords
            z_scale = 0.3
            zcenter =  z_scale/2.

            scale_passed = [x_scale,y_scale,z_scale]
            pt_curr = [xcenter,ycenter,zcenter]
            cube_marker = self.plot_marker_rviz(pt_curr,scale_passed,color,marker_type,frame_id)

            # marker = self.plot_marker_rviz(pt_curr,scale_passed,color,marker_type,self.topic_name)
            cube_marker.id = idx+1
            self.marker_array_obs_cube_pts.markers.append(cube_marker)
        self.pub_topic.publish(self.marker_array_obs_cube_pts)


    def plot_final_path(self,nav_msg_traj_path,scale_to_divide,*frame_arg):
        scale = .06
        if len(frame_arg) == 0:
            frame_id = "/odom"
        else:
            frame_id = frame_arg[0]

        color = [1,0,0]

        path_marker = Marker()
        path_marker.action = 0#path_marker.ADD
        path_marker.header.frame_id = frame_id
        # path_marker.type = 4
        path_marker.type = path_marker.LINE_STRIP
        path_marker.color.a = 1.0  #transparency
        path_marker.color.r = color[0]
        path_marker.color.g = color[1]
        path_marker.color.b = color[2]
        path_marker.id = 1
        path_marker.scale.x = scale #may be different 
        path_marker.scale.y = scale
        path_marker.scale.z = scale

        path_marker.pose.position.x = 0.
        path_marker.pose.position.y = 0.
        path_marker.pose.position.z = 0.
        path_marker.pose.orientation.w = 1.
        path_marker.pose.orientation.x = 0.
        path_marker.pose.orientation.y = 0.
        path_marker.pose.orientation.z = 0.

        # path_marker.lifetime = 0
        # path_pt = Point()
        # path_marker.points = []
        point_list = []
        for idx in range(len(nav_msg_traj_path)):
            point_list.append(Point())

        for idx in range(len(nav_msg_traj_path)):
            point_list[idx].x = nav_msg_traj_path[idx].pose.position.x/scale_to_divide
            point_list[idx].y = nav_msg_traj_path[idx].pose.position.y/scale_to_divide
            point_list[idx].z = nav_msg_traj_path[idx].pose.position.z
        path_marker.points = point_list
            # print "current position: ", nav_msg_traj_path[idx].pose.position.x/scale_to_divide,nav_msg_traj_path[idx].pose.position.y/scale_to_divide
        #     print "appending point: ", path_pt
        # print "final list of points: ", path_marker.points
        self.pub_topic.publish(path_marker)
        print "just published markers for obstacle cubes:", len(point_list)


    def plot_robot_cvx_boundary_at_path_pts(self,nav_msg_traj_path,scale_to_divide,robot_cvx_hull_pts,*frame_arg):
        print "plotting chull in r2"
        if len(frame_arg) == 0:
            frame_id = "/odom"
        else:
            frame_id = frame_arg[0]
        color = [0.,0.,1.]
        scale = [0.01]
        m_type = 4
        path_steps = 5.
        x1 = robot_cvx_hull_pts["x1"]; x2 = robot_cvx_hull_pts["x2"]; y1 = robot_cvx_hull_pts["y1"]; y2 = robot_cvx_hull_pts["y2"];

        hull_pts = [[x1,y1,0.],[x2,y1,0.],[x2,y2,0.],[x1,y2,0.],[x1,y1,0.]]

        markar_array_list = []
        for idx in range(len(nav_msg_traj_path)):
            markar_array_list.append(Marker())


        for jdx in range( int(len(nav_msg_traj_path)/path_steps + 1.)):
            #get transform of points in R2: 
            if jdx >=  int(len(nav_msg_traj_path)/path_steps ):
                idx = len(nav_msg_traj_path)-1
            else:
                idx = int(jdx*path_steps)
            # print "idx vs len: ", idx, " len: ", len(nav_msg_traj_path),"total range: ",int(len(nav_msg_traj_path)/path_steps + 1.)," path steps: ", path_steps," jdx: ", jdx
            x_curr = nav_msg_traj_path[idx].pose.position.x/scale_to_divide
            y_curr = nav_msg_traj_path[idx].pose.position.y/scale_to_divide
            z_curr = 0.
            point = [x_curr, y_curr, z_curr]
            
            orientation = [nav_msg_traj_path[idx].pose.orientation.w,nav_msg_traj_path[idx].pose.orientation.x,nav_msg_traj_path[idx].pose.orientation.y,nav_msg_traj_path[idx].pose.orientation.z]

            # theta_curr = np.arctan2(nav_msg_traj_path[idx].pose.orientation.z , nav_msg_traj_path[idx].pose.orientation.w)*2. 
            # if theta_curr > math.pi: 
            #     theta_curr = theta_curr - 2*math.pi
            # elif theta_curr < -math.pi: 
            #     theta_curr = theta_curr + 2*math.pi
            # R_th_curr = np.matrix([[np.cos(theta_curr), -np.sin(theta_curr), 0],[np.sin(theta_curr), np.cos(theta_curr),0],[0,0,1]])

            # Transform = np.hstack([R_th_curr,pose_vector])
            # hull_pts_rotated = Transform * np.vstack([hull_pts,np.matrix(np.ones([1,5]))])

            marker = self.plot_marker_rviz(point,scale,color,m_type,frame_id,orientation,hull_pts)
            marker.id = idx
            markar_array_list[idx] = marker
        # self.marker_array_obs_cube_pts.markers.append(marker)
        self.marker_array_obs_cube_pts.markers = markar_array_list
        self.pub_topic.publish(self.marker_array_obs_cube_pts)



class Plotting_debugging_rviz(object):
    def __init__(self,path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub):
        self.path_points_pub = path_points_pub
        self.cvx_path_points_pub  = cvx_path_points_pub
        self.obstacle_points_pub = obstacle_points_pub
        self.vector_pub =vector_pub
        self.planes_pub = planes_pub
        self.valid_occ_topic = valid_occ_topic

        self.topic_name = "/vis_pts" #e.g. "/neck"
        self.marker_array_world_pts = PointCloud()
        self.marker_array_path_pts = MarkerArray()

        self.scatter_idx = 0
        self.planes_initialized = False

    def Plot_obstacles(self,cloud):
        self.marker_array_world_pts.header.frame_id = self.topic_name
        for idx in xrange(len(cloud)):
            pt = cloud[idx]
            point = geomPoint()
            point.x = pt[0]
            point.y = pt[1]
            point.z = pt[2]
            self.marker_array_world_pts.points.append(point)
        self.obstacle_points_pub.publish(self.marker_array_world_pts)

    def Plot_scatter_list(self,pts,scale,color):
        for idx in range(len(pts)):
            pt_curr = pts[idx]#; scale = 1.0; #color = [0,0,1] 
            scale_passed = [scale,scale,scale]
            marker_type = "sphere"
            marker = self.plot_marker_rviz(pt_curr,scale_passed,color,marker_type,self.topic_name)
            marker.id = self.scatter_idx; self.scatter_idx = self.scatter_idx + 1;# print "scatter idx" ,self.scatter_idx
            self.marker_array_path_pts.markers.append(marker)
        self.path_points_pub.publish(self.marker_array_path_pts)
            #check type of pt, convert to list if necessary

    def Plot_cvx_path(self,pts,scale,color):
        for idx in range(len(pts)):
            pt_curr = pts[idx]#; scale = 1.0; #color = [0,0,1] 
            scale_passed = [scale,scale,scale]
            marker_type = "sphere"
            marker = self.plot_marker_rviz(pt_curr,scale_passed,color,marker_type,self.topic_name)
            marker.id = self.scatter_idx; self.scatter_idx = self.scatter_idx + 1;# print "scatter idx" ,self.scatter_idx
            self.marker_array_path_pts.markers.append(marker)
        self.cvx_path_points_pub.publish(self.marker_array_path_pts)
            #check type of pt, convert to list if necessary


    def hat_map(self,vec):
        if type(vec) == np.matrixlib.defmatrix.matrix: v1 = vec.item(0); v2 = vec.item(1); v3 = vec.item(2)
        elif type(vec) == list: v1 = vec[0]; v2 = vec[1]; v3 = vec[2]
        S = np.matrix([[0,-v3,v2],[v3,0,-v1],[-v2,v1,0]])
        return S

    def rotation_given_two_vectors(self,v1,v2):
        #this function gets the rotation matrix given to vectors: rotation from v1 to v2
        # print "v1 and v2 passed to rotation finder: ", v1, v2
        v1_normed = np.divide(v1,np.linalg.norm(v1)); v2_normed = np.divide(v2,np.linalg.norm(v2));
        theta = math.acos(v1_normed.T*v2_normed) #angle between two vectors
        omega = self.hat_map(v1_normed)*v2_normed
        omega = np.divide(omega,np.linalg.norm(omega))
        omega_hat = self.hat_map(omega)
        R = np.matrix(np.eye(3)) + np.multiply(math.sin(theta),omega_hat) + np.multiply((1 - math.cos(theta)), omega_hat**2)
        # print "ROTATION CHECK: ", v2_normed, " and ", R*v1_normed
        #rotation method validated
        return R

    def Plot_planes(self,plane_equation,plane_centroid):
        # print "calc planes"
        # print "plane eqn: ", plane_equation, " its type: ", type(plane_equation)
        # print "plane_centroid: ", plane_centroid, " its type: ", type(plane_centroid)
        scale = .5#8.0
        v1 = np.matrix([scale,0.,0.]).T; ang2 =120.*math.pi/180.; ang3 =240.*math.pi/180.;  
        R2 = np.matrix([[math.cos(ang2),-math.sin(ang2),0.],[math.sin(ang2), math.cos(ang2),0.],[0.,0.,1.]]); R3 = np.matrix([[math.cos(ang3),-math.sin(ang3),0.],[math.sin(ang3), math.cos(ang3),0.],[0.,0.,1.]])
        v2 = R2*v1; v3 = R3*v1
        plane_tri_vector = np.vstack([v1.T,v2.T,v3.T]) #assumes plane is x,y plane and plane vector is z vector: [0,0,1]
        # print "plane tri-vector: ", plane_tri_vector
        init_vector = np.matrix([0.,0.,1.]).T
        final_vector = plane_equation[0,:3].T
        # print "init and final vectors: ", init_vector, final_vector
        R_z_to_vec = self.rotation_given_two_vectors(init_vector, final_vector)
        # print "rotation matrix: ", R_z_to_vec, "determinant: ", np.linalg.det(R_z_to_vec)
        v1_rotated = R_z_to_vec*v1; v2_rotated = R_z_to_vec*v2; v3_rotated = R_z_to_vec*v3; #rotate these vectors
        plane_tri_vector = np.vstack([v1_rotated.T,v2_rotated.T,v3_rotated.T])
        final_plane_vectors = plane_tri_vector + np.matlib.repmat(plane_centroid,3,1)
        # print "final plane vectors: ", plane_tri_vector#final_plane_vectors
        augmented_vectors =  np.hstack([final_plane_vectors,np.matrix(np.ones([3,1]))])
        color = [0.,0.,1.]
        ball_scale = 0.2
        self.Plot_scatter_list(plane_centroid.tolist(), ball_scale,color) #to plot verticies of triangle
        #make triangular plane points: 
        pt1 =  geomPoint(); pt1.x = final_plane_vectors[0,0];pt1.y = final_plane_vectors[0,1];pt1.z = final_plane_vectors[0,2]
        pt2 =  geomPoint(); pt2.x = final_plane_vectors[1,0];pt2.y = final_plane_vectors[1,1];pt2.z = final_plane_vectors[1,2]
        pt3 =  geomPoint(); pt3.x = final_plane_vectors[2,0];pt3.y = final_plane_vectors[2,1];pt3.z = final_plane_vectors[2,2]
        
        if self.planes_initialized == False:
            self.plane_marker = Marker()
            self.plane_marker.header.frame_id = self.topic_name
            self.plane_marker.points.append(pt1);self.plane_marker.points.append(pt2);self.plane_marker.points.append(pt3)
            self.plane_marker.type = self.plane_marker.TRIANGLE_LIST
            color = [0.,1.,0.]
            self.plane_marker.color.a = 1.0 #may want to increase transparancy
            self.plane_marker.color.r = color[0]
            self.plane_marker.color.g = color[1]
            self.plane_marker.color.b = color[2]
            scale = [1.,1.,1.]
            self.plane_marker.scale.x = scale[0] #may be different though
            self.plane_marker.scale.y = scale[1]
            self.plane_marker.scale.z = scale[2]
            # self.plane_marker.id = self.planes_idx; self.planes_idx = self.planes_idx + 1; print "planes idx" ,self.planes_idx
            # self.marker_array_planes.markers.append(plane_marker)
            self.planes_initialized = True
        else:
            self.plane_marker.points.append(pt1);self.plane_marker.points.append(pt2);self.plane_marker.points.append(pt3)
        self.planes_pub.publish(self.plane_marker)
        # self.planes_pub.publish(self.marker_array_planes)

    def Plot_vectors(self,pt_start,pt_end):
        # diff = np.array(pt_end - pt_start)[0]
        pt_start = np.array(pt_start)
        pt_end = np.array(pt_end)
        # print "pts: ", pt_start, pt_end
        pt1 = geomPoint(); pt2 = geomPoint()
        pt1.x = pt_start[0]; pt1.y = pt_start[1]; pt1.z = pt_start[2]
        pt2.x = pt_end[0]; pt2.y = pt_end[1]; pt2.z = pt_end[2]

        marker_type = "vector"
        color = [1.,0.,0.]
        vector_marker =  Marker()
        vector_marker.header.frame_id =self.topic_name #= self.plot_marker_rviz(pt_start,diff,color,marker_type)
        vector_marker.points.append(pt1)
        vector_marker.points.append(pt2)
        scale = [.1,.2,.5]
        vector_marker.scale.x = scale[0] #may be different though
        vector_marker.scale.y = scale[1]
        vector_marker.scale.z = scale[2]
        vector_marker.color.a = 1.0  #transparency?
        vector_marker.color.r = color[0]
        vector_marker.color.g = color[1]
        vector_marker.color.b = color[2]
        # vector_marker.pose.position.x = pt_start[0]
        # vector_marker.pose.position.y = pt_start[1]
        # vector_marker.pose.position.z = pt_start[2]
        # vector_marker.pose.orientation.w = 1.0

        self.vector_pub.publish(vector_marker)



    def plot_marker_rviz(self,point,scale,color,m_type,frame_id,*orient_arg):
        # print "make points"
        marker = Marker()
        marker.header.frame_id = frame_id
        if m_type in "vector":
            marker.type = marker.ARROW
        elif m_type in "sphere":
            marker.type = marker.SPHERE
            marker.pose.orientation.w = 1.0
        elif m_type in "cube":
            marker.type= marker.CUBE
        marker.action = marker.ADD
        # marker.lifetime = 400 #time its alive
        marker.scale.x = scale[0] #may be different though
        marker.scale.y = scale[1]
        marker.scale.z = scale[2]
        marker.color.a = 1.0  #transparency?
        marker.color.r = color[0]
        marker.color.g = color[1]
        marker.color.b = color[2]
        
        # print "the point: ", point
        marker.pose.position.x = point[0]
        marker.pose.position.y = point[1]
        marker.pose.position.z = point[2]
        if len(orient_arg) == 4:
            #specify the orientation
            marker.pose.orientation.w = orient_arg[0]
            marker.pose.orientation.x = orient_arg[1]
            marker.pose.orientation.y = orient_arg[2]
            marker.pose.orientation.z = orient_arg[3]

        return marker


