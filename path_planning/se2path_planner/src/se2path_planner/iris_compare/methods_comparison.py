#!/usr/bin/env python

import rospy
import irispy
import numpy as np
from scipy.spatial import ConvexHull
from ompl import base as ob
from ompl import geometric as og
from ompl import control, tools, util
from se2path_planner.convex_corridor.cvx_corridor_alg import CVX_corridor_decomp
from shapely.geometry import Point as shapely_Point, Polygon as shapely_Polygon

from visualization_msgs.msg import Marker

from sensor_msgs.msg import PointCloud  #for visualizing cells
from geometry_msgs.msg import Point32, Point

class MethodCompClass(object):
  def __init__(self,show, num_obs, world_bounds={'lower':[-1,-1,0], 'upper':[1,1,0]}, voxel_points=51, origin_bounding_box={'lower':[-0.31,-0.1,0.0], 'upper':[0.31,0.1,0.0]}):
    self.show = show
    self.num_obs = num_obs
    self.world_bounds = world_bounds
    self.bounds = irispy.Polyhedron.from_bounds(world_bounds['lower'], world_bounds['upper'])
    self.obstacles = []
    # self.start= np.array([0.3,0,0])
    # self.goal = np.array([-0.3,0,0])
    self.start= np.array([0.3,0])
    self.goal = np.array([-0.3,0])
    self.midpoint = (self.start + self.goal)*0.5
    self.cvx_planner = CVX_corridor_decomp()


    #DRAW CHORD
    self.chord_marker = Marker()
    self.chord_marker.header.frame_id = '/map'
    self.chord_marker.ns = "test"
    self.chord_pub = rospy.Publisher('/chord_arrow',Marker,queue_size=1,latch=True)
    # self.start_cloud = PointCloud(); self.start_cloud.header.frame_id='/map'
    self.start_marker = Point(); self.start_marker.x=self.start[0]; self.start_marker.y=self.start[1];
    self.chord_marker.points.append(self.start_marker)
    # self.pub_start_point = rospy.Publisher('/start_pt',PointCloud,queue_size=1,latch=True)
    # self.goal_cloud = PointCloud(); self.goal_cloud.header.frame_id='/map'
    self.goal_marker = Point(); self.goal_marker.x=self.goal[0]; self.goal_marker.y=self.goal[1];
    self.chord_marker.points.append(self.goal_marker)
    # self.pub_goal_point = rospy.Publisher('/goal_pt',PointCloud,queue_size=1,latch=True)
    self.chord_marker.scale.x = 0.03 #shaft diameter
    self.chord_marker.scale.y = 0.05 #head diameter
    self.chord_marker.scale.z = 0.05 #head length

    self.chord_marker.color.a = 1.0
    self.chord_marker.color.r = 1.0
    self.chord_marker.color.g = .0
    self.chord_marker.color.b = .0


    self.origin_bounding_box = origin_bounding_box

    self.voxel_pts = voxel_points

    self.pub_world_pts = rospy.Publisher('/world_pts', PointCloud, queue_size=1, latch=True)
    self.pub_obs_pts = rospy.Publisher('/obstacle_pts', PointCloud, queue_size=1, latch=True)
    self.pub_iris_pts = rospy.Publisher('/iris_pts', PointCloud, queue_size=1, latch=True)
    self.pub_sikang_pts = rospy.Publisher('/sikang_pts', PointCloud, queue_size=1, latch=True)
    self.pub_sikang_edge_pts = rospy.Publisher('/sikang_edge_pts', PointCloud, queue_size=1, latch=True)


    self.pub_cvx_pts = rospy.Publisher('/cvx_pts', PointCloud, queue_size=1, latch=True)

    #make method statistics
    self.my_cvx_method = {'avg':0,'std_dev':0, 'list':[]}
    self.iris_method = {'avg':0,'std_dev':0, 'list':[]}
    self.method_stat = {'my_cvx_over_iris':[],'my_cvx_over_iris_avg':0, 'my_cvx_over_iris_std_dev':0,
                        'liu_over_iris':[],'liu_over_iris_avg':0, 'liu_over_iris_std_dev':0 } #percentage (0-1)
    self.lui_method= {'avg':0,'std_dev':0, 'list':[]}

  def Generate_obstacles(self):
    bounds = self.bounds
    obstacles = []
    obs_centers = []

    num_valid_samples = 0

    while num_valid_samples < self.num_obs:
      # for i in range(self.num_obs):
      # center = np.random.random((2,))
      center_scale = np.random.random((len(self.world_bounds['lower']),))
      center = [center_scale[0]*(self.world_bounds['upper'][0]-self.world_bounds['lower'][0])-1.0,
               center_scale[1]*(self.world_bounds['upper'][1]-self.world_bounds['lower'][1])-1.0]#,
               # center_scale[2]*(self.world_bounds['upper'][2]-self.world_bounds['lower'][2])-1.0] 
      for idx in range(len(self.world_bounds['lower'])):
        if center[idx] > self.world_bounds['upper'][idx]:
          center[idx] = self.world_bounds['upper'][idx]
        elif center[idx] < self.world_bounds['lower'][idx]:
          center[idx] = self.world_bounds['lower'][idx]
      # scale = np.random.random() * 0.3
      scale_rand = np.random.random()
      scale = [scale_rand*(self.world_bounds['upper'][0]-self.world_bounds['lower'][0]) * 0.3,
               scale_rand*(self.world_bounds['upper'][1]-self.world_bounds['lower'][1]) * 0.3]#,
               # scale_rand*(self.world_bounds['upper'][2]-self.world_bounds['lower'][2]) * 0.3] 
      #generate points

      #ensure they dont overlap
      theta = np.random.rand()*np.pi
      v1 = (np.random.rand()) * np.array([1,0])
      v2 = (np.random.rand()) * np.array([0,1])
      v1_r = np.array([(np.cos(theta)*v1[0] + np.sin(theta)*v1[1]),
                      (-np.sin(theta)*v1[0] + np.cos(theta)*v1[1])])
      v2_r = np.array([(np.cos(theta)*v2[0] + np.sin(theta)*v2[1]),
                      (-np.sin(theta)*v2[0] + np.cos(theta)*v2[1])])

      pts= np.array([[v1_r[0]+v2_r[0],v1_r[0]-v2_r[0],-v1_r[0]+v2_r[0],-v1_r[0]-v2_r[0]],
                     [v1_r[1]+v2_r[1],v1_r[1]-v2_r[1],-v1_r[1]+v2_r[1],-v1_r[1]-v2_r[1]]])

      # pts = np.random.random((len(self.world_bounds['lower']),4))
      # pts = pts - np.mean(pts, axis=1)[:,np.newaxis]
      pts_scaled = np.array([[pts[idx,jdx]*scale[idx] for jdx in range(4)] for idx in range(len(self.world_bounds['lower']))])
      # pts = pts_scaled + center[:,np.newaxis]
      pts= np.array([[ pts_scaled[idx,jdx] + center[idx] for jdx in range(4)] for idx in range(len(self.world_bounds['lower']))])
      
      #Check that the proposed point is outside of the center region, if so then add it to list 

      #Generate many points inside proposed polygon
      start_pt_formed = shapely_Point(self.start[0],self.start[1])
      goal_pt_formed = shapely_Point(self.goal[0],self.goal[1])
      midpoint_pt_formed = shapely_Point(self.midpoint[0],self.midpoint[1])

      pts_sorted = self.order_points_old(pts.transpose())
      pts_sorted = pts_sorted.transpose()
      poly_formed_input = [(pts_sorted[0,i],pts_sorted[1,i]) for i in range(pts_sorted.shape[1])]
      poly_formed = shapely_Polygon(poly_formed_input)


      if poly_formed.contains(start_pt_formed) or poly_formed.contains(goal_pt_formed) or poly_formed.contains(midpoint_pt_formed):
        valid_sample = False
      else:
        valid_sample = True

      # valid_sample = True
      for idx in range(4):
        pt_corn_curr = pts[:,idx]
        if pt_corn_curr[0] <= self.origin_bounding_box['upper'][0] and pt_corn_curr[0] >= self.origin_bounding_box['lower'][0]:
          if pt_corn_curr[1] <= self.origin_bounding_box['upper'][1] and pt_corn_curr[1] >= self.origin_bounding_box['lower'][1]:
            # if pt_corn_curr[2] <= self.origin_bounding_box['upper'][2] and pt_corn_curr[2] >= self.origin_bounding_box['lower'][2]:
            valid_sample = False
      if valid_sample:
        num_valid_samples += 1 #add this obstacle to the list
        # print "num pts added:", num_valid_samples
        # pts_sorted = self.order_points_old(pts.transpose())
        # pts = pts_sorted.transpose()
        obstacles.append(pts)
        obs_centers.append(center)
      #choose this
      # start = np.array([0.5, 0.5])
    #hullobs= ConvexHull(obstacles[0].transpose())  and hullobs.equations, multiplying [px,py,1]^T should all have the same sign (strict negative): np.matrix(htest.equations)*np.matrix(orgin)
    self.obstacles = obstacles
    # self.obstacles = [np.array([[-0.2+0.5,-0.2+0.5,0.2+0.5,0.2+0.5],
    #                             [-0.2,0.2,-0.2,0.2]]),
    #                   np.array([[-0.2-0.2,-0.2-0.5,0.2-0.5,0.2-0.5],
    #                             [-0.2-0.5,0.2-0.5,-0.2-0.5,0.2-0.5]])]

    for idx in range(len(self.obstacles)):
      pts = self.obstacles[idx]
      pts_sorted = self.order_points_old(pts.transpose())
      pts = pts_sorted.transpose()
      self.obstacles[idx] = pts

    self.obs_centers = obs_centers
    # print "centers: ", obs_centers
    # return obstacles


  def order_points_old(self,pts):
    # initialize a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype="float32")
   
    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
   
    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
   
    # return the ordered coordinates
    return rect


  def Generate_world_point_cloud(self):
    self.world_voxels = PointCloud()
    self.world_voxels.header.frame_id = 'map'
    self.xpts = np.unique(np.linspace(self.world_bounds['lower'][0],self.world_bounds['upper'][0], self.voxel_pts))
    self.ypts = np.unique(np.linspace(self.world_bounds['lower'][1],self.world_bounds['upper'][1], self.voxel_pts))
    # zpts = np.unique(np.linspace(self.world_bounds['lower'][2],self.world_bounds['upper'][2], self.voxel_pts))

    for x_steps in self.xpts:
      for y_steps in self.ypts:
        # for z_steps in zpts:
        pt_curr = Point32()
        pt_curr.x = x_steps
        pt_curr.y = y_steps
        # pt_curr.z = z_steps
        self.world_voxels.points.append(pt_curr)






    #now generate point cloud for points interior to obstacles
    self.occupied_pt_cloud = PointCloud()
    self.occupied_pt_cloud.header.frame_id = 'map'

    self.free_pt_cloud = PointCloud()
    self.free_pt_cloud.header.frame_id='/map'

    # print "world pts: ", self.world_voxels.points
    # print "range",range(len(self.world_voxels.points))
    # stop
    for world_pt_idx in range(len(self.world_voxels.points)):
      curr_pt = self.world_voxels.points[world_pt_idx]  #find current point in world list

      curr_pt_formed = shapely_Point(curr_pt.x,curr_pt.y)

      for obs_idx in range(len(self.obstacles)):
        curr_obs = self.obstacles[obs_idx]  #see if this point is interior to the current obstacle
        #USE CORRECT POLYGON CHECK:
        poly_formed_input = [(curr_obs[0,i],curr_obs[1,i]) for i in range(curr_obs.shape[1])]
        poly_formed = shapely_Polygon(poly_formed_input)



        # # print "curr_obs:", curr_obs
        # # print "curr_pt:", curr_pt
        # if curr_pt.x <= np.max(curr_obs[0,:]) and curr_pt.x >= np.min(curr_obs[0,:]):
        #   # print "pt was in x"
        #   if curr_pt.y <= np.max(curr_obs[1,:]) and curr_pt.y >= np.min(curr_obs[1,:]):
        #     # print "pt was in y"
        #     # if curr_pt.z <= np.max(curr_obs[2,:]) and curr_pt.z >= np.min(curr_obs[2,:]):
        if poly_formed.contains(curr_pt_formed):
          self.occupied_pt_cloud.points.append(curr_pt)
        else:
          self.free_pt_cloud.points.append(curr_pt)

  def publish_point_clouds(self):
    self.pub_world_pts.publish(self.world_voxels)
    self.pub_obs_pts.publish(self.occupied_pt_cloud)
    self.pub_iris_pts.publish(self.iris_point_cloud)
    self.pub_sikang_pts.publish(self.sikang_point_cloud)
    self.pub_cvx_pts.publish(self.monroe_cvx_point_cloud)

    #PUBLISH START AND GOAL POINTS
    self.chord_pub.publish(self.chord_marker)
    # self.pub_start_point.publish(self.start_cloud)
    # self.pub_goal_point.publish(self.goal_cloud)


    # self.pub_sikang_edge_pts.publish(self.sikang_edge_pts)
    # self.pub_cvx_pts.publish()



  def Iris_method_volume(self, show=False):
    #solve the iris method
    # print "obstacle points: ", self.obstacles
    region,debug = irispy.inflate_region(self.obstacles,self.midpoint,bounds=self.bounds, return_debug_data=True)
    # region.polyhedron.contains(np.array([10,1]),0.0)  returns true or false
    #test every point:
    # region.polyhedron.contains(pt,0.0)

    self.iris_point_cloud = PointCloud()
    self.iris_point_cloud.header.frame_id = 'map'
    for idx in range(len(self.world_voxels.points)):
      pt_curr = np.array([self.world_voxels.points[idx].x,self.world_voxels.points[idx].y,self.world_voxels.points[idx].z])
      if region.polyhedron.contains(pt_curr,0.0):
        self.iris_point_cloud.points.append(self.world_voxels.points[idx])

    debug.animate(pause=0.5, show=show)

  def Sikang_method_volume_correct(self):
    xhat_start = self.xpts - self.start[0]
    chord_start_idx = np.argmin(np.abs(xhat_start))
    xhat_end = self.xpts - self.goal[0]
    chord_goal_idx = np.argmin(np.abs(xhat_end))
    yhat_start = self.ypts - self.start[1]
    ypt_start_idx = np.argmin(np.abs(yhat_start))


    # xpts_examine = self.xpts[chord_goal_idx:chord_start_idx]
    # ypts_examine = self.ypts[chord_goal_idx:chord_start_idx]

    #Sikang algorithm
    x_prev = 0.; y_prev = 0. #store previous values
    pt_store = []
    self.sikang_point_cloud = PointCloud()
    self.sikang_point_cloud.header.frame_id = '/map'
    for idx in range(chord_goal_idx,chord_start_idx):
      #given the current points
      xpt_curr = self.xpts[idx]#; ypt_curr = self.ypts[idx]
      #for a given x value, find the max and min y points on that line (if any exist)
      curr_obst_max_y = 1; curr_obs_min_y = -1 #start by filling the world boundary
      # for jdx in range(len(self.ypts)):


      #ray trace up
      ray_bool = False
      for idx_trace in range(int((self.voxel_pts-1)*0.5)):
        ypt = self.ypts[ypt_start_idx-idx_trace]
        pt_test_max = Point32()
        pt_test_max.x = xpt_curr;  pt_test_max.y = ypt

        for mdx in range(len(self.occupied_pt_cloud.points)):
          curr_test_pt = self.occupied_pt_cloud.points[mdx]
          if curr_test_pt.x == xpt_curr:
            if curr_test_pt.y == ypt:
              ray_bool = True
              # print "\ncurrent test pt: ", xpt_curr,ypt, " compared to", curr_test_pt
              break
              
        if ray_bool == True:
          break
        else:
          # print "adding the point", ypt
          self.sikang_point_cloud.points.append(pt_test_max)



      #ray trace down
      ray_bool = False
      for idx_trace in range(int((self.voxel_pts-1)*0.5)):
        ypt = self.ypts[ypt_start_idx+idx_trace]
        pt_test_min = Point32()
        pt_test_min.x = xpt_curr; pt_test_min.y = ypt

        for mdx in range(len(self.occupied_pt_cloud.points)):
          curr_test_pt = self.occupied_pt_cloud.points[mdx]
          if curr_test_pt.x == xpt_curr:
            if curr_test_pt.y == ypt:
              ray_bool = True
              # print "\ncurrent test pt: ", xpt_curr,ypt, " compared to", curr_test_pt
              break
              
        if ray_bool == True:
          break
        else:
          # print "adding the point", ypt
          self.sikang_point_cloud.points.append(pt_test_min)



  def Sikang_method_volume(self):
    #given vectors [1,0],[-1,0],[0,-1],[0,1] to expand
    plane_vectors = [np.array([1,0,-0.3]),
                     np.array([-1,0,-0.3]),
                     np.array([0,1,0]),
                     np.array([0,-1,0])]

    #keep a running list of obstacle points to check
    obs_pt_cloud_list = []
    for idx in range(len(self.occupied_pt_cloud.points)):
      obs_pt_cloud_list.append(np.array([self.occupied_pt_cloud.points[idx].x,self.occupied_pt_cloud.points[idx].y]))


    #1. first find points right chord (right start point), (sort and find minimum, which is used to define new plane upward), remove all positive points from points to check
    right_point = {'point':[0,0],'dist':1e100}
    obs_pt_cloud_list_temp = []
    for idx in range(len(obs_pt_cloud_list)):
      curr_pt = obs_pt_cloud_list[idx]
      val_pt = plane_vectors[0][0]*curr_pt[0] + plane_vectors[0][1]*curr_pt[1] + plane_vectors[0][2]
      if val_pt >= 0:
        if val_pt < right_point['dist']:
          right_point['point'] = curr_pt
          right_point['dist'] = -(plane_vectors[0][0]*curr_pt[0] + plane_vectors[0][1]*curr_pt[1])
      else:
        obs_pt_cloud_list_temp.append(curr_pt)
    if right_point['dist'] == 1e100:
      right_point['dist'] = -self.world_bounds['upper'][0] #this means the origin was the only point, so use the world boundary
    obs_pt_cloud_list = obs_pt_cloud_list_temp




    #2. second find points left chord (left end point), (sort and find minimum, which is used to define a new plane downward), remove all positive points from points to check
    left_point = {'point':[0,0],'dist':1e100}
    obs_pt_cloud_list_temp = []
    for idx in range(len(obs_pt_cloud_list)):
      curr_pt = obs_pt_cloud_list[idx]
      val_pt = plane_vectors[1][0]*curr_pt[0] + plane_vectors[1][1]*curr_pt[1] + plane_vectors[1][2]
      if val_pt >= 0:
        if val_pt < left_point['dist']:
          left_point['point'] = curr_pt
          left_point['dist'] = -(plane_vectors[1][0]*curr_pt[0] + plane_vectors[1][1]*curr_pt[1])
      else:
        obs_pt_cloud_list_temp.append(curr_pt)
    if left_point['dist'] == 1e100:
      left_point['dist'] = -np.abs(self.world_bounds['lower'][0]) #this means the origin was the only point, so use the world boundary
    obs_pt_cloud_list = obs_pt_cloud_list_temp

    #3. third find points above (from remaining points)
    top_point = {'point':[],'dist':1e100}
    obs_pt_cloud_list_temp = []
    for idx in range(len(obs_pt_cloud_list)):
      curr_pt = obs_pt_cloud_list[idx]
      val_pt = plane_vectors[2][0]*curr_pt[0] + plane_vectors[2][1]*curr_pt[1] + plane_vectors[2][2]
      if val_pt >= 0:
        if val_pt < top_point['dist']:
          top_point['point'] = curr_pt
          top_point['dist'] = -(plane_vectors[2][0]*curr_pt[0] + plane_vectors[2][1]*curr_pt[1])
      else:
        obs_pt_cloud_list_temp.append(curr_pt)
    if top_point['dist'] == 1e100:
      top_point['dist'] = -self.world_bounds['upper'][1] #this means the origin was the only point, so use the world boundary
    obs_pt_cloud_list = obs_pt_cloud_list_temp

    #4. fourth fidn points below (from remaining points)
    bottom_point = {'point':[0,0],'dist':1e100}
    obs_pt_cloud_list_temp = []
    for idx in range(len(obs_pt_cloud_list)):
      curr_pt = obs_pt_cloud_list[idx]
      val_pt = plane_vectors[3][0]*curr_pt[0] + plane_vectors[3][1]*curr_pt[1] + plane_vectors[3][2]
      if val_pt >= 0:
        if val_pt < bottom_point['dist']:
          bottom_point['point'] = curr_pt
          bottom_point['dist'] = -(plane_vectors[3][0]*curr_pt[0] + plane_vectors[3][1]*curr_pt[1])
      else:
        obs_pt_cloud_list_temp.append(curr_pt)
    if bottom_point['dist'] == 1e100:
      bottom_point['dist'] = -np.abs(self.world_bounds['lower'][0]) #this means the origin was the only point, so use the world boundary
    obs_pt_cloud_list = obs_pt_cloud_list_temp

    
    #4. Next, construct planes with each of these
    new_planes = [np.array([1,0,right_point['dist']]),
                  np.array([-1,0,left_point['dist']]),
                  np.array([0,1,top_point['dist']]),
                  np.array([0,-1,bottom_point['dist']])]

    #5. Now go through the world points, and determine which points are interior to all new planes:
    self.sikang_point_cloud = PointCloud()
    self.sikang_point_cloud.header.frame_id = '/map'
    for idx in range(len(self.world_voxels.points)):
      curr_pt = self.world_voxels.points[idx]
      val_right = new_planes[0][0]*curr_pt.x + new_planes[0][1]*curr_pt.y + new_planes[0][2]
      val_left = new_planes[1][0]*curr_pt.x + new_planes[1][1]*curr_pt.y + new_planes[1][2]
      val_top = new_planes[2][0]*curr_pt.x + new_planes[2][1]*curr_pt.y + new_planes[2][2]
      val_bottom = new_planes[3][0]*curr_pt.x + new_planes[3][1]*curr_pt.y + new_planes[3][2]
      # if val_right < 0 and val_left < 0 and val_top < 0 and val_bottom < 0:
      if val_right < 0:# and val_left < 0 and val_top < 0 and val_bottom < 0:
        self.sikang_point_cloud.points.append(curr_pt)


    self.sikang_edge_pts = PointCloud()
    self.sikang_edge_pts.header.frame_id = '/map'
    right_pt = Point32();left_pt = Point32();top_pt = Point32();bottom_pt = Point32();
    right_pt.x = right_point['point'][0]; right_pt.y = right_point['point'][1]
    left_pt.x = left_point['point'][0]; left_pt.y = left_point['point'][1]
    top_pt.x = top_point['point'][0]; top_pt.y = top_point['point'][1]
    bottom_pt.x = bottom_point['point'][0]; bottom_pt.y = bottom_point['point'][1]
    self.sikang_edge_pts.points.append(right_pt) 
    # self.sikang_edge_pts.points.append(left_pt)
    # self.sikang_edge_pts.points.append(top_pt)
    # self.sikang_edge_pts.points.append(bottom_pt)



  def unique_obstacles(self,obstacles_list):
    unique_obs_list = []
    for idx in range(len(obstacles_list)):
      curr_pt = obstacles_list[idx]
      flag_present = False
      for jdx in range(len(unique_obs_list)):
        curr_test = unique_obs_list[jdx]
        if curr_test[0]==curr_pt[0] and curr_test[1]==curr_pt[1]:
          flag_present = True
      if flag_present == False:
        unique_obs_list.append(curr_pt)
    return unique_obs_list


  def My_method_volume(self):

    #make a list of obstacles
    obstacles_list = []
    for idx in range(len(self.occupied_pt_cloud.points)):
      pt_curr = [self.occupied_pt_cloud.points[idx].x,self.occupied_pt_cloud.points[idx].y]
      obstacles_list.append(pt_curr)

    obstacles_list = self.unique_obstacles(obstacles_list)

    #make a list of world planes
    world_planes = [[ 1, 0,-1],
                    [ 0, 1,-1],
                    [-1, 0,-1],
                    [ 0,-1,-1]]

    #make a list of path points
    path_pts = [self.start.tolist(),self.goal.tolist()]
    path_pts[0][1]=0.01; path_pts[1][1]=0.01

    Aset, active_obs_pts = self.cvx_planner.Corridor_generator(path_pts,obstacles_list, world_planes, True) #returns list of lists of planes for each chord

    self.monroe_cvx_point_cloud = PointCloud()
    self.monroe_cvx_point_cloud.header.frame_id = '/map'
    for idx in range(len(self.world_voxels.points)):
      curr_pt = self.world_voxels.points[idx]
      interior_bool = True
      for mdx in range(len(Aset[0])):
        curr_plane = Aset[0][mdx]

        if curr_plane[0]*curr_pt.x + curr_plane[1]*curr_pt.y + curr_plane[2] >= 0:
          interior_bool = False
      if interior_bool:
        self.monroe_cvx_point_cloud.points.append(curr_pt)


  def area_statistics(self):

    if len(self.my_cvx_method['list']) > 3 and len(self.iris_method['list']) > 3 and len(self.lui_method['list'])>3:
      #append new points
      self.my_cvx_method['list'].append(len(self.monroe_cvx_point_cloud.points))
      self.lui_method['list'].append(len(self.sikang_point_cloud.points))
      self.iris_method['list'].append(len(self.iris_point_cloud.points))
      division = float(len(self.monroe_cvx_point_cloud.points))/float(len(self.iris_point_cloud.points))
      self.method_stat['my_cvx_over_iris'].append(division)
      lui_division = float(len(self.sikang_point_cloud.points))/float(len(self.iris_point_cloud.points))
      self.method_stat['liu_over_iris'].append(lui_division)
      #calculate respective statistics
      self.my_cvx_method['avg'] = np.mean(self.my_cvx_method['list'])
      self.my_cvx_method['std_dev'] = np.std(self.my_cvx_method['list'])

      self.iris_method['avg'] = np.mean(self.iris_method['list'])
      self.iris_method['std_dev'] = np.std(self.iris_method['list'])
      #calculate respecive statistics

      self.method_stat['my_cvx_over_iris_avg'] = np.mean(self.method_stat['my_cvx_over_iris'])
      self.method_stat['my_cvx_over_iris_std_dev'] = np.std(self.method_stat['my_cvx_over_iris'])


      self.method_stat['liu_over_iris_avg'] = np.mean(self.method_stat['liu_over_iris'])
      self.method_stat['liu_over_iris_std_dev'] = np.std(self.method_stat['liu_over_iris'])




    else:
      #just append points
      self.my_cvx_method['list'].append(len(self.monroe_cvx_point_cloud.points))
      self.iris_method['list'].append(len(self.iris_point_cloud.points))
      self.lui_method['list'].append(len(self.sikang_point_cloud.points))
      division = float(len(self.monroe_cvx_point_cloud.points))/float(len(self.iris_point_cloud.points))
      self.method_stat['my_cvx_over_iris'].append(division)

      lui_division = float(len(self.sikang_point_cloud.points))/float(len(self.iris_point_cloud.points))
      self.method_stat['liu_over_iris'].append(lui_division)








def main():
  rospy.init_node('method_comparison')

  num_obs = 50
  num_trials = 100
  #instantiate class
  world_bounds = {'upper':[1.,1.], 'lower':[-1.,-1.]}
  origin_bounding_box={'lower':[-0.31,-0.21], 'upper':[0.31,0.21]}
  voxel_points = 101#50

  cls_obj = MethodCompClass(True,num_obs, voxel_points=voxel_points, world_bounds=world_bounds, origin_bounding_box=origin_bounding_box)
  #generate obstacles that do not violate chord distance contraint (dont overlap proposed chord (one chord from start to goal whose midpoint is the origin))


  for idx in range(num_trials):


    cls_obj.Generate_obstacles()
    #generate world voxel points
    cls_obj.Generate_world_point_cloud()

    #find points in iris method:
    cls_obj.Iris_method_volume(show=False)


    #sikang method: 
    # cls_obj.Sikang_method_volume()
    cls_obj.Sikang_method_volume_correct()

    #my method: 
    cls_obj.My_method_volume()


    cls_obj.area_statistics()

    #publish the point clouds (world and obstacle)
    # for rdx in range(5):
    cls_obj.publish_point_clouds()
    rospy.sleep(0.05)

  print "the final statistics:", cls_obj.method_stat


  rospy.loginfo("published point clouds")
  # while not rospy.is_shutdown():
  #   cls_obj.publish_point_clouds()
  #   rospy.sleep(1.0)


  #statistics: 30 obs, 50 trials, cvx/iris  0.76avg, 0.35 stdev


if __name__ == '__main__':
  main()