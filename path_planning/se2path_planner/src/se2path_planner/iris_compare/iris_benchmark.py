import irispy
import numpy as np
from scipy.spatial import ConvexHull
from ompl import base as ob
from ompl import geometric as og
from ompl import control, tools, util
from se2path_planner.convex_corridor.cvx_corridor_alg import CVX_corridor_decomp




class MySE2(ob.SE2StateSpace):
    def distance(self,state1, state2):
        distxy = np.sqrt((state1.getX()-state2.getX())**2 + (state1.getY()-state2.getY())**2)
        return distxy


class Comparison_btw_iris_cvx_hull(object):
    """docstring for Comparison_btw_iris_cvx_hull"""
    def __init__(self, show, num_obs):
        self.show = show
        self.num_obs = num_obs
        self.bounds = irispy.Polyhedron.from_bounds([0, 0], [1, 1])
        self.obstacles = []
        self.start= []
        self.goal = []
        self.rrt_path = []

        self.cvx_planner = CVX_corridor_decomp()

    def Define_world(self,num_obs):
        bounds = self.bounds
        obstacles = []
        for i in range(num_obs):
            center = np.random.random((2,))
            scale = np.random.random() * 0.3
            pts = np.random.random((2,4))
            pts = pts - np.mean(pts, axis=1)[:,np.newaxis]
            pts = scale * pts + center[:,np.newaxis]
            obstacles.append(pts)
            #choose this
            # start = np.array([0.5, 0.5])
        #hullobs= ConvexHull(obstacles[0].transpose())  and hullobs.equations, multiplying [px,py,1]^T should all have the same sign (strict negative): np.matrix(htest.equations)*np.matrix(orgin)
        self.obstacles = obstacles
        return obstacles

    def define_start_and_goal(self):
        def resample():
            start = np.random.random((2,)); goal = np.random.random((2,))
            return start, goal
        start,goal = resample()
        if start.item(0) == goal.item(0) and start.item(1) == goal.item(1): start,goal = resample()

        start_in_obstacle_bool = self.check_interior_to_obstacles(start)
        goal_in_obstacle_bool = self.check_interior_to_obstacles(goal)

        while start_in_obstacle_bool == True or goal_in_obstacle_bool == True:
            start,goal = resample()
            start_in_obstacle_bool = self.check_interior_to_obstacles(start)
            goal_in_obstacle_bool = self.check_interior_to_obstacles(goal)
        #final start and goal that aren't in obstacles but inside world of (0,0):(1,1)
        return start, goal

    def check_interior_to_obstacles(self,point):
        obstacles = self.obstacles
        #return True if interior, False if not interior to any
        interior_bool = False
        point_test = np.matrix([[point.item(0)],[point.item(1)],[1.]])
        for idx in range(len(obstacles)):
            hullobs= ConvexHull(obstacles[idx].transpose())
            pt_signs = np.sign( np.matrix(hullobs.equations)*point_test)
            interior_bool= False not in [pt_signs.item(idx)==-1 for idx in range(np.max(pt_signs.shape))]
            if interior_bool == True:
                return interior_bool
                break
        return interior_bool

    def IRIS_test_random_obstacles_2d(self,show=False):
        bounds = self.bounds
        obstacles = []
        for i in range(10):
            center = np.random.random((2,))
            scale = np.random.random() * 0.3
            pts = np.random.random((2,4))
            pts = pts - np.mean(pts, axis=1)[:,np.newaxis]
            pts = scale * pts + center[:,np.newaxis]
            obstacles.append(pts)
            #choose this
            start = np.array([0.5, 0.5])
        region, debug = irispy.inflate_region(obstacles, start, bounds=bounds, return_debug_data=True)
        #find the convex hull
        iris_hull = ConvexHull(region.polyhedron.generatorPoints()) #hull object, requires points (2D) in list [array(x1,y1),..,array(xn,yn)]
        iris_hull.vertices #this is the order for the verticies around
        iris_hull.volume #MOST IMPORTANT, this is the area of the polyhedron
        #may want to do this at the end
        debug.animate(pause=0.5, show=show)
        return region,debug,iris_hull



        #the following is for rrt-connect
    def isStateValid(self,state):
        curr_state_xy = np.matrix([state.getX(),state.getY()]).T
        state_inside_object = self.check_interior_to_obstacles(curr_state_xy)
        valid_state = state_inside_object is False
        return valid_state
    def convert_path_to_list(self,path):
        # x = []; y = []
        # path_list_first = {'x':[],'y':[]}
        path_list = []
        for idx in range(len(path.getStates())):
            # x.append(path.getStates()[idx].getX())
            # y.append(path.getStates()[idx].getY())
            path_list.append([path.getStates()[idx].getX(), path.getStates()[idx].getY()])
        # path_list_first['x'] = x
        # path_list_first['y'] = y
        # return path_list_first
        return path_list

    def ompl_path_plan(self, start_pt, goal_pt):
        #define world bounds in ompl
        bounds = ob.RealVectorBounds(2) 
        bounds.setLow(0,0);bounds.setLow(1,0);
        bounds.setHigh(0,1);bounds.setHigh(1,1);
        #define space in ompl, with start and goals
        space = MySE2(); space.setBounds(bounds)
        start = ob.State(space); start().setX(start_pt.item(0)); start().setY(start_pt.item(1))
        goal = ob.State(space); goal().setX(goal_pt.item(0)); goal().setY(goal_pt.item(1))

        si = ob.SpaceInformation(space); si.setStateValidityChecker(ob.StateValidityCheckerFn(self.isStateValid))

        pd = ob.ProblemDefinition(si); pd.setStartAndGoalStates(start,goal)

        planner = og.RRTConnect(si)
        planner.setProblemDefinition(pd)
        planner.setup()

        solved = planner.solve(10.0)

        #interesting: 
        # ps = og.PathSimplifier(si)
        # ps.smoothBSpline(path) #makes bspline path out of path variable

        if solved:
            path = pd.getSolutionPath()
            path_list_first=self.convert_path_to_list(path)
            print "path found", path_list_first

            #now plot the path
            # fig = plt.figure(); ax = fig.add_subplot(121, projection='3d'); ax2 = fig.add_subplot(122, projection='3d')
            # c = 'r'; m = 'o'; xs = path_list_first['x']; ys = path_list_first['y']; zs = path_list_first['yaw']; ax.scatter(xs, ys, zs, c=c, marker=m)
            # c = 'r'; m = 'o'; xs = path_list_second['x']; ys = path_list_second['y']; zs = path_list_second['yaw']; ax2.scatter(xs, ys, zs, c=c, marker=m)
            # ax.set_xlabel('X axis'); ax.set_ylabel('Y axis'); ax.set_zlabel('theta axis'); ax2.set_xlabel('X axis'); ax2.set_ylabel('Y axis'); ax2.set_zlabel('theta axis'); 
            # plt.show()
            return path_list_first
        else:
            print "no path"
            return []

    def test_iris(self,rrt_path,obstacles, bool_keep_run_list):
        free_space_area = 0.0
        for idx in range(len(rrt_path)-1):
            if bool_keep_run_list[idx] == True:
                pi = rrt_path[idx]; pj= rrt_path[idx+1]
                p_mid = np.array(pj)- np.array(pi)
                region = irispy.inflate_region(obstacles, p_mid, bounds=self.bounds, return_debug_data=False)
                #Find the convex hull
                iris_hull = ConvexHull(region.polyhedron.generatorPoints()) #Hull object, requires points (2D) in list [array(x1,y1),..,array(xn,yn)]
                iris_hull.vertices #This is the order for the verticies around
                iris_hull.volume #MOST IMPORTANT, this is the volume inside of the polyhedron (area is exterior surface area of poly)
                free_space_area += iris_hull.volume
        return free_space_area

    def find_verticies_given_polytope_planes(self,planes_list, point_list):
        def vertex_from_lines(l1,l2):
            l1x,l1y,l1z = l1.item(0),l1.item(1),l1.item(2)
            l2x,l2y,l2z = l2.item(0),l2.item(1),l2.item(2)
            cprod = np.matrix([[0,-l1z,l1y],[l1z,0,-l1x],[-l1y,l1x,0]])* np.matrix([[l2x],[l2y],[l2z]])
            if cprod.item(2) == 0:
                cprod_return = np.array([cprod.item(0),cprod.item(1)])
            else:
                cprod_return = np.array([cprod.item(0)/cprod.item(2),cprod.item(1)/cprod.item(2)])
            return cprod_return


        all_possible_vertex = []
        for idx in range(len(planes_list)-1):
            plane_main = planes_list[idx]
            for jdx in range(idx+1, len(planes_list)):
                plane_test = planes_list[jdx]
                vert_return = vertex_from_lines(np.array(plane_main), np.array(plane_test))
                all_possible_vertex.append(vert_return)
        #after obtaining all possible planes, now discriminate
        all_verts_mat = np.vstack([np.matrix(all_possible_vertex).T ,np.ones([1,len(all_possible_vertex)])])

        vert_keep = []
        for idx in range(len(all_possible_vertex)):

            plane_interior = np.sign(np.matrix(planes_list)*all_verts_mat[:,idx])
            if 1 not in plane_interior:
                vert_keep.append(np.array([all_verts_mat[0,idx],all_verts_mat[1,idx]]))


        verticies_chord = vert_keep

        bool_keep_run = False
        if len(vert_keep) > 2:
            bool_keep_run = True

        return verticies_chord, bool_keep_run






    def test_my_cvx_alg(self,rrt_path, obstacles):
        obstacles_list = []
        for idx in range(len(obstacles)):
            for jdx in range(len(obstacles[idx])):
                obstacles_list.append([obstacles[idx][0][jdx], obstacles[idx][1][jdx]])

        world_planes = [[1,0,-1],[-1,0,-1],[0,1,-1],[0,-1,-1]]
        Aset, active_obs_pts = self.cvx_planner.Corridor_generator(rrt_path,obstacles_list, world_planes, True) #returns list of lists of planes for each chord
        #Since the order of obstacle points match the Aset ordering, cvx_hull can be used to find the order of these points in arrangement and that order c
        chord_polytope_volumes = 0.0
        bool_keep_run_list = []
        for idx in range(len(Aset)):
            #For each chord obtain the verticies: 
            planes = Aset[idx]#[4:] #remove world planes
            vert_points = active_obs_pts[idx]
            #Find verticies of polytope by cross products of consecutive lines
            chord_polytope_vert, bool_keep_run = self.find_verticies_given_polytope_planes(planes, vert_points)
            bool_keep_run_list.append(bool_keep_run)
            #Using the verticies for this chord, now find the resultant volume of the polytope
            cvx_chord_hull = ConvexHull(chord_polytope_vert) #hull object, requires points (2D) in list [array(x1,y1),..,array(xn,yn)]
            # cvx_chord_hull.volume #MOST IMPORTANT, this is the area of the polyhedron
            chord_polytope_volumes += cvx_chord_hull.volume

        return chord_polytope_volumes, bool_keep_run_list



    def run_statistical_comparison(self,num_trials):
        poly_volume_ration_list = []
        for idx in range(num_trials):
            #Define the world and obstacles
            self.obstacles = self.Define_world(self.num_obs)
            #Define the start and goal (ensuring they are not inside obstacles)
            bool_fail = True
            while bool_fail:
                #sample pts until it works
                self.start, self.goal = self.define_start_and_goal() 
                #Define a path using RRT-Connect between start and goal
                try:
                    self.rrt_path = self.ompl_path_plan(self.start, self.goal)
                    #For each chord of path, define free space and store it in a running list (the volume from each method)
                    #My method from path
                    my_cvx_free_space,bool_keep_run_list = self.test_my_cvx_alg(self.rrt_path, self.obstacles)
                    #IRIS from path
                    iris_free_space = self.test_iris(self.rrt_path, self.obstacles,bool_keep_run_list)
                    bool_fail = False
                except:
                    bool_fail = True
                    # pass
            #calculate free space ratio: 
            try:
                ratio_mine_over_iris = float(my_cvx_free_space)/float(iris_free_space)
                poly_volume_ration_list.append(ratio_mine_over_iris)
            except:
                pass
        #calculate statistics
        poly_volume_ration_avg = np.mean(np.array(poly_volume_ration_list))
        poly_volume_ration_stddev = np.std(np.array(poly_volume_ration_list))


        data = np.array(poly_volume_ration_list)
        median = np.median(data)
        upper_quartile = np.percentile(data, 75)
        lower_quartile = np.percentile(data, 25)

        iqr = upper_quartile - lower_quartile
        upper_whisker = data[data<=upper_quartile+1.5*iqr].max()
        lower_whisker = data[data>=lower_quartile-1.5*iqr].min()


        #given the number of trials, run n times, and collect statisitics
        return poly_volume_ration_avg,poly_volume_ration_stddev


def main():
    #main script to compare methods
    num_obs = 10
    num_trials = 100
    cls_obj = Comparison_btw_iris_cvx_hull(True, num_obs)
    #Run statistical trials
    pvol_avg,pvol_stdev = cls_obj.run_statistical_comparison(num_trials)
    print "average ratio: ", pvol_avg, "\n stdev: ", pvol_stdev
    #Plot output (box plot, clutter percentage (x axis), and avg,stdev of ratio my_volume/iris_volume in y axis)
    #debug.iters shows number of iterations required, also present this information


if __name__ == '__main__':
    main()
    # test_random_obstacles_2d(True)