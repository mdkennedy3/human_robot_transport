/*
* Copyright (c) <2016>, <Dinesh Thakur>
* All rights reserved.
*
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*   1. Redistributions of source code must retain the above copyright notice,
*   this list of conditions and the following disclaimer.
*
*   2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
*   3. Neither the name of the University of Pennsylvania nor the names of its
*   contributors may be used to endorse or promote products derived from this
*   software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "ros/ros.h"
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Point.h>
#include <nav_msgs/GetMap.h>
#include <tf/transform_datatypes.h>
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>
#include <cmath>
#include <costmap_2d/footprint.h>

#include <se2path_planner/Mink3d.h>
#include <se2path_planner/environment_nav_se2.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <sbpl/headers.h>

#include <exception>

#include <Eigen/Core>

#define M_PI 3.14159265358979323846

class Se2PathPlanner
{
public:
  Se2PathPlanner();
  ~Se2PathPlanner();
  void publishStates();
private:
  void goalCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);

  ros::NodeHandle nh_;

  ros::Publisher path_pub_, grid_pub_, pc_pub_;
  ros::Subscriber goal_sub_;
  ros::ServiceClient map_client_, mink3d_client_;
  ros::Publisher vis_pub_;


  float cellsize_m_;

};

Se2PathPlanner::Se2PathPlanner()
{
  nh_ = ros::NodeHandle("~");

  path_pub_ = nh_.advertise<nav_msgs::Path>("path", 1);
  //grid_pub_ = nh_.advertise<grid_map_msgs::GridMap>("grid_map", 1, true);
  //pc_pub_ = nh_.advertise<sensor_msgs::PointCloud>("pc", 1);

  double inscribed_radius;
  nh_.param("/inscribed_radius", inscribed_radius, 0.1);
  cellsize_m_ = inscribed_radius/2.0; //m

  map_client_ = nh_.serviceClient<nav_msgs::GetMap>("/static_map");
  mink3d_client_ = nh_.serviceClient<se2path_planner::Mink3d>("/mink3d");

  goal_sub_ = nh_.subscribe<geometry_msgs::PoseStamped>("/move_base_simple/goal", 10, boost::bind(&Se2PathPlanner::goalCallback, this, _1));
  vis_pub_ = nh_.advertise<visualization_msgs::MarkerArray>( "obs_marker", 0 );

}

Se2PathPlanner::~Se2PathPlanner()
{

}

void Se2PathPlanner::goalCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{

  nav_msgs::GetMap map_srv;
  if (map_client_.call(map_srv))
  {
    ROS_INFO("Received a map");
  }
  else
  {
    ROS_ERROR("Failed to call service static_map");
    return;
  }

  std::string frame_id = map_srv.response.map.header.frame_id;
  if(frame_id == "")
    frame_id = "/map";
  ROS_INFO("Map frame %s", frame_id.c_str());

  //Get the goal from msg and check frame
  if(frame_id != msg->header.frame_id)
  {
    ROS_ERROR("Goal frame_id %s does not match frame_id %s of map", msg->header.frame_id.c_str(), frame_id.c_str());
    return;
  }
  sbpl_xy_theta_pt_t goal_m(msg->pose.position.x, msg->pose.position.y, tf::getYaw(msg->pose.orientation));

  se2path_planner::Mink3d mink_srv;

  unsigned char obsthresh = 1;

  //Set the Robot hull.
  std::string full_param_name;
  std::vector<geometry_msgs::Point> points, new_points;

  points = costmap_2d::makeFootprintFromParams(nh_);

  //Add points along the hull
  for(int i = 0; i < points.size() -1; i++)
  {
    Eigen::Matrix<double, 1, 2> current_point, next_point;

    current_point << points.at(i).x, points.at(i).y;
    next_point << points.at(i+1).x, points.at(i+1).y;

    Eigen::Matrix<double, 1, 2> l_vect, l_vect_normed;
    l_vect = next_point - current_point;
    l_vect_normed = l_vect.normalized();

    double length = l_vect.norm();
    double delta = 0.05;
    int n_pts = length/delta;

    for(int j = 0; j < n_pts; j++ )
    {
      Eigen::Matrix<double, 1, 2> new_point;
      new_point = current_point + delta*l_vect_normed;
      geometry_msgs::Point pt;
      pt.x = new_point(0,0);
      pt.y = new_point(0,1);
      new_points.push_back(pt);
      current_point = new_point;
    }
  }

  ROS_INFO("Hull points");
  for(int i = 0; i < new_points.size() -1; i++)
  {
    ROS_INFO("x: %g y: %g", new_points.at(i).x, new_points.at(i).y);
  }

  mink_srv.request.robot_hull_pts = new_points;

  geometry_msgs::Pose2D world_min, world_max;
  world_min.theta = -M_PI;
  world_max.theta = M_PI;
  int NumThetaDirs = 8;

  mink_srv.request.map = map_srv.response.map;
  mink_srv.request.world_min = world_min;
  mink_srv.request.world_max = world_max;
  mink_srv.request.num_theta_dirs = NumThetaDirs;

  ROS_INFO("Calling Mink3d service");
  if (mink3d_client_.call(mink_srv))
  {
    ROS_INFO("service call sucess");
  }
  else
  {
    ROS_ERROR("Failed to call service Path_finder");
    return;
  }

  float width_m = map_srv.response.map.info.width*map_srv.response.map.info.resolution;
  float height_m = map_srv.response.map.info.height*map_srv.response.map.info.resolution;

  //Origin is the lower left pizel in the map.
  sbpl_2Dpt_t origin_m(map_srv.response.map.info.origin.position.x, map_srv.response.map.info.origin.position.y);

  //Get coordinates for start
  sbpl_xy_theta_pt_t start_m;
  boost::shared_ptr<geometry_msgs::PoseStamped const> start_ptr;
  start_ptr = ros::topic::waitForMessage<geometry_msgs::PoseStamped>("/robot_pose", ros::Duration(1));

  if(start_ptr == NULL){
    ROS_ERROR("Robot pose not published on /robot_pose");
    return;
  }

  geometry_msgs::PoseStamped ps = *start_ptr;
  if(frame_id != ps.header.frame_id)
  {
    ROS_ERROR("Start frame_id %s does not match frame_id %s of map", ps.header.frame_id.c_str(), frame_id.c_str());
    return;
  }

  start_m.x = ps.pose.position.x;
  start_m.y = ps.pose.position.y;
  start_m.theta = tf::getYaw(ps.pose.orientation);
  if(std::isnan(start_m.theta) | std::isinf(start_m.theta))
  {
    ROS_ERROR("Start orientation not set properly nan/inf, setting to zero");
    start_m.theta = 0;
  }

  sensor_msgs::PointCloud pc = mink_srv.response.mink_sum;
  if(frame_id != pc.header.frame_id)
  {
    ROS_ERROR("Pointcloud frame_id %s does not match frame_id %s of map", pc.header.frame_id.c_str(), frame_id.c_str());
    return;
  }

  int bRet = 0;
  double allocated_time_secs = 100.0; // in seconds
  double initialEpsilon = 3.0;
  MDPConfig MDPCfg;
  bool bsearchuntilfirstsolution = false;
  bool bforwardsearch = true;

  EnvironmentNAV_SE2 env;

  try
  {
    // Initialize Environment (should be called before initializing anything else)
    if(!env.InitializeEnv(width_m, height_m, start_m, goal_m, obsthresh, cellsize_m_, origin_m, NumThetaDirs, pc))
    {
      throw SBPL_Exception("ERROR: InitializeEnv failed");
    }

    // Initialize MDP Info
    if (!env.InitializeMDPCfg(&MDPCfg))
    {
      throw SBPL_Exception("ERROR: InitializeMDPCfg failed");
    }

    // plan a path
    std::vector<int> solution_stateIDs_V;

    SBPLPlanner* planner = NULL;
    printf("Initializing ARAPlanner...\n");
    planner = new ARAPlanner(&env, bforwardsearch);

    // set search mode
    planner->set_search_mode(bsearchuntilfirstsolution);

    printf("done Initializing ARAPlanner...\n");

    if (planner->set_start(MDPCfg.startstateid) == 0) {
        throw SBPL_Exception("ERROR: failed to set start state");
    }

    if (planner->set_goal(MDPCfg.goalstateid) == 0) {
        throw SBPL_Exception("ERROR: failed to set goal state");
    }

    planner->set_initialsolution_eps(initialEpsilon);

    printf("start planning...\n");
    bRet = planner->replan(allocated_time_secs, &solution_stateIDs_V);
    printf("done planning\n");
    std::cout << "size of solution=" << solution_stateIDs_V.size() << std::endl;

    env.PrintTimeStat(stdout);
    //env.PrintHashHist();

    //print a path
    if (bRet)
    {

      visualization_msgs::Marker marker, marker1;
      marker.header.frame_id = "/map";
      marker.header.stamp = ros::Time::now();
      marker.ns = "bel_points";
      marker.action = visualization_msgs::Marker::ADD;
      marker.pose.orientation.w = 1.0;
      marker.id = 5;
      marker.type = visualization_msgs::Marker::POINTS;
      marker.scale.x = cellsize_m_; //width of the points
      marker.scale.y = cellsize_m_; //height of the points
      marker.scale.y = 0.1;
      //marker.color.r = 1.0;
      //marker.color.a = 1.0;

      marker1 = marker;
      marker1.id = 2;

      marker.color.a = 0.5; marker.color.r = 0.0; marker.color.g = 0.0; marker.color.b = 1.0; marker.scale.y = 0.1; marker.id = 0;
      marker1.color.a = 0.5; marker1.color.r = 1.0; marker1.color.g = 0.0; marker1.color.b = 0.0; marker1.scale.y = 0.1; marker1.id = 1;

      for(int th = 0; th < env.EnvNAV_SE2Cfg.NumThetaDirs; th ++)
      {
        for(int w = 0; w < env.EnvNAV_SE2Cfg.EnvWidth_c; w++)
        {
          for(int h = 0; h < env.EnvNAV_SE2Cfg.EnvHeight_c; h++)
          {
            int val = env.EnvNAV_SE2Cfg.se2_grid[th][w][h];
            geometry_msgs::Point p;

            p.x = DISCXY2CONT(w,cellsize_m_) + origin_m.x;
            p.y = DISCXY2CONT(h,cellsize_m_) + origin_m.y;
            p.z = DiscTheta2Cont(th, NumThetaDirs);
            if(val > obsthresh)
              marker1.points.push_back(p);
            else
              marker.points.push_back(p);
          }
        }
      }
      visualization_msgs::MarkerArray marker_arr;

      marker_arr.markers.push_back(marker);
      marker_arr.markers.push_back(marker1);
      vis_pub_.publish(marker_arr);

      ros::Duration d(2);

      marker.lifetime = d;

      //print the solution
      printf("Solution is found\n");
      nav_msgs::Path xythetaPath;
      xythetaPath.header.frame_id = "/map";

      double x_diff, y_diff, theta_diff;
      double x_diff_prev, y_diff_prev, theta_diff_prev;
      int x_prev, y_prev, theta_prev;
      bool keep_test = true;
      for( unsigned int i = 0; i< solution_stateIDs_V.size(); i++)
      {
        int x_c, y_c, theta_c;
        env.GetCoordFromState(solution_stateIDs_V.at(i), x_c, y_c, theta_c);

        //ROS_INFO("valid %d",env.IsValidCell(x_c, y_c, theta_c));

        if (i == 0)
        {
          x_prev = x_c;
          y_prev = y_c;
          theta_prev = theta_c;
        }

        if( i == 1)
        {
          x_diff = x_c - x_prev;
          y_diff = y_c - y_prev;
          theta_diff = theta_c - theta_prev;
          x_prev = x_c;
          y_prev = y_c;
          theta_prev = theta_c;
        }

        if( i > 1)
        {
          x_diff_prev = x_diff;
          y_diff_prev = y_diff;
          theta_diff_prev = theta_diff;

          x_diff = x_c - x_prev;
          y_diff = y_c - y_prev;
          theta_diff = theta_c - theta_prev;

          std::vector<double> v1 = {double(x_diff), double(y_diff), double(theta_diff)};
          std::vector<double> v2 = {double(x_diff_prev), double(y_diff_prev), double(theta_diff_prev)};

          double dot_prod_12 = 0.0;
          double v1_norm_sqr = 0.0;
          double v2_norm_sqr = 0.0;
          for(int idx = 0; idx < 3; idx++)
          {
            dot_prod_12 += v1[idx]*v2[idx];
            v1_norm_sqr += std::pow(v1[idx],2);
            v2_norm_sqr += std::pow(v2[idx],2);
            // ROS_INFO("idx and v1,v2 elem: %d %.3f %.3f\n", idx, v1[idx], v2[idx]);
          }
          double v1_norm = std::sqrt( v1_norm_sqr);
          double v2_norm = std::sqrt( v2_norm_sqr);
          double th12 = std::acos(dot_prod_12/(v1_norm*v2_norm));
          // ROS_INFO("th, v1n, v2n: %.3f %.3f %.3f\n", th12, v1_norm, v2_norm);
          if ( th12 <= 0.261799 )
          {
            // ROS_INFO("point discarded");
            // this is 15def: 0.261799
            //This is 5degrees: 0.08726646259
            keep_test = false;
          }
          else
          {
            // ROS_INFO("point kept");
            keep_test = true;
            geometry_msgs::PoseStamped ps;
            ps.pose.position.x = DISCXY2CONT(x_prev,cellsize_m_) + origin_m.x;
            ps.pose.position.y = DISCXY2CONT(y_prev,cellsize_m_) + origin_m.y;
            ps.pose.position.z = 0.0;
            ps.pose.orientation = tf::createQuaternionMsgFromYaw(DiscTheta2Cont(theta_prev, NumThetaDirs));

            ROS_INFO("ID: %d %d %d %d %.3f %.3f %.3f\n", solution_stateIDs_V.at(i), x_prev, y_prev, theta_prev, ps.pose.position.x, ps.pose.position.y, tf::getYaw(ps.pose.orientation));

            xythetaPath.poses.push_back(ps);
          }

          // double x_diff_prev = x_diff;
          // double y_diff_prev = y_diff;
          // double theta_diff_prev = theta_diff;
          // double x_diff = x_c - x_prev;
          // double y_diff = y_c - y_prev;
          // double theta_diff = theta_c - theta_prev;
          x_prev = x_c;
          y_prev = y_c;
          theta_prev = theta_c;
          //Perform test to see if the current point (xc,yc,thc) should be kept or discarded
        } // End of if for i == 0, i == 1, i > 1

        if ( (i == (solution_stateIDs_V.size() - 1) ) | (i == 0) | (i == 1))
        {
          geometry_msgs::PoseStamped ps;
          ps.pose.position.x = DISCXY2CONT(x_c,cellsize_m_) + origin_m.x;
          ps.pose.position.y = DISCXY2CONT(y_c,cellsize_m_) + origin_m.y;
          ps.pose.position.z = 0.0;
          ps.pose.orientation = tf::createQuaternionMsgFromYaw(DiscTheta2Cont(theta_c, NumThetaDirs));

          ROS_INFO("ID: %d %d %d %d %.3f %.3f %.3f\n", solution_stateIDs_V.at(i), x_c, y_c, theta_c, ps.pose.position.x, ps.pose.position.y, tf::getYaw(ps.pose.orientation));

          xythetaPath.poses.push_back(ps);
        }
      }
      path_pub_.publish(xythetaPath);
    }
    else
    {
        ROS_ERROR("Solution does not exist");
    }

    delete planner;
  }
  catch(SBPL_Exception& e)
  {
    ROS_ERROR("SBPL exception");
    std::cout << e.what() << std::endl;
  }
  catch(std::exception& e)
  {
    ROS_ERROR("SBPL exception");
    std::cout << e.what() << std::endl;
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "se2path_planner");

  Se2PathPlanner se2_planner;
  ros::Rate loop_rate(2);

  while (ros::ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  }
}