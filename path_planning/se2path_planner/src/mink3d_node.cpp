/*
* Copyright (c) <2016>, <Dinesh Thakur>
* All rights reserved.
*
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*   1. Redistributions of source code must retain the above copyright notice,
*   this list of conditions and the following disclaimer.
*
*   2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
*   3. Neither the name of the University of Pennsylvania nor the names of its
*   contributors may be used to endorse or promote products derived from this
*   software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "ros/ros.h"
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Point32.h>
#include <nav_msgs/GetMap.h>
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_msgs/GridMap.h>

#include <se2path_planner/Mink3d.h>

#define M_PI 3.14159265358979323846

class Mink3D
{
public:
  Mink3D();
  bool serviceCallback(se2path_planner::Mink3d::Request &req, se2path_planner::Mink3d::Response  &res);
private:

  ros::NodeHandle nh_;
  ros::ServiceServer mink3d_service_;
  ros::Publisher pc_pub_;

};

Mink3D::Mink3D()
{
  nh_ = ros::NodeHandle("~");
  pc_pub_ = nh_.advertise<sensor_msgs::PointCloud>("/pc", 1);
  mink3d_service_ = nh_.advertiseService<se2path_planner::Mink3d::Request, se2path_planner::Mink3d::Response>("/mink3d", boost::bind(&Mink3D::serviceCallback, this, _1, _2));
}

bool Mink3D::serviceCallback(se2path_planner::Mink3d::Request &req, se2path_planner::Mink3d::Response  &res)
{

  ros::Time begin = ros::Time::now();

  res.mink_sum.header.frame_id =  req.map.header.frame_id;

  //Precompute sin and cosine of thetas
  float theta = req.world_min.theta;
  float theta_step = (req.world_max.theta - req.world_min.theta)/(float)(req.num_theta_dirs);
  std::vector<double> cos_theta, sin_theta, theta_vec;
  while(theta < req.world_max.theta)
  {
    cos_theta.push_back(cos(theta));
    sin_theta.push_back(sin(theta));
    theta_vec.push_back(theta);
    theta += theta_step;
  }
  int n_theta = theta_vec.size();

  grid_map::GridMap map({"layer"});
  grid_map::GridMapRosConverter::fromOccupancyGrid(req.map, "layer", map);
  //map.setPosition(grid_map::Position(req.map.info.origin.position.x, req.map.info.origin.position.y));
  ROS_INFO("Origin %g %g, Res: %g, size: %g, %g m (%d, %d cells)", map.getPosition().x(), map.getPosition().y(), map.getResolution(),
    map.getLength().x(), map.getLength().y(), map.getSize()(0), map.getSize()(1));

  for (grid_map::GridMapIterator iterator(map); !iterator.isPastEnd(); ++iterator)
  {
    //const int i = iterator.getLinearIndex();
    if(map.at("layer", *iterator) != 0)
    {

      grid_map::Position ps;
      map.getPosition(*iterator, ps);
      geometry_msgs::Point32 pt;
      pt.x = ps.x();
      pt.y = ps.y();

      for(int th = 0; th < n_theta; th++)
      {
        Eigen::Matrix2d T;
        T << cos_theta.at(th), -sin_theta.at(th), sin_theta.at(th), cos_theta.at(th);

        //ROS_INFO("theta %g, step %g, x %g, y %g", theta, theta_step, pt.x, pt.y);
        //Add the current map point as obsctacle in point cloud
        res.mink_sum.points.push_back(pt);

        //Add -robot_hull points to the point cloud
        for(int i=0; i < req.robot_hull_pts.size(); i++)
        {
          Eigen::Vector2d hull_pt;
          hull_pt << - req.robot_hull_pts.at(i).x, - req.robot_hull_pts.at(i).y; //negate the robot hull

          Eigen::Vector2d new_pt;
          new_pt = T*hull_pt;

          Eigen::Vector2d obs_pt = new_pt + ps;
          pt.x =  obs_pt.x();
          pt.y =  obs_pt.y();
          pt.z = theta_vec.at(th);
          res.mink_sum.points.push_back(pt);
        }
      }
    }
  }

  ros::Duration dur = ros::Time::now() - begin;
  ROS_INFO("Took %g sec", dur.toSec());

  //Publish the pointcloud
  pc_pub_.publish(res.mink_sum);

return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "mink3d_server");

  Mink3D mink3d;
  ros::spin();
}