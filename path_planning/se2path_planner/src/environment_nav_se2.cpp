/*
 * Copyright (c) 2016, Maxim Likhachev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Carnegie Mellon University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *  Author: Dinesh Thakur, Modified for SE2 A* Environment
 */

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <sstream>

#include <se2path_planner/environment_nav_se2.h>
#include <sbpl/planners/planner.h>
#include <sbpl/utils/mdp.h>
#include <sbpl/utils/mdpconfig.h>

#include "ros/ros.h"
using namespace std;

#if TIME_DEBUG
static clock_t time3_addallout = 0;
static clock_t time_gethash = 0;
static clock_t time_createhash = 0;
static clock_t time_getsuccs = 0;
#endif

#define XYTHETA2INDEX(X,Y,THETA) (THETA + X*EnvNAV_SE2Cfg.NumThetaDirs + \
                                  Y*EnvNAV_SE2Cfg.EnvWidth_c*EnvNAV_SE2Cfg.NumThetaDirs)

//-------------------constructor--------------------------------------------
EnvironmentNAV_SE2::EnvironmentNAV_SE2()
{
    EnvNAV_SE2Cfg.obsthresh = ENVNAV_SE2_DEFAULTOBSTHRESH;

    EnvNAV_SE2Cfg.numofdirs = 8;
    EnvNAV_SE2.bInitialized = false;

    EnvNAV_SE2.Coord2StateIDHashTable = NULL;
    Coord2StateIDHashTable_lookup = NULL;
}

//-------------------problem specific and local functions---------------------

static unsigned int inthash(unsigned int key)
{
    key += (key << 12);
    key ^= (key >> 22);
    key += (key << 4);
    key ^= (key >> 9);
    key += (key << 10);
    key ^= (key >> 2);
    key += (key << 7);
    key ^= (key >> 12);
    return key;
}

//examples of hash functions: map state coordinates onto a hash value
//#define GETHASHBIN(X, Y) (Y*WIDTH_Y+X)
//here we have state coord: <X1, X2, X3, X4>
unsigned int EnvironmentNAV_SE2::GETHASHBIN(unsigned int X1, unsigned int X2, unsigned int Theta)
{
    return inthash(inthash(X1) + (inthash(X2) << 1) + (inthash(Theta) << 2)) & (EnvNAV_SE2.HashTableSize - 1);
}

void EnvironmentNAV_SE2::PrintHashTableHist()
{
    int s0 = 0, s1 = 0, s50 = 0, s100 = 0, s200 = 0, s300 = 0, slarge = 0;

    for (int j = 0; j < EnvNAV_SE2.HashTableSize; j++) {
        if ((int)EnvNAV_SE2.Coord2StateIDHashTable[j].size() == 0)
            s0++;
        else if ((int)EnvNAV_SE2.Coord2StateIDHashTable[j].size() < 50)
            s1++;
        else if ((int)EnvNAV_SE2.Coord2StateIDHashTable[j].size() < 100)
            s50++;
        else if ((int)EnvNAV_SE2.Coord2StateIDHashTable[j].size() < 200)
            s100++;
        else if ((int)EnvNAV_SE2.Coord2StateIDHashTable[j].size() < 300)
            s200++;
        else if ((int)EnvNAV_SE2.Coord2StateIDHashTable[j].size() < 400)
            s300++;
        else
            slarge++;
    }
    SBPL_PRINTF("hash table histogram: 0:%d, <50:%d, <100:%d, <200:%d, <300:%d, <400:%d >400:%d\n", s0, s1, s50, s100,
                s200, s300, slarge);
}

void EnvironmentNAV_SE2::PrintHashHist()
{
    PrintHashTableHist();
}

void EnvironmentNAV_SE2::SetConfiguration(float width_m, float height_m, sbpl_xy_theta_pt_t start_m, sbpl_xy_theta_pt_t goal_m, sensor_msgs::PointCloud pc)
{

    EnvNAV_SE2Cfg.EnvWidth_c = CONTXY2DISC(width_m, EnvNAV_SE2Cfg.cellsize_m);
    EnvNAV_SE2Cfg.EnvHeight_c = CONTXY2DISC(height_m, EnvNAV_SE2Cfg.cellsize_m);

    ROS_INFO("width_c: %d, height_c: %d, width: %g, height: %g, cellsize_m: %g", EnvNAV_SE2Cfg.EnvWidth_c, EnvNAV_SE2Cfg.EnvHeight_c, width_m, height_m, EnvNAV_SE2Cfg.cellsize_m);

    ROS_INFO("Setting up SE2 voxel");
    EnvNAV_SE2Cfg.se2_grid.resize(boost::extents[EnvNAV_SE2Cfg.NumThetaDirs][EnvNAV_SE2Cfg.EnvWidth_c][EnvNAV_SE2Cfg.EnvHeight_c]);

    ROS_INFO("SE2 voxel adding obstacles %d", (int)pc.points.size());
    //Transpose all the obstacle points
    for(int i = 0; i < pc.points.size(); i++)
    {
        geometry_msgs::Point32 pt = pc.points[i];
        se2_index x_c = CONTXY2DISC(pt.x - EnvNAV_SE2Cfg.origin_m.x, EnvNAV_SE2Cfg.cellsize_m);
        se2_index y_c = CONTXY2DISC(pt.y - EnvNAV_SE2Cfg.origin_m.y, EnvNAV_SE2Cfg.cellsize_m);
        se2_index theta = ContTheta2Disc(normalizeAngle(pt.z), EnvNAV_SE2Cfg.NumThetaDirs);

        //ROS_ERROR("%d %d %d %d %d %g %g %g\n", i, pc.points.size(), x_c, y_c, theta, pt.x, pt.y, normalizeAngle(pt.z));

        if (!IsWithinMapCell(x_c, y_c)) {
            continue;
            //ROS_ERROR("x %d, y %d", x_c, y_c);
            //throw SBPL_Exception("illegal obstacle coordinates");
        }

        //ROS_ERROR("valid");

        EnvNAV_SE2Cfg.se2_grid[theta][x_c][y_c] = 100*EnvNAV_SE2Cfg.obsthresh;
    }

    ROS_INFO("Done setting up SE2 voxel");

    int startx_c = CONTXY2DISC(start_m.x - EnvNAV_SE2Cfg.origin_m.x, EnvNAV_SE2Cfg.cellsize_m);
    int starty_c = CONTXY2DISC(start_m.y - EnvNAV_SE2Cfg.origin_m.y, EnvNAV_SE2Cfg.cellsize_m);
    int starttheta_c = ContTheta2Disc(normalizeAngle(start_m.theta), EnvNAV_SE2Cfg.NumThetaDirs);

    int goalx_c = CONTXY2DISC(goal_m.x - EnvNAV_SE2Cfg.origin_m.x, EnvNAV_SE2Cfg.cellsize_m);
    int goaly_c = CONTXY2DISC(goal_m.y - EnvNAV_SE2Cfg.origin_m.y, EnvNAV_SE2Cfg.cellsize_m);
    int goaltheta_c = ContTheta2Disc(normalizeAngle(goal_m.theta), EnvNAV_SE2Cfg.NumThetaDirs);

    ROS_INFO("Setting Start x: %g, y: %g, theta: %g, x_c: %d, y_c: %d, theta_c: %d", start_m.x, start_m.y, start_m.theta, startx_c, starty_c, starttheta_c);

    if (!IsWithinMapCell(startx_c, starty_c)) {
        throw SBPL_Exception("illegal start coordinates");
    }

    if (!IsValidCell(startx_c, starty_c, starttheta_c)) {
        throw SBPL_Exception("WARNING: start cell is invalid\n");
    }

    ROS_INFO("Setting Goal x: %g, y: %g, theta: %g, x_c: %d, y_c: %d, theta_c: %d", goal_m.x, goal_m.y, goal_m.theta, goalx_c, goaly_c, goaltheta_c);
    if (!IsWithinMapCell(goalx_c, goaly_c)) {
        throw SBPL_Exception("illegal goal coordinates");
    }

    if (!IsValidCell(goalx_c, goaly_c, goaltheta_c)) {
        throw SBPL_Exception("WARNING: goal cell is invalid\n");
    }

    EnvNAV_SE2Cfg.StartX_c = startx_c;
    EnvNAV_SE2Cfg.StartY_c = starty_c;
    EnvNAV_SE2Cfg.StartTheta = starttheta_c;

    EnvNAV_SE2Cfg.EndX_c = goalx_c;
    EnvNAV_SE2Cfg.EndY_c = goaly_c;
    EnvNAV_SE2Cfg.EndTheta = goaltheta_c;

    ROS_INFO("Done setting config");
}

bool EnvironmentNAV_SE2::InitializeEnv(const char* sEnvFile)
{
    return true;
}

void EnvironmentNAV_SE2::InitializeEnvConfig()
{
    //aditional to configuration file initialization of EnvNAV_SE2Cfg if necessary

    /*
     //actions
     EnvNAV_SE2Cfg.dXY[0][0] = -1;
     EnvNAV_SE2Cfg.dXY[0][1] = -1;
     EnvNAV_SE2Cfg.dXY[1][0] = -1;
     EnvNAV_SE2Cfg.dXY[1][1] = 0;
     EnvNAV_SE2Cfg.dXY[2][0] = -1;
     EnvNAV_SE2Cfg.dXY[2][1] = 1;
     EnvNAV_SE2Cfg.dXY[3][0] = 0;
     EnvNAV_SE2Cfg.dXY[3][1] = -1;
     EnvNAV_SE2Cfg.dXY[4][0] = 0;
     EnvNAV_SE2Cfg.dXY[4][1] = 1;
     EnvNAV_SE2Cfg.dXY[5][0] = 1;
     EnvNAV_SE2Cfg.dXY[5][1] = -1;
     EnvNAV_SE2Cfg.dXY[6][0] = 1;
     EnvNAV_SE2Cfg.dXY[6][1] = 0;
     EnvNAV_SE2Cfg.dXY[7][0] = 1;
     EnvNAV_SE2Cfg.dXY[7][1] = 1;
     */
    Computedxy();
}

void EnvironmentNAV_SE2::Computedxy()
{
    //initialize some constants for 2D search
    EnvNAV_SE2Cfg.dx_[0] = 1;
    EnvNAV_SE2Cfg.dy_[0] = 1;
    EnvNAV_SE2Cfg.dxintersects_[0][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[0][0] = 1;
    EnvNAV_SE2Cfg.dxintersects_[0][1] = 1;
    EnvNAV_SE2Cfg.dyintersects_[0][1] = 0;

    EnvNAV_SE2Cfg.dx_[1] = 1;
    EnvNAV_SE2Cfg.dy_[1] = 0;
    EnvNAV_SE2Cfg.dxintersects_[1][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[1][0] = 0;
    EnvNAV_SE2Cfg.dxintersects_[1][1] = 0;
    EnvNAV_SE2Cfg.dyintersects_[1][1] = 0;

    EnvNAV_SE2Cfg.dx_[2] = 1;
    EnvNAV_SE2Cfg.dy_[2] = -1;
    EnvNAV_SE2Cfg.dxintersects_[2][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[2][0] = -1;
    EnvNAV_SE2Cfg.dxintersects_[2][1] = 1;
    EnvNAV_SE2Cfg.dyintersects_[2][1] = 0;

    EnvNAV_SE2Cfg.dx_[3] = 0;
    EnvNAV_SE2Cfg.dy_[3] = 1;
    EnvNAV_SE2Cfg.dxintersects_[3][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[3][0] = 0;
    EnvNAV_SE2Cfg.dxintersects_[3][1] = 0;
    EnvNAV_SE2Cfg.dyintersects_[3][1] = 0;

    EnvNAV_SE2Cfg.dx_[4] = 0;
    EnvNAV_SE2Cfg.dy_[4] = -1;
    EnvNAV_SE2Cfg.dxintersects_[4][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[4][0] = 0;
    EnvNAV_SE2Cfg.dxintersects_[4][1] = 0;
    EnvNAV_SE2Cfg.dyintersects_[4][1] = 0;

    EnvNAV_SE2Cfg.dx_[5] = -1;
    EnvNAV_SE2Cfg.dy_[5] = 1;
    EnvNAV_SE2Cfg.dxintersects_[5][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[5][0] = 1;
    EnvNAV_SE2Cfg.dxintersects_[5][1] = -1;
    EnvNAV_SE2Cfg.dyintersects_[5][1] = 0;

    EnvNAV_SE2Cfg.dx_[6] = -1;
    EnvNAV_SE2Cfg.dy_[6] = 0;
    EnvNAV_SE2Cfg.dxintersects_[6][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[6][0] = 0;
    EnvNAV_SE2Cfg.dxintersects_[6][1] = 0;
    EnvNAV_SE2Cfg.dyintersects_[6][1] = 0;

    EnvNAV_SE2Cfg.dx_[7] = -1;
    EnvNAV_SE2Cfg.dy_[7] = -1;
    EnvNAV_SE2Cfg.dxintersects_[7][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[7][0] = -1;
    EnvNAV_SE2Cfg.dxintersects_[7][1] = -1;
    EnvNAV_SE2Cfg.dyintersects_[7][1] = 0;

    //Note: these actions have to be starting at 8 and through 15, since they
    //get multiplied correspondingly in Dijkstra's search based on index
    EnvNAV_SE2Cfg.dx_[8] = 2;
    EnvNAV_SE2Cfg.dy_[8] = 1;
    EnvNAV_SE2Cfg.dxintersects_[8][0] = 1;
    EnvNAV_SE2Cfg.dyintersects_[8][0] = 0;
    EnvNAV_SE2Cfg.dxintersects_[8][1] = 1;
    EnvNAV_SE2Cfg.dyintersects_[8][1] = 1;

    EnvNAV_SE2Cfg.dx_[9] = 1;
    EnvNAV_SE2Cfg.dy_[9] = 2;
    EnvNAV_SE2Cfg.dxintersects_[9][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[9][0] = 1;
    EnvNAV_SE2Cfg.dxintersects_[9][1] = 1;
    EnvNAV_SE2Cfg.dyintersects_[9][1] = 1;

    EnvNAV_SE2Cfg.dx_[10] = -1;
    EnvNAV_SE2Cfg.dy_[10] = 2;
    EnvNAV_SE2Cfg.dxintersects_[10][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[10][0] = 1;
    EnvNAV_SE2Cfg.dxintersects_[10][1] = -1;
    EnvNAV_SE2Cfg.dyintersects_[10][1] = 1;

    EnvNAV_SE2Cfg.dx_[11] = -2;
    EnvNAV_SE2Cfg.dy_[11] = 1;
    EnvNAV_SE2Cfg.dxintersects_[11][0] = -1;
    EnvNAV_SE2Cfg.dyintersects_[11][0] = 0;
    EnvNAV_SE2Cfg.dxintersects_[11][1] = -1;
    EnvNAV_SE2Cfg.dyintersects_[11][1] = 1;

    EnvNAV_SE2Cfg.dx_[12] = -2;
    EnvNAV_SE2Cfg.dy_[12] = -1;
    EnvNAV_SE2Cfg.dxintersects_[12][0] = -1;
    EnvNAV_SE2Cfg.dyintersects_[12][0] = 0;
    EnvNAV_SE2Cfg.dxintersects_[12][1] = -1;
    EnvNAV_SE2Cfg.dyintersects_[12][1] = -1;

    EnvNAV_SE2Cfg.dx_[13] = -1;
    EnvNAV_SE2Cfg.dy_[13] = -2;
    EnvNAV_SE2Cfg.dxintersects_[13][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[13][0] = -1;
    EnvNAV_SE2Cfg.dxintersects_[13][1] = -1;
    EnvNAV_SE2Cfg.dyintersects_[13][1] = -1;

    EnvNAV_SE2Cfg.dx_[14] = 1;
    EnvNAV_SE2Cfg.dy_[14] = -2;
    EnvNAV_SE2Cfg.dxintersects_[14][0] = 0;
    EnvNAV_SE2Cfg.dyintersects_[14][0] = -1;
    EnvNAV_SE2Cfg.dxintersects_[14][1] = 1;
    EnvNAV_SE2Cfg.dyintersects_[14][1] = -1;

    EnvNAV_SE2Cfg.dx_[15] = 2;
    EnvNAV_SE2Cfg.dy_[15] = -1;
    EnvNAV_SE2Cfg.dxintersects_[15][0] = 1;
    EnvNAV_SE2Cfg.dyintersects_[15][0] = 0;
    EnvNAV_SE2Cfg.dxintersects_[15][1] = 1;
    EnvNAV_SE2Cfg.dyintersects_[15][1] = -1;

    //compute distances
    for (int dind = 0; dind < ENVNAV_SE2_MAXDIRS; dind++) {
        if (EnvNAV_SE2Cfg.dx_[dind] != 0 && EnvNAV_SE2Cfg.dy_[dind] != 0) {
            if (dind <= 7)
                //the cost of a diagonal move in millimeters
                EnvNAV_SE2Cfg.dxy_distance_mm_[dind] = (int)ceil(ENVNAV_SE2_COSTMULT * 1.414);
            else
                //the cost of a move to 1,2 or 2,1 or so on in millimeters
                EnvNAV_SE2Cfg.dxy_distance_mm_[dind] = (int)ceil(ENVNAV_SE2_COSTMULT * 2.236);
        }
        else
            EnvNAV_SE2Cfg.dxy_distance_mm_[dind] = ENVNAV_SE2_COSTMULT; //the cost of a horizontal move in millimeters
    }
}

EnvNAV_SE2HashEntry_t* EnvironmentNAV_SE2::GetHashEntry_lookup(
    int X, int Y, int Theta)
{
    if (X < 0 || X >= EnvNAV_SE2Cfg.EnvWidth_c ||
        Y < 0 || Y >= EnvNAV_SE2Cfg.EnvHeight_c ||
        Theta < 0 || Theta >= EnvNAV_SE2Cfg.NumThetaDirs)
    {
        return NULL;
    }
    int index = XYTHETA2INDEX(X,Y,Theta);
    return Coord2StateIDHashTable_lookup[index];
}

EnvNAV_SE2HashEntry_t*
EnvironmentNAV_SE2::GetHashEntry_hash(int X, int Y, int Theta)
{
#if TIME_DEBUG
    clock_t currenttime = clock();
#endif

    int binid = GETHASHBIN(X, Y, Theta);

#if DEBUG
    if ((int)EnvNAV_SE2.Coord2StateIDHashTable[binid].size() > 5) {
        SBPL_FPRINTF(fDeb, "WARNING: Hash table has a bin %d (X=%d Y=%d) of size %d\n", binid, X, Y, (int)EnvNAV_SE2.Coord2StateIDHashTable[binid].size());

        PrintHashTableHist(fDeb);
    }
#endif

    // iterate over the states in the bin and select the perfect match
    std::vector<EnvNAV_SE2HashEntry_t*>* binV = &EnvNAV_SE2.Coord2StateIDHashTable[binid];
    for (int ind = 0; ind < (int)binV->size(); ind++) {
        EnvNAV_SE2HashEntry_t* hashentry = binV->at(ind);
        if (hashentry->X == X && hashentry->Y == Y && hashentry->Theta == Theta) {
#if TIME_DEBUG
            time_gethash += clock()-currenttime;
#endif
            return hashentry;
        }
    }

#if TIME_DEBUG
    time_gethash += clock()-currenttime;
#endif

    return NULL;
}

EnvNAV_SE2HashEntry_t*
EnvironmentNAV_SE2::CreateNewHashEntry_lookup(int X, int Y, int Theta)
{
    int i;

#if TIME_DEBUG
    clock_t currenttime = clock();
#endif

    EnvNAV_SE2HashEntry_t* HashEntry = new EnvNAV_SE2HashEntry_t;

    HashEntry->X = X;
    HashEntry->Y = Y;
    HashEntry->Theta = Theta;
    HashEntry->iteration = 0;

    HashEntry->stateID = EnvNAV_SE2.StateID2CoordTable.size();

    // insert into the tables
    EnvNAV_SE2.StateID2CoordTable.push_back(HashEntry);

    int index = XYTHETA2INDEX(X,Y,Theta);

#if DEBUG
    if (Coord2StateIDHashTable_lookup[index] != NULL) {
        throw SBPL_Exception("ERROR: creating hash entry for non-NULL hashentry");
    }
#endif

    Coord2StateIDHashTable_lookup[index] = HashEntry;

    // insert into and initialize the mappings
    int* entry = new int[NUMOFINDICES_STATEID2IND];
    StateID2IndexMapping.push_back(entry);
    for (i = 0; i < NUMOFINDICES_STATEID2IND; i++) {
        StateID2IndexMapping[HashEntry->stateID][i] = -1;
    }

    if (HashEntry->stateID != (int)StateID2IndexMapping.size() - 1) {
        throw SBPL_Exception("ERROR in Env... function: last state has incorrect stateID");
    }

#if TIME_DEBUG
    time_createhash += clock()-currenttime;
#endif

    return HashEntry;
}

EnvNAV_SE2HashEntry_t*
EnvironmentNAV_SE2::CreateNewHashEntry_hash(int X, int Y, int Theta)
{
    int i;

#if TIME_DEBUG
    clock_t currenttime = clock();
#endif

    EnvNAV_SE2HashEntry_t* HashEntry = new EnvNAV_SE2HashEntry_t;

    HashEntry->X = X;
    HashEntry->Y = Y;
    HashEntry->Theta = Theta;
    HashEntry->iteration = 0;

    HashEntry->stateID = EnvNAV_SE2.StateID2CoordTable.size();

    // insert into the tables
    EnvNAV_SE2.StateID2CoordTable.push_back(HashEntry);

    // get the hash table bin
    i = GETHASHBIN(HashEntry->X, HashEntry->Y, HashEntry->Theta);

    // insert the entry into the bin
    EnvNAV_SE2.Coord2StateIDHashTable[i].push_back(HashEntry);

    // insert into and initialize the mappings
    int* entry = new int[NUMOFINDICES_STATEID2IND];
    StateID2IndexMapping.push_back(entry);
    for (i = 0; i < NUMOFINDICES_STATEID2IND; i++) {
        StateID2IndexMapping[HashEntry->stateID][i] = -1;
    }

    if (HashEntry->stateID != (int)StateID2IndexMapping.size() - 1) {
        throw SBPL_Exception("ERROR in Env... function: last state has incorrect stateID");
    }

#if TIME_DEBUG
    time_createhash += clock() - currenttime;
#endif

    return HashEntry;
}

bool EnvironmentNAV_SE2::IsValidCell(int X, int Y)
{
    throw SBPL_Exception("EnvNAV_SE2: function: IsValidCell is undefined");
    return (X >= 0 && X < EnvNAV_SE2Cfg.EnvWidth_c && Y >= 0 && Y < EnvNAV_SE2Cfg.EnvHeight_c &&
            EnvNAV_SE2Cfg.Grid2D[X][Y] < EnvNAV_SE2Cfg.obsthresh);
}

bool EnvironmentNAV_SE2::IsValidCell(int X, int Y, int theta)
{
    return (X >= 0 && X < EnvNAV_SE2Cfg.EnvWidth_c && Y >= 0 && Y < EnvNAV_SE2Cfg.EnvHeight_c &&
            EnvNAV_SE2Cfg.se2_grid[theta][X][Y] < EnvNAV_SE2Cfg.obsthresh);
}

bool EnvironmentNAV_SE2::IsWithinMapCell(int X, int Y)
{
    return (X >= 0 && X < EnvNAV_SE2Cfg.EnvWidth_c && Y >= 0 && Y < EnvNAV_SE2Cfg.EnvHeight_c);
}

EnvironmentNAV_SE2::~EnvironmentNAV_SE2()
{
    for (unsigned int i = 0; i < EnvNAV_SE2.StateID2CoordTable.size(); ++i) {
        if (EnvNAV_SE2.StateID2CoordTable[i] != NULL){
         delete EnvNAV_SE2.StateID2CoordTable[i];
         EnvNAV_SE2.StateID2CoordTable[i] = NULL;
        }
    }
    EnvNAV_SE2.StateID2CoordTable.clear();

    if (EnvNAV_SE2Cfg.Grid2D != NULL) {
        for (int x = 0; x < EnvNAV_SE2Cfg.EnvWidth_c; x++) {
            if (EnvNAV_SE2Cfg.Grid2D[x] != NULL) delete[] EnvNAV_SE2Cfg.Grid2D[x];
        }
        delete[] EnvNAV_SE2Cfg.Grid2D;
    }

    // delete hashtable
    if (EnvNAV_SE2.Coord2StateIDHashTable != NULL) {
        delete[] EnvNAV_SE2.Coord2StateIDHashTable;
        EnvNAV_SE2.Coord2StateIDHashTable = NULL;
    }

    if (Coord2StateIDHashTable_lookup != NULL) {
        delete[] Coord2StateIDHashTable_lookup;
        Coord2StateIDHashTable_lookup = NULL;
    }
}

void EnvironmentNAV_SE2::InitializeEnvironment()
{
    EnvNAV_SE2HashEntry_t* HashEntry;

    int maxsize = EnvNAV_SE2Cfg.EnvWidth_c * EnvNAV_SE2Cfg.EnvHeight_c * EnvNAV_SE2Cfg.NumThetaDirs;

    if (maxsize <= SBPL_XYTHETALAT_MAXSTATESFORLOOKUP) {
        SBPL_PRINTF("environment stores states in lookup table\n");

        Coord2StateIDHashTable_lookup = new EnvNAV_SE2HashEntry_t*[maxsize];
        for (int i = 0; i < maxsize; i++) {
            Coord2StateIDHashTable_lookup[i] = NULL;
        }
        GetHashEntry = &EnvironmentNAV_SE2::GetHashEntry_lookup;
        CreateNewHashEntry = &EnvironmentNAV_SE2::CreateNewHashEntry_lookup;

        // not using hash table
        EnvNAV_SE2.HashTableSize = 0;
        EnvNAV_SE2.Coord2StateIDHashTable = NULL;
    }
    else {
        SBPL_PRINTF("environment stores states in hashtable\n");

        // initialize the map from Coord to StateID
        EnvNAV_SE2.HashTableSize = 4 * 1024 * 1024; // should be power of two
        EnvNAV_SE2.Coord2StateIDHashTable = new std::vector<EnvNAV_SE2HashEntry_t*>[EnvNAV_SE2.HashTableSize];
        GetHashEntry = &EnvironmentNAV_SE2::GetHashEntry_hash;
        CreateNewHashEntry = &EnvironmentNAV_SE2::CreateNewHashEntry_hash;

        // not using hash
        Coord2StateIDHashTable_lookup = NULL;
    }

    // initialize the map from StateID to Coord
    EnvNAV_SE2.StateID2CoordTable.clear();

    // create start state
    if (NULL == (HashEntry = (this->*GetHashEntry)(
            EnvNAV_SE2Cfg.StartX_c,
            EnvNAV_SE2Cfg.StartY_c,
            EnvNAV_SE2Cfg.StartTheta)))
    {
        // have to create a new entry
        HashEntry = (this->*CreateNewHashEntry)(
                EnvNAV_SE2Cfg.StartX_c,
                EnvNAV_SE2Cfg.StartY_c,
                EnvNAV_SE2Cfg.StartTheta);
    }
    EnvNAV_SE2.startstateid = HashEntry->stateID;

    // create goal state
    if ((HashEntry = (this->*GetHashEntry)(
            EnvNAV_SE2Cfg.EndX_c,
            EnvNAV_SE2Cfg.EndY_c,
            EnvNAV_SE2Cfg.EndTheta)) == NULL)
    {
        // have to create a new entry
        HashEntry = (this->*CreateNewHashEntry)(
                EnvNAV_SE2Cfg.EndX_c,
                EnvNAV_SE2Cfg.EndY_c,
                EnvNAV_SE2Cfg.EndTheta);
    }
    EnvNAV_SE2.goalstateid = HashEntry->stateID;

    // initialized
    EnvNAV_SE2.bInitialized = true;

}

static int EuclideanDistance(int X1, int Y1, int X2, int Y2)
{
    int sqdist = ((X1 - X2) * (X1 - X2) + (Y1 - Y2) * (Y1 - Y2));
    double dist = sqrt((double)sqdist);
    return (int)(ENVNAV_SE2_COSTMULT * dist);
}

//------------------------------------------------------------------------------

//------------------------------Heuristic computation--------------------------

void EnvironmentNAV_SE2::ComputeHeuristicValues()
{
    //whatever necessary pre-computation of heuristic values is done here
    SBPL_PRINTF("Precomputing heuristics...\n");

    SBPL_PRINTF("done\n");
}

//-----------interface with outside functions-----------------------------------

bool EnvironmentNAV_SE2::InitializeEnv(float width_m, float height_m, sbpl_xy_theta_pt_t start_m, sbpl_xy_theta_pt_t goal_m,
        unsigned char obsthresh, double cellsize_m, sbpl_2Dpt_t origin_m, int se2_numofdirs, sensor_msgs::PointCloud pc)
{

    EnvNAV_SE2Cfg.obsthresh = obsthresh;
    EnvNAV_SE2Cfg.cellsize_m = cellsize_m;
    EnvNAV_SE2Cfg.NumThetaDirs = se2_numofdirs;
    EnvNAV_SE2Cfg.origin_m = origin_m;

    SetConfiguration(width_m, height_m, start_m, goal_m, pc);

    InitGeneral();

    return true;
}

bool EnvironmentNAV_SE2::InitGeneral()
{
    //Initialize other parameters of the environment
    InitializeEnvConfig();

    //initialize Environment
    InitializeEnvironment();

    //pre-compute heuristics
    ComputeHeuristicValues();

    return true;
}

bool EnvironmentNAV_SE2::InitializeMDPCfg(MDPConfig *MDPCfg)
{
    //initialize MDPCfg with the start and goal ids
    MDPCfg->goalstateid = EnvNAV_SE2.goalstateid;
    MDPCfg->startstateid = EnvNAV_SE2.startstateid;

    return true;
}

int EnvironmentNAV_SE2::GetFromToHeuristic(int FromStateID, int ToStateID)
{
#if USE_HEUR==0
    return 0;
#endif

#if DEBUG
    if (FromStateID >= (int)EnvNAV_SE2.StateID2CoordTable.size() ||
        ToStateID >= (int)EnvNAV_SE2.StateID2CoordTable.size())
    {
        throw SBPL_Exception("EnvNAV_SE2... function: stateID illegal");
    }
#endif

    //get X, Y for the state
    EnvNAV_SE2HashEntry_t* FromHashEntry = EnvNAV_SE2.StateID2CoordTable[FromStateID];
    EnvNAV_SE2HashEntry_t* ToHashEntry = EnvNAV_SE2.StateID2CoordTable[ToStateID];

    return EuclideanDistance(FromHashEntry->X, FromHashEntry->Y, ToHashEntry->X, ToHashEntry->Y);
}

int EnvironmentNAV_SE2::GetGoalHeuristic(int stateID)
{
#if USE_HEUR==0
    return 0;
#endif

#if DEBUG
    if (stateID >= (int)EnvNAV_SE2.StateID2CoordTable.size()) {
        throw SBPL_Exception("EnvNAV_SE2... function: stateID illegal");
    }
#endif

    //define this function if it used in the planner (heuristic forward search would use it)
    return GetFromToHeuristic(stateID, EnvNAV_SE2.goalstateid);
}

int EnvironmentNAV_SE2::GetStartHeuristic(int stateID)
{
#if USE_HEUR==0
    return 0;
#endif

#if DEBUG
    if (stateID >= (int)EnvNAV_SE2.StateID2CoordTable.size()) {
        throw SBPL_Exception("EnvNAV_SE2... function: stateID illegal");
    }
#endif

    //define this function if it used in the planner (heuristic backward search would use it)
    return GetFromToHeuristic(EnvNAV_SE2.startstateid, stateID);
}

void EnvironmentNAV_SE2::SetAllActionsandAllOutcomes(CMDPSTATE* state)
{
    throw SBPL_Exception("EnvNAV_SE2: function:  SetAllActionsandAllOutcomes is undefined");

    int cost;

#if DEBUG
    if (state->StateID >= (int)EnvNAV_SE2.StateID2CoordTable.size()) {
        SBPL_ERROR("ERROR in Env... function: stateID illegal\n");
        throw SBPL_Exception("Env: function: stateID illegal");
    }

    if ((int)state->Actions.size() != 0) {
        throw SBPL_Exception("Env setAllActionsandAllOutcomes: actions already exist for the state");
    }
#endif

    //goal state should be absorbing
    if (state->StateID == EnvNAV_SE2.goalstateid) return;

    //get X, Y for the state
    EnvNAV_SE2HashEntry_t* HashEntry = EnvNAV_SE2.StateID2CoordTable[state->StateID];

    //iterate through actions
    bool bTestBounds = false;
    if (HashEntry->X <= 1 || HashEntry->X >= EnvNAV_SE2Cfg.EnvWidth_c - 2 || HashEntry->Y <= 1 ||
        HashEntry->Y >= EnvNAV_SE2Cfg.EnvHeight_c - 2)
    {
        bTestBounds = true;
    }
    for (int aind = 0; aind < EnvNAV_SE2Cfg.numofdirs; aind++) {
        int newX = HashEntry->X + EnvNAV_SE2Cfg.dx_[aind];
        int newY = HashEntry->Y + EnvNAV_SE2Cfg.dy_[aind];

        //skip the invalid cells
        if (bTestBounds) {
            if (!IsValidCell(newX, newY)) continue;
        }

        //compute cost multiplier
        int costmult = EnvNAV_SE2Cfg.Grid2D[newX][newY];
        //for diagonal move, take max over adjacent cells
        if (newX != HashEntry->X && newY != HashEntry->Y && aind <= 7) {
            //check two more cells through which the action goes
            costmult = __max(costmult, EnvNAV_SE2Cfg.Grid2D[HashEntry->X][newY]);
            costmult = __max(costmult, EnvNAV_SE2Cfg.Grid2D[newX][HashEntry->Y]);
        }
        else if (aind > 7) {
            //check two more cells through which the action goes
            costmult = __max(costmult,
                             EnvNAV_SE2Cfg.Grid2D[HashEntry->X + EnvNAV_SE2Cfg.dxintersects_[aind][0]][HashEntry->Y
                                 + EnvNAV_SE2Cfg.dyintersects_[aind][0]]);
            costmult = __max(costmult,
                             EnvNAV_SE2Cfg.Grid2D[HashEntry->X + EnvNAV_SE2Cfg.dxintersects_[aind][1]][HashEntry->Y
                                 + EnvNAV_SE2Cfg.dyintersects_[aind][1]]);
        }

        //check that it is valid
        if (costmult >= EnvNAV_SE2Cfg.obsthresh) continue;

        //otherwise compute the actual cost
        cost = (costmult + 1) * EnvNAV_SE2Cfg.dxy_distance_mm_[aind];

        //add the action
        CMDPACTION* action = state->AddAction(aind);

#if TIME_DEBUG
        clock_t currenttime = clock();
#endif

        int newTheta = 0; //#TODO-------------------------

        EnvNAV_SE2HashEntry_t* OutHashEntry;
        if ((OutHashEntry = (this->*GetHashEntry)(newX, newY, newTheta)) == NULL) {
            //have to create a new entry
            OutHashEntry = (this->*CreateNewHashEntry)(newX, newY, newTheta);
        }
        action->AddOutcome(OutHashEntry->stateID, cost, 1.0);

#if TIME_DEBUG
        time3_addallout += clock()-currenttime;
#endif
    }
}

void EnvironmentNAV_SE2::SetAllPreds(CMDPSTATE* state)
{
    //implement this if the planner needs access to predecessors
    throw SBPL_Exception("EnvNAV_SE2: function: SetAllPreds is undefined");
}

void EnvironmentNAV_SE2::GetSuccs(int SourceStateID, vector<int>* SuccIDV, vector<int>* CostV)
{
    int aind;

#if TIME_DEBUG
    clock_t currenttime = clock();
#endif

    //clear the successor array
    SuccIDV->clear();
    CostV->clear();
    SuccIDV->reserve(EnvNAV_SE2Cfg.numofdirs);
    CostV->reserve(EnvNAV_SE2Cfg.numofdirs);

    //goal state should be absorbing
    if (SourceStateID == EnvNAV_SE2.goalstateid) return;

    //get X, Y for the state
    EnvNAV_SE2HashEntry_t* HashEntry = EnvNAV_SE2.StateID2CoordTable[SourceStateID];

    //iterate through actions
    bool bTestBounds = false;
    if (HashEntry->X <= 1 || HashEntry->X >= EnvNAV_SE2Cfg.EnvWidth_c - 2 || HashEntry->Y <= 1 ||
        HashEntry->Y >= EnvNAV_SE2Cfg.EnvHeight_c - 2)
    {
        bTestBounds = true;
    }
    for (aind = 0; aind < EnvNAV_SE2Cfg.numofdirs; aind++) {
        int newX = HashEntry->X + EnvNAV_SE2Cfg.dx_[aind];
        int newY = HashEntry->Y + EnvNAV_SE2Cfg.dy_[aind];

        //Get all successor theta
        int newTheta = HashEntry->Theta;

        int newTheta1 = HashEntry->Theta + 1;
        //Wrap the angles
        if(newTheta1 >= EnvNAV_SE2Cfg.NumThetaDirs)
            newTheta1 = 0;

        int newTheta2 = HashEntry->Theta - 1;
        //wrap the angle
        if(newTheta2 < 0)
            newTheta2 = EnvNAV_SE2Cfg.NumThetaDirs - 1;

        std::vector<int> thetas; thetas.push_back(newTheta); thetas.push_back(newTheta1); thetas.push_back(newTheta2);

        for(int i = 0; i < thetas.size(); i++)
        {
            newTheta = thetas[i];

            //skip the invalid cells
            if (bTestBounds) {
                if (!IsValidCell(newX, newY, newTheta)) continue;
            }

            int costmult = EnvNAV_SE2Cfg.se2_grid[newTheta][newX][newY];

            //for diagonal move, take max over adjacent cells
            if (newX != HashEntry->X && newY != HashEntry->Y && aind <= 7) {
                costmult = __max(costmult, EnvNAV_SE2Cfg.se2_grid[newTheta][HashEntry->X][newY]);
                costmult = __max(costmult, EnvNAV_SE2Cfg.se2_grid[newTheta][newX][HashEntry->Y]);
            }
            else if (aind > 7) {
                //check two more cells through which the action goes
                costmult = __max(costmult,
                                 EnvNAV_SE2Cfg.se2_grid[newTheta][HashEntry->X + EnvNAV_SE2Cfg.dxintersects_[aind][0]][HashEntry->Y
                                     + EnvNAV_SE2Cfg.dyintersects_[aind][0]]);
                costmult = __max(costmult,
                                 EnvNAV_SE2Cfg.se2_grid[newTheta][HashEntry->X + EnvNAV_SE2Cfg.dxintersects_[aind][1]][HashEntry->Y
                                     + EnvNAV_SE2Cfg.dyintersects_[aind][1]]);
            }

            //check that it is valid
            if (costmult >= EnvNAV_SE2Cfg.obsthresh) continue;

            //otherwise compute the actual cost
            //if same theta
            int cost = (costmult + 1) * EnvNAV_SE2Cfg.dxy_distance_mm_[aind];

            if(i!=0)
                cost = (costmult + 2) * EnvNAV_SE2Cfg.dxy_distance_mm_[aind];

            EnvNAV_SE2HashEntry_t* OutHashEntry;
            if ((OutHashEntry = (this->*GetHashEntry)(newX, newY, newTheta)) == NULL) {
                //have to create a new entry
                OutHashEntry = (this->*CreateNewHashEntry)(newX, newY, newTheta);
            }

            SuccIDV->push_back(OutHashEntry->stateID);
            CostV->push_back(cost);
        }

    }

    //turn in place actions
    int newX = HashEntry->X;
    int newY = HashEntry->Y;

    //Get all successor theta
    int newTheta = HashEntry->Theta;

    int newTheta1 = HashEntry->Theta + 1;
    //Wrap the angles
    if(newTheta1 >= EnvNAV_SE2Cfg.NumThetaDirs)
        newTheta1 = 0;

    int newTheta2 = HashEntry->Theta - 1;
    //wrap the angle
    if(newTheta2 < 0)
        newTheta2 = EnvNAV_SE2Cfg.NumThetaDirs - 1;

    std::vector<int> thetas; thetas.push_back(newTheta1); thetas.push_back(newTheta2);

    for(int i = 0; i < thetas.size(); i++)
    {
        newTheta = thetas[i];

        //skip the invalid cells
        if (bTestBounds) {
            if (!IsValidCell(newX, newY, newTheta)) continue;
        }

        int costmult = EnvNAV_SE2Cfg.se2_grid[newTheta][newX][newY];

        //for diagonal move, take max over adjacent cells
        if (newX != HashEntry->X && newY != HashEntry->Y && aind <= 7) {
            costmult = __max(costmult, EnvNAV_SE2Cfg.se2_grid[newTheta][HashEntry->X][newY]);
            costmult = __max(costmult, EnvNAV_SE2Cfg.se2_grid[newTheta][newX][HashEntry->Y]);
        }
        else if (aind > 7) {
            //check two more cells through which the action goes
            costmult = __max(costmult,
                             EnvNAV_SE2Cfg.se2_grid[newTheta][HashEntry->X + EnvNAV_SE2Cfg.dxintersects_[aind][0]][HashEntry->Y
                                 + EnvNAV_SE2Cfg.dyintersects_[aind][0]]);
            costmult = __max(costmult,
                             EnvNAV_SE2Cfg.se2_grid[newTheta][HashEntry->X + EnvNAV_SE2Cfg.dxintersects_[aind][1]][HashEntry->Y
                                 + EnvNAV_SE2Cfg.dyintersects_[aind][1]]);
        }

        //check that it is valid
        if (costmult >= EnvNAV_SE2Cfg.obsthresh) continue;

        //otherwise compute the actual cost
        //if same theta
        int cost = (costmult + 2) * EnvNAV_SE2Cfg.dxy_distance_mm_[aind];

        EnvNAV_SE2HashEntry_t* OutHashEntry;
        if ((OutHashEntry = (this->*GetHashEntry)(newX, newY, newTheta)) == NULL) {
            //have to create a new entry
            OutHashEntry = (this->*CreateNewHashEntry)(newX, newY, newTheta);
        }

        SuccIDV->push_back(OutHashEntry->stateID);
        CostV->push_back(cost);
    }

#if TIME_DEBUG
    time_getsuccs += clock()-currenttime;
#endif
}

void EnvironmentNAV_SE2::GetPreds(int TargetStateID, vector<int>* PredIDV, vector<int>* CostV)
{

    throw SBPL_Exception("EnvNAV_SE2: function: GetPreds is undefined");

    int aind;

#if TIME_DEBUG
    clock_t currenttime = clock();
#endif

    //clear the successor array
    PredIDV->clear();
    CostV->clear();
    PredIDV->reserve(EnvNAV_SE2Cfg.numofdirs);
    CostV->reserve(EnvNAV_SE2Cfg.numofdirs);

    //get X, Y for the state
    EnvNAV_SE2HashEntry_t* HashEntry = EnvNAV_SE2.StateID2CoordTable[TargetStateID];

    //no predecessors if obstacle
    if (EnvNAV_SE2Cfg.Grid2D[HashEntry->X][HashEntry->Y] >= EnvNAV_SE2Cfg.obsthresh) return;

    int targetcostmult = EnvNAV_SE2Cfg.Grid2D[HashEntry->X][HashEntry->Y];

    //iterate through actions
    bool bTestBounds = false;
    if (HashEntry->X <= 1 || HashEntry->X >= EnvNAV_SE2Cfg.EnvWidth_c - 2 || HashEntry->Y <= 1 ||
        HashEntry->Y >= EnvNAV_SE2Cfg.EnvHeight_c - 2)
    {
        bTestBounds = true;
    }
    for (aind = 0; aind < EnvNAV_SE2Cfg.numofdirs; aind++) {
        // the actions are undirected, so we can use the same array of actions as in getsuccs case
        int predX = HashEntry->X + EnvNAV_SE2Cfg.dx_[aind];
        int predY = HashEntry->Y + EnvNAV_SE2Cfg.dy_[aind];

        // skip the invalid cells
        if (bTestBounds) {
            if (!IsValidCell(predX, predY)) continue;
        }

        // compute costmult
        int costmult = targetcostmult;
        // for diagonal move, take max over adjacent cells
        if (predX != HashEntry->X && predY != HashEntry->Y && aind <= 7) {
            costmult = __max(costmult, EnvNAV_SE2Cfg.Grid2D[HashEntry->X][predY]);
            costmult = __max(costmult, EnvNAV_SE2Cfg.Grid2D[predX][HashEntry->Y]);
        }
        else if (aind > 7) {
            // check two more cells through which the action goes since actions
            // are undirected, we don't have to figure out what are the
            // intersecting cells on moving from <predX,predY> to <X,Y>.  it is
            // the same cells as moving from <X,Y> to <predX,predY>, which is
            // action aind
            costmult = __max(costmult,
                             EnvNAV_SE2Cfg.Grid2D[HashEntry->X + EnvNAV_SE2Cfg.dxintersects_[aind][0]][HashEntry->Y
                                 + EnvNAV_SE2Cfg.dyintersects_[aind][0]]);
            costmult = __max(costmult,
                             EnvNAV_SE2Cfg.Grid2D[HashEntry->X + EnvNAV_SE2Cfg.dxintersects_[aind][1]][HashEntry->Y
                                 + EnvNAV_SE2Cfg.dyintersects_[aind][1]]);
        }

        // check that it is valid
        if (costmult >= EnvNAV_SE2Cfg.obsthresh) continue;

        // otherwise compute the actual cost (once again we use the fact that
        // actions are undirected to determine the cost)
        int cost = (costmult + 1) * EnvNAV_SE2Cfg.dxy_distance_mm_[aind];

        //#TODO
        int predTheta = 0;

        EnvNAV_SE2HashEntry_t* OutHashEntry;
        if ((OutHashEntry = (this->*GetHashEntry)(predX, predY, predTheta)) == NULL) {
            // have to create a new entry
            OutHashEntry = (this->*CreateNewHashEntry)(predX, predY, predTheta);
        }

        PredIDV->push_back(OutHashEntry->stateID);
        CostV->push_back(cost);
    }

#if TIME_DEBUG
    time_getsuccs += clock()-currenttime;
#endif
}

int EnvironmentNAV_SE2::SizeofCreatedEnv()
{
    return (int)EnvNAV_SE2.StateID2CoordTable.size();
}

void EnvironmentNAV_SE2::PrintState(int stateID, bool bVerbose, FILE* fOut /*=NULL*/)
{

    /*
#if DEBUG
    if (stateID >= (int)EnvNAV_SE2.StateID2CoordTable.size()) {
        throw SBPL_Exception("ERROR in EnvNAV_SE2... function: stateID illegal (2)");
    }
#endif

    if (fOut == NULL) fOut = stdout;

    EnvNAV_SE2HashEntry_t* HashEntry = EnvNAV_SE2.StateID2CoordTable[stateID];

    if (stateID == EnvNAV_SE2.goalstateid && bVerbose) {
        SBPL_FPRINTF(fOut, "the state is a goal state\n");
    }

    if (bVerbose)
        SBPL_FPRINTF(fOut, "X=%d Y=%d\n", HashEntry->X, HashEntry->Y);
    else
        SBPL_FPRINTF(fOut, "%d %d\n", HashEntry->X, HashEntry->Y);

    */
}

void EnvironmentNAV_SE2::GetCoordFromState(int stateID, int& x, int& y, int& theta) const
{
    EnvNAV_SE2HashEntry_t* HashEntry = EnvNAV_SE2.StateID2CoordTable[stateID];
    x = HashEntry->X;
    y = HashEntry->Y;
    theta = HashEntry->Theta;
}

int EnvironmentNAV_SE2::GetStateFromCoord(int x, int y, int theta)
{

    EnvNAV_SE2HashEntry_t* OutHashEntry;
    if ((OutHashEntry = (this->*GetHashEntry)(x, y, theta)) == NULL) {
        //have to create a new entry
        OutHashEntry = (this->*CreateNewHashEntry)(x, y, theta);
    }
    return OutHashEntry->stateID;
}

const EnvNAV_SE2Config_t* EnvironmentNAV_SE2::GetEnvNavConfig()
{
    return &EnvNAV_SE2Cfg;
}

//returns the stateid if success, and -1 otherwise
int EnvironmentNAV_SE2::SetGoal(int x, int y, int theta)
{
    if (!IsWithinMapCell(x, y)) {
        SBPL_ERROR("ERROR: trying to set a goal cell %d %d that is outside of map\n", x, y);
        return -1;
    }

    if (!IsValidCell(x, y, theta)) {
        SBPL_PRINTF("WARNING: goal cell is invalid\n");
    }

    EnvNAV_SE2HashEntry_t* OutHashEntry;
    if ((OutHashEntry = (this->*GetHashEntry)(x, y, theta)) == NULL) {
        //have to create a new entry
        OutHashEntry = (this->*CreateNewHashEntry)(x, y, theta);
    }
    EnvNAV_SE2.goalstateid = OutHashEntry->stateID;
    EnvNAV_SE2Cfg.EndX_c = x;
    EnvNAV_SE2Cfg.EndY_c = y;
    EnvNAV_SE2Cfg.EndTheta = theta;

    return EnvNAV_SE2.goalstateid;
}

void EnvironmentNAV_SE2::SetGoalTolerance(double tol_x, double tol_y, double tol_theta)
{
    // not used yet
}

//returns the stateid if success, and -1 otherwise
int EnvironmentNAV_SE2::SetStart(int x, int y, int theta)
{
    if (!IsWithinMapCell(x, y)) {
        SBPL_ERROR("ERROR: trying to set a start cell %d %d that is outside of map\n", x, y);
        return -1;
    }

    if (!IsValidCell(x, y, theta)) {
        SBPL_PRINTF("WARNING: start cell is invalid\n");
    }

    EnvNAV_SE2HashEntry_t* OutHashEntry;
    if ((OutHashEntry = (this->*GetHashEntry)(x, y, theta)) == NULL) {
        //have to create a new entry
        OutHashEntry = (this->*CreateNewHashEntry)(x, y, theta);
    }
    EnvNAV_SE2.startstateid = OutHashEntry->stateID;
    EnvNAV_SE2Cfg.StartX_c = x;
    EnvNAV_SE2Cfg.StartY_c = y;
    EnvNAV_SE2Cfg.StartTheta = theta;

    return EnvNAV_SE2.startstateid;
}

bool EnvironmentNAV_SE2::UpdateCost(int x, int y, unsigned char newcost)
{
    throw SBPL_Exception("EnvNAV_SE2: function: UpdateCost is undefined");
    EnvNAV_SE2Cfg.Grid2D[x][y] = newcost;

    return true;
}

void EnvironmentNAV_SE2::PrintEnv_Config(FILE* fOut)
{
    //implement this if the planner needs to print out EnvNAV_SE2. configuration

    throw SBPL_Exception("ERROR in EnvNAV_SE2... function: PrintEnv_Config is undefined");
}

void EnvironmentNAV_SE2::PrintTimeStat(FILE* fOut)
{
#if TIME_DEBUG
    SBPL_FPRINTF(fOut,
                "time3_addallout = %f secs, time_gethash = %f secs, time_createhash = %f secs, time_getsuccs = %f\n",
                time3_addallout/(double)CLOCKS_PER_SEC, time_gethash/(double)CLOCKS_PER_SEC,
                time_createhash/(double)CLOCKS_PER_SEC, time_getsuccs/(double)CLOCKS_PER_SEC);
#endif
}

void EnvironmentNAV_SE2::GetPredsofChangedEdges(vector<nav2dcell_t> const * changedcellsV,
                                              vector<int> *preds_of_changededgesIDV)
{
    nav2dcell_t cell;
    int aind;

    //#TODO
    int theta = 0;

    for (int i = 0; i < (int)changedcellsV->size(); i++) {
        cell = changedcellsV->at(i);
        preds_of_changededgesIDV->push_back(GetStateFromCoord(cell.x, cell.y, theta));

        for (aind = 0; aind < EnvNAV_SE2Cfg.numofdirs; aind++) {
            //the actions are undirected, so we can use the same array of actions as in getsuccs case
            int affx = cell.x + EnvNAV_SE2Cfg.dx_[aind];
            int affy = cell.y + EnvNAV_SE2Cfg.dy_[aind];
            if (affx < 0 || affx >= EnvNAV_SE2Cfg.EnvWidth_c || affy < 0 || affy >= EnvNAV_SE2Cfg.EnvHeight_c) continue;
            preds_of_changededgesIDV->push_back(GetStateFromCoord(affx, affy, theta));
        }
    }
}

// identical to GetPredsofChangedEdges except for changing "preds"
// into "succs"... can probably have just one method.
void EnvironmentNAV_SE2::GetSuccsofChangedEdges(vector<nav2dcell_t> const * changedcellsV,
                                              vector<int> *succs_of_changededgesIDV)
{
    nav2dcell_t cell;
    int aind;

    //#TODO
    int theta = 0;

    for (int i = 0; i < (int)changedcellsV->size(); i++) {
        cell = changedcellsV->at(i);
        succs_of_changededgesIDV->push_back(GetStateFromCoord(cell.x, cell.y, theta));
        for (aind = 0; aind < EnvNAV_SE2Cfg.numofdirs; aind++) {
            int affx = cell.x + EnvNAV_SE2Cfg.dx_[aind];
            int affy = cell.y + EnvNAV_SE2Cfg.dy_[aind];
            if (affx < 0 || affx >= EnvNAV_SE2Cfg.EnvWidth_c || affy < 0 || affy >= EnvNAV_SE2Cfg.EnvHeight_c) continue;
            succs_of_changededgesIDV->push_back(GetStateFromCoord(affx, affy, theta));
        }
    }
}

bool EnvironmentNAV_SE2::IsObstacle(int x, int y)
{
    throw SBPL_Exception("EnvNAV_SE2: function: IsObstacle is undefined");
    return (EnvNAV_SE2Cfg.Grid2D[x][y] >= EnvNAV_SE2Cfg.obsthresh);
}

unsigned char EnvironmentNAV_SE2::GetMapCost(int x, int y)
{
    throw SBPL_Exception("EnvNAV_SE2: function: GetMapCost is undefined");
    return EnvNAV_SE2Cfg.Grid2D[x][y];
}

void EnvironmentNAV_SE2::GetEnvParms(int *size_x, int *size_y, int* startx, int* starty, int* goalx, int* goaly,
                                   unsigned char* obsthresh)
{
    *size_x = EnvNAV_SE2Cfg.EnvWidth_c;
    *size_y = EnvNAV_SE2Cfg.EnvHeight_c;

    *startx = EnvNAV_SE2Cfg.StartX_c;
    *starty = EnvNAV_SE2Cfg.StartY_c;
    *goalx = EnvNAV_SE2Cfg.EndX_c;
    *goaly = EnvNAV_SE2Cfg.EndY_c;

    *obsthresh = EnvNAV_SE2Cfg.obsthresh;
}

bool EnvironmentNAV_SE2::SetEnvParameter(const char* parameter, int value)
{
    if (EnvNAV_SE2.bInitialized == true) {
        SBPL_ERROR("ERROR: all parameters must be set before initialization of the environment\n");
        return false;
    }

    SBPL_PRINTF("setting parameter %s to %d\n", parameter, value);

    if (strcmp(parameter, "is16connected") == 0) {
        if (value != 0)
            EnvNAV_SE2Cfg.numofdirs = 16;
        else
            EnvNAV_SE2Cfg.numofdirs = 8;
    }
    else {
        SBPL_ERROR("ERROR: invalid parameter %s\n", parameter);
        return false;
    }

    return true;
}

//returns true if two states meet the same condition - see environment.h for more info
bool EnvironmentNAV_SE2::AreEquivalent(int StateID1, int StateID2)
{
#if DEBUG
    if (StateID1 >= (int)EnvNAV_SE2.StateID2CoordTable.size() || StateID2 >= (int)EnvNAV_SE2.StateID2CoordTable.size()) {
        throw SBPL_Exception("ERROR in EnvNAV_SE2... function: stateID illegal (2)");
    }
#endif

    //get X, Y for the states
    EnvNAV_SE2HashEntry_t* HashEntry1 = EnvNAV_SE2.StateID2CoordTable[StateID1];
    EnvNAV_SE2HashEntry_t* HashEntry2 = EnvNAV_SE2.StateID2CoordTable[StateID2];

    if (HashEntry1->X == HashEntry2->X && HashEntry1->Y == HashEntry2->Y) return true;

    return false;
}


//------------------------------------------------------------------------------
