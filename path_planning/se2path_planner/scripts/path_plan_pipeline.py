#!/usr/bin/env python2
import time, sys, rospy, numpy as np, numpy.matlib, time, tf, yaml, math, pcl , copy
from nav_msgs.srv import GetMap, GetMapRequest #for this service
from se2path_planner.srv import Mink3d, Mink3dRequest
from geometry_msgs.msg import Point, Pose2D

#do read dinesh setup
from sensor_msgs.msg import PointCloud
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
from se2path_planner.srv import CvxCorr

from se2path_planner.msg import PlaneCvx, Plane


from se2path_planner.qp_solving_support import cvx_gurobi_solver, poly_qp
from se2path_planner.convex_corridor import cvx_corridor_alg  #cvx_corridor_alg.CVX_corridor_decomp()  then cls obj: Corridor_generator(self,Path,Obstacles,*args) both of list type, last arg is boolean

#new solver
from se2path_planner.qp_solving_support import simple_cvx_gurobi_solver 

#For visualization
from nav_msgs.msg import Path
from geometry_msgs.msg import Pose, Point, Quaternion, PoseStamped
import scipy; from scipy import linalg

class Path_pipline(object):
    """class that takes callbacks that collect the point cloud and path"""
    def __init__(self):
        # super(Path_pipline, self).__init__()
        self.cloud = []
        self.m3pc = []
        self.se2path = []
        self.se2path_orig = []
        self.start_pose = [] #was type geometry_msgs/PoseStamped ("/robot_pose" has req.pose.(position.(x,y,z),orientation.(x,y,z,w)))
        self.goal_pose = [] #geometry_msgs/PoseStamped  ("/move_base_simple/goal" has variable .point.(x,y,z))
        self.cvx_corridor = []
        self.opt_path = []
        self.cvx_corridor_obj = cvx_corridor_alg.CVX_corridor_decomp()
        self.cvx_gurobi_solver = cvx_gurobi_solver.Gurobi_solver_cls()
        self.simple_cvx_gurobi_solver = simple_cvx_gurobi_solver.Gurobi_Solver_Simple()

        self.m3pc_finished_bool = False
        self.se2path_finished_bool = False
        self.start_pose_finished_bool = False
        self.goal_pose_finished_bool = False

        #get the start and goal locations
        self.sub1 = rospy.Subscriber("/robot_pose", PoseStamped, self.start_callback, queue_size = 1)
        self.sub2 = rospy.Subscriber("/move_base_simple/goal", PoseStamped, self.goal_callback, queue_size = 1)
        #Topics are  #"/pc" of type sensor_msgs/PointCloud #"/se2path_planner_node/path" of type nav_msgs/Path
        self.sub3 = rospy.Subscriber("/pc", PointCloud, self.mink3d_callback, queue_size = 1)
        self.sub4 = rospy.Subscriber("/se2path_planner_node/path", Path, self.se2path_callback, queue_size = 1)

        #pub path from opt
        self.path_pub = rospy.Publisher('/se2path_optimized',Path, queue_size=1, latch=True)
        self.poly_order = 3 #for polynomials in gurobi path

    # def list_theta_pts(self,list):
    #     z_list= []; for idx in range(len(list)): z_list.append(list[idx].z)
    #     print "minimum value: ", np.min(z_list)  #was -3.14159 (or -pi), "max value: ", np.max(z_list)  #was 2.35


    def mink3d_callback(self, req):
        '''Question: is the pc repeated, or do they need to be repeated to be in the range [-3pi,3pi]? '''
        print "callback for minkowski point cloud"
        val = [-2*math.pi, 0, 2*math.pi]
        for jdx in range(3):
            for idx in range(len(req.points)):
                pt_curr = [req.points[idx].x, req.points[idx].y, req.points[idx].z + val[jdx]] #access req.points.x(.y,.z)
                self.m3pc.append(pt_curr)
        # self.list_theta_pts(req.points)
        self.cloud = req; self.m3pc_finished_bool = True
        ''' need to repeat from -3pi:3pi '''
        if self.test_all_required_terms(): print "Path found"

    def se2path_callback(self,req):
        """This function reads the se2 path, and if full rotation outside [-pi,pi] occurs, then 2pi is either added or subtracted until a flip occurs again """

        self.se2path_orig = req #poses list of PoseStamped
        #access req.pose.position.(x,y,z) and req.pose.orientation.(x,y,z,w) in a list
        #note, this returns a path in r^3 where the objects are repeated from -3pi:3pi 3 times, but the optimal path is found in first pass with A*
        #We still perform this step to ensure

        #Handle wrap around with path
        theta_offset = 0 #this handles wrap around
        for idx in range(len(req.poses)):
            pose_cur = req.poses[idx]
            theta_z = self.quat_to_theta_z(pose_cur)
            #Still need to check for jumps in theta_z
            if len(self.se2path) > 2:
                #more than two values, so compare the last two
                theta_diff = self.se2path[-1][2] - self.se2path[-2][2] #last minus 2nd from last
                if np.abs(theta_diff) > (2*math.pi - 0.1):
                    if self.se2path[-1][2] > self.se2path[-2][2]:
                        if theta_offset < math.pi: theta_offset += 2*math.pi
                    elif self.se2path[-1][2] < self.se2path[-2][2]:
                        if theta_offset > -math.pi: theta_offset -= 2*math.pi
                        #the check of pi in both cases, ensures you don't keep adding/subtracting 2pi if you just rotate in circles, shouldn't be required, but safety check
            z = theta_z + theta_offset
            pt_obj = [pose_cur.pose.position.x, pose_cur.pose.position.y, z]
            self.se2path.append(pt_obj)

        # print "path min val", np.min(np.array(self.se2path)[:,2])
        # print "path max val", np.max(np.array(self.se2path)[:,2])
        self.se2path_finished_bool = True

        if self.test_all_required_terms():
            print "Path found"

    def start_callback(self,req):
        #callback for start and goal position setup
        theta = self.quat_to_theta_z(req)
        pose = [req.pose.position.x,req.pose.position.y,theta]
        self.start_pose = pose
        self.start_pose_finished_bool = True

    def goal_callback(self,req):
        #callback for start and goal position setup
        theta = self.quat_to_theta_z(req)
        pose = [req.pose.position.x,req.pose.position.y,theta]

        self.goal_pose = pose
        self.goal_pose_finished_bool = True

    def pose_callback(self, req, arg):
        #callback for start and goal position setup
        theta = self.quat_to_theta_z(req)
        pose = [req.pose.position.x,req.pose.position.y,theta]
        if arg[0] in "start":
            self.start_pose = pose
            self.start_pose_finished_bool = True

        elif arg[0] in "goal":
            self.goal_pose = pose
            self.goal_pose_finished_bool = True

        if self.test_all_required_terms():
            print "Path found"

    def quat_to_theta_z(self, odom):
        theta_odom = np.arctan2(odom.pose.orientation.z , odom.pose.orientation.w)*2.
        if theta_odom > math.pi:
            theta_odom = theta_odom - 2*math.pi
        elif theta_odom < -math.pi:
            theta_odom = theta_odom + 2*math.pi
        return theta_odom

    def test_all_required_terms(self):
        # if  self.m3pc_finished_bool and (len(self.se2path) > 0) and  (len(self.m3pc) > 0) and (len(self.start_pose) > 0) and  (len(self.goal_pose) > 0):
        if  self.m3pc_finished_bool and self.se2path_finished_bool and self.start_pose_finished_bool and self.goal_pose_finished_bool:
            print "\n Finding corridors"
            self.find_cvx_corridors() #call the rest of the code

            #Reset flags
            self.m3pc_finished_bool = False
            self.se2path_finished_bool = False
            self.start_pose_finished_bool = False
            self.goal_pose_finished_bool = False
            return True
        else:
            return False

    def find_cvx_corridors(self):
        """cvx corridor generator given seed path and obstacle points as lists of lists """
        seed_path = self.se2path; obstacle_pts = self.m3pc

        # world_planes = np.array([[-1.,  0.,  0., -10.],[ 0.,  1.,  0., -10.],[ 0., -1.,  0., -10.],[ 1.,  0.,  0., -10.],[ 0.,  0.,  1., -3*math.pi],[ 0.,  0., -1.,-3*math.pi]])

        print "length of the seedpath: ", len(seed_path); self.cvx_corridor = self.cvx_corridor_obj.Corridor_generator(seed_path,obstacle_pts, world_planes)#Pass path list and obstacle list to cvx corridor generator
        #print "\n Generating Optimal Path", "cvx_corridor generated", len(self.cvx_corridor)
        #Print the corridor
        # for idx in range(len(self.cvx_corridor)): A_curr = self.cvx_corridor[idx] #list of inequality constraints for the idx'th chord
        #    print '----', len(A_curr); for idx2 in range(len(A_curr)): print A_curr[idx2]

        '''
        try:
            #send it to the service:
            self.corr_service = rospy.ServiceProxy('/cvx_corridor',CvxCorr) #needs args ['map', 'robot_hull_pts', 'world_min', 'world_max']
            corr_service_response = self.corr_service(self.cloud, self.se2path_orig)
            print "response recieved: \n", corr_service_response
            print ""
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

        #Now append world planes to all corridors, to ensure closure 
        new_corr_service_response = self.append_world_planes(corr_service_response)
        #Now prep for optimization:
        self.cvx_corridor_struct = new_corr_service_response.corridors
        self.cvx_corridor = self.cvx_corr_service_resp_to_lists(new_corr_service_response)
        '''
        #Optimize:
        self.optimal_path_generator() #Now call the optimal path generator

    def append_world_planes(self, cvx_corr_resp):
        #Define world planes:

        """Get these from map topic """
        world_planes = np.array([[-1.,  0.,  0., -10.],
                                 [ 0.,  1.,  0., -10.],
                                 [ 0., -1.,  0., -10.],
                                 [ 1.,  0.,  0., -10.],
                                 [ 0.,  0.,  1., -3*math.pi],
                                 [ 0.,  0., -1., -3*math.pi]])

        # world_planes_obj = PlaneCvx() #contains wpo.planes[idx].plane[jdx]

        plane = Plane()
        for idx in range(len(cvx_corr_resp.corridors)):
            for jdx in range(6):
                plane_curr = copy.copy(plane)
                plane_curr.plane = world_planes[0].tolist()
                cvx_corr_resp.corridors[idx].planes.append(plane_curr)  #this element is also a list (with size d+1 for d dimension)


        return cvx_corr_resp

    def cvx_corr_service_resp_to_lists(self, cvx_corr_resp):
        # cvx_corr_resp.corridors[idx].planes[jdx].plane #last is list as well        
        cvx_corr_set = []
        for idx in range(len(cvx_corr_resp.corridors)):
            cvx_corr_list = []
            for jdx in range(len(cvx_corr_resp.corridors[idx].planes)):
                cvx_corr_list.append(cvx_corr_resp.corridors[idx].planes[jdx].plane)
            cvx_corr_set.append(cvx_corr_list)
        return cvx_corr_set


    def define_chord_segement_lengths(self,path_pts):
        chord_segment_lengths = {"lengths":[], "total_length":[]}#1. Divide up segments in order to determine relative time requirements for each chord (using ratio of chord lengths to total distance)
        th_scaling = 1  #for scaling considerations
        for idx in range(len(path_pts)-1):
            li = np.sqrt(np.sum((path_pts[idx+1][0] - path_pts[idx][0])**2 +
                                (path_pts[idx+1][1] - path_pts[idx][1])**2 +
                                (th_scaling*path_pts[idx+1][2] - th_scaling*path_pts[idx][2])**2)) #linalg.norm([ - cvx_plane_constraints["Path_pts"][idx]])
            chord_segment_lengths["lengths"].append(li)
        chord_segment_lengths["lengths"] = np.array(np.double(chord_segment_lengths["lengths"])); chord_segment_lengths["total_length"] = np.sum(chord_segment_lengths["lengths"])
        return chord_segment_lengths

    def optimal_path_generator(self):
        #take corridor and path and find optimal with gurobi
        spacial_dimension = 3
        num_chords = len(self.se2path) - 1  #number of chords is number of points -1
        poly_order = self.poly_order #3 with vel, 5 with acc, #7 with jerk #order of polynomials
        Ti_discrete = [0]# np.divide(chord_segment_lengths["lengths"],chord_segment_lengths["total_length"])
        chord_segment_lengths = self.define_chord_segement_lengths(self.se2path)
        for idx in range(len(chord_segment_lengths["lengths"])):
            Ti_discrete.append(chord_segment_lengths["lengths"][idx]/chord_segment_lengths["total_length"] + Ti_discrete[-1])
        
        print "Ti_discrete", Ti_discrete
        time_scale = 0.5 
        Ti_discrete = np.multiply(Ti_discrete, time_scale).tolist()
        print "Ti_discrete new", Ti_discrete
        # stop
        # Ti_discrete = np.multiply(Ti_discrete, 0.1)
        chord_sub_division = 50  #May not be required, need to check
        inequality_constraints = self.cvx_corridor 
        #Get these from pub message too (robot_pose and nav_to_goal)
        # ''' same callback (if same structure pose.---) Subscriber("msg", msgtype, callback, (funct_var, ...)), then access in callback with callback(req,arg), then arg[0] etc  '''
        start_pose = self.start_pose
        goal_pose = self.goal_pose
        optimization_order = 1 #derivative: 1:vel, 2:acc, 3:jerk, 4:snap etc
        # alpha_mat, Ti_0_to_t = self.cvx_gurobi_solver.Gurobi_solver(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose,optimization_order)
        inequality_constraints = self.cvx_corridor_struct  #with struct A.corridors[].planes[].plane[]
        alpha_mat, Ti_0_to_t = self.simple_cvx_gurobi_solver.Gurobi_solver(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose,optimization_order)
        #return final optimized path
        self.opt_path = {"coeff":alpha_mat, "T":Ti_0_to_t}
        print "optimal path: ", self.opt_path
        self.display_optimal_path(self.opt_path)


    def gen_basis_vect(self,t, poly_order):
        t_basis = []
        for idx in range(poly_order+1):
            t_basis.append(t**idx)
        # t_basis = [t**3, t**2, t, 1]
        return np.matrix(t_basis)


    def display_optimal_path(self,opt_path):
        print "display the optimal path in rviz"
        #First convert from 4d to se2
        path_list = Path()
        path_list.header.frame_id = '/map'
        pts_per_chord = 20 #number of point to use to sample the curve on a particular chord
        for idx in range(len(opt_path["coeff"])):
            dt =  np.linspace(0, opt_path["T"][idx+1], pts_per_chord)
            for jdx in range(len(dt)):
                dt_curr= dt[jdx] 
                dt_vect = self.gen_basis_vect(dt_curr, self.poly_order) 
                Tmat = scipy.linalg.block_diag(dt_vect, dt_vect, dt_vect)
                coeff_vect = np.matrix(opt_path["coeff"][idx]).T             
                # print "Tmat: ", Tmat, Tmat.shape, "\n coeff: ", coeff_vect, coeff_vect.shape
                # print "dt_vect", dt_vect
                pose_curr= Tmat * coeff_vect
                print "current pose: ", pose_curr
                theta = pose_curr.item(2)
                if np.sign(theta) < 0: theta = 2*math.pi + theta
                theta_div_2 = theta/2.; qw = np.cos(theta_div_2); qz = np.sin(theta_div_2)
                pose_stamped = PoseStamped()
                pose_stamped.header.frame_id = '/map'
                pose_stamped.pose = Pose(Point(pose_curr.item(0),pose_curr.item(1),0),Quaternion(0,0,qz,qw))
                path_list.poses.append(pose_stamped)
        # print "but seed path was: ", self.seed_path
        # print "and time vector was", opt_path["T"]
        self.path_pub.publish(path_list)
        print "published path"







def main():
    print "path plan pipeline"
    rospy.init_node('main_node_path_planner')

    cls_obj = Path_pipline()

    #get the start and goal locations
    #rospy.Subscriber("/robot_pose", PoseStamped, cls_obj.pose_callback, ("start"))
    #rospy.Subscriber("/move_base_simple/goal", PoseStamped, cls_obj.pose_callback, ("goal"))
    #Topics are  #"/pc" of type sensor_msgs/PointCloud #"/se2path_planner_node/path" of type nav_msgs/Path
    #rospy.Subscriber("/pc", PointCloud, cls_obj.mink3d_callback)
    #rospy.Subscriber("/se2path_planner_node/path", Path, cls_obj.se2path_callback)

    rospy.spin()


if __name__ == '__main__':
    main()
