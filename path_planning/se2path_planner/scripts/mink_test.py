#!/usr/bin/env python2
import time, sys, rospy, numpy as np, numpy.matlib, time, tf, yaml, math, pcl , copy
from nav_msgs.srv import GetMap, GetMapRequest #for this service
from se2path_planner.srv import Mink3d, Mink3dRequest
from geometry_msgs.msg import Point, Pose2D

class Mink_test_main(object):
    def mink_call(self):
        self.map_service = rospy.ServiceProxy('/static_map',GetMap)
        # map_request =  self.map_service() #nav_msgs.srv.GetMapRequest() #this is not correct
        map_request =  GetMapRequest() #this is not correct
        resp = self.map_service(map_request)
        #set map request terms
        robot_hull = np.array([[1,1],[1,-1],[-1,1],[-1,-1]])
        robot_hull_pts = []
        pt = Point()
        for idx in range(robot_hull.shape[0]):
            pt_cp = copy.copy(pt)
            pt_cp.x = robot_hull[idx][0]
            pt_cp.y = robot_hull[idx][1]
            pt_cp.z = 0
            robot_hull_pts.append(pt_cp)


        world_min = Pose2D(); world_min.x = -100; world_min.y = -100; world_min.theta = -3*math.pi
        world_max = Pose2D(); world_max.x = 100; world_max.y = 100; world_max.theta = 3*math.pi


        #send it to the service: 
        self.mink_3d_service = rospy.ServiceProxy('Path_finder',Mink3d) #needs args ['map', 'robot_hull_pts', 'world_min', 'world_max']
        map_service_response = self.mink_3d_service(resp.map, robot_hull_pts, world_min, world_max)
        print "response recieved: \n", map_service_response
        print ""



def main():
    print "mink tester"
    rospy.init_node('mink_test_main')
    cls_obj = Mink_test_main()
    cls_obj.mink_call()



if __name__ == '__main__':
    main()