#!/usr/bin/env python2
from se2path_planner.qp_solving_support import simple_cvx_gurobi_solver, cvx_gurobi_solver
from se2path_planner.msg import PlaneCvx, Plane
import numpy as np, math
import sympy as sp
#For visualization
from nav_msgs.msg import Path
from geometry_msgs.msg import Pose, Point, Quaternion, PoseStamped
import scipy; from scipy import linalg
import rospy
from visualization_msgs.msg import Marker, MarkerArray


class Test_qp(object):
    """docstring for Test_qp"""
    def __init__(self):
        self.path_pub = rospy.Publisher('/se2path_optimized',Path, queue_size=1, latch=True)
        self.plane_pub = rospy.Publisher('/cvx_planes', MarkerArray, queue_size=1, latch=True)
        self.point_step_pub = rospy.Publisher('/opt_path_pts', MarkerArray, queue_size=1, latch=True)
        self.plane_point_pub = rospy.Publisher('/plane_pt_pub', MarkerArray, queue_size=1, latch=True)
        self.marker_array = MarkerArray()
        self.pt_step_pub_marker_array = MarkerArray()
        self.plane_pt_pub_marker_array = MarkerArray()
        self.poly_order = 3
        self.marker_ids = 0
        self.pt_marker_id = 0
    def gen_basis_vect(self,t, poly_order):
        t_basis = []
        for idx in range(poly_order+1): t_basis.append(t**idx)
        return np.matrix(t_basis)

    def gen_basis_vect_derivative(self,t, poly_order):
        t_basis = []
        tt = sp.Symbol('tt')
        for idx in range(poly_order+1): t_basis.append(tt**idx)
        tb = sp.diff(sp.Matrix(t_basis)).subs(tt,t).T.tolist()[0]
        # print "tb: ", tb

        return np.matrix(tb)


    def display_optimal_path(self,opt_path, spacial_dimension, poly_order, pts_per_chord):
        self.poly_order = poly_order
        print "display the optimal path in rviz"
        #First convert from 4d to se2
        path_list = Path()
        path_list.header.frame_id = '/map'
        # pts_per_chord = 20 #number of point to use to sample the curve on a particular chord
        for idx in range(len(opt_path["coeff"])):
            dt =  np.linspace(0, opt_path["T"][idx+1], pts_per_chord)
            for jdx in range(len(dt)):
                dt_curr= dt[jdx] 
                dt_vect = self.gen_basis_vect(dt_curr, self.poly_order) 
                dt_vect_derv = self.gen_basis_vect_derivative(dt_curr, self.poly_order) 
                if spacial_dimension == 2:
                    Tmat = scipy.linalg.block_diag(dt_vect, dt_vect)
                    Tdmat = scipy.linalg.block_diag(dt_vect_derv, dt_vect_derv)
                elif spacial_dimension == 3:
                    Tmat = scipy.linalg.block_diag(dt_vect, dt_vect, dt_vect)
                    Tdmat = scipy.linalg.block_diag(dt_vect_derv, dt_vect_derv,dt_vect_derv)
                coeff_vect = np.matrix(opt_path["coeff"][idx]).T             
                # print "Tmat: ", Tmat, Tmat.shape, "\n coeff: ", coeff_vect, coeff_vect.shape
                # print "dt_vect", dt_vect
                pose_curr= Tmat * coeff_vect
                vel_curr = Tdmat*coeff_vect
                # print "\n\ncurrent pose: ", pose_curr, " curr chord,",idx+1," curr time: ", dt_curr, " vel curr: ", vel_curr, #" coeff vector", coeff_vect
                marker_curr= self.generate_pt_marker(pose_curr, [1.,1.,0.])
                self.pt_step_pub_marker_array.markers.append(marker_curr)

                pose_stamped = PoseStamped()
                pose_stamped.header.frame_id = '/map'
                pose_stamped.pose = Pose(Point(pose_curr.item(0),pose_curr.item(1),pose_curr.item(2)),Quaternion(0,0,0,1))
                path_list.poses.append(pose_stamped)
        # print "but seed path was: ", self.seed_path
        # print "and time vector was", opt_path["T"]
        self.path_pub.publish(path_list)
        self.point_step_pub.publish(self.pt_step_pub_marker_array)
        print "published path"


    def generate_pt_marker(self, point, *color):
        pt_marker = Marker()
        pt_marker.header.frame_id = "map"
        pt_marker.header.stamp = rospy.Time()
        pt_marker.id = self.pt_marker_id; self.pt_marker_id += 1
        pt_marker.type = 2
        pt_marker.pose.position.x = point.item(0)
        pt_marker.pose.position.y = point.item(1)
        pt_marker.pose.position.z = point.item(2)
        pt_marker.pose.orientation.x = 0.
        pt_marker.pose.orientation.y = 0.
        pt_marker.pose.orientation.z = 0.
        pt_marker.pose.orientation.w = 1.
        if len(color) == 0:
            pt_marker.scale.x = 0.05
            pt_marker.scale.y = 0.05
            pt_marker.scale.z = 0.05
            pt_marker.color.a = 1.0
        else:
            pt_marker.scale.x = 0.15
            pt_marker.scale.y = 0.15
            pt_marker.scale.z = 0.15
            pt_marker.color.a = 1.0

        if len(color) == 0:
            pt_marker.color.r = 1.0
            pt_marker.color.g = 1.0
            pt_marker.color.b = .0
        else:
            pt_marker.color.r = color[0][0]
            pt_marker.color.g = color[0][1]
            pt_marker.color.b = color[0][2]
        return pt_marker






    def draw_planes(self,plane_pt, plane, color):
        theta = np.arctan2(plane[1],plane[0]);         
        qz = np.sin(theta/2); qw = np.cos(theta/2); 

        print "\n drawing plane: ", plane
        marker = Marker()
        marker.header.frame_id = "map";
        marker.header.stamp = rospy.Time();
        marker.ns = "my_namespace";
        marker.id = self.marker_ids;  self.marker_ids += 1
        marker.type = 0;
        # marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = plane_pt[0];
        marker.pose.position.y = plane_pt[1];
        marker.pose.position.z = 0.;
        if len(plane_pt) == 3: marker.pose.position.z = plane_pt[2]
        marker.pose.orientation.x = 0.0; marker.pose.orientation.y = 0.0;
        # theta = (360-135)*math.pi/180.; qz = np.sin(theta/2); qw = np.cos(theta/2)
        marker.pose.orientation.z = qz; marker.pose.orientation.w = qw;
        # marker.pose.orientation.z = 0; marker.pose.orientation.w = 1;
        marker.scale.x = 0.3;
        marker.scale.y = 0.1;
        marker.scale.z = 0.1;
        marker.color.a = 1.0; #Don't forget to set the alpha!
        marker.color.r = color[0];
        marker.color.g = color[1];
        marker.color.b = color[2];
        self.marker_array.markers.append(marker)

        #Now draw the points that define them explicitly
        pcurr = np.matrix(plane_pt)
        pt_color = [0,0,1]
        size = 2
        marker_curr= self.generate_pt_marker(pcurr,pt_color,size)
        self.plane_pt_pub_marker_array.markers.append(marker_curr)
        

        # marker_array = MarkerArray()
        # marker_array.markers = self.marker_array
        # self.plane_pub.publish(self.marker_array)

    def two_dim_planes(self):
        cvx_corridor = []; chord1 = PlaneCvx(); chord2 = PlaneCvx()

        A11 = [-1.,0.,-1.]; A12 = [1.,-1.,-.5]; A13 = [0.,1.,-1.1];
        A21 = [1.,0.,-2.5]; A22 = [-1.,-1.,1.5]; A23 = [0.,1.,-1.1];
        #pts which define the planes
        pt11 = [-1.,0.]; pt12 = [0.5,0.]; pt13 = [1.,1.1]
        pt21 = [2.5,0.]; pt22 = [1.5,0.]; pt23 = [1.1,1.1]

        self.draw_planes(pt11,A11,[1,0,0]); 
        self.draw_planes(pt12,A12,[1,0,0]); 
        self.draw_planes(pt13,A13,[1,0,0])
        
        self.draw_planes(pt21,A21,[0,1,0]); 
        self.draw_planes(pt22,A22,[0,1,0]); 
        self.draw_planes(pt23,A23,[0,1,0])
        


        p11 = Plane(); p11.plane = A11; p12 = Plane(); p12.plane = A12; p13 = Plane(); p13.plane = A13 
        p21 = Plane(); p21.plane = A21; p22 = Plane(); p22.plane = A22; p23 = Plane(); p23.plane = A23
        chord1.planes = [p11,p12,p13]; chord2.planes = [p21,p22,p23]
        cvx_corridor = [chord1, chord2]
        return cvx_corridor

    def three_dim_planes(self):
        cvx_corridor = []; chord1 = PlaneCvx(); chord2 = PlaneCvx()

        A11 = [-1.,0.,0,-1.]; A12 = [1.,-1.,0.,-.5]; A13 = [0.,1.,0.,-1.1];
        A21 = [1.,0.,0.,-2.5]; A22 = [-1.,-1.,0.,1.5]; A23 = [0.,1.,0.,-1.1];
        pt11 = [-1.,0.,0.]; pt12 = [0.5,0.,0.]; pt13 = [1.,1.1,0.0]#pts which define the planes
        pt21 = [2.5,0.,0.]; pt22 = [1.5,0.,0.]; pt23 = [1.1,1.1,0.]

        self.draw_planes(pt11,A11,[1,0,0]); 
        self.draw_planes(pt12,A12,[1,0,0]); 
        self.draw_planes(pt13,A13,[1,0,0])
        
        self.draw_planes(pt21,A21,[0,1,0]); 
        self.draw_planes(pt22,A22,[0,1,0]); 
        self.draw_planes(pt23,A23,[0,1,0])
        
        self.plane_point_pub.publish(self.plane_pt_pub_marker_array)
        self.plane_pub.publish(self.marker_array)

        self.plane_pub.publish(self.marker_array)

        p11 = Plane(); p11.plane = A11; p12 = Plane(); p12.plane = A12; p13 = Plane(); p13.plane = A13 
        p21 = Plane(); p21.plane = A21; p22 = Plane(); p22.plane = A22; p23 = Plane(); p23.plane = A23
        chord1.planes = [p11,p12,p13] ; chord2.planes = [p21,p22,p23]
        cvx_corridor = [chord1, chord2]
        return cvx_corridor



def main():

    rospy.init_node('test_gurobi_simple')

    Test_qp_clss = Test_qp()


    cvx_corridor = Test_qp_clss.three_dim_planes()

    spacial_dimension = 3 #2

    num_chords = 2
    #4 doesn't work [0,0,1]

    poly_order = 3  #3 with vel, 5 with acc, #7 with jerk #order of polynomials
    Test_qp_clss.poly_order = poly_order

    Ti_discrete = [0]; time_scale = 1.
    for idx in range(num_chords):
        Ti_discrete.append(Ti_discrete[-1] + 1./num_chords)

    # Ti_discrete = [0,1.0]; time_scale = 0.05
    Ti_discrete = np.multiply(Ti_discrete, time_scale).tolist()

    chord_sub_division = 3# 10  #May not be required, need to check
    # start_pose = [0,0]
    # # goal_pose = [1,1]
    # goal_pose = [2,0]


    start_pose = [0,0,0]
    # goal_pose = [1,1]
    # goal_pose = [2,0,1]
    # goal_pose = [-0.3480238914489746, -3.4986040592193604, -1.5707963267948966]
    # goal_pose = [0.005356788635253906, 0.19668054580688477, 3.1415926535897931]
    # goal_pose = [-12.495692253112793, -3.2243311405181885, -0.78539816339744828]
    goal_pose = [2,0,0.]
    optimization_order = 1 #derivative: 1:vel, 2:acc, 3:jerk, 4:snap etc

    inequality_constraints = cvx_corridor

 

    solver_obj = simple_cvx_gurobi_solver.Optimizer(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose)    
    vis_bool = False
    opt_path = solver_obj.run_optimization(vis_bool)


    # solver = simple_cvx_gurobi_solver.Gurobi_Solver_Simple()  
    # # # alpha_mat, Ti_0_to_t = solver.Gurobi_solver(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose,optimization_order)
    # alpha_mat, Ti_0_to_t = solver.Gurobi_solver(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose)


    # # old_solver = cvx_gurobi_solver.Gurobi_solver_cls()
    # #just figure out the inequality constraint forms
    # # alpha_mat, Ti_0_to_t = old_solver.Gurobi_solver(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose)#,optimization_order)
    # print "Found solution: ", alpha_mat, Ti_0_to_t

    # opt_path = {"coeff":alpha_mat, "T":Ti_0_to_t}
    Test_qp_clss.display_optimal_path(opt_path, spacial_dimension, poly_order,chord_sub_division)
    # print "\n Goal pose: ", goal_pose
    # print "\n times: ", Ti_0_to_t, " Time basis: ", Test_qp_clss.gen_basis_vect(Ti_0_to_t[1], poly_order)
    # print "\n returned coefficients: ", alpha_mat


    rospy.spin()

if __name__ == '__main__':
    main()