#!/usr/bin/env python2

import numpy as np
import numpy.matlib
import time
import rospy
import tf
import yaml
import math
import pcl 
from se2path_finding.msg import *
from se2path_finding.astar_support import astar,seed_path_astar
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from geometry_msgs.msg import Pose2D
from se2path_finding.path_plotting_lib import  plot_path_plan_lib
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point32 as geomPoint
from geometry_msgs.msg import Point, Quaternion, PoseStamped
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import PointCloud

class world_info(object):
    def __init__(self,xmin,xmax,ymin,ymax,theta_min,theta_max,x_steps,y_steps,theta_steps):
        self.extents = {'xmin':xmin,'xmax':xmax,'ymin':ymin,'ymax':ymax,"th_min":theta_min,"th_max":theta_max}
        self.steps = {'x_steps':x_steps,'y_steps':y_steps, 'theta_steps':theta_steps}
        self.world_discretized = []
        self.occupied_cell_bool = []  #0 is free, 1 is occupied
        self.x_coord = []
        self.y_coord = []
        self.th_coord = []
class obs_type(object):
    def __init__(self):
        self.x = []
        self.y = []

class virtual_obstacles(object):
    def __init__(self,x1,y1,x2,y2,x_steps,y_steps):

        #for DENSE obstacles, however very memory intensive
        xstep = x_steps; ystep = y_steps
        x_num,x_rem = divmod(abs(x2-x1),xstep); y_num,y_rem = divmod(abs(y2-y1),ystep)
        x_coord = np.linspace(x1,x2,(x_num+1)); y_coord = np.linspace(y1,y2,(y_num+1))  

        x,y = np.meshgrid(x_coord,y_coord)
        X = x.reshape(x.size,1)
        Y = y.reshape(y.size,1)
        pts = np.matrix(np.hstack([X,Y]))


        self.points = pts
        # print "got here: ", self.points
        #obstacle points plot:
        # fig = plt.figure(); ax = fig.add_subplot(111);c = 'r'; m = 'o'; xs = pts[:,0].T; ys = pts[:,1].T; xs = xs.tolist()[0]; ys = ys.tolist()[0]; ax.scatter(xs, ys, c=c, marker=m); plt.show()
        #real sensor has 270 readings per sweep, that times number of convex hull verticies, then times the number of steps of theta

class astar_test_class(object):
    def __init__(self,path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub):
        self.point_cloud = []
        self.Plotting_debugging_rviz = plot_path_plan_lib.Plotting_debugging_rviz(path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub)



        self.world_bound = World_boundary()
        #mocap space is ~50ftx20ft  in meters: 15.24x6.096
        # self.world_bound.xmin = -30.0; self.world_bound.xmax = 30.0; self.world_bound.ymin = -30.0; self.world_bound.ymax = 30.0
        graph_scale = 1.0
        self.goal = Pose2D(); self.start = Pose2D() #consists of float 64
        
        self.start.x = 0.0
        self.start.y = 0.0
        self.start.theta = 0.

        self.world_bound.xmin = -2.*graph_scale #cm#-7.62*graph_scale #cm
        self.world_bound.xmax = 7.62*graph_scale #cm
        self.world_bound.ymin = -3.048*graph_scale #cm#-3.048*graph_scale #cm
        self.world_bound.ymax = 3.048*graph_scale #cm
        # x_steps,y_steps,theta_steps = .1*graph_scale,.1*graph_scale,10*math.pi/180  #in meters and radians  10 step is not ram intensive
        x_steps,y_steps,theta_steps = 10*math.pi/180,10*math.pi/180,10*math.pi/180  #in meters and radians  10 step is not ram intensive
        self.world_bound.xsteps = x_steps
        self.world_bound.ysteps = y_steps
        self.world_bound.thsteps = theta_steps
        # theta_min,theta_max = -3*math.pi, 3*math.pi
        theta_min,theta_max = -math.pi, math.pi  #for astar

        th_num,th_rem = divmod(abs(theta_max-theta_min),theta_steps)
        th_coord = np.linspace(theta_min,theta_max,th_num)
        self.world = world_info(self.world_bound.xmin,self.world_bound.xmax,self.world_bound.ymin,self.world_bound.ymax,theta_min,theta_max,x_steps,y_steps,theta_steps)
        self.world.th_coord = th_coord
    def vertical_list(self,obstacle_map,x_steps, y_steps):

        virt_obs_list = []
        # print "given obs map: ", obstacle_map
        for idx in range(len(obstacle_map)):
            x1 = obstacle_map[idx][0];  y1 = obstacle_map[idx][1];  x2 = obstacle_map[idx][2];  y2 = obstacle_map[idx][3]
            vobs = virtual_obstacles(x1,y1,x2,y2,x_steps,y_steps) 
            vobs = vobs.points.tolist()
            virt_obs_list.append(vobs)
        virt_obs = np.vstack(virt_obs_list)
        obstacles_list  = np.array(virt_obs)
        obstacle_obj = obs_type()
        # print "failing here: obslist: ",obstacles_list
        obstacle_obj.x = list(obstacles_list[:,0])
        obstacle_obj.y = list(obstacles_list[:,1])
        return obstacle_obj


    def vertical_list_sensor(self,obstacle_map):
        obstacle_obj = obs_type()
        obstacle_obj.x = list(obstacle_map[:,0])
        obstacle_obj.y = list(obstacle_map[:,1])
        return obstacle_obj

    def build_3D_world_with_obstacles(self,object_pts_ompl):

        mink_layered = []
        for idx in range(len(self.world.th_coord)):
            theta = self.world.th_coord[idx]
            # print "change"
            theta_vect = np.matrix(np.matlib.repmat(theta,len(object_pts_ompl),1))
            mink_xy  = object_pts_ompl 
            mink_pts_obj = np.concatenate((object_pts_ompl,theta_vect),axis=1)
            mink_layered.append(mink_pts_obj)
        mink_layered_plot = np.array(np.vstack(mink_layered))
        print "\nmink_layered_plot", mink_layered_plot
        

        self.mink_pts_layered = {"mink_xy":mink_layered, "theta":self.world.th_coord,"point_cloud":[]} #every element of mink_xy_list contains all occupied points and the corresponding theta

        world_step_factor = np.float32(0.9)
        leaf = np.array([np.float32(world_step_factor*self.world.steps['x_steps']),
                 np.float32(world_step_factor*self.world.steps['y_steps']),
                 np.float32(world_step_factor*self.world.steps['theta_steps'])],dtype=np.float32)

        pc_cloud_list = []
        for idx in range(len(self.mink_pts_layered["theta"])):
            # print 9, idx, time.time() - start
            pc_init_list = pcl.PointCloud()
            pc_init_list.from_list(self.mink_pts_layered["mink_xy"][idx].tolist())
            #filter points
            filtered_pts = pc_init_list
            cloud = filtered_pts.make_octree(max([leaf[0],leaf[1]]))
            cloud.add_points_from_input_cloud()
            pc_cloud_list.append(cloud)
        #Pass the below to the ompl to check which theta its closest too, then if xy are viable states to handle inability to make non cubic octrees
        self.mink_pts_layered["point_cloud"] = pc_cloud_list


        self.fig = plt.figure(); self.ax = self.fig.add_subplot(111, projection='3d')
        self.ax.set_xlabel('X axis'); self.ax.set_ylabel('Y axis'); self.ax.set_zlabel('Z axis')
        # c = 'b'; m = '*'
        # print "x values: ", mink_layered[:,0]
        # self.ax.scatter(mink_layered[:,0],mink_layered[:,1],mink_layered[:,2],c=c,marker=m)
        # plt.axis('equal')

        self.Plotting_debugging_rviz.Plot_obstacles(mink_layered_plot)
        
        print "should have plotted"
        r = rospy.Rate(.5)
        r.sleep()
        # time.sleep(.1)

        # self.ax.scatter(self.start.x,self.start.y,self.start.theta,c='g',marker='^')
        # self.ax.scatter(self.goal.x,self.goal.y,self.goal.theta,c='r',marker='^')

        # ax.set_xticks(np.arange(self.world_bound.xmin,self.world_bound.xmax,self.world_bound.xsteps))
        # ax.set_yticks(np.arange(self.world_bound.ymin,self.world_bound.ymax,self.world_bound.ysteps))
        # ax.set_zticks(np.arange(self.world.extents["th_min"],self.world.extents["th_max"],self.world_bound.thsteps))
        # plt.grid()
        # plt.show()

        return mink_layered_plot


    def make_point_cloud(self,mink_layered):
        self.point_cloud = []
        world_step_factor = np.float32(0.9)
        leaf = np.array([np.float32(world_step_factor*self.world.steps['x_steps']),
                         np.float32(world_step_factor*self.world.steps['y_steps']),
                         np.float32(world_step_factor*self.world.steps['theta_steps'])],dtype=np.float32)
        pc_init = pcl.PointCloud()
        pc_init.from_array(np.float32(mink_layered))
        vox_filt = pc_init.make_voxel_grid_filter()
        vox_filt.set_leaf_size(leaf[0],leaf[1],leaf[2])
        filtered_pts = vox_filt.filter()
        active_obstacle_pts = np.matrix(filtered_pts.to_array()).tolist()
        pc = pcl.PointCloud()
        pc.from_array(np.array(np.float32(active_obstacle_pts)))
        self.cloud = pc.make_octree(max(leaf)*1.5)
        self.cloud.add_points_from_input_cloud()


    def Perform_astar(self):
        world = self.world
        pt_cloud = self.cloud
        # pt_cloud = self.mink_pts_layered
        start_pose = np.matrix([self.start.x, self.start.y, self.start.theta])
        goal_pose = np.matrix([self.goal.x, self.goal.y, self.goal.theta])

        self.seed_path_astar = seed_path_astar.astar_support(world, pt_cloud,start_pose, goal_pose)
        path = self.seed_path_astar.get_path()
        print "\n\npath found:", path

        self.ax.scatter(path[:,0],path[:,1],path[:,2],c='r',marker='*')
        scale = .1; color = [1,0,0]; self.Plotting_debugging_rviz.Plot_scatter_list(path.tolist(),scale,color)
        print "\n Goal: ", goal_pose, "\n Start: ", start_pose
        # plt.show()



def main():
    print "main"
    rospy.init_node('astar_test')
    path_topic = "/path_points"; path_points_pub = rospy.Publisher(path_topic,MarkerArray,queue_size = 1, latch=True)
    cvx_path_topic = "/cvx_path_points"; cvx_path_points_pub = rospy.Publisher(cvx_path_topic,MarkerArray,queue_size = 1, latch=True)
    topic = "/points_visualization"; obstacle_points_pub = rospy.Publisher(topic,PointCloud,queue_size = 1, latch=True)
    vector_topic = "/vectors_drawn"; vector_pub = rospy.Publisher(vector_topic,Marker,queue_size = 1, latch=True)
    plane_topic = "/planes_drawn"; planes_pub = rospy.Publisher(plane_topic,Marker,queue_size = 1, latch=True)
    valid_occ_topic = "/valid_points_visualization"; valid_obstacle_points_pub = rospy.Publisher(valid_occ_topic,PointCloud,queue_size = 1, latch=True)
    # time.sleep(0.5) # Sleep for publishers to be registered on the ROS master

    cls_obj = astar_test_class(path_points_pub,obstacle_points_pub,vector_pub,planes_pub,valid_occ_topic, cvx_path_points_pub)




    use_yaml = True

    if use_yaml:
        file_name = '/home/kmonroe/kuka7dof_ws/src/human_robot_transport/se2path_finding/scripts/obs_map_data.yaml'
        with open(file_name, 'r') as f:
            obs_map_test = yaml.load(f)
        obstacle_map_curr = []
        for key in obs_map_test.keys():
            if key not in "goal_pose" and key not in "sensor_reading":
                row =np.hstack(obs_map_test[key])
                obstacle_map_curr.append(row.tolist())
        cls_obj.goal.x = obs_map_test["goal_pose"][0]
        cls_obj.goal.y = obs_map_test["goal_pose"][1]
        cls_obj.goal.theta = obs_map_test["goal_pose"][2] #betwen -pi:pi


        # cls_obj.goal.x = cls_obj.start.x
        # cls_obj.goal.y = cls_obj.start.y
        # cls_obj.goal.theta = obs_map_test["goal_pose"][2] #betwen -pi:pi



        Obstacle_map_1 = obstacle_map_curr

    obstacle_points = cls_obj.vertical_list(obstacle_map_curr,cls_obj.world_bound.xsteps, cls_obj.world_bound.ysteps)
    obstacle_points_ompl = obstacle_points
    obstacle_points_ompl = np.matrix(np.vstack([obstacle_points_ompl.x,obstacle_points_ompl.y])).T
    obstacle_points_ompl = obstacle_points_ompl.tolist()
    obstacle_list_ompl = obstacle_points_ompl 

    if len(obs_map_test["sensor_reading"]) != 0:
        print "using sensor readings"
        obstacle_map_curr = obs_map_test["sensor_reading"]
        obstacle_points = cls_obj.vertical_list_sensor(obstacle_map_curr)
        obstacle_points = np.matrix(np.vstack([obstacle_points.x,obstacle_points.y])).T
        obstacle_points = obstacle_points.tolist()
    print "obs pts:",obstacle_points
    # cls_obj.world_bound.thsteps


    print "stop here", obstacle_points
    mink_layered = cls_obj.build_3D_world_with_obstacles(obstacle_list_ompl)
    # mink_layered = cls_obj.build_3D_world_with_obstacles(obstacle_points)

    cls_obj.make_point_cloud(mink_layered)
    print "point cloud made"
    cls_obj.Perform_astar()
    # rospy.spin()


if __name__ == '__main__':
    main()


