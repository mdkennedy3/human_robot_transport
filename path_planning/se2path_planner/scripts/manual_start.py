#!/usr/bin/env python2
import rospy
from geometry_msgs.msg import PoseStamped


def main():
    rospy.init_node('manual_start_pose')
    pose_init = PoseStamped()
    pose_init.header.frame_id = 'map'
    pose_init.pose.position.x = 0.#1.8
    pose_init.pose.position.y = 0.#1.0
    pose_init.pose.position.z = 0.0
    
    pose_init.pose.orientation.x = 0.0
    pose_init.pose.orientation.y = 0.0
    pose_init.pose.orientation.z = 0.0
    pose_init.pose.orientation.w = 1.0

    pub = rospy.Publisher("/robot_pose", PoseStamped, queue_size=1)


    while not rospy.is_shutdown():
        pub.publish(pose_init)




if __name__ == '__main__':
    main()




