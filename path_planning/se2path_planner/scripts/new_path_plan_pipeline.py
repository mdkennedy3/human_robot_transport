#!/usr/bin/env python2
import time, sys, rospy, numpy as np, numpy.matlib, time, tf, yaml, math , copy
from nav_msgs.srv import GetMap, GetMapRequest #for this service
from se2path_planner.srv import Mink3d, Mink3dRequest
from geometry_msgs.msg import Point, Pose2D

#do read dinesh setup
from sensor_msgs.msg import PointCloud
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
from se2path_planner.srv import CvxCorr

from se2path_planner.msg import PlaneCvx, Plane, TrajMsg, ListMsg


from se2path_planner.qp_solving_support import cvx_gurobi_solver, poly_qp
from se2path_planner.convex_corridor import cvx_corridor_alg  #cvx_corridor_alg.CVX_corridor_decomp()  then cls obj: Corridor_generator(self,Path,Obstacles,*args) both of list type, last arg is boolean

#new solver
from se2path_planner.qp_solving_support import simple_cvx_gurobi_solver
from se2path_planner.qp_solving_support import  cvx_gurobi_solver

from se2path_planner.path_plotting_lib import  plot_path_plan_lib

#For visualization
from nav_msgs.msg import Path
from geometry_msgs.msg import Pose, Point, Quaternion, PoseStamped
import scipy; from scipy import linalg
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray

class Path_pipline(object):
    """class that takes callbacks that collect the point cloud and path"""
    def __init__(self):
        # super(Path_pipline, self).__init__()
        self.cloud = []
        self.m3pc = []
        self.se2path = []
        self.se2path_orig = []
        self.start_pose = [] #was type geometry_msgs/PoseStamped ("/robot_pose" has req.pose.(position.(x,y,z),orientation.(x,y,z,w)))
        self.goal_pose = [] #geometry_msgs/PoseStamped  ("/move_base_simple/goal" has variable .point.(x,y,z))
        self.cvx_corridor = []
        self.opt_path = []
        self.cvx_corridor_obj = cvx_corridor_alg.CVX_corridor_decomp()
        self.cvx_gurobi_solver = cvx_gurobi_solver.Gurobi_solver_cls()
        self.simple_cvx_gurobi_solver = simple_cvx_gurobi_solver.Gurobi_Solver_Simple()

        self.m3pc_finished_bool = False
        self.se2path_finished_bool = False
        self.start_pose_finished_bool = False
        self.goal_pose_finished_bool = False

        #get the start and goal locations
        self.sub1 = rospy.Subscriber("/robot_pose", PoseStamped, self.start_callback, queue_size = 1)
        self.sub2 = rospy.Subscriber("/move_base_simple/goal", PoseStamped, self.goal_callback, queue_size = 1)
        #Topics are  #"/pc" of type sensor_msgs/PointCloud #"/se2path_planner_node/path" of type nav_msgs/Path
        self.sub3 = rospy.Subscriber("/pc", PointCloud, self.mink3d_callback, queue_size = 1)
        self.sub4 = rospy.Subscriber("/se2path_planner_node/path", Path, self.se2path_callback, queue_size = 1)

        #pub path from opt
        self.path_pub = rospy.Publisher('/se2path_optimized',Path, queue_size=1, latch=True)
        self.init_path_pub = rospy.Publisher('/init_path_3d', Path, queue_size=1, latch=True)
        self.poly_order = 3 #for polynomials in gurobi path

        #plot hull of robot
        cvx_hull_path_pub = rospy.Publisher('/cvx_hull_path',MarkerArray, queue_size = 1, latch=True)
        self.Plotting_rviz_cvx_hull_path_pub = plot_path_plan_lib.Plot_general_markers_rviz(cvx_hull_path_pub)


        #save coefficients and time traj
        self.opt_path_terms_pub = rospy.Publisher('/opt_path_traj',TrajMsg, queue_size = 1, latch=True)



    def mink3d_callback(self, req):
        '''Question: is the pc repeated, or do they need to be repeated to be in the range [-3pi,3pi]? '''
        print "callback for minkowski point cloud"
        # val = [-2*math.pi, 0, 2*math.pi]
        # for jdx in range(3):
        #     for idx in range(len(req.points)):
        #         pt_curr = [req.points[idx].x, req.points[idx].y, req.points[idx].z + val[jdx]] #access req.points.x(.y,.z)
        #         self.m3pc.append(pt_curr)
        # self.list_theta_pts(req.points)

        """ This needs to handle the wrap around with addition of 2pi, -2pi etc """
        self.cloud = req; self.m3pc_finished_bool = True
        ''' need to repeat from -3pi:3pi '''
        if self.test_all_required_terms(): print "Path found"

    def se2path_callback(self,req):
        """This function reads the se2 path, and if full rotation outside [-pi,pi] occurs, then 2pi is either added or subtracted until a flip occurs again """

        req_adjusted = copy.copy(req)

        # self.se2path_orig = req #poses list of PoseStamped
        #access req.pose.position.(x,y,z) and req.pose.orientation.(x,y,z,w) in a list
        #note, this returns a path in r^3 where the objects are repeated from -3pi:3pi 3 times, but the optimal path is found in first pass with A*
        #We still perform this step to ensure

        #Handle wrap around with path
        
        theta_offset = 0 #this handles wrap around
        for idx in range(len(req_adjusted.poses)):
            pose_cur = req_adjusted.poses[idx]
            theta_z = self.quat_to_theta_z(pose_cur)
            req_adjusted.poses[idx].pose.position.z = copy.copy(theta_z)
            #Still need to check for jumps in theta_z
            if len(req_adjusted.poses) > 2:
                #more than two values, so compare the last two
                # theta_diff = self.se2path[-1][2] - self.se2path[-2][2] #last minus 2nd from last
                theta_diff = req_adjusted.poses[idx].pose.position.z - req_adjusted.poses[idx-1].pose.position.z #last minus 2nd from last
                if np.abs(theta_diff) > (2*math.pi - 0.1):
                    if req_adjusted.poses[idx].pose.position.z > req_adjusted.poses[idx-1].pose.position.z:
                        if theta_offset < math.pi: theta_offset += 2*math.pi
                    elif req_adjusted.poses[idx].pose.position.z < req_adjusted.poses[idx-1].pose.position.z:
                        if theta_offset > -math.pi: theta_offset -= 2*math.pi
                        #the check of pi in both cases, ensures you don't keep adding/subtracting 2pi if you just rotate in circles, shouldn't be required, but safety check
            z = theta_z + theta_offset
            req_adjusted.poses[idx].pose.position.z = copy.copy(z)
            # pt_obj = [pose_cur.pose.position.x, pose_cur.pose.position.y, z]
            # self.se2path.append(pt_obj)
        

        self.se2path_orig = req_adjusted #poses list of PoseStamped

        self.init_path_pub.publish(self.se2path_orig)
        self.se2path_finished_bool = True

        if self.test_all_required_terms():
            print "Path found"

    def start_callback(self,req):
        #callback for start and goal position setup
        theta = self.quat_to_theta_z(req)
        pose = [req.pose.position.x,req.pose.position.y,theta]
        self.start_pose = pose
        self.start_pose_finished_bool = True

    def goal_callback(self,req):
        #callback for start and goal position setup
        theta = self.quat_to_theta_z(req)
        pose = [req.pose.position.x,req.pose.position.y,theta]

        self.goal_pose = pose
        self.goal_pose_finished_bool = True

    def quat_to_theta_z(self, odom):
        theta_odom = np.arctan2(odom.pose.orientation.z , odom.pose.orientation.w)*2.
        if theta_odom > math.pi:
            theta_odom = theta_odom - 2*math.pi
        elif theta_odom < -math.pi:
            theta_odom = theta_odom + 2*math.pi
        return theta_odom

    def test_all_required_terms(self):
        # if  self.m3pc_finished_bool and (len(self.se2path) > 0) and  (len(self.m3pc) > 0) and (len(self.start_pose) > 0) and  (len(self.goal_pose) > 0):
        if  self.m3pc_finished_bool and self.se2path_finished_bool and self.start_pose_finished_bool and self.goal_pose_finished_bool:
            print "\n Finding corridors"
            self.find_cvx_corridors() #call the rest of the code

            #Reset flags
            self.m3pc_finished_bool = False
            self.se2path_finished_bool = False
            self.start_pose_finished_bool = False
            self.goal_pose_finished_bool = False
            return True
        else:
            return False

    def find_cvx_corridors(self):
        """cvx corridor generator given seed path and obstacle points as lists of lists """
        # seed_path = self.se2path; obstacle_pts = self.m3pc

        # world_planes = np.array([[-1.,  0.,  0., -10.],[ 0.,  1.,  0., -10.],[ 0., -1.,  0., -10.],[ 1.,  0.,  0., -10.],[ 0.,  0.,  1., -3*math.pi],[ 0.,  0., -1.,-3*math.pi]])

        # print "length of the seedpath: ", len(seed_path); self.cvx_corridor = self.cvx_corridor_obj.Corridor_generator(seed_path,obstacle_pts, world_planes)#Pass path list and obstacle list to cvx corridor generator
        #print "\n Generating Optimal Path", "cvx_corridor generated", len(self.cvx_corridor)
        #Print the corridor
        # for idx in range(len(self.cvx_corridor)): A_curr = self.cvx_corridor[idx] #list of inequality constraints for the idx'th chord
        #    print '----', len(A_curr); for idx2 in range(len(A_curr)): print A_curr[idx2]

        
        try:
            #send it to the service:
            self.corr_service = rospy.ServiceProxy('/cvx_corridor',CvxCorr) #needs args ['map', 'robot_hull_pts', 'world_min', 'world_max']
            corr_service_response = self.corr_service(self.cloud, self.se2path_orig)
            # print "response recieved: \n", corr_service_response
            # print ""
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

        #Now append world planes to all corridors, to ensure closure 
        new_corr_service_response = self.append_world_planes(corr_service_response)
        #Now prep for optimization:
        self.cvx_corridor_struct = new_corr_service_response.corridors
        # self.cvx_corridor_struct = corr_service_response.corridors
        # print "the planes that are passed: ", self.cvx_corridor_struct
        # self.cvx_corridor = self.cvx_corr_service_resp_to_lists(new_corr_service_response)
        
        #Optimize:
        self.optimal_path_generator() #Now call the optimal path generator

    def append_world_planes(self, cvx_corr_resp):
        #Define world planes:
        """Get these from map topic """
        # world_planes = np.array([[-1.,  0.,  0., -10.],
        #                          [ 1.,  0.,  0., -10.],
        #                          [ 0.,  1.,  0., -10.],
        #                          [ 0., -1.,  0., -10.],                                 
        #                          [ 0.,  0.,  1., -3*math.pi],
        #                          [ 0.,  0., -1., -3*math.pi]])
        # plane = Plane()


        for idx in range(len(cvx_corr_resp.corridors)):
            # for jdx in range(6):
            #     plane_curr = copy.copy(plane)
            #     plane_curr.plane = world_planes[jdx].tolist()
            #     cvx_corr_resp.corridors[idx].planes.append(plane_curr)  #this element is also a list (with size d+1 for d dimension)



            A11 = [-1.,  0.,  0., -7.]
            A12 = [ 1.,  0.,  0., -7.]
            A13 = [ 0.,  1.,  0., -3.]
            A21 = [ 0., -1.,  0., -3.]                                 
            A22 = [ 0.,  0.,  1., -3*math.pi]
            A23 = [ 0.,  0., -1., -3*math.pi]
            # A22 = [ 0.,  0.,  1., -10]
            # A23 = [ 0.,  0., -1., -10]
            cvx_corridor = []; chord1 = PlaneCvx(); 
            p11 = Plane(); p11.plane = A11; p12 = Plane(); p12.plane = A12; p13 = Plane(); p13.plane = A13 
            p21 = Plane(); p21.plane = A21; p22 = Plane(); p22.plane = A22; p23 = Plane(); p23.plane = A23
            chord1.planes = [p11,p12,p13,p21,p22,p23]#; chord2.planes = [p21,p22,p23]

            #Whatever is set here, the point becomes an equality to at some ponit in the trajectory        
            cvx_corr_resp.corridors[idx].planes.append(p11) 
            cvx_corr_resp.corridors[idx].planes.append(p12)
            cvx_corr_resp.corridors[idx].planes.append(p13)
            cvx_corr_resp.corridors[idx].planes.append(p21)
            cvx_corr_resp.corridors[idx].planes.append(p22)
            cvx_corr_resp.corridors[idx].planes.append(p23)
        # cvx_corridor = [chord1]


        return cvx_corr_resp

    def cvx_corr_service_resp_to_lists(self, cvx_corr_resp):
        # cvx_corr_resp.corridors[idx].planes[jdx].plane #last is list as well        
        cvx_corr_set = []
        for idx in range(len(cvx_corr_resp.corridors)):
            cvx_corr_list = []
            for jdx in range(len(cvx_corr_resp.corridors[idx].planes)):
                cvx_corr_list.append(cvx_corr_resp.corridors[idx].planes[jdx].plane)
            cvx_corr_set.append(cvx_corr_list)
        return cvx_corr_set

    def define_chord_segement_lengths_from_path(self,path_pts):
        chord_segment_lengths = {"lengths":[], "total_length":[]}#1. Divide up segments in order to determine relative time requirements for each chord (using ratio of chord lengths to total distance)
        th_scaling = 1  #for scaling considerations
        for idx in range(len(path_pts.poses)-1):
            thidx_curr = self.quat_to_theta_z(path_pts.poses[idx]); thidx_next = self.quat_to_theta_z(path_pts.poses[idx+1])
            li = np.sqrt(np.sum((path_pts.poses[idx+1].pose.position.x - path_pts.poses[idx].pose.position.x)**2 +
                                (path_pts.poses[idx+1].pose.position.y - path_pts.poses[idx].pose.position.y)**2 +
                                (th_scaling*(thidx_next - thidx_curr))**2)) #linalg.norm([ - cvx_plane_constraints["Path_pts"][idx]])
            chord_segment_lengths["lengths"].append(li)
        chord_segment_lengths["lengths"] = np.array(np.double(chord_segment_lengths["lengths"])); chord_segment_lengths["total_length"] = np.sum(chord_segment_lengths["lengths"])
        return chord_segment_lengths

 
    def optimal_path_generator(self):
        #take corridor and path and find optimal with gurobi
        spacial_dimension = 3
        # num_chords = len(self.se2path) - 1  #number of chords is number of points -1
        num_chords = len(self.se2path_orig.poses) - 1  #number of chords is number of points -1
        poly_order = self.poly_order #3 with vel, 5 with acc, #7 with jerk #order of polynomials
        Ti_discrete = [0]# np.divide(chord_segment_lengths["lengths"],chord_segment_lengths["total_length"])
        # chord_segment_lengths = self.define_chord_segement_lengths(self.se2path)
        chord_segment_lengths = self.define_chord_segement_lengths_from_path(self.se2path_orig)
        for idx in range(len(chord_segment_lengths["lengths"])):
            # Ti_discrete.append(chord_segment_lengths["lengths"][idx]/chord_segment_lengths["total_length"] + Ti_discrete[-1])
            Ti_discrete.append(1./num_chords + Ti_discrete[-1])
        
        # print "Ti_discrete", Ti_discrete
        time_scale = 1.0 
        Ti_discrete = np.multiply(Ti_discrete, time_scale).tolist()
        print "Ti_discrete new", Ti_discrete
        # stop
        # Ti_discrete = np.multiply(Ti_discrete, 0.1)
        chord_sub_division = 50  #May not be required, need to check
        start_pose = copy.copy(self.start_pose)
        goal_pose = copy.copy(self.goal_pose)

        #Set them equal to the values from se2path z
        start_pose[2] = self.se2path_orig.poses[0].pose.position.z
        goal_pose[2]  = self.se2path_orig.poses[-1].pose.position.z
        optimization_order = 1 #derivative: 1:vel, 2:acc, 3:jerk, 4:snap etc
        inequality_constraints = self.cvx_corridor_struct  #with struct A.corridors[].planes[].plane[]

        self.Ti_discrete = Ti_discrete

        solver_obj = simple_cvx_gurobi_solver.Optimizer(spacial_dimension,num_chords,poly_order,Ti_discrete, chord_sub_division,inequality_constraints,start_pose, goal_pose)    
        vis_bool = False
        opt_path = solver_obj.run_optimization(vis_bool)

        del solver_obj  #make sure to wipe clean, to avoid overwriting if queried again

        self.display_optimal_path(opt_path)

        #plot cvx boundary of the robot
        nav_msg_traj_path = self.nav_msg_traj_path.poses
        self.scale_to_divide = 1 #no scaling        
        # x1,x2,y1,y2 = -0.37, 0.37, -0.27,0.27 #cm or m
        x1,x2,y1,y2 = -0.15, 0.15, -0.1,0.1 #cm or m
        self.robot_cvx_hull_pts = {"x1":x1,"x2":x2,"y1":y1,"y2":y2}
        self.frame_id = "map"
        self.Plotting_rviz_cvx_hull_path_pub.plot_robot_cvx_boundary_at_path_pts(nav_msg_traj_path,self.scale_to_divide,self.robot_cvx_hull_pts, self.frame_id)

        #publish optimal trajectory info

        self.publish_opt_traj_fnct(opt_path)






    def publish_opt_traj_fnct(self, opt_path):
        opt_path_msg = TrajMsg() 
        opt_path_msg.normed_time = self.Ti_discrete # float64[] normed_time
        opt_path_msg.optimized_time = opt_path["T"]  # float64[] optimized_time
        opt_path_msg.poly_order = self.poly_order # float64 poly_order        
        opt_path_msg.path_coeff = []# se2path_planner/ListMsg[] path_coeff

        list_msg = ListMsg()
        for idx in range(len(opt_path["coeff"])):
            list_msg_curr = copy.copy(list_msg)
            list_msg_curr.list_row = opt_path["coeff"][idx]
            opt_path_msg.path_coeff.append(list_msg_curr)

        self.opt_path_terms_pub.publish(opt_path_msg)
               
        
        




    def gen_basis_vect(self,t, poly_order):
        t_basis = []
        for idx in range(poly_order+1): t_basis.append(t**idx)        
        return np.matrix(t_basis)


    def display_optimal_path(self,opt_path):
        print "display the optimal path in rviz"
        #First convert from 4d to se2
        path_list = Path()
        path_list.header.frame_id = '/map'
        pts_per_chord = 20 #number of point to use to sample the curve on a particular chord
        # print "opt path: ", opt_path
        for idx in range(len(opt_path["coeff"])):
            dt =  np.linspace(0, opt_path["T"][idx+1], pts_per_chord)
            for jdx in range(len(dt)):
                dt_curr= dt[jdx] 
                dt_vect = self.gen_basis_vect(dt_curr, self.poly_order) 
                Tmat = scipy.linalg.block_diag(dt_vect, dt_vect, dt_vect)
                coeff_vect = np.matrix(opt_path["coeff"][idx]).T             
                # print "Tmat: ", Tmat, Tmat.shape, "\n coeff: ", coeff_vect, coeff_vect.shape
                # print "dt_vect", dt_vect
                pose_curr= Tmat * coeff_vect
                # print "current pose: ", pose_curr, " curr chord,",idx," curr time: ", dt_curr
                theta = pose_curr.item(2)
                # if np.sign(theta) < 0: theta = 2*math.pi + theta
                theta_div_2 = theta/2.; qw = np.cos(theta_div_2); qz = np.sin(theta_div_2)
                pose_stamped = PoseStamped()
                pose_stamped.header.frame_id = '/map'
                pose_stamped.pose = Pose(Point(pose_curr.item(0),pose_curr.item(1),theta),Quaternion(0,0,qz,qw))
                path_list.poses.append(pose_stamped)
        # print "but seed path was: ", self.seed_path
        # print "and time vector was", opt_path["T"]
        self.nav_msg_traj_path = path_list
        self.path_pub.publish(path_list)
        print "published path"








def main():
    print "path plan pipeline"
    rospy.init_node('main_node_path_planner')

    cls_obj = Path_pipline()

    #get the start and goal locations
    #rospy.Subscriber("/robot_pose", PoseStamped, cls_obj.pose_callback, ("start"))
    #rospy.Subscriber("/move_base_simple/goal", PoseStamped, cls_obj.pose_callback, ("goal"))
    #Topics are  #"/pc" of type sensor_msgs/PointCloud #"/se2path_planner_node/path" of type nav_msgs/Path
    #rospy.Subscriber("/pc", PointCloud, cls_obj.mink3d_callback)
    #rospy.Subscriber("/se2path_planner_node/path", Path, cls_obj.se2path_callback)

    rospy.spin()


if __name__ == '__main__':
    main()
